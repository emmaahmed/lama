// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup


/**
 * Here is is the code snippet to initialize Firebase Messaging in the Service
 * Worker when your app is not hosted on Firebase Hosting.

 // Give the service worker access to Firebase Messaging.
 // Note that you can only use Firebase Messaging here. Other Firebase libraries
 // are not available in the service worker.
 importScripts('https://www.gstatic.com/firebasejs/8.3.0/firebase-app.js');
 importScripts('https://www.gstatic.com/firebasejs/8.3.0/firebase-messaging.js');

 // Initialize the Firebase app in the service worker by passing in
 // your app's Firebase config object.
 // https://firebase.google.com/docs/web/setup#config-object
 firebase.initializeApp({
   apiKey: 'api-key',
   authDomain: 'project-id.firebaseapp.com',
   databaseURL: 'https://project-id.firebaseio.com',
   projectId: 'project-id',
   storageBucket: 'project-id.appspot.com',
   messagingSenderId: 'sender-id',
   appId: 'app-id',
   measurementId: 'G-measurement-id',
 });

 // Retrieve an instance of Firebase Messaging so that it can handle background
 // messages.
 const messaging = firebase.messaging();
 **/
//  const messaging = firebase.messaging();

//             messaging.onBackgroundMessage(function(payload) {
//                 console.log('[firebase-messaging-sw.js] Received background message ', payload);
//                 // Customize notification here
//                 const notificationTitle = 'Background Message Title';
//                 const notificationOptions = {
//                     body: 'Background Message body.',
//                     icon: '/firebase-logo.png'
//                 };

//                 self.registration.showNotification(notificationTitle,
//                     notificationOptions);
//             });


// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// Keep in mind that FCM will still show notification messages automatically
// and you should use data messages for custom notifications.
// For more info see:
// https://firebase.google.com/docs/cloud-messaging/concept-options


// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/8.3.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.0/firebase-messaging.js');

const tokenDivId = 'token_div';
const permissionDivId = 'permission_div';
firebase.initializeApp({
    apiKey: 'AIzaSyADVFxbyZ5RxYuahKD2oVNHeTWGT70KLQE',
    authDomain: 'lamah-9a470.firebaseapp.com',
    databaseURL: 'https://lamah-9a470.firebaseio.com',
    projectId: 'lamah-9a470',
    storageBucket: 'lamah-9a470.appspot.com',
    messagingSenderId: '151230146434',
    appId: '1:151230146434:web:08452f3deb1f224be5919b',
    measurementId: 'G-measurement-id',

});





const messaging = firebase.messaging();
self.addEventListener('notificationclick', function(event) {
    event.notification.close();
    const notificationData = event.notification.data;
// console.log('');
// console.log('The notification data has the following parameters:');
// Object.keys(notificationData).forEach((key) => {
//   console.log(`  ${key}: ${notificationData[key]}`);
// });
// console.log('');
    var examplePage='';
    var promise = new Promise(function(resolve) {
        setTimeout(resolve, 1000);
    }).then(function() {

        if(notificationData['type']=='order'){
            examplePage = '/shop/orders/show-details?order_id='+notificationData['order_id'];
        }
        else if(notificationData['type']=='user-message'){
            examplePage = '/shop/reply-user/'+notificationData['user_id'];

        }
        else if(notificationData['type']=='admin-message'){
            examplePage = '/shop/reply/'+notificationData['shop_id'];

        }
        else{
            examplePage = '/admin/reply/'+notificationData['shop_id'];

        }
        console.log(notificationData['type']);
        console.log(notificationData['type']);

        return clients.openWindow(examplePage);
    });

    event.waitUntil(promise);
});

// messaging.setBackgroundMessageHandler(payload => {
//   const title = payload.data.title;

//   const notificationOptions = {
//     body: 'Background Message body.',
//     icon: '/firebase-logo.png',
//     link:'https://github.com/'
//   };

//   return self.registration.showNotification(title, options);
// });

messaging.onBackgroundMessage((payload) => {

    console.log('[firebase-messaging-sw.js] Received background message Emma', payload.data);
    // Customize notification here
    var notificationTitle='';
    var notificationBody='';

    var data= '';
    if(payload.data['gcm.notification.type']=='order'){
        notificationTitle = 'New Order';
        notificationBody = 'Click Here To View Order';
        data={
            message: 'New Notification!',
            type:payload.data['gcm.notification.type'],
            order_id:payload.data['gcm.notification.order_id'],
        };
    }
    else if(payload.data['gcm.notification.type']=='shop-message' || payload.data['gcm.notification.type']=='admin-message'){
        notificationTitle = 'New Message';
        notificationBody = 'Click Here To View Message';
        data={
            message: 'New Notification!',
            type:payload.data['gcm.notification.type'],
            user_message:payload.data['gcm.notification.user_message'],
            user_id:payload.data['gcm.notification.user_id'],
            user_name:payload.data['gcm.notification.user_name'],
            shop_id:payload.data['gcm.notification.shop_id'],
        };

    }
    else{
        notificationTitle = 'New Message';
        notificationBody = 'Click Here To View Message';
        data={
            message: 'New Notification!',
            type:payload.data['gcm.notification.type'],
            user_message:payload.data['gcm.notification.user_message'],
            user_id:payload.data['gcm.notification.user_id'],
            user_name:payload.data['gcm.notification.user_name'],
        };

    }
    const notificationOptions = {
        icon: '/logo.jpeg',
        body: notificationBody,
        badge: '/logo.jpeg',


        tag: 'data-notification',
        data:data,
        sound: 'https://lamh.online/sound.mp3',


        //    sound:'sound.mp3'

    };


    self.registration.showNotification(notificationTitle,
        notificationOptions);


});






