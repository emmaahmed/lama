var Cropimg = (function() {
	function imageUpload(id) {
		var imgvariable = eval('var $uploadCrop'+ id);

		function readFile(input) {
 			if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
					$('.upload-demo'+id).addClass('ready');
        	imgvariable.croppie('bind', {
        		url: e.target.result
        	}).then(function(){});
        }
        reader.readAsDataURL(input.files[0]);
	    }
		}

		imgvariable = $('#upload-demo'+id).croppie({
			viewport: {
				width: 250,
				height: 250,
			},
			enableOrientation: true,
			enableExif: true
		});

		$('#file_profile_pic'+id).on('change', function () {
			readFile(this);
			$('#crop_image_model .actions').removeClass('hidden');
			$('#selectFile').css('display', 'block')
		});

		$('.vanilla-rotate').on('click', function(ev) {
			imgvariable.croppie('rotate', parseInt($(this).data('deg')));
		});



		function clear_image_canvas(){
			$('#upload-demo'+id).croppie('destroy');
			imgvariable = $('#upload-demo'+id).croppie({
				viewport: {
					width: 250,
					height: 250,
				},
				enableOrientation: true,
				enableExif: true
			});
			imgvariable.croppie('bind', {url: ' '}).then(function(){});
			$('#file_profile_pic'+id).val('')
			$('.upload-demo'+id).removeClass('ready');
			$('#crop_image_model .actions').addClass('hidden');
			$('#selectFile').css('display', 'table')
		}

		$('.change_image_'+id).on('click', function () {
			clear_image_canvas()
		});

		$('.upload-result'+id).on('click', function (ev) {
			imgvariable.croppie('result', {
				type: 'canvas',
				size: 'viewport'
			}).then(function (resp) {
				existinginputs = document.getElementsByName('images_'+id);
				while (existinginputs.length > 0) {
					form.removeChild(existinginputs[0]);
				}
				if (resp.length > 7){
					document.getElementById('img_'+id).src = resp;
					var newinput = document.createElement("input");
					newinput.type = 'hidden';
					newinput.name = 'images_'+id;
					newinput.id = 'myimages_'+id;
					newinput.value = resp;
					form.appendChild(newinput);
				}
			});
		});
	}

	function init(id) {
		imageUpload(id);
	}

	return {
		init: init
	};
})();

