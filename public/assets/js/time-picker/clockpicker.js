'use strict';
// $('.clockpicker').clockpicker()
//             .find('input').change(function(){
//         console.log(this.value);
//     });
$('.clockpicker').each(function() {
    var clockpicker = $(this).find('input').clockpicker({
        autoclose: false,
        twelvehour: true,
        afterDone: function () {
            clockpicker.val(clockpicker.val().slice(0, -2) + ' ' + clockpicker.val().slice(-2));

        }
    });
});


    if (/Mobile/.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }
