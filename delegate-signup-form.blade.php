<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1, user-scalable=no"/>

    <title>طلب التسجيل كمندوب في لمه</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('default.png')}}">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/pure/0.6.0/pure-min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/pure/0.6.0/grids-responsive-min.css">
    <link rel="stylesheet" href="{{asset('assets/landing')}}/forms/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="{{asset('assets/landing')}}/forms/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets/landing')}}/forms/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('assets/landing')}}/forms/css/grids-responsive-min.css">
    <link rel="stylesheet" href="{{asset('assets/landing')}}/forms/css/pure-min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel='stylesheet' href='https://foliotek.github.io/Croppie/croppie.css'>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="{{asset('assets/landing')}}/forms/css/style.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/toastr.min.css') }}">


    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://getbootstrap.com/docs/3.3/examples/justified-nav/justified-nav.css" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- <noscript><meta http-equiv="refresh" content="0; url=nojsmsg.html" /></noscript> -->
    <style>
        body {
            background: #41A4AD;
        }

        .row {
            background: #fff;
            border-radius: 5px;
            margin: 0px;
        }

        div .rtl {
            direction: rtl !important;
            text-align: right !important;
        }

        .number-txt {
            direction: ltr !important;
            text-align: right !important;
        }

        p {
            text-align: right !important;
        }

        .red-text {
            color: red;
            font-weight: bold;
        }

        .submit-btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 16px;
            font-weight: 600;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-color: #41A4AD !important;
            color: white;
            border: 1px solid #41A4AD;
            border-radius: 5px;
            width: 100%;
        }

        .submit-btn:hover,
        .submit-btn:active {
            background-color: #41A4AD !important;
            color: white;
        }

        .submit-btn[disabled] {
            /*background-color: #b3b3b3!important;*/
            /*border: 1px solid #969696;*/
            border: 1px solid #41A4AD;
            background-color: #eee !important;
            color: #41A4AD;
            cursor: not-allowed;
        }

        #header {
            margin: 0 auto;
        }

        .header-top {
            width: auto;
            margin: 0 auto;
            background: white;
        }

        .main-menu-container {
            width: 1100px;
            margin: 0 auto;
            padding-bottom: 14px;
            overflow: hidden;
        }

        .logo {
            float: right;
            margin: 17px 0 0;
            background: top left no-repeat;
        }

        .form_link {
            color: white;
            text-decoration: underline;
        }

        @media only screen and (-webkit-min-device-pixel-ratio: 2), not all, not all, not all, only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
            .logo {
                background-repeat: no-repeat !important;
                background-size: 146px 30px !important;
            }
        }

        @media (max-width: 767px) {
            .logo {
                float: none !important;
                width: auto;
                background-position: top center;
                display: table;
                margin: 0 auto !important;
            }

            .main-menu-container {
                float: none;
                width: auto;
                padding-bottom: 10px;
            }
        }

        @media (max-width: 1199px) and (min-width: 992px) {
            .main-menu-container {
                width: 920px;
            }
        }

        @media (max-width: 991px) and (min-width: 768px) {
            .main-menu-container {
                padding-bottom: 18px;
                width: 750px;
            }
        }

        .pure-input-1 {
            border-bottom: 1px solid #e5e5e5 !important;
            border-radius: 0px !important;
            box-shadow: none !important;
        }

        .pure-form .pure-checkbox {
            margin: .5em 0 .5em .5em;
        }

        .thumbnail {
            padding: 0;
            margin-bottom: 20px;
            border: none;
        }

        .custom .checkbox input[type=checkbox] {
            display: none;
        }

        .custom .checkbox label {
            display: inline-block;
            cursor: auto;
            position: relative;
            font-size: 14px;
            font-weight: 600;
        }

        .custom .checkbox label:before {
            content: "";
            display: inline-block;
            width: 18px;
            height: 18px;
            margin-left: 10px;
            position: relative;
            left: 0;
            bottom: 1px;
            background: url("{{asset('assets/landing')}}/checkbox.png") no-repeat;
            background-size: 18px;
            top: 1px;
        }

        .custom .checkbox label:before {
            border-radius: 0px;
        }

        .custom .checkbox input[type=checkbox]:checked + label:before {
            content: "";
            background: url("{{asset('assets/landing')}}/checkbox-act.png") no-repeat;
            background-size: 18px;
        }

        .logo-icons {
            margin-right: 10px;
        }

        .tab-headings {
            font-weight: bold;
            color: #04d1d6 !important;
            font-size: 20px;
        }

        .nav-justified > li > a {
            background-color: #ededed !important;
        }

        .nav-tabs.nav-justified > .active > a,
        .nav-tabs.nav-justified > .active > a:hover {
            background-color: white !important;
        }

        @media (max-width: 768px) {
            .nav-justified > li {
                display: table-cell;
                width: 1%;
            }

            .nav-justified > li > a {
                border: none !important;
                border-radius: 4px 4px 0 0 !important;
                margin-bottom: 0 !important;
            }
        }

        .links {
            margin: 4px 10px;
            color: #049ea3 !important;
            /*font-size: 12px;*/
        }

        .langauge-selection-container {
            padding: 0 20px 0 20px;
        }
    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <script type='application/javascript' src='{{asset('assets/landing')}}/fastclick.js'></script>
</head>

<body>
<style type="text/css">
    #preview_cr1 canvas,
    #preview_cr2 canvas,
    #preview_cr3 canvas,
    #preview_cr4 canvas {
        width: 150px !important;
        height: 150px !important
    }

    #radioBtn img {
        width: 100%;
    }

    .hide_tab {
        display: none;
    }

    .error {
        border: 2px solid red !important;
    }

    .select2-container--default .select2-selection--single {
        border-radius: 0 !important;
        border-color: #ddd !important;
    }

    .select2-container .select2-selection--single {
        height: 35px !important;
    }

    .select2-container {
        margin-top: 3px !important;
    }

    .select2-results {
        direction: rtl !important;
    }
</style>


<div id="header">
    <div class="header-top">
        <div class="main-menu-container">
            <div class="logo">
                {{--<a href="#"><img height="50" width="200" src="{{asset('assets/landing')}}/logo.png"></a>--}}
                <a href="#"><img height="120" width="120" src="{{asset('default.png')}}"/></a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 rtl" style="padding: 10px 30px; color: white;font-weight: bold;">
        <h2 style="font-weight: bold;">نموذج توثيق مندوب في تطبيق لمه</h2>
        {{--        <h4 style="font-weight: bold;" class="pure-u-1 pure-u-md-1 pure-u-lg-1">--}}
        {{--            قبل تعبئة الاستمارة، نرجو منكم الاطلاع على الفيديو التالي: الأن بامكانك توثيق حسابك والحصول على الشارة الزرقاء في ملفك الشخصي يستخدم التوثيق لإثبات هوية الموصلين وذلك حتى يتم التعامل معهم بكل أمان!--}}
        {{--        </h4>--}}

        {{--        <h4 class="pure-u-1 pure-u-md-1 pure-u-lg-1">--}}
        {{--            <a target="_blank" href="#" style="color: #ffffff;">اضغط هنا</a>--}}
        {{--            <br>--}}
        {{--        </h4>--}}
    </div>

    <div class="row rtl" style="box-shadow: 0px -2px 9px -1px #06797c">
        <div class="signup_screen1 pure-form pure-form-stacked pure-g hide_tab">
            <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                <br><br>
                <div class="row">
                    <div class="col-md-4 col-sm-12" style="padding: 0;">
                        <div class="col-sm-12">
                            <label for="phone_code_signup" class="rtl">الدولة <i class="red-text">*</i></label>
                        </div>
                        <div class="col-sm-12">
                            <select name="phone_code_signup" id="phone_code_signup"
                                    class="rtl validate pure-input-1 select2-hidden-accessible"
                                    data-select2-id="phone_code_signup" tabindex="-1" aria-hidden="true">
                                <option value="" data-select2-id="4">الرجاء الاختيار</option>
                                <option value="1264">Anguilla - 1264</option>
                                <option value="297">Aruba - 297</option>
                                <option value="267">Botswana - 267</option>
                                <option value="1345">Cayman Islands - 1345</option>
                                <option value="61">Christmas Island - 61</option>
                                <option value="672">Cocos (Keeling) Islands - 672</option>

                                <option value="682">Cook Islands - 682</option>
                                <option value="225">Cote D&#39;ivoire - 225</option>
                                <option value="500">Falkland Islands (Malvinas) - 500</option>
                                <option value="298">Faroe Islands - 298</option>
                                <option value="350">Gibraltar - 350</option>
                                <option value="299">Greenland - 299</option>
                                <option value="590">Guadeloupe - 590</option>
                                <option value="1671">Guam - 1671</option>
                                <option value="852">Hong Kong - 852</option>
                                <option value="686">Kiribati - 686</option>
                                <option value="850">Korea, Democratic People&#39;s Republic Of - 850</option>
                                <option value="82">Korea, Republic Of - 82</option>
                                <option value="266">Lesotho - 266</option>
                                <option value="853">Macao - 853</option>
                                <option value="596">Martinique - 596</option>
                                <option value="269">Mayotte - 269</option>
                                <option value="1664">Montserrat - 1664</option>
                                <option value="599">Netherlands Antilles - 599</option>
                                <option value="687">New Caledonia - 687</option>
                                <option value="683">Niue - 683</option>
                                <option value="672">Norfolk Island - 672</option>
                                <option value="1670">Northern Mariana Islands - 1670</option>
                                <option value="262">Réunion - 262</option>
                                <option value="290">Saint Helena - 290</option>
                                <option value="1869">Saint Kitts And Nevis - 1869</option>
                                <option value="508">Saint Pierre And Miquelon - 508</option>
                                <option value="1784">Saint Vincent And The Grenadines - 1784</option>
                                <option value="47">Svalbard And Jan Mayen - 47</option>
                                <option value="670">Timor Leste - 670</option>
                                <option value="690">Tokelau - 690</option>
                                <option value="7370">Turkmenistan - 7370</option>
                                <option value="1649">Turks And Caicos Islands - 1649</option>
                                <option value="1">United States Minor Outlying Islands - 1</option>
                                <option value="678">Vanuatu - 678</option>
                                <option value="39">Vatican City - 39</option>
                                <option value="681">Wallis And Futuna - 681</option>
                                <option value="212">Western Sahara - 212</option>
                                <option value="93">Afghanistan - 93</option>
                                <option value="355">Albania - 355</option>
                                <option value="213">Algeria - 213</option>
                                <option value="1684">American Samoa - 1684</option>
                                <option value="1">United States - 1</option>
                                <option value="1340">Virgin Islands, U.S. - 1340</option>
                                <option value="376">Andorra - 376</option>
                                <option value="244">Angola - 244</option>
                                <option value="1268">Antigua And Barbuda - 1268</option>
                                <option value="54">Argentina - 54</option>
                                <option value="374">Armenia - 374</option>
                                <option value="61">Australia - 61</option>
                                <option value="43">Austria - 43</option>
                                <option value="994">Azerbaijan - 994</option>
                                <option value="1242">Bahamas - 1242</option>
                                <option value="973">Bahrain - 973</option>
                                <option value="880">Bangladesh - 880</option>
                                <option value="1246">Barbados - 1246</option>
                                <option value="375">Belarus - 375</option>
                                <option value="32">Belgium - 32</option>
                                <option value="501">Belize - 501</option>
                                <option value="229">Benin - 229</option>

                                <option value="975">Bhutan - 975</option>
                                <option value="591">Bolivia - 591</option>
                                <option value="387">Bosnia And Herzegovina - 387</option>
                                <option value="55">Brazil - 55</option>
                                <option value="246">British Indian Ocean Territory - 246</option>
                                <option value="44">United Kingdom - 44</option>
                                <option value="1284">Virgin Islands, British - 1284</option>
                                <option value="673">Brunei Darussalam - 673</option>
                                <option value="359">Bulgaria - 359</option>
                                <option value="226">Burkina Faso - 226</option>
                                <option value="1441">Bermuda - 1441</option>
                                <option value="95">Myanmar - 95</option>
                                <option value="257">Burundi - 257</option>
                                <option value="855">Cambodia - 855</option>
                                <option value="237">Cameroon - 237</option>
                                <option value="1">Canada - 1</option>
                                <option value="238">Cape Verde - 238</option>
                                <option value="236">Central African Republic - 236</option>
                                <option value="235">Chad - 235</option>
                                <option value="56">Chile - 56</option>
                                <option value="86">China - 86</option>
                                <option value="57">Colombia - 57</option>
                                <option value="269">Comoros - 269</option>
                                <option value="242">Congo - 242</option>
                                <option value="506">Costa Rica - 506</option>
                                <option value="385">Croatia - 385</option>
                                <option value="53">Cuba - 53</option>
                                <option value="357">Cyprus - 357</option>
                                <option value="420">Czech Republic - 420</option>
                                <option value="45">Denmark - 45</option>
                                <option value="253">Djibouti - 253</option>
                                <option value="1767">Dominica - 1767</option>
                                <option value="1809">Dominican Republic - 1809</option>
                                <option value="31">Netherlands - 31</option>
                                <option value="593">Ecuador - 593</option>
                                <option value="20">Egypt - 20</option>
                                <option value="971">United Arab Emirates - 971</option>
                                <option value="240">Equatorial Guinea - 240</option>
                                <option value="291">Eritrea - 291</option>
                                <option value="372">Estonia - 372</option>
                                <option value="251">Ethiopia - 251</option>
                                <option value="679">Fiji - 679</option>
                                <option value="63">Philippines - 63</option>
                                <option value="358">Finland - 358</option>
                                <option value="33">France - 33</option>
                                <option value="594">French Guiana - 594</option>
                                <option value="689">French Polynesia - 689</option>
                                <option value="241">Gabon - 241</option>
                                <option value="220">Gambia - 220</option>
                                <option value="995">Georgia - 995</option>
                                <option value="49">Germany - 49</option>
                                <option value="233">Ghana - 233</option>
                                <option value="30">Greece - 30</option>
                                <option value="1473">Grenada - 1473</option>
                                <option value="502">Guatemala - 502</option>
                                <option value="245">Guinea Bissau - 245</option>
                                <option value="224">Guinea - 224</option>
                                <option value="592">Guyana - 592</option>
                                <option value="509">Haiti - 509</option>
                                <option value="504">Honduras - 504</option>
                                <option value="36">Hungary - 36</option>
                                <option value="354">Iceland - 354</option>
                                <option value="91">India - 91</option>
                                <option value="62">Indonesia - 62</option>
                                <option value="98">Iran - 98</option>
                                <option value="964">Iraq - 964</option>
                                <option value="353">Ireland - 353</option>
                                <option value="972">Israel - 972</option>
                                <option value="39">Italy - 39</option>
                                <option value="1876">Jamaica - 1876</option>
                                <option value="81">Japan - 81</option>
                                <option value="962">Jordan - 962</option>
                                <option value="7">Kazakhstan - 7</option>
                                <option value="254">Kenya - 254</option>
                                <option value="965">Kuwait - 965</option>
                                <option value="996">Kyrgyzstan - 996</option>
                                <option value="856">Lao People&#39;s Democratic Republic - 856</option>
                                <option value="371">Latvia - 371</option>
                                <option value="961">Lebanon - 961</option>
                                <option value="231">Liberia - 231</option>
                                <option value="218">Libyan Arab Jamahiriya - 218</option>
                                <option value="423">Liechtenstein - 423</option>
                                <option value="370">Lithuania - 370</option>
                                <option value="352">Luxembourg - 352</option>
                                <option value="389">Macedonia, The Former Yugoslav Republic Of - 389</option>
                                <option value="261">Madagascar - 261</option>
                                <option value="265">Malawi - 265</option>
                                <option value="60">Malaysia - 60</option>
                                <option value="960">Maldives - 960</option>
                                <option value="223">Mali - 223</option>
                                <option value="356">Malta - 356</option>
                                <option value="692">Marshall Islands - 692</option>
                                <option value="222">Mauritania - 222</option>
                                <option value="230">Mauritius - 230</option>
                                <option value="52">Mexico - 52</option>
                                <option value="691">Micronesia, Federated States Of - 691</option>
                                <option value="373">Moldova, Republic Of - 373</option>
                                <option value="377">Monaco - 377</option>
                                <option value="976">Mongolia - 976</option>
                                <option value="212">Morocco - 212</option>
                                <option value="258">Mozambique - 258</option>
                                <option value="264">Namibia - 264</option>
                                <option value="674">Nauru - 674</option>
                                <option value="977">Nepal - 977</option>
                                <option value="64">New Zealand - 64</option>
                                <option value="505">Nicaragua - 505</option>
                                <option value="227">Niger - 227</option>
                                <option value="234">Nigeria - 234</option>
                                <option value="47">Norway - 47</option>
                                <option value="968">Oman - 968</option>
                                <option value="92">Pakistan - 92</option>
                                <option value="680">Palau - 680</option>
                                <option value="970">Palestinian Territory, Occupied - 970</option>
                                <option value="507">Panama - 507</option>
                                <option value="675">Papua New Guinea - 675</option>
                                <option value="595">Paraguay - 595</option>
                                <option value="51">Peru - 51</option>
                                <option value="48">Poland - 48</option>
                                <option value="351">Portugal - 351</option>

                                <option value="1787">Puerto Rico - 1787</option>
                                <option value="974">Qatar - 974</option>
                                <option value="40">Romania - 40</option>
                                <option value="70">Russia - 70</option>
                                <option value="250">Rwanda - 250</option>
                                <option value="1758">Saint Lucia - 1758</option>
                                <option value="503">El Salvador - 503</option>
                                <option value="684">Samoa - 684</option>
                                <option value="378">San Marino - 378</option>
                                <option value="239">Sao Tome And Principe - 239</option>
                                <option value="966">Saudi Arabia - 966</option>
                                <option value="221">Senegal - 221</option>
                                <option value="381">Serbia - 381</option>
                                <option value="248">Seychelles - 248</option>
                                <option value="232">Sierra Leone - 232</option>
                                <option value="65">Singapore - 65</option>
                                <option value="421">Slovakia - 421</option>
                                <option value="386">Slovenia - 386</option>
                                <option value="677">Solomon Islands - 677</option>
                                <option value="252">Somalia - 252</option>
                                <option value="27">South Africa - 27</option>
                                <option value="211">South Sudan - 211</option>
                                <option value="34">Spain - 34</option>
                                <option value="94">Sri Lanka - 94</option>
                                <option value="249">Sudan - 249</option>
                                <option value="597">Suriname - 597</option>
                                <option value="268">Swaziland - 268</option>
                                <option value="46">Sweden - 46</option>
                                <option value="41">Switzerland - 41</option>
                                <option value="963">Syrian Arab Republic - 963</option>
                                <option value="886">Taiwan, Province Of China - 886</option>
                                <option value="992">Tajikistan - 992</option>
                                <option value="255">Tanzania, United Republic Of - 255</option>
                                <option value="66">Thailand - 66</option>
                                <option value="228">Togo - 228</option>
                                <option value="676">Tonga - 676</option>
                                <option value="1868">Trinidad And Tobago - 1868</option>
                                <option value="216">Tunisia - 216</option>
                                <option value="90">Turkey - 90</option>
                                <option value="688">Tuvalu - 688</option>
                                <option value="256">Uganda - 256</option>
                                <option value="380">Ukraine - 380</option>
                                <option value="598">Uruguay - 598</option>
                                <option value="998">Uzbekistan - 998</option>
                                <option value="58">Venezuela - 58</option>
                                <option value="84">Viet Nam - 84</option>
                                <option value="967">Yemen - 967</option>
                                <option value="260">Zambia - 260</option>
                                <option value="263">Zimbabwe - 263</option>
                            </select><span class="select2 select2-container select2-container--default" dir="rtl"
                                           data-select2-id="3" style="width: 213px;"><span class="selection needsclick"><span
                                        class="select2-selection select2-selection--single needsclick" role="combobox"
                                        aria-haspopup="true" aria-expanded="false" tabindex="0"
                                        aria-labelledby="select2-phone_code_signup-container"><span
                                            class="select2-selection__rendered needsclick"
                                            id="select2-phone_code_signup-container" role="textbox" aria-readonly="true"
                                            title="Egypt - 20">Egypt - 20</span><span
                                            class="select2-selection__arrow needsclick" role="presentation"><b
                                                role="presentation"></b></span></span></span><span
                                    class="dropdown-wrapper needsclick" aria-hidden="true"></span></span>
                        </div>
                    </div>

                    <div class="col-md-8 col-sm-12" style="padding: 0;">
                        <div class="col-sm-12">
                            <label for="phone_signup" class="rtl">رقم الهاتف (أدخل الرقم مباشرة بدون الصفر وبدون مفتاح
                                الدولة) <i class="red-text">*</i></label>
                        </div>
                        <div class="col-sm-12">
                            <input id="phone_signup" type="tel" name="Phone"
                                   class="rtl validate pure-input-1 number-txt">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br>
            </div>


            <div id="send_code_success_signup" class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12"
                 style="display:none">
                <h3></h3>
                <div class="clearfix"></div>
                <br>
            </div>

        </div>
        <div class="signup_screen2 pure-form pure-form-stacked pure-g hide_tab">
            <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                <label class="rtl">رقم الهاتف</label>
                <div style="display: inline-flex;">
                    <label style="direction: ltr !important;" id="signup_user_phone">+201090624441</label>
                    <a href="javascript:void(0);" class="links" id="change_no" title="غير رقم الجوال"><i
                            class="fa fa-edit"></i></a>
                    <input type="hidden" name="user_phone_verify" id="user_phone_verify" value="201090624441">
                </div>
                <div class="clearfix"></div>
                <br>
            </div>

            <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                <label for="code_signup" class="rtl">رقم التحقق</label>
                <div class="col-md-12 col-xs-12" style="display: inline-flex;padding: 0;">
                    <div class="col-sm-8" style="padding: 0;">
                        <input id="code_signup" type="text" pattern="[0-9]*" name="Code"
                               class="validate pure-input-1 number-txt" required="" aria-required="true">
                    </div>
                    <div class="col-sm-4" style="padding-top: 12px;">
                        <a href="javascript:void(0);" class="links" id="timer" style="display: none;"></a>
                        <a href="javascript:void(0);" class="links" id="resend_code_signup" style=""> أعد ارسال
                            الكود </a>

                        <!-- <a href="javascript:void(0);" class="links" id="resend_code_sms_signup" style="display:none"> ارسل كرسالة نصية </a> -->
                    </div>
                </div>
                <div class="clearfix"></div>
                <br>
            </div>


            <div id="verify_code_success_signup" class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-12 col-xs-12"
                 style="display:none">
                <h3></h3>
                <div class="clearfix"></div>
                <br>
            </div>
        </div>
        <div class="signup_screen3 pure-form pure-form-stacked">
            <form id="form" method="post" action="{{route('form-submit')}}" enctype="multipart/form-data"
                  class="pure-form pure-form-stacked pure-g" style="padding: 10px 20px" accept-charset="UTF-8">
                @csrf
                {{--                <input name="verified_email" value="{{!empty(Session::get('email')) ? Session::get('email') : 0}}" style="display:none" >--}}
                {{--                <input name="verified_phone" value="{{!empty(Session::get('phone')) ? Session::get('phone') : 0}}" style="display:none" >--}}
                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                    <h3 class="rtl">بيانات المندوب</h3>
                    <div class="clearfix"></div>
                    <br>
                </div>
                <input type="hidden" value="0" name="type"/>
                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                    <label for="name" class="rtl">الاسم كاملاً <i class="red-text">*</i> Full name</label>
                    <input id="name" type="text" name="name" class="rtl validate pure-input-1"
                           onchange="check_missing_input(&#39;name&#39;);">
                    <div class="clearfix"></div>
                    <br>
                </div>

                {{--                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">--}}
                {{--                    <label for="national_id" class="rtl">رقم الهوية الوطنية / هوية مقيم <i class="red-text">*</i> NationalID</label>--}}
                {{--                    <label class="nationalid_validation" style="color: red;"></label>--}}
                {{--                    <input id="national_id" type="text" name="national_id" class="rtl validate pure-input-1" onchange="check_missing_input(&#39;nationalid&#39;);">--}}
                {{--                    <div class="clearfix"></div>--}}
                {{--                    <br>--}}
                {{--                </div>--}}

                {{--                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">--}}
                {{--                    <label for="citizen_ship" class="rtl">الجنسية <i class="red-text">*</i> citizenship</label>--}}
                {{--                    <select name="citizen_ship" id="citizen_ship" class="rtl validate pure-input-1 select2-hidden-accessible"  style="width:100%;" data-select2-id="citizinship" tabindex="-1" >--}}
                {{--                        <option value="Afghanistan - AFGHAN">Afghanistan - AFGHAN</option>--}}
                {{--                        <option value="Albania - ALBANIAN">Albania - ALBANIAN</option>--}}
                {{--                        <option value="Algeria - ALGERIAN">Algeria - ALGERIAN</option>--}}
                {{--                        <option value="American Samoa - AMERICAN">American Samoa - AMERICAN</option>--}}
                {{--                        <option value="United States - AMERICAN">United States - AMERICAN</option>--}}
                {{--                        <option value="Virgin Islands, U.S. - AMERICAN">Virgin Islands, U.S. - AMERICAN</option>--}}
                {{--                        <option value="Andorra - ANDORRAN">Andorra - ANDORRAN</option>--}}
                {{--                        <option value="Angola - ANGOLAN">Angola - ANGOLAN</option>--}}
                {{--                        <option value="Antigua And Barbuda - ANTIGUAN">Antigua And Barbuda - ANTIGUAN</option>--}}
                {{--                        <option value="Argentina - ARGENTINEAN">Argentina - ARGENTINEAN</option>--}}
                {{--                        <option value="Armenia - ARMENIAN">Armenia - ARMENIAN</option>--}}
                {{--                        <option value="Australia - AUSTRALIAN">Australia - AUSTRALIAN</option>--}}
                {{--                        <option value="Austria - AUSTRIAN">Austria - AUSTRIAN</option>--}}
                {{--                        <option value="Azerbaijan - AZERBAIJANI">Azerbaijan - AZERBAIJANI</option>--}}
                {{--                        <option value="Bahamas - BAHAMIAN">Bahamas - BAHAMIAN</option>--}}
                {{--                        <option value="Bahrain - BAHRAINI">Bahrain - BAHRAINI</option>--}}
                {{--                        <option value="Bangladesh - BANGLADESHI">Bangladesh - BANGLADESHI</option>--}}
                {{--                        <option value="Barbados - BARBADIAN">Barbados - BARBADIAN</option>--}}
                {{--                        <option value="Belarus - BELARUSIAN">Belarus - BELARUSIAN</option>--}}
                {{--                        <option value="Belgium - BELGIAN">Belgium - BELGIAN</option>--}}
                {{--                        <option value="Belize - BELIZEAN">Belize - BELIZEAN</option>--}}
                {{--                        <option value="Benin - BENINESE">Benin - BENINESE</option>--}}
                {{--                        <option value="Bhutan - BHUTANESE">Bhutan - BHUTANESE</option>--}}
                {{--                        <option value="Bolivia - BOLIVIAN">Bolivia - BOLIVIAN</option>--}}
                {{--                        <option value="Bosnia And Herzegovina - BOSNIAN">Bosnia And Herzegovina - BOSNIAN</option>--}}
                {{--                        <option value="Brazil - BRAZILIAN">Brazil - BRAZILIAN</option>--}}
                {{--                        <option value="British Indian Ocean Territory - BRITISH">British Indian Ocean Territory - BRITISH</option>--}}
                {{--                        <option value="United Kingdom - BRITISH">United Kingdom - BRITISH</option>--}}
                {{--                        <option value="Virgin Islands, British - BRITISH">Virgin Islands, British - BRITISH</option>--}}
                {{--                        <option value="Brunei Darussalam - BRUNEIAN">Brunei Darussalam - BRUNEIAN</option>--}}
                {{--                        <option value="Bulgaria - BULGARIAN">Bulgaria - BULGARIAN</option>--}}
                {{--                        <option value="Burkina Faso - BURKINABE">Burkina Faso - BURKINABE</option>--}}
                {{--                        <option value="Bermuda - BURMESE">Bermuda - BURMESE</option>--}}
                {{--                        <option value="Myanmar - Burmese">Myanmar - Burmese</option>--}}
                {{--                        <option value="Burundi - BURUNDIAN">Burundi - BURUNDIAN</option>--}}
                {{--                        <option value="Cambodia - CAMBODIAN">Cambodia - CAMBODIAN</option>--}}
                {{--                        <option value="Cameroon - CAMEROONIAN">Cameroon - CAMEROONIAN</option>--}}
                {{--                        <option value="Canada - CANADIAN">Canada - CANADIAN</option>--}}
                {{--                        <option value="Cape Verde - CAPE VERDEAN">Cape Verde - CAPE VERDEAN</option>--}}
                {{--                        <option value="Central African Republic - CENTRAL AFRICAN">Central African Republic - CENTRAL AFRICAN</option>--}}
                {{--                        <option value="Chad - CHADIAN">Chad - CHADIAN</option>--}}
                {{--                        <option value="Chile - CHILEAN">Chile - CHILEAN</option>--}}
                {{--                        <option value="China - CHINESE">China - CHINESE</option>--}}
                {{--                        <option value="Colombia - COLOMBIAN">Colombia - COLOMBIAN</option>--}}
                {{--                        <option value="Comoros - COMORAN">Comoros - COMORAN</option>--}}
                {{--                        <option value="Congo - CONGOLESE">Congo - CONGOLESE</option>--}}
                {{--                        <option value="Costa Rica - COSTA RICAN">Costa Rica - COSTA RICAN</option>--}}
                {{--                        <option value="Croatia - CROATIAN">Croatia - CROATIAN</option>--}}
                {{--                        <option value="Cuba - CUBAN">Cuba - CUBAN</option>--}}
                {{--                        <option value="Cyprus - CYPRIOT">Cyprus - CYPRIOT</option>--}}
                {{--                        <option value="Czech Republic - CZECH">Czech Republic - CZECH</option>--}}
                {{--                        <option value="Denmark - DANISH">Denmark - DANISH</option>--}}
                {{--                        <option value="Djibouti - DJIBOUTIAN">Djibouti - DJIBOUTIAN</option>--}}
                {{--                        <option value="Dominica - DOMINICAN">Dominica - DOMINICAN</option>--}}
                {{--                        <option value="Dominican Republic - DOMINICAN">Dominican Republic - DOMINICAN</option>--}}
                {{--                        <option value="Netherlands - DUTCH">Netherlands - DUTCH</option>--}}
                {{--                        <option value="Ecuador - ECUADOREAN">Ecuador - ECUADOREAN</option>--}}
                {{--                        <option value="Egypt - EGYPTIAN" data-select2-id="5">Egypt - EGYPTIAN</option>--}}
                {{--                        <option value="United Arab Emirates - EMIRATI">United Arab Emirates - EMIRATI</option>--}}
                {{--                        <option value="Equatorial Guinea - EQUATORIAL GUINEAN">Equatorial Guinea - EQUATORIAL GUINEAN</option>--}}
                {{--                        <option value="Eritrea - ERITREAN">Eritrea - ERITREAN</option>--}}
                {{--                        <option value="Estonia - ESTONIAN">Estonia - ESTONIAN</option>--}}
                {{--                        <option value="Ethiopia - ETHIOPIAN">Ethiopia - ETHIOPIAN</option>--}}
                {{--                        <option value="Fiji - FIJIAN">Fiji - FIJIAN</option>--}}
                {{--                        <option value="Philippines - FILIPINO">Philippines - FILIPINO</option>--}}
                {{--                        <option value="Finland - FINNISH">Finland - FINNISH</option>--}}
                {{--                        <option value="France - FRENCH">France - FRENCH</option>--}}
                {{--                        <option value="French Guiana - FRENCH">French Guiana - FRENCH</option>--}}
                {{--                        <option value="French Polynesia - FRENCH">French Polynesia - FRENCH</option>--}}
                {{--                        <option value="French Southern Territories - FRENCH">French Southern Territories - FRENCH</option>--}}
                {{--                        <option value="Gabon - GABONESE">Gabon - GABONESE</option>--}}
                {{--                        <option value="Gambia - GAMBIAN">Gambia - GAMBIAN</option>--}}
                {{--                        <option value="Georgia - GEORGIAN">Georgia - GEORGIAN</option>--}}
                {{--                        <option value="Germany - GERMAN">Germany - GERMAN</option>--}}
                {{--                        <option value="Ghana - GHANAIAN">Ghana - GHANAIAN</option>--}}
                {{--                        <option value="Greece - GREEK">Greece - GREEK</option>--}}
                {{--                        <option value="Grenada - GRENADIAN">Grenada - GRENADIAN</option>--}}
                {{--                        <option value="Guatemala - GUATEMALAN">Guatemala - GUATEMALAN</option>--}}
                {{--                        <option value="Guinea Bissau - GUINEA-BISSAUAN">Guinea Bissau - GUINEA-BISSAUAN</option>--}}
                {{--                        <option value="Guinea - GUINEAN">Guinea - GUINEAN</option>--}}
                {{--                        <option value="Guyana - GUYANESE">Guyana - GUYANESE</option>--}}
                {{--                        <option value="Haiti - HAITIAN">Haiti - HAITIAN</option>--}}
                {{--                        <option value="Honduras - HONDURAN">Honduras - HONDURAN</option>--}}
                {{--                        <option value="Hungary - HUNGARIAN">Hungary - HUNGARIAN</option>--}}
                {{--                        <option value="Iceland - ICELANDER">Iceland - ICELANDER</option>--}}
                {{--                        <option value="India - INDIAN">India - INDIAN</option>--}}
                {{--                        <option value="Indonesia - INDONESIAN">Indonesia - INDONESIAN</option>--}}
                {{--                        <option value="Iran - IRANIAN">Iran - IRANIAN</option>--}}
                {{--                        <option value="Iraq - IRAQI">Iraq - IRAQI</option>--}}
                {{--                        <option value="Ireland - IRISH">Ireland - IRISH</option>--}}
                {{--                        <option value="Israel - ISRAELI">Israel - ISRAELI</option>--}}
                {{--                        <option value="Italy - ITALIAN">Italy - ITALIAN</option>--}}
                {{--                        <option value="Jamaica - JAMAICAN">Jamaica - JAMAICAN</option>--}}
                {{--                        <option value="Japan - JAPANESE">Japan - JAPANESE</option>--}}
                {{--                        <option value="Jordan - JORDANIAN">Jordan - JORDANIAN</option>--}}
                {{--                        <option value="Kazakhstan - KAZAKHSTANI">Kazakhstan - KAZAKHSTANI</option>--}}
                {{--                        <option value="Kenya - KENYAN">Kenya - KENYAN</option>--}}
                {{--                        <option value="Kuwait - KUWAITI">Kuwait - KUWAITI</option>--}}
                {{--                        <option value="Kyrgyzstan - KYRGYZ">Kyrgyzstan - KYRGYZ</option>--}}
                {{--                        <option value="Lao People's Democratic Republic - LAOTIAN">Lao People's Democratic Republic - LAOTIAN</option>--}}
                {{--                        <option value="Latvia - LATVIAN">Latvia - LATVIAN</option>--}}
                {{--                        <option value="Lebanon - LEBANESE">Lebanon - LEBANESE</option>--}}
                {{--                        <option value="Liberia - LIBERIAN">Liberia - LIBERIAN</option>--}}
                {{--                        <option value="Libyan Arab Jamahiriya - LIBYAN">Libyan Arab Jamahiriya - LIBYAN</option>--}}
                {{--                        <option value="Liechtenstein - LIECHTENSTEINER">Liechtenstein - LIECHTENSTEINER</option>--}}
                {{--                        <option value="Lithuania - LITHUANIAN">Lithuania - LITHUANIAN</option>--}}
                {{--                        <option value="Luxembourg - LUXEMBOURGER">Luxembourg - LUXEMBOURGER</option>--}}
                {{--                        <option value="Macedonia, The Former Yugoslav Republic Of - MACEDONIAN">Macedonia, The Former Yugoslav Republic Of - MACEDONIAN</option>--}}
                {{--                        <option value="Madagascar - MADAGASCAN">Madagascar - MADAGASCAN</option>--}}
                {{--                        <option value="Malawi - MALAWIAN">Malawi - MALAWIAN</option>--}}
                {{--                        <option value="Malaysia - MALAYSIAN">Malaysia - MALAYSIAN</option>--}}
                {{--                        <option value="Maldives - MALDIVIAN">Maldives - MALDIVIAN</option>--}}
                {{--                        <option value="Mali - MALIAN">Mali - MALIAN</option>--}}
                {{--                        <option value="Malta - MALTESE">Malta - MALTESE</option>--}}
                {{--                        <option value="Marshall Islands - MARSHALLESE">Marshall Islands - MARSHALLESE</option>--}}
                {{--                        <option value="Mauritania - MAURITANIAN">Mauritania - MAURITANIAN</option>--}}
                {{--                        <option value="Mauritius - MAURITIAN">Mauritius - MAURITIAN</option>--}}
                {{--                        <option value="Mexico - MEXICAN">Mexico - MEXICAN</option>--}}
                {{--                        <option value="Micronesia, Federated States Of - MICRONESIAN">Micronesia, Federated States Of - MICRONESIAN</option>--}}
                {{--                        <option value="Moldova, Republic Of - MOLDOVAN">Moldova, Republic Of - MOLDOVAN</option>--}}
                {{--                        <option value="Monaco - MONACAN">Monaco - MONACAN</option>--}}
                {{--                        <option value="Mongolia - MONGOLIAN">Mongolia - MONGOLIAN</option>--}}
                {{--                        <option value="Morocco - MOROCCAN">Morocco - MOROCCAN</option>--}}
                {{--                        <option value="Mozambique - MOZAMBICAN">Mozambique - MOZAMBICAN</option>--}}
                {{--                        <option value="Namibia - NAMIBIAN">Namibia - NAMIBIAN</option>--}}
                {{--                        <option value="Nauru - NAURUAN">Nauru - NAURUAN</option>--}}
                {{--                        <option value="Nepal - NEPALESE">Nepal - NEPALESE</option>--}}
                {{--                        <option value="New Zealand - NEW ZEALANDER">New Zealand - NEW ZEALANDER</option>--}}
                {{--                        <option value="Nicaragua - NICARAGUAN">Nicaragua - NICARAGUAN</option>--}}
                {{--                        <option value="Niger - NIGERIAN">Niger - NIGERIAN</option>--}}
                {{--                        <option value="Nigeria - NIGERIEN">Nigeria - NIGERIEN</option>--}}
                {{--                        <option value="Norway - NORWEGIAN">Norway - NORWEGIAN</option>--}}
                {{--                        <option value="Oman - OMANI">Oman - OMANI</option>--}}
                {{--                        <option value="Pakistan - PAKISTANI">Pakistan - PAKISTANI</option>--}}
                {{--                        <option value="Palau - PALAUAN">Palau - PALAUAN</option>--}}
                {{--                        <option value="Palestinian Territory, Occupied - Palestinian">Palestinian Territory, Occupied - Palestinian</option>--}}
                {{--                        <option value="Panama - PANAMANIAN">Panama - PANAMANIAN</option>--}}
                {{--                        <option value="Papua New Guinea - PAPUA NEW GUINEAN">Papua New Guinea - PAPUA NEW GUINEAN</option>--}}
                {{--                        <option value="Paraguay - PARAGUAYAN">Paraguay - PARAGUAYAN</option>--}}
                {{--                        <option value="Peru - PERUVIAN">Peru - PERUVIAN</option>--}}
                {{--                        <option value="Poland - POLISH">Poland - POLISH</option>--}}
                {{--                        <option value="Portugal - PORTUGUESE">Portugal - PORTUGUESE</option>--}}
                {{--                        <option value="Puerto Rico - PUERTO RICAN">Puerto Rico - PUERTO RICAN</option>--}}
                {{--                        <option value="Qatar - QATARI">Qatar - QATARI</option>--}}
                {{--                        <option value="Romania - ROMANIAN">Romania - ROMANIAN</option>--}}
                {{--                        <option value="Russia - RUSSIAN">Russia - RUSSIAN</option>--}}
                {{--                        <option value="Rwanda - RWANDAN">Rwanda - RWANDAN</option>--}}
                {{--                        <option value="Saint Lucia - SAINT LUCIAN">Saint Lucia - SAINT LUCIAN</option>--}}
                {{--                        <option value="El Salvador - SALVADOREAN">El Salvador - SALVADOREAN</option>--}}
                {{--                        <option value="Samoa - SAMOAN">Samoa - SAMOAN</option>--}}
                {{--                        <option value="San Marino - SAN MARINESE">San Marino - SAN MARINESE</option>--}}
                {{--                        <option value="Sao Tome And Principe - SAO TOMEAN">Sao Tome And Principe - SAO TOMEAN</option>--}}
                {{--                        <option value="Saudi Arabia - SAUDI" selected>Saudi Arabia - SAUDI</option>--}}
                {{--                        <option value="Senegal - SENEGALESE">Senegal - SENEGALESE</option>--}}
                {{--                        <option value="Serbia - SERBIAN">Serbia - SERBIAN</option>--}}
                {{--                        <option value="Seychelles - SEYCHELLOIS">Seychelles - SEYCHELLOIS</option>--}}
                {{--                        <option value="Sierra Leone - SIERRA LEONEAN">Sierra Leone - SIERRA LEONEAN</option>--}}
                {{--                        <option value="Singapore - SINGAPOREAN">Singapore - SINGAPOREAN</option>--}}
                {{--                        <option value="Slovakia - SLOVAKIAN">Slovakia - SLOVAKIAN</option>--}}
                {{--                        <option value="Slovenia - SLOVENIAN">Slovenia - SLOVENIAN</option>--}}
                {{--                        <option value="Solomon Islands - SOLOMON ISLANDER">Solomon Islands - SOLOMON ISLANDER</option>--}}
                {{--                        <option value="Somalia - SOMALI">Somalia - SOMALI</option>--}}
                {{--                        <option value="South Africa - SOUTH AFRICAN">South Africa - SOUTH AFRICAN</option>--}}
                {{--                        <option value="South Sudan - SOUTH SUDANESE">South Sudan - SOUTH SUDANESE</option>--}}
                {{--                        <option value="Spain - SPANISH">Spain - SPANISH</option>--}}
                {{--                        <option value="Sri Lanka - SRI LANKAN">Sri Lanka - SRI LANKAN</option>--}}
                {{--                        <option value="Sudan - SUDANESE">Sudan - SUDANESE</option>--}}
                {{--                        <option value="Suriname - SURINAMER">Suriname - SURINAMER</option>--}}
                {{--                        <option value="Swaziland - SWAZI">Swaziland - SWAZI</option>--}}
                {{--                        <option value="Sweden - SWEDISH">Sweden - SWEDISH</option>--}}
                {{--                        <option value="Switzerland - SWISS">Switzerland - SWISS</option>--}}
                {{--                        <option value="Syrian Arab Republic - SYRIAN">Syrian Arab Republic - SYRIAN</option>--}}
                {{--                        <option value="Taiwan, Province Of China - TAIWANESE">Taiwan, Province Of China - TAIWANESE</option>--}}
                {{--                        <option value="Tajikistan - TAJIK">Tajikistan - TAJIK</option>--}}
                {{--                        <option value="Tanzania, United Republic Of - TANZANIAN">Tanzania, United Republic Of - TANZANIAN</option>--}}
                {{--                        <option value="Thailand - THAI">Thailand - THAI</option>--}}
                {{--                        <option value="Togo - TOGOLESE">Togo - TOGOLESE</option>--}}
                {{--                        <option value="Tonga - TONGAN">Tonga - TONGAN</option>--}}
                {{--                        <option value="Trinidad And Tobago - TRINIDADIAN OR TOBAGONIAN">Trinidad And Tobago - TRINIDADIAN OR TOBAGONIAN</option>--}}
                {{--                        <option value="Tunisia - TUNISIAN">Tunisia - TUNISIAN</option>--}}
                {{--                        <option value="Turkey - TURKISH">Turkey - TURKISH</option>--}}
                {{--                        <option value="Tuvalu - TUVALUAN">Tuvalu - TUVALUAN</option>--}}
                {{--                        <option value="Uganda - UGANDAN">Uganda - UGANDAN</option>--}}
                {{--                        <option value="Ukraine - UKRAINIAN">Ukraine - UKRAINIAN</option>--}}
                {{--                        <option value="Uruguay - URUGUAYAN">Uruguay - URUGUAYAN</option>--}}
                {{--                        <option value="Uzbekistan - UZBEK">Uzbekistan - UZBEK</option>--}}
                {{--                        <option value="Venezuela - VENEZUELAN">Venezuela - VENEZUELAN</option>--}}
                {{--                        <option value="Viet Nam - VIETNAMESE">Viet Nam - VIETNAMESE</option>--}}
                {{--                        <option value="Yemen - YEMENI">Yemen - YEMENI</option>--}}
                {{--                        <option value="Zambia - ZAMBIAN">Zambia - ZAMBIAN</option>--}}
                {{--                        <option value="Zimbabwe - ZIMBABWEAN">Zimbabwe - ZIMBABWEAN</option>--}}
                {{--                    </select>--}}
                {{--                    --}}{{--                    <span class="select2 select2-container select2-container--default" dir="rtl" data-select2-id="1" style="width: 100%;"><span class="selection needsclick"><span class="select2-selection select2-selection--single needsclick" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-citizinship-container"><span class="select2-selection__rendered needsclick" id="select2-citizinship-container" role="textbox" aria-readonly="true" title="Egypt - EGYPTIAN">Egypt - EGYPTIAN</span><span class="select2-selection__arrow needsclick" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper needsclick" aria-hidden="true"></span></span>--}}
                {{--                    <div class="clearfix"></div>--}}
                {{--                    <br>--}}
                {{--                </div>--}}

                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                    <label for="mobile" class="rtl">رقم الهاتف (أدخل الرقم مباشرة بدون الصفر وبدون مفتاح الدولة) <i
                            class="red-text">*</i> Phone
                    </label>
                    <label class="nationalid_validation" style="color: red;"></label>
                    <input id="phone" type="text" name="phone" class="rtl validate pure-input-1" value="">
                    <div class="clearfix"></div>
                    <br>
                </div>

                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                    <label for="email" class="rtl">البريد الإلكتروني <i class="red-text">*</i> Email</label>
                    <input id="email" name="email" class="rtl validate pure-input-1"
                           onchange="check_missing_input(&#39;email&#39;);">
                    <div class="clearfix"></div>
                    <br>
                </div>

                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                    <label for="address" class="rtl"> عنوان السكن <i class="red-text">*</i> Address</label>
                    <input id="address" name="address" class="rtl validate pure-input-1"
                           onchange="check_missing_input(&#39;email&#39;);">
                    <div class="clearfix"></div>
                    <br>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">&nbsp;</div>
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">&nbsp;</div>
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">&nbsp;</div>
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">

                        <div class="row">
                            <div class="col-xs-12">
                                <label class="cabinet center-block">
                                    <figure>
                                        <img src="" class="gambar img-responsive img-thumbnail" id="item-img-output"/>
                                        <figcaption><i class="fa fa-camera"></i></figcaption>
                                    </figure>
                                    <input type="file" class="item-img file center-block" name="image"
                                           accept="image/*"/>
                                </label>
                            </div>
                        </div>


                        <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-body">
                                        <div id="upload-demo" class="center-block"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                        <button type="button" id="cropImageBtn" class="btn btn-primary">Crop</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <label>الصورة الشخصية <i class="red-text">*</i></label>
                        <button type="button" class="crop-img-modal-cr5">الصورة الشخصية</button>
                        <br>
                    </div>
                </div>

                {{--                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">--}}
                {{--                    <label for="car_type" class="rtl">نوع السيارة <i class="red-text">*</i></label>--}}
                {{--                    <select name="car_type" id="car_type" class="rtl validate pure-input-1" >--}}
                {{--                        <option value="">Please Select</option>--}}
                {{--                        <option value="AC">AC</option>--}}
                {{--                        <option value="Acura">Acura</option>--}}
                {{--                        <option value="Alfa Romeo">Alfa Romeo</option>--}}
                {{--                        <option value="Alpina">Alpina</option>--}}
                {{--                        <option value="Alpine">Alpine</option>--}}
                {{--                        <option value="Aro">Aro</option>--}}
                {{--                        <option value="Asia">Asia</option>--}}
                {{--                        <option value="Aston Martin">Aston Martin</option>--}}
                {{--                        <option value="Audi">Audi</option>--}}
                {{--                        <option value="Austin">Austin</option>--}}
                {{--                        <option value="Autobianchi">Autobianchi</option>--}}
                {{--                        <option value="Beijing">Beijing</option>--}}
                {{--                        <option value="Bentley">Bentley</option>--}}
                {{--                        <option value="Bertone">Bertone</option>--}}
                {{--                        <option value="Bitter">Bitter</option>--}}
                {{--                        <option value="BMW">BMW</option>--}}
                {{--                        <option value="Bristol">Bristol</option>--}}
                {{--                        <option value="Bugatti">Bugatti</option>--}}
                {{--                        <option value="Buick">Buick</option>--}}
                {{--                        <option value="BYD">BYD</option>--}}
                {{--                        <option value="Cadillac">Cadillac</option>--}}
                {{--                        <option value="Callaway">Callaway</option>--}}
                {{--                        <option value="Carbodies">Carbodies</option>--}}
                {{--                        <option value="Caterham">Caterham</option>--}}
                {{--                        <option value="Chery">Chery</option>--}}
                {{--                        <option value="Chevrolet">Chevrolet</option>--}}
                {{--                        <option value="Chrysler">Chrysler</option>--}}
                {{--                        <option value="Citroen">Citroen</option>--}}
                {{--                        <option value="Cizeta">Cizeta</option>--}}
                {{--                        <option value="Coggiola">Coggiola</option>--}}
                {{--                        <option value="Dacia">Dacia</option>--}}
                {{--                        <option value="Dadi">Dadi</option>--}}
                {{--                        <option value="Daewoo">Daewoo</option>--}}
                {{--                        <option value="DAF">DAF</option>--}}
                {{--                        <option value="Daihatsu">Daihatsu</option>--}}
                {{--                        <option value="Daimler">Daimler</option>--}}
                {{--                        <option value="Datsun">Datsun</option>--}}
                {{--                        <option value="DeLorean">DeLorean</option>--}}
                {{--                        <option value="De Tomaso">De Tomaso</option>--}}
                {{--                        <option value="Derways">Derways</option>--}}
                {{--                        <option value="Dodge">Dodge</option>--}}
                {{--                        <option value="Doninvest">Doninvest</option>--}}
                {{--                        <option value="Donkervoort">Donkervoort</option>--}}
                {{--                        <option value="Eagle">Eagle</option>--}}
                {{--                        <option value="FAW">FAW</option>--}}
                {{--                        <option value="Ferrari">Ferrari</option>--}}
                {{--                        <option value="Fiat">Fiat</option>--}}
                {{--                        <option value="Ford">Ford</option>--}}
                {{--                        <option value="FSO">FSO</option>--}}
                {{--                        <option value="Geely">Geely</option>--}}
                {{--                        <option value="Geo">Geo</option>--}}
                {{--                        <option value="GMC">GMC</option>--}}
                {{--                        <option value="Great Wall">Great Wall</option>--}}
                {{--                        <option value="Hindustan">Hindustan</option>--}}
                {{--                        <option value="Holden">Holden</option>--}}
                {{--                        <option value="Honda">Honda</option>--}}
                {{--                        <option value="Hummer">Hummer</option>--}}
                {{--                        <option value="Hyundai">Hyundai</option>--}}
                {{--                        <option value="Iran Khodro">Iran Khodro</option>--}}
                {{--                        <option value="Infiniti">Infiniti</option>--}}
                {{--                        <option value="Innocenti">Innocenti</option>--}}
                {{--                        <option value="Invicta">Invicta</option>--}}
                {{--                        <option value="Isdera">Isdera</option>--}}
                {{--                        <option value="Isuzu">Isuzu</option>--}}
                {{--                        <option value="IVECO">IVECO</option>--}}
                {{--                        <option value="JAC">JAC</option>--}}
                {{--                        <option value="Jaguar">Jaguar</option>--}}
                {{--                        <option value="Jeep">Jeep</option>--}}
                {{--                        <option value="Jensen">Jensen</option>--}}
                {{--                        <option value="Kia">Kia</option>--}}
                {{--                        <option value="Koenigsegg">Koenigsegg</option>--}}
                {{--                        <option value="Lamborghini">Lamborghini</option>--}}
                {{--                        <option value="Lancia">Lancia</option>--}}
                {{--                        <option value="Land Rover">Land Rover</option>--}}
                {{--                        <option value="Landwind">Landwind</option>--}}
                {{--                        <option value="Lexus">Lexus</option>--}}
                {{--                        <option value="Lincoln">Lincoln</option>--}}
                {{--                        <option value="Lotus">Lotus</option>--}}
                {{--                        <option value="LTI">LTI</option>--}}
                {{--                        <option value="Mahindra">Mahindra</option>--}}
                {{--                        <option value="Marcos">Marcos</option>--}}
                {{--                        <option value="Maruti">Maruti</option>--}}
                {{--                        <option value="Maserati">Maserati</option>--}}
                {{--                        <option value="Maybach">Maybach</option>--}}
                {{--                        <option value="Mazda">Mazda</option>--}}
                {{--                        <option value="McLaren">McLaren</option>--}}
                {{--                        <option value="Mega">Mega</option>--}}
                {{--                        <option value="Mercedes-Benz">Mercedes-Benz</option>--}}
                {{--                        <option value="Mercury">Mercury</option>--}}
                {{--                        <option value="Metrocab">Metrocab</option>--}}
                {{--                        <option value="MG">MG</option>--}}
                {{--                        <option value="Microcar">Microcar</option>--}}
                {{--                        <option value="Minelli">Minelli</option>--}}
                {{--                        <option value="MINI">MINI</option>--}}
                {{--                        <option value="Mitsubishi">Mitsubishi</option>--}}
                {{--                        <option value="Mitsuoka">Mitsuoka</option>--}}
                {{--                        <option value="Morgan">Morgan</option>--}}
                {{--                        <option value="Morris">Morris</option>--}}
                {{--                        <option value="Nissan">Nissan</option>--}}
                {{--                        <option value="Noble">Noble</option>--}}
                {{--                        <option value="Oldsmobile">Oldsmobile</option>--}}
                {{--                        <option value="Opel">Opel</option>--}}
                {{--                        <option value="Osca">Osca</option>--}}
                {{--                        <option value="Pagani">Pagani</option>--}}
                {{--                        <option value="Panoz">Panoz</option>--}}
                {{--                        <option value="Perodua">Perodua</option>--}}
                {{--                        <option value="Peugeot">Peugeot</option>--}}
                {{--                        <option value="Plymouth">Plymouth</option>--}}
                {{--                        <option value="Pontiac">Pontiac</option>--}}
                {{--                        <option value="Porsche">Porsche</option>--}}
                {{--                        <option value="Premier">Premier</option>--}}
                {{--                        <option value="Proton">Proton</option>--}}
                {{--                        <option value="PUCH">PUCH</option>--}}
                {{--                        <option value="Puma">Puma</option>--}}
                {{--                        <option value="Qvale">Qvale</option>--}}
                {{--                        <option value="Reliant">Reliant</option>--}}
                {{--                        <option value="Renault">Renault</option>--}}
                {{--                        <option value="Rolls-Royce">Rolls-Royce</option>--}}
                {{--                        <option value="Ronart">Ronart</option>--}}
                {{--                        <option value="Rover">Rover</option>--}}
                {{--                        <option value="Saab">Saab</option>--}}
                {{--                        <option value="Saleen">Saleen</option>--}}
                {{--                        <option value="Renault Samsung">Renault Samsung</option>--}}
                {{--                        <option value="Saturn">Saturn</option>--}}
                {{--                        <option value="Scion">Scion</option>--}}
                {{--                        <option value="SEAT">SEAT</option>--}}
                {{--                        <option value="Skoda">Skoda</option>--}}
                {{--                        <option value="Smart">Smart</option>--}}
                {{--                        <option value="Spectre">Spectre</option>--}}
                {{--                        <option value="Spyker">Spyker</option>--}}
                {{--                        <option value="SsangYong">SsangYong</option>--}}
                {{--                        <option value="Subaru">Subaru</option>--}}
                {{--                        <option value="Suzuki">Suzuki</option>--}}
                {{--                        <option value="Talbot">Talbot</option>--}}
                {{--                        <option value="TATA">TATA</option>--}}
                {{--                        <option value="Tatra">Tatra</option>--}}
                {{--                        <option value="Tianma">Tianma</option>--}}
                {{--                        <option value="Tianye">Tianye</option>--}}
                {{--                        <option value="Tofas">Tofas</option>--}}
                {{--                        <option value="Toyota">Toyota</option>--}}
                {{--                        <option value="Trabant">Trabant</option>--}}
                {{--                        <option value="Triumph">Triumph</option>--}}
                {{--                        <option value="TVR">TVR</option>--}}
                {{--                        <option value="Vauxhall">Vauxhall</option>--}}
                {{--                        <option value="Vector">Vector</option>--}}
                {{--                        <option value="Venturi">Venturi</option>--}}
                {{--                        <option value="Volkswagen">Volkswagen</option>--}}
                {{--                        <option value="Volvo">Volvo</option>--}}
                {{--                        <option value="Wartburg">Wartburg</option>--}}
                {{--                        <option value="Westfield">Westfield</option>--}}
                {{--                        <option value="Wiesmann">Wiesmann</option>--}}
                {{--                        <option value="Xin Kai">Xin Kai</option>--}}
                {{--                        <option value="Zastava">Zastava</option>--}}
                {{--                        <option value="VAZ (Lada)">VAZ (Lada)</option>--}}
                {{--                        <option value="GAZ">GAZ</option>--}}
                {{--                        <option value="ZAZ">ZAZ</option>--}}
                {{--                        <option value="ZIL">ZIL</option>--}}
                {{--                        <option value="Izh">Izh</option>--}}
                {{--                        <option value="LUAZ">LUAZ</option>--}}
                {{--                        <option value="Moskvich">Moskvich</option>--}}
                {{--                        <option value="UAZ">UAZ</option>--}}
                {{--                        <option value="KTM">KTM</option>--}}
                {{--                        <option value="Marlin">Marlin</option>--}}
                {{--                        <option value="PGO">PGO</option>--}}
                {{--                        <option value="Piaggio">Piaggio</option>--}}
                {{--                        <option value="Vortex">Vortex</option>--}}
                {{--                        <option value="Hafei">Hafei</option>--}}
                {{--                        <option value="Foton">Foton</option>--}}
                {{--                        <option value="DongFeng">DongFeng</option>--}}
                {{--                        <option value="Lifan">Lifan</option>--}}
                {{--                        <option value="ChangFeng">ChangFeng</option>--}}
                {{--                        <option value="ShuangHuan">ShuangHuan</option>--}}
                {{--                        <option value="Brilliance">Brilliance</option>--}}
                {{--                        <option value="Soueast">Soueast</option>--}}
                {{--                        <option value="ZX">ZX</option>--}}
                {{--                        <option value="Fuqi">Fuqi</option>--}}
                {{--                        <option value="HuangHai">HuangHai</option>--}}
                {{--                        <option value="SMZ">SMZ</option>--}}
                {{--                        <option value="TagAZ">TagAZ</option>--}}
                {{--                        <option value="Gonow">Gonow</option>--}}
                {{--                        <option value="Baltijas Dzips">Baltijas Dzips</option>--}}
                {{--                        <option value="JMC">JMC</option>--}}
                {{--                        <option value="Bufori">Bufori</option>--}}
                {{--                        <option value="Changan">Changan</option>--}}
                {{--                        <option value="Liebao Motor">Liebao Motor</option>--}}
                {{--                        <option value="Tesla">Tesla</option>--}}
                {{--                        <option value="Marussia">Marussia</option>--}}
                {{--                        <option value="Haima">Haima</option>--}}
                {{--                        <option value="Tazzari">Tazzari</option>--}}
                {{--                        <option value="Ariel">Ariel</option>--}}
                {{--                        <option value="Santana">Santana</option>--}}
                {{--                        <option value="Zotye">Zotye</option>--}}
                {{--                        <option value="Luxgen">Luxgen</option>--}}
                {{--                        <option value="AM General">AM General</option>--}}
                {{--                        <option value="Fisker">Fisker</option>--}}
                {{--                        <option value="Ecomotors">Ecomotors</option>--}}
                {{--                        <option value="E-Car">E-Car</option>--}}
                {{--                        <option value="Byvin">Byvin</option>--}}
                {{--                        <option value="Eagle Cars">Eagle Cars</option>--}}
                {{--                        <option value="Kanonir">Kanonir</option>--}}
                {{--                        <option value="Yo-mobile">Yo-mobile</option>--}}
                {{--                        <option value="Bronto">Bronto</option>--}}
                {{--                        <option value="Tramontana">Tramontana</option>--}}
                {{--                        <option value="Avtokam">Avtokam</option>--}}
                {{--                        <option value="Qoros">Qoros</option>--}}
                {{--                        <option value="Brabus">Brabus</option>--}}
                {{--                        <option value="Renaissance">Renaissance</option>--}}
                {{--                        <option value="Hawtai">Hawtai</option>--}}
                {{--                        <option value="Ultima">Ultima</option>--}}
                {{--                        <option value="Gordon">Gordon</option>--}}
                {{--                        <option value="Haval">Haval</option>--}}
                {{--                        <option value="DS">DS</option>--}}
                {{--                        <option value="Shanghai Maple">Shanghai Maple</option>--}}
                {{--                        <option value="Zenvo">Zenvo</option>--}}
                {{--                        <option value="W Motors">W Motors</option>--}}
                {{--                        <option value="Rezvani">Rezvani</option>--}}
                {{--                        <option value="Rimac">Rimac</option>--}}
                {{--                        <option value="Adler">Adler</option>--}}
                {{--                        <option value="Willys">Willys</option>--}}
                {{--                        <option value="DeSoto">DeSoto</option>--}}
                {{--                        <option value="Packard">Packard</option>--}}
                {{--                        <option value="Kombat">Kombat</option>--}}
                {{--                        <option value="Borgward">Borgward</option>--}}
                {{--                        <option value="Ravon">Ravon</option>--}}
                {{--                        <option value="ZiS">ZiS</option>--}}
                {{--                        <option value="AMC">AMC</option>--}}
                {{--                        <option value="Zenos">Zenos</option>--}}
                {{--                        <option value="Hudson">Hudson</option>--}}
                {{--                        <option value="Batmobile">Batmobile</option>--}}
                {{--                        <option value="Excalibur">Excalibur</option>--}}
                {{--                        <option value="Genesis">Genesis</option>--}}
                {{--                        <option value="Wanderer">Wanderer</option>--}}
                {{--                        <option value="Zibar">Zibar</option>--}}
                {{--                        <option value="Delage">Delage</option>--}}
                {{--                        <option value="Steyr">Steyr</option>--}}
                {{--                        <option value="Hanomag">Hanomag</option>--}}
                {{--                        <option value="Bilenkin">Bilenkin</option>--}}
                {{--                        <option value="Think">Think</option>--}}
                {{--                        <option value="Bajaj">Bajaj</option>--}}
                {{--                        <option value="Hispano-Suiza">Hispano-Suiza</option>--}}
                {{--                        <option value="Jinbei">Jinbei</option>--}}
                {{--                        <option value="GP">GP</option>--}}
                {{--                        <option value="Horch">Horch</option>--}}
                {{--                        <option value="Lucid">Lucid</option>--}}
                {{--                        <option value="DW Hower">DW Hower</option>--}}
                {{--                        <option value="Bike">Bike</option>--}}
                {{--                    </select>--}}
                {{--                    <div class="clearfix"></div>--}}
                {{--                    <br>--}}
                {{--                </div>--}}
                {{--                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">--}}
                {{--                    <label for="car_type" class="rtl">نوع السيارة <i class="red-text">*</i></label>--}}
                {{--                    <select name="car_type_id" id="car_type_id" class="rtl validate pure-input-1 car_type_id" required>--}}
                {{--                        <option value="">اختر نوع السيارة</option>--}}
                {{--                        @foreach($car_types as $car_type)--}}
                {{--                            <option value="{{$car_type->id}}">{{$car_type->name}}</option>--}}
                {{--                        @endforeach--}}
                {{--                    </select>--}}
                {{--                    <div class="clearfix"></div>--}}
                {{--                    <br>--}}
                {{--                </div>--}}

                {{--                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12" id="car_model" hidden>--}}
                {{--                    <label for="car_model" class="rtl"> اختر موديل السيارة <i class="red-text">*</i></label>--}}
                {{--                    <select name="car_model_id" id="car_model_id" class="rtl pure-input-1" required>--}}
                {{--                        <option disabled selected> اختر موديل السيارة </option>--}}
                {{--                        <option value="AC">AC</option>--}}
                {{--                    </select>--}}

                {{--                    <div class="clearfix"></div>--}}
                {{--                    <br>--}}
                {{--                </div>--}}

                {{--                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">--}}
                {{--                    <label for="creation_year" class="rtl">سنة الصنع <i class="red-text">*</i></label>--}}
                {{--                    <select name="creation_year" id="creation_year" class="rtl validate pure-input-1" required>--}}
                {{--                        <option value="1995">1995</option>--}}
                {{--                        <option value="1996">1996</option>--}}
                {{--                        <option value="1997">1997</option>--}}
                {{--                        <option value="1998">1998</option>--}}
                {{--                        <option value="1999">1999</option>--}}
                {{--                        <option value="2000">2000</option>--}}
                {{--                        <option value="2001">2001</option>--}}
                {{--                        <option value="2002">2002</option>--}}
                {{--                        <option value="2003">2003</option>--}}
                {{--                        <option value="2004">2004</option>--}}
                {{--                        <option value="2005">2005</option>--}}
                {{--                        <option value="2006">2006</option>--}}
                {{--                        <option value="2007">2007</option>--}}
                {{--                        <option value="2008">2008</option>--}}
                {{--                        <option value="2009">2009</option>--}}
                {{--                        <option value="2010">2010</option>--}}
                {{--                        <option value="2011">2011</option>--}}
                {{--                        <option value="2012">2012</option>--}}
                {{--                        <option value="2013">2013</option>--}}
                {{--                        <option value="2014">2014</option>--}}
                {{--                        <option value="2015">2015</option>--}}
                {{--                        <option value="2016">2016</option>--}}
                {{--                        <option value="2017">2017</option>--}}
                {{--                        <option value="2018">2018</option>--}}
                {{--                        <option value="2019">2019</option>--}}
                {{--                    </select>--}}
                {{--                    <div class="clearfix"></div>--}}
                {{--                    <br>--}}
                {{--                </div>--}}

                {{--                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">--}}
                {{--                    <label for="account_number" class="rtl">(اختياري) رقم الحساب البنكي الآيبان والمكون من 24 خانة (حاليا ندعم البنوك التالية: الأهلي)</label>--}}
                {{--                    <input id="account_number" type="text" name="account_number" class="rtl pure-input-1">--}}
                {{--                    <div class="clearfix"></div>--}}
                {{--                    <br>--}}
                {{--                </div>--}}

                {{--                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">--}}
                {{--                    <label for="address" class="rtl">عنوان صاحب الحساب - الرجاء كتابة العنوان كاملا</label>--}}
                {{--                    <input id="address" type="text" name="address" class="pure-input-1 rtl">--}}
                {{--                    <div class="clearfix"></div>--}}
                {{--                    <br>--}}
                {{--                </div>--}}


                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                    <br>
                    <h3 class="rtl">ماهي شروط العمل في لمه؟</h3>
                </div>

                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                    <p class="rtl">
                        🚩 أن يكون المندوب حسن السيرة والسلوك وغير محكوم عليه بالإدانة في جريمة مخلة بالشرف والأمانة
                        مالم يكن قد رد اعتباره إليه.<br>
                        🚩 وضع الاسم الثنائي مع الصورة الشخصية الخاصة بالمندوب.<br>
                        🚩 يتوجب على المندوب تشغيل الجوال المسجل في لمه حال قبول الطلب. <br>
                        🚩 عدم التدخين في السيارة أثناء التوصيل لتجنب إفساد الطلب، وفي حال فسد الطلب بسبب خطأ من المندوب
                        فإنه لن يعوض عن قيمة التوصيل، ولايحق له المطالبة بأي تعويض.<br>
                        🚩 يمنع الطلب من العميل التواصل من خلال "واتساب" أو وسائل الاتصال الأخرى، والاكتفاء بتطبيق لمه
                        والذي من خلاله يمكنك طلب موقع العميل والتواصل معه بشكل سريع وآمن.<br>
                        🚩 يجب على المندوب التعريف بنفسه عند الاتصال ومراسلة العميل بالطريقة التالية: السلام عليكم، معك
                        (فلان) مندوب تطبيق لمه، ثم تزويده بتفاصيل الطلب.<br>
                        🚩 يتم احتساب عمولة بمقدار 20% من رسوم التوصيل عن كل عملية توصيل.
                    </p>
                </div>

                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                    <br>
                    <h3 class="rtl">ماهي الوثائق المطلوبة؟</h3>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">&nbsp;</div>
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">&nbsp;</div>
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">
                        <div class="thumbnail">
                            <div id="preview_cr1">
                                <div id="default_cr1">
                                    <img height="150" class="full-width"
                                         src="{{asset('assets/landing')}}/default-thumbnail.jpg">
                                </div>
                            </div>
                        </div>

                        <label>صورة من بطاقة الهوية الوطنية / أو هوية مقيم. <i class="red-text">*</i></label>
                        <input id="file_id" name="id_image" type="file" data-maxwidth="720" data-maxheight="720"
                               accept="image/*">
                        <br>
                    </div>

                    <div class="rtl col-md-12 col-sm-12 col-xs-12">
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">&nbsp;</div>
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">&nbsp;</div>
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">
                        <div class="thumbnail">
                            <div id="preview_cr2" class="rtl">
                                <div id="default_cr2">
                                    <img height="150" class="full-width"
                                         src="{{asset('assets/landing')}}/default-thumbnail.jpg">
                                </div>
                            </div>
                        </div>

                        <label>صورة السيارة <i class="red-text">*</i></label>
                        <input id="car_image" name="car_image" type="file" accept="image/*" data-maxwidth="720"
                               data-maxheight="720">
                        <br>
                    </div>


                </div>


                <div class="rtl col-md-12 col-sm-12 col-xs-12">
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">&nbsp;</div>
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">&nbsp;</div>
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">
                        <div class="thumbnail">
                            <div id="preview_cr3" class="rtl">
                                <div id="default_cr3">
                                    <img height="150" class="full-width"
                                         src="{{asset('assets/landing')}}/default-thumbnail.jpg">
                                </div>
                            </div>
                        </div>

                        <label>رخصة القيادة للسائق  <i class="red-text">*</i></label>
                        <input id="driving_license" name="driving_license" type="file" accept="image/*"
                               data-maxwidth="720" data-maxheight="720">
                        <br>
                    </div>
                </div>
                <div class="rtl col-md-12 col-sm-12 col-xs-12">
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">&nbsp;</div>
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">&nbsp;</div>
                    <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-3 col-xs-12">
                        <div class="thumbnail">
                            <div id="preview_cr4">
                                <div id="default_cr4">
                                    <img height="150" class="full-width"
                                         src="{{asset('assets/landing')}}/default-thumbnail.jpg">
                                </div>
                            </div>
                        </div>

                        <label>استمارة للسيارة <i class="red-text">*</i></label>
                        <input id="car_contract" name="car_contract" type="file" accept="image/*" data-maxwidth="720"
                               data-maxheight="720">
                        <br>
                    </div>
                </div>


                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12"
                     style="color:red;font-size: 120%;">
                    <br>
                    <b class="rtl">ملاحظة مهمة</b><br>
                    يتوجب عليك قبل التوثيق التحقق من ملف البروفايل الخاص بك من خلال خانة “انا” في التطبيق: <br>
                    1- أن تكون الصورة لك واضحة بدون نظارات شمسية وتظهر كامل الوجة . <br>
                    2- الاسم الثنائي لك كما هو ظاهر بالهوية الوطنية . <br>
                    مع العلم أنه لن يتم النظر لأي طلب لا يحقق الشروط المذكورة أعلاه .
                    <br>
                </div>

                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                    <br>
                    <h4 class="rtl">إقرار وموافقة</h4>
                </div>

                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12 custom">
                    <div class="checkbox">
                        <input id="agreement" type="checkbox" name="agreementchk" class="validate" required=""
                               aria-required="true">
                        <label for="agreement">أقر بأنني قد قرأت الشروط المذكورة ووافقت عليها، اً.</label>
                    </div>
                </div>

                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-8 col-sm-12 col-xs-12">
                    <br>
                    <button class="submit-btn" id="subBtn_signup1" type="submit"> أرسل الطلب</button>
                    <span class="input-field pure-u-1 pure-u-md-1 pure-u-lg-1 rtl" style="margin-top: 5px;"
                          id="register_error_signup"></span>
                    <br><br>
                </div>
            </form>

            <div id="success_signup" class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-12 col-xs-12" style="display:none">
                <h3></h3>
                <div class="clearfix"></div>
                <br>
            </div>

        </div>
        <div class="signup_screen4 pure-form pure-form-stacked pure-g hide_tab">
            <div id="success_process_complete" class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-sm-12 col-xs-12"
                 style="display:none">
                <h3 class="rtl"></h3>
                <div class="clearfix"></div>
                <br>
            </div>
        </div>
        <div id="endOfPage"></div>
    </div>
</div>

<div class="modal fade" id="crop_image_model" tabindex="-1" role="dialog" aria-labelledby="image_model_label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close change_image_cr5" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">إلغاء</span>
                </button>
                <h4 class="modal-title">قم برفع صورتك الشخصية</h4>
            </div>

            <div class="modal-body" id="modelUploadImg">
                <div class="upload-demo upload-democr5">
                    <div class="row">
                        <div class="col-sm-12 actions hidden">
                            <button class="btn btn-default vanilla-rotate rotate_image" data-deg="-90">قلب الصورة
                            </button>
                            <button type="button" class="btn btn-primary set_image upload-resultcr5"
                                    data-dismiss="modal">موافق
                            </button>
                            <button type="button" class="btn btn-default change_image_cr5">إلغاء</button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12 select_file">
                            <div id="selectFile">
                                <div class="upload-msg">
                                    قم برفع الصورة هنا
                                    <input id="file_profile_piccr5" type="file" accept="image/*" data-maxwidth="720"
                                           data-maxheight="720" class="file_options">
                                </div>
                                <div class="upload-demo-wrap">
                                    <div id="upload-democr5" class="croppie-container">
                                        <div class="cr-boundary" aria-dropeffect="none">
                                            <canvas class="cr-image" alt="preview" aria-grabbed="false"></canvas>
                                            <div class="cr-viewport cr-vp-square" tabindex="0"
                                                 style="width: 250px; height: 250px;"></div>
                                            <div class="cr-overlay"></div>
                                        </div>
                                        <div class="cr-slider-wrap"><input class="cr-slider" type="range" step="0.0001"
                                                                           aria-label="zoom"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.crop-img-modal-cr5').on("click", function () {
        $("#crop_image_model").modal("show");
    })
</script>

<!-- The Modal -->
<div class="modal" id="signupWarningModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" style="float: right;">!تنبيه مهم</h4>
                <button type="button" class="close" data-dismiss="modal" style="float: left;">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <p id="warning_msg">
                <p>لمهنا العزيز, نجو منك التأكد من قراءة شروط ومتطلبات التوثيق, في حالة تم رفض طلبك <b
                        style="color:red;"> لن تتمكن من رفع طلب جديد الا بعد مرور 4 ساعات</b></p>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="cancel-btn" data-dismiss="modal">تراجع, اريد التأكد من معلومات الطلب
                </button>
                <button type="button" class="ok-btn" data-dismiss="modal" id="subBtn_signup2">اوافق على الشروط
                    والأحكام
                </button>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .ok-btn {
        padding: 6px 12px;
        font-size: 16px;
        font-weight: 600;
        cursor: pointer;
        background-color: #04d1d6 !important;
        color: white;
        border: 1px solid #09c6cc;
        border-radius: 5px;
    }

    .cancel-btn {
        padding: 6px 12px;
        font-size: 16px;
        font-weight: 600;
        cursor: pointer;
        background-color: red !important;
        color: white;
        border: 1px solid red;
        border-radius: 5px;
    }
</style>
<script>
    $('.car_type_id').change(function () {
        $('#car_model').attr('hidden', false);
        var car_type_id = $(this).val();
        if (car_type_id) {
            $.ajax({
                type: "get",
                url: "{{url('signup/form/get-car-models')}}/" + car_type_id,
                success: function (response) {
                    $('#car_model_id').empty();
                    $("#car_model_id").append('<option disabled selected>--اختر موديل السيارة--</option>');
                    if (response) {
                        $.each(response, function (key, value) {
                            $('#car_model_id').append($("<option/>", {
                                value: value.id,
                                text: value.name
                            }));
                        });
                    }
                }
            });
        }
    });
</script>

<link rel="stylesheet" href="{{asset('assets/landing')}}/forms/css/cropimg.css">
<link rel="stylesheet" href="{{asset('assets/landing')}}/forms/css/croppie.css">
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
<script src='https://foliotek.github.io/Croppie/croppie.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>
<script src="{{asset('assets/landing')}}/forms/js/script.js"></script>
<script src="{{asset('assets/landing')}}/forms/js/exif.js"></script>
<script src="{{asset('assets/landing')}}/forms/js/cropimg.js"></script>
<script src="{{asset('assets/landing')}}/forms/js/croppie.js"></script>
<script src="{{asset('assets/landing')}}/forms/js/multi_file_processor.js"></script>
<script src="{{asset('assets/landing')}}/forms/js/bootstrap.min.js"></script>
<script src="{{ url('/assets/js/toastr.min.js') }}"></script>

<!-- Plugin used-->
<script type="text/javascript">
    toastr.options = {
        "progressBar": true,
        "positionClass": "toast-bottom-right",
    };
    toastr.options.timeOut = 3000;
    toastr.options.fadeOut = 3000;
</script>
<script type="text/javascript">
    @if(session()->has('success'))
    toastr.success("{{session()->get('success')}}");
    @elseif(session()->has('error'))
    toastr.error("{{session()->get('error')}}");
    @endif
</script>


</body>

</html>
