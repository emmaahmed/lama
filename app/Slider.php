<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Modules\Shop\Entities\Shop;

class Slider extends Model
{
    protected $fillable = ['shop_id','image'];

    public function shop()
    {
        return $this->belongsTo(Shop::class,'shop_id');
    }

    public function getImageAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/sliders').'/'.$image;
        }
        return "";
    }

    public function setImageAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'sliders');
            $this->attributes['image'] = $imageFields;
        }
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }
}
