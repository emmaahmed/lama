<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use mysql_xdevapi\Schema;
use Neodynamic\SDK\Web\WebClientPrint;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //date_default_timezone_set('Africa/Cairo');
        date_default_timezone_set('Asia/Riyadh');
        \Illuminate\Support\Facades\Schema::defaultStringLength('191');
        app()->setLocale('ar');
        \Carbon\Carbon::setLocale('ar');
        request()->headers->set('lang', 'ar');
//        if (!request()->headers->has('lang')){
//            return callback_data(error() , "check_lang");
//        }
    }
}
