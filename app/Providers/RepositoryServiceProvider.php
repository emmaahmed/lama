<?php

namespace App\Providers;

use App\Repositories\AuthRepository;
use App\Repositories\CityRepository;
use App\Repositories\ContactRepository;
use App\Repositories\HomeRepository;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use App\Repositories\Interfaces\CityRepositoryInterface;
use App\Repositories\Interfaces\ContactRepositoryInterface;
use App\Repositories\Interfaces\HomeRepositoryInterface;
use App\Repositories\Interfaces\NotificationsRepositoryInterface;
use App\Repositories\Interfaces\SlidersRepositoryInterface;
use App\Repositories\Interfaces\SocialMediaRepositoryInterface;
use App\Repositories\Interfaces\TutorialRepositoryInterface;
use App\Repositories\Interfaces\UserAddressRepositoryInterface;
use App\Repositories\NotificationsRepository;
use App\Repositories\SlidersRepository;
use App\Repositories\SocialMediaRepository;
use App\Repositories\TutorialRepository;
use App\Repositories\UserAddressRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(AuthRepositoryInterface::class,AuthRepository::class);
        $this->app->bind(TutorialRepositoryInterface::class,TutorialRepository::class);
        $this->app->bind(CityRepositoryInterface::class,CityRepository::class);
        $this->app->bind(HomeRepositoryInterface::class,HomeRepository::class);
        $this->app->bind(SocialMediaRepositoryInterface::class,SocialMediaRepository::class);
        $this->app->bind(ContactRepositoryInterface::class,ContactRepository::class);
        $this->app->bind(UserAddressRepositoryInterface::class,UserAddressRepository::class);
        $this->app->bind(NotificationsRepositoryInterface::class,NotificationsRepository::class);
        $this->app->bind(SlidersRepositoryInterface::class,SlidersRepository::class);
    }
}
