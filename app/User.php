<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Modules\Cart\Entities\Cart;
use Modules\Order\Entities\BlockedOrder;
use Modules\Order\Entities\Order;
use Modules\Product\Entities\FavouriteProduct;
use Modules\Product\Entities\Product;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type','name', 'email', 'phone', 'image', 'jwt', 'password',
        'firebase_token', 'social_id', 'latitude', 'longitude',
        'location', 'status','expire_at','password_wrong_times','platform'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','email_verified_at','created_at','updated_at','expire_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }
    public function setImageAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'users');
            $this->attributes['image'] = $imageFields;
        }
    }
    public function getImageAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/users').'/'.$image;
        }
        return "";
    }

//    public function notifications()
//    {
//        return $this->hasMany(Notification::class,'user_id');
//    }

    public function favourite_products()
    {
        return $this->hasManyThrough(Product::class, FavouriteProduct::class, 'user_id', 'id', 'product_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function user_orders()
    {
        return $this->hasMany(Order::class,'user_id');
    }
    public function delegate_orders()
    {
        return $this->hasMany(Order::class,'delegate_id');
    }

    public function blocked_orders()
    {
        return $this->hasMany(BlockedOrder::class,'delegate_id');
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }

    public function cart()
    {
        return $this->hasOne(Cart::class);
    }

}
