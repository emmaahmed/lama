<?php

namespace App\Http\Controllers\Api\User;

use App\Repositories\Interfaces\NotificationsRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    protected $notificationsRepository;

    public function __construct(NotificationsRepositoryInterface $notificationsRepository)
    {
        $this->notificationsRepository = $notificationsRepository;
    }

    public function getNotifications()
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT() ? checkJWT() : checkDelegateJWT();
        if ($user){
            $notifications = $this->notificationsRepository->getNotifications($user);
            if ($notifications){
                return callback_data(success(), 'notifications', $notifications);
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }
}
