<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Requests\UserAddressRequest;
use App\Repositories\Interfaces\UserAddressRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserAddressController extends Controller
{
    protected $addressRepository;

    public function __construct(UserAddressRepositoryInterface $addressRepository)
    {
        $this->addressRepository = $addressRepository;
    }

    public function getAddresses()
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $addresses = $this->addressRepository->getUserAddresses($user->id);
            return callback_data(success(),'user_addresses',$addresses);
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function store(UserAddressRequest $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $address = $this->addressRepository->store($request,$user->id);
            if ($address){
                if ($request->has('id')){
                    return callback_data(success(), 'address_updated');
                }
                return callback_data(success(), 'address_created');
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function delete(UserAddressRequest $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $address = $this->addressRepository->delete($request->address_id,$user->id);
            if ($address){
                return callback_data(success(), 'address_deleted');
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }
}
