<?php

namespace App\Http\Controllers\Api\User;

use App\Repositories\HomeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    protected $homeRepository;

    public function __construct(HomeRepository $homeRepository)
    {
        $this->homeRepository = $homeRepository;
    }

    public function index()
    {
        $data = $this->homeRepository->index();
        return callback_data( success(),'home', $data);
    }
    public function allOffers()
    {
        $data = $this->homeRepository->allOffers();
        return callback_data( success(),'offers', $data)
            ;    }

}
