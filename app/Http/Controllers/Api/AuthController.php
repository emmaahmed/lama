<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\AuthRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\SendCodeRequest;
use App\Http\Requests\VerificationRequest;
use App\Mail\ActivationMail;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use App\User;
use App\Verification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;
    }
    public function Register(AuthRequest $request)
    {
        dd($request->validated());
        $user = $this->authRepository->Register($request);
        if ($user){
            $this->authRepository->sendCode($user->phone);
            return callback_data(success(),'registered');
        }

        else{
            return callback_data(error(),'invalid_data');
        }
    }

    public function Verify(AuthRequest $request)
    {
        return $this->authRepository->verifyCode($request);
    }
    public function sendCode(AuthRequest $request)
    {
        $key = $request->key;
        if (is_numeric($key)) {     // $key => Phone
            // Check Phone Exists
            $phones = User::pluck('phone')->toArray();
            if (!in_array($key, $phones)) {
                return callback_data(error(),'phone_not_found');
            }
        }else{                       // $key => Email
            // Check Email Exists
            $emails = User::pluck('email')->toArray();
            if (!in_array($key,$emails)){
                return callback_data(error(),'email_not_found');
            }
        }
        $this->authRepository->sendCode($key);
        return callback_data(success(),'code_sent');
    }

    public function Login(AuthRequest $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = User::where('email',$request->email)->first();

            if(!Hash::check($request->password,$user->password)){
                $user->update([
                    'password_wrong_times'=>$user->password_wrong_times+1
                ]);

                if($user->password_wrong_times==2){
                    return callback_data(error(),'account_will_be_suspended');

                }
                if($user->password_wrong_times>2){
                    $user->update([
                        'status'=>2
                    ]);

                    return callback_data(error(),'account_suspended');

                }
            }
            if ($user->status != 1){
                $this->authRepository->sendCode($user->phone);
                return callback_data(not_active(),'account_not_activated');
            }
            $this->authRepository->Login($user,$request);
            return callback_data(success(),'logged_in',$user);
        }
        else {
            return callback_data(error(),'email_or_password_not_correct');
        }
    }

    public function changePassword(AuthRequest $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $this->authRepository->changePassword($user,$request);
            return callback_data(success(),'password_changes',$user);
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function updateProfile(AuthRequest $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $updated = $this->authRepository->updateProfile($user,$request);
            if ($updated){
                return callback_data(success(),'profile_updated',$user);
            }
            return callback_data(error(),'errors');

        }else{
            return callback_data(error(),'user_not_found');
        }
    }

}
