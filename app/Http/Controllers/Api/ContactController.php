<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ContactRequest;
use App\Repositories\Interfaces\ContactRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    protected $contactRepository;

    public function __construct(ContactRepositoryInterface $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    public function sendContact(ContactRequest $request)
    {
        $contact = $this->contactRepository->sendContact($request);
        if ($contact){
            return callback_data(success(), 'contact_sent');
        }
        return callback_data(error(), 'errors');

    }
}
