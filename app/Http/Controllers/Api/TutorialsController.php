<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Interfaces\TutorialRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TutorialsController extends Controller
{
    protected $tutorialrepository;

    public function __construct(TutorialRepositoryInterface $tutorialRepository)
    {
        $this->tutorialrepository = $tutorialRepository;
    }

    public function tutorials(Request $request)
    {
        $tutorials = $this->tutorialrepository->tutorials($request->type);
        return callback_data(success(),'tutorials',$tutorials);
    }
}
