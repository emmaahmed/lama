<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Interfaces\CityRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CitiesController extends Controller
{
    protected $cityRepository;

    public function __construct(CityRepositoryInterface $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    public function cities()
    {
        $cities = $this->cityRepository->getCities();
        return callback_data(success(), 'cities', $cities);
    }
}
