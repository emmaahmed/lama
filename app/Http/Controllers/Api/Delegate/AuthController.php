<?php

namespace App\Http\Controllers\Api\Delegate;

use App\User;
use App\Http\Requests\AuthRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function Login(AuthRequest $request)
    {
//        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'type' => 1])){
        $user = User::where('email', $request->key)->orWhere('phone', $request->key)->where('type',1)->first();
        if ($user && $user->status == 5) {
            return callback_data(error(), 'account_blocked');

        }
        if($user&& $request->platform){
            $user->update(['platform'=>$request->platform]);
        }

        if ($user && Hash::check($request->password, $user->password)) {
            if ($user->status == 0) {
                $this->authRepository->sendCode($user->phone);
                return callback_data(not_active(), 'account_not_activated');
            }
            $this->authRepository->Login($user, $request);
            return callback_data(success(), 'logged_in', $user);
        }
        if ($user && !Hash::check($request->password, $user->password)) {

            if ($user->password_wrong_times <= 2) {
                $user->update([
                    'password_wrong_times' => $user->password_wrong_times + 1
                ]);

                return callback_data(error(), 'account_will_be_suspended');

            }
            if ($user->password_wrong_times > 2) {
                $user->update([
                    'status' => 2
                ]);

                return callback_data(error(), 'account_blocked');

            }
        } //        }
        else {
            return callback_data(error(), 'email_or_password_not_correct');
        }
    }

    public function Verify(AuthRequest $request)
    {
        return $this->authRepository->verifyCode($request);
    }

    public function sendCode(AuthRequest $request)
    {
        $key = $request->key;
        $user = User::where('email', $key)->orWhere('phone', $key)->first();
        if ($user && $user->status == 5) {
            return callback_data(error(), 'account_blocked');

        }

        if (is_numeric($key)) {     // $key => Phone
            // Check Phone Exists
            $phones = User::pluck('phone')->toArray();
            if (!in_array($key, $phones)) {
                return callback_data(error(), 'phone_not_found');
            }
        } else {                       // $key => Email
            // Check Email Exists
            $emails = User::pluck('email')->toArray();
            if (!in_array($key, $emails)) {
                return callback_data(error(), 'email_not_found');
            }
        }
        $this->authRepository->sendCode($key);
        return callback_data(success(), 'code_sent');
    }

    public function changePassword(AuthRequest $request)
    {
        if (!request()->headers->has('jwt')) {
            return callback_data(error(), 'check_jwt');
        }
        $user = checkJWT();
        if ($user) {
            $this->authRepository->changePassword($user, $request);
            return callback_data(success(), 'password_changes', $user);
        } else {
            return callback_data(error(), 'user_not_found');
        }
    }

    public function updateProfile(AuthRequest $request)
    {
        if (!request()->headers->has('jwt')) {
            return callback_data(error(), 'check_jwt');
        }
        $user = checkJWT();
        if ($user) {
            $updated = $this->authRepository->updateProfile($user, $request);
            if ($updated) {
                return callback_data(success(), 'profile_updated', $user);
            }
            return callback_data(error(), 'errors');

        } else {
            return callback_data(error(), 'user_not_found');
        }
    }

    public function changeStatus()
    {
        if (!request()->headers->has('jwt')) {
            return callback_data(error(), 'check_jwt');
        }
        $user = checkJWT();
        if ($user) {
            $updated = $this->authRepository->changeStatus($user);
            if ($updated) {
                return callback_data(success(), 'status_updated');
            }
            return callback_data(error(), 'errors');

        } else {
            return callback_data(error(), 'user_not_found');
        }

    }
}
