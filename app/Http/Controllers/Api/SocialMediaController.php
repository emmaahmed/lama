<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Interfaces\SocialMediaRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocialMediaController extends Controller
{
    protected $socialMediaRepository;

    public function __construct(SocialMediaRepositoryInterface $socialMediaRepository)
    {
        $this->socialMediaRepository = $socialMediaRepository;
    }

    public function social()
    {
        $social = $this->socialMediaRepository->getSocial();
        return callback_data(success(), 'social', $social);
    }
}
