<?php

namespace App\Http\Controllers;

use App\DelegateRequest;
use App\ShopRequest;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function formRequest(Request $request)
    {
        if ($request->type == 0) {
            //delegate form
            $add                    = new DelegateRequest();
            $add->name              = $request->name;
            $add->email             = $request->email;
            $add->phone             = $request->phone;
            $add->address           = $request->address;
            $add->image             = $request->image;
            $add->id_image          = $request->id_image;
            $add->driving_license   = $request->driving_license;
            $add->car_contract      = $request->car_contract;
            $add->car_image         = $request->car_image;
            $add->save();
        }elseif($request->type == 1) {
            //shop form
            $add                        = new ShopRequest();
            $add->shop_name             = $request->shop_name;
            $add->image                 = $request->image;
            $add->phone                 = $request->phone;
            $add->email                 = $request->email;
            $add->commercial_register   = $request->commercial_register;
            $add->license               = $request->license;
            $add->name                  = $request->name;
            $add->id_image              = $request->id_image;
            $add->address               = $request->address;
            $add->longitude             = $request->longitude;
            $add->latitude               = $request->latitude;
            $add->inner_image           = $request->inner_image;
            $add->outer_image           = $request->outer_image;
            $add->document           = $request->document;
            $add->save();
        }
        session()->flash('success', 'تم إرسال الطلب بنجاح');
        return redirect()->back();
    }
    public function policy(){
        return view('policy');
    }
}
