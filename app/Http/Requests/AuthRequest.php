<?php

namespace App\Http\Requests;

class AuthRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->is('api/user/register')) {
            return $this->registerRules();
        }
        if($this->is('api/user/login')
            || $this->is('api/delegate/login')) {
                return $this->loginRules();
        }
        if($this->is('api/user/send-code')
            || $this->is('api/delegate/send-code')) {
                return $this->sendCodeRules();
        }
        if($this->is('api/user/verify-code')
            || $this->is('api/delegate/verify-code')) {
                return $this->verificationRules();
        }
        if($this->is('api/user/change-password')
            || $this->is('api/delegate/change-password')) {
                return $this->changePasswordRules();
        }
        if($this->is('api/user/update-profile')
            || $this->is('api/delegate/update-profile')) {
                return $this->updateProfileRules();
        }
    }

    public function messages()
    {
        if($this->is('api/user/register')) {
            return $this->registerMessages();
        }
        if($this->is('api/user/login')
            || $this->is('api/delegate/login')) {
                return $this->loginMessages();
        }
        if($this->is('api/user/send-code')
            || $this->is('api/delegate/send-code')) {
                return $this->sendCodeMessages();
        }
        if($this->is('api/user/verify-code')
            || $this->is('api/delegate/verify-code')) {
                return $this->verificationMessages();
        }
        if($this->is('api/user/change-password')
            || $this->is('api/delegate/change-password')) {
                return $this->changePasswordMessages();
        }
        if($this->is('api/user/update-profile')
            || $this->is('api/delegate/update-profile')) {
                return $this->updateProfileMessages();
        }
    }

    public function registerRules()
    {
        return [
            'name' => 'required',
//            'email' => 'required_without:phone',
//            'phone' => 'required_without:email',
            'email' => '',
            'phone' => 'required',
            'password' => 'required',
        ];
    }

    public function registerMessages()
    {
        return [
            'name.required' => 'name_required',
            'email.required' => 'email_required',
            'email.unique' => 'email_exist',
            'email.required_without' => 'email_or_phone',
            'phone.required_without' => 'email_or_phone',
            'phone.required' => 'phone_required',
            'phone.unique' => 'phone_exist',
            'password.required' => 'password_required',
        ];
    }

    public function loginRules()
    {
        return [
            'key' => 'required',
            'password' => 'required',
        ];
    }

    public function loginMessages()
    {
        return [
//            'email.required' => 'email_required',
//            'email.exists' => 'email_not_found',
            'password.required' => 'password_required'
        ];
    }

    public function sendCodeRules()
    {
        return [
            'key' => 'required',
        ];
    }

    public function sendCodeMessages()
    {
        return [
            'key.required' => 'phone_or_email_required',
        ];
    }

    public function verificationRules()
    {
        return [
            'code' => 'required|exists:verifications',
            'platform'=>'nullable'
        ];
    }

    public function verificationMessages()
    {
        return [
            'code.required' => 'code_required',
            'code.exists' => 'code_not_found'
        ];
    }

    public function changePasswordRules()
    {
        return [
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ];
    }

    public function changePasswordMessages()
    {
        return [
            'password.required' => 'password_required',
            'password.confirmed' => 'password_not_matches',
            'password_confirmation.required' => 'password_confirmation_required',
        ];
    }

    public function updateProfileRules()
    {
        $user = checkJWT();
        return [
            'name' => 'required|min:3',
            'email' => 'required_without:phone',
            'phone' => 'required_without:email',
        ];
    }

    public function updateProfileMessages()
    {
        return [
            'name.required' => 'name_required',
            'name.min' => 'name_min',
            'email.required' => 'email_required',
            'email.unique' => 'email_exist',
            'phone.required' => 'phone_required',
            'phone.unique' => 'phone_exist',
            'email.required_without' => 'email_or_phone',
            'phone.required_without' => 'email_or_phone',

        ];
    }
}
