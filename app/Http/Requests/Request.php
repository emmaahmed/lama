<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class Request extends FormRequest
{
    /**
     * @var string
     */
    protected $error = '';

    /**
     * @var string
     */

    /**
     * @return $this
     */
    public function forbiddenResponse()
    {
        if (empty($error)) {
            $this->error = trans('auth.general_error');
        }
        return $this->response($this->error);
    }

    protected function failedAuthorization()
    {

        return $this->response(['This action is unauthorized.']);
    }

    public function response(array $errors)
    {
        // Optionally, send a custom response on authorize failure
        // (default is to just redirect to initial page with errors)
        //
        // Can return a response, a view, a redirect, or whatever else

        return new JsonResponse($errors, 422);
    }

    public function respondWithError($messages_array)
    {
//        dd('bkrhak');

        foreach ($messages_array as $messages){
            $message_key = $messages[0];
        }
        return callback_data(error(), $message_key);
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
//        dd('test');
        $response = $this->respondWithError((array)$validator->errors()->toArray());

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
