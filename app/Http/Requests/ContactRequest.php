<?php

namespace App\Http\Requests;

class ContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => '',
            'email' => '',
            'message' => 'required|min:6',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'name_required',
            'email.required' => 'email_required',
            'phone.required' => 'phone_required',
            'message.required' => 'message_required',
            'message.min' => 'message_min',
        ];
    }
}
