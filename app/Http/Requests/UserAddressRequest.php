<?php

namespace App\Http\Requests;


class UserAddressRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->is('api/user/create-address')) {
            return $this->createRules();
        }
        if($this->is('api/user/remove-address')) {
            return $this->deleteRules();
        }
    }

    public function messages()
    {
        if($this->is('api/user/create-address')) {
            return $this->createMessages();
        }
        if($this->is('api/user/remove-address')) {
            return $this->deleteMessages();
        }
    }

    public function createRules()
    {
        return [
            'city_id' => 'required|exists:cities,id',
            'latitude' => 'required',
            'longitude' => 'required',
            'address' => 'required',
            'full_name' => 'required',
            'phone' => 'required',
            'building_name' => 'required',
            'building_image' => 'required',
            'building_number' => 'required',
            'apartment' => 'required',
        ];
    }

    public function createMessages()
    {
        return [
            'city_id.required' => 'city_id_required',
            'city_id.exists' => 'city_id_exists',
            'latitude.required' => 'latitude_required',
            'longitude.required' => 'longitude_required',
            'address.required' => 'address_required',
            'full_name.min' => 'full_name_min',
            'phone.required' => 'phone_required',
            'building_name.required' => 'building_name_required',
            'building_image.required' => 'building_image_required',
            'building_number.required' => 'building_number_required',
            'apartment.required' => 'apartment_required',
        ];
    }

    public function deleteRules()
    {
        return [
            'address_id' => 'required|exists:user_addresses,id',
        ];
    }

    public function deleteMessages()
    {
        return [
            'address_id.required' => 'address_required',
            'address_id.exists' => 'address_id_exists',
        ];
    }
}
