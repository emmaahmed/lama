<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class checkDatabase
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        dd(!($request->headers->has('database')));
        if(!($request->headers->has('database'))){
            config(['database.default' => 'mysql']);
            return $next($request);

        }
        else{
            // dd($request->header('database'));
            if($request->header('database')=='live'){
                //   DB::connection('mysql');
                config(['database.default' => 'mysql']);


                return $next($request);

            }
            else{
                //DB::connection('mysql1');
                config(['database.default' => 'mysql1']);

                return $next($request);

            }

        }
    }
}
