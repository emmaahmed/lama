<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Modules\Shop\Entities\Shop;

class Contact extends Model
{
    protected $fillable = ['shop_id','type','name','email','phone','message','is_replied','reply'];

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }


    public static function getTableName()
    {
        return (new self())->getTable();
    }
}
