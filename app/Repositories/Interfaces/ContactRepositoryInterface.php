<?php

namespace App\Repositories\Interfaces;

interface ContactRepositoryInterface
{
    public function sendContact($request);

    public function index();

    public function reply($request);
}
