<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/22/2020
 * Time: 2:34 PM
 */

namespace App\Repositories\Interfaces;

interface SlidersRepositoryInterface
{
    public function index();

    public function create();

    public function store($request);

    public function edit($request);

    public function update($request);
}
