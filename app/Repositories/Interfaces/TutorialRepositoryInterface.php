<?php

namespace App\Repositories\Interfaces;

interface TutorialRepositoryInterface
{
    public function tutorials($type=null);

    public function store($request);

    public function edit($request);

    public function update($request);

}
