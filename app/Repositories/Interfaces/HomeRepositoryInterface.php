<?php

namespace App\Repositories\Interfaces;

interface HomeRepositoryInterface
{
    public function index();
    public function sliders();
    public function categories();
    public function cities();
    public function shops();
    public function subCategories();
    public function products();
    public function reviews();
    public function offers();
    public function users();
    public function delegates();
    public function tutorials();
    public function socials();
}
