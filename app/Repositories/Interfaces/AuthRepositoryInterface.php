<?php

namespace App\Repositories\Interfaces;

interface AuthRepositoryInterface
{
    public function Register($request);
    public function sendCode($key);
    public function verifyCode($request);
    public function Login($user,$request);
    public function changePassword($user,$request);
    public function updateProfile($user,$request);
    public function changeStatus($user);

}
