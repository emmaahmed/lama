<?php

namespace App\Repositories\Interfaces;

interface UserAddressRepositoryInterface
{
    public function getUserAddresses($user_id);

    public function store($request,$user_id);

    public function delete($user_id,$address_id);
}