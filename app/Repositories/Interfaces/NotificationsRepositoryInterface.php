<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/19/2020
 * Time: 9:19 AM
 */

namespace App\Repositories\Interfaces;

interface NotificationsRepositoryInterface
{
    public function getNotifications($user_id);
}
