<?php

namespace App\Repositories\Interfaces;

interface SocialMediaRepositoryInterface
{
    public function getSocial();

    public function store($request);

    public function edit($request);

    public function update($request);
}
