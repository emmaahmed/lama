<?php


namespace App\Repositories;


use App\Mail\ActivationMail;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use App\User;
use App\Verification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthRepository implements AuthRepositoryInterface
{
    public function Register($request)
    {
        if($request->email!='') {
            $data = $request->all();
        }
        else{
            $data = $request->except('email');

        }
        $data['jwt']=Str::random('25');
//        dd($data);

        $user = User::create($data);
        return $user;
    }

    public function sendCode($key)
    {
        $data=array();
        $code = verification_code();
        if (is_numeric($key)){     // $key => $phone

        //

        $message = "كود التفعيل الخاص بك هو".$code;
        $message = urlencode($message);
        $data['recipients']=[$key];
        $data['body']=$message;
        $data['sender']='Lama';
        $datastring=json_encode($data);

      //  $url = "https://www.alfa-cell.com/api/msgSend.php?apiKey=53f217ca2e287ba8eceff64d38dcfb9a&numbers=". $key ."&sender=Sedra-AD&msg=" . $message . "&applicationType=68&returnJson=1&lang=3" ;
            $url = "https://api.taqnyat.sa/v1/messages" ;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $datastring);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: Bearer 2e92f3ecd680c605b7ae51d7a59d2538'));

            $data = curl_exec($ch);
            //dd($data);
        curl_close($ch);
        $decodedData = json_decode($data);

        //

            $user = User::wherePhone($key)->first();
            Verification::updateOrCreate(
                ['user_id' => $user->id],
                ['user_id' => $user->id, 'phone' => $key, 'code' => $code, 'expire_at' => Carbon::now()->addHour()]
            );
        }else{
            $user = User::whereEmail($key)->first();// $key => $email
            Verification::updateOrCreate(
                ['user_id' => $user->id],
                ['user_id' => $user->id, 'email' => $key, 'code' => $code, 'expire_at' => Carbon::now()->addHour()]
            );
        }
        // Send SMS
//        $this->sms($user->phone,$code, true);
        // Send Email
      //  Mail::to($user->email)->send(new ActivationMail(['code' => $code]));
        $from='lama@emails.mobile-app-company.com';
        $jsonurl = 'http://emails.mobile-app-company.com/index.php?' . "email=" . $user->email  . "&from=" . $from."&verification_code=".$code;
        $json = file_get_contents($jsonurl);

    }

    public function verifyCode($request)
    {
        $verified = Verification::where('code',$request->code)->first();
        if ($verified->used == 1){
            return callback_data(error(), 'code_used');
        }
        if ($verified->expire_date > Carbon::now()){
            return callback_data(error(),'time_exceeded');
        }
        //$user = User::where('phone',$verified->phone)->orWhere('email',$verified->email)->first();
        $user=User::whereId($verified->user_id)->first();
//        dd($user);
        if ($user){
            // Update User
            $user->status = 1;
            $user->platform = $request->platform??'ios';
            $user->save();
            // Delete Verified
            $verified->delete();

            return callback_data(success(),'activated',$user);
        }else{
            return callback_data(error(),'invalid_data');
        }
    }

    public function Login($user,$request)
    {
        $user->jwt = Str::random('25');
        if ($request->has('firebase_token')){
            $user->firebase_token = $request->firebase_token;
        }
        if ($request->has('platform')){
            $user->platform = $request->platform;
        }
        return $user->save();
    }

    public function changePassword($user,$request)
    {
        $user->password = $request->password;
        $user->save();
    }

    public function updateProfile($user,$request)
    {
        if ($request->has('image') && is_file($request->image)){
            if (!empty($user->getOriginal('image'))){
                unlinkFile($user->getOriginal('image'), 'users');
            }
        }
        return $user->update($request->all());
    }
    public function changeStatus($user){
        if($user->status==1){
            $user->update(['status'=>2]);
        }
        else{
            $user->update(['status'=>1]);

        }
        return true;
    }

}
