<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/19/2020
 * Time: 9:16 AM
 */

namespace App\Repositories;


use App\Notification;
use App\Repositories\Interfaces\NotificationsRepositoryInterface;

class NotificationsRepository implements NotificationsRepositoryInterface
{
    public function getNotifications($user)
    {
//        if ($user->type == 0){
            $notifications = Notification::orderBy('id','desc')->where('user_id', $user->id)->with('shop')->paginate(10);
//dd($notifications);
            //        }else{
//            $notifications = Notification::orderBy('id','desc')->where('delegate_id', $user->id)->with('shop')->paginate(10);
//        }

        return $notifications;
    }


}
