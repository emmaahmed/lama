<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/22/2020
 * Time: 2:30 PM
 */

namespace App\Repositories;


use App\Repositories\Interfaces\HomeRepositoryInterface;
use App\Repositories\Interfaces\SlidersRepositoryInterface;
use App\Slider;

class SlidersRepository implements SlidersRepositoryInterface
{
    protected $homeRepository;

    public function __construct(HomeRepositoryInterface $homeRepository)
    {
        $this->homeRepository = $homeRepository;
    }

    public function index()
    {
        $data['title'] = "السلايدر";
        $data['sliders'] = $this->homeRepository->sliders()->paginate(10);
        return $data;
    }

    public function create()
    {
        $data['title'] = "إضافة سلايدر";
        $data['shops'] = $this->homeRepository->shops()->get();
        return $data;
    }

    public function store($request)
    {
        $created = $this->homeRepository->sliders()->create($request->all());
        return $created;

    }

    public function edit($request)
    {
        $data['title'] = "تعديل السلايدر";
        $slider = $this->homeRepository->sliders()->where('id',$request->slider_id)->first();
        $data['slider'] = $slider;
        $data['shops'] = $this->homeRepository->shops()->get();
        return $data;
    }

    public function update($request)
    {
        $slider = $this->homeRepository->sliders()->where('id',$request->slider_id)->first();
        if (isset($slider)){
            $slider->update($request->only(['shop_id', 'image']));
            return true;
        }
        return false;
    }

}
