<?php


namespace App\Repositories;


use App\City;
use App\Repositories\Interfaces\CityRepositoryInterface;
use App\Repositories\Interfaces\HomeRepositoryInterface;

class CityRepository implements CityRepositoryInterface
{
    protected $homeRepository;

    public function __construct(HomeRepositoryInterface $homeRepository)
    {
        $this->homeRepository = $homeRepository;
    }

    public function getCities()
    {
//        $cities = $this->homeRepository->cities()->paginate(10);
        $cities = $this->homeRepository->cities()->get();
        return $cities;
    }

    // Admin

    public function store($request)
    {
        $city = new City();
        $city->save();
        $city->translateOrNew('ar')->name = $request->name_ar;
        $city->translateOrNew('en')->name = $request->name_ar;
//        $city->translateOrNew('en')->name = $request->name_en;
        return $city->save();
    }

    public function edit($request)
    {
        $data['title'] = "تعديل المدينة";
        $city = $this->homeRepository->cities()->where('id',$request->city_id)->first();
        $data['city'] = $city;
        return $data;
    }

    public function update($request)
    {
        $city = $this->homeRepository->cities()->where('id',$request->city_id)->first();
        if (isset($city)){
            $city->translateOrNew('ar')->name = $request->name_ar;
            $city->translateOrNew('en')->name = $request->name_ar;
//            $city->translateOrNew('en')->name = $request->name_en;
            if ($city->save()){
                return true;
            }
        }
        return false;
    }
}
