<?php


namespace App\Repositories;


use App\Contact;
use App\Mail\ShopMail;
use App\Repositories\Interfaces\ContactRepositoryInterface;
use Illuminate\Support\Facades\Mail;

class ContactRepository implements ContactRepositoryInterface
{
    public function sendContact($request)
    {
        $contact = Contact::create($request->all());
        return $contact;
    }

    // Admin

    public function index()
    {
        $data['title'] = 'الوارد';
        $data['contacts'] = Contact::get();
        return $data;
    }

    public function reply($request)
    {
//        dd($request->all());
        $contact = Contact::where('id',$request->contact_id)->first();
        $contact->update([
           'reply' => $request->reply,
           'is_replied' => 1,
        ]);
        $send_message = urlencode($request->reply);
        $from='lama@emails.mobile-app-company.com';
        $jsonurl = 'http://emails.mobile-app-company.com/index-lamma.php?' . "email=" . $contact->email  . "&from=" . $from."&message=".$send_message;
        $url =  preg_replace("/ /", "%20", $jsonurl);
        $json = file_get_contents($url);

        return $contact;
    }
}
