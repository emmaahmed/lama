<?php


namespace App\Repositories;


use App\Repositories\Interfaces\UserAddressRepositoryInterface;
use App\UserAddress;

class UserAddressRepository implements UserAddressRepositoryInterface
{
    public function getUserAddresses($user_id)
    {
        $user_addresses = UserAddress::where('user_id',$user_id)->paginate(10);
        return $user_addresses;
    }
    public function store($request,$user_id)
    {
//        $user_address = UserAddress::create(array_merge($request->all(), ['user_id' => $user_id]));
        $user_address = UserAddress::updateOrCreate(['id' => $request->id], array_merge($request->all(), ['user_id' => $user_id]));
        return $user_address;
    }

    public function delete($address_id, $user_id)
    {
        $user_address = UserAddress::where(['id' => $address_id, 'user_id' => $user_id])->first();
        return $user_address->delete();
    }
}
