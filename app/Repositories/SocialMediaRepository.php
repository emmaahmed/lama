<?php


namespace App\Repositories;


use App\Repositories\Interfaces\HomeRepositoryInterface;
use App\Repositories\Interfaces\SocialMediaRepositoryInterface;
use App\SocialMedia;

class SocialMediaRepository implements SocialMediaRepositoryInterface
{

    protected $homeRepository;

    public function __construct(HomeRepositoryInterface $homeRepository)
    {
        $this->homeRepository = $homeRepository;
    }

    public function getSocial()
    {
//        $social_medias = $this->homeRepository->socials()->paginate(10);
        $social_medias = $this->homeRepository->socials()->get();
        return $social_medias;
    }

    public function store($request)
    {
        $created = $this->homeRepository->socials()->create($request->all());
        return $created;
    }

    public function edit($request)
    {
        $data['title'] = "تعديل السوشيال";
        $social = $this->homeRepository->socials()->where('id',$request->social_id)->first();
        $data['social'] = $social;
        return $data;
    }

    public function update($request)
    {
        $social = $this->homeRepository->socials()->where('id',$request->social_id)->first();
        $social->link = $request->link;
        if ($request->has('logo')){
            if (!empty($social->getOriginal('logo'))){
                unlinkFile($social->getOriginal('logo'), 'socials');
            }
            $social->logo = $request->logo;
            $social->save();
        }
        return true;
    }
}
