<?php


namespace App\Repositories;


use App\City;
use App\Repositories\Interfaces\HomeRepositoryInterface;
use App\Slider;
use App\SocialMedia;
use App\Tutorial;
use App\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Modules\Category\Entities\Category;
use Modules\Product\Entities\Offer;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\Rate;
use Modules\Shop\Entities\Shop;
use Modules\Shop\Entities\SubCategory;
use Carbon\Carbon;
class HomeRepository implements HomeRepositoryInterface
{
    public function index()
    {
        $data['sliders'] = $this->sliders()
            ->select('id','image','shop_id')->get();
        $data['categories'] = $this->categories()
            ->select('id','image')->where('active',1)->take(9)->get();
        $data['products'] = $this->products()->whereHas('shop',function($query){
                $query->where('expire_at','>',Carbon::now())->where('status','1')->where('online',1);
            })
            ->where('status',1)

            ->orderBy('id','desc')

            ->select('id','shop_id','price_before','price_after','percent','has_discount','created_at') ->take(9)
            ->get();
        $bestOffers=$this->products()

            ->where(['has_discount'=>1,'status'=>1])
            ->whereHas('shop',function($query){
                $query->where('expire_at','>',Carbon::now())->where('status','1')->where('online',1);
            })
            ->where('percent','>',0)

            ->select('id','shop_id','price_before','price_after','percent','has_discount','created_at')


            ->get()->sortByDesc('created_at')->sortByDesc('percent')->take(9)->values()->all();
        $data['best_offers'] =$bestOffers;
        return $data;
    }
    public function allOffers(){
       $data= $this->products()

            ->where(['has_discount'=>1,'status'=>1])
            ->whereHas('shop',function($query){
                $query->where('expire_at','>',Carbon::now())->where('status','1')->where('online',1);
            })
            ->where('percent','>',0)

            ->select('id','shop_id','price_before','price_after','percent','has_discount','created_at')

            ->get()->sortByDesc('percent')->values()->all();
        $salons = new LengthAwarePaginator($data, count($data), 20);
        return $salons->withPath('https://lamh.online/api/user/all-offers');

    }

    public function sliders()
    {
        return Slider::query();
    }
    public function categories()
    {
        return Category::query();
    }
    public function cities()
    {
        return City::query();
    }
    public function shops()
    {
        return Shop::query();
    }
    public function subCategories()
    {
        return SubCategory::query();
    }
    public function products()
    {
        return Product::query();

;
    }
    public function reviews()
    {
        return Rate::query();
    }
    public function offers()
    {
        return Offer::query();
    }
    public function users()
    {
        return User::query();
    }
    public function delegates()
    {
        return User::where('type', 1);
    }
    public function tutorials()
    {
        return Tutorial::query();
    }
    public function socials()
    {
        return SocialMedia::query();
    }
}
