<?php


namespace App\Repositories;


use App\Repositories\Interfaces\HomeRepositoryInterface;
use App\Repositories\Interfaces\TutorialRepositoryInterface;
use App\Tutorial;

class TutorialRepository implements TutorialRepositoryInterface
{
    protected $homeRepository;

    public function __construct(HomeRepositoryInterface $homeRepository)
    {
        $this->homeRepository = $homeRepository;
    }

    public function tutorials($type=null)
    {
        if (!empty($type) && ($type == 0 || $type == 1|| $type == 2)){
            $tutorials = $this->homeRepository->tutorials()->where('type',$type)->get();
        }else{
            $tutorials = $this->homeRepository->tutorials()->get();
        }

        return $tutorials;
    }

    public function store($request)
    {
        $tutorial = $this->homeRepository->tutorials()->create($request->all());
        $tutorial->translateOrNew('ar')->name = $request->name_ar;
        $tutorial->translateOrNew('en')->name = $request->name_ar;
//        $tutorial->translateOrNew('en')->name = $request->name_en;
        $tutorial->translateOrNew('ar')->description = $request->description_ar;
        $tutorial->translateOrNew('en')->description = $request->description_ar;
//        $tutorial->translateOrNew('en')->description = $request->description_en;
        return $tutorial->save();
    }

    public function edit($request)
    {
        $data['title'] = "تعديل الصفحة";
        $tutorial = $this->homeRepository->tutorials()->where('id',$request->tutorial_id)->first();
        $data['tutorial'] = $tutorial;
        return $data;
    }

    public function update($request)
    {
        $tutorial = $this->homeRepository->tutorials()->where('id',$request->tutorial_id)->first();
        if (isset($tutorial)){
            $tutorial->type = $request->type;
            $tutorial->translateOrNew('ar')->name = $request->name_ar;
            $tutorial->translateOrNew('en')->name = $request->name_ar;
//        $tutorial->translateOrNew('en')->name = $request->name_en;
            $tutorial->translateOrNew('ar')->description = $request->description_ar;
            $tutorial->translateOrNew('en')->description = $request->description_ar;
//        $tutorial->translateOrNew('en')->description = $request->description_en;
            if ($tutorial->save()){
                if ($request->has('image')){
                    if (!empty($tutorial->getOriginal('image'))){
                        unlinkFile($tutorial->getOriginal('image'), 'tutorials');
                    }
                    $tutorial->image = $request->image;
                    $tutorial->save();
                }
                return true;
            }
        }
        return false;
    }
}
