<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{
    protected $fillable = [
        'user_id',
        'shop_id',
        'phone',
        'email',
        'code',
        'expire_at',
        'used',
    ];
}
