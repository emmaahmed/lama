<?php

use App\ShopRequest;
use App\User;
use Illuminate\Support\Facades\Config;
use Intervention\Image\Facades\Image;
use Modules\Setting\Entities\Setting;
use Modules\Shop\Entities\Shop;
use Modules\Admin\Entities\Admin;

// Status Codes
function sendOrderNotification($shop_id, $order_id)
{
    $api_key = getServerKey();

//dd(Shop::whereId($shop_id)->orWhere('parent_id',$shop_id)->get());
    // dd($api_key);
    $shop = Shop::whereId($shop_id)->orWhere('parent_id',$shop_id)->where('firebase_web_token','!=',null)->pluck('firebase_web_token')->toArray();
//    dd($shop);


//    $firebase_token="cPeA8jTglxf67T1sGLSIbr:APA91bFFhTwubeQX15u5hNjbp_OaRaKNMauJvKF5VEZpbygp8YIgthflpbaj3EMq-J65Hk4Z6msoBHDKSBX8ufeoPzFOQaeAlEHEo32tE1JlFa8CC3Z1o4LA4cIDnbESEdCRTgGepBny";

    $fields = array
    (
        "registration_ids" => $shop,
        "priority" => 10,

        'message' => [
            'body' => "testtttt",
            'title' => "رسالة جديدة",
            "click_action" => "https://www.google.com/",

        ],
        'notification' => [
            'type' => 'order',
            "order_id" => $order_id,
        ],
//        'webpush'=>[
//            'notification'=>[
//            'requireInteraction'=> true,
//            'icon'=>'/icons/notification.png'
//        ], 'fcm_options'=> [
//            'link'=> 'https://www.google.com/',
//
//            ]
//        ],
        'vibrate' => 1,
    );
    $headers = array
    (
        'accept: application/json',
        'Content-Type: application/json',
        'Authorization: key=' . $api_key
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    //  var_dump($result);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
//    dd($result);

}

function sendMessageNotification($shop_id, $user_id, $user_name, $user_message, $type, $user_image)
{
    $api_key = getServerKey();


    // dd($api_key);
    $shop = Shop::whereId($shop_id)->first();
//    $firebase_token="cPeA8jTglxf67T1sGLSIbr:APA91bFFhTwubeQX15u5hNjbp_OaRaKNMauJvKF5VEZpbygp8YIgthflpbaj3EMq-J65Hk4Z6msoBHDKSBX8ufeoPzFOQaeAlEHEo32tE1JlFa8CC3Z1o4LA4cIDnbESEdCRTgGepBny";

    $fields = array
    (
        "registration_ids" => [$shop->firebase_web_token],
        "priority" => 10,

        'message' => [
            'body' => "رسالة جديدة",
            'title' => "رسالة جديدة",
            "click_action" => "https://www.google.com/",

        ],
        'notification' => [
            'type' => $type,
            "user_id" => $user_id,
//            'message'=>$message,
            "user_name" => $user_name,
            "user_message" => $user_message,
            "user_image" => $user_image,

        ],
//        'webpush'=>[
//            'notification'=>[
//            'requireInteraction'=> true,
//            'icon'=>'/icons/notification.png'
//        ], 'fcm_options'=> [
//            'link'=> 'https://www.google.com/',
//
//            ]
//        ],
        'vibrate' => 1,
    );
    $headers = array
    (
        'accept: application/json',
        'Content-Type: application/json',
        'Authorization: key=' . $api_key
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    //  var_dump($result);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);

}

function sendNotificationToAdmin($admin_id, $shop_name, $message, $shop_image, $shop_id, $type)
{
    $api_key = getServerKey();


    // dd($api_key);
    if ($type == 'shop-message') {
        $shop = Admin::whereId($admin_id)->first();
        $token=$shop->firebase_token;
    } else {
        $shop = Shop::whereId($shop_id)->first();
        $token=$shop->firebase_web_token;

    }
//    $firebase_token="cPeA8jTglxf67T1sGLSIbr:APA91bFFhTwubeQX15u5hNjbp_OaRaKNMauJvKF5VEZpbygp8YIgthflpbaj3EMq-J65Hk4Z6msoBHDKSBX8ufeoPzFOQaeAlEHEo32tE1JlFa8CC3Z1o4LA4cIDnbESEdCRTgGepBny";

    $fields = array
    (
        "registration_ids" => [$token],
        "priority" => 10,

        'message' => [
            'body' => "testtttt",
            'title' => "رسالة جديدة",
            "click_action" => "https://www.google.com/",

        ],
        'notification' => [
            'type' => $type,
            "user_id" => $admin_id,
            'user_message' => $message,
            "user_name" => $shop_name,
            "user_image" => $shop_image,
            "shop_id" => $shop_id,

        ],
//        'webpush'=>[
//            'notification'=>[
//            'requireInteraction'=> true,
//            'icon'=>'/icons/notification.png'
//        ], 'fcm_options'=> [
//            'link'=> 'https://www.google.com/',
//
//            ]
//        ],
        'vibrate' => 1,
    );
    $headers = array
    (
        'accept: application/json',
        'Content-Type: application/json',
        'Authorization: key=' . $api_key
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    //  var_dump($result);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);

}

function success()
{
    return 200;
}

function error()
{
    return 401;
}

function token_expired()
{
    return 403;
}

function not_active()
{
    return 405;
}

function verification_code()
{
    $code = mt_rand(1000, 9999);
    return $code;
}

function getFastShippingFees()
{
    $settings = Setting::first();
    return $settings->fast_delivery_fees;
}

function checkJWT()
{
    $jwt = request()->header('jwt');
    $user = User::where("jwt", $jwt)->first();
    return $user;
}

function checkShopJWT()
{
    $jwt = request()->header('jwt');
    $user = Shop::where("jwt", $jwt)->first();
    return $user;
}

function checkShopRequestJWT()
{
    $jwt = request()->header('jwt');
    $user = ShopRequest::where("jwt", $jwt)->first();
    return $user;
}

function checkDelegateJWT()
{
    $jwt = request()->header('jwt');
    $user = User::where('type', 1)->where("jwt", $jwt)->first();
    return $user;
}

function send_to_user($tokens, $msg, $type, $shop_id, $order_id = "", $shop = null,$platform=null)
{

    send($tokens, $msg, $type, $shop_id, $order_id, $shop,$platform);
}
function send_to_shop($tokens, $msg, $type, $user_id, $order_id = "", $shop = null,$platform=null)
{
    sendShop($tokens, $msg, $type, $user_id, $order_id, $shop);
}


//function send_to_worker($tokens, $msg , $type, $order_id,$status = "",$image = ""){
//    send($tokens, $msg , $type,$order_id,false,$status,$image);
//}

function send($tokens, $msg, $type, $shop_id, $order_id, $shop,$platform)
{
    $api_key = getServerKey();

    if (is_string($shop)) {
        $shop = json_decode($shop);
    }
    if($platform=='android')
    $fields = array
    (
        "registration_ids" => (array)$tokens,
        "priority" => 10,
        'data' => [
            'title' => $shop ? $shop->name : 'Admin',
            'sound' =>$type==1? 'sms.wav':'default',
            'message' => $msg,
            'body' => $msg,
            'type' => $type,
            'shop_id' => $shop_id,
            'order_id' => $order_id,
            'shop_details' => $shop,
            'logo' => $shop ? $shop->logo : '',
            'mutable-content' => 1
        ],
//        'notification' => [
//            'title' => $shop ? $shop->name : 'Admin',
//            'sound' =>$type==1? 'sms.wav':'default',
//            'message' => $msg,
//            'body' => $msg,
//            'type' => $type,
//            'shop_id' => $shop_id,
//            'order_id' => $order_id,
//            'shop_details' => $shop,
//            'logo' => $shop ? $shop->logo : '',
//            'mutable-content' => 1
//
//        ],
//        'notification' => [
//            'type'    => $type,
//            'body' => $msg,
//            'order_id' => $order_id,
//            'title' => "رسالة جديدة",
//            'sound' => 'default'
//        ],
        'vibrate' => 1,
        'sound' => 1
    );
    else{
        $fields = array
        (
            "registration_ids" => (array)$tokens,
            "priority" => 10,
            'data' => [
                'title' => $shop ? $shop->name : 'Admin',
                'sound' =>$type==1? 'sms.wav':'default',
                'message' => $msg,
                'body' => $msg,
                'type' => $type,
                'shop_id' => $shop_id,
                'order_id' => $order_id,
                'shop_details' => $shop,
                'logo' => $shop ? $shop->logo : '',
                'mutable-content' => 1
            ],
        'notification' => [
            'title' => $shop ? $shop->name : 'Admin',
            'sound' =>$type==1? 'sms.wav':'default',
            'message' => $msg,
            'body' => $msg,
            'type' => $type,
            'shop_id' => $shop_id,
            'order_id' => $order_id,
            'shop_details' => $shop,
            'logo' => $shop ? $shop->logo : '',
            'mutable-content' => 1

        ],
//        'notification' => [
//            'type'    => $type,
//            'body' => $msg,
//            'order_id' => $order_id,
//            'title' => "رسالة جديدة",
//            'sound' => 'default'
//        ],
            'vibrate' => 1,
            'sound' => 1
        );

    }
    $headers = array
    (
        'accept: application/json',
        'Content-Type: application/json',
        'Authorization: key=' . $api_key
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    //  var_dump($result);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
//dd($result);
    return $result;
}
function sendShop($tokens, $msg, $type, $user_id, $order_id, $shop,$platform=null)
{
//    dd($shop);
    $api_key = getServerKey();
    if (is_string($shop)) {
        $shop = json_decode($shop);
    }
    if($platform=='android') {
        $fields = array
        (
            "registration_ids" => (array)$tokens,
            "priority" => 10,
            'data' => [
                'title' => $shop ? $shop->name : 'Admin',
                'sound' => $type == 2 ? 'sms.wav' : 'default',
                'message' => $msg,
                'body' => $msg,

                'type' => $type,
                'order_id' => $order_id,
                'user_id' => $user_id,
                'logo' => $shop ? $shop->logo : '',
                'mutable-content' => 1
            ],
//        'notification' => [
//            'title' => $shop ? $shop->name : 'shop',
//            'sound' =>$type==2? 'sms.wav':'default',
//            'message' => $msg,
//            'body' => $msg,
//            'type' => $type,
//            'order_id' => $order_id,
//            'logo' => $shop ? $shop->logo : '',
//            'mutable-content' => 1
//
//        ],
//        'notification' => [
//            'type'    => $type,
//            'body' => $msg,
//            'order_id' => $order_id,
//            'title' => "رسالة جديدة",
//            'sound' => 'default'
//        ],
            'vibrate' => 1,
            'sound' => $type == 2 ? 'sms.wav' : 'default',
        );
    }else{
        $fields = array
        (
            "registration_ids" => (array)$tokens,
            "priority" => 10,
            'data' => [
                'title' => $shop ? $shop->name : 'Admin',
                'sound' => $type == 2 ? 'sms.wav' : 'default',
                'message' => $msg,
                'body' => $msg,

                'type' => $type,
                'order_id' => $order_id,
                'user_id' => $user_id,
                'logo' => $shop ? $shop->logo : '',
                'mutable-content' => 1
            ],
        'notification' => [
            'title' => $shop ? $shop->name : 'shop',
            'sound' =>$type==2? 'sms.wav':'default',
            'message' => $msg,
            'body' => $msg,
            'type' => $type,
            'order_id' => $order_id,
            'logo' => $shop ? $shop->logo : '',
            'mutable-content' => 1

        ],
//        'notification' => [
//            'type'    => $type,
//            'body' => $msg,
//            'order_id' => $order_id,
//            'title' => "رسالة جديدة",
//            'sound' => 'default'
//        ],
            'vibrate' => 1,
            'sound' => $type == 2 ? 'sms.wav' : 'default',
        );

    }
    $headers = array
    (
        'accept: application/json',
        'Content-Type: application/json',
        'Authorization: key=' . $api_key
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
//    dd($result);
    //  var_dump($result);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
//    dd($result);
    return $result;
}

function sendBlocked($tokens, $msg, $type)
{
    $api_key = getServerKey();
    $fields = array
    (
        "registration_ids" => (array)$tokens,
        "priority" => 10,
        'data' => [
            'title' => 'Lama',
            'sound' => 'default',
            'message' => $msg,
            'body' => $msg,
            'type' => $type,
            'mutable-content' => 1

        ],
        'notification' => [
            'title' => 'Lama',
            'sound' => 'default',
            'message' => $msg,
            'body' => $msg,
            'type' => $type,
            'mutable-content' => 1

        ],
//        'notification' => [
//            'type'    => $type,
//            'body' => $msg,
//            'order_id' => $order_id,
//            'title' => "رسالة جديدة",
//            'sound' => 'default'
//        ],
        'vibrate' => 1,
        'sound' => 1
    );
    $headers = array
    (
        'accept: application/json',
        'Content-Type: application/json',
        'Authorization: key=' . $api_key
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    //dd($result);
    //  var_dump($result);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

function getServerKey()
{
    return 'AAAAmG_dTmI:APA91bG1J25M0E-1vIXW2sdnVxQJoMPeceO1RoP5ieFleJGP_UqbdrQD2p4jEA196F6lGZ2I14Qryb6rZJmWfxVt3cxbPR8dMEflAPsboq2by6DCCFcvRTGVsRUrZX_U7gati5adKTP0';
}

function callback_data($status, $key, $data = [])
{
    $language = request()->header('lang');
    if (!empty($data)) {
        if (is_array($data)) {
            if (sizeof($data) > 0) {
                return response()->json([
                    'status' => $status,
                    'msg' => isset($language) ? Config::get('response.' . $key . '.' . request()->header('lang')) : Config::get('response.' . $key),
                    'data' => $data,
                ]);
            } else {
                return response()->json([
                    'status' => $status,
                    'msg' => isset($language) ? Config::get('response.' . $key . '.' . request()->header('lang')) : Config::get('response.' . $key),
                ]);
            }
        }
        return response()->json([
            'status' => $status,
            'msg' => isset($language) ? Config::get('response.' . $key . '.' . request()->header('lang')) : Config::get('response.' . $key),
            'data' => $data,
        ]);
    }
    return response()->json([
        'status' => $status,
        'msg' => isset($language) ? Config::get('response.' . $key . '.' . request()->header('lang')) : Config::get('response.' . $key),
    ]);
}

function callback_empty_data($status, $key, $data = [])
{
    $language = request()->header('lang');
//    if (!empty($data)){
//        if (is_array($data)){
//            if (sizeof($data) > 0){
//                return response()->json([
//                    'status' => $status,
//                    'msg' => isset($language) ? Config::get('response.'.$key.'.'.request()->header('lang')) : Config::get('response.'.$key),
//                    'data' => $data,
//                ]);
//            }else{
//                return response()->json([
//                    'status' => $status,
//                    'msg' => isset($language) ? Config::get('response.'.$key.'.'.request()->header('lang')) : Config::get('response.'.$key),
//                ]);
//            }
//        }
//        return response()->json([
//            'status' => $status,
//            'msg' => isset($language) ? Config::get('response.'.$key.'.'.request()->header('lang')) : Config::get('response.'.$key),
//            'data' => $data,
//        ]);
//    }
    return response()->json([
        'status' => $status,
        'msg' => isset($language) ? Config::get('response.' . $key . '.' . request()->header('lang')) : Config::get('response.' . $key),
        'data' => $data,

    ]);
}

function callback_data_cart($status, $key, $product_name)
{
    $language = request()->header('lang');
    return response()->json([
        'status' => $status,
        'msg' => isset($language) ? Config::get('response.' . $key . '.' . request()->header('lang')) . $product_name : Config::get('response.' . $key) . $product_name,
    ]);


}

function callback_data_chat($status, $key, $data = [])
{
    $language = request()->header('lang');
    if (!empty($data)) {
        if (is_array($data)) {
            if (sizeof($data) > 0) {
                return response()->json([
                    'status' => $status,
                    'msg' => isset($language) ? Config::get('response.' . $key . '.' . request()->header('lang')) : Config::get('response.' . $key),
                    'data' => $data,
                ]);
            } else {
                return response()->json([
                    'status' => $status,
                    'msg' => isset($language) ? Config::get('response.' . $key . '.' . request()->header('lang')) : Config::get('response.' . $key),
                    'data' => $data
                ]);
            }
        }
        return response()->json([
            'status' => $status,
            'msg' => isset($language) ? Config::get('response.' . $key . '.' . request()->header('lang')) : Config::get('response.' . $key),
            'data' => $data,
        ]);
    }
    return response()->json([
        'status' => $status,
        'msg' => isset($language) ? Config::get('response.' . $key . '.' . request()->header('lang')) : Config::get('response.' . $key),
        'data' => $data
    ]);
}

function distance($lat1, $lon1, $lat2, $lon2, $unit = "K")
{
    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
    } else {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}

function upload($file, $dir)
{
    $image = time() . uniqid() . '.jpg';
    $img = Image::make($file);
    // dd(Image::make($file)->encode('jpg',50));
//    dd($img->filesize());
    if ($img->filesize() > 10) {
        $img->encode('jpg', 20)->save(public_path('uploads' . '/' . $dir . '/' . $image), 20);

    } else {
        $img->encode('jpg', 50)->save(public_path('uploads' . '/' . $dir . '/' . $image), 50);

    }

    return $image;
}

function unlinkFile($image, $path)
{
    if ($image != null) {
        if (!strpos($image, 'https')) {
            if (file_exists(public_path("uploads/$path/") . $image)) {
                unlink(public_path("uploads/$path/") . $image);
            }
        }
    }
    return true;
}

function inRange($mylat, $mylng, $radius, $shop_id)
{
    $haversine = "(6371 * acos(cos(radians($mylat))
                           * cos(radians(shops.latitude))
                           * cos(radians(shops.longitude)
                           - radians($mylng))
                           + sin(radians($mylat))
                           * sin(radians(shops.latitude))))";
//    return $haversine;
    if ($shop_id != 0) {
        $datainradiusrange = Shop::whereId($shop_id)
            ->select('*')
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius])
            ->first();
    } else {
        $datainradiusrange = Shop::select('*')->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius]);

    }

    return $datainradiusrange;
}

// Admin Helper Functions

if (!function_exists('admin_url')) {
    function admin_url($url = null)
    {
        return url('admin/' . $url);
    }
}
if (!function_exists('shop_url')) {
    function shop_url($url = null)
    {
        return url('shop/' . $url);
    }
}
if (!function_exists('admin')) {
    function admin()
    {
        return auth()->guard('admin');
    }
}
if (!function_exists('shop')) {
    function shop()
    {
        return auth()->guard('shop');
    }
}


