<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TutorialTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = ['tutorial_id','name','description','locale'];

    protected $hidden = ['translations','created_at','updated_at'];
}
