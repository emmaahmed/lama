<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    protected $fillable = [
        'user_id',
        'start_date',
        'end_date',
        'code',
        'discount_type',
        'value',
        ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
