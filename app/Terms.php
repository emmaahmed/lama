<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terms extends Model
{
    protected $table='form_terms';
    protected $fillable = [
        'text', 'type'    ];

}
