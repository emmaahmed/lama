<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductInfo;
use Modules\Product\Entities\ProductInfoTranslation;
use Modules\Product\Entities\ProductTranslation;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;

class ProductsImport implements ToCollection, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \App\Models\Product
     */
    public function collection(Collection $rows)
    {
//        dd($rows);

        $spreadsheet = IOFactory::load(request()->file('excel'));
        $i = 0;
        $j = 0;
        foreach ($spreadsheet->getActiveSheet()->getDrawingCollection() as $drawing) {
            if ($drawing instanceof MemoryDrawing) {
                ob_start();
                call_user_func(
                    $drawing->getRenderingFunction(),
                    $drawing->getImageResource()
                );
                $imageContents = ob_get_contents();
                ob_end_clean();
                switch ($drawing->getMimeType()) {
                    case MemoryDrawing::MIMETYPE_PNG :
                        $extension = 'png';
                        break;
                    case MemoryDrawing::MIMETYPE_GIF:
                        $extension = 'gif';
                        break;
                    case MemoryDrawing::MIMETYPE_JPEG :
                        $extension = 'jpg';
                        break;
                }
            } else {
                $zipReader = fopen($drawing->getPath(), 'r');
                $imageContents = '';
                while (!feof($zipReader)) {
                    $imageContents .= fread($zipReader, 1024);
                }
                fclose($zipReader);
                $extension = $drawing->getExtension();
            }

            $myFileName = time() .++$i. '.' . $extension;
            Image::make($imageContents)->encode('jpg',50)->save(public_path('uploads'.'/products/'.$myFileName), 50);

//            file_put_contents('public/uploads/products/' . $myFileName, $imageContents);

            $product = Product::create([
                'quantity'     => $rows[$j]['alkmy'],
                'price_before'    => $rows[$j]['alsaar_alasasy'],
                'price_after'    => $rows[$j]['saar_almntj_baad_alkhsm'],
                'percent'    => $rows[$j]['nsb_alkhsm'],
                'sub_category_id'    => $rows[$j]['rkm_alksm_alfraay'],
                'has_discount'    => $rows[$j]['saar_almntj_baad_alkhsm']>0? 1:0,
                'shop_id'    => Auth::guard('shop')->user()->parent_id!=null?Auth::guard('shop')->user()->parent_id:Auth::guard('shop')->user()->id,
//                'image' => $myFileName,
            ]);
            $product->translateOrNew('ar')->name = $rows[$j]['esm_almntj_ballgh_alaarby'];
            $product->translateOrNew('en')->name = $rows[$j]['esm_almntj_ballgh_alaarby'];
//        $product->translateOrNew('en')->name = $request->name_en;
            $product->translateOrNew('ar')->short_description = $rows[$j]['alosf_almkhtsr_ballgh_alaarby'];
            $product->translateOrNew('en')->short_description = $rows[$j]['alosf_almkhtsr_ballgh_alaarby'];
//        $product->translateOrNew('en')->short_description = $request->short_description_en;
            $product->translateOrNew('ar')->long_description = $rows[$j]['alosf_alkaml_ballgh_alaarby'];
            $product->translateOrNew('en')->long_description = $rows[$j]['alosf_alkaml_ballgh_alaarby'];
            $product->save();
            $product->images()->create([
                'product_id'=>$product->id,
                'image'=>$myFileName

            ]);
//            dd($product);
            $j++;
        }

    }
}
