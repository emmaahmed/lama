<?php

namespace App;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['name','description'];

    protected $fillable = ['image','type'];

    protected $hidden = ['translations','type','created_at','updated_at'];

    public function getImageAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/tutorials').'/'.$image;
        }
        return "";
    }

    public function setImageAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'tutorials');
            $this->attributes['image'] = $imageFields;
        }
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }
}
