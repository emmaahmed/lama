<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $fillable = ['user_id','city_id','latitude','longitude','address','full_name','phone',
        'building_name','building_image','building_number','apartment','more_info','primary_address'];

    protected $hidden = ['created_at','updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class,'city_id');
    }

    public function setBuildingImageAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'addresses');
            $this->attributes['building_image'] = $imageFields;
        }
    }
    public function getBuildingImageAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/addresses').'/'.$image;
        }
        return "";
    }
}
