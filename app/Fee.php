<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    protected $fillable = ['normal_delivery','fast_delivery','distance_from','distance_to'];


    public static function getTableName()
    {
        return (new self())->getTable();
    }
}
