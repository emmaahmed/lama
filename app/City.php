<?php

namespace App;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Shop\Entities\Shop;

class City extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['name'];

    protected $hidden = ['translations','created_at','updated_at'];

    public function shops(){
        return $this->hasMany( Shop::class);
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }
}
