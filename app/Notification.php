<?php

namespace App;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Order\Entities\Order;
use Modules\Shop\Entities\Shop;

class Notification extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['title','body'];

    protected $fillable = ['img','user_id','shop_id','order_id','status','type','sender_type','read_at'];

    protected $hidden = ['translations','updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function shop()
    {
        return $this->belongsTo(Shop::class,'shop_id');
    }
    public function order()
    {
        return $this->belongsTo(Order::class,'order_id');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->format('Y-m-d h:i');
    }
    public function getShopIdAttribute($id)
    {
        return $id?:0;
    }
    public function getUserIdAttribute($id)
    {
        return $id?:0;
    }
    public function getOrderIdAttribute($id)
    {
        return $id?:0;
    }
    public function getImgAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/notifications').'/'.$image;
        }
        return "";
    }

    public function setImgAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'notifications');
            $this->attributes['img'] = $imageFields;
        }
    }

}
