<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    protected $fillable = ['link','logo'];

    protected $hidden = ['created_at','updated_at'];

    public function getLogoAttribute($logo)
    {
        if (!empty($logo)){
            return asset('uploads/social_media').'/'.$logo;
        }
        return "";
    }

    public function setLogoAttribute($logo)
    {
        if (is_file($logo)) {
            $imageFields = upload($logo, 'social_media');
            $this->attributes['logo'] = $imageFields;
        }
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }
}
