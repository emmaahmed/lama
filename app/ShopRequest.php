<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Modules\Shop\Entities\Shop;

class ShopRequest extends Model
{
    protected $fillable = [
        'jwt', 'category_id', 'city_id', 'area',
        'shop_name', 'shop_owner_name', 'image', 'email',
        'phone', 'password', 'verified', 'commercial_register',
        'license', 'name', 'id_image', 'address', 'latitude',
        'longitude', 'logo', 'cover', 'document', 'min_order',
        'delivery_types', 'from', 'to', 'payment', 'status', 'shop_info'
   ,'short_description','long_description','delivery_time','delivery_fees','platform'
    ];

    protected $hidden=['password'];

    public function setImageAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('shops_requests/image/'),$img_name);
        $this->attributes['image'] = $img_name ;
    }

    public function getImageAttribute($value)
    {
        if($value)
        {
            return asset('/shops/image/'.$value);
        }else{
            return asset('/default.png');
        }
    }
    public function setCoverAttribute($value)
    {
//        dd($value);
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('shops/covers/'),$img_name);
        $this->attributes['cover'] = $img_name ;
    }

    public function getCoverAttribute($value)
    {
        if($value)
        {
            return asset('/shops/covers/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    //
    public function setDocumentAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('shops_requests/documents/'),$img_name);
        $this->attributes['document'] = $img_name ;
    }
    public function getDocumentAttribute($value)
    {
        if($value)
        {
            return asset('/shops_requests/documents/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    public function setShopImageAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();

        $value->move(public_path('shops_requests/shop_name/'),$img_name);
        $this->attributes['shop_name'] = $img_name ;
    }

    public function getShopImageAttribute($value)
    {
        if($value)
        {
            return asset('/shops_requests/shop_name/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    //
    public function setCommercialRegisterAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('shops/commercials/'),$img_name);
        $this->attributes['commercial_register'] = $img_name ;
    }

    public function getCommercialRegisterAttribute($value)
    {
        if($value)
        {
            return asset('/shops/commercials/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    //
    public function setLicenseAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('shops/licenses/'),$img_name);
        $this->attributes['license'] = $img_name ;
    }

    public function getLicenseAttribute($value)
    {
        if($value)
        {
            return asset('/shops/licenses/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    //
    public function setIdImageAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('shops_requests/id_image/'),$img_name);
        $this->attributes['id_image'] = $img_name ;
    }

    public function getIdImageAttribute($value)
    {
        if($value)
        {
            return asset('/shops_requests/id_image/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    //
    public function setInnerImageAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('shops_requests/inner_image/'),$img_name);
        $this->attributes['inner_image'] = $img_name ;
    }

    public function getInnerImageAttribute($value)
    {
        if($value)
        {
            return asset('/shops_requests/inner_image/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    //
    public function setOuterImageAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('shops_requests/outer_image/'),$img_name);
        $this->attributes['outer_image'] = $img_name ;
    }

    public function getOuterImageAttribute($value)
    {
        if($value)
        {
            return asset('/shops_requests/outer_image/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    public function getCreatedAtAttribute()
    {
        if($this->attributes['created_at']!=null) {
            return Carbon::parse($this->attributes['created_at'])->format('Y-m-d g:i A');
        }

    }
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }
    public function getPaymentAttribute($value)
    {
        return  explode(",",$value);

    }
    public function getDeliveryTypesAttribute($value)
    {
        return  explode(",",$value);

    }
    public function getFromAttribute()
    {
        if($this->attributes['from']!=null) {

            return Carbon::createFromFormat('H:i:s', $this->attributes['from'])->format('G:i A');
        }
    }
    public function getToAttribute()
    {
        if($this->attributes['to']!=null) {

            return Carbon::createFromFormat('H:i:s', $this->attributes['to'])->format('G:i A');
        }
    }
    public function setFromAttribute($value)
    {
        if($value) {
            $this->attributes['from'] = Carbon::createFromFormat('G:i A', $value)->format('H:i:s');

        }
    }
    public function setToAttribute($value)
    {
        if($value) {

            $this->attributes['to'] = Carbon::createFromFormat('G:i A', $value)->format('H:i:s');

        } }
    public function getLogoAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/shops').'/'.$image;
        }
        return "";
    }

    public function setLogoAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'shops');
            $this->attributes['logo'] = $imageFields;
        }
    }

}
