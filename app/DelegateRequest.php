<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Shop\Entities\Shop;

class DelegateRequest extends Model
{
    protected $fillable = ['name','email','phone','address','image','id_image','driving_license',
        'car_contract','car_image','status'
        ];

    public function setImageAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('delegates_requests/image/'),$img_name);
        $this->attributes['image'] = $img_name ;
    }

    public function getImageAttribute($value)
    {
        if($value)
        {
            return asset('/delegates_requests/image/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    //
    public function setIdImageAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('delegates_requests/id_image/'),$img_name);
        $this->attributes['id_image'] = $img_name ;
    }

    public function getIdImageAttribute($value)
    {
        if($value)
        {
            return asset('/delegates_requests/id_image/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    //
    public function setDrivingLicenseAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('delegates_requests/driving_license/'),$img_name);
        $this->attributes['driving_license'] = $img_name ;
    }

    public function getDrivingLicenseAttribute($value)
    {
        if($value)
        {
            return asset('/delegates_requests/driving_license/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    //
    public function setCarImageAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('delegates_requests/car_image/'),$img_name);
        $this->attributes['car_image'] = $img_name ;
    }

    public function getCarImageAttribute($value)
    {
        if($value)
        {
            return asset('/delegates_requests/car_image/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    //
    //
    public function setCarContractAttribute($value)
    {
        $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
        $value->move(public_path('delegates_requests/car_contract/'),$img_name);
        $this->attributes['car_contract'] = $img_name ;
    }

    public function getCarContractAttribute($value)
    {
        if($value)
        {
            return asset('/delegates_requests/car_contract/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->format('Y-m-d g:i A');
    }


}
