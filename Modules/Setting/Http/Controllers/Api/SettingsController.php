<?php

namespace Modules\Setting\Http\Controllers\Api;

use App\Repositories\Interfaces\SocialMediaRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Setting\Repositories\Interfaces\SettingsRepositoryInterface;

class SettingsController extends Controller
{
    protected $settingsRepository, $socialMediaRepository;

    public function __construct(SettingsRepositoryInterface $settingsRepository, SocialMediaRepositoryInterface $socialMediaRepository)
    {
        $this->settingsRepository = $settingsRepository;
        $this->socialMediaRepository = $socialMediaRepository;
    }

    public function settings()
    {
        $settings = $this->settingsRepository->settings();
        $social = $this->socialMediaRepository->getSocial();
        $data['id'] = $settings->id;
        $data['image'] = $settings->image;
        $data['name'] = $settings->name;
        $data['about'] = $settings->about;
        $data['social'] = $social;
        return callback_data(success(), 'settings', $data);
    }
    
    public function aboutUs(Request $request)
    {
        $lang=$request->header('lang');
        $about = $this->settingsRepository->aboutUs()->first();
        //return response()->json(msgdata($request, success(), 'success',$about));
        return callback_data(success(), 'settings', $about);
    }

    public function termCondition(Request $request)
    {
        $term = $this->settingsRepository->termCondition()->select('term')->first();
        //return response()->json(msgdata($request, success(), 'success',$term));
        return callback_data(success(), 'settings', $term);
    }

    public function appExplanation(Request $request)
    {
        $data = $this->settingsRepository->appExplanation()->get();
        //return response()->json(msgdata($request, success(), 'success',$data));
        return callback_data(success(), 'settings', $data);
    }
}
