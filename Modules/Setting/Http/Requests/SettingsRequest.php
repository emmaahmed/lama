<?php

namespace Modules\Setting\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('admin/settings/edit')) {
            return $this->checkSettingsRules();
        }
        if($this->is('admin/settings/update')) {
            return $this->updateSettingsRules();
        }
    }

    public function messages()
    {
        if($this->is('admin/settings/edit')) {
            return $this->checkSettingsMessages();
        }
        if($this->is('admin/settings/update')) {
            return $this->updateSettingsMessages();
        }
    }

    public function checkSettingsRules()
    {
        return [
            'setting_id' => 'required|exists:settings,id',
        ];
    }

    public function checkSettingsMessages()
    {
        return [
            'setting_id.required' => 'اختر الإعداد',
            'setting_id.exists' => 'الإعداد غير موجود',
        ];
    }

    public function updateSettingsRules()
    {
        return [
            'name_ar' => 'required',
//            'name_en' => 'required',
            'about_ar' => 'required',
//            'about_en' => 'required',
            'fast_delivery_fees' => 'required',
        ];



    }

    public function updateSettingsMessages()
    {
        return [
            'name_ar.required' => 'ادخل الإسم باللغة العربية',
//            'name_en.required' => 'ادخل الإسم باللغة الإنجليزية',
            'about_ar.required' => 'ادخل عن التطبيق باللغة العربية',
//            'about_en.required' => 'ادخل التطبيق باللغة الإنجليزية',
            'fast_delivery_fees.required' => 'ادخل قيمة التوصيل السريع',
        ];
    }
}
