<?php

namespace Modules\Setting\Entities;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['name','about'];

    protected $hidden = ['translations','created_at','updated_at'];

    public $fillable = ['image','fast_delivery_fees'];

    public function setImageAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'settings');
            $this->attributes['image'] = $imageFields;
        }
    }
    public function getImageAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/settings').'/'.$image;
        }
        return "";
    }
}
