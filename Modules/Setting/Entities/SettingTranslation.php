<?php

namespace Modules\Setting\Entities;

use Illuminate\Database\Eloquent\Model;

class SettingTranslation extends Model
{
    protected $fillable = ['setting_id','name','about'];

    protected $hidden = ['translations','created_at','updated_at'];
}
