<?php


namespace Modules\Setting\Repositories;


use Modules\Admin\Entities\AboutUs;
use Modules\Admin\Entities\AppExplanation;
use Modules\Admin\Entities\Term;
use Modules\Setting\Entities\Setting;
use Modules\Setting\Repositories\Interfaces\SettingsRepositoryInterface;

class SettingsRepository implements SettingsRepositoryInterface
{
    public function settings()
    {
        $settings = Setting::first();
        return $settings;
    }

    public function edit()
    {
        $settings = $this->settings();
        $data['settings'] = $settings;
        $data['title'] = 'الاعدادات';
        return $data;
    }

    public function update($request)
    {
        $settings = $this->settings();
        if (isset($settings)){
            $settings->fast_delivery_fees = $request->fast_delivery_fees;
            $settings->image = $request->image;
            $settings->save();
            $settings->translateOrNew('ar')->name = $request->name_ar;
            $settings->translateOrNew('en')->name = $request->name_ar;
//            $settings->translateOrNew('en')->name = $request->name_en;
            $settings->translateOrNew('ar')->about = $request->about_ar;
            $settings->translateOrNew('en')->about = $request->about_ar;
//            $settings->translateOrNew('en')->about = $request->about_en;
            $settings->save();
        }else{
            $settings = new Setting();
            $settings->fast_delivery_fees = $request->fast_delivery_fees;
            $settings->image = $request->image;
            $settings->save();
            $settings->translateOrNew('ar')->name = $request->name_ar;
            $settings->translateOrNew('en')->name = $request->name_ar;
//            $settings->translateOrNew('en')->name = $request->name_en;
            $settings->translateOrNew('ar')->about = $request->about_ar;
            $settings->translateOrNew('en')->about = $request->about_ar;
//            $settings->translateOrNew('en')->about = $request->about_en;
            $settings->save();
        }
        session()->flash('success','تم التعديل بنجاح');
        return redirect()->back();
    }

    public function aboutUs()
    {
        return new Aboutus;
    }

    public function termCondition()
    {
        return new Term();
    }

    public function appExplanation()
    {
        return new AppExplanation();
    }
}
