<?php

namespace Modules\Setting\Repositories\Interfaces;

interface SettingsRepositoryInterface
{
    public function settings();

    public function edit();

    public function update($request);
    
    public function aboutUs();
    public function termCondition();
    public function appExplanation();
}
