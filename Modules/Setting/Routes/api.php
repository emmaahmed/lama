<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/setting', function (Request $request) {
    return $request->user();
});

Route::middleware('checkDatabase')->group(function () {

    Route::namespace('Api')->group(function () {

        Route::get('/settings', 'SettingsController@settings');

        //
        Route::get('/about-us', 'SettingsController@aboutUs');
        Route::get('/terms', 'SettingsController@termCondition');
        Route::get('/questions', 'SettingsController@appExplanation');
        //
    });
});
