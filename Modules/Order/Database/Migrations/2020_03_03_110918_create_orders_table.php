<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('delegate_id')->unsigned()->nullable();
            $table->integer('shop_id')->unsigned()->nullable();
            $table->integer('address_id')->unsigned()->nullable();
            $table->string('total_cost')->nullable();
            $table->string('delivery_time')->nullable();
            $table->string('delivery_type')->nullable()
                ->comment('0 => Normal, 1 => Fast');
            $table->string('status')->default(0);
            
            //status=> 0=waiting_accept , 1=accept , 2=on_way , 3=delivered , 4=cancelled
            
            $table->string('payment')->nullable()
                ->comment('0 => Cache, 1 => Visa');
            
            $table->double('shop_rate')->nullable();
            $table->string('shop_comment')->nullable();
            $table->double('delegate_rate')->nullable();
            $table->string('delegate_comment')->nullable();
            $table->double('user_rate')->nullable();
            $table->string('user_comment')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('delegate_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
            $table->foreign('address_id')->references('id')->on('user_addresses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
