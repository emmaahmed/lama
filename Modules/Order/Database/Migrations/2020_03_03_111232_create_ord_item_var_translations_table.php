<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdItemVarTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ord_item_var_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ord_item_var_id')->unsigned();
            $table->string('variation_name')->nullable();
            $table->string('option_name')->nullable();
            $table->string('locale')->index();
            $table->unique(['ord_item_var_id','locale']);
            $table->timestamps();

            $table->foreign('ord_item_var_id')->references('id')->on('ord_item_vars')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ord_item_var_translations');
    }
}
