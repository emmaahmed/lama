<?php

namespace Modules\Order\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BlockedOrder extends Model
{
    protected $fillable = ['order_id','delegate_id'];

    public function order()
    {
        $this->belongsTo(Order::class);
    }

    public function delegate()
    {
        $this->belongsTo(User::class,'delegate_id');
    }
}
