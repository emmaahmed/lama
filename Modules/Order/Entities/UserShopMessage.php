<?php

namespace Modules\Order\Entities;

use App\User;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserShopMessage extends Authenticatable
{
    use Notifiable;
    //use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sender_id', 'receiver_id','sender_type', 'receiver_type','shop_id', 'message','read_at'
    ];
    protected $dates=[
        'created_at',
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'sender_id')
            ->select('id','name','image');
    }






}
