<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;

class OrdItemVarTranslation extends Model
{
    protected $fillable = ['ord_item_var_id','variation_name','option_name'];

    protected $hidden = ['translations','created_at','updated_at'];

    public function ordItemVar()
    {
        return $this->belongsTo(OrdItemVar::class,'ord_item_var_id');
    }
}
