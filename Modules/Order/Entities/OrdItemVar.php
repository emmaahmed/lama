<?php

namespace Modules\Order\Entities;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class OrdItemVar extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['variation_name','option_name'];

    protected $fillable = ['order_item_id','price'];

    protected $hidden = ['translations','created_at','updated_at'];

    public function OrderItem()
    {
        return $this->belongsTo(OrderItem::class,'order_item_id');
    }
}
