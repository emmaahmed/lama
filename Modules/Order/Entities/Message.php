<?php

namespace Modules\Order\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Message extends Authenticatable
{
    use Notifiable;
    //use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'name', 'email', 'password','phone','jwt','token','active',
        'lat','lng','image','is_captin','gender','dateOfBirth','points','busy'
    ];*/

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')
            ->select('id','name','phone','lat','lng','image');
    }






}
