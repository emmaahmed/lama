<?php

namespace Modules\Order\Entities;

use App\User;
use App\UserAddress;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Shop\Entities\Shop;

class Order extends Model
{
    protected $fillable = ['order_number','user_id','delegate_id','shop_id',
        'address_id','total_cost','status','notes',
        'payment','delivery_time','delivery_type'];
protected $appends=['shop_name'];
    //status=> 0=waiting_accept , 1=accept , 2=on_way , 3=delivered , 4=cancelled

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function delegate()
    {
        return $this->belongsTo(User::class,'delegate_id');
    }
    public function shop()
    {
        return $this->belongsTo(Shop::class,'shop_id');
    }
    public function address()
    {
        return $this->belongsTo(UserAddress::class,'address_id');
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function itemVariations()
    {
        return $this->hasManyThrough(OrdItemVar::class, OrderItem::class);
    }

    public function getCreatedAtAttribute()
    {
        //return Carbon::parse($this->attributes['created_at'])->toDateString();
        return Carbon::parse($this->attributes['created_at'])->format('Y-m-d g:i A');
    }

    public function getCreatedAtTimeAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->format('Y-m-d g:i A');
    }

    public function getShopNameAttribute()
    {
        return $this->shop()->first()?$this->shop()->first()->name:'';
    }


    public static function getTableName()
    {
        return (new self())->getTable();
    }
}
