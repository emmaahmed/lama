<?php

namespace Modules\Order\Entities;

use App\User;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserDelegateMessage extends Authenticatable
{
    use Notifiable;
    //use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sender_id', 'receiver_id','delegate_id', 'message'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')
            ->select('id','name','phone','lat','lng','image');
    }
    public function delegate(){
        return $this->belongsTo(User::class,'delegate_id','id');
    }


    public function getCreatedAtAttribute()
    {
        return  Carbon::parse($this->attributes['created_at'])->format('y-m-d H:i:s');

    }




}
