<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Product;

class OrderItem extends Model
{
    protected $fillable = ['order_id','product_id','quantity','cost','description'];

    public function order()
    {
        return $this->belongsTo(Order::class,'order_id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function ordItemVars()
    {
        return $this->hasMany(OrdItemVar::class,'order_item_id');
    }
}
