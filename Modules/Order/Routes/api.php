<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/order', function (Request $request) {
    return $request->user();
});
Route::middleware('checkDatabase')->group(function () {

Route::namespace('Api')->group(function(){
    Route::get('/get-delivery-fees', 'OrderController@getDeliveryFees');
    Route::post('/create-order', 'OrderController@createOrder');
    Route::get('/order-details', 'OrderController@orderDetails');
    Route::get('/get-orders', 'OrderController@getOrders');
    Route::post('/cancel-order', 'OrderController@cancelOrder');
    Route::post('/rate-order', 'OrderController@rateOrder');
    Route::get('/chat_history','OrderController@chatHistory');
    Route::post('/add_message','OrderController@addMessage');
    Route::get('/shop_chat_history','OrderController@shopChatHistory');
    Route::post('/add_message_shop','OrderController@addMessageShop');
    Route::get('/chat_details','OrderController@chatDetails');

});


Route::namespace('Api\Delegate')->prefix('delegate')->group(function(){
    Route::get('/pending-orders', 'OrderController@getOrders');
    Route::post('/order-action', 'OrderController@orderAction');
    Route::get('/myOrders', 'OrderController@myOrders');
});
});
