<?php

namespace Modules\Order\Repositories\Interfaces;

interface OrderRepositoryInterface
{
    public function getDeliveryFees($shop_id);

    public function createOrder($request, $user_id);

    public function orderDetails($order_id);

    public function getOrders($user_id = null,$shop_id = null);
    public function finishedOrders($user_id = null,$shop_id = null);
    public function unfinishedOrders($user_id = null,$shop_id = null);

    public function cancelOrder($order_id,$type);
    public function rateOrder($request);

    // Delegate

    public function getPendingOrders($delegate);

    public function orderAction($request);

    public function myOrders($delegate_id);

    // Admin

    public function changeStatus($request);

    public function filterOrders($request);

    public function showProducts($request);

    public function chatHistory($request,$user_id,$lang);
    public function addMessage($request,$user_id,$lang);
    public function shopChatHistory($request,$user_id,$lang);
    public function addMessageShop($request,$user_id,$lang);
    public function chatDetails($request,$user_id,$type,$lang);
}
