<?php


namespace Modules\Order\Repositories;


use App\Notification;
use App\User;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Entities\AdminNotification;
use Modules\Cart\Entities\Cart;
use Modules\Cart\Repositories\Interfaces\CartRepositoryInterface;
use Modules\Order\Entities\BlockedOrder;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\UserDelegateMessage;
use Modules\Order\Entities\UserShopMessage;
use Modules\Order\Repositories\Interfaces\OrderRepositoryInterface;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\Variation;
use Modules\Setting\Entities\Setting;
use Modules\Shop\Entities\Shop;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use Psy\Util\Str;
use DB;

class OrderRepository implements OrderRepositoryInterface
{
    protected $cartRepository;

    public function __construct(CartRepositoryInterface $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function getDeliveryFees($shop_id)
    {
        $shop = Shop::where('id', $shop_id)->first();
        $settings = Setting::first();
        $data['normal'] = $shop ? $shop->delivery_fees : 0;
        $data['fast'] = $settings ? $settings->fast_delivery_fees : 0;
        return $data;
    }

    public function createOrder($request, $user_id)
    {
        $cart = Cart::where('user_id', $user_id)->first();
        $cart_array = $this->cartRepository->getCartItems($cart);
        $order = Order::create([
            /*'order_number' => '#'. \Illuminate\Support\Str::random(8),*/
            'order_number' => '#' . $user_id . rand(1000, 9999),
            'user_id' => $user_id,
            'shop_id' => $cart_array['shop_id'],
            'address_id' => $request->address_id,
            'delivery_type' => $request->delivery_type,
            'delivery_time' => $request->delivery_time,
            'payment' => $request->payment,
            'notes' => $request->notes,
        ]);
        if(count($cart_array['not_available_products'])>0){
            return $cart_array['not_available_products'][0];
        }
        foreach ($cart_array['items'] as $item) {
            $order_item = $order->orderItems()->create([
                'product_id' => $item['item_id'],
                'quantity' => $item['item_quantity'],
                'cost' => $item['item_price'],
                'description' => isset($item['description']) ? $item['description'] : ""
            ]);
            $product = Product::where('id', $item['item_id'])->first();
            $product->selling += $item['item_quantity'];
            $product->quantity -= $item['item_quantity'];
            $product->save();
            if (isset($item['variations'])) {
                foreach ($item['variations'] as $variation) {
                    foreach ($variation['options'] as $option) {
                        $order_item_variation = $order_item->ordItemVars()->create([
                            'price' => $option['price']
                        ]);
//                        $order_item_variation->translateOrNew('en')->variation_name = $variation['name'];
                        $order_item_variation->translateOrNew('ar')->variation_name = $variation['name'];
//                        $order_item_variation->translateOrNew('en')->option_name = $option['name'];
                        $order_item_variation->translateOrNew('ar')->option_name = $option['name'];
                        $order_item_variation->save();
                    }
                }
            }
            $order_item->cost = $item['item_price'];
            $order_item->save();
        }
        $order->total_cost = $cart_array['total_cost'];
//        dd($order);
        if ($order->save()) {
            $cart->delete();
            // send notifications to all delegates if order delivery type is fase
//            if ($order->delivery_type == 1){
//                $delegates_firebase_tokens = User::where('type', 1)->pluck('firebase_token')->toArray();
//                send_to_user($delegates_firebase_tokens,'تم إنشاء طلب جديد' ,'1',$order->shop_id,$order->id);
//            }
            $shop=Shop::whereId($cart_array['shop_id'])->first();
            $from='lama@emails.mobile-app-company.com';
            $jsonurl = 'http://emails.mobile-app-company.com/index-order.php?' . "email=" . $shop->email  . "&from=" . $from . "&id=". $order->id;
            $json = file_get_contents($jsonurl);
            $notification = new Notification();
            $notification->type = -1;
            $notification->sender_type = 2;
            $notification->shop_id = $shop->id;
            $notification->order_id = $order->id;
            $notification->save();
            $notification->translateOrNew('en')->title = 'New Order';
            $notification->translateOrNew('ar')->title = 'لديك طلب جديد';
            $notification->translateOrNew('en')->body = 'New Order';
            $notification->translateOrNew('ar')->body = 'لديك طلب جديد';
            $notification->save();

            sendOrderNotification($cart_array['shop_id'],$order->id);
            send_to_shop([$shop->firebase_token],' لديك  طلب جديد' ,3,$order->shop_id,$order->id,$shop,$shop->platform);
            $pdfFilePath = public_path('uploads/invoices/orders-'.$order->order_number.'.pdf');
            $input = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=".route('shop.orders.pdf',$order->id);
            $output = public_path($order->order_number.'.png');
            file_put_contents($output, file_get_contents($input));
            if($order->delivery_type == 0){
                $fees=$order->shop->delivery_fees;
            }
            elseif($order->delivery_type == 1){
                $fees=getFastShippingFees();
            }
            else{
                $fees=0;
            }

            $pdf=PDF::loadView('invoice-pdf', compact('order','fees','output'));
            $pdf->save($pdfFilePath);

            return $order;
        }
        return false;
    }

    public function orderDetails($order_id)
    {
        $order = Order::where('id', $order_id)->first();
        if($order->delivery_type == 0){
            $fees=$order->shop->delivery_fees;
        }
        elseif($order->delivery_type == 1){
            $fees=getFastShippingFees();
        }
        else{
            $fees=0;
        }
        $order_details = [];
        $order_details['id'] = $order->id;
        $order_details['order_number'] = $order->order_number;
        $order_details['order_status'] = $order->status;
        $order_details['total_cost'] = strval($order->total_cost + ($fees));
        $order_details['delivery_type'] = $order->delivery_type;
        // $order_details['delivery_time'] = $order->shop ? $order->shop->delivery_time : "";
        $order_details['delivery_time'] = $order->delivery_time ? $order->delivery_time : $order->shop->delivery_time;
        $order_details['shipping_fees'] = $fees;
        $order_details['payment_type'] = $order->payment;
        $order_details['user_id'] = isset($order->user_id) ? $order->user_id : 0;
        $order_details['user_name'] = isset($order->user) ? $order->user->name : "";
        $order_details['user_phone'] = isset($order->user) ? $order->user->phone : "";
        $order_details['delegate_id'] = isset($order->delegate_id) ? $order->delegate_id : 0;
        $order_details['delegate_phone'] = isset($order->delegate) ? $order->delegate->phone : "";
        $order_details['latitude'] = isset($order->address) ? $order->address->latitude : "";
        $order_details['longitude'] = isset($order->address) ? $order->address->longitude : "";
        $order_details['address'] = isset($order->address) ? $order->address->address : "";
        $order_details['notes'] = isset($order->notes) ? $order->notes : "";
        $order_details['created_at'] = $order->created_at;
        $order_details['is_rated_shop'] = $order->shop_rate ? 1 : 0;
        $order_details['is_rated_user'] = $order->user_rate ? 1 : 0;
        $order_details['is_rated_delegate'] = $order->delegate_rate ? 1 : 0;

        $order_details['items'] = [];

        if (sizeof($order->orderItems) > 0) {
            foreach ($order->orderItems as $key => $order_item) {
                $order_details['items'][$key]['item_id'] = $order_item->product_id;
                $order_details['items'][$key]['item_quantity'] = $order_item->quantity;
                $order_details['items'][$key]['item_name'] = $order_item->product->name;
                $order_details['items'][$key]['item_image'] = $order_item->product->image;
                $order_details['items'][$key]['item_cost'] = $order_item->cost;
                $order_details['items'][$key]['description'] = $order_item->description;
                $shop=Shop::whereId($order_item->product->shop_id)->first();
                $order_details['shop_lat'] = $shop->latitude;
                $order_details['shop_lng'] = $shop->longitude;
                $order_details['items'][$key]['shop_id'] = $shop->id;
                $order_details['items'][$key]['variations'] = [];
                if (sizeof($order_item->ordItemVars) > 0) {
                    foreach ($order_item->ordItemVars as $key2 => $variation) {
                        $order_details['items'][$key]['variations'][$variation->variation_name]['id'] = $variation->id;
                        $order_details['items'][$key]['variations'][$variation->variation_name]['name'] = $variation->variation_name;
                        $order_details['items'][$key]['variations'][$variation->variation_name]['options'][]['name'] = $variation->option_name;
                    }
                    $order_details['items'][$key]['variations'][$variation->variation_name]['options'] = array_values($order_details['items'][$key]['variations'][$variation->variation_name]['options']);
                }
                $order_details['items'][$key]['variations'] = array_values($order_details['items'][$key]['variations']);
            }
        }
        return $order_details;
    }

    public function getOrders($user_id = null, $shop_id = null)
    {
        if (!empty($user_id) && $user_id != 0) {
            $data = Order::where('user_id', $user_id)->select('id', 'order_number', 'total_cost', 'status','delivery_type','shop_id','created_at')->orderBy('id', 'DESC')->paginate(10);

        } elseif (!empty($shop_id)) {
            $data['title'] = "طلبات المستخدمين";
            $data['orders'] = Order::where('shop_id', $shop_id)->orderBy('id', 'DESC')->get();
        } else {
            $data['title'] = "الطلبات";
            $data['orders'] = Order::orderBy('id', 'DESC')->get();
        }

        return $data;
    }

    public function finishedOrders($user_id = null, $shop_id = null)
    {
        if (!empty($user_id) && $user_id != 0) {
            $data = Order::where('user_id', $user_id)
                ->select('id', 'order_number', 'total_cost', 'status', 'created_at')
                ->orderBy('id', 'DESC')
                ->where('status', 3)
                ->paginate(10);
        } elseif (!empty($shop_id)) {
            $data['title'] = " الطلبات المنفذة";
            $data['orders'] = Order::where('shop_id', $shop_id)
                ->where('status', 3)
                ->orderBy('id', 'DESC')->get();
        } else {
            $data['title'] = " الطلبات المنفذة";
            $data['orders'] = Order::orderBy('id', 'DESC')->where('status', 3)->get();
        }

        return $data;
    }

    public function unfinishedOrders($user_id = null, $shop_id = null)
    {
        if (!empty($user_id) && $user_id != 0) {
            $data = Order::where('user_id', $user_id)
                ->select('id', 'order_number', 'total_cost', 'status', 'created_at')
                ->where('status', '<', 3)
                ->orderBy('id', 'DESC')->paginate(10);
        } elseif (!empty($shop_id)) {
            $data['title'] = "الطلبات الغير المنفذة";
            $data['orders'] = Order::where('shop_id', $shop_id)
                ->where('status', '<', 3)
                ->orderBy('id', 'DESC')->get();
        } else {
            $data['title'] = "الطلبات الغير المنفذة";
            $data['orders'] = Order::orderBy('id', 'DESC')
                ->where('status', '<', 3)
                ->get();
        }

        return $data;
    }

    public function cancelOrder($order_id,$type)
    {
        $order = Order::where('id', $order_id)->first();
        if ($order->status != 0) {
            return false;
        }

        if($type==0) {
            $order->status = -1;

        }
        else{
            $order->status = 4;

        }
        return $order->save();

    }

    public function rateOrder($request)
    {
        $order = Order::where('id', $request->order_id)->first();
        if ($request->has('shop_rate') OR $request->has('shop_comment') OR $request->has('user_rate') OR $request->has('user_comment')) {
            $order->shop_rate = $request->shop_rate;
            $order->shop_comment = $request->shop_comment;
            $order->user_rate = $request->user_rate;
            $order->user_comment = $request->user_comment;

            return $order->save();////////////
        }
        if ($request->has('delegate_rate') OR $request->has('delegate_comment')) {
            $order->delegate_rate = $request->delegate_rate;
            $order->delegate_comment = $request->delegate_comment;
            return $order->save();////////////
        }

        return false;
    }


    // Delegate

    public function getPendingOrders($delegate)
    {
        if($delegate->status!=1) {
            return Order::where(['status' => 15])->paginate(10);

        }
        $blocked_orders_ids = $delegate->blocked_orders()->pluck('order_id')->toArray();
        $data = Order::where(['status' => 5, 'delivery_type' => 1])->whereNotIn('id', $blocked_orders_ids)->select('id', 'shop_id', 'order_number', 'total_cost', 'status', 'created_at')->orderBy('id', 'DESC')->paginate(10);
        foreach ($data as $order) {
            $order->shop_name = $order->shop->name;
            $order->shop_logo = $order->shop->logo;
            unset($order->shop);
        }
        return $data;
    }

    public function orderAction($request)
    {
        $order = Order::where('id', $request->order_id)->where(function ($query) use ($request) {
            $query->where('status', 5)
                ->orWhere('delegate_id', $request->delegate_id);
        })->first();
        $delegate =User::whereId($request->delegate_id)->first();

        if ($order) {
            if ($request->status == 1 && $order->status == 5) {
                $order->status = 1;
                $order->delegate_id = $request->delegate_id;
                $order->save();
                BlockedOrder::where('order_id', $request->order_id)->delete();
                // order accepted
                $shop=Shop::whereId($order->shop_id)->first();
//                dd($shop);
                unset($shop->images);
               send_to_user([$order->user->firebase_token], 'تم الموافقة علي طلبك بنجاح', '2', $order->shop_id, $order->id,$shop,$order->user->platform);
                $notification = Notification::create([
                    'user_id' => $order->user->id,
                    'delegate_id' => $order->delegate_id,
                    'order_id' => $order->id,
                ]);
                $notification->translateOrNew('ar')->title = $shop->name;
                $notification->translateOrNew('en')->title = $shop->name;
                $notification->translateOrNew('ar')->body = 'تم الموافقة علي الطلب بنجاح';
                $notification->translateOrNew('en')->body = 'تم الموافقة علي الطلب بنجاح';
                $notification->save();

                $notification = new Notification();
                $notification->type = -1;
                $notification->sender_type = 2;
                $notification->shop_id = $shop->id;
                $notification->order_id = $order->id;
                $notification->save();
                $notification->translateOrNew('en')->title = 'Order Accepted by delegate '.$delegate->name;
                $notification->translateOrNew('ar')->title = $delegate->name.'تم الموافقة علي الطلب بنجاح من المندوب ';
                $notification->translateOrNew('en')->body = 'Order Accepted by delegate';
                $notification->translateOrNew('ar')->body = 'تم الموافقة علي الطلب بنجاح';
                $notification->save();

                send_to_shop([$shop->firebase_token],$delegate->name.' تم الموافقة علي الطلب بنجاح من المندوب ' ,3,$order->shop_id,$order->id,$shop,$shop->platform);

                return 'order_accepted';
            }
            if ($request->status == 2 && $order->status == 1) {
                $order->status = 2;
                $order->delegate_id = $request->delegate_id;
                $order->save();
                // order on way
                $shop=Shop::whereId($order->shop_id)->first();
                send_to_user([$order->user->firebase_token], 'الطلب في الطريق', '3', $order->shop_id, $order->id,$shop,$order->user->platform);
                $notification = Notification::create([
                    'user_id' => $order->user->id,
                    'delegate_id' => $order->delegate_id,
                    'order_id' => $order->id,
                ]);
                $notification->translateOrNew('ar')->title = $shop->name;
                $notification->translateOrNew('en')->title = $shop->name;
                $notification->translateOrNew('ar')->body = 'الطلب في الطريق';
                $notification->translateOrNew('en')->body = 'الطلب في الطريق';
                $notification->save();
                $notification = new Notification();
                $notification->type = -1;
                $notification->sender_type = 2;
                $notification->shop_id = $shop->id;
                $notification->order_id = $order->id;
                $notification->save();
                $notification->translateOrNew('en')->title = 'Order Accepted by delegate '.$delegate->name;
                $notification->translateOrNew('ar')->title = 'المندوب في الطريق للعميل';
                $notification->translateOrNew('en')->body = 'Delegate is on the way to client';
                $notification->translateOrNew('ar')->body = 'المندوب في الطريق للعميل';
                $notification->save();

                send_to_shop([$shop->firebase_token],' المندوب في الطريق للعميل ' ,3,$order->shop_id,$order->id,$shop,$shop->platform);

                return 'on_way';
            }
            if ($request->status == 3 && $order->status == 2) {
                $order->status = 3;
                $order->delegate_id = $request->delegate_id;
                $order->save();
                // order arrived
                $shop=Shop::whereId($order->shop_id)->first();

                send_to_user([$order->user->firebase_token], 'تم التوصيل', '4', $order->shop_id, $order->id,$shop,$order->user->platform);
                $notification = Notification::create([
                    'user_id' => $order->user->id,
                    'delegate_id' => $order->delegate_id,
                    'order_id' => $order->id,
                ]);
                $notification->translateOrNew('ar')->title = $shop->name;
                $notification->translateOrNew('en')->title = $shop->name;
                $notification->translateOrNew('ar')->body = 'تم التوصيل';
                $notification->translateOrNew('en')->body = 'تم التوصيل';
                $notification->save();
                $notification = new Notification();
                $notification->type = -1;
                $notification->sender_type = 2;
                $notification->shop_id = $shop->id;
                $notification->order_id = $order->id;
                $notification->save();
                $notification->translateOrNew('en')->title = 'Order finished ';
                $notification->translateOrNew('ar')->title = 'تم توصيل الطلب بنجاح';
                $notification->translateOrNew('en')->body = 'Order finished';
                $notification->translateOrNew('ar')->body = 'تم توصيل الطلب بنجاح للعميل';
                $notification->save();

                send_to_shop([$shop->firebase_token],' المندوب في الطريق للعميل ' ,3,$order->shop_id,$order->id,$shop,$shop->platform);

                return 'arrived';
            }
            if ($request->status == 4 && $order->status == 5) {
                // block order
                BlockedOrder::create([
                    'order_id' => $order->id,
                    'delegate_id' => $request->delegate_id
                ]);
                $order->save();
                // order canceled
                $shop=Shop::whereId($order->shop_id)->first();

                send_to_user([$order->user->firebase_token], 'تم إلغاء الطلب', '5', $order->shop_id, $order->id,$shop,$order->user->platform);
                $notification = Notification::create([
                    'user_id' => $order->user->id,
                    'delegate_id' => $order->delegate_id,
                    'order_id' => $order->id,
                ]);
                $notification->translateOrNew('ar')->title = $shop->name;
                $notification->translateOrNew('en')->title = $shop->name;
                $notification->translateOrNew('ar')->body = 'تم إلغاء الطلب';
                $notification->translateOrNew('en')->body = 'تم إلغاء الطلب';
                $notification->save();
                $notification = new Notification();
                $notification->type = -1;
                $notification->sender_type = 2;
                $notification->shop_id = $shop->id;
                $notification->order_id = $order->id;
                $notification->save();
                $notification->translateOrNew('en')->title = 'Order Cancelled ';
                $notification->translateOrNew('ar')->title = 'تم إلغاء الطلب من المندوب';
                $notification->translateOrNew('en')->body = 'Order Cancelled';
                $notification->translateOrNew('ar')->body = 'تم إلغاء الطلب من المندوب';
                $notification->save();

                send_to_shop([$shop->firebase_token],' تم إلغاء الطلب من المندوب ' ,3,$order->shop_id,$order->id,$shop,$shop->platform);

                return 'canceled';
            }
            return false;
        }
        return false;
    }


    public function myOrders($delegate_id)
    {
        $data = Order::where('delegate_id', $delegate_id)->select('id', 'shop_id', 'order_number', 'total_cost', 'status', 'created_at')->orderBy('id', 'DESC')->paginate(10);
        foreach ($data as $order) {
            $order->shop_name = $order->shop->name;
            $order->shop_logo = $order->shop->logo;
            unset($order->shop);
        }
        return $data;
    }


    // Admin

    public function changeStatus($request)
    {
        $order = Order::where('id', $request->order_id)->where('shop_id', $request->shop_id)->first();
//        return $order->user->firebase_token;

        $order->status = $request->status;
        $shop=Shop::whereId($order->shop_id)->select('id','logo','expire_at')->first();
        unset($shop->images);

        if ($order->save()) {
            if($order->delivery_type==2) {
                if ($request->status == 1) {

                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز تم قبول طلبك', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => (Auth::guard('shop')->user()->parent_id == NULL) ? (Auth::guard('shop')->user()->id) : (Auth::guard('shop')->user()->parent_id), // shop
                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'تم قبول طلبك';
                    $notification->translateOrNew('en')->body = 'تم قبول طلبك';
                    $notification->save();
                }
                if ($request->status == 2) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز لقد تم تجهيز الطلب', '1', $order->shop_id, $order->id, $sho,$order->user->platformp);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => (Auth::guard('shop')->user()->parent_id == NULL) ? (Auth::guard('shop')->user()->id) : (Auth::guard('shop')->user()->parent_id), // shop

                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'عميلنا العزيز تم تجهيز طلبك ';
                    $notification->translateOrNew('en')->body = 'عميلنا العزيز تم تجهيز طلبك ';
                    $notification->save();
                }
                if ($request->status == 3) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز لقد تم استلام الطلب', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => (Auth::guard('shop')->user()->parent_id == NULL) ? (Auth::guard('shop')->user()->id) : (Auth::guard('shop')->user()->parent_id), // shop

                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'عميلنا العزيز تم إستلام طلبك ';
                    $notification->translateOrNew('en')->body = 'عميلنا العزيز تم إستلام طلبك ';
                    $notification->save();
                }
                if ($request->status == 4) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز نأسف لإبلاغك بأنه تم إلغاء طلبك', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => (Auth::guard('shop')->user()->parent_id == NULL) ? (Auth::guard('shop')->user()->id) : (Auth::guard('shop')->user()->parent_id), // shop

                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'عميلنا العزيز نأسف لإبلاغك بأنه تم إلغاء طلبك';
                    $notification->translateOrNew('en')->body = 'عميلنا العزيز نأسف لإبلاغك بأنه تم إلغاء طلبك';
                    $notification->save();
                }
            }
            else{
                if ($request->status == 1) {
                   send_to_user([$order->user->firebase_token], 'عميلنا العزيز تم قبول طلبك', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => (Auth::guard('shop')->user()->parent_id == NULL) ? (Auth::guard('shop')->user()->id) : (Auth::guard('shop')->user()->parent_id), // shop
                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'تم قبول طلبك';
                    $notification->translateOrNew('en')->body = 'تم قبول طلبك';
                    $notification->save();
                }
                if ($request->status == 2) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز طلبك في الطريق', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => (Auth::guard('shop')->user()->parent_id == NULL) ? (Auth::guard('shop')->user()->id) : (Auth::guard('shop')->user()->parent_id), // shop

                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'عميلنا العزيز طلبك في الطريق ';
                    $notification->translateOrNew('en')->body = 'عميلنا العزيز طلبك في الطريق ';
                    $notification->save();
                }
                if ($request->status == 3) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز تم إنهاء طلبك بنجاح', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => (Auth::guard('shop')->user()->parent_id == NULL) ? (Auth::guard('shop')->user()->id) : (Auth::guard('shop')->user()->parent_id), // shop

                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'عميلنا العزيز تم إنهاء طلبك بنجاح';
                    $notification->translateOrNew('en')->body = 'عميلنا العزيز تم إنهاء طلبك بنجاح';
                    $notification->save();

                }
                if ($request->status == 4) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز نأسف لإبلاغك بأنه تم إلغاء طلبك', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => (Auth::guard('shop')->user()->parent_id == NULL) ? (Auth::guard('shop')->user()->id) : (Auth::guard('shop')->user()->parent_id), // shop

                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'عميلنا العزيز نأسف لإبلاغك بأنه تم إلغاء طلبك';
                    $notification->translateOrNew('en')->body = 'عميلنا العزيز نأسف لإبلاغك بأنه تم إلغاء طلبك';
                    $notification->save();
                }

            }

            return true;
        } else {
            return false;
        }
    }

    public function filterOrders($request)
    {
        if($request->shop_id!=null){
                                $builder = Order::where('shop_id', $request->shop_id);

        }
        else{
                    $builder = Order::orderBy('id', 'DESC');

        }
        if (isset($request->from) && isset($request->to)) {
            $builder->where('created_at', '>=', $request->from)->where('created_at', '<=', $request->to);
        }
        $orders = $builder->paginate(10);
        $data['orders'] = $orders;
        return $data;
    }

    public function showProducts($request)
    {
        $data['title'] = 'تفاصيل الطلب';
        $order = Order::where('id', $request->order_id)->first();
        $settings=Setting::select('fast_delivery_fees')->first();
        $order['fast_delivery']=$settings->fast_delivery_fees;
        $data['order'] = $order;
        return $data;
    }

    public function editRate($request)
    {
        $order = Order::where('id', $request->order_id)->update($request->except('_token', 'order_id'));
        return $order;
    }

//     public function chatHistory($input, $user_id, $lang)
//     {
//         $users = UserDelegateMessage::where('sender_id', $user_id)
//             ->orWhere('receiver_id', $user_id)
// //            ->select('delegate_id','receiver_id')
// //
// //            ->groupBy('delegate_id')
//             ->get();
//             $userSelected=User::whereId($user_id)->first();
//         foreach ($users as $user) {
//             $delegate = User::where('id', $user->delegate_id)->first();
//             if(!$delegate){
//                 continue;
//             }
//             $delegate_id = $delegate->id;
// //            $user->id = $delegate->id;
//             if($userSelected->type=='0'){
//                 $user['user_id'] = $user_id;
//                 $user['user_name'] = $delegate->name;
//                 $user['user_image'] = $delegate->image;

//             }
//             else{
//                 $userReceiver=User::whereId($user->receiver_id)->first();
//                 $user['user_id'] = $user->receiver_id;
//                 $user['user_name'] = $userReceiver->name;
//                 $user['user_image'] = $userReceiver->image;

//             }
// //            $user->name = $delegate->name;
// //            $user->image = $delegate->image;
//             $user->email = $delegate->email;
//             $user->phone = $delegate->phone;
//             $message = DB::select("
//                 SELECT message,created_at as message_created_at
//                 FROM user_delegate_messages
//                 Where
//                 (sender_id = $user_id and delegate_id = $delegate_id)
//                 OR
//                 (receiver_id = $user_id and delegate_id = $delegate_id)
//                 Order By id desc limit 1
//             ");
//             $user->message = $message[0]->message;
//             $user->message_created_at = $message[0]->message_created_at;
//         }
//         return $users;
//     }

    public function chatHistory($input, $user_id, $lang)
    {


        $user = User::whereId($user_id)->first();


        $users = UserDelegateMessage::where('sender_id', $user_id)
            ->orWhere('receiver_id', $user_id)
            ->select('delegate_id')
           ->whereHas('delegate')

           ->groupBy('delegate_id')

            ->get();

            if($user->type == 1){
                $data=array();
                 $senders = UserDelegateMessage::where('delegate_id', $user_id)
                     ->where('receiver_id', $user_id)->orwhere('sender_id', $user_id)

                ->get()->where('sender_id','!=',$user_id)->pluck('sender_id')->toArray();
                 $receivers = UserDelegateMessage::where('delegate_id', $user_id)
                     ->where('receiver_id', $user_id)->orwhere('sender_id', $user_id)

                ->get()->where('receiver_id','!=',$user_id)->pluck('receiver_id')->toArray();
               $ids= array_merge($senders,$receivers);
                $ids=array_unique($ids);
                foreach ($ids as $id) {
                    $lastMessage=UserDelegateMessage::where(function ($query) use ($id) {
                        $query->where('receiver_id', $id)->orwhere('sender_id', $id);
                    })->latest()->first();
                        $receiver = User::whereId($id)->first();
                   if($receiver) {
                       $lastMessage['user_name'] = $receiver->name;
                       $lastMessage['user_image'] = $receiver->image;
                       $lastMessage['message'] = $lastMessage->message;
                       $lastMessage['receiver_id'] = $id;
                       $lastMessage['sender_id'] = $user_id;
                       $lastMessage['message_created_at'] = $lastMessage->created_at;


                       array_push($data, $lastMessage);
                   }


                }

                return $data;
//                foreach ($users as $user) {
//                    $user_to_call = $user->receiver_id;
//                $delegate_id =$user_id;
//                $user2 = DB::select("
//                SELECT message,created_at as message_created_at
//                ,receiver_id,sender_id
//                FROM user_delegate_messages
//                Where
//                (
//                (sender_id = $user_to_call and delegate_id = $delegate_id)
//                OR
//                (receiver_id = $user_to_call and delegate_id = $delegate_id)
//                )
//                AND (delegate_id = $delegate_id)
//                Order By id desc
//                ");
//                return $user2;
//                $user->message = $user2[0]->message;
//            $user->message_created_at = $user2[0]->message_created_at;
//                global $user3;
//                if($user->type == 0){
//                    if($delegate_id == $user2[0]->receiver_id){
//
//                        if($input->type== 0){
//                        $user3 = User::where('id', $user2[0]->receiver_id)->first();
//                        }else {
//                        $user3 = User::where('id', $user2[0]->sender_id)->first();
//                        }
//
//                    }else{
//                         if($input->type== 0){
//                        $user3 = User::where('id', $user2[0]->sender_id)->first();
//                        }else {
//                        $user3 = User::where('id', $user2[0]->receiver_id)->first();
//                        }
//                    }
//                     return $user3;
//                    $user['user_id'] = $user3->id;
//                    $user['user_name'] = $user3->name;
//                    $user['user_image'] = $user3->image;
//
//                       //return $user2[0];
//
//                }else{
//
//                    $user['user_id'] = $user3->id;
//                    $user['user_name'] = $user3->name;
//                    $user['user_image'] = $user3->image;
//                }
//                // $user->message = $user2->message;
//                // $user->message_created_at = $user2->created_at;
//
//                $delegate = User::whereId($user->delegate_id)->first();
//                //dd($delegate);
//                // $user->email = $delegate->email;
//                // $user->phone = $delegate->phone;
//            }

//                return $users;
            }
            else{
//                return $users;
                foreach ($users as $user) {
                $delegate_id =$user->delegate_id;
                $user2 = DB::select("
                SELECT message,created_at as message_created_at
                ,receiver_id,sender_id
                FROM user_delegate_messages
                Where
                (
                (sender_id = $user_id and delegate_id = $delegate_id)
                OR
                (receiver_id = $user_id and delegate_id = $delegate_id)
                )
                AND (delegate_id = $delegate_id)
                Order By id desc limit 1
                ");
                $user->message = $user2[0]->message;
            $user->message_created_at = $user2[0]->message_created_at;
                global $user3;
                if($user->type == 0){
                    if($delegate_id == $user2[0]->receiver_id){

                        if($input->type== 0){
                        $user3 = User::where('id', $user2[0]->receiver_id)->first();
                        }else {
                        $user3 = User::where('id', $user2[0]->sender_id)->first();
                        }

                    }else{
                         if($input->type== 0){
                        $user3 = User::where('id', $user2[0]->sender_id)->first();
                        }else {
                        $user3 = User::where('id', $user2[0]->receiver_id)->first();
                        }
                    }
                    if($user3) {

                        $user['user_id'] = $user3->id;
                        $user['user_name'] = $user3->name;
                        $user['user_image'] = $user3->image;
                    }

                       //return $user2[0];

                }else{
                    if($user3) {

                        $user['user_id'] = $user3->id;
                        $user['user_name'] = $user3->name;
                        $user['user_image'] = $user3->image;
                    }
                }
                // $user->message = $user2->message;
                // $user->message_created_at = $user2->created_at;

                $delegate = User::whereId($user->delegate_id)->first();
                //dd($delegate);
                // $user->email = $delegate->email;
                // $user->phone = $delegate->phone;
            }
            }



            return $users;
    }


    public function addMessage($input, $user_id, $lang)
    {

        if($user_id == $input->receiver_id)
            return false;

        $user = User::where('id', $user_id)->first();



        $add = new UserDelegateMessage();
        $add->sender_id = $user_id;
        $add->receiver_id = $input->receiver_id;
        $add->delegate_id = $user->type == 0 ? $input->receiver_id : $user_id;
        $add->message = isset($input->message) ? $input->message : '-';
        $add->save();



         if ($lang == "en") {
            $title = 'New message '.$user->name;
        } else {
           $title = ' رسالة جديدة من  '.$user->name;
        }

        $receiver = User::where('id', $input->receiver_id)->first();
        send_to_user($receiver->firebase_token, $title, 80, $user_id, "",$user,$receiver->platform);


        $message = UserDelegateMessage::orderBy("id", "desc")
            ->where("sender_id", $user_id)
            ->first();


        $user = User::where("id", $input->receiver_id)->first();

//        Notification::send(
//            "$user->token",
//            $title ,
//            $input->message ,
//            "" ,
//            $input->is_captin,
//            null,
//            $message
//        );

    }

    public function shopChatHistory($input, $user_id, $lang)
    {
//        $messages=UserShopMessage::where("sender_id",$user_id)
//            ->orWhere("receiver_id",$user_id)->get();
        $user=User::whereId($user_id)->first();

        $sender_type=$user->type==0?2:0;
        $users = DB::select("
                SELECT shop_id
                FROM user_shop_messages
                Where
                (sender_id = $user_id and sender_type = $sender_type)
                OR
                (receiver_id = $user_id and receiver_type = 2 )
                group by shop_id
            ");
        $messages=[];
        foreach ($users as $user) {
            $shop = Shop::where('id', $user->shop_id)->first();
            if ($shop && $user->shop_id!=0) {

                $shop_id = $shop->id;
                $user->id = $shop->id;
                $user->name = $shop->name;
                $user->user_id = $shop->id;
                $user->user_name = $shop->name;
                $user->user_img = $shop->logo;
                $user->logo = $shop->logo;
                $user->email = $shop->email;
                $user->phone = $shop->phone;
                $message = DB::select("
                SELECT message,created_at as message_created_at
                FROM user_shop_messages
                Where
                (sender_id = $user_id and shop_id = $shop_id)
                OR
                (receiver_id = $user_id and shop_id = $shop_id)
                Order By id desc limit 1
            ");
                $user->message = $message[0]->message;
                $user->message_created_at = $message[0]->message_created_at;
                $messages[]=$user;
            }

        }
        return $messages;
    }

    public function addMessageShop($input, $user_id, $lang)
    {
        $user=User::whereId($user_id)->first();

        if ($lang == "en") {
            $title = 'New message ';
        } else {
            $title = ' رسالة جديدة ';
        }

        $add = new UserShopMessage();
        $add->sender_id = $user_id;
        $add->sender_type = $user->type==0?2:0;
        $add->receiver_id = $input->receiver_id;
        $add->receiver_type = 1;
        $add->shop_id = $input->receiver_id;
        $add->message = isset($input->message) ? $input->message : '-';
        $add->save();

        $message = UserShopMessage::orderBy("id", "desc")
            ->where("sender_id", $user_id)
            ->where("sender_type", 0)
            ->first();


        $shop = Shop::where("id", $input->receiver_id)->first();
            $type='user-message';


        sendMessageNotification($input->receiver_id,$user_id,$user->name,$input->message,$type,  $user->image);
        send_to_shop([$shop->firebase_token],$input->message ,2,$user_id,"",$user,$shop->platform);

        AdminNotification::create([
            'shop_id' => $input->receiver_id,
            'message' => $input->message,
            'type' => 3,  // Shop
            'user_id'=>$user_id,
            'sender_type'=>2
        ]);



    }

    public function chatDetails($input, $user_id, $type, $lang)
    {
        global $messages;
        //

        if ($input->type == 0) {
            //chat user with delegate
            $delegate_id = $input->delegate_id;
//            dd('test');

            if ($type == 0) {//user
                $messages = DB::select("
                SELECT *
                FROM user_delegate_messages
                Where
                (sender_id = $user_id and delegate_id = $delegate_id)
                OR
                (receiver_id = $user_id and delegate_id = $delegate_id)
                Order By id asc
            ");
            }
            else {//delegate
                $messages = DB::select("
                SELECT *
                FROM user_delegate_messages
                Where
                (sender_id = $delegate_id and delegate_id = $user_id)
                OR
                (receiver_id = $delegate_id and delegate_id = $user_id)
                Order By id asc
            ");
            }

            foreach ($messages as $message) {
                $message->sender_image = User::where('id', $user_id)->first()?User::where('id', $user_id)->first()->image:'';
                $message->receiver_image = User::where('id', $delegate_id)->first()?User::where('id', $delegate_id)->first()->image:'';
            }
        }
        elseif ($input->type == 1) {
            //chat user with shop
            $shop_id = $input->delegate_id;
            $messages = DB::select("
                SELECT *
                FROM user_shop_messages
                Where
                (sender_id = $user_id and shop_id = $shop_id)
                OR
                (receiver_id = $user_id and shop_id = $shop_id)
                Order By id asc
            ");
            foreach ($messages as $message) {
                $message->sender_image = User::where('id', $user_id)->first()->image;
                $message->receiver_image = Shop::where('id', $shop_id)->first()->logo;
            }
        }
        return $messages;

    }


}
