<?php

namespace Modules\Order\Http\Requests;

use App\Http\Requests\Request;

class OrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->is('api/get-delivery-fees')) {
            return $this->shopDeliveryFeesRules();
        }
        if($this->is('api/create-order')) {
            return $this->createOrderRules();
        }
        if($this->is('api/order-details')
            || $this->is('api/cancel-order')) {
            return $this->orderDetailsRules();
        }
        if($this->is('admin/orders/change-status')) {
            return $this->changeStatusRules();
        }
        if($this->is('admin/orders/filter-orders')) {
            return $this->filterOrdersRules();
        }
        if($this->is('admin/orders/show-details')
            || $this->is('admin/orders/delete')) {
            return $this->adminOrderDetailsRules();
        }
        if($this->is('admin/orders/delete-multi')) {
            return $this->deleteMultiRules();
        }




        ////////




        if($this->is('shop/orders/change-status')) {
            return $this->changeStatusRules();
        }
        if($this->is('shop/orders/filter-orders')) {
            return $this->filterOrdersRules();
        }
        if($this->is('shop/orders/show-details')
            || $this->is('shop/orders/delete')) {
            return $this->adminOrderDetailsRules();
        }
        if($this->is('shop/orders/delete-multi')) {
            return $this->deleteMultiRules();
        }




        ////////
//      Delegate Action
        if($this->is('api/delegate/order-action')) {
            return $this->orderActionRules();
        }
    }

    public function messages()
    {
        if($this->is('api/get-delivery-fees')) {
            return $this->shopDeliveryFeesMessages();
        }
        if($this->is('api/create-order')) {
            return $this->createOrderMessages();
        }
        if($this->is('api/order-details')
            || $this->is('api/cancel-order')) {
            return $this->orderDetailsMessages();
        }
        if($this->is('admin/orders/change-status')) {
            return $this->changeStatusMessages();
        }
        if($this->is('admin/orders/filter-orders')) {
            return $this->filterOrdersMessages();
        }
        if($this->is('admin/orders/show-details')
            || $this->is('admin/orders/delete')) {
            return $this->adminOrderDetailsMessages();
        }
        if($this->is('admin/orders/delete-multi')) {
            return $this->deleteMultiMessages();
        }

        //////



        if($this->is('shop/orders/change-status')) {
            return $this->changeStatusMessages();
        }
        if($this->is('shop/orders/filter-orders')) {
            return $this->filterOrdersMessages();
        }
        if($this->is('shop/orders/show-details')
            || $this->is('shop/orders/delete')) {
            return $this->adminOrderDetailsMessages();
        }
        if($this->is('shop/orders/delete-multi')) {
            return $this->deleteMultiMessages();
        }


        //////


        //      Delegate Action
        if($this->is('api/delegate/order-action')) {
            return $this->orderActionMessages();
        }
    }

    public function shopDeliveryFeesRules()
    {
        return [
            'shop_id' => 'required|exists:shops,id',
        ];
    }

    public function shopDeliveryFeesMessages()
    {
        return [
            'shop_id.required' => 'shop_required',
            'shop_id.exists' => 'shop_not_found',
        ];
    }

    public function createOrderRules()
    {
        return [
            'address_id' => 'required|exists:user_addresses,id',
            'delivery_type' => 'required|in:0,1,2',
            'payment' => 'required|in:0,1,2',
//            'cart' => 'present|array'
        ];
    }

    public function createOrderMessages()
    {
        return [
            'address_id.required' => 'address_required',
            'address_id.exists' => 'address_not_found',
            'delivery_type.required' => 'delivery_required',
            'delivery_type.in' => 'delivery_type_not_found',
            'payment.required' => 'payment_required',
            'payment.in' => 'payment_not_found',
//            'cart.present' => 'cart_not_found',
//            'cart.array' => 'cart_not_array',
        ];
    }

    public function orderDetailsRules()
    {
        return [
            'order_id' => 'required|exists:orders,id',
        ];
    }

    public function orderDetailsMessages()
    {
        return [
            'order_id.required' => 'order_id_required',
            'order_id.exists' => 'order_not_found',
        ];
    }

    public function changeStatusRules()
    {
        return [
            'order_id' => 'required|exists:orders,id',
            'status' => 'required|in:0,1,2,3,4',
        ];
    }

    public function changeStatusMessages()
    {
        return [
            'order_id.required' => 'من فضلك اختر الطلب',
            'order_id.exists' => 'الطلب غير موجود',
            'status.required' => 'من فضلك اختر الحالة',
            'status.in' => 'الحالة غير موجودة',
        ];
    }

    public function filterOrdersRules()
    {
        return [
            'from' => 'required',
            'to' => 'required',
        ];
    }

    public function filterOrdersMessages()
    {
        return [
            'from.required' => 'من فضلك اختر بداية التاريخ',
            'from.date' => 'صيغة التاريخ غير صحيحة',
            'to.required' => 'من فضلك اختر نهاية التاريخ',
            'to.date' => 'صيغة التاريخ غير صحيحة',
        ];
    }


    public function adminOrderDetailsRules()
    {
        return [
            'order_id' => 'required|exists:orders,id',
        ];
    }

    public function adminOrderDetailsMessages()
    {
        return [
            'order_id.required' => 'اختر الطلب',
            'order_id.exists' => 'الطلب غير موجود',
        ];
    }

    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:orders,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذا الطلب غير موجود',
        ];
    }

    public function orderActionRules()
    {
        return [
            'order_id' => 'required|exists:orders,id',
            'status' => 'required|in:1,2,3,4'
        ];
    }

    public function orderActionMessages()
    {
        return [
            'order_id.required' => 'order_id_required',
            'order_id.exists' => 'order_not_found',
            'status.required' => 'status_required',
            'status.in' => 'status_not_valid'
        ];
    }
}
