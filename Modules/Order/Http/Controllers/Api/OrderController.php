<?php

namespace Modules\Order\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Order\Http\Requests\OrderRequest;
use Modules\Order\Repositories\Interfaces\OrderRepositoryInterface;

class OrderController extends Controller
{
    protected $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function getDeliveryFees(OrderRequest $request)
    {
        $data = $this->orderRepository->getDeliveryFees($request->shop_id);
        return callback_data(success(), 'delivery_fees',$data);
    }

    public function createOrder(OrderRequest $request)
    {
//        return $request->all();
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $order = $this->orderRepository->createOrder($request, $user->id);

            if (isset($order->id)){
                return callback_data(success(), 'order_created',$order);
            }
            if(is_string($order)){
                return callback_data_cart(error(), 'product_quantity_unavailable',$order);

            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function orderDetails(OrderRequest $request)
    {
//        if (!request()->headers->has('jwt')){
//            return callback_data(error(),'check_jwt');
//        }
//        $user = checkJWT();
//        if ($user){
            $order = $this->orderRepository->orderDetails($request->order_id);
            if ($order){
                return callback_data(success(), 'order_details', $order);
            }
            return callback_data(error(), 'errors');
//        }else{
//            return callback_data(error(),'user_not_found');
//        }
    }

    public function getOrders()
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $orders = $this->orderRepository->getOrders($user->id);
            if ($orders){
                return callback_data(success(), 'my_orders', $orders);
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function cancelOrder(OrderRequest $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $cancelled = $this->orderRepository->cancelOrder($request->order_id,$user->type);
            if ($cancelled){
                return callback_data(success(), 'order_cancelled');
            }
            return callback_data(error(), 'order_can_not_be_cancelled');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function rateOrder(Request $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $rated = $this->orderRepository->rateOrder($request);
            if ($rated){
                return callback_data(success(), 'order_rated');
            }
            return callback_data(error(), 'order_can_not_be_rated');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function chatHistory(Request $request){
        if($user=checkJWT()){
            $data=$this->orderRepository->chatHistory($request,$user->id,$request->header('lang'));
            return callback_data(success(), 'success', $data);
        }
        else return callback_data(error(),'user_not_found');
    }

    public function addMessage(Request $request){
        if($user=checkJWT()){
            $data=$this->orderRepository->addMessage($request,$user->id,$request->header('lang'));
            return callback_data(success(),'success');
        }
        else return callback_data(error(),'user_not_found');
    }

    public function shopChatHistory(Request $request){
        if($user=checkJWT()){
            $data=$this->orderRepository->shopChatHistory($request,$user->id,$request->header('lang'));
            return callback_data(success(), 'success', $data);
        }
        else return callback_data(error(),'user_not_found');
    }

    public function addMessageShop(Request $request){
        if($user=checkJWT()){
            $data=$this->orderRepository->addMessageShop($request,$user->id,$request->header('lang'));
            return callback_data(success(),'success');
        }
        else return callback_data(error(),'user_not_found');
    }

    public function chatDetails(Request $request){
        if($user=checkJWT()){
            $data=$this->orderRepository->chatDetails($request,$user->id,$user->type,$request->header('lang'));
            return callback_data_chat(success(), 'success', $data);
        }
        else return callback_data(error(),'user_not_found');
    }

}
