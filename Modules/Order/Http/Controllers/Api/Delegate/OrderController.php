<?php

namespace Modules\Order\Http\Controllers\Api\Delegate;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Order\Http\Requests\OrderRequest;
use Modules\Order\Repositories\Interfaces\OrderRepositoryInterface;

class OrderController extends Controller
{
    protected $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function getOrders()
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $delegate = checkDelegateJWT();
        if ($delegate){
            $orders = $this->orderRepository->getPendingOrders($delegate);
            if ($orders){
                return callback_data(success(), 'orders', $orders);
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'delegate_not_found');
        }
    }

    public function orderAction(OrderRequest $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $delegate = checkDelegateJWT();
        if ($delegate){
//            $request->request->add(['delegate_id' => $delegate->id]);
            $request->merge(['delegate_id' => $delegate->id]);
//            return $request->all();
            $action = $this->orderRepository->orderAction($request);
            if ($action){
                return callback_data(success(), $action);
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'delegate_not_found');
        }
    }

    public function myOrders()
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $delegate = checkDelegateJWT();
        if ($delegate){
            $orders = $this->orderRepository->myOrders($delegate->id);
            if ($orders){
                return callback_data(success(), 'orders', $orders);
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'delegate_not_found');
        }
    }


}
