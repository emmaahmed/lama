<?php

namespace Modules\Shop\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Shop\Repositories\AdminRepository;
use Modules\Shop\Repositories\AppRepository;
use Modules\Shop\Repositories\AuthRepository;
use Modules\Shop\Repositories\Interfaces\AdminRepositoryInterface;
use Modules\Shop\Repositories\Interfaces\AppRepositoryInterface;
use Modules\Shop\Repositories\Interfaces\AuthRepositoryInterface;
use Modules\Shop\Repositories\Interfaces\ShopAdminRepositoryInterface;
use Modules\Shop\Repositories\Interfaces\ShopRepositoryInterface;
use Modules\Shop\Repositories\Interfaces\SubCategoryRepositoryInterface;
use Modules\Shop\Repositories\Interfaces\AdminNotificationRepositoryInterface;
use Modules\Shop\Repositories\ShopAdminRepository;
use Modules\Shop\Repositories\ShopRepository;
use Modules\Shop\Repositories\SubCategoryRepository;
use Modules\Shop\Repositories\AdminNotificationRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ShopRepositoryInterface::class,ShopRepository::class);
        $this->app->bind(SubCategoryRepositoryInterface::class,SubCategoryRepository::class);
        $this->app->bind(AuthRepositoryInterface::class,AuthRepository::class);
        $this->app->bind(ShopAdminRepositoryInterface::class,ShopAdminRepository::class);
        $this->app->bind(AdminRepositoryInterface::class, AdminRepository::class);
        $this->app->bind(AdminNotificationRepositoryInterface::class, AdminNotificationRepository::class);
        $this->app->bind(AppRepositoryInterface::class, AppRepository::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
