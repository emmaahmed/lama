<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('shop')->group(function() {
    Route::get('/', 'ShopController@index');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'shop'], function () {
    Config::set('auth.defines', 'shop');
    // Login
    Route::get('/all-notifications', 'HomeController@notifications')->name('shops.notifications');

    Route::get('login', 'AuthController@login');
    Route::post('login', 'AuthController@doLogin')->name('shop.login');
    //Forget & Reset
    Route::get('forget/password', 'AuthController@forgetPassword');
    Route::post('/doforget/password', 'AuthController@doForgetPassword')->name('shop.forget.password');
    Route::get('reset/password/{token}', 'AuthController@resetPassword');
    Route::post('reset/password', 'AuthController@doResetPassword')->name('shop.reset.password');
    Route::group(['middleware' => 'shop:shop'], function () {
        //
        Route::group(['prefix' => 'notifications'], function () {
            Route::get('/2', 'NotificationsController@index')->name('shop.notifications2');
//            Route::post('/delete', 'NotificationsController@delete')->name('shop.notifications.delete');
//            Route::post('/delete-multi', 'NotificationsController@destroyMulti')->name('shop.notifications.deleteMulti');
            Route::get('send-notification2', 'NotificationsController@sendNotificationView')->name('shop.notifications.send2');
            Route::post('send-notification2', 'NotificationsController@sendNotification')->name('shop.notifications.send-notification2');
        });
        //
        Route::get('/dashboard', 'HomeController@index')->name('shop.dashboard');
        Route::group(['prefix' => 'admins'], function () {
            Route::get('/', 'AdminsController@index')->name('shop.admins');
            Route::get('/add', 'AdminsController@create')->name('shop.admins.create');
            Route::post('/add', 'AdminsController@store')->name('shop.admins.store');
            Route::get('/edit', 'AdminsController@edit')->name('shop.admins.edit');
            Route::post('/edit', 'AdminsController@update')->name('shop.admins.update');
            Route::get('/show', 'AdminsController@show')->name('shop.admins.show');
            Route::post('/delete', 'AdminsController@delete')->name('shop.admins.delete');
            Route::post('/delete-multi', 'AdminsController@destroyMulti')->name('shop.admins.deleteMulti');
            Route::post('/send-multi', 'AdminsController@sendMulti')->name('shop.admins.sendMulti');
            Route::post('/update-token', 'AdminsController@updateToken')->name('shop.shops.update-token');

        });
        Route::group(['prefix' => 'subCategories'], function () {
            Route::get('/', 'SubCategoriesController@index')->name('shop.subCategories');
            Route::post('/add/', 'SubCategoriesController@store')->name('shop.subCategories.store');
            Route::get('/edit', 'SubCategoriesController@edit')->name('shop.subCategories.edit');
            Route::post('/update/', 'SubCategoriesController@update')->name('shop.subCategories.update');
            Route::post('/delete/', 'SubCategoriesController@delete')->name('shop.subCategories.delete');
            Route::post('/delete-multi/', 'SubCategoriesController@destroyMulti')->name('shop.subCategories.deleteMulti');
            Route::post('/change-status', 'SubCategoriesController@changeStatus')->name('shop.sub-categories.status');

        });
//        Route::group(['prefix' => 'shops'], function () {
        Route::get('/edit', 'ShopsController@edit')->name('shop.edit');
        Route::post('/update', 'ShopsController@update')->name('shop.update');
        Route::post('/change-shop-status', 'ShopsController@changeShopStatus')->name('shop.status.action');
        Route::get('remove-images', 'ShopsController@shopRemoveImages')->name('shop.images.remove');
        // Shop Products
        Route::post('/change-product-status', 'ShopsController@changeProductStatus')->name('shop.shops.products.status');
        Route::get('/shop-products', 'ShopsController@showProducts')->name('shop.shops.products.show');
        Route::post('/delete/products', 'ShopsController@deleteProduct')->name('shop.shops.products.delete');
        Route::post('/delete-multi/products', 'ShopsController@destroyMultiProducts')->name('shop.shops.products.deleteMulti');
        Route::any('/search/products', 'ShopsController@search')->name('shop.shops.products.search');
//        });

        Route::group(['prefix' => 'products'], function () {
            Route::get('/', 'ProductsController@index')->name('shop.products');
            Route::get('/not-active-products', 'ProductsController@notActiveProducts')->name('shop.not-active-products');
            Route::get('/add', 'ProductsController@create')->name('shop.products.create');
            Route::post('/add', 'ProductsController@store')->name('shop.products.store');
            Route::get('/edit', 'ProductsController@edit')->name('shop.products.edit');
            Route::post('/update', 'ProductsController@update')->name('shop.products.update');
            Route::post('/change-status', 'ProductsController@changeStatus')->name('shop.products.status');
            Route::get('/show', 'ProductsController@show')->name('shop.products.show');
            Route::get('remove-images', 'ProductsController@productRemoveImages')->name('shop.products.images.remove');
            Route::post('/delete', 'ProductsController@delete')->name('shop.products.delete');
            Route::post('/delete-multi', 'ProductsController@destroyMulti')->name('shop.products.deleteMulti');
            Route::post('/import', 'ProductsController@importProducts')->name('shop.products.import');
            Route::get('/delete-comment/{comment_id}', 'ProductsController@deleteComment')->name('shop.products.delete-comment');
        });
        Route::group(['prefix' => 'comments'], function () {
            Route::get('/edit/{id}', 'HomeController@editComment')->name('shop.comments.edit');
            Route::post('/delete', 'HomeController@deleteComment')->name('shop.comments.delete');
            Route::patch('/update/{id}', 'HomeController@updateComment')->name('shop.comments.update');
        });

        Route::group(['prefix' => 'orders'], function () {
            Route::get('/', 'OrdersController@index')->name('shop.orders');
            Route::get('/finishedOrders', 'OrdersController@finishedOrders')->name('shop.finishedOrders');
            Route::get('/unfinishedOrders', 'OrdersController@unfinishedOrders')->name('shop.unfinishedOrders');
            Route::post('/change-status', 'OrdersController@changeStatus')->name('shop.orders.status');
            Route::get('/show-details', 'OrdersController@showProducts')->name('shop.orders.details');
            //
            Route::get('/print-invoice', 'OrdersController@printInvoice')->name('shop.orders.invoice');
            Route::get('/send-to-delegates', 'OrdersController@sendToDelegates')->name('shop.orders.sendToDelegates');
            //
            Route::post('/delete', 'OrdersController@delete')->name('shop.orders.delete');
            Route::post('/delete-multi', 'OrdersController@destroyMulti')->name('shop.orders.deleteMulti');
            Route::get('/filter-orders', 'OrdersController@filterOrders')->name('shop.orders.filter');
            Route::get('/print','OrdersController@printInvoice')->name('shop.order.print');
            Route::get('/export-orders/{type}/{id?}','OrdersController@exportOrders')->name('shop.order.export');

            Route::get('/reports','ReportsController@index')->name('shop.orders.reports');
            Route::get('/get-reports','ReportsController@Reports')->name('shop.orders.getReports');
            Route::get('/export-reports/{type}','ReportsController@exportReports')->name('shop.orders.reports.export');
            /*
            type :
                0 => all orders ,
                1 => user orders ,
                2 => delegate orders ,
            */
            Route::get('/export-users','OrdersController@exportUsers')->name('shop.user.export');
            Route::get('pdf/{order_id}', 'OrdersController@pdfOrder')->name('shop.orders.pdf');

        });
        Route::group(['prefix' => 'notifications'], function () {
            Route::get('/', 'NotificationsController@index')->name('shop.notifications');
            Route::post('/delete', 'NotificationsController@delete')->name('shop.notifications.delete');
            Route::post('/delete-multi', 'NotificationsController@destroyMulti')->name('shop.notifications.deleteMulti');
            Route::get('send-notification', 'NotificationsController@sendNotificationPage')->name('shop.notifications.send');
            Route::post('send-notification', 'NotificationsController@sendNotification')->name('shop.notifications.send-notification');
        });
        Route::group(['prefix' => 'offers'], function () {
            Route::get('/', 'OffersController@index')->name('shop.offers');

        });

            Route::group(['prefix' => 'settings'], function () {
            Route::get('/edit', 'SettingController@edit')->name('shop.settings');
            Route::post('/edit', 'SettingController@update')->name('shop.settings.update');
        });

        Route::any('logout', 'AuthController@logout')->name('shop.logout');
        Route::get('user_shop_chat', 'ChatController@userShopChat')->name('shop.user_shop_chat');
        Route::get('user_shop_chat_shop', 'ChatController@userShopChatShop')->name('shop.user_shop_chat_shop');
        Route::get('userDetails/{user_id}','ChatController@userChatDetails')->name('shop.user_chat_details');
        Route::get('reply-user/{id}', 'ChatController@replyToUser')->name('shop.replyUser');
        Route::post('send-user', 'ChatController@createMessageUser')->name('shop.sendUser');



        //chat between admin and users
        Route::get('chat', 'ChatController@index')->name('shop.chat');
        Route::get('reply/{id}', 'ChatController@reply')->name('shop.reply');
        Route::post('send', 'ChatController@create_message')->name('shop.send');

    });

});
