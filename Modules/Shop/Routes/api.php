<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/shop', function (Request $request) {
//    return $request->user();
//});
Route::middleware('checkDatabase')->group(function () {

    Route::namespace('Api')->group(function () {
//        Route::post('/register', 'AuthController@register');
       //// new apis
        Route::prefix('shop')->group(function () {
            Route::post('/register', 'AuthController@register');
            Route::post('/verify', 'AuthController@Verify');
            Route::post('/send-code', 'AuthController@sendCode');
            Route::post('/login', 'AuthController@Login');
            Route::post('/change-password', 'AuthController@changePassword');
            Route::post('/add-shop-info', 'AuthController@createShop');
            Route::post('/update-shop-info', 'AuthController@updateShop');
            Route::get('/shop-info', 'AuthController@shopInfo');

//////////
            Route::get('/about-us', 'AppController@aboutUs');
            Route::get('/terms', 'AppController@terms');
            Route::get('/privacy', 'AppController@privacy');
            Route::post('/contact-us', 'AppController@contactUs');
            //shop
            Route::get('/home', 'ShopController@home');
            Route::get('/my-orders', 'ShopController@MyOrders');
            Route::get('/order-details', 'ShopController@orderDetails');
            Route::post('/send-order-to-delegates', 'ShopController@sendToDelegates');
            Route::get('/all-delegates', 'ShopController@allDelegates');
            Route::post('/change-order-status', 'ShopController@changeOrderStatus');
            Route::get('/rates', 'ShopController@rates');
            Route::get('/followers', 'ShopController@followers');
            Route::get('/notifications', 'ShopController@notifications');
          //messages
            Route::get('/chat-history','ShopController@chatHistory');
            Route::get('/messages','ShopController@messages');
            Route::post('/send-message','ShopController@sendMessage');
            Route::post('/send-notification','ShopController@sendNotification');
            Route::post('/switch-notification','ShopController@switchNotification');
            Route::get('/search-orders', 'ShopController@searchOrders');
            Route::get('/download-pdf-order', 'ShopController@downloadOrderPdf')->name('download.order.pdf');


        });

        /////
        Route::get('/shops-by-category', 'ShopController@shopsByCategory');
        Route::post('/follow', 'ShopController@followAndUnfollow');
        Route::get('/following-list', 'ShopController@followingList');
        Route::get('/comments-list/{shop_id}', 'ShopController@commentsList');
        Route::post('/add-comment', 'ShopController@addComment');
    });
});
