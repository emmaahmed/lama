<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('type')->nullable()->comment('0 => super-admin (parent) , 1 => sub-admin (child)');
            $table->string('email')->unique()->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('logo')->nullable();
            $table->string('password')->nullable();
            $table->string('rate')->nullable();
            $table->string('status')->default(0);
            $table->string('min_order')->nullable();
            $table->string('delivery_time')->nullable();
            $table->string('delivery_fees')->nullable();
            $table->time('from')->nullable();
            $table->time('to')->nullable();
            $table->string('online')->default(1);

            $table->rememberToken();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('parent_id')->references('id')->on('shops')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->date('expire_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
