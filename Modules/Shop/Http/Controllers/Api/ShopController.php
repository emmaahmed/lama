<?php

namespace Modules\Shop\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Shop\Entities\Follower;
use Modules\Shop\Http\Requests\CategoryRequest;
use Modules\Shop\Repositories\Interfaces\ShopRepositoryInterface;
use Illuminate\Support\Facades\Validator;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class ShopController extends Controller
{
    protected $shopRepository;

    public function __construct(ShopRepositoryInterface $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    public function shopsByCategory(CategoryRequest $request)
    {
        global $user;$user=null;
        if($request->hasHeader('jwt') && !empty($request->header('jwt'))){
            $user=User::where('jwt',$request->header('jwt'))->first();
        }
         if(isset($request->lat)&&isset($request->lat)) {
                $shops = $this->shopRepository->shopsByCategoryByLatLng($request);

         }
         else {
             $shops = $this->shopRepository->shopsByCategory($request);
         }
        //        dd($shops);
       $data = $shops->select('id','city_id','rate','logo','online','from','to','min_order','fast_delivery','delivery_time','expire_at')
           ->get();
        foreach ($data as $d){
            if($user == null){
                $d->is_follow = false;
            }else{
                $follow=Follower::where('shop_id',$d->id)->where('user_id',$user->id)->first();
                if($follow)
                    $d->is_follow = true;
                else
                    $d->is_follow = false;
            }
        }
        return callback_data(success(), 'shops_by_category',$data);
    }

    public function followAndUnfollow(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'shop_id'   => 'required|exists:shops,id',
        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
            if ($user){
                $result=$this->shopRepository->followAndUnfollow($user->id,$request,$request->header('lang'));

            if($result == false){
                return callback_data(error(),'unfollowed');
                //return response()->json(msg($request,not_found(),'unfollowed'));
            }
            if($result == true){
                return callback_data(success(),'followed');
                //return response()->json(msg($request,success(),'success'));
            }
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));
    }

    public function followingList(Request $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $result=$this->shopRepository->followingList($user->id,$request,$request->header('lang'));
            return callback_data(success(),'following_list',$result);
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));
    }

    public function commentsList(Request $request,$shop_id)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $result=$this->shopRepository->commentsList($user->id,$request,$request->header('lang'),$shop_id);
            return callback_data(success(),'comments_list',$result);
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));
    }

    public function addComment(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'shop_id'   => 'required|exists:shops,id',
            'comment'   => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $result=$this->shopRepository->addComment($user->id,$request,$request->header('lang'));

            if($result == true){
                return callback_data(success(),'success');
                //return response()->json(msg($request,success(),'success'));
            }
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function home(Request $request)
    {
//        $validator = Validator::make($request->all(),[
//            'shop_id'   => 'required|exists:shops,id',
//            'comment'   => 'required',
//        ]);
//
//        if($validator->fails())
//        {
//            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
//        }
//
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $result=$this->shopRepository->home($shop);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function myOrders(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'type'   => 'required|in:0,1,2,3',
        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }
//
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
//            dd($shop->id);
            $result=$this->shopRepository->myOrders($shop,$request->type);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function orderDetails(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'order_id'   => 'required|exists:orders,id',
        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }
//
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $result=$this->shopRepository->orderDetails($request->order_id);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function sendToDelegates(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'order_id'   => 'required|exists:orders,id',
            'all'   => 'required|in:0,1',
            'delegates_id'   => 'required_if:all,==,0|exists:users,id',

        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }
//
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $result=$this->shopRepository->sendToDelegates($request);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function allDelegates(Request $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $result=$this->shopRepository->allDelegates($request);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function changeOrderStatus(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'order_id'   => 'required|exists:orders,id',
            'status'   => 'required|in:1,2,3,4',

        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $result=$this->shopRepository->changeOrderStatus($request);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function rates(Request $request)
    {

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $result=$this->shopRepository->rates($shop->id);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function followers(Request $request)
    {

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $result=$this->shopRepository->followers($shop->id);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function notifications(Request $request)
    {

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $request['shop_id']=$shop->id;
            $result=$this->shopRepository->notifications($request);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function chatHistory(Request $request)
    {

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $result=$this->shopRepository->chatHistory($request,$shop->id);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function messages(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user_id'   => 'required|exists:users,id',

        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $result=$this->shopRepository->messages($shop->id,$request->user_id);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function sendMessage(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'receiver_id'   => 'required|exists:users,id',
            'message'   => 'required',

        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $request['shop_id']=$shop->id;
            $result=$this->shopRepository->sendMessage($request);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function sendNotification(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'all'   => 'required|in:0,1',
            'type'   => 'required|in:notify,order',
            'user_ids'   => 'required_if:all,==,0|exists:users,id',
            'title'=>'required_without:img',
            'message'=>'required_without:img',
            'img'=>'required_without:title,message',

        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $request['shop_id']=$shop->id;
            $result=$this->shopRepository->sendNotification($request);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function switchNotification(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'notification_flag'   => 'required|in:0,1',
            'notification_time'   => 'required_if:notification_flag,==,0|in:all,1,8,12,24',

        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){
            $request['shop_id']=$shop->id;
            $result=$this->shopRepository->switchNotification($request);

                return callback_data(success(),'success',$result);
                //return response()->json(msg($request,success(),'success'));
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');
        //return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function searchOrders(Request $request){
        $validator = Validator::make($request->all(),[
            'key'   => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $shop = checkShopJWT();
        if ($shop){

        $request['shop_id']=$shop->id;

        $result=$this->shopRepository->searchOrders($request);
        return callback_data(success(),'success',$result);
        }
        //dd('sss');
        return callback_data(error(),'check_jwt');

    }
    public function downloadOrderPdf(Request $request){

        $validator = Validator::make($request->all(),[
            'order_id'   => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }
//        if (!request()->headers->has('jwt')){
//            return callback_data(error(),'check_jwt');
//        }
//        $shop = checkShopJWT();
//        if ($shop){

//            $request['shop_id']=$shop->id;

            $result=$this->shopRepository->downloadOrderPdf($request);
            return callback_data(success(),'success',$result);
//        }
        //dd('sss');
//        return callback_data(error(),'check_jwt');

    }



}
