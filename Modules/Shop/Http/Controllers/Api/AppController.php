<?php

namespace Modules\Shop\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Shop\Repositories\Interfaces\AppRepositoryInterface;
use Modules\Shop\Repositories\Interfaces\ShopRepositoryInterface;

class AppController extends Controller
{
    protected $appRepository;

    public function __construct(AppRepositoryInterface $appRepository)
    {
        $this->appRepository = $appRepository;
    }
    public function aboutUs(){
        $data= $this->appRepository->aboutUs();
        return callback_data(success(), 'about_us',$data);

    }
    public function terms(){
        $data= $this->appRepository->terms();
        return callback_data(success(), 'terms',$data);

    }
    public function privacy(){
        $data= $this->appRepository->privacy();
        return callback_data(success(), 'privacy',$data);

    }
    public function contactUs(Request $request){
        $rules = [
            'name'=>'required',
            'email'=>'required|email',
            'phone'=>'numeric',
            'message'=>'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $response['response'] = $validator->messages();
        }

        $this->appRepository->contactUs($request);
        return callback_data(success(), 'success');

    }


}
