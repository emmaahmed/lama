<?php

namespace Modules\Shop\Http\Controllers\Api;

use App\ShopRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Shop\Entities\Shop;
use Modules\Shop\Http\Requests\Api\AuthRequest;
use Modules\Shop\Repositories\Interfaces\AuthRepositoryInterface;

class AuthController extends Controller
{
    protected $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;
    }
    public function register(Request $request)
    {
        $rules = [
            'email'=>'required|email',
            'phone'=>'required|numeric',
            'password'=>'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $response['response'] = $validator->messages();
        }


        $user = $this->authRepository->register($request);
        if ($user=='email_exist'){
            return callback_data(error(),'email_exist');
        }
        if ($user=='phone_exist'){
            return callback_data(error(),'phone_exist');
        }
        if (isset($user->id)){
           $this->authRepository->sendCode($user->phone);
            return callback_data(success(),'registered',$user);
        }
        else{
            return callback_data(error(),'invalid_data');
        }
    }
    public function Verify(Request $request)
    {
        $rules = [
            'code'=>'required|numeric',
            'phone'=>'required|numeric',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $response['response'] = $validator->messages();
        }

        return $this->authRepository->verifyCode($request);
    }
    public function sendCode(Request $request)
    {
        $rules = [
            'phone'=>'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $response['response'] = $validator->messages();
        }

            // Check Phone Exists
            $phones = ShopRequest::pluck('phone')->toArray();
            if (!in_array($request->phone, $phones)) {
                return callback_data(error(),'phone_not_found');
            }
         $this->authRepository->sendCode($request->phone);
        return callback_data(success(),'code_sent');
    }

    public function Login(Request $request)
    {
        //if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'type' => 0])){
        $rules = [
            'phone'=>'required|numeric',
            'password'=>'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $response['response'] = $validator->messages();
        }

        $shopRequest = ShopRequest::where(['phone' => $request->phone])->select('id','email','phone','verified','jwt','shop_info','status')->first();
        if ($shopRequest&&$shopRequest->status == 0 && $shopRequest->shop_info == 1) {
            return callback_data(error(), 'account_waiting_for_approval');


        }
        if ($shopRequest&&$shopRequest->verified == 1 && $shopRequest->shop_info == 0 || $shopRequest&&$shopRequest->verified == 0) {
            return callback_data(success(), 'success', $shopRequest);


        }
//        if(!$shopRequest){
//            return callback_data(error(),'check_phone_data');
//
//        }
        $shop=Shop::where(['phone' => $request->phone])->first();
        if($shop&& $request->platform){
            $shop->update(['platform'=>$request->platform]);
        }

        if($shop &&  Hash::check($request->password, $shop->password)) {
            $this->authRepository->Login($shop, $request);
            $shop['verified']=$shopRequest?$shopRequest->verified:1;
            $shop['shop_info']=$shopRequest?$shopRequest->shop_info:1;
            $shop['status']=(int)$shop->status;
            return callback_data(success(), 'logged_in', $shop);

        }

        //   }
        else {
            return callback_data(error(),'check_phone_data');
        }
    }

    public function changePassword(Request $request)
    {
        $rules = [
            'old_password' => '',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $response['response'] = $validator->messages();
        }


        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkShopJWT();
//        dd($user);
        if ($user){
            $this->authRepository->changePassword($user,$request);
            return callback_data(success(),'password_changes',$user);
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function createShop(Request $request)
    {
        $rules = [
            'shop_name'=>'required',
            'shop_owner_name'=>'required',
//            'image'=>'required',
            'city_id'=>'required|exists:cities,id',
            'logo'=>'required',
            'latitude'=>'required',
            'longitude'=>'required',
            'address'=>'required',
            'category_id'=>'required|exists:categories,id',
            'commercial_register'=>'required',
            'license'=>'required',
            'min_order'=>'required',
            'delivery_types'=>'required',
            'from'=>'required',
            'to'=>'required',
            'payment'=>'required',
            'delivery_time'=>'required',
            'delivery_fees'=>'required',
            'cover'=>'required',
            'long_description'=>'required',
            'short_description'=>'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $response['response'] = $validator->messages();
        }

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkShopRequestJWT();
        if ($user){
             $updated = $this->authRepository->createShop($user,$request);
            if ($updated){
                return callback_data(success(),'success',$updated);
            }
            return callback_data(error(),'errors');

        }else{
            return callback_data(error(),'user_not_found');
        }
    }
    public function updateShop(Request $request){

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkShopJWT();
        if ($user){
            $user= $this->authRepository->updateShop($user,$request);
            return callback_data(success(),'success',$user);
        }else{
            return callback_data(error(),'user_not_found');
        }

    }
    public function shopInfo(Request $request){

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkShopJWT();
        if ($user){
            $user= $this->authRepository->shopInfo($user,$request);
            return callback_data(success(),'success',$user);
        }else{
            return callback_data(error(),'user_not_found');
        }

    }

}
