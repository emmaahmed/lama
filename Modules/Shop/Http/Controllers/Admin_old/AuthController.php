<?php

namespace Modules\Shop\Http\Controllers\Admin_old;

use App\Admin;
use App\Mail\AdminResetPassword;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Modules\Shop\Entities\Shop;

class AuthController extends Controller
{
    public function login()
    {
        return view('shop::admin.auth.login');
    }
    public function doLogin(Request $request)
    {
        $remember_me = $request->remember_me == 1 ? true : false;
        if (shop()->attempt(['email'=>$request->email,'password'=>$request->password,'status' => 1],$remember_me)){
            return redirect('shop/dashboard');
        }else{
            session()->flash('error','بيانات الدخول خاطئة');
            return redirect('shop/login');
        }
    }

    public function logout()
    {
        auth()->guard('shop')->logout();
        return redirect('shop/logout');
    }
    public function forgetPassword()
    {
        return view('shop::admin.auth.passwords.email');
    }

    public function doForgetPassword(Request $request)
    {
        $shop = Shop::where('email',$request->email)->first();
        if (!empty($shop)){
            $token = Str::random(64);
            DB::table('password_resets')->insert([
                'email' => $shop->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);
//            Mail::to($shop->email)->send(new AdminResetPassword(['data' => $shop , 'token' => $token]));
            session()->flash('success','تم الإرسال للبريد الإلكتروني');
            return redirect()->back();
        }
        session()->flash('error','عذرا هذا البريد الإلكتروني غير مسجل لدينا');
        return redirect()->back();
    }

    public function resetPassword ($token)
    {
        $check_token = DB::table('password_resets')
            ->where('token',$token)
            ->where('created_at' , '>' , Carbon::now()->subHours(2))->first();
        if (!empty($check_token)){
            return view('shop::admin.auth.passwords.reset',['data' => $check_token]);
        }else{
            session()->flash('error','للأسف انتهت صلاحية كود التفعيل');
            return redirect(shop_url('forget/password'));
        }
    }
    public function doResetPassword (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required',
        ],[
            'password.required' => 'من فضلك أدخل كلمة المرور',
            'password.confirmed' => 'عفوا كلمة المرور غير متطابقة',
            'password.min' => 'من فضلك أدخل كلمة المرور أكثر من 6 حروف',
            'password_confirmation.required' => 'من فضلك أدخل نأكيد كلمة المرور'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $check_token = DB::table('password_resets')
            ->where('token',$request->token)
            ->where('created_at' , '>' , Carbon::now()->subHours(2))->first();
        if (!empty($check_token)){
            $shop = Shop::where('email',$check_token->email)->update([
                'email' => $check_token->email,
                'password' => bcrypt($request->password)
            ]);
            DB::table('password_resets')->where('email',$request->email)->delete();
            shop()->attempt(['email'=>$check_token->email,'password'=>$request->password,'status' => 1],true);
            session()->flash('success','تم تسجيل الدخول بنجاح');
            return redirect(shop_url('dashboard'));
        }else{
            session()->flash('error','للأسف انتهت صلاحية كود التفعيل');
            return redirect(shop_url('forget/password'));
        }
    }
}
