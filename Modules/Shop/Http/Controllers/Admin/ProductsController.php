<?php

namespace Modules\Shop\Http\Controllers\Admin;

use App\Imports\ProductsImport;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Product\Entities\Rate;
use Modules\Product\Repositories\Interfaces\ProductRepositoryInterface;
use Modules\Shop\Http\Requests\ProductRequest;

class ProductsController extends Controller
{
    protected $productRepository, $indexRepository;

    public function __construct(ProductRepositoryInterface $productRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->productRepository = $productRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_product', ['only' => ['index']]);
//        $this->middleware('permission:add_product', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_product', ['only' => ['edit','update','changeStatus']]);
//        $this->middleware('permission:delete_product', ['only' => ['delete','destroyMulti','productRemoveImages']]);
    }

    public function index()
    {
        $shop = Auth::guard('shop')->user();
        if($shop->parent_id!=null){
            $id=$shop->parent_id;
        }
        else{
            $id=$shop->id;
        }

        $data = $this->productRepository->getProducts($id);

        return view('shop::admin.pages.products.index',compact('data'));
    }

    public function notActiveProducts()
    {
        $shop = Auth::guard('shop')->user();
        if($shop->parent_id!=null){
            $id=$shop->parent_id;
        }
        else{
            $id=$shop->id;
        }

        $data = $this->productRepository->getNotActiveProducts($id);
        return view('shop::admin.pages.products.not-active-products',compact('data'));
    }

    public function create()
    {
        $data = $this->productRepository->create();
        return view('shop::admin.pages.products.create',compact('data'));
    }

    public function store(ProductRequest $request)
    {
//        dd($request->all());
        $created = $this->productRepository->store($request);
        if ($created){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('shop.products');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function changeStatus(ProductRequest $request)
    {
        $result = $this->productRepository->changeStatus($request);
        if ($result){
            return $result == 'product_activated' ? response()->json(['success' => 'تم تفعيل المنتج بنجاح']) : response()->json(['success' => 'تم إلغاء تفعيل المنتج بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function edit(ProductRequest $request)
    {
        $data = $this->productRepository->edit($request);
        return view('shop::admin.pages.products.edit',compact('data'));
    }

    public function update(ProductRequest $request)
    {
//        return $request->all();
        $updated = $this->productRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
        //return redirect()->route('shop.products');
            return redirect()->back();

        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function productRemoveImages(ProductRequest $request)
    {
        $image_removed = $this->productRepository->productRemoveImages($request);
        if ($image_removed){
            session()->flash('success', 'تم حذف الصورة بنجاح');
            return redirect()->back();
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(ProductRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('product', $request->product_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(ProductRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('product', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
    public function importProducts(Request $request){
        Excel::import(new ProductsImport(), request()->file('excel'));
        session()->flash('success', 'تم الرفع بنجاح');
        //return redirect()->route('shop.products');
        return redirect()->back();

    }
    public function deleteComment($id){
        $comment=Rate::whereId($id)->first();
        $comment->delete();
        session()->flash('success', 'تم المسح بنجاح');
        //return redirect()->route('shop.products');
        return redirect()->back();
    }
}
