<?php

namespace Modules\Shop\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Order\Entities\Order;
use Modules\Order\Http\Requests\OrderRequest;
use Modules\Order\Repositories\Interfaces\OrderRepositoryInterface;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
include_once(app_path() . '/WebClientPrint/WebClientPrint.php');
use Neodynamic\SDK\Web\WebClientPrint;

class OrdersController extends Controller
{
    protected $orderRepository,$indexRepository;

    public function __construct(OrderRepositoryInterface $orderRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_order', ['only' => ['index','changeStatus','filterOrders','showProducts']]);
////        $this->middleware('permission:show_product', ['only' => ['showProducts']]);
//        $this->middleware('permission:delete_order', ['only' => ['delete','destroyMulti']]);
//        $this->middleware('permission:export_order', ['only' => ['']]);

    }

    public function index()
    {
        $shop = Auth::guard('shop')->user();
//        $request->request->add(['shop_id' => $shop->id]);
        if($shop->parent_id!=null){
            $id=$shop->parent_id;
        }
        else{
            $id=$shop->id;
        }
         $data = $this->orderRepository->getOrders(0,$id);
//       return $shop;
//       dd( send_to_user(['fotWQsNaSj2gA0j5KFKkHf:APA91bHeKaOoqeQRws8Xf6U0UcEQRtzOw_NmdTcmvS_OfXjAD5ebGhenZTRW_XjBlb0bwat1ujVTLmR8DBkePVRU0dNK0Rb1F77QzGKcPCZ4ezY_Cjt4JsCmsvP2F6PPz_1Q-77mXE76'], 'تم إلغاء الطلب', '5', 17, 1847,$shop));
        $wcpScript = WebClientPrint::createScript(route('print.process'), route('print.commands'), Session::getId());

        return view('shop::admin.pages.orders.index',compact('data','wcpScript'));
    }

    public function finishedOrders()
    {
        $shop = Auth::guard('shop')->user();

        $data = $this->orderRepository->finishedOrders(0,$shop->id);
        return view('shop::admin.pages.orders.index',compact('data'));
    }
    public function unfinishedOrders()
    {
        $shop = Auth::guard('shop')->user();
        $data = $this->orderRepository->unfinishedOrders(0,$shop->id);
        return view('shop::admin.pages.orders.index',compact('data'));
    }

    public function changeStatus(OrderRequest $request)
    {
        if(Auth::guard('shop')->user()->parent_id!=null){
        $id = Auth::guard('shop')->user()->parent_id;
        }
        else {
            $id = Auth::guard('shop')->user()->id;
        }


        $request->request->add(['shop_id' => $id]);
         $changed = $this->orderRepository->changeStatus($request);
        if ($changed){
            return response()->json(['success' => 'تم تعديل حالة الطلب بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function filterOrders(Request $request)
    {
        $shop = Auth::guard('shop')->user();
        if(Auth::guard('shop')->user()->parent_id!=null){
            $id=Auth::guard('shop')->user()->parent_id;
        }
        else{
                      $id=Auth::guard('shop')->user()->id;

        }
        $request->request->add(['shop_id' => $id]);
                       if (isset($request->from) && isset($request->to)) {

        $request['from'] = date('Y-m-d',strtotime($request->from));
        $request['to'] = date('Y-m-d',strtotime($request->to));
                       }
        $data = $this->orderRepository->filterOrders($request);
        return View::make('shop::admin.pages.orders.div',compact('data'));
    }

    public function showProducts(Request $request)
    {
        $data = $this->orderRepository->showProducts($request);
        return view('shop::admin.pages.orders.show',compact('data'));
    }

    public function printInvoice(Request $request)
    {
        $data = $this->orderRepository->showProducts($request);
        return view('shop::admin.pages.orders.print',compact('data'));
    }

    public function delete(OrderRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('order', $request->order_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(OrderRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('order', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function sendToDelegates(Request $request){
        // send notifications to all delegates if order delivery type is fase
        $order = Order::whereId($request->order_id)->first();
            if ($order->delivery_type < 2){
                //delivery_type <==> 0=>Normal, 1=>Fast, 2=>From shop
                //status <===> 0=>created ,1=>accepted ,2=>on_way ,3=>delivered ,4=>cancelled
                $order->update(['status'=>5]);
                $delegates_firebase_tokens = User::where('type', 1)->where('status',1)->get();
                foreach ($delegates_firebase_tokens as $delegate) {

                    send_to_user([$delegate->firebase_token], 'تم إنشاء طلب جديد', '1', $order->shop_id, $order->id, Auth::guard('shop')->user(),$delegate->platform);
                }
            }
        return back()->with('success' , 'تم الارسال بنجاح');
    }
    public function pdfOrder($id)
    {
        $order = Order::whereId($id)
            ->with(['user', 'delegate', 'shop', "orderItems" ])

            ->first();
        if($order->delivery_type == 0){
            $fees=$order->shop->delivery_fees;
        }
        elseif($order->delivery_type == 1){
            $fees=getFastShippingFees();
        }
        else{
            $fees=0;
        }
        $pdfFilePath = public_path('uploads/invoices/orders-'.$order->order_number.'.pdf');
        $input = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=".route('shop.orders.pdf',$order->id);
        $output = public_path($order->order_number.'.png');
        file_put_contents($output, file_get_contents($input));
        if($order->delivery_type == 0){
            $fees=$order->shop->delivery_fees;
        }
        elseif($order->delivery_type == 1){
            $fees=getFastShippingFees();
        }
        else{
            $fees=0;
        }

//        $pdf=PDF::loadView('invoice-pdf', compact('order','fees','output'));
//        $pdf->save($pdfFilePath);

        return view('invoice-pdf', compact('order','fees','output'));


        $pdf = PDF::loadView('invoice.orders.pdf', compact( 'order', 'fees'));
        return $pdf->stream('Order-' . $order->order_number . '.pdf');


    }

}
