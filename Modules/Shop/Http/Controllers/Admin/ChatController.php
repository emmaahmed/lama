<?php

namespace Modules\Shop\Http\Controllers\Admin;

//use App\Model\Chat;
//use App\Model\User;
use App\User;
use Modules\Shop\Entities\Shop;
use Modules\Admin\Entities\Chat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\Order\Entities\UserShopMessage;
use DB;

class ChatController extends Controller
{
    public function index()
    {
        $users = Shop::where('id',Auth::guard('shop')->user()->id)
//            ->orWhere('parent_id',Auth::guard('shop')->user()->id)
            ->where('status', 1)->with(['messages' => function ($query) {
            $query->where('sender_type', 1)->orderBy('id', 'desc');
        }])->get();
        $data=[];
        $data['title'] ='الدردشة';
        //dd($users);

        return view('shop::admin.pages.chat.index',compact('users','data'));
        //return view('chat.index', compact('users'));
    }

    public function reply($id)
    {
        $messages = Chat::where('shop_id', Auth::guard('shop')->user()->id)
            ->where('admin_id', 1)->get();
        foreach ($messages as $message) {
            $message->is_read = 1;
            $message->save();
        }
        $user = Shop::where('id', $id)->first();
        $data=[];
        $data['title'] ='الدردشة';

        return view('shop::admin.pages.chat.reply',compact('messages','user','data'));
        //return view('chat.reply', compact('messages', 'user'));
    }

    public function create_message(Request $request)
    {
        //dd($request->all());
//        $validator = Validator::make($request->all(), [
//            'shop_id' => 'required|exists:shops,id',
//            'message' => 'required',
//        ], [
//            'shop_id.required' => 'من فضلك اختر المستخدم',
//            'shop_id.exists' => 'عذرًا هذا المستخدم غير موجودة لدينا',
//            'message.required' => 'من فضلك ادخل الرسالة',
//        ]);
//        if ($validator->fails()) {
//            return redirect()->back()->withErrors($validator);
//        }
        $message = Chat::create(array_merge($request->all(), [
            'admin_id' => 1,
            'sender_type' => 1,
            'receiver_type' => 2,
        ]));
        $shop=Shop::whereId($request->shop_id)->first();
        $time = date("F j, Y, g:i a",strtotime($message->created_at));
        //Chat::sendChat($request->user_id, $request->message,$time,$request->imag);
        //$message->load('admin');

//        if ($message){
        sendNotificationToAdmin(1,$shop->name,$request->message,$shop->image,$request->shop_id,'shop-message');
        return response()->json($message);
//        }else{
//            session()->flash('error', __('خطا في البيانات'));
//            return redirect()->back();
//        }
    }

    public function userShopChat(Request $request)
    {
        $data=[];

        $users = UserShopMessage::where('shop_id',Auth::guard('shop')->user()->id)
            ->where('receiver_type',1)->where('sender_type',0)
            ->select('sender_id')
            ->groupBy('sender_id')
            ->get();

        foreach ($users as $user){
            $d = User::where('id',$user->sender_id)->first();
            if($d) {

                $user->id = $d->id;
                $user->name = $d->name;
                $user->image = $d->image;
                $user->email = $d->email;
                $user->phone = $d->phone;
            }

        }

        $data['title'] ='الشات مع المستخدمين';
        //$data['client'] = User::where('id',$request->user_id)->first();
        //dd($users);

        return view('shop::admin.pages.users.show_users',compact('users','data'));
        //return view('chat.index', compact('users'));
    }
    public function userShopChatShop(Request $request)
    {
        $data=[];

        $users = UserShopMessage::where('shop_id',Auth::guard('shop')->user()->id)
            ->where('receiver_type',1)->where('sender_type',2)
            ->select('sender_id')
            ->groupBy('sender_id')
            ->get();

        foreach ($users as $user){
            $d = User::where('id',$user->sender_id)->first();
            if($d) {
                $user->id = $d->id;
                $user->name = $d->name;
                $user->image = $d->image;
                $user->email = $d->email;
                $user->phone = $d->phone;
            }

        }

        $data['title'] ='الشات مع المستخدمين';
        //$data['client'] = User::where('id',$request->user_id)->first();
        //dd($users);

        return view('shop::admin.pages.users.show_users',compact('users','data'));
        //return view('chat.index', compact('users'));
    }

    public function replyToUser($id)
    {
        $shop_id=Auth::guard('shop')->user()->id;
        $messages = UserShopMessage::where('shop_id', Auth::guard('shop')->user()->id)
            ->where('sender_id', $id)->orWhere('receiver_id', $id)->get();
        $user=User::whereId($id)->first();
        if($user->type==0){
            $type=2;
        }
        else{
            $type=0;
        }
        $messages = DB::select("
                SELECT *
                FROM user_shop_messages
                Where
                (sender_id = $id and shop_id = $shop_id and sender_type = $type)
                OR
                (receiver_id = $id and shop_id = $shop_id and sender_type = 1)
                Order By id asc
            ");
//        foreach ($messages as $message) {
//            //$message->is_read = 1;
//            $message->save();
//        }
        $user = User::where('id', $id)->first();
        $data=[];
        $data['title'] ='الدردشة';
//dd($messages);
        return view('shop::admin.pages.users.users_chat_details',compact('messages','user','data'));
        //return view('chat.reply', compact('messages', 'user'));
    }

    public function createMessageUser(Request $request)
    {

        $shop_id=Auth::guard('shop')->user()->id;
        $user_id=$request->user_id;
        $add                    = new UserShopMessage();
        $add->sender_id         = $shop_id;
        $add->sender_type       = 1;
        $add->receiver_id       = $user_id;
        $add->receiver_type     = 0;
        $add->shop_id           = $shop_id;
        $add->message           = isset($request->message)? $request->message : '-';
        $add->save();
        $message = DB::select("
                SELECT *
                FROM user_shop_messages
                Where
                (receiver_id = $user_id and shop_id = $shop_id and sender_type = 1)
                Order By id desc limit 1
            ");
        //dd($message);
        //$time = date("F j, Y, g:i a",strtotime($request->message->created_at));
        //Chat::sendChat($request->user_id, $request->message,$time,$request->imag);
        //$message->load('admin');
        $shop = Shop::whereId($shop_id)->select('id','expire_at','logo')->first();
        unset($shop->images);

        $receiver = User::where('id', $user_id)->first();
        send_to_user($receiver->firebase_token, $request->message, 80, $shop_id, "",$shop,$receiver->platform);

//        if ($message){
        return response()->json($message[0]);
//        }else{
//            session()->flash('error', __('خطا في البيانات'));
//            return redirect()->back();
//        }
    }

}
