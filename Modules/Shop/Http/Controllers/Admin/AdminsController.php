<?php

namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Shop\Entities\Shop;
use Modules\Shop\Http\Requests\AdminRequest;
use Modules\Shop\Repositories\Interfaces\AdminRepositoryInterface;

class AdminsController extends Controller
{
    protected $adminRepository, $indexRepository;

    public function __construct(AdminRepositoryInterface $adminRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->adminRepository = $adminRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_admin', ['only' => ['index']]);
//        $this->middleware('permission:add_admin', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_admin', ['only' => ['edit','update']]);
//        $this->middleware('permission:delete_admin', ['only' => ['delete', 'destroyMulti']]);
    }

    public function index()
    {
        $data = $this->adminRepository->index();
        return view('shop::admin.pages.admins.index',compact('data'));
    }

    public function create()
    {
         $data = $this->adminRepository->create();
        return view('shop::admin.pages.admins.add',compact('data'));
    }

    public function store(AdminRequest $request)
    {
//        dd($request->all());
        $created = $this->adminRepository->store($request);
        if ($created){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('shop.admins');
        }
        session()->flash('error', 'عذراً حدث خطأ ما');
        return redirect()->back();
    }

    public function edit(AdminRequest $request)
    {
        $data = $this->adminRepository->edit($request);
        return view('shop::admin.pages.admins.edit',compact('data'));
    }

    public function update(AdminRequest $request)
    {
        $updated = $this->adminRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->route('shop.admins');
        }
        session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
        return redirect()->back();
    }

    public function delete(AdminRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('shop', $request->admin_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(AdminRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('shop', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
    public function updateToken(Request $request){
        $id=Auth::guard('shop')->user()->id;

        $shop=Shop::whereId($id)->first();
        $shop->update([
            'firebase_web_token'=>$request->firebase_token
        ]);
        return $shop;
        return response()->json(['success' => 'تم الحذف بنجاح']);

    }

}
