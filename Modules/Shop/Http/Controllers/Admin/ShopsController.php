<?php

namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Shop\Http\Requests\ShopRequest;
use Modules\Shop\Repositories\Interfaces\ShopRepositoryInterface;

class ShopsController extends Controller
{
    protected $shopRepository, $indexRepository;

    public function __construct(ShopRepositoryInterface $shopRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->shopRepository = $shopRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_shop', ['only' => ['index']]);
//        $this->middleware('permission:add_shop', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_shop', ['only' => ['edit','update','changeStatus']]);
//        $this->middleware('permission:delete_shop', ['only' => ['delete', 'destroyMulti','shopRemoveImages']]);
////        $this->middleware('permission:export_shop', ['only' => ['delete','destroyMulti']]);
//        $this->middleware('permission:show_product', ['only' => ['showProducts']]);
//        $this->middleware('permission:delete_product', ['only' => ['deleteProduct','destroyMultiProducts']]);
    }

    public function edit(Request $request)
    {

        if ($request->has('shop_id')){
            if ($request->shop_id != Auth::guard('shop')->user()->id){
                session()->flash('error', 'عذراً حدث خطأ ما !');
                return redirect()->back();
            }
        }
        $request->request->add(['shop_id' => Auth::guard('shop')->user()->id]);
        $data = $this->shopRepository->edit($request);
        return view('shop::admin.pages.shops.edit',compact('data'));
    }

    public function update(Request $request)
    {
        $request['payment']= implode(",",$request->payment);
        $request['delivery_types']= implode(",",$request->delivery_types);

        if ($request->has('shop_id')){
            if ($request->shop_id != Auth::guard('shop')->user()->id){
                session()->flash('error', 'عذراً حدث خطأ ما !');
                return redirect()->back();
            }
        }
        $request->request->add(['shop_id' => Auth::guard('shop')->user()->id]);
        $updated = $this->shopRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->back();
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function shopRemoveImages(ShopRequest $request)
    {
        if ($request->has('shop_id')){
            if ($request->shop_id != Auth::guard('shop')->user()->id){
                session()->flash('error', 'عذراً حدث خطأ ما !');
                return redirect()->back();
            }
        }
        $request->request->add(['shop_id' => Auth::guard('shop')->user()->id]);
        $image_removed = $this->shopRepository->shopRemoveImages($request);
        if ($image_removed){
            session()->flash('success', 'تم حذف الصورة بنجاح');
            return redirect()->back();
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function showProducts(ShopRequest $request)
    {
        if ($request->has('shop_id')){
            if ($request->shop_id != Auth::guard('shop')->user()->id){
                session()->flash('error', 'عذراً حدث خطأ ما !');
                return redirect()->back();
            }
        }
        $request->request->add(['shop_id' => Auth::guard('shop')->user()->id]);
        $data = $this->shopRepository->showProducts($request);
        return view('shop::admin.pages.shops.show',compact('data'));
    }

    public function deleteProduct(ShopRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('product', $request->product_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMultiProducts(ShopRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('product', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function changeShopStatus(Request $request)
    {
        $request->request->add(['shop_id' => Auth::guard('shop')->user()->id]);
        $result = $this->shopRepository->changeShopStatus($request);
        if ($result){
            return $result == 'shop_online' ? response()->json(['success' => 'المتجر مفتوح']) : response()->json(['warning' => 'المتجر مغلق']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function search(Request $request){
//        return $request->all();
        if ($request->has('shop_id')){
            if ($request->shop_id != Auth::guard('shop')->user()->id){
                session()->flash('error', 'عذراً حدث خطأ ما !');
                return redirect()->back();
            }
        }
        $request->request->add(['shop_id' => Auth::guard('shop')->user()->id]);
        $data = $this->shopRepository->search($request);
        if($request->status==1) {
            return view('shop::admin.pages.products.index', compact('data'));
        }
        else{
            return view('shop::admin.pages.products.not-active-products', compact('data'));

        }
        }
}
