<?php

namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Shop\Http\Requests\AuthRequest;
use Modules\Shop\Repositories\Interfaces\AuthRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function login()
    {
        return view('shop::admin.auth.login');
    }
    public function doLogin(AuthRequest $request)
    {
         $logged_in = $this->authRepository->doLogin($request);
        if ($logged_in){
//            session()->flash('success','تم الدخول بنجاح وجاري تحويلك ..');
//            return Auth::guard('shop')->user()->roles[0]->permissions;
//           dd( Auth::guard('shop')->user()->can('show_offer'));
            return redirect('shop/dashboard');
        }
        session()->flash('error','بيانات الدخول خاطئة');
        return redirect('shop/login');
    }

    public function logout()
    {
        Auth::guard('shop')->user()->update(['firebase_web_token'=>'']);
        auth()->guard('shop')->logout();
        return redirect('shop/logout');
    }
    public function forgetPassword()
    {
        return view('shop::admin.auth.passwords.email');
    }

    public function doForgetPassword(AuthRequest $request)
    {
        $this->authRepository->doForgetPassword($request);
        session()->flash('success','تم الإرسال للبريد الإلكتروني');
        return redirect()->back();
    }

    public function resetPassword ($token)
    {
        $check_token = $this->authRepository->resetPassword($token);
        if ($check_token){
            return view('shop::admin.auth.passwords.reset',['data' => $check_token]);
        }else{
            session()->flash('error','للأسف انتهت صلاحية كود التفعيل');
            return redirect(shop_url('forget/password'));
        }
    }
    public function doResetPassword (AuthRequest $request)
    {
        $check_token = $this->authRepository->doResetPassword($request);
        if ($check_token){
            session()->flash('success','تم تسجيل الدخول بنجاح');
            return redirect(shop_url('dashboard'));
        }
        session()->flash('error','للأسف انتهت صلاحية كود التفعيل');
        return redirect(shop_url('forget/password'));

    }
}
