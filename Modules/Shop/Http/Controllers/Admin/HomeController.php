<?php

namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\UserShopMessage;
use Modules\Shop\Entities\Comment;
use Modules\Shop\Repositories\Interfaces\ShopAdminRepositoryInterface;

class HomeController extends Controller
{
    protected $shopAdminRepository;

    public function __construct(ShopAdminRepositoryInterface $shopAdminRepository)
    {
        $this->shopAdminRepository = $shopAdminRepository;
        // $this->middleware('permission:show_statistics', ['only' => ['index']]);
    }

    public function index()
    {
        $data = $this->shopAdminRepository->statistics();
        return view('shop::admin.pages.index',compact('data'));
    }
    public function editComment($id){
        $comment=Comment::whereId($id)->first();
        $data['comment']=$comment;
        $data['title']="التعليق";
        return view('shop::admin.pages.comments.edit',compact('data'));

    }
    public function deleteComment(Request $request)
    {
        $comment=Comment::whereId($request->comment_id)->first();
        $comment->delete();
        return response()->json(['success' => 'تم الحذف بنجاح']);

    }
    public function updateComment($id,Request $request){
        $comment=Comment::whereId($id)->first();
        $comment->update([
            'comment'=>$request->comment
        ]);
        session()->flash('success', 'تم التعديل بنجاح');
        //return redirect()->route('shop.products');
        return redirect()->back();

    }
    public function notifications(){
        $orders = Order::where('shop_id', Auth::guard('shop')->user()->id)->latest()->get();
        $messages = UserShopMessage::where('shop_id', Auth::guard('shop')->user()->id)->latest()->get();
        $notifications = $orders->merge($messages)->sortByDesc('created_at');
        $data['title']='الإشعارات';
        $data['notifications']=$notifications;
        return view('shop::admin.pages.notifications',compact('data'));

    }

}
