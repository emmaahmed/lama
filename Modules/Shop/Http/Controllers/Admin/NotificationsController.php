<?php

namespace Modules\Shop\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Shop\Http\Requests\AdminNotificationRequest;
use Modules\Shop\Repositories\Interfaces\AdminNotificationRepositoryInterface;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Shop\Entities\Shop;

class NotificationsController extends Controller
{
    protected $adminNotificationRepository;
    protected $indexRepository;

    public function __construct(AdminNotificationRepositoryInterface $adminNotificationRepository)
    {
        $this->adminNotificationRepository = $adminNotificationRepository;
        //$this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_sent', ['only' => ['index']]);
//        $this->middleware('permission:send_notification', ['only' => ['sendNotificationView','sendNotification','delete','destroyMulti']]);
    }

    public function index()
    {
        $data = $this->adminNotificationRepository->index();
        return view('shop::admin.pages.notifications.index',compact('data'));
    }

    public function sendNotificationPage()
    {
        $data['title'] = 'ارسال رسالة';
        $users = User::where('type',0)->get();
        $shops = Shop::get();
        foreach ($users as $user){
            $user->type = 'user';
        }
        foreach ($shops as $shop){
            $shop->type = 'shop';
        }
        $data['users'] = $users;
        $data['shops'] = $shops;
        return view('shop::admin.pages.notifications.send',compact('data'));
    }

    public function sendNotificationView()
    {
        $data = $this->adminNotificationRepository->sendNotificationView();
        return view('shop::admin.pages.notifications.send',compact('data'));
    }

    public function sendNotification(Request $request)
    {
        $sent = $this->adminNotificationRepository->sendNotification($request);
        session()->flash('success', __("تم إرسال الرسالة بنجاح"));
        return redirect()->back();
    }

    public function delete(AdminNotificationRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('adminNotification', $request->notification_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(AdminNotificationRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('adminNotification', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
