<?php

namespace Modules\Shop\Http\Controllers\Admin;

use App\Repositories\Interfaces\HomeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Shop\Http\Requests\SubCategoryRequest;
use Modules\Shop\Repositories\Interfaces\SubCategoryRepositoryInterface;

class SubCategoriesController extends Controller
{
    protected $subCategoryRepository, $homeRepository, $indexRepository;

    public function __construct(SubCategoryRepositoryInterface $subCategoryRepository, HomeRepositoryInterface $homeRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->subCategoryRepository = $subCategoryRepository;
        $this->homeRepository = $homeRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:add_sub_category', ['only' => ['store']]);
//        $this->middleware('permission:show_sub_category', ['only' => ['index']]);
//        $this->middleware('permission:edit_sub_category', ['only' => ['edit','update']]);
//        $this->middleware('permission:delete_sub_category', ['only' => ['delete','destroyMulti']]);
    }

    public function index(Request $request)
    {
        $shop = Auth::guard('shop')->user();
        if($shop->parent_id!=null){
            $id=$shop->parent_id;
        }
        else{
            $id=$shop->id;
        }

        $request->request->add(['shop_id' => $id]);
        $data = $this->subCategoryRepository->index($request);
        return view('shop::admin.pages.subCategories.index',compact('data'));
    }

    public function store(SubCategoryRequest $request)
    {
        $created = $this->subCategoryRepository->store($request);
        if ($created){
            session()->flash('success', 'تم بنجاح');
            return redirect()->back();
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(SubCategoryRequest $request)
    {
        $subCategory  = $this->homeRepository->subCategories()->where('id', $request->subCategory_id)->first();
        $subCategory->name_ar = $subCategory->translate('ar')->name;
        $subCategory->name_en = $subCategory->translate('en')->name;
        return response()->json($subCategory);
    }

    public function delete(SubCategoryRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('subCategory', $request->subCategory_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(SubCategoryRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('subCategory', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
    public function changeStatus(Request $request)
    {
        $status = $this->subCategoryRepository->changeStatus($request);
        if ($status){
            if ($status == 'activated'){
                return response()->json(['success' => 'تم تفعيل القسم بنجاح']);
            }
            return response()->json(['success' => 'تم إلغاء تفعيل القسم بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

}
