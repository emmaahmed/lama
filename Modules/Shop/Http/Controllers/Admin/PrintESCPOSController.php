<?php

namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Routing\Controller;

//*********************************
// IMPORTANT NOTE
// ==============
// If your website requires user authentication, then
// THIS FILE MUST be set to ALLOW ANONYMOUS access!!!
//
//*********************************

//Includes WebClientPrint classes
include_once(app_path() . '/WebClientPrint/WebClientPrint.php');

use Illuminate\Support\Facades\Storage;
use Modules\Order\Entities\Order;
use Neodynamic\SDK\Web\ClientPrintJobGroup;
use Neodynamic\SDK\Web\PrintFilePDF;
use Neodynamic\SDK\Web\WebClientPrint;
use Neodynamic\SDK\Web\Utils;
use Neodynamic\SDK\Web\DefaultPrinter;
use Neodynamic\SDK\Web\InstalledPrinter;
use Neodynamic\SDK\Web\PrintFile;
use Neodynamic\SDK\Web\ClientPrintJob;

use niklasravnsborg\LaravelPdf\Facades\Pdf;
use Session;

class PrintESCPOSController extends Controller
{
    public function printCommands(Request $request){




        //return view('cp_shop.orders.invoice', compact('order', 'tax'));

//dd($order);



//        $printerName = urldecode('XP-80');
        $printerName = urldecode('PB-A11P Miniprinter');

        $order = Order::whereId($request->id)

            ->first();

        $pdfFilePath = public_path('uploads/invoices/orders-'.$order->order_number.'.pdf');

        $cpjLogo = new ClientPrintJob();
        //dd(public_path());



        $cpjLogo->printFile = new PrintFilePDF($pdfFilePath, 'invoice.pdf', null);

        $cpjLogo->clientPrinter = new InstalledPrinter($printerName);

        return response($cpjLogo->sendToClient())
            ->header('Content-Type', 'application/octet-stream');



    }

}
