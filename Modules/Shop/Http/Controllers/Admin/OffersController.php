<?php

namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Http\Requests\OffersRequest;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Product\Entities\Product;
use Modules\Product\Repositories\Interfaces\OfferRepositoryInterface;
use Modules\Shop\Entities\Shop;

class OffersController extends Controller
{
    protected $offerRepository, $indexRepository;

    public function __construct(OfferRepositoryInterface $offerRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->offerRepository = $offerRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_offer', ['only' => ['index']]);
//        $this->middleware('permission:add_offer', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_offer', ['only' => ['edit','update']]);
//        $this->middleware('permission:delete_offer', ['only' => ['delete','destroyMulti']]);
    }

    public function index()
    {
        $data = $this->offerRepository->indexOffersShop(Auth::guard('shop')->user()->id);
        return view('shop::admin.pages.offers.index',compact('data'));
    }


    public function getOffers(Request $request){
        $data= Product::where('shop_id',$request->shop_id)
//            ->select('id','name')
            ->get();
        //dd($data['products']);
        return response()->json($data);
    }


}
