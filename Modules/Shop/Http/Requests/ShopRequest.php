<?php

namespace Modules\Shop\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShopRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('admin/shops/add')) {
            return $this->storeShopRules();
        }
        if($this->is('admin/shops/edit')
            || $this->is('admin/shops/delete')
            || $this->is('admin/shops/change-status')
            || $this->is('admin/shops/shop-products')
            || $this->is('shop/delete')
            || $this->is('shop/change-status')
            || $this->is('shop/shop-products')) {
            return $this->checkShopRules();
        }
        if($this->is('admin/shops/update')
            || $this->is('shop/update')) {
            return $this->updateShopRules();
        }
        if($this->is('admin/shops/remove-images')
            || $this->is('shop/remove-images')) {
            return $this->removeShopImagesRules();
        }
        if($this->is('admin/shops/delete-multi')) {
            return $this->deleteMultiRules();
        }
        if($this->is('admin/shops/delete/products')
            || $this->is('shop/delete/products')) {
            return $this->deleteProductRules();
        }
        if($this->is('admin/shops/delete-multi/products')
            || $this->is('shop/delete-multi/products')) {
            return $this->deleteMultiProductsRules();
        }
    }

    public function messages()
    {
        if($this->is('admin/shops/add')) {
            return $this->storeShopMessages();
        }
        if($this->is('admin/shops/edit')
            || $this->is('admin/shops/delete')
            || $this->is('admin/shops/change-status')
            || $this->is('admin/shops/shop-products')
            || $this->is('shop/delete')
            || $this->is('shop/change-status')
            || $this->is('shop/shop-products')) {
            return $this->checkShopMessages();
        }
        if($this->is('admin/shops/update')
            || $this->is('shop/update')) {
            return $this->updateShopMessages();
        }
        if($this->is('admin/shops/remove-images')
            || $this->is('shop/remove-images')) {
            return $this->removeShopImagesMessages();
        }
        if($this->is('admin/shops/delete-multi')) {
            return $this->deleteMultiMessages();
        }
        if($this->is('admin/shops/delete/products')
            || $this->is('shop/delete/products')) {
            return $this->deleteProductMessages();
        }
        if($this->is('admin/shops/delete-multi/products')
            || $this->is('shop/delete-multi/products')) {
            return $this->deleteMultiProductsMessages();
        }
    }


    public function storeShopRules()
    {
        return [
            'city_id' => 'required|exists:cities,id',
            'category_id' => 'required|exists:categories,id',
            'name_ar' => 'required',
//            'name_en' => 'required',
            'short_description_ar' => 'required',
//            'short_description_en' => 'required',
            'long_description_ar' => 'required',
//            'long_description_en' => 'required',
            'from' => 'required',
            'to' => 'required',
            'delivery_time' => 'required',
            'delivery_fees' => 'required',
            'phone' => 'required|unique:shops,phone',
            'logo' => 'required|mimes:jpg,jpeg,png',
            'images' => 'required',
            'images.*' => 'mimes:jpg,jpeg,png',
            'email' => 'required|unique:shops,email',
            'password' => 'required',
            'fast_delivery'=>'',
            'area'=>'required'
        ];
    }

    public function storeShopMessages()
    {
        return [
            'city_id.required' => 'من فضلك أختر المدينة',
            'city_id.exists' => 'عذرا المدينة غير موجودة',
            'category_id.required' => 'من فضلك اختر القسم',
            'category_id.exists' => 'عذرا القسم غير موجود',
            'name_ar.required' => 'من فضلك أدخل الإسم باللغة العربية',
//            'name_en.required' => 'من فضلك ادخل الإسم باللغة الإنجليزية',
            'short_description_ar.required' => 'من فضلك أدخل الوصف المختصر باللغة العربية',
//            'short_description_en.required' => 'من فضلك أدخل الوصف المختصر باللغة الانجليزية',
            'long_description_ar.required' => 'من فضلك أدخل الوصف بالكامل باللغة العربية',
//            'long_description_en.required' => 'من فضلك أدخل الوصف بالكامل باللغة الانجليزية',
            'from.required' => 'من فضلك أدخل موعد بداية الدوام',
            'to.required' => 'من فضلك أدخل موعد نهاية الدوام',
            'delivery_time.required' => 'من فضلك أدخل وقت التوصيل',
            'delivery_fees.required' => 'من فضلك مصاريف التوصيل',
            'phone.required' => 'من فضلك أدخل رقم التواصل',
            'phone.unique' => 'عذرا رقم التواصل موجود من قبل',
            'logo.required' => 'من فضلك أختر لوجو المتجر',
            'logo.mimies' => 'من فضلك أختر صورة',
            'images.required' => 'من فضلك اختر صور المتجر',
            'images.image' => 'من فضلك اختر صورة',
            'images.mimies' => 'من فضلك أختر صورة',
            'email.required' => 'من فضلك أدخل البريد الإلكتروني',
            'email.unique' => 'عذرا البريد الإلكتروني موجود من قبل',
            'password.required' => 'من فضلك ادخل كلمة المرور',
            'area.required' => 'من فضلك ادخل نطاق المتجر',

        ];
    }

    public function checkShopRules()
    {
        return [
            'shop_id' => 'required|exists:shops,id',
        ];
    }

    public function checkShopMessages()
    {
        return [
            'shop_id.required' => 'اختر المتجر',
            'shop_id.exists' => 'المتجر غير موجود',
        ];
    }

    public function updateShopRules()
    {
//        dd($this->all());
        return [
            'city_id' => 'required|exists:cities,id',
            'category_id' => 'required|exists:categories,id',
            'name_ar' => 'required',
//            'name_en' => 'required',
            'short_description_ar' => 'required',
//            'short_description_en' => 'required',
            'long_description_ar' => 'required',
//            'long_description_en' => 'required',
            'from' => 'required',
            'to' => 'required',
            'delivery_time' => 'required',
            'delivery_fees' => 'required',
            'phone' => 'required|unique:shops,phone,'.$this->shop_id,
            'logo' => 'mimes:jpg,jpeg,png',
            'images.*' => 'mimes:jpg,jpeg,png',
            'email' => 'required|unique:shops,email,'.$this->shop_id,
            'fast_delivery'=>'',
            'area'=>'required',
            'latitude'=>'required',
            'longitude'=>'required'


        ];
    }

    public function updateShopMessages()
    {
        return [
            'city_id.required' => 'من فضلك أختر المدينة',
            'city_id.exists' => 'عذرا المدينة غير موجودة',
            'category_id.required' => 'من فضلك اختر القسم',
            'category_id.exists' => 'عذرا القسم غير موجود',
            'name_ar.required' => 'من فضلك أدخل الإسم باللغة العربية',
//            'name_en.required' => 'من فضلك ادخل الإسم باللغة الإنجليزية',
            'short_description_ar.required' => 'من فضلك أدخل الوصف المختصر باللغة العربية',
//            'short_description_en.required' => 'من فضلك أدخل الوصف المختصر باللغة الانجليزية',
            'long_description_ar.required' => 'من فضلك أدخل الوصف بالكامل باللغة العربية',
//            'long_description_en.required' => 'من فضلك أدخل الوصف بالكامل باللغة الانجليزية',
            'from.required' => 'من فضلك أدخل موعد بداية الدوام',
            'to.required' => 'من فضلك أدخل موعد نهاية الدوام',
            'delivery_time.required' => 'من فضلك أدخل وقت التوصيل',
            'delivery_fees.required' => 'من فضلك مصاريف التوصيل',
            'phone.required' => 'من فضلك أدخل رقم التواصل',
            'phone.unique' => 'عذرا رقم التواصل موجود من قبل',
            'logo.mimies' => 'من فضلك أختر صورة',
            'images.image' => 'من فضلك اختر صورة',
            'images.mimies' => 'من فضلك أختر صورة',
            'email.required' => 'من فضلك أدخل البريد الإلكتروني',
            'email.unique' => 'عذرا البريد الإلكتروني موجود من قبل',
            'area.required' => 'من فضلك ادخل نطاق المتجر',
        ];
    }

    public function removeShopImagesRules()
    {
        return [
            'shop_id' => 'required|exists:shops,id|exists:shop_images,shop_id',
            'image_id' => 'required|exists:shop_images,id',
        ];
    }

    public function removeShopImagesMessages()
    {
        return [
            'shop_id.required' => 'اختر المتجر',
            'shop_id.exists' => 'المتجر غير موجود',
            'image_id.required' => 'اختر المتجر',
            'image_id.exists' => 'الصورة غير موجودة',
        ];
    }

    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:shops,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذا المستخدم غير موجود',
        ];
    }

    public function deleteProductRules()
    {
        return [
            'product_id' => 'required|exists:products,id',
        ];
    }

    public function deleteProductMessages()
    {
        return [
            'product_id.required' => 'product_required',
            'product_id.exists' => 'product_not_found',
        ];
    }

    public function deleteMultiProductsRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:products,id',
        ];
    }

    public function deleteMultiProductsMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذا المستخدم غير موجود',
        ];
    }
}
