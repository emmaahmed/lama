<?php

namespace Modules\Shop\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubCategoryRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('admin/shops/subCategories')) {
            return $this->checkShopRules();
        }
        if($this->is('admin/shops/add/subCategory') || $this->is('shop/subCategories/add')) {
            return $this->createShopSubCategoryRules();
        }
        if($this->is('admin/shops/edit/subCategory') || $this->is('shop/subCategories/edit')) {
            return $this->editShopSubCategoryRules();
        }
        if($this->is('admin/shops/delete/subCategory') || $this->is('shop/subCategories/delete')) {
            return $this->checkSubCategoryRules();
        }
        if($this->is('admin/shops/delete-multi/subCategory') || $this->is('shop/subCategories/delete-multi')) {
            return $this->deleteMultiRules();
        }
    }

    public function messages()
    {
        if($this->is('admin/shops/subCategories')) {
            return $this->checkShopMessages();
        }
        if($this->is('admin/shops/add/subCategory') || $this->is('shop/subCategories/add')) {
            return $this->createShopSubCategoryMessages();
        }
        if($this->is('admin/shops/edit/subCategory') || $this->is('shop/subCategories/edit')) {
            return $this->editShopSubCategoryMessages();
        }
        if($this->is('admin/shops/delete/subCategory') || $this->is('shop/subCategories/delete')) {
            return $this->checkSubCategoryMessages();
        }
        if($this->is('admin/shops/delete-multi/subCategory') || $this->is('shop/subCategories/delete-multi')) {
            return $this->deleteMultiMessages();
        }
    }

    public function checkShopRules()
    {
        return [
            'shop_id' => 'required|exists:shops,id',
        ];
    }

    public function checkShopMessages()
    {
        return [
            'shop_id.required' => 'اختر المتجر',
            'shop_id.exists' => 'المتجر غير موجود',
        ];
    }
    public function createShopSubCategoryRules()
    {
        return [
            'shop_id' => 'required|exists:shops,id',
            'name_ar' => 'required',
//            'name_en' => 'required'
        ];
    }

    public function createShopSubCategoryMessages()
    {
        return [
            'shop_id.required' => 'اختر المتجر',
            'shop_id.exists' => 'المتجر غير موجود',
            'name_ar.required' => 'من فضلك أدخل إسم القسم باللغة العربية',
//            'name_en.required' => 'من فضلك أدخل إسم القسم باللغة الانجليزية'
        ];
    }
    public function editShopSubCategoryRules()
    {
        return [
            'subCategory_id' => 'required|exists:sub_categories,id',
            'shop_id' => 'required|exists:shops,id',
        ];
    }

    public function editShopSubCategoryMessages()
    {
        return [
            'subCategory_id.required' => 'اختر القسم الفرعي',
            'subCategory_id.exists' => 'القسم الفرعي غير موجود',
            'shop_id.required' => 'اختر المتجر',
            'shop_id.exists' => 'المتجر غير موجود',
        ];
    }

    public function checkSubCategoryRules()
    {
        return [
            'subCategory_id' => 'required|exists:sub_categories,id',
        ];
    }

    public function checkSubCategoryMessages()
    {
        return [
            'subCategory_id.required' => 'اختر المتجر',
            'subCategory_id.exists' => 'المتجر غير موجود',
        ];
    }

    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:sub_categories,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذا المستخدم غير موجود',
        ];
    }
}
