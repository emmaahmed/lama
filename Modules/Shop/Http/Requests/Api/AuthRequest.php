<?php

namespace Modules\Shop\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email'=>'required|unique:shop_requests',
            'phone'=>'required|unique:shop_requests',
            'password'=>'required',
        ];
//        return ['success'];
//        dd($this->is('api/shop/register'));
//        if($this->is('api/shop/register')) {
//            return $this->storeShopRequestRules();
//        }

    }

//
//    public function messages()
//    {
////                dd($this->is('api/shop/register'));
//
//        if($this->is('api/shop/register')) {
//            return $this->storeShopRequestMessages();
//        }
//
//    }
//    public function storeShopRequestRules()
//    {
//        return [
//            'email'=>'required|unique:shop_requests',
//            'phone'=>'required|unique:shop_requests',
//            'password'=>'required',
//        ];
//    }
//    public function storeShopRequestMessages(){
//        return [
//            'email.required' => 'من فضلك أدخل البريد الإلكتروني',
//            'email.unique' => 'عذرا البريد الإلكتروني موجود من قبل',
//            'phone.required' => 'من فضلك أدخل رقم التواصل',
//            'phone.unique' => 'عذرا رقم التواصل موجود من قبل',
//
//            'password.required' => 'من فضلك ادخل كلمة المرور',
//
//        ];
//
//    }

}
