<?php

namespace Modules\Shop\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('shop/products/add')) {
            return $this->storeProductRules();
        }
        if($this->is('shop/products/change-status')
            || $this->is('shop/products/edit')
            || $this->is('shop/products/delete')) {
            return $this->checkProductRules();
        }
        if($this->is('shop/products/update')) {
            return $this->updateProductRules();
        }
        if($this->is('shop/products/remove-images')) {
            return $this->removeProductImagesRules();
        }
        if($this->is('shop/products/delete-multi')) {
            return $this->deleteMultiRules();
        }
    }

    public function messages()
    {
        if($this->is('shop/products/add')) {
            return $this->storeProductMessages();
        }
        if($this->is('shop/products/change-status')
            || $this->is('shop/products/edit')
            || $this->is('shop/products/delete')) {
            return $this->checkProductMessages();
        }
        if($this->is('shop/products/update')) {
            return $this->updateProductMessages();
        }
        if($this->is('shop/products/remove-images')) {
            return $this->removeProductImagesMessages();
        }

        if($this->is('shop/products/delete-multi')) {
            return $this->deleteMultiMessages();
        }
    }


    public function storeProductRules()
    {
        return [
            'name_ar' => 'required',
//            'name_en' => 'required',
            'short_description_ar' => 'required',
//            'short_description_en' => 'required',
            'long_description_ar' => 'required',
//            'long_description_en' => 'required',
            'quantity' => 'required',
            //'price_before' => 'required',
            //'price_after' => 'required',
            'percent' => 'required',
            'images' => 'required',
            'images.*' => 'mimes:jpeg,jpg,png,bmp,gif,svg',
        ];
    }

    public function storeProductMessages()
    {
        return [
            'name_ar.required' => 'من فضلك أدخل إسم المنتج باللغة العربية',
//            'name_en.required' => 'من فضلك أدخل إسم المنتج باللغة الانجليزية',
            'short_description_ar.required' => 'من فضلك أدخل وصف المنتج المختصر باللغة العربية',
//            'short_description_en.required' => 'من فضلك أدخل وصف المنتج المختصر باللغة الانجليزية',
            'long_description_ar.required' => 'من فضلك أدخل وصف المنتج الكامل باللغة العربية',
//            'long_description_en.required' => 'من فضلك أدخل وصف المنتج الكامل باللغة الانجليزية',
            'quantity.required' => 'من فضلك أدخل الكمية المتاحة',
            //'price_before.required' => 'من فضلك أدخل سعر المنتج قبل الخصم',
            //'price_after.required' => 'من فضلك أدخل سعر المنتج بعد الخصم',
            'percent.required' => 'من فضلك أدخل نسبة الخصم',
            'images.required' => 'من فضلك اختر صور المنتج',
            'images.image' => 'من فضلك اختر صورة',
            'images.mimes' => 'من فضلك أختر صورة',
        ];
    }

    public function checkProductRules()
    {
        return [
            'product_id' => 'required|exists:products,id',
        ];
    }

    public function checkProductMessages()
    {
        return [
            'product_id.required' => 'اختر المنتج',
            'product_id.exists' => 'المنتج غير موجود',
        ];
    }

    public function updateProductRules()
    {
        return [
            'product_id' => 'required|exists:products,id',
            'name_ar' => 'required',
//            'name_en' => 'required',
            'short_description_ar' => 'required',
//            'short_description_en' => 'required',
            'long_description_ar' => 'required',
//            'long_description_en' => 'required',
            'quantity' => 'required',
            //'price_before' => 'required',
            //'price_after' => 'required',
            'images' => 'array',
            'images.*' => 'mimes:jpg,jpeg,png',
        ];
    }

    public function updateProductMessages()
    {
        return [
            'product_id.required' => 'اختر المنتج',
            'product_id.exists' => 'المنتج غير موجود',
            'name_ar.required' => 'من فضلك أدخل إسم المنتج باللغة العربية',
//            'name_en.required' => 'من فضلك أدخل إسم المنتج باللغة الانجليزية',
            'short_description_ar.required' => 'من فضلك أدخل وصف المنتج المختصر باللغة العربية',
//            'short_description_en.required' => 'من فضلك أدخل وصف المنتج المختصر باللغة الانجليزية',
            'long_description_ar.required' => 'من فضلك أدخل وصف المنتج الكامل باللغة العربية',
//            'long_description_en.required' => 'من فضلك أدخل وصف المنتج الكامل باللغة الانجليزية',
            'quantity.required' => 'من فضلك أدخل الكمية المتاحة',
            //'price_before.required' => 'من فضلك أدخل سعر المنتج قبل الخصم',
            //'price_after.required' => 'من فضلك أدخل سعر المنتج بعد الخصم',
            'percent.required' => 'من فضلك أدخل نسبة الخصم',
            'images.image' => 'من فضلك اختر صورة',
            'images.mimes' => 'من فضلك أختر صورة',
        ];
    }

    public function removeProductImagesRules()
    {
        return [
            'product_id' => 'required|exists:products,id|exists:product_images,product_id',
            'image_id' => 'required|exists:product_images,id',
        ];
    }

    public function removeProductImagesMessages()
    {
        return [
            'product_id.required' => 'اختر المنتج',
            'product_id.exists' => 'المنتج غير موجود',
            'image_id.required' => 'اختر الصورة',
            'image_id.exists' => 'الصورة غير موجودة',
        ];
    }

    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:products,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذا المنتج غير موجود',
        ];
    }


}
