<?php

namespace Modules\Shop\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'category_id' => 'required|exists:categories,id',
            'sort_by_price' => 'in:high,low',
            'sort_by_date' => 'in:latest,oldest',
        ];
    }

    public function messages()
    {
        return [
            'category_id.required' => 'category_required',
            'category_id.exists' => 'category_not_found',
            'sort_by_price.in' => 'in_high-and-low',
            'sort_by_date.in' => 'in_latest-and-oldest',

        ];
    }
}
