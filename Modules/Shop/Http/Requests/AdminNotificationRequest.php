<?php

namespace Modules\Shop\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class AdminNotificationRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('shop/notifications/delete')) {
            return $this->checkNotificationRules();
        }
        if($this->is('shop/notifications/send-notification')) {
            return $this->sendNotificationRules();
        }
        if($this->is('shop/notifications/delete-multi')) {
            return $this->deleteMultiRules();
        }
    }

    public function messages()
    {
        if($this->is('shop/notifications/delete')) {
            return $this->checkNotificationMessages();
        }
        if($this->is('shop/notifications/send-notification')) {
            return $this->sendNotificationMessages();
        }
        if($this->is('shop/notifications/delete-multi')) {
            return $this->deleteMultiMessages();
        }
    }

    public function checkNotificationRules()
    {
        return [
            'notification_id' => 'required|exists:admin_notifications,id',
        ];
    }

    public function checkNotificationMessages()
    {
        return [
            'notification_id.required' => 'اختر العرض',
            'notification_id.exists' => 'العرض غير موجود',
        ];
    }

    public function sendNotificationRules()
    {
        return [
            'message' => 'required',
        ];
    }

    public function sendNotificationMessages()
    {
        return [
            'message.required' => 'من فضلك أدخل الرد',
        ];
    }

    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:admin_notifications,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذا الإشعار غير موجود',
        ];
    }
}
