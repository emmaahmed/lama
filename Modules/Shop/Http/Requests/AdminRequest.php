<?php

namespace Modules\Shop\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('shop/admins/add')) {
            return $this->storeAdminRules();
        }
        if($this->is('shop/admins/edit')
            || $this->is('shop/admins/delete')) {
            return $this->checkAdminRules();
        }
        if($this->is('shop/admins/update')) {
            return $this->updateAdminRules();
        }
        if($this->is('shop/admins/delete-multi')) {
            return $this->deleteMultiRules();
        }
    }

    public function messages()
    {
        if($this->is('shop/admins/add')) {
            return $this->storeAdminMessages();
        }
        if($this->is('shop/admins/edit')
            || $this->is('shop/admins/delete')) {
            return $this->checkAdminMessages();
        }
        if($this->is('shop/admins/update')) {
            return $this->updateAdminMessages();
        }
        if($this->is('shop/admins/delete-multi')) {
            return $this->deleteMultiMessages();
        }
    }

    public function storeAdminRules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:shops,email',
            'password' => 'required|min:6'
        ];
    }

    public function storeAdminMessages()
    {
        return [
            'name.required' => 'من فضلك أدخل إسم المستخدم',
            'email.required' => 'من فضلك أدخل البريد الإلكتروني',
            'email.email' => 'من فضلك أدخل بريد إلكتروني صالح',
            'email.unique' => 'هذا البريد الالكتروني موجود من قبل',

            'password.required' => 'من فضلك أدخل كلمة المرور',
            'password.min' => 'من فضلك أدخل كلمة المرور أكثر من 3 حروف'
        ];
    }

    public function checkAdminRules()
    {
        return [
            'admin_id' => 'required|exists:shops,id',
        ];
    }

    public function checkAdminMessages()
    {
        return [
            'admin_id.required' => 'اختر الأدمن',
            'admin_id.exists' => 'الأدمن غير موجود',
        ];
    }

    public function updateAdminRules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:shops,email,'.$this->admin_id,
        ];
    }

    public function updateAdminMessages()
    {
        return [
            'name.required' => 'من فضلك أدخل إسم المستخدم',
            'email.required' => 'من فضلك أدخل البريد الإلكتروني',
            'email.email' => 'من فضلك أدخل بريد إلكتروني صالح',
            'email.unique' => 'عذرا البريد الإلكتروني مستخدم من قبل',
        ];
    }

    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:shops,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذه المدينة غير موجودة',
        ];
    }
}
