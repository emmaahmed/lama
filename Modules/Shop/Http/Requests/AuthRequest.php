<?php

namespace Modules\Shop\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('shop/login')) {
            return $this->checkLoginRules();
        }
        if($this->is('shop/doforget/password')) {
            return $this->checkEmailRules();
        }
        if($this->is('shop/reset/password')) {
            return $this->resetPasswordRules();
        }
    }

    public function messages()
    {
        if($this->is('shop/login')) {
            return $this->checkLoginMessages();
        }
        if($this->is('shop/doforget/password')) {
            return $this->checkEmailMessages();
        }
        if($this->is('shop/reset/password')) {
            return $this->resetPasswordMessages();
        }
    }


    public function checkLoginRules()
    {
        return [
            'email' => 'required|email|exists:shops,email',
            'password' => 'required'
        ];
    }

    public function checkLoginMessages()
    {
        return [
            'email.required' => 'من فضلك أدخل البريد الإلكتروني',
            'email.email' => 'من فضلك أدخل بريد إلكتروني صالح',
            'email.exists' => 'عذرا البريد الإلكتروني غير مسجل لدينا',
            'password.required' => 'من فضلك أدخل كلمة المرور',
        ];
    }



    public function checkEmailRules()
    {
        return [
            'email' => 'required|email|exists:shops,email'
        ];
    }

    public function checkEmailMessages()
    {
        return [
            'email.required' => 'من فضلك أدخل البريد الإلكتروني',
            'email.email' => 'من فضلك أدخل بريد إلكتروني صالح',
            'email.exists' => 'عذرا البريد الإلكتروني غير مسجل لدينا',
        ];
    }


    public function resetPasswordRules()
    {
        return [
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required',
        ];
    }

    public function resetPasswordMessages()
    {
        return [
            'password.required' => 'من فضلك أدخل كلمة المرور',
            'password.confirmed' => 'عفوا كلمة المرور غير متطابقة',
            'password.min' => 'من فضلك أدخل كلمة المرور أكثر من 6 حروف',
            'password_confirmation.required' => 'من فضلك أدخل نأكيد كلمة المرور'
        ];
    }
}
