<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;

class ShopImage extends Model
{
    protected $fillable = ['shop_id','image'];

    public function getImageAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/shops').'/'.$image;
        }
        return "";
    }

    public function setImageAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'shops');
            $this->attributes['image'] = $imageFields;
        }
    }
}
