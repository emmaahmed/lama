<?php

namespace Modules\Shop\Entities;

use App\City;
use App\User;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Modules\Category\Entities\Category;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderItem;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\Rate;
use Spatie\Permission\Traits\HasRoles;
use Modules\Admin\Entities\Chat;
use Modules\Shop\Entities\Follower;

class Shop extends Authenticatable implements TranslatableContract
//class Shop extends Model implements TranslatableContract
{
    use Translatable;

    use Notifiable , HasRoles;

    protected $guard_name = 'shop';
    protected $with=['images'];

    public $translatedAttributes = ['name','short_description','long_description'];

    protected $appends = ['image','added_days'];
    public $preventAttrSet = false;

    protected $fillable = [
        'jwt',
        'category_id',
        'city_id',
        'parent_id',
        'type',                      // Super admin (parent) or sub admin (child)
        'email',
        'phone',
        'logo',
        'password',
        'rate',
        'status',
        'payment',                  //0=>cash, 1=>online, 2=>cash&online
        'min_order',
        'fast_delivery',
        'delivery_fees',
        'delivery_time',
        'from',
        'to',
        'online',
        'latitude',
        'longitude',
        'expire_at',
        'firebase_token',
        'firebase_web_token',
        'area',
        'delivery_types',
        'shop_owner_name',
        'notification_flag',
        'notification_time',
        'license',
        'commercial_register',
        'notification_start_date',
        'cover',
        'platform'



    ];

    protected $hidden = ['translations','created_at','updated_at','password', 'remember_token',];

    public function getRoleNames()
    {
        return $this->roles;
    }

    public function parent()
    {
        return $this->hasOne( Shop::class, 'id', 'parent_id' );
    }

    public function children()
    {
        return $this->hasMany( Shop::class, 'parent_id', 'id' );
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class,'city_id');
    }

    public function subCategories()
    {
        return $this->hasMany(SubCategory::class)->select('id');
    }
    public function products()
    {
        return $this->hasMany(Product::class)->select('id','image','price_before','price_after','selling');
    }

    public function shop_products()
    {
        return $this->hasMany(Product::class);
    }

    public function images()
    {
        return $this->hasMany(ShopImage::class);
    }

    public function getImageAttribute()
    {
        return $this->images()->first()?$this->images()->first()['image']:"";
    }

    public function getLogoAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/shops').'/'.$image;
        }
        return "";
    }

    public function setLogoAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'shops');
            $this->attributes['logo'] = $imageFields;
        }
        else{
            $this->attributes['logo'] = $image;

        }
    }
    public function getCoverAttribute($image)
    {
        if($image!=null && !filter_var($this->attributes['cover'], FILTER_VALIDATE_URL) && $this->attributes['cover']!=null ) {

            return asset('uploads/shops/covers/'.$image);
        }
        elseif($image!=null && filter_var($this->attributes['cover'], FILTER_VALIDATE_URL)) {
            return $this->attributes['cover'];

        }
        else{
            return "";
        }
    }

    public function setCoverAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'shops/covers');
            $this->attributes['cover'] = $imageFields;
        }
        else{
            $this->attributes['cover'] = $image;

        }
    }

    public function setPasswordAttribute($password)
    {
//        dd($this->preventAttrSet);
        if ($this->preventAttrSet) {
            $this->attributes['password'] = $password;

        }
        else{
            $this->attributes['password'] = Hash::make($password);

        }
    }

    public function getCreatedAtAttribute()
    {
        if($this->attributes['created_at']) {
            return Carbon::parse($this->attributes['created_at'])->diffForHumans();
        }
    }

    public function getFromAttribute()
    { if($this->attributes['from']) {
        return Carbon::createFromFormat('H:i:s', $this->attributes['from'])->format('G:i A');
    }
    }
    public function getToAttribute()
    {
        if($this->attributes['to']) {
            return Carbon::createFromFormat('H:i:s', $this->attributes['to'])->format('G:i A');
        }
    }
    public function setFromAttribute($value)
    {

        $this->attributes['from']= Carbon::createFromFormat('G:i A',$value)->format('H:i:s');
    }
    public function setToAttribute($value)
    {

        $this->attributes['to']= Carbon::createFromFormat('G:i A',$value)->format('H:i:s');
    }

    public function rates()
    {
        return $this->hasManyThrough(Rate::class, Product::class);
    }

    public function getRateAttribute()
     {
         $sum = 0;
         $rates = $this->rates()->get();
        if (sizeof($rates) > 0) {
            foreach ($rates as $rate) {
                $sum += $rate->rate;
            }
            return intVal($sum/sizeof($rates));
        }else{
            return 0;
        }
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function ordered_products()
    {
        return $this->hasManyThrough(OrderItem::class,Order::class);
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }

    public function messages()
    {
        return $this->hasMany(Chat::class,'shop_id', 'id');
    }

    public function followers()
    {
        return $this->hasMany(Follower::class, 'shop_id', 'id');
    }
    public function userFollowers()
    {
        return $this->hasManyThrough(User::class, Follower::class,'shop_id', 'id','id','user_id');
    }

    public function num_of_followers($shop_id)
    {
        return Follower::where('shop_id',$shop_id)->count();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'shop_id', 'id')->with('user');
    }

    public function getAddedDaysAttribute()
    {
        if ($this->attributes['expire_at']) {
            return Carbon::parse($this->attributes['expire_at'])->addDays(15)->format('Y-m-d');
        }
    }
    public function getPaymentAttribute($value)
    {
      return  explode(",",$value);

    }
    public function getDeliveryTypesAttribute($value)
    {
      return  explode(",",$value);

    }
    public function setCommercialRegisterAttribute($value)
    {if(is_file($value)) {
        $img_name = time() . uniqid() . '.' . $value->getClientOriginalExtension();
        $value->move(public_path('uploads/shops/commercials/'), $img_name);
        $this->attributes['commercial_register'] = $img_name;
    }
    else{
        $this->attributes['commercial_register'] = $value;

    }

    }

    public function getCommercialRegisterAttribute($value)
    {
        if($value!=null && !filter_var($this->attributes['commercial_register'], FILTER_VALIDATE_URL) && $this->attributes['commercial_register']!=null ) {

            return asset('/uploads/shops/commercials/'.$value);
        }
        elseif($value!=null && filter_var($this->attributes['commercial_register'], FILTER_VALIDATE_URL)) {
            return $this->attributes['commercial_register'];

        }
        else{
            return "";
        }
    }

    //
    public function setLicenseAttribute($value)
    {
        if(is_file($value)) {

            $img_name = time() . uniqid() . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('uploads/shops/licenses/'), $img_name);
            $this->attributes['license'] = $img_name;
        }else{
            $this->attributes['license'] = $value;

        }
    }

    public function getLicenseAttribute($value)
    {
        if($value!=null && !filter_var($this->attributes['license'], FILTER_VALIDATE_URL) && $this->attributes['license']!=null ) {

            return asset('/uploads/shops/licenses/'.$value);
        }
        elseif($value!=null && filter_var($this->attributes['license'], FILTER_VALIDATE_URL)) {
            return $this->attributes['license'];

        }
        else{
            return "";
        }

//        if($value)
//        {
//            return asset('/uploads/shops/licenses/'.$value);
//        }else{
//            return asset('/default.png');
//        }
    }

}
