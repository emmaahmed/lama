<?php

namespace Modules\Shop\Entities;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Product;

class SubCategory extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = [
        'shop_id','active'
    ];

    protected $hidden = ['translations','created_at','updated_at'];

    public function shop()
    {
        return $this->belongsTo(Shop::class,'shop_id');
    }
    public function products()
    {
        return $this->hasMany(Product::class)->select('id','price_before','price_after','selling');
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

}
