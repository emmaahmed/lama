<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;

class SubCategoryTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = ['sub_category_id','name','locale'];
}
