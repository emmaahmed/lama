<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;

class ShopTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = ['shop_id','name','short_description','long_description','locale'];
}
