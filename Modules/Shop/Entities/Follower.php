<?php

namespace Modules\Shop\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Follower extends Model
{
    use Notifiable;


    protected $table = 'followers';
    protected $primaryKey= 'id';

    protected $fillable = [
        'shop_id',
        'user_id'
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function followers($shop_id)
    {
        return $this->belongsTo(Shop::class, 'shop_id')
            ->where("shop_id",$shop_id)
            ->count();
    }






}
