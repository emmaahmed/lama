<?php

namespace Modules\Shop\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Comment extends Model
{
    use Notifiable;


    protected $table = 'comments';
    protected $primaryKey= 'id';

    protected $fillable = [
        'shop_id',
        'user_id',
        'rate',
        'comment'
    ];

    public function comments($shop_id)
    {
        return $this->belongsTo(Shop::class, 'shop_id')
            ->where("shop_id",$shop_id)
            ->get();
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }






}
