@include('shop::admin.layouts.includes.header')
    <div class="container-fluid p-0">
        <!-- login page start-->
        <div class="authentication-main">
            <div class="row">
                <div class="col-md-12">
                    <div class="auth-innerright">
                        <div class="authentication-box">
                            <div class="text-center"><img src="{{url('/assets/images/shop-icon.png')}}" style="width: 150px;" alt=""></div>
                            <div class="card mt-4">
                                <div class="card-body">
                                    <div class="text-center">
                                        <h4>تسجيل الدخول</h4>
                                        <h6>ادخل البريد الإلكتروني وكلمة المرور </h6>
                                    </div>
                                    <form class="needs-validation"  novalidate="" class="theme-form" method="POST" action="{{ route('shop.login') }}">
                                        @csrf
                                        <div class="form-group">
                                            <label class="col-form-label pt-0">البريد الإلكتروني</label>
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">كلمة المرور</label>
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
{{--                                        <div class="checkbox p-0" style="margin-right: 4%;">--}}
{{--                                            <input class="form-check-input" type="checkbox" name="remember" id="checkbox1" {{ old('remember') ? 'checked' : '' }}>--}}
{{--                                            <label for="checkbox1">تذكرني</label>--}}
{{--                                        </div>--}}
                                        <div class="form-group form-row mt-3 mb-0">
                                            <button class="btn btn-primary btn-block" type="submit">تسجيل الدخول</button>
                                        </div>
                                        <a class="btn btn-link" href="{{ url('shop/forget/password') }}">
                                            نسيت كلمة المرور ؟
                                        </a>
                                    </form>
                                </div>
                            </div>
                            <div class="text-center">
                                <img style="width: 120px" src="{{url('/assets/images/logo/grand.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- login page end-->
    </div>
@include('shop::admin.layouts.includes.footer')
