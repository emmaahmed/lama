<table class="display" id="basic-9">
    <thead>
    <tr>
        <th>اسم المنتج</th>
        <th>الكمية المباعة</th>
        <th>عدد المشترين</th>
        <th>اجمالي البيع</th>
    </tr>
    </thead>
    <tbody>
    @isset($data['shop']->products)
        @foreach($data['shop']->products as $product)
            <tr>
                <td>{{isset($product['product_name']) ? $product['product_name'] : ""}}</td>
                <td>{{isset($product['quantity']) ? $product['quantity'] : ""}}</td>
                <td>{{isset($product['num_of_users']) ? $product['num_of_users'] : ""}}</td>
                <td>{{isset($product['cost']) ? $product['cost'] : ""}}</td>
            </tr>
        @endforeach
        <tr class="odd">
            <td valign="top" colspan="4" class="dataTables_empty">
                <strong>
                    التكلفة الإجمالية :
                    {{isset($data['shop']->total_cost) ? $data['shop']->total_cost : ""}}
                </strong>
            </td>
        </tr>
    @endisset
    </tbody>
</table>
<div>

</div>