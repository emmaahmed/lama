@extends('shop::admin.layouts.master')
@section('styles')
    <!-- Plugins css start-->
    {{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
@endsection
@section('content')
    @if(Auth::guard('shop')->user()->can('show_statistics'))
{{--    <div class="page-header-right badge badge-danger">--}}
{{--        <span>:</span>--}}
{{--        <b>(  )</b>--}}
{{--    </div>--}}
    <div class="page-header-left">
        <button title="عرض التعليقات" type="button" class="btn btn-danger" style="padding: 2px"
                data-toggle="modal" data-target="#followers{{Auth::guard('shop')->user()->id}}">
             عدد المتابعين : {{$data['followers_count']}}
        </button>
        <div class="modal fade" id="followers{{Auth::guard('shop')->user()->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-12 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>كل التعليقات</h5>
                                    </div>
                                    <div class="card-body chart-block">
                                        <div class="table-responsive">
                                            <table class="display" id="basic-9">
                                                <thead>
                                                <tr>
                                                    <th>رقم </th>
                                                    <th >المتابع</th>
                                                    <th style="width: 15%;">تاريخ الإنشاء</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @isset(Auth::guard('shop')->user()->followers)
                                                    @foreach(Auth::guard('shop')->user()->followers as $follower)
                                                        <tr>

                                                            <td>{{isset($follower->id) ? $follower->id : ""}}</td>
                                                            <td>{{isset($follower->user) ? $follower->user->name : ""}}</td>
                                                            <td>{{isset($follower->created_at) ? $follower->created_at : ""}}</td>

                                                        </tr>

                                                    @endforeach
                                                @endisset
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="page-header-left">
        <button title="عرض التعليقات" type="button" class="btn btn-success" style="padding: 2px"
                data-toggle="modal" data-target="#comments{{Auth::guard('shop')->user()->id}}">
            التعليقات علي المتجر
        </button>
        <div class="modal fade" id="comments{{Auth::guard('shop')->user()->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-12 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>كل التعليقات</h5>
                                    </div>
                                    <div class="card-body chart-block">
                                        <div class="table-responsive">
                                            <table class="display" id="basic-9">
                                                <thead>
                                                <tr>
                                                    <th>رقم </th>
                                                    <th>التعليق</th>
                                                    <th >بواسطة</th>
                                                    <th style="width: 15%;">تاريخ الإنشاء</th>
{{--                                                    <th>العمليات</th>--}}

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @isset(Auth::guard('shop')->user()->comments)
                                                    @foreach(Auth::guard('shop')->user()->comments as $comment)
                                                        <tr>

                                                            <td>{{isset($comment->id) ? $comment->id : ""}}</td>
                                                            <td>{{isset($comment->comment) ? $comment->comment : ""}}</td>
                                                            <td>{{isset($comment->user) ? $comment->user->name : ""}}</td>
                                                            <td>{{isset($comment->created_at) ? $comment->created_at : ""}}</td>
                                                            <td>
                                                                <div class="row">




  {{--                                                                    <a onclick='return deleteComment({{$comment->id}})' title="حذف" data-id="{{$comment->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a>--}}
                                                                </div>
                                                            </td>

                                                        </tr>

                                                    @endforeach
                                                @endisset
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @endif
    <br>
    @if(Auth::guard('shop')->user()->can('show_product'))

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-xl-6 col-lg-6">
                <a href="{{route('shop.products')}}" title="المنتجات">
                    <div class="card o-hidden">
                        <div class="bg-default b-r-4 card-body">
                            <div class="media static-top-widget">
                                <div class="align-self-center text-center"><i data-feather="list"></i></div>
                                <div class="media-body"><span class="m-0">المنتجات</span>
                                    <h4 class="mb-0 ">{{$data['products']}}</h4><i class="icon-bg" data-feather="list"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-xl-6 col-lg-6">
                <a href="{{route('shop.orders')}}" title="الطلبات">
                    <div class="card o-hidden">
                        <div class="bg-default b-r-4 card-body">
                            <div class="media static-top-widget">
                                <div class="align-self-center text-center"><i data-feather="shopping-cart"></i></div>
                                <div class="media-body"><span class="m-0">الطلبات</span>
                                    <h4 class="mb-0 ">{{$data['orders']}}</h4><i class="icon-bg" data-feather="shopping-cart"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>عدد المنتجات</h5>
                    </div>
                    <div class="card-body chart-block">
                        <canvas id="productGraph"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>عدد الطلبات</h5>
                    </div>
                    <div class="card-body chart-block">
                        <canvas id="ordersGraph"></canvas>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @endif
    <!-- Container-fluid Ends-->
{{--    @if(Auth::guard('admin')->user()->can('show_order'))--}}
    <!-- Container-fluid starts-->
    @if(Auth::guard('shop')->user()->can('show_order'))

        <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>أخر الطلبات</h5>
                    </div>
                    <div class="card-body chart-block">
                        <div class="table-responsive">
                            <table class="display" id="basic-9">
                                <thead>
                                    <tr>
                                        <th>رقم الطلب</th>
                                        <th>صاحب الطلب</th>
                                        <th>السعر الكلي</th>
                                        <th>نوع الدفع</th>
                                        <th>نوع التوصيل</th>
                                        <th style="width: 25%;">الحالة</th>
                                        <th style="width: 15%;">تاريخ الإنشاء</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @isset($data['latest_orders'])
                                    @foreach($data['latest_orders'] as $key => $order)
                                        <tr>
                                            <td>
                                                <a href="{{route('shop.orders.details',['order_id' => $order->id])}}" title="تفاصيل الطلب">
                                                    {{isset($order->order_number) ? $order->order_number : ""}}
                                                </a>
                                            </td>
                                            <td>{{isset($order->user) ? $order->user->name : ""}}</td>
                                            <td>{{isset($order->total_cost) ? $order->total_cost : ""}}</td>
                                            <td>
                                                @if($order->payment_type == 0)
                                                    <span class="badge badge-primary">كاش</span>
                                                @elseif($order->payment_type == 1)
                                                    <span class="badge badge-success">اونلاين</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($order->delivery_type == 0)
                                                    <span class="badge badge-primary">توصيل عادي</span>
                                                @elseif($order->delivery_type == 1)
                                                    <span class="badge badge-success">توصيل سريع</span>
                                                @endif
                                            </td>
                                            <td>
                                                <select @if($order->delivery_type == 1) disabled @endif name="order_status" class="custom-select form-control order_status" data-order-id="{{$order->id}}" data-shop-id="{{$order->shop->id}}">
                                                    <option value="0" @if($order->status == 0) selected @endif>قيد الإنشاء</option>
                                                    <option value="1" @if($order->status == 1) selected @endif>قبول الطلب</option>
                                                    <option value="2" @if($order->status == 2) selected @endif>في الطريق</option>
                                                    <option value="3" @if($order->status == 3) selected @endif>تم التوصيل</option>
                                                    <option value="4" @if($order->status == 4) selected @endif>ملغي</option>
                                                </select>
                                            </td>
                                            <td>{{isset($order->created_at) ? $order->created_at : ""}}</td>
                                        </tr>
                                    @endforeach
                                @endisset
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <!-- Container-fluid Ends-->
{{--    @endif--}}
@endsection


@section('scripts')
    <!-- Plugins JS start-->
{{--        <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--        <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
    <script src="{{ url('assets/js/chart/chartjs/chart.min.js') }}"></script>
    <script src="{{ url('assets/js/chat-menu.js') }}"></script>
    <script>
        $('.order_status').change(function () {
            var order_id = $(this).attr('data-order-id');
            var status = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('shop.orders.status')}}",
                data: {
                    order_id: order_id,
                    status: status,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response){
                    // $('.order_status option').prop('disabled', false);
                    // var index = $(this).find('option:selected').index();
                    // $('select').not(this).find('option:lt(' + index + ')').prop('disabled', true);
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });
    </script>

    <script>
        Chart.defaults.global = {
            animation: true,
            animationSteps: 60,
            animationEasing: "easeOutIn",
            showScale: true,
            scaleOverride: false,
            scaleSteps: null,
            scaleStepWidth: null,
            scaleStartValue: null,
            scaleLineColor: "#eeeeee",
            scaleLineWidth: 1,
            scaleShowLabels: true,
            scaleLabel: "<%=value%>",
                scaleIntegersOnly: true,
                scaleBeginAtZero: false,
                scaleFontSize: 12,
                scaleFontStyle: "normal",
                scaleFontColor: "#717171",
                responsive: true,
                maintainAspectRatio: true,
                showTooltips: true,
                multiTooltipTemplate: "<%= value %>",
                tooltipFillColor: "#333333",
                tooltipEvents: ["mousemove", "touchstart", "touchmove"],
                tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
                tooltipFontSize: 14,
                tooltipFontStyle: "normal",
                tooltipFontColor: "#fff",
                tooltipTitleFontSize: 16,
                TitleFontStyle : "Raleway",
                tooltipTitleFontStyle: "bold",
                tooltipTitleFontColor: "#ffffff",
                tooltipYPadding: 10,
                tooltipXPadding: 10,
                tooltipCaretSize: 8,
                tooltipCornerRadius: 6,
                tooltipXOffset: 5,
                onAnimationProgress: function() {},
                onAnimationComplete: function() {}
            };
        var lineGraphData = {
            labels: [
                   '{{Carbon\Carbon::now()->subDays(7)->format('d/m/Y')}}',
                   '{{Carbon\Carbon::now()->subDays(6)->format('d/m/Y')}}',
                   '{{Carbon\Carbon::now()->subDays(5)->format('d/m/Y')}}',
                   '{{Carbon\Carbon::now()->subDays(4)->format('d/m/Y')}}',
                   '{{Carbon\Carbon::now()->subDays(3)->format('d/m/Y')}}',
                   '{{Carbon\Carbon::now()->subDays(2)->format('d/m/Y')}}',
                   '{{Carbon\Carbon::now()->subDays(1)->format('d/m/Y')}}',
                   'اليوم'
               ],
            datasets: [{
                label: "My First dataset",
                fillColor: "rgba(68, 102, 242, 0.3)",
                strokeColor: "#4466f2",
                pointColor: "#4466f2",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "#000",
                data: [
                   {{$data['product_statistics']['7dayago']}},
                   {{$data['product_statistics']['6dayago']}},
                   {{$data['product_statistics']['5dayago']}},
                   {{$data['product_statistics']['4dayago']}},
                   {{$data['product_statistics']['3dayago']}},
                   {{$data['product_statistics']['2dayago']}},
                   {{$data['product_statistics']['1dayago']}},
                   {{$data['product_statistics']['today']}}
                ]
            }]
        };
        var lineGraphOptions = {
            scaleShowGridLines: true,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            scaleShowHorizontalLines: true,
            scaleShowVerticalLines: true,
            bezierCurve: true,
            bezierCurveTension: 0.4,
            pointDot: true,
            pointDotRadius: 4,
            pointDotStrokeWidth: 1,
            pointHitDetectionRadius: 20,
            datasetStroke: true,
            datasetStrokeWidth: 2,
            datasetFill: true,
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
        };
        var lineCtx = document.getElementById("productGraph").getContext("2d");
        var myLineCharts = new Chart(lineCtx).Line(lineGraphData, lineGraphOptions);


        </script>

        <script>
            Chart.defaults.global = {
                animation: true,
                animationSteps: 60,
                animationEasing: "easeOutIn",
                showScale: true,
                scaleOverride: false,
                scaleSteps: null,
                scaleStepWidth: null,
                scaleStartValue: null,
                scaleLineColor: "#eeeeee",
                scaleLineWidth: 1,
                scaleShowLabels: true,
                scaleLabel: "<%=value%>",
                scaleIntegersOnly: true,
                scaleBeginAtZero: false,
                scaleFontSize: 12,
                scaleFontStyle: "normal",
                scaleFontColor: "#717171",
                responsive: true,
                maintainAspectRatio: true,
                showTooltips: true,
                multiTooltipTemplate: "<%= value %>",
                tooltipFillColor: "#333333",
                tooltipEvents: ["mousemove", "touchstart", "touchmove"],
                tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
                tooltipFontSize: 14,
                tooltipFontStyle: "normal",
                tooltipFontColor: "#fff",
                tooltipTitleFontSize: 16,
                TitleFontStyle : "Raleway",
                tooltipTitleFontStyle: "bold",
                tooltipTitleFontColor: "#ffffff",
                tooltipYPadding: 10,
                tooltipXPadding: 10,
                tooltipCaretSize: 8,
                tooltipCornerRadius: 6,
                tooltipXOffset: 5,
                onAnimationProgress: function() {},
                onAnimationComplete: function() {}
            };
        var lineGraphData = {
            labels: [
                   '{{Carbon\Carbon::now()->subDays(7)->format('d/m/Y')}}',
                   '{{Carbon\Carbon::now()->subDays(6)->format('d/m/Y')}}',
                   '{{Carbon\Carbon::now()->subDays(5)->format('d/m/Y')}}',
                   '{{Carbon\Carbon::now()->subDays(4)->format('d/m/Y')}}',
                   '{{Carbon\Carbon::now()->subDays(3)->format('d/m/Y')}}',
                   '{{Carbon\Carbon::now()->subDays(2)->format('d/m/Y')}}',
                   '{{Carbon\Carbon::now()->subDays(1)->format('d/m/Y')}}',
                   'اليوم'
               ],
            datasets: [{
                label: "My First dataset",
                fillColor: "rgba(68, 102, 242, 0.3)",
                strokeColor: "#4466f2",
                pointColor: "#4466f2",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "#000",
                data: [
                   {{$data['order_statistics']['7dayago']}},
                   {{$data['order_statistics']['6dayago']}},
                   {{$data['order_statistics']['5dayago']}},
                   {{$data['order_statistics']['4dayago']}},
                   {{$data['order_statistics']['3dayago']}},
                   {{$data['order_statistics']['2dayago']}},
                   {{$data['order_statistics']['1dayago']}},
                   {{$data['order_statistics']['today']}}
                ]
            }]
        };
        var lineGraphOptions = {
            scaleShowGridLines: true,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            scaleShowHorizontalLines: true,
            scaleShowVerticalLines: true,
            bezierCurve: true,
            bezierCurveTension: 0.4,
            pointDot: true,
            pointDotRadius: 4,
            pointDotStrokeWidth: 1,
            pointHitDetectionRadius: 20,
            datasetStroke: true,
            datasetStrokeWidth: 2,
            datasetFill: true,
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
        };
        var lineCtx = document.getElementById("ordersGraph").getContext("2d");
        var myLineCharts = new Chart(lineCtx).Line(lineGraphData, lineGraphOptions);


        </script>
    <script>
        function deleteComment(comment_id)
        {
            var id = comment_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('shop.comments.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {comment_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
            //
        }


    </script>
    @endsection
