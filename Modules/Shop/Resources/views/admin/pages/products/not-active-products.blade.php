@extends('shop::admin.layouts.master')
@section('styles')
    <!-- Plugins css start-->
    {{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
    {{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
                    {{--                    <div class="card-header">--}}
                    {{--                        <h5>{{$data['title']}}</h5>--}}
                    {{--                    </div>--}}

                    <div class="card-body">

                        <div class="table-responsive">
                            {{--                            <table class="display" id="basic-9">--}}
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    @if(Auth::guard('shop')->user()->can('add_product'))
                                        <a class="btn btn-square btn-success" href="{{route('shop.products.create')}}"
                                           title="إضافة"> إضافة منتج جديد</a> &nbsp; &nbsp;
                                    @endif


                                    @if(Auth::guard('shop')->user()->can('delete_product'))
                                        <button class="btn btn-square btn-danger" style="float: left; margin-right: 2%;"
                                                id="bulk_delete_btn" type="button" title="حذف متعدد">حذف متعدد
                                        </button>
                                    @endif
                                    <div class="row">
                                        <form method="post" enctype="multipart/form-data" action="{{route('shop.shops.products.search')}}">
                                            @csrf
                                            <div class="col-lg-6">
                                                <input type="hidden" value="0" name="status">
                                                <input name="keySearch" class="form form-control"
                                                       type="text" placeholder="بحث في كل المنتجات">
                                            </div>
                                            <div class="col-lg-4">
                                                <button class="btn btn-square btn-success " style=""
                                                        type="submit" title="بحث">بحث</button>
                                            </div>
                                        </form>
                                    </div>
                                    <input type="hidden" name="myids" id="bulk_delete_input" value="">

                                    <th>
                                        <label class="d-block" for="chk-ani">
                                            <input class="checkbox_animated select_all" id="chk-ani" type="checkbox">
                                        </label>
                                    </th>
                                    <th>صورة المنتج</th>
                                    {{--<th>القسم التابع له</th>--}}
                                    <th>الإسم</th>
                                    <th>الوصف</th>
                                    <th>الكمية</th>
                                    <th>السعر قبل الخصم</th>
                                    <th>السعر بعد الخصم</th>
                                    <th>الحالة</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['products'] as $product)
                                    <tr>
                                        <td>
                                            <label class="d-block" for="chk-ani">
                                                <input type="checkbox" class="checkbox_animated" name="row_id"
                                                       id="checkbox_{{ $product->id }}" value="{{ $product->id }}">
                                            </label>
                                        </td>
                                        <td class="col" style="width: auto;"><img class="img-60 rounded-circle"
                                                                                  src="{{!empty($product->image) ? $product->image : url('assets/images/default.png')}}"
                                                                                  alt="#" data-original-title=""
                                                                                  title=""></td>
                                        {{--                                        <td>{{isset($product->subCategory) ? $product->subCategory->name : ""}}</td>--}}
                                        <td class="col"
                                            style="width: auto;">{{isset($product->name) ? $product->name : ""}}</td>
                                        <td class="col"
                                            style="width: auto;">{{isset($product->short_description) ? $product->short_description : ""}}</td>
                                        <td class="col"
                                            style="width: auto;">{{isset($product->quantity) ? $product->quantity : ""}}</td>
                                        <td class="col"
                                            style="width: auto;">{{isset($product->price_before) ? $product->price_before : ""}}</td>
                                        <td class="col"
                                            style="width: auto;">{{isset($product->price_after) ? $product->price_after : $product->price_before}}</td>
                                        <td class="col" style="width: auto;">
                                            <div class="form-group" style="margin-top: 1rem;">
                                                <div class="media-body icon-state switch-outline">
                                                    <label class="switch">
                                                        <input type="checkbox" id="status" class="product_status"
                                                               data-product-id="{{$product->id}}" name="status"
                                                               @if($product->status == 1) checked @endif><span
                                                            class="switch-state bg-success"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="col" style="width: auto;">
                                            <div class="row">
                                                @if(Auth::guard('shop')->user()->can('edit_product'))
                                                    <a title="تعديل"
                                                       href="{{route('shop.products.edit',['product_id' => $product->id])}}"><i
                                                            width="20" height="20" color="green"
                                                            data-feather="edit"></i></a> &nbsp;
                                                @endif
                                                @if(Auth::guard('shop')->user()->can('delete_product'))
                                                    <a onclick='return deleteProduct({{$product->id}})' title="حذف"
                                                       data-id="{{$product->id}}" href="#"><i width="20" height="20"
                                                                                              color="red"
                                                                                              data-feather="trash-2"></i></a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                        {{$data['products']->links()}}
                        <br>
                        <form action="{{route('shop.products.import')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                                <div class="col-lg-3">

                                    <input type="file" required name="excel">
                                </div>
                                <div class="col-lg-4">

                                    <input type="submit" class="btn btn-square btn-success " value="استيراد ملف المنتجات ">
                                </div>
                            </div>


                        </form>
                        <br>

                    </div>

                </div>
            </div>
            <!-- State saving Ends-->

        </div>
    </div>

    <!-- Container-fluid Ends-->
@endsection


@section('scripts')
    <!-- Plugins JS start-->
    {{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
    {{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
    {{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>
        $('.product_status').on('change.bootstrapSwitch', function (e) {
            var product_id = $(this).attr('data-product-id');
            var status = "";
            if (e.target.checked == true) {
                var status = 1;
            } else {
                var status = 0;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('shop.products.status')}}",
                data: {
                    product_id: product_id,
                    status: status,
                    "_token": "{{ csrf_token() }}"
                },
                success: function (response) {
                    toastr.success(response.success);
                },
                error: function (jqXHR) {
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });

        function deleteProduct(product_id) {
            var id = product_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('shop.products.delete')}}",
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {product_id: id, "_token": "{{ csrf_token() }}"},

                        success: function (response) {
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function () {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
            //
        }

        $(document).ready(function () {
            $('.select_all').on('click', function (e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        if (value !== 'on') {
                            myids.push(value);
                        }
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('shop.products.deleteMulti')}}",
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {ids: myids, "_token": "{{ csrf_token() }}"},

                        success: function (response) {
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function (jqXHR) {
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

            // End Bulk Delete
        });
    </script>
@endsection
