@extends('shop::admin.layouts.master')

@section('styles')
    <style>
        /*body {*/
            /*padding: 20px;*/
        /*}*/
        .image-area {
            position: relative;
            width: 15%;
            background: #fff;
            margin-bottom: 1%;
        }
        .image-area img{
            width: 100%;
            height: 75%;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            /*background: #E54E4E;*/
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }
    </style>
@endsection

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$data['title']}}</h5>
                    </div>
                    <form class="needs-validation" enctype="multipart/form-data" id="update-form" novalidate="" method="POST" action="{{route('shop.products.update')}}" >
                        @csrf
                        <input type="hidden" name="product_id" value="{{$data['product']->id}}"/>
                        <div class="card-body">
                            {{--<div class="text-center">--}}
                                {{--<img id="image-display" width="200px" height="70px" class="profile-user-img img-fluid img-circle" style="border: 1px solid #e5eaff;"--}}
                                     {{--alt="صورة المتجر" src="{{$data['product']->image ? $data['product']->image : url('assets/images/default.png')}}" >--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="sub_category_id">القسم
                                            <span class="valid-feedback" style="display: inline;">(اختياري)</span>
                                        </label>
                                        <select name="sub_category_id" class="custom-select form-control" id="sub_category_id">
                                            <option value="" disabled selected>اختر القسم</option>
                                            @foreach($data['subCategories'] as $sub_category)
                                                <option value="{{$sub_category->id}}"  @if($data['product']->sub_category_id == $sub_category->id) selected @endif>{{$sub_category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">إسم المنتج باللغة العربية</label>
                                        <input class="form-control btn-square" name="name_ar" value="{{isset($data['product']->translate('ar')->name) ? $data['product']->translate('ar')->name : ""}}" id="name_ar" type="text" placeholder="إسم المنتج باللغة العربية" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="name_en">إسم المنتج باللغة الإنجليزية</label>--}}
                                        {{--<input class="form-control btn-square" name="name_en" value="{{isset($data['product']->translate('en')->name) ? $data['product']->translate('en')->name : ""}}" id="name_en" type="text" placeholder="إسم المنتج باللغة الإنجليزية" required>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="short_description_ar">الوصف المختصر (باللغة العربية)</label>
                                        <textarea class="form-control" name="short_description_ar" id="short_description_ar" cols="20" rows="4" required>
                                            {{isset($data['product']->translate('ar')->short_description) ? $data['product']->translate('ar')->short_description : ""}}
                                        </textarea>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-form-label float-right" for="short_description_en">الوصف المختصر (باللغة الانجليزية)</label>--}}
                                        {{--<textarea class="form-control" name="short_description_en" id="short_description_en" cols="20" rows="4" required>--}}
                                            {{--{{isset($data['product']->translate('en')->short_description) ? $data['product']->translate('en')->short_description : ""}}--}}
                                        {{--</textarea>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="long_description_ar">الوصف الكامل (باللغة العربية)</label>
                                        <textarea class="form-control" name="long_description_ar" id="long_description_ar" cols="20" rows="4" required>
                                            {{isset($data['product']->translate('ar')->long_description) ? $data['product']->translate('ar')->long_description : ""}}
                                        </textarea>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-form-label float-right" for="long_description_en">الوصف الكامل (باللغة الانجليزية)</label>--}}
                                        {{--<textarea class="form-control" name="long_description_en" id="long_description_en" cols="20" rows="4" required>--}}
                                            {{--{{isset($data['product']->translate('en')->long_description) ? $data['product']->translate('en')->long_description : ""}}--}}
                                        {{--</textarea>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="quantity">الكمية المتاحة</label>
                                        <input class="form-control btn-square" name="quantity" id="quantity" value="{{isset($data['product']->quantity) ? $data['product']->quantity : ""}}" type="number" placeholder="الكمية المتاحة" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="price_before">سعر المنتج (قبل الخصم)</label>
                                        <input class="form-control btn-square" name="price_before" id="price_before" type="number" step="any" placeholder="سعر المنتج (قبل الخصم)" value="{{isset($data['product']->price_before) ? $data['product']->price_before : ""}}" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="price_after">سعر المنتج (بعد الخصم)</label>
                                        <input class="form-control btn-square" name="price_after" id="price_after" type="number" step="any" placeholder="سعر المنتج (بعد الخصم)" value="{{isset($data['product']->price_after) ? $data['product']->price_after : ""}}" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="percent">نسبة الخصم (%)</label>
                                        <input class="form-control btn-square" name="percent" id="percent" type="text" placeholder="0 %" value="{{isset($data['product']->percent) ? $data['product']->percent : ""}}" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="card">
                                <div class="card-header">
                                    <h5>صور المنتج</h5>
                                </div>
                                <div class="card-body shadow-sm p-3 mb-5 bg-white rounded">
                                    <div class="row" id="images">
                                        @foreach($data['product']->images as $image)
                                            <div class="image-area col-md-3">
                                                <img src="{{$image->image}}"  alt="{{$image->image}}"  class="img-thumbnail img-fluid">
                                                {{--@if(!$loop->first)--}}
                                                    <a class="remove-image" href="{{route('shop.products.images.remove', ['product_id' => $data['product']->id,'image_id' => $image->id])}}" style="display: inline;">&#215;</a>
                                                {{--@endif--}}
                                            </div>
                                        @endforeach
{{--                                            <div class="image-area col-md-3">--}}
{{--                                                <img src=""  alt="" id="image"  class="img-thumbnail img-fluid">--}}
{{--                                            </div>--}}
                                    </div>
                                    <br/>
                                    <br/>
                                    <div class="row">
                                        <div class="col">
                                            <div class="custom-file">
                                                <input class="custom-file-input" name="images[]" id="images" multiple type="file" data-original-title="" title="">
                                                <label class="custom-file-label" for="images">صور المنتج</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--                            Variations      --}}
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <button style="border-radius: 0;" type="button" id="VariationAdd" class="btn btn-success" data-var-length="@if (isset($data['product']->variations)){{sizeof($data['product']->variations)}}@else 0 @endif">
                                          إضافة اختيار
                                        </button>
                                        <br/>
                                        <br/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div id="VariationsContainer">
                                        @isset($data['product']->variations)
                                            @foreach($data['product']->variations as $x => $variation)
                                                <div style="border-top: 3px solid #f6f7fb;"></div>
                                                <div class="form-group row" style="margin-top: 2%;">
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control variation_ar" min="1" name="variations[{{$x}}][ar]" placeholder="إسم الإختيار باللغة العربية" value="{{isset($variation->translate('ar')->name) ? $variation->translate('ar')->name : ""}}" required>
                                                </div>
                                                {{--<div class="col-md-5">--}}
                                                    {{--<input type="text" class="form-control float-type variation_en" min="1" name="variations[{{$x}}][en]" placeholder="إسم الإختيار باللغة الانجليزية" value="{{isset($variation->translate('en')->name) ? $variation->translate('en')->name : ""}}" required>--}}
                                                {{--</div>--}}
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <select name="variations[{{$x}}][type]" class="custom-select form-control type">
                                                            <option value="" disabled selected>اختر النوع</option>
                                                            <option value="0" @if($variation->type == 0) selected @endif>اختيار واحد</option>
                                                            <option value="1" @if($variation->type == 1) selected @endif>اختيار متعدد</option>
                                                            <option value="2" @if($variation->type == 2) selected @endif>لون</option>
                                                            <option value="3" @if($variation->type == 3) selected @endif>حجم</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <label class="d-block" for="check-required">
                                                        <input type="checkbox" class="checkbox_animated required" @if($variation->required == 1) checked @endif name="variations[{{$x}}][required]" id="check-required" value="1">
                                                        <label for="check-required">إجباري</label>
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <button type="button" class="btn btn-xs btn-danger variation-remove-btn" style="margin:0; border-radius: 0;"><i data-feather="x"></i></button>
                                                </div>
                                                <br/>
                                                <br/>
                                                <br/>
                                                <div class="col-md-2" style="margin-right: 35%;">
                                                    <button style="border-radius: 0;" type="button" class="btn btn-success option_btn OptionAdd" data-var-id="{{$x}}"> إضافة جديدة</button>
                                                </div>

                                                <br/>
                                                <br/>
                                                <br/>
{{--                                                <div class="col-md-12 row" style="margin-right: 1%;">--}}
{{--                                                    <div class="col">--}}
                                                        <div id="OptionsContainer" style="margin-right: 3%;">
                                                            <input type="hidden" class="options_length" data-var-length="@if (isset($variation->options)){{sizeof($variation->options)}}@else 0 @endif"/>
                                                            @foreach($variation->options as $y => $option)
                                                                <div class="form-group row">
                                                                    <div class="col-md-4">
                                                                        <input type="text" class="form-control option_ar" min="0" name="variations[{{$x}}][options][{{$y}}][ar]" placeholder="إسم الإضافة باللغة العربية" value="{{isset($option->translate('ar')->name) ? $option->translate('ar')->name : ""}}" required>
                                                                    </div>
                                                                    {{--<div class="col-md-4">--}}
                                                                        {{--<input type="text" class="form-control float-type option_en" min="1" name="variations[{{$x}}][options][{{$y}}][en]" placeholder="إسم الإضافة باللغة الانجليزية" value="{{isset($option->translate('en')->name) ? $option->translate('en')->name : ""}}" required>--}}
                                                                    {{--</div>--}}
                                                                    <div class="col-md-3">
                                                                        <input type="number"  step="0.01" class="form-control float-type option_price" min="1" name="variations[{{$x}}][options][{{$y}}][price]" placeholder="سعر الإضافة" value="{{$option->price}}" required>
                                                                    </div>
                                                                    @if($variation->type == 2)
                                                                    <div class="col-md-2">
                                                                        {{--                                    <input type="number" class="form-control float-type option_price" name="variations[x][options][y][price]" min="1" placeholder="سعر الإضافة" required>--}}
                                                                        <input type="color" name="variations[{{$x}}][options][{{$y}}][color]" class="form-control float-type option_color" value="{{isset($option->color) ? $option->color : ""}}">
                                                                    </div>
                                                                    @endif
                                                                    <div class="col-md-2">
                                                                        <label class="d-block" for="check-status">
                                                                            <input type="checkbox" class="checkbox_animated status" name="variations[{{$x}}][options][{{$y}}][status]" id="check-status" @if($option->status == 1) checked @endif value="1">
                                                                            <label for="check-status">متاح</label>
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <button type="button" class="btn btn-xs btn-danger option-remove-btn" style="margin:0; border-radius: 0;"><i data-feather="x"></i></button>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                            </div>
                                            @endforeach
                                        @endisset
                                    </div>
                                </div>
                            </div>
                            <div class="row border">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 style="text-align: center;">ملحوظة : هذه المعلومات إختيارية وليست إجبارية</h5>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <button style="border-radius: 0;" type="button" id="InfoAdd" class="btn btn-info" data-var-length="@if (isset($data['product']->other_info)){{sizeof($data['product']->other_info)}}@else 0 @endif"> معلومات إضافية <span class="fa fa-plus-circle"></span></button>
                                                        <br/>
                                                        <br/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div id="InfosContainer">
                                                        @isset($data['product']->other_info)
                                                            @foreach($data['product']->other_info as $x => $info)
                                                                <div style="border-top: 3px solid #f6f7fb;"></div>
                                                                <div class="form-group row" style="margin-top: 2%;">
                                                                    <div class="col-md-3">
                                                                        <input type="text" class="form-control text1" name="info[{{$x}}][text1]" placeholder="النص الأول" value="{{isset($info->translate('ar')->text1) ? $info->translate('ar')->text1 : ""}}" required>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <input type="text" class="form-control text2" name="info[{{$x}}][text2]" placeholder="النص الثاني" value="{{isset($info->translate('ar')->text2) ? $info->translate('ar')->text2 : ""}}" required>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <input type="text" class="form-control text3" name="info[{{$x}}][text3]" placeholder="النص الثالث" value="{{isset($info->translate('ar')->text3) ? $info->translate('ar')->text3 : ""}}" required>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <button type="button" class="btn btn-xs btn-danger info-remove-btn" style="margin:0;"><i data-feather="x"></i></button>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endisset
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Variations Shell --}}

                            <div id="VariationShell" class="form-group row add" style="display: none;">
                                <div class="col-md-5">
                                    <input type="text" class="form-control variation_ar" min="1" placeholder="إسم الإختيار باللغة العربية" required>
                                </div>
                                {{--<div class="col-md-5">--}}
                                    {{--<input type="text" class="form-control float-type variation_en" min="1" placeholder="إسم الإختيار باللغة الانجليزية" required>--}}
                                {{--</div>--}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select name="type" class="custom-select form-control type">
                                            <option value="" disabled selected>اختر النوع</option>
                                            <option value="0">اختيار واحد</option>
                                            <option value="1">اختيار متعدد</option>
                                            <option value="2">لون</option>
                                            <option value="3">حجم</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="d-block" for="check-required">
                                        <input type="checkbox" class="checkbox_animated required" name="required" id="check-required" value="1">
                                        <label for="check-required">إجباري</label>
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-xs btn-danger variation-remove-btn" style="margin:0;"><i data-feather="x"></i></button>
                                </div>
                                <br/>
                                <br/>
                                <br/>
                                <div class="col-md-2" style="margin-right: 35%;">
                                    <button type="button" class="btn btn-success option_btn OptionAdd" data-var-id="x"> إضافة جديدة<span class="fa fa-plus-circle"></span></button>
                                </div>

                                <br/>
                                <br/>
                                <br/>
                                <div class="col-md-12 row" style="margin-right: 1%;">
                                    <div class="col">
                                        <div id="OptionsContainer">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Options Shell --}}

                            <div id="OptionShell" class="form-group row" style="display: none; margin-right: 1%;">
                                <div class="col-md-4">
                                    <input type="text" class="form-control option_ar" min="1" placeholder="إسم الإضافة باللغة العربية" required>
                                </div>
                                {{--<div class="col-md-4">--}}
                                    {{--<input type="text" class="form-control float-type option_en" min="1" placeholder="إسم الإضافة باللغة الانجليزية" required>--}}
                                {{--</div>--}}
                                <div class="col-md-3">
                                    <input type="number" step="0.01" class="form-control float-type option_price" min="1" placeholder="سعر الإضافة" required>
                                </div>
                                <div class="col-md-2">
                                    {{--                                    <input type="number" class="form-control float-type option_price" name="variations[x][options][y][price]" min="1" placeholder="سعر الإضافة" required>--}}
                                    <input type="color" class="form-control float-type option_color" >
                                </div>
                                <div class="col-md-2">
                                    <label class="d-block" for="check-status">
                                        <input type="checkbox" class="checkbox_animated status" name="status" id="check-status" value="1">
                                        <label for="check-status">متاح</label>
                                    </label>
                                </div>

                                <div class="col-md-1">
                                    <button type="button" class="btn btn-xs btn-danger option-remove-btn" style="margin:0;"><i data-feather="x"></i></button>
                                </div>
                                <br/>
                                <br/>
                            </div>

                            {{-- Other Info Shell --}}

                            <div id="InfoShell" class="form-group row add" style="display: none;">
                                <div class="col-md-3">
                                    <input type="text" class="form-control text1" placeholder="النص الأول" required>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control text2" placeholder="النص الثاني" required>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control text3" placeholder="النص الثالث" required>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-xs btn-danger info-remove-btn" style="margin:0;"><i data-feather="x"></i></button>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" style="width: 100%; border-radius: 0;" type="submit"  id="update-button">حفط</button>
                            {{--<input class="btn btn-light" type="reset" value="إلغاء">--}}
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <script>
        $(function() {
            $("#price_after, #price_before").keyup(function() { // input on change
                var result = 100 - parseFloat(parseInt($("#price_after").val(), 10) * 100)/ parseInt($("#price_before").val(), 10);
                $('#percent').val(Math.round(result)||'0'); //shows value in "#percent"
            })
        });
    </script>
    <script>
        // function readURL(input) {
        //     if (input.files && input.files[0]) {
        //         var reader = new FileReader();
        //
        //         reader.onload = function(e) {
        //             $('#image-display').attr('src', e.target.result);
        //         }
        //         reader.readAsDataURL(input.files[0]);
        //     }
        // }
        //
        // $("#image").change(function() {
        //     readURL(this);
        // });
    </script>
    <script>
        $('#update-button').on('click',function () {
            $('#update-form').submit();
        })
    </script>

    {{--     for explaination feel free to call me on 01009313883 --}}
    <script>
        var i = $('#VariationAdd').data('var-length');

        $("#VariationAdd").click(function(){
            $("#VariationShell").clone(true).attr('id','VariationShell'+i).appendTo('#VariationsContainer').fadeIn();
            // $("#VariationShell").clone(true).attr('id','VariationShell'+i).appendTo($(this).closest('.form-group')).fadeIn();

            $('#VariationShell'+i).find('input.variation_ar').attr('name', 'variations['+i+'][ar]');
            // $('#VariationShell'+i).find('input.variation_en').attr('name', 'variations['+i+'][en]');
            $('#VariationShell'+i).find('select.type').attr('name', 'variations['+i+'][type]');
            $('#VariationShell'+i).find('input.required').attr('name', 'variations['+i+'][required]');
            $('#VariationShell'+i).find('button.option_btn').data('var-id', i);

            i++;
        });
        $('body').on('click', '.variation-remove-btn', function(){
            $(this).closest('.form-group').remove();
        });
        // Options
        // var y = 0;
        var y = $('.options_length').data('var-length') + 1;
        $(".OptionAdd").click(function(){

            var x = $(this).data('var-id');
            $("#OptionShell").clone(true).attr('id','OptionShell'+y).appendTo($(this).closest('.row')).fadeIn();
            $('#OptionShell'+y).find('input.option_ar').attr('name', 'variations['+x+'][options]['+y+'][ar]');
            // $('#OptionShell'+y).find('input.option_en').attr('name', 'variations['+x+'][options]['+y+'][en]');
            $('#OptionShell'+y).find('input.option_price').attr('name', 'variations['+x+'][options]['+y+'][price]');
            $('#OptionShell'+y).find('input.status').attr('name', 'variations['+x+'][options]['+y+'][status]');
            if($('select[name="variations['+x+'][type]"]').val() == 2){
                // $('.option_color').css('display','');
                //alert('test');
                $('#OptionShell'+y).find('input.option_color').attr('name', 'variations['+x+'][options]['+y+'][color]');
            }else{
                $('#OptionShell'+y).find('input.option_color').attr('name', 'variations['+x+'][options]['+y+'][color]');
                $('#OptionShell'+y).find('input.option_color').css('display','none');
            }
            y++;
        });
        $('body').on('click', '.option-remove-btn', function(){
            $(this).closest('.form-group').remove();
        });

    //    Other Info

        var k = $('#InfoAdd').data('var-length');

        $("#InfoAdd").click(function(){
            $("#InfoShell").clone(true).attr('id','InfoShell'+k).appendTo('#InfosContainer').fadeIn();
            $('#InfoShell'+k).find('input.text1').attr('name', 'info['+k+'][text1]');
            $('#InfoShell'+k).find('input.text2').attr('name', 'info['+k+'][text2]');
            $('#InfoShell'+k).find('input.text3').attr('name', 'info['+k+'][text3]');

            k++;
        });
        $('body').on('click', '.info-remove-btn', function(){
            $(this).closest('.form-group').remove();
        });
    </script>
@endsection

