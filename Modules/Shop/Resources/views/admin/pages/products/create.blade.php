@extends('shop::admin.layouts.master')
@section('styles')
    <style>
        #field {
            margin-bottom:20px;
        }
    </style>
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$data['title']}}</h5>
                    </div>
                    <form class="needs-validation" id="create-form"  novalidate="" method="POST" action="{{route('shop.products.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="sub_category_id">القسم
                                            <span class="valid-feedback" style="display: inline;">(اختياري)</span>
                                        </label>
                                        <select name="sub_category_id" class="custom-select form-control" id="sub_category_id">
                                            <option value="" disabled selected>اختر القسم</option>
                                            @foreach($data['subCategories'] as $sub_category)
                                                <option value="{{$sub_category->id}}" {{ (old("sub_category_id") == $sub_category->id ? "selected":"") }}>{{$sub_category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">إسم المنتج باللغة العربية</label>
                                        <input class="form-control btn-square" name="name_ar" value="{{old('name_ar')}}"  id="name_ar" type="text" placeholder="إسم المنتج باللغة العربية" required>
{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="name_en">إسم المنتج باللغة الإنجليزية</label>--}}
                                        {{--<input class="form-control btn-square" name="name_en" id="name_en" type="text" placeholder="إسم المنتج باللغة الإنجليزية" required>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="short_description_ar">الوصف المختصر (باللغة العربية)</label>
                                        <textarea class="form-control" name="short_description_ar" value="{{old('short_description_ar')}}" id="short_description_ar" cols="20" rows="4" required></textarea>
{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-form-label float-right" for="short_description_en">الوصف المختصر (باللغة الانجليزية)</label>--}}
                                        {{--<textarea class="form-control" name="short_description_en" id="short_description_en" cols="20" rows="4" required></textarea>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="long_description_ar">الوصف الكامل (باللغة العربية)</label>
                                        <textarea class="form-control" name="long_description_ar" value="{{old('long_description_ar')}}" id="long_description_ar" cols="20" rows="4" required></textarea>
{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-form-label float-right" for="long_description_en">الوصف الكامل (باللغة الانجليزية)</label>--}}
                                        {{--<textarea class="form-control" name="long_description_en" id="long_description_en" cols="20" rows="4" required></textarea>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="quantity">الكمية المتاحة</label>
                                        <input class="form-control btn-square" name="quantity" value="{{old('quantity')}}" id="quantity" type="number" placeholder="الكمية المتاحة" required>
{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="price_before">سعر المنتج (قبل الخصم)</label>
                                        <input class="form-control btn-square" name="price_before" value="{{old('price_before')}}" id="price_before" type="number" step="any" placeholder="سعر المنتج (قبل الخصم)" required>
{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="price_after">سعر المنتج (بعد الخصم)</label>
                                        <input class="form-control btn-square" name="price_after" value="{{old('price_after')}}"  id="price_after" type="number" step="any" placeholder="سعر المنتج (بعد الخصم)" required>
{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="percent">نسبة الخصم (%)</label>
                                        <input class="form-control btn-square" name="percent"  id="percent" value="0" type="text" placeholder="0 %" required>
{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="images[]" id="images" type="file" required="" multiple data-original-title="" title="">
                                        <label class="custom-file-label" for="images">صور المنتج</label>
{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

                                    </div>
                                </div>
                            </div>
{{--                            Variations      --}}
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <button style="border-radius: 0;" type="button" id="VariationAdd" class="btn btn-success"> إضافة اختيار <span class="fa fa-plus-circle"></span></button>
                                        <br/>
                                        <br/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div id="VariationsContainer">
                                    </div>
                                </div>
                            </div>
                            <div class="row border">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 style="text-align: center;">ملحوظة : هذه المعلومات إختيارية وليست إجبارية</h5>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <button style="border-radius: 0;" type="button" id="InfoAdd" class="btn btn-info"> معلومات إضافية  <span class="fa fa-plus-circle"></span></button>
                                                        <br/>
                                                        <br/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div id="InfosContainer">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Variations Shell --}}

                            <div id="VariationShell" class="form-group row add" style="display: none;">
                                <div class="col-md-5">
{{--                                    <input type="text" class="form-control variation_ar" name="variations[x][ar]" min="1" placeholder="إسم الإختيار باللغة العربية" required>--}}
                                    <input type="text" class="form-control variation_ar" min="1" placeholder="إسم الإختيار باللغة العربية" >
                                </div>
                                {{--<div class="col-md-5">--}}
{{--                                    <input type="text" class="form-control float-type variation_en" name="variations[x][en]" min="1" placeholder="إسم الإختيار باللغة الانجليزية" required>--}}
                                    {{--<input type="text" class="form-control float-type variation_en" min="1" placeholder="إسم الإختيار باللغة الانجليزية" required>--}}
                                {{--</div>--}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select name="type" class="custom-select form-control type">
                                            <option value="" disabled selected>اختر النوع</option>
                                            <option value="0">اختيار واحد</option>
                                            <option value="1">اختيار متعدد</option>
                                            <option value="2">لون</option>
                                            <option value="3">حجم</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <label class="d-block" for="check-required">
                                        <input type="checkbox" class="checkbox_animated required" name="required" id="check-required" value="1">
                                        <label for="check-required">إجباري</label>
                                    </label>
                                </div>
                                <!--<div class="col-md-12">
                                    <label class="d-block" for="check-required">
                                        <input type="checkbox" class="checkbox_animated required" name="is_size" id="check-required2" value="1">
                                        <label for="check-required">هل هذا الاختيار هو حجم للمنتج ؟</label>
                                    </label>
                                </div>-->
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-xs btn-danger variation-remove-btn" style="margin:0; border-radius: 0;"><i data-feather="x"></i></button>
                                </div>
                                <br/>
                                <br/>
                                <br/>
                                <div class="col-md-2" style="margin-right: 35%;">
                                    <button style="border-radius: 0;" type="button" id="OptionAdd" class="btn btn-success option_btn" data-var-id="x"> إضافة جديدة<span class="fa fa-plus-circle"></span></button>
                                </div>

                                <br/>
                                <br/>
                                <br/>
                                <div class="col-md-12 row" style="margin-right: 1%;">
                                    <div class="col">
                                        <div id="OptionsContainer">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Options Shell --}}

                            <div id="OptionShell" class="form-group row" style="display: none; margin-right: 10%;">
                                <div class="col-md-4">
{{--                                    <input type="text" class="form-control option_ar" name="variations[x][options][y][ar]" min="1" placeholder="إسم الإضافة باللغة العربية" required>--}}
                                    <input type="text" class="form-control option_ar"  min="1" placeholder="إسم الإضافة باللغة العربية" >
                                </div>
                                {{--<div class="col-md-4">--}}
{{--                                    <input type="text" class="form-control float-type option_en" name="variations[x][options][y][en]" min="1" placeholder="إسم الإضافة باللغة الانجليزية" required>--}}
                                    {{--<input type="text" class="form-control float-type option_en" min="1" placeholder="إسم الإضافة باللغة الانجليزية" required>--}}
                                {{--</div>--}}
                                <div class="col-md-3">
{{--                                    <input type="number" class="form-control float-type option_price" name="variations[x][options][y][price]" min="1" placeholder="سعر الإضافة" required>--}}
                                    <input type="number" step="0.01" class="form-control float-type option_price" min="1" placeholder="سعر الإضافة" >
                                </div>
                                <div class="col-md-2">
                                    {{--                                    <input type="number" class="form-control float-type option_price" name="variations[x][options][y][price]" min="1" placeholder="سعر الإضافة" required>--}}
                                    <input type="color" class="form-control float-type option_color" >
                                </div>
                                <div class="col-md-2">
                                    <label class="d-block" for="check-status">
                                        <input type="checkbox" class="checkbox_animated status" name="status" id="check-status" value="1">
                                        <label for="check-status">متاح</label>
                                    </label>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-xs btn-danger option-remove-btn" style="margin:0; border-radius: 0;"><i data-feather="x"></i></button>
                                </div>
                            </div>

                            {{-- Other Info Shell --}}

                            <div id="InfoShell" class="form-group row add" style="display: none;">
                                <div class="col-md-3">
                                    <input type="text" class="form-control text1" placeholder="النص الأول" >
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control text2" placeholder="النص الثاني" >
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control text3" placeholder="النص الثالث" >
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-xs btn-danger info-remove-btn" style="margin:0; border-radius: 0;"><i data-feather="x"></i></button>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" id="create-button" style="width: 100%; border-radius: 0;" type="submit">حفط</button>
                            {{--<input class="btn btn-light" type="reset" value="إلغاء">--}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@section('scripts')
    <script>
            $(function() {
                $("#price_after, #price_before").keyup(function() { // input on change
                    var result = 100 - parseFloat(parseInt($("#price_after").val(), 10) * 100)/ parseInt($("#price_before").val(), 10);
                    $('#percent').val(Math.round(result)||'0'); //shows value in "#percent"
                })
            });
    </script>
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <script>
        // $('#create-button').on('click',function () {
        //     $('#create-form').submit();
        // })
    </script>

{{--     for explaination feel free to call me on 01009313883 --}}
    <script>


        var i = 0;

        $("#VariationAdd").click(function(){
            $("#VariationShell").clone(true).attr('id','VariationShell'+i).appendTo('#VariationsContainer').fadeIn();
            // $("#VariationShell").clone(true).attr('id','VariationShell'+i).appendTo($(this).closest('.form-group')).fadeIn();


            $('#VariationShell'+i).find('input.variation_ar').attr('name', 'variations['+i+'][ar]');
            // $('#VariationShell'+i).find('input.variation_en').attr('name', 'variations['+i+'][en]');
            $('#VariationShell'+i).find('select.type').attr('name', 'variations['+i+'][type]');
            // $('#VariationShell'+i).find('input.required').attr('name', 'variations['+i+'][required]');

            // Is the variation is size of product ??
            //$('#VariationShell'+i).find('input.is_size').attr('name', 'variations['+i+'][is_size]');
            // end Is the variation is size of product ??

            $('#VariationShell'+i).find('button.option_btn').data('var-id', i);

            i++;
        });

        $('body').on('click', '.variation-remove-btn', function(){
            $(this).closest('.form-group').remove();
        });

        $('body').on('click', '.variation-remove-btn', function(){
            $(this).closest('.form-group').remove();
        });
        // Options
        var y = 0;
        $("#OptionAdd").click(function(){
            var x = $(this).data('var-id');
            $("#OptionShell").clone(true).attr('id','OptionShell'+y).appendTo($(this).closest('.form-group')).fadeIn();
            $('#OptionShell'+y).find('input.option_ar').attr('name', 'variations['+x+'][options]['+y+'][ar]');
            // $('#OptionShell'+y).find('input.option_en').attr('name', 'variations['+x+'][options]['+y+'][en]');
            $('#OptionShell'+y).find('input.option_price').attr('name', 'variations['+x+'][options]['+y+'][price]');
            if($('select[name="variations['+x+'][type]"]').val() == 2){
                // $('.option_color').css('display','');
                //alert('test');
                $('#OptionShell'+y).find('input.option_color').attr('name', 'variations['+x+'][options]['+y+'][color]');
            }else{
                $('#OptionShell'+y).find('input.option_color').attr('name', 'variations['+x+'][options]['+y+'][color]');
                $('#OptionShell'+y).find('input.option_color').css('display','none');
            }
            $('#OptionShell'+y).find('input.status').attr('name', 'variations['+x+'][options]['+y+'][status]');
            y++;
        });
        $('body').on('click', '.option-remove-btn', function(){
            $(this).closest('.form-group').remove();
        });



        // InFo Shell Append

        $("#InfoAdd").click(function(){
            $("#InfoShell").clone(true).attr('id','InfoShell'+i).appendTo('#InfosContainer').fadeIn();
            $('#InfoShell'+i).find('input.text1').attr('name', 'info['+i+'][text1]');
            $('#InfoShell'+i).find('input.text2').attr('name', 'info['+i+'][text2]');
            $('#InfoShell'+i).find('input.text3').attr('name', 'info['+i+'][text3]');

            i++;
        });

        $('body').on('click', '.info-remove-btn', function(){
            $(this).closest('.form-group').remove();
        });


        // $('document').on('change','.type', function(){
        //     //$('select[name="' + name + '"]')
        //     console.log(this);
        //     if($('.type').attr("selected") == 2 ){
        //         console.log('aaaaaaaaaaa');
        //     }
        // });
        // $('body').on('change', '.type', function(){
        //     if($(this).val() == 2 ){ //type = 2 for color
        //         console.log('aaaaaaaaaaa');
        //         $(this).closest('.color').attr('display','block');
        //     }else{
        //         console.log('bbbbbbbbbbb');
        //         $(this).closest('.color').attr('display','none');
        //     }
        // });
    </script>

@endsection

