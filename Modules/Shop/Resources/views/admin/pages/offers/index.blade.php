@extends('shop::admin.layouts.master')
@section('styles')
    <!-- Plugins css start-->
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
{{--                    <div class="card-header">--}}
{{--                        <h5>{{$data['title']}}</h5>--}}
{{--                    </div>--}}
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    <input type="hidden" name="myids" id="bulk_delete_input" value="">
                                    <br>
                                    <br>
                                    <th>
                                        <label class="d-block" for="chk-ani">
                                            <input class="checkbox_animated select_all" id="chk-ani" type="checkbox">
                                        </label>
                                    </th>
                                    <th>الصورة</th>
                                    <th>المنتج</th>
                                    <th>تاريخ الانتهاء</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['offers'] as $offer)
                                    <tr>
                                        <td>
                                            <label class="d-block" for="chk-ani">
                                                <input type="checkbox" class="checkbox_animated" name="row_id" id="checkbox_{{ $offer->id }}" value="{{ $offer->id }}">
                                            </label>
                                        </td>
                                        <td><img class="img-60 rounded-circle" src="{{!empty($offer->image) ? $offer->image : url('assets/images/default.png')}}" alt="#" data-original-title="" title=""></td>
                                        <td>{{isset($offer->product) ? $offer->product->name : "-"}}</td>
                                        <td>{{isset($offer->expire_at) ? $offer->expire_at : "-"}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection


@section('scripts')
    <!-- Plugins JS start-->
{{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>
        function deleteOffer(offer_id)
        {
            var id = offer_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.offers.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {offer_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
        //
        }
        $(document).ready(function(){
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        myids.push(value);
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.offers.deleteMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

            // End Bulk Delete
        });
    </script>
@endsection
