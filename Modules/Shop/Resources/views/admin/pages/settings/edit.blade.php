@extends('shop::admin.layouts.master')
@section('styles')
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/simple-mde.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <form class="needs-validation"  novalidate="" method="post" action="{{route('admin.settings.update')}}">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-form-label float-right" for="about_ar">عن التطبيق (باللغة العربية)</label>
                                    <textarea class="form-control" name="about_ar" id="about_ar" cols="20" rows="8"required>{{isset($data['settings']) ? $data['settings']->translateOrnew('ar')->about_us: ""}}</textarea>
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    <div class="valid-feedback">بيانات صحيحة</div>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label float-right" for="about_en">عن التطبيق (باللغة الانجليزية)</label>
                                    <textarea class="form-control" name="about_en" id="about_en" cols="20" rows="8"required>{{isset($data['settings']) ? $data['settings']->translateOrnew('en')->about_us: ""}}</textarea>
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label float-right" for="terms_ar">شروط الإستخدام (باللغة العربية)</label>
                                    <textarea class="form-control" name="terms_ar" id="terms_ar" cols="20" rows="8"required>{{isset($data['settings']) ? $data['settings']->translateOrnew('ar')->terms : ""}}</textarea>
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label float-right" for="terms_en">شروط الإستخدام (باللغة الانجليزية)</label>
                                    <textarea class="form-control" name="terms_en" id="terms_en" cols="20" rows="8"required>{{isset($data['settings']) ? $data['settings']->translateOrnew('en')->terms : ""}}</textarea>
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary float-right" type="submit">حفظ</button>
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">إغلاق</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('modals')

@endsection

@section('scripts')

{{--    <script src="{{ url('/assets/js/editor/simple-mde/simplemde.min.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/editor/simple-mde/simplemde.custom.js') }}"></script>--}}
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
@endsection
