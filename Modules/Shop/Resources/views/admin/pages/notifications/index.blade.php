@extends('shop::admin.layouts.master')
@section('styles')
    <!-- Plugins css start-->
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/simple-mde.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    {{--@if(Auth::guard('admin')->user()->can('delete_user'))--}}
{{--                                    <button class="btn btn-square btn-danger" style="float: left; margin-right: 2%;" id="bulk_delete_btn" type="button" title="حذف متعدد">حذف متعدد</button>--}}
                                    {{--@endif--}}
                                    <input type="hidden" name="myids" id="bulk_delete_input" value="">
                                    <br>
                                    <br>
                                    <th>
                                        <label class="d-block" for="chk-ani">
                                            <input class="checkbox_animated select_all" id="chk-ani" type="checkbox">
                                        </label>
                                    </th>
                                    <th>موجهة لـ</th>
                                    <th>الرسالة</th>
                                    <th>التاريخ</th>
{{--                                    <th>العمليات</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['notifications'] as $notification)
                                    <tr>
                                        <td>
                                            <label class="d-block" for="chk-ani">
                                                <input type="checkbox" class="checkbox_animated" name="row_id" id="checkbox_{{ $notification->id }}" value="{{ $notification->id }}">
                                            </label>
                                        </td>
                                        <td>
                                            @if ($notification->type == '0')
                                                <span class="badge badge-success">كل المستخدمين</span>
                                            @elseif ($notification->type == '1')
                                                <span class="badge badge-success">كل المندوبين</span>
                                            @elseif ($notification->type == '2')
                                                <span class="badge badge-info">{{$notification->user->name}}</span>
                                                <span class="badge badge-success">{{$notification->user->phone}}</span>
                                            @elseif ($notification->type == '3')
                                                <span class="badge badge-info">{{$notification->shop->name}}</span>
                                                <span class="badge badge-success">{{$notification->shop->email}}</span>
                                            @endif
                                        </td>
                                        <td>{{isset($notification->message) ? str_limit($notification->message , 100) : ""}}</td>
                                        <td>{{$notification->created_at}}</td>
{{--
 <td>--}}
{{--                                            <div class="row">--}}
{{--                                                @if(Auth::guard('admin')->user()->can('delete_notifications'))--}}
{{--                                                    <a onclick='return deleteNotification({{$notification->id}})' title="حذف" data-id="{{$notification->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a>--}}
{{--                                                @endif--}}
{{--                                            </div>--}}
{{--                                        </td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
    <!-- Plugins JS start-->
{{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
    <!-- Plugins JS start-->
{{--    <script src="{{ url('/assets/js/editor/simple-mde/simplemde.min.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/editor/simple-mde/simplemde.custom.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>
        function deleteNotification(notification_id)
        {
            var id = notification_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.notifications.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {notification_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
        }
        $(document).ready(function(){
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        myids.push(value);
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.notifications.deleteMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

            // End Bulk Delete
        });
    </script>
@endsection
