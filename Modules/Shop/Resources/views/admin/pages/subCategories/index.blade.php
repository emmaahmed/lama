@extends('shop::admin.layouts.master')
@section('styles')
    <!-- Plugins css start-->
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
{{--                    <div class="card-header">--}}
{{--                        <h5>{{$data['title']}}</h5>--}}
{{--                    </div>--}}
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    @if(Auth::guard('shop')->user()->can('add_sub_category'))
                                    <a class="btn btn-square btn-success" href="javascript:void(0)" title="إضافة" id="create-new-subCategory"> إضافة قسم جديد</a> &nbsp; &nbsp;
                                    @endif
                                    @if(Auth::guard('shop')->user()->can('delete_sub_category'))
                                        <button class="btn btn-square btn-danger" style="float: left; margin-right: 2%;" id="bulk_delete_btn" type="button" title="حذف متعدد">حذف متعدد</button>
                                    @endif
                                    <input type="hidden" name="myids" id="bulk_delete_input" value="">
                                    <br>
                                    <br>
                                    <th>
                                        <label class="d-block" for="chk-ani">
                                            <input class="checkbox_animated select_all" id="chk-ani" type="checkbox">
                                        </label>
                                    </th>
                                    <th>الId</th>
                                    <th>الإسم</th>
                                    <th>الحالة</th>
                                    <th>عدد المنتجات</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody id="subCategories-crud">
                                @foreach($data['subCategories'] as $subCategory)
                                    <tr id="subCategory_id_{{ $subCategory->id }}">
                                        <td>
                                         {{$subCategory->id}}
                                        </td>
                                        <td>
                                            <label class="d-block" for="chk-ani">
                                                <input type="checkbox" class="checkbox_animated" name="row_id" id="checkbox_{{ $subCategory->id }}" value="{{ $subCategory->id }}">
                                            </label>
                                        </td>
                                        <td>{{isset($subCategory->name) ? $subCategory->name : ""}}</td>
                                        <td>
                                            <div class="form-group">
                                                <div class="media-body icon-state switch-outline">
                                                    <label class="switch">
                                                        <input type="checkbox" id="active" class="sub_category_status" data-category-id="{{$subCategory->id}}" name="active" @if($subCategory->active == 1) checked @endif><span class="switch-state bg-success"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>

                                        <td>{{isset($subCategory->products) ? count($subCategory->products) : 0}}</td>
                                        <td>
                                            <div class="row" id="alert">
                                                @if(Auth::guard('shop')->user()->can('edit_sub_category'))
                                                    <a title="تعديل" href="javascript:void(0)" id="edit-subCategory" data-id="{{ $subCategory->id }}"><i width="20" height="20" color="green" data-feather="edit"></i></a> &nbsp;
                                                @endif
                                                @if(Auth::guard('shop')->user()->can('delete_sub_category'))
                                                    <a onclick='return deletesubCategory({{$subCategory->id}})' title="حذف" data-id="{{$subCategory->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('modals')
    <div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="subCategoryCrudModal"></h4>
                </div>
                <form id="subCategoryForm" method="post" action="{{route('shop.subCategories.store')}}" name="subCategoryForm"  class="needs-validation"  novalidate="">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="subCategory_id" id="subCategory_id">
                        <input type="hidden" name="shop_id" value="{{$data['shop_id']}}">
{{--                        <div class="form-group">--}}
{{--                            <label for="name" class="col-sm-2 control-label" style="float: right;">الإسم</label>--}}
{{--                            <div class="col-sm-12">--}}
{{--                                <input type="text" class="form-control" id="name" name="name" placeholder="اكتب اسم القسم" value="" maxlength="50" required="">--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="form-group">
                            <label for="name_ar" style="float: right;">إسم القسم باللغة العربية</label>
                            <input class="form-control btn-square" name="name_ar" id="name_ar" type="text" placeholder="القسم باللغة العربية" required>
                            <div class="invalid-feedback" style="float: right;">لا يمكنك ترك هذا الحقل فارغ</div>
                            {{--<div class="valid-feedback" style="float: right;">بيانات صحيحة</div>--}}
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<label for="name_ar" style="float: right;">إسم القسم باللغة الإنجليزية</label>--}}
                            {{--<input class="form-control btn-square" name="name_en" id="name_en" type="text" placeholder="القسم باللغة الإنجليزية" required>--}}
                            {{--<div class="invalid-feedback" style="float: right;">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                            {{--<div class="valid-feedback" style="float: right;">بيانات صحيحة</div>--}}
                        {{--</div>--}}
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="btn-save" value="create">
                            حفظ
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />--}}
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>--}}
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script>
        function deletesubCategory(subCategory_id)
        {
            var id = subCategory_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('shop.subCategories.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {subCategory_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
        //
        }
        $(document).ready(function(){
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        myids.push(value);
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('shop.subCategories.deleteMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

            // End Bulk Delete
        });
    </script>

<script>
    $(document).ready(function () {
        if(localStorage.getItem("success"))
        {
            toastr.success('تم بنجاح');
            localStorage.clear();
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        /*  When user click add user button */
        $('#create-new-subCategory').click(function () {
            $('#btn-save').val("create-subCategory");
            $('#subCategoryForm').trigger("reset");
            $('#subCategoryCrudModal').html("إضافة قسم جديد");
            $('#ajax-crud-modal').modal('show');
        });
        /* When click edit user */
        $('body').on('click', '#edit-subCategory', function () {
            var subCategory_id = $(this).data('id');
            // $.get('edit/subCategory/' + subCategory_id, function (data) {
            //     $('#subCategoryCrudModal').html("تعديل القسم");
            //     $('#btn-save').val("edit-subCategory");
            //     $('#ajax-crud-modal').modal('show');
            //     $('#subCategory_id').val(data.id);
            //     $('#name_ar').val(data.name_ar);
            //     $('#name_en').val(data.name_en);
            // })
            $.ajax({
                method: 'GET',
                url: "{{URL::route('shop.subCategories.edit')}}",
                dataType: 'json',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: {
                    subCategory_id:subCategory_id,
                    shop_id:{{$data['shop_id']}},
                    "_token": "{{ csrf_token() }}"},
                success: function(response){
                    $('#subCategoryCrudModal').html("تعديل القسم");
                    $('#btn-save').val("edit-subCategory");
                    $('#ajax-crud-modal').modal('show');
                    $('#subCategory_id').val(response.id);
                    $('#name_ar').val(response.name_ar);
                    $('#name_en').val(response.name_en);
                },
                error: function() {
                    swal(
                        'يوجد خطأ ما',
                        'من فضلك حاول مرة أخري',
                        'خطأ'
                    )
                }
            });
        });

    });
    $('.sub_category_status').on('change.bootstrapSwitch', function(e) {
        var sub_category_id = $(this).attr('data-category-id');
        var active = "";
        if (e.target.checked == true){
            var active = 1;
        }else{
            var active = 0;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "{{URL::route('shop.sub-categories.status')}}",
            data: {
                sub_category_id: sub_category_id,
                active: active,
                "_token": "{{ csrf_token() }}"
            },
            success: function(response){
                toastr.success(response.success);
            },
            error: function(jqXHR){
                toastr.error(jqXHR.responseJSON.message);
            }
        });
    });

</script>
@endsection
