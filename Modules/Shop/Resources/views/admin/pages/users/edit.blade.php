@extends('shop::admin.layouts.master')
@section('styles')
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>تعديل المستخدم</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.users.update',['user_id' => $data['user']])}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name">إسم المستخدم</label>
                                        <input class="form-control btn-square" value="{{isset($data['user']->name) ? $data['user']->name : ""}}" name="name" id="name" type="text" placeholder="اسم المستخدم" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="phone">رقم الهاتف</label>
                                        <input class="form-control btn-square" value="{{isset($data['user']->phone) ? $data['user']->phone : ""}}" name="phone" id="phone" type="text" placeholder="رقم الهاتف" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="email">البريد الإلكتروني</label>
                                        <input class="form-control btn-square" value="{{isset($data['user']->email) ? $data['user']->email : ""}}" name="email" id="email" type="email" placeholder="البريد الإلكتروني" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="status">حالة المستخدم</label>
                                <div class="media-body icon-state switch-outline">
                                    <label class="switch">
                                        <input type="checkbox" @if($data['user']->status == 1) checked @endif id="status" name="status"><span class="switch-state bg-success"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="text-center">
                                    <img id="image-display" width="100px" height="100px" class="profile-user-img img-fluid img-circle" style="border: 1px solid #e5eaff;"
                                         alt="Admin profile picture" src="{{$data['user']->image ? $data['user']->image : url('assets/images/user.png')}}" >
                                </div>
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="image" id="image" type="file" data-original-title="" title="">
                                        <label class="custom-file-label" for="image">صورة المستخدم</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <!-- Plugins JS start-->
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image-display').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function() {
            readURL(this);
        });
    </script>
    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('userlocation'), {
                zoom: 7,
                center: {lat: {{$data['user']->latitude}}, lng: {{$data['user']->longitude}} }
            });
            var MaekerPos = new google.maps.LatLng({{$data['user']->latitude}} , {{$data['user']->longitude}});
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("userlat").value=e.latLng.lat();
                document.getElementById("userlng").value=e.latLng.lng();
            });
        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
    </script>
@endsection
