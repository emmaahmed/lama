@extends('shop::admin.layouts.master')
@section('styles')
    <!-- Plugins css start-->
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
{{--                    <div class="card-header">--}}
{{--                        <h5>{{$data['title']}}</h5>--}}
{{--                    </div>--}}
                    <div class="card-body animate-chk">
                        <div class="table-responsive">
{{--                            @if(Auth::guard('admin')->user()->can('show_order')) --}}
                                <form class="row needs-validation"  novalidate="" id="form-range">
                                    <div class="col">
{{--                                        @isset($data['user_id'])--}}
{{--                                            <input type="hidden" name="user_id" id="user_id" value="{{$data['user_id']}}"/>--}}
{{--                                        @endisset--}}
{{--                                        @isset($data['worker_id'])--}}
{{--                                            <input type="hidden" name="worker_id" id="worker_id" value="{{$data['worker_id']}}"/>--}}
{{--                                        @endisset--}}
                                        <div class="form-group">
                                            <label for="from">من : </label>
                                            <input type="date" name="from" class="form-control" id="from" required>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                            <div class="valid-feedback">بيانات صحيحة</div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="to">إلي : </label>
                                            <input type="date" name="to" class="form-control" id="to" required>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                            <div class="valid-feedback">بيانات صحيحة</div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <button type="submit" name="filter" id="filter" class="btn btn-info btn-sm" style="margin-top: 9%;">بحث</button>
                                    </div>
                                </form>
{{--                            @endif--}}
                            <table class="display" id="basic-9">
                                <thead>
                                    <tr>
{{--                                    @if(Auth::guard('admin')->user()->can('delete_order'))--}}
                                        <button class="btn btn-square btn-danger" style="float: left; margin-right: 2%;" id="bulk_delete_btn" type="button" title="حذف متعدد">حذف متعدد</button>
{{--                                    @endif--}}
{{--                                     @if(Auth::guard('admin')->user()->can('export_order'))--}}
{{--                                        <a href="{{route('admin.order.export',['type' => 0])}}" class="btn btn-square btn-success" style="float: left;" title="تحميل الملف">تحميل الملف</a>--}}
{{--                                     @endif--}}
                                    <input type="hidden" name="myids" id="bulk_delete_input" value="">
                                    <br>
                                    <br>
                                    <th>
                                        <label class="d-block" for="chk-ani">
                                            <input class="checkbox_animated select_all" id="chk-ani" type="checkbox">
                                        </label>
                                    </th>
                                    <th>لوجو المتجر</th>
                                    <th>اسم المتجر</th>
                                    <th>صاحب الطلب</th>
                                    <th>السعر الكلي</th>
                                    <th>نوع الدفع</th>
                                    <th>الحالة</th>
                                    <th>تاريخ الإنشاء</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @isset($data['orders'])
                                        @foreach($data['orders'] as $order)
                                        <tr>
                                            <td>
                                                <label class="d-block" for="chk-ani">
                                                    <input type="checkbox" class="checkbox_animated" name="row_id" id="checkbox_{{ $order->id }}" value="{{ $order->id }}">
                                                </label>
                                            </td>
                                            <td><img class="img-60 rounded-circle" src="{{!empty($order->shop->logo) ? $order->shop->logo : url('assets/images/default.png')}}" alt="#" data-original-title="" title=""></td>
                                            <td>{{isset($order->shop) ? $order->shop->name : ""}}</td>
                                            <td>{{isset($order->user) ? $order->user->name : ""}}</td>
                                            <td>{{isset($order->total_cost) ? $order->total_cost : ""}}</td>
                                            <td>
                                                @if($order->payment_type == 0)
                                                    <span class="badge badge-primary">كاش</span>
                                                @elseif($order->payment_type == 1)
                                                    <span class="badge badge-success">اونلاين</span>
                                                @endif
                                            </td>
                                            <td>
{{--                                                <select @if(!Auth::guard('admin')->user()->can('edit_order'))  disabled @endif name="order_status" class="custom-select form-control order_status" data-order-id="{{$order->id}}" data-shop-id="{{$order->shop->id}}">--}}
{{--                                                    <option value="0" @if($order->status == 0) selected @endif>في الإنتظار</option>--}}
{{--                                                    <option value="1" @if($order->status == 1) selected @endif>في الطريق</option>--}}
{{--                                                    <option value="2" @if($order->status == 2) selected @endif>منتهي</option>--}}
{{--                                                    <option value="3" @if($order->status == 3) selected @endif>ملغي</option>--}}
{{--                                                </select>--}}
                                                <select name="order_status" class="custom-select form-control order_status" data-order-id="{{$order->id}}" data-shop-id="{{$order->shop->id}}">
                                                    <option value="0" @if($order->status == 0) selected @endif>في الإنتظار</option>
                                                    <option value="1" @if($order->status == 1) selected @endif>في الطريق</option>
                                                    <option value="2" @if($order->status == 2) selected @endif>منتهي</option>
                                                    <option value="3" @if($order->status == 3) selected @endif>ملغي</option>
                                                </select>
                                            </td>
                                            <td>{{isset($order->created_at) ? $order->created_at : ""}}</td>
                                            <td>
                                                <div class="row">
                                                    <a href="{{route('shop.orders.details',[$order->id])}}" title="تفاصيل الطلب"> <i width="20" height="20" color="blue" data-feather="eye"></i></a> &nbsp;
{{--                                                    @if(Auth::guard('admin')->user()->can('delete_order'))--}}
                                                        <a onclick='return deleteOrder({{$order->id}})' title="حذف" data-id="{{$order->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a>
{{--                                                    @endif--}}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @endisset
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
        <!-- Plugins JS start-->
    {{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
    {{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
    {{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
        <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <script>
        $('.order_status').change(function () {
            var order_id = $(this).attr('data-order-id');
            var status = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('shop.orders.status')}}",
                data: {
                    order_id: order_id,
                    status: status,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response){
                    // $('.order_status option').prop('disabled', false);
                    // var index = $(this).find('option:selected').index();
                    // $('select').not(this).find('option:lt(' + index + ')').prop('disabled', true);
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });
        function deleteOrder(order_id)
        {
            var id = order_id;
            swal({
                    title: 'هل أنت متأكد!',
                    text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'حذف',
                    cancelButtonText: 'تراجع'
            }).then((result) => {
                    if (result.value) {
                            $.ajax({
                                   method: 'POST',
                                   url: "{{URL::route('shop.orders.delete')}}",
                                   dataType: 'json',
                                   headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                data: {order_id:id,"_token": "{{ csrf_token() }}"},

                                    success: function(response){
                                        window.location.reload();
                                        toastr.success(response.success);
                                    },
                                error: function() {
                                        swal(
                                                'يوجد خطأ ما',
                                                'من فضلك حاول مرة أخري',
                                                'خطأ'
                                            )
                                    }
                            });
                        }
                });
        //
            }
        $(document).ready(function(){
                $('.select_all').on('click', function(e) {
                        $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
                    });
                // Bulk Delete
                    var $bulkDeleteBtn = $('#bulk_delete_btn');
                var $bulkdeleteinput = $('#bulk_delete_input');
                // Reposition modal to prevent z-index issues
                    // Bulk delete listener
                        $bulkDeleteBtn.click(function (e) {
                                var myids = [];
                                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                                var count = $checkedBoxes.length;
                                if (count) {
                                        // Reset input value
                                            $bulkdeleteinput.val('');
                                        $.each($checkedBoxes, function () {
                                                var value = $(this).val();
                                                myids.push(value);
                                            });
                                        // Set input value
                                            $bulkdeleteinput.val(myids);
                                        // Show modal
                                            e.preventDefault();
                                        $.ajax({
                                                method: 'POST',
                                                url: "{{URL::route('shop.orders.deleteMulti')}}",
                                                dataType: 'json',
                                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                            data: {ids:myids,"_token": "{{ csrf_token() }}"},

                                                success: function(response){
                                                    window.location.reload();
                                                    toastr.success(response.success);
                                                },
                                            error: function(jqXHR){
                                                    toastr.error(jqXHR.responseJSON.message);
                                                }
                                        });
                                    } else {
                                        // No row selected
                                            toastr.warning('Nothing to Delete');
                                    }
                           });

                    // End Bulk Delete
                    });
    </script>

    <script>
        $('#form-range').submit(function (e) {
                    e.preventDefault();
                    var date_from = $('#from').val();
                    var date_to = $('#to').val();
                    $.ajax({
                            type: 'get',
                            dataType: 'html',
                            url: '{{route('shop.orders.filter')}}',
                            data: {
                                "from": date_from,
                                    "to": date_to
                                },
                        success: function (response) {
                                $('#basic-9').html(response);
                            }
                    });
                })
    </script>
    @endsection
