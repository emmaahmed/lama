@extends('shop::admin.layouts.master')
@section('styles')
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/owlcarousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
@endsection
@section('content')

    <div class='row'>
        <div class='col-md-2' >
            <a  style='font-size: x-large'>
                حالة الطلب:
            </a>
        </div>
        <div class='col-md-4' >
            @if($data['order']->delivery_type!=2)
                <select @if($data['order']->delivery_type == 1||$data['order']->status == -1) disabled @endif name="order_status" class="custom-select form-control order_status" data-order-id="{{$data['order']->id}}" data-shop-id="{{$data['order']->shop->id}}">
                    <option value="0" @if($data['order']->status == 0) selected @endif>قيد الإنشاء</option>
                    <option value="1" @if($data['order']->status == 1) selected @endif>قبول الطلب</option>
                    <option value="2" @if($data['order']->status == 2) selected @endif>في الطريق</option>
                    <option value="3" @if($data['order']->status == 3) selected @endif>تم التوصيل</option>
                    <option value="4" @if($data['order']->status == 4) selected @endif>ملغي</option>
                    <option value="-1" @if($data['order']->status == -1) selected @endif>ملغي من المستخدم</option>

                </select>
            @else
                <select name="order_status" class="custom-select form-control order_status" @if($data['order']->status == -1) disabled @endif data-order-id="{{$data['order']->id}}" data-shop-id="{{$data['order']->shop->id}}">
                    <option value="0" @if($data['order']->status == 0) selected @endif>جاري التنفيذ</option>
                    <option value="1" @if($data['order']->status == 1) selected @endif>تم القبول</option>
                    <option value="2" @if($data['order']->status == 2) selected @endif>تم التجهيز</option>
                    <option value="3" @if($data['order']->status == 3) selected @endif>تم الإستلام</option>
                    <option value="4" @if($data['order']->status == 4) selected @endif>الطلب ملغي</option>
                    <option value="-1" @if($data['order']->status == -1) selected @endif>ملغي من المستخدم</option>

                </select>

            @endif
        </div>
        <div class='col-md-2'></div>
        <div class='col-md-4'>
                <a href="{{route('shop.orders.invoice',['order_id' => $data['order']->id])}}" target="_blank" title="طباعة الطلب" style='color:green; font-size: x-large;'>
                طباعة الطلب
                <i class="fa fa-print"></i>
                </a> &nbsp;
        </div>
    </div>
    <br>

    <!-- Container-fluid starts-->
    <div class="container-fluid" >

        <!--<a href="{{route('admin.orders.invoice',['order_id' => $data['order']->id])}}" target="_blank" title="طباعة الطلب"> <i class="fa fa-print"></i></a> &nbsp;-->
        <div class="card">
            <div class="row product-page-main">
                <div class="col-xl-4" style='text-align:center'>
                    <img width="100%" src="{{!empty($data['order']->shop) ? $data['order']->shop->logo : url('assets/images/default.png')}}" alt=""/>
                    <a href="{{route('admin.shops.products.show',['shop_id' => $data['order']->shop->id])}}" >
                                    <strong>المتجر : </strong>{{isset($data['order']->shop) ? $data['order']->shop->name : ''}}
                                </a>
                </div>
                <div class="col-xl-8">
                    <div class="product-page-details">
                        <div class="row">
                            <div class="col-md-6">
                                <strong>رقم الطلب : </strong><span style="color: #0b43c6;">{{isset($data['order']->order_number) ? $data['order']->order_number : ''}}</span>
                            </div>
                            <div class="col-md-6">
                                <strong>نوع التوصيل : </strong>
                                @if($data['order']->delivery_type == 0)
                                    <span class="badge badge-secondary" style="padding-left: 15px;">عادي</span>
                                @elseif($data['order']->delivery_type == 1)
                                    <span class="badge badge-primary" style="padding-left: 15px;">سريع</span>
                                @elseif($data['order']->delivery_type == 2)
                                    <span class="badge badge-primary" style="padding-left: 15px;">من الفرع</span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>صاحب الطلب : </strong>{{isset($data['order']->user) ? $data['order']->user->name : ''}}
                            </div>
                            <div class="col-md-6">
                                <!--<a href="{{route('admin.shops.products.show',['shop_id' => $data['order']->shop->id])}}">
                                    <strong>المتجر : </strong>{{isset($data['order']->shop) ? $data['order']->shop->name : ''}}
                                </a>-->
                                <strong>موبايل صاحب الطلب : </strong>{{isset($data['order']->user) ? $data['order']->user->phone : ''}}
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>تكلفة الطلب : </strong>{{isset($data['order']->total_cost) ? $data['order']->total_cost : ''}}
                                ريال
                            </div>
                            <div class="col-md-6">
                                <strong>العنوان : </strong>
                                @if (isset($data['order']->address))
                                    @if (isset($data['order']->address->address))
                                        {{$data['order']->address->address}}
                                    @else
                                        {{$data['order']->address->area}}
                                    @endif
                                @endif
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>رقم المبني : </strong>{{isset($data['order']->address->building_number) ? $data['order']->address->building_number : ''}}

                            </div>
                            <div class="col-md-6">
                                <strong>رقم الشقة : </strong>{{isset($data['order']->address->apartment) ? $data['order']->address->apartment : ''}}
                            </div>
                        </div>
                        <hr/>

                        <div class="row">
                            <div class="col-md-12">
                                <strong>عنوان اضافي : </strong>{{isset($data['order']->address->more_info) ? $data['order']->address->more_info : ''}}

                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>مصاريف الشحن : </strong>
                                @if($data['order']->delivery_type==2)
                                    0
                                @elseif($data['order']->delivery_type==1)
                                    {{$data['order']['fast_delivery'] }}

                                @else
                                    {{$data['order']->shop->delivery_fees}}
                                @endif
                                ريال
                            </div>
                            <div class="col-md-6">
                                <strong>وقت التوصيل : </strong>{{isset($data['order']->delivery_time) ? $data['order']->delivery_time : 'غير محدد'}}
                                <!--دقيقة-->
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>الذهاب للعنوان علي الخريطة : </strong>
<a target="_blank" href="https://www.google.com/maps/place/ {{$data['order']->address->latitude }},{{$data['order']->address->longitude}}"> أضغط هنا</a>
                            </div>
                            <div class="col-md-6">
                                <!--دقيقة-->
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>إجمالي التكلفة : </strong>
                                @if($data['order']->delivery_type==2)
                                    {{$data['order']->total_cost}}
                                @elseif($data['order']->delivery_type==1)
                                    {{$data['order']->total_cost + $data['order']['fast_delivery'] }}
                                @else
                                    {{($data['order']->total_cost + $data['order']->shop->delivery_fees)}}
                                @endif

                                ريال
                            </div>
                            <div class="col-md-6">
                                <strong>نوع الدفع : </strong>
                                @if($data['order']->payment == 0)
                                    <span class="badge badge-dark" style="padding-left: 15px;">كاش</span>
                                @elseif($data['order']->payment == 1)
                                    <span class="badge badge-success" style="padding-left: 15px;">أونلاين</span>
                                @elseif($data['order']->payment == 2)
                                    <span class="badge badge-success" style="padding-left: 15px;">شبكة</span>
                                @endif
                            </div>
                        </div>
                        <hr/>
                        @if($data['order']->delegate_id != NULL)
                        <div class="row">
                            <div class="col-md-6">
                                <strong> اسم المندوب  : </strong>{{isset($data['order']->delegate) ? $data['order']->delegate->name : ''}}
                            </div>
                            <div class="col-md-6">
                                <strong> موبايل المندوب  : </strong>{{isset($data['order']->delegate) ? $data['order']->delegate->phone : ''}}
                            </div>
                        </div>
                        <hr/>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <strong>  ملاحظات :
                                    {{($data['order']->notes)}}
                                </strong>
                            </div>

                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong> توقيت الطلب :
                                {{($data['order']->created_at_time)}}
                                </strong>
                            </div>
                            <div>
                                @if($data['order']->delivery_type == 1 && $data['order']->status != 3 && $data['order']->status != 1)
{{--                                    delivery_type <===> 0=>Normal, 1=>Fast, 2=>From shop--}}
{{--                                    status <===> 0=>created ,1=>accepted ,2=>on_way ,3=>delivered ,4=>cancelled--}}
                                <a class="btn btn-dark" href="{{route('shop.orders.sendToDelegates',['order_id' => $data['order']->id])}}"  title="ارسال الطلب للمناديب" style='color:white; font-size: x-large;'>
                                    ارسال الطلب للمناديب
                                </a>
                                @endif
                            </div>
{{--                            <div class="col-md-6">--}}
{{--                                <strong> طريقة استلام الطلب : </strong>--}}
{{--                                @if($data['order']->delivery_type == 0)--}}
{{--                                    <span class="badge badge-success">توصيل عادي</span>--}}
{{--                                @elseif($data['order']->delivery_type == 1)--}}
{{--                                    <span class="badge badge-success">توصيل سريع</span>--}}
{{--                                @elseif($data['order']->delivery_type == 2)--}}
{{--                                    <span class="badge badge-success">استلام من الفرع</span>--}}
{{--                                @endif--}}
{{--                            </div>--}}


                        </div>
                        <hr/>

                </div>
            </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h5>تفاصيل إضافية</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-9">
                        <thead>
                        <tr>
                            <th>الصورة</th>
                            <th>الإسم</th>
                            <th>الوصف</th>
                            <th>سعر المنتج</th>
                            <th>التفاصيل</th>
                            <th>الكمية المطلوبة</th>
                            <th>السعر</th>
                        </tr>
                        </thead>
                        <tbody>
                        @isset($data['order']->orderItems)
                            @foreach($data['order']->orderItems as $order_item)
                                <tr>
                                    <td><img class="img-60 rounded-circle" src="{{isset($order_item->product) ? (!empty($order_item->product->image) ? $order_item->product->image : url('assets/images/default.png')) : url('assets/images/default.png')}}" alt="#" data-original-title="" title=""></td>
                                    {{--                                    <td>{{isset($order_item->product) ? ($order_item->product->subCategory ? $order_item->product->subCategory->name : "") : ""}}</td>--}}
                                    <td>{{isset($order_item->product) ? $order_item->product->name : ""}}</td>
                                    <!--<td>{{isset($order_item->description) ? $order_item->description : ""}}</td>-->
                                    <td>{{isset($order_item->product) ? $order_item->product->short_description : ""}}</td>
                                    <td>{{isset($order_item->product) ? ($order_item->product->has_discount == 1 ? $order_item->product->price_after : $order_item->product->price_before) : ""}}</td>
                                    <td>
                                        @isset($order_item->ordItemVars)
                                            @foreach($order_item->ordItemVars as $item_variation)
                                                <strong>{{$item_variation->variation_name}}</strong> : @if(ctype_xdigit(ltrim($item_variation->option_name, '#'))) <div style="width: 20px; height: 20px; display: inline-block; border-style: ridge; background-color: {{$item_variation->option_name}}"></div> @else {{$item_variation->option_name}} @endif
                                                <br><span>({{$item_variation->price/$order_item->quantity}}</span>
                                                <span>*</span>
                                                <span>{{$order_item->quantity}}</span>
                                                <span> =</span>
                                                <span>{{$item_variation->price}}</span>
                                                <span>ريال)</span>
                                                <br/>
                                                <br/>
                                            @endforeach
                                        @endisset
                                    </td>
                                    <td>{{isset($order_item->quantity) ? $order_item->quantity : ""}}</td>
                                    <td>{{isset($order_item->cost) ? $order_item->cost." ريال" : ""}}</td>
                                </tr>
                            @endforeach
                        @endisset
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('modals')

@endsection

@section('scripts')

    <script src="{{ url('/assets/js/owlcarousel/owl.carousel.js') }}"></script>
    <script src="{{ url('/assets/js/rating/jquery.barrating.js') }}"></script>
    <script src="{{ url('/assets/js/ecommerce.js') }}"></script>
    <script src="{{ url('/assets/js/jquery.printPage.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('#btnprn').printPage();
        });
    </script>
@section('scripts')
    <!-- Plugins JS start-->
    {{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
    {{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
    {{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}

    <script>
    $('.order_status').change(function () {
            var order_id = $(this).attr('data-order-id');
            var status = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('shop.orders.status')}}",
                data: {
                    order_id: order_id,
                    status: status,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response){
                    // $('.order_status option').prop('disabled', false);
                    // var index = $(this).find('option:selected').index();
                    // $('select').not(this).find('option:lt(' + index + ')').prop('disabled', true);
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });
</script>
@endsection

