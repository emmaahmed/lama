@extends('shop::admin.layouts.master')
@section('styles')
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>تعديل مشرف</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('shop.admins.update',['admin_id' => $data['admin']->id])}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="text-center">
                                <img id="image-display" width="200px" height="70px" class="profile-user-img img-fluid img-circle" style="border: 1px solid #e5eaff;"
                                     alt="Admin profile picture" src="{{$data['admin']->logo ? $data['admin']->logo : url('assets/images/default.png')}}" >
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name">إسم المشرف</label>
                                        <input class="form-control btn-square" name="name" id="name" value="{{$data['admin']->name}}" type="text" placeholder="اسم المشرف" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="email">البريد الإلكتروني</label>
                                        <input class="form-control btn-square" name="email" id="email" type="email" value="{{$data['admin']->email}}" placeholder="البريد الإلكتروني" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="password">كلمة المرور</label>
                                        <input class="form-control btn-square" name="password" id="password" type="password" placeholder="كلمة المرور">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label class="col-form-label m-l-10 float-right" for="status">حالة المستخدم</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="media-body text-left icon-state switch-outline">
                                                <label class="switch">
                                                    <input type="checkbox" value="1" id="status" name="status" @if($data['admin']->status == 1) checked @endif><span class="switch-state bg-success"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="logo" id="logo" type="file" data-original-title="" title="">
                                        <label class="custom-file-label" for="logo">صورة المشرف</label>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h3><b>الصلاحيات</b></h3>
                            <hr>
                            @foreach($data['all_permissions'] as $key => $permission)
                                <h5 style="color: #4466f2;">{{$key}}</h5>
                                <div class="row">
                                    @foreach($permission as $single_permission)
                                        <div class="col-md-2">
                                            <label for="check{{$single_permission->id}}">{{$single_permission->display_name}}</label>
{{--                                            <input type="checkbox" id="check{{$single_permission->id}}" name="permissions[]" value="{{ $single_permission->name }}"--}}
{{--                                           @if($data['admin']->can($single_permission->name)) checked @endif>--}}
                                            <label class="d-block" for="check{{$single_permission->id}}">
                                                <input type="checkbox" class="checkbox_animated" name="permissions[]" id="check{{$single_permission->id}}" value="{{ $single_permission->name }}"
                                                       @if($data['admin']->can($single_permission->name)) checked @endif>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
                {{--
                <div class="card">
                    <div class="card-header">
                        <h5>Custom controls</h5>
                    </div>
                    <form class="form theme-form">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Upload File</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row mb-0">
                                        <label class="col-sm-3 col-form-label">Custom select</label>
                                        <div class="col-sm-9">
                                            <select class="custom-select form-control">
                                                <option selected="">Open this select menu</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">Submit</button>
                            <input class="btn btn-light" type="reset" value="Cancel">
                        </div>
                    </form>
                </div>
                --}}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('modals')

@endsection

@section('scripts')
    <!-- Plugins JS start-->
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>
{{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image-display').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#logo").change(function() {
            readURL(this);
        });
    </script>
    <script>
        $('#editAdmin').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var email = button.data('email');
            var mobile = button.data('mobile');
            var image = button.data('image');
            var modal = $(this);
            modal.find('.modal-body #admin_id').val(id);
            modal.find('.modal-body #name').val(name);
            modal.find('.modal-body #email').val(email);
            modal.find('.modal-body #mobile').val(mobile);
            modal.find('.modal-body #profile-image').attr("src",image);
        });

        $('#editAdminForm').on('submit', function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: "{{URL::route('shop.admins.update')}}",
                method: "POST",
                processData: false,
                contentType: false,
                data: formData,
                dataType: 'json',
                success: function(response){
                    $('#editAdmin').modal('hide');
                    // window.location.reload();
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseText.message);
                }

            });
        });
        $(document).ready(function(){
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        myids.push(value);
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('shop.admins.deleteMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

        });
    </script>
@endsection
