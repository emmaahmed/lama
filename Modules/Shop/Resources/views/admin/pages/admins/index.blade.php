@extends('shop::admin.layouts.master')
@section('styles')
    <!-- Plugins css start-->
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
{{--                    <div class="card-header">--}}
{{--                        <h5>{{$data['title']}}</h5>--}}
{{--                    </div>--}}
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    @if(Auth::guard('shop')->user()->can('add_admin'))
                                        <a class="btn btn-square btn-success" href="{{route('shop.admins.create')}}" title="إضافة"> إضافة مشرف </a> &nbsp; &nbsp;
                                    @endif
                                    @if(Auth::guard('shop')->user()->can('delete_admin'))
                                        <button class="btn btn-square btn-danger" style="float: left; margin-right: 2%;" id="bulk_delete_btn" type="button" title="حذف متعدد">حذف متعدد</button>
                                    @endif
                                    <input type="hidden" name="myids" id="bulk_delete_input" value="">
                                    <br>
                                    <br>
                                    <th>
                                        <label class="d-block" for="chk-ani">
                                            <input class="checkbox_animated select_all" id="chk-ani" type="checkbox">
                                        </label>
                                    </th>
                                    <th>الصورة</th>
                                    <th>الإسم</th>
                                    <th>البريد الإلكتروني</th>
                                    <th>الحالة</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @isset($data['admins'])
                                    @foreach($data['admins'] as $admin)
                                    <tr>
                                        <td>
                                            <label class="d-block" for="chk-ani">
                                                <input type="checkbox" class="checkbox_animated" name="row_id" id="checkbox_{{ $admin->id }}" value="{{ $admin->id }}">
                                            </label>
                                        </td>
                                        <td><img class="img-60 rounded-circle" src="{{!empty($admin->getOriginal('logo')) ? $admin->logo : url('assets/images/user.png')}}" alt="#" data-original-title="" title=""></td>
                                        <td>{{isset($admin->name) ? $admin->name : ""}}</td>
                                        <td>{{isset($admin->email) ? $admin->email : ""}}</td>
                                        <td>
                                            @if($admin->status == 1)
                                                <span class="badge badge-info">مفعل</span>
                                            @else
                                                <span class="badge badge-warning">غير مفعل</span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="row">
{{--                                                @if(Auth::guard('admin')->user()->can('show_admin'))--}}
{{--                                                    <a href="{{route('admin.admins.show',['admin_id' => $admin->id])}}" title="view"> <i width="20" height="20" color="orange" data-feather="eye"></i></a> &nbsp; &nbsp;--}}
{{--                                                @endif--}}
                                                @if(Auth::guard('shop')->user()->can('edit_admin'))
                                                    <a title="تعديل" href="{{route('shop.admins.edit',['admin_id' => $admin->id])}}"><i width="20" height="20" color="green" data-feather="edit"></i></a> &nbsp; &nbsp;
                                                @endif
                                                @if(Auth::guard('shop')->user()->can('delete_admin'))
                                                    <a onclick='return deleteAdmin({{$admin->id}})' title="حذف" data-id="{{$admin->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                @endisset
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('modals')
    <div class="modal fade rtl" id="editAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">تعديل المستخدم</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <form id="editAdminForm">
                    @csrf
                    <div class="modal-body">
                        <div class="text-center">
                            <img src="" id="profile-image" class="img-thumbnail img-fluid" alt="Admin Profile" width="100" height="100" />
                        </div>
                        <input type="hidden" name="admin_id" id="admin_id"/>
                        <div class="form-group">
                            <label class="col-form-label float-right" for="name">الإسم</label>
                            <input class="form-control" name="name" id="name" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label float-right" for="email">البريد الإلكتروني</label>
                            <input class="form-control" name="email" id="email" type="email" value="">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label float-right" for="mobile">رقم الجوال</label>
                            <input class="form-control" name="mobile" id="mobile" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label float-right" for="gender">النوع</label>
                            <select name="gender" class="custom-select form-control" id="gender">
                                <option value="1">ذكر</option>
                                <option value="2">أنثي</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label float-right" for="image">الصورة الشخصية</label>
                            <input name="image" class="form-control" id="image" type="file" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary float-right" type="submit">حفظ</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">إغلاق</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <!-- Plugins JS start-->
{{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>
        function deleteAdmin(admin_id)
        {
            var id = admin_id;

            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('shop.admins.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {admin_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            toastr.success(response.success);
                            window.location.reload();
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
        }
        $(document).ready(function(){
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        myids.push(value);
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('shop.admins.deleteMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

            // End Bulk Delete
        });
    </script>
@endsection
