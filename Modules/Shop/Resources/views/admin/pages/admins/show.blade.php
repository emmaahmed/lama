{{--@extends('admin.layouts.master')--}}
{{--@section('styles')--}}
    {{--<!-- Plugins css start-->--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ url('/assets/css/photoswipe.css') }}">--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">--}}
{{--@endsection--}}
{{--@section('content')--}}
    {{--<div class="container-fluid">--}}
        {{--<div class="page-header">--}}
            {{--<div class="row">--}}
                {{--<div class="col">--}}
                    {{--<div class="page-header-left">--}}
                        {{--<h3>{{$title}}</h3>--}}
                        {{--<ol class="breadcrumb">--}}
                            {{--<li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>--}}
                            {{--<li class="breadcrumb-item">الرئيسية</li>--}}
                            {{--<li class="breadcrumb-item">{{$title}}</li>--}}
                        {{--</ol>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- Bookmark Start-->--}}
                {{--<div class="col">--}}
                    {{--<div class="bookmark pull-right">--}}
                        {{--<ul>--}}
                            {{--<li><a href="datatable-basic-init.html#" data-container="body" data-toggle="popover" data-placement="top" title="" data-original-title="Calendar"><i data-feather="calendar"></i></a></li>--}}
                            {{--<li><a href="datatable-basic-init.html#" data-container="body" data-toggle="popover" data-placement="top" title="" data-original-title="Mail"><i data-feather="mail"></i></a></li>--}}
                            {{--<li><a href="datatable-basic-init.html#" data-container="body" data-toggle="popover" data-placement="top" title="" data-original-title="Chat"><i data-feather="message-square"></i></a></li>--}}
                            {{--<li><a href="datatable-basic-init.html#"><i class="bookmark-search" data-feather="star"></i></a>--}}
                                {{--<form class="form-inline search-form">--}}
                                    {{--<div class="form-group form-control-search">--}}
                                        {{--<input type="text" placeholder="Search..">--}}
                                    {{--</div>--}}
                                {{--</form>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- Bookmark Ends-->--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- Container-fluid starts-->--}}
    {{--<div class="container-fluid">--}}
        {{--<div class="user-profile">--}}
            {{--<div class="row">--}}
                {{--<!-- user profile first-style start-->--}}
                {{--<div class="col-sm-12">--}}
                    {{--<div class="card hovercard text-center">--}}
                        {{--<div style="width: 948px; height: 470px;">--}}
                            {{--{!! Mapper::render() !!}--}}
                        {{--</div>--}}
                        {{--<div class="user-image">--}}
                            {{--<div class="avatar"><img alt="" src="{{$user->image ? $user->image->name : url('assets/images/user/7.jpg')}}"></div>--}}
{{--                            <div class="icon-wrapper"><i class="icofont icofont-pencil-alt-5"></i></div>--}}
                        {{--</div>--}}
                        {{--<div class="info">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-sm-6 col-lg-4 order-sm-1 order-xl-0">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="ttl-info text-left">--}}
                                                {{--<h6><i class="fa fa-envelope"></i>   الربيد الإلكتروني</h6><span>{{$user->email}}</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="ttl-info text-left">--}}
                                                {{--<h6><i class="fa fa-calendar"></i>   تاريخ الميلاد</h6><span>{{$user->date_of_birth}}</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-12 col-lg-4 order-sm-0 order-xl-1">--}}
                                    {{--<div class="user-designation">--}}
                                        {{--<div class="title"><a target="_blank" href="">{{$user->name}}</a></div>--}}
                                        {{--<div class="desc mt-2">عميل</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-6 col-lg-4 order-sm-2 order-xl-2">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="ttl-info text-left">--}}
                                                {{--<h6><i class="fa fa-phone"></i>   إتصل بنا</h6><span>{{$user->mobile}}</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="ttl-info text-left">--}}
                                                {{--<h6><i class="fa fa-location-arrow"></i>   الحالة</h6>--}}
                                                {{--@if($user->status == 1)--}}
                                                    {{--<span class="badge badge-info">نشط</span>--}}
                                                {{--@else--}}
                                                    {{--<span class="badge badge-warning">معلق</span>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<hr>--}}
                            {{--<div class="social-media">--}}
                                {{--<ul class="list-inline">--}}
                                    {{--<div class="rating-container">--}}
                                        {{--<div class="br-wrapper br-theme-fontawesome-stars">--}}
                                            {{--<div class="br-widget">--}}
                                                {{--@for ($i = 0; $i < 5; ++$i)--}}
                                                    {{--<a href="#" data-rating-value="1" data-rating-text="1" class="{{ $user->rate<=$i?'':'br-selected' }}"></a>--}}
                                                {{--@endfor--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            {{--<div class="follow">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-4 text-md-left border-right">--}}
                                        {{--<div class="follow-num counter">{{$user->orders_count}}</div>--}}
                                        {{--<span>--}}
                                            {{--<a title="كل الطلبات" href="{{route('admin.users.orders',['user_id' => $user->id , 'type' => 'all'])}}">كل الطلبات</a>--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-4 text-md-left border-right">--}}
                                        {{--<div class="follow-num counter">{{$user->user_orders_count}}</div>--}}
                                        {{--<span>--}}
                                            {{--<a title="طلباتي" href="{{route('admin.users.orders',['user_id' => $user->id , 'type' => 'user'])}}">طلباتي</a>--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-4 text-md-left">--}}
                                        {{--<div class="follow-num counter">{{$user->sales_orders_count}}</div>--}}
                                        {{--<span>--}}
                                            {{--<a title="توصيل" href="{{route('admin.users.orders',['user_id' => $user->id , 'type' => 'sales'])}}">توصيل</a>--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- user profile first-style end-->--}}
                {{--<!-- user profile second-style start-->--}}
                {{--<div class="col-sm-12">--}}
                    {{--<div class="card">--}}
                        {{--<div class="profile-img-style">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-sm-8">--}}
                                    {{--<div class="media"><img class="img-thumbnail rounded-circle mr-3" src="../assets/images/user/7.jpg" alt="Generic placeholder image">--}}
                                        {{--<div class="media-body align-self-center">--}}
                                            {{--<h5 class="mt-0 user-name">JOHAN DIO</h5>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-4 align-self-center">--}}
                                    {{--<div class="float-sm-right"><small>10 Hours ago</small></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<hr>--}}
                            {{--<p>you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</p>--}}
                            {{--<div class="img-container">--}}
                                {{--<div class="my-gallery" id="aniimated-thumbnials" itemscope="">--}}
                                    {{--<figure itemprop="associatedMedia" itemscope=""><a href="http://admin.pixelstrap.com/endless/assets/images/other-images/profile-style-img3.png" itemprop="contentUrl" data-size="1600x950"><img class="img-fluid rounded" src="http://admin.pixelstrap.com/endless/assets/images/other-images/profile-style-img3.png" itemprop="thumbnail" alt="gallery"></a>--}}
                                        {{--<figcaption itemprop="caption description">Image caption  1</figcaption>--}}
                                    {{--</figure>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="like-comment">--}}
                                {{--<ul class="list-inline">--}}
                                    {{--<li class="list-inline-item border-right pr-3">--}}
                                        {{--<label class="m-0"><a href="user-profile.html#"><i class="fa fa-heart"></i></a>  Like</label><span class="ml-2 counter">2659</span>--}}
                                    {{--</li>--}}
                                    {{--<li class="list-inline-item ml-2">--}}
                                        {{--<label class="m-0"><a href="user-profile.html#"><i class="fa fa-comment"></i></a>  Comment</label><span class="ml-2 counter">569</span>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">--}}
                    {{--<div class="pswp__bg"></div>--}}
                    {{--<div class="pswp__scroll-wrap">--}}
                        {{--<div class="pswp__container">--}}
                            {{--<div class="pswp__item"></div>--}}
                            {{--<div class="pswp__item"></div>--}}
                            {{--<div class="pswp__item"></div>--}}
                        {{--</div>--}}
                        {{--<div class="pswp__ui pswp__ui--hidden">--}}
                            {{--<div class="pswp__top-bar">--}}
                                {{--<div class="pswp__counter"></div>--}}
                                {{--<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>--}}
                                {{--<button class="pswp__button pswp__button--share" title="Share"></button>--}}
                                {{--<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>--}}
                                {{--<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>--}}
                                {{--<div class="pswp__preloader">--}}
                                    {{--<div class="pswp__preloader__icn">--}}
                                        {{--<div class="pswp__preloader__cut">--}}
                                            {{--<div class="pswp__preloader__donut"></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">--}}
                                {{--<div class="pswp__share-tooltip"></div>--}}
                            {{--</div>--}}
                            {{--<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>--}}
                            {{--<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>--}}
                            {{--<div class="pswp__caption">--}}
                                {{--<div class="pswp__caption__center"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- Container-fluid Ends-->--}}
{{--@endsection--}}

{{--@section('modals')--}}

{{--@endsection--}}

{{--@section('scripts')--}}
    {{--<script src="{{ url('/assets/js/counter/jquery.waypoints.min.js') }}"></script>--}}
    {{--<script src="{{ url('/assets/js/counter/jquery.counterup.min.js') }}"></script>--}}
    {{--<script src="{{ url('/assets/js/counter/counter-custom.js') }}"></script>--}}
    {{--<script src="{{ url('/assets/js/photoswipe/photoswipe.min.js') }}"></script>--}}
    {{--<script src="{{ url('/assets/js/photoswipe/photoswipe-ui-default.min.js') }}"></script>--}}
    {{--<script src="{{ url('/assets/js/photoswipe/photoswipe.js') }}"></script>--}}

{{--@endsection--}}
