@extends('shop::admin.layouts.master')
@section('styles')
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/owlcarousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="card">
            <div class="row product-page-main">
                <div class="col-xl-4">
                    <img width="100%" src="{{!empty($data['shop']->getOriginal('logo')) ? $data['shop']->logo : url('assets/images/default.png')}}" alt=""/>
                </div>
                <div class="col-xl-8">
                    <div class="product-page-details">
                        <div class="row">
                            <div class="col-md-6">
                                <strong>القسم التابع له : </strong>{{isset($data['shop']->category) ? $data['shop']->category->name : ''}}
                            </div>
                            <div class="col-md-6">
                                <strong>الإسم : </strong>{{isset($data['shop']->name) ? $data['shop']->name : ''}}
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>الإسم بالكامل : </strong>{{isset($data['shop']->full_name) ? $data['shop']->full_name : ''}}
                            </div>
                            <div class="col-md-6">
                                <strong>الوصف : </strong>{{isset($data['shop']->description) ? $data['shop']->description : ''}}
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>الرقم الضريبي : </strong>{{isset($data['order']->tax_number) ? $data['order']->tax_number : ''}}
                            </div>
                            <div class="col-md-6">
                                <strong>مدة التوصيل : </strong>{{isset($data['shop']->delivery_time) ? $data['shop']->delivery_time : ''}}
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>تكلفة التوصيل : </strong>{{isset($data['shop']->delivery_fees) ? $data['shop']->delivery_fees : ''}}
                            </div>
                            <div class="col-md-6">
                                <strong>رقم الهاتف : </strong>{{isset($data['shop']->phone) ? $data['shop']->phone : ''}}
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>البريد الإلكتروني : </strong>{{isset($data['shop']->email) ? $data['shop']->email : ''}}
                            </div>
                            <div class="col-md-6">
                                <strong>التقييم : </strong>
                                <div class="rating-container" style="display: inline;">
                                    <div class="br-wrapper br-theme-fontawesome-stars" style="display: inline;">
                                        <div class="br-widget" style="display: inline;">
                                            @for ($i = 0; $i < 5; ++$i)
                                                <a href="#" data-rating-value="1" data-rating-text="1" class="{{ $data['shop']->rate<=$i?'':'br-selected' }}"></a>
                                            @endfor
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>حالة المتجر : </strong>
                                @if($data['shop']->status == 0)
                                    <span class="badge badge-warning" style="padding-left: 15px;">غير مفعل</span>
                                @elseif($data['shop']->status == 1)
                                    <span class="badge badge-success" style="padding-left: 15px;">مفعل</span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <strong>الإتاحة : </strong>
                                @if($data['shop']->online == 0)
                                    <span class="badge badge-dark" style="padding-left: 15px;">مغلق</span>
                                @elseif($data['shop']->online == 1)
                                    <span class="badge badge-success" style="padding-left: 15px;">مفتوح</span>
                                @endif
                            </div>
                        </div>
                    <hr>
                </div>
            </div>
        </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h5>منتجات المتجر</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-9">
                        <thead>
                        <tr>
                            {{--  @if(Auth::guard('admin')->user()->can('delete_category'))--}}
                            <button class="btn btn-square btn-danger" style="float: left; margin-right: 2%;" id="bulk_delete_btn" type="button" title="حذف متعدد">حذف متعدد</button>
                            {{--  @endif--}}
                            <input type="hidden" name="myids" id="bulk_delete_input" value="">
                            <br>
                            <br>
                            <th>
                                <label class="d-block" for="chk-ani">
                                    <input class="checkbox_animated select_all" id="chk-ani" type="checkbox">
                                </label>
                            </th>
                            <th>الصورة</th>
                            <th>القسم التابع له</th>
                            <th>الإسم</th>
                            <th>الوصف</th>
                            <th>السعر</th>
                            <th>الكمية المتاحة</th>
                            <th>الحالة</th>
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @isset($data['shop']->shop_products)
                            @foreach($data['shop']->shop_products as $product)
                            <tr>
                                <td>
                                    <label class="d-block" for="chk-ani">
                                        <input type="checkbox" class="checkbox_animated" name="row_id" id="checkbox_{{ $product->id }}" value="{{ $product->id }}">
                                    </label>
                                </td>
                                <td><img class="img-60 rounded-circle" src="{{!empty($product->image) ? $product->image : url('assets/images/default.png')}}" alt="#" data-original-title="" title=""></td>
                                <td>{{isset($product->subCategory) ? $product->subCategory->name : ""}}</td>
                                <td>{{isset($product->name) ? $product->name : ""}}</td>
                                <td>{{isset($product->description) ? $product->description : ""}}</td>
                                <td>{{isset($product->initial_price) ? $product->initial_price : ""}}</td>
                                <td>{{isset($product->quantity) ? $product->quantity : ""}}</td>
                                <td>
                                    <div class="form-group">
                                        <div class="media-body icon-state switch-outline">
                                            <label class="switch">
                                                <input type="checkbox" id="status" class="product_status" data-product-id="{{$product->id}}" name="status" @if($product->status == 1) checked @endif><span class="switch-state bg-success"></span>
                                            </label>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        {{--  @if(Auth::guard('admin')->user()->can('delete_category'))--}}
                                        <a onclick='return deleteProduct({{$product->id}})' title="حذف" data-id="{{$product->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a>
                                        {{--  @endif--}}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        @endisset
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('modals')

@endsection

@section('scripts')
    <script src="{{ url('/assets/js/owlcarousel/owl.carousel.js') }}"></script>
    <script src="{{ url('/assets/js/rating/jquery.barrating.js') }}"></script>
    <script src="{{ url('/assets/js/ecommerce.js') }}"></script>
    <script src="{{ url('/assets/js/jquery.printPage.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('#btnprn').printPage();
        });
    </script>
@section('scripts')
    <!-- Plugins JS start-->
    {{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
    {{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
    {{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>
        $('.product_status').on('change.bootstrapSwitch', function(e) {
            var product_id = $(this).attr('data-product-id');
            var status = "";
            if (e.target.checked == true){
                var status = 1;
            }else{
                var status = 0;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('admin.shops.products.status')}}",
                data: {
                    product_id: product_id,
                    status: status,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response){
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });
        function deleteProduct(product_id)
        {
            var id = product_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.shops.products.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {product_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
            //
        }
        $(document).ready(function(){
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        myids.push(value);
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.shops.products.deleteMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

            // End Bulk Delete
        });
    </script>
@endsection

@endsection
