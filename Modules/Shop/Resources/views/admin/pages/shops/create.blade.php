@extends('shop::admin.layouts.master')

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$data['title']}}</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.shops.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="category_id">الأقسام</label>
                                        <select name="category_id" class="custom-select form-control" id="category_id" required>
                                            <option value="" disabled selected>اختر القسم</option>
                                            @foreach($data['categories'] as $category)
                                                <option value="{{$category->id}}" {{ (old("category_id") == $category->id ? "selected":"") }}>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">إسم المتجر باللغة العربية</label>
                                        <input class="form-control btn-square" name="name_ar" id="name_ar" type="text" placeholder="إسم المتجر باللغة العربية" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_en">إسم المتجر باللغة الإنجليزية</label>
                                        <input class="form-control btn-square" name="name_en" id="name_en" type="text" placeholder="إسم المتجر باللغة الإنجليزية" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">الإسم بالكامل باللغة العربية</label>
                                        <input class="form-control btn-square" name="full_name_ar" id="full_name_ar" type="text" placeholder="الإسم بالكامل باللغة العربية" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_en">الإسم بالكامل باللغة الإنجليزية</label>
                                        <input class="form-control btn-square" name="full_name_en" id="full_name_en" type="text" placeholder="الإسم بالكامل باللغة الإنجليزية" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="description_ar">الوصف (باللغة العربية)</label>
                                        <textarea class="form-control" name="description_ar" id="description_ar" cols="20" rows="4" required></textarea>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="description_en">الوصف (باللغة الانجليزية)</label>
                                        <textarea class="form-control" name="description_en" id="description_en" cols="20" rows="4" required></textarea>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="tax_number">الرقم الضريبي</label>
                                        <input class="form-control btn-square" name="tax_number" id="tax_number" type="text" placeholder="الرقم الضريبي" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="delivery_time">وقت التوصيل</label>
                                        <input class="form-control btn-square" name="delivery_time" id="delivery_time" type="text" placeholder="وقت التوصيل" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="delivery_fees">مصاريف التوصيل</label>
                                        <input class="form-control btn-square" name="delivery_fees" id="delivery_fees" type="text" placeholder="مصاريف التوصيل" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="phone">رقم الهاتف</label>
                                        <input class="form-control btn-square" name="phone" id="phone" type="text" placeholder="رقم الهاتف" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h5>بيانات لوحة التحكم</h5>
                                </div>
                                <div class="card-body shadow-sm p-3 mb-5 bg-white rounded">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="email">البريد الإلكتروني</label>
                                                <input class="form-control btn-square" name="email" id="email" type="email" placeholder="البريد الإلكتروني" required>
                                                <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                                <div class="valid-feedback">بيانات صحيحة</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="password">كلمة المرور</label>
                                                <input class="form-control btn-square" name="password" id="password" type="password" placeholder="كلمة المرور" required>
                                                <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                                <div class="valid-feedback">بيانات صحيحة</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="business_registeration" id="business_registeration" type="file" required="" data-original-title="" title="">
                                        <label class="custom-file-label" for="business_registeration">السجل التجاري</label>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="logo" id="logo" type="file" required="" data-original-title="" title="">
                                        <label class="custom-file-label" for="logo">لوجو المتجر</label>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="images[]" id="images" type="file" required="" multiple data-original-title="" title="">
                                        <label class="custom-file-label" for="images">صور المتجر</label>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
@endsection

