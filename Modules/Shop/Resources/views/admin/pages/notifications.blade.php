@extends('shop::admin.layouts.master')
@section('styles')
    <!-- Plugins css start-->
    {{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
    {{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
                    {{--                    <div class="card-header">--}}
                    {{--                        <h5>{{$data['title']}}</h5>--}}
                    {{--                    </div>--}}
                    <div class="card-body">
                        <ul class="notification-dropdown " id="notifications-ul">
                            @if( count($data['notifications'])>0)
                                @foreach($data['notifications'] as $notification)
                                    @if(isset($notification->order_number))
                                        <li>
                                            <a href="{{route('shop.orders.details',['order_id' => $notification->id])}}">
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h6 class="mt-0">
                                                            <span><i class="shopping-color" data-feather="shopping-bag"></i></span>
                                                            لديك طلب جديد!
                                                            <small class="pull-right">{{$notification->created_at}}</small>
                                                        </h6>
                                                        <p class="mb-0">اضغط هنا لتصفح الطلب</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <br>
                                        <hr>

                                    @else
                                        <li>
                                            <a href="{{route('shop.replyUser',$notification->sender_id)}}">
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h6 class="mt-0"><span><i class="shopping-color" data-feather="shopping-bag"></i></span>
                                                            {{$notification->message}}
                                                            <small class="pull-right">{{$notification->created_at}}</small>
                                                        </h6>
                                                        <p class="mb-0">أضغط هنا لتصفح الرسالة</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <br>
                                        <hr>

                                    @endif

                                @endforeach
                            @endif
                        </ul>

                    </div>
                </div>
            </div>
            <!-- State saving Ends-->

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
