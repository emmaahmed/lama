<div class="page-sidebar">
  <div class="main-header-left d-none d-lg-block">
{{--    <div class="logo-wrapper"><a href="#"><img style="margin-top: 595%;" src="{{url('/assets/images/grand.png')}}" alt="غصن"></a></div>--}}
    <div class="logo-wrapper"><a href="#"><img src="{{url('/assets/images/logo-horizontal.png')}}" alt="غصن"></a></div>
  </div>
  <div class="sidebar custom-scrollbar">
    <div class="sidebar-user text-center">
      <div><img class="img-60 rounded-circle" src="{{Auth::guard('shop')->user()->logo ? Auth::guard('shop')->user()->logo : url('assets/images/user.png')}}" alt="profile">
        {{--<div class="profile-edit"><a href="{{route('shop.edit')}}"><i data-feather="edit"></i></a></div>--}}
      </div>
      <h6 class="mt-3 f-14">{{Auth::guard('shop')->user()->name}}</h6>
      <p>مشرف</p>
    </div>
    <ul class="sidebar-menu">
        @if(Auth::guard('shop')->user()->can('show_statistics'))
            <li @if(request()->segment(2) == 'dashboard') class="active" @endif> <a class="sidebar-header" href="{{route('shop.dashboard')}}"><i data-feather="home"></i><span> الرئيسية</span></a></li>
        @endif
        @if(Auth::guard('shop')->user()->can('show_admin'))
            <li @if(request()->segment(2) == 'admins') class="active" @endif> <a class="sidebar-header" href="{{route('shop.admins')}}"><i data-feather="user-check"></i><span> المشرفين</span></a></li>
        @endif
{{--      @if(Auth::guard('shop')->user()->can('show_user'))--}}
{{--            <li @if(request()->segment(2) == 'users') class="active" @endif> <a class="sidebar-header" href="{{route('shop.users')}}"><i data-feather="users"></i><span> المستخدمين</span></a></li>--}}
{{--      @endif--}}
        @if(Auth::guard('shop')->user()->can('show_sub_category'))
            <li @if(request()->segment(2) == 'sub_categories') class="active" @endif> <a class="sidebar-header" href="{{route('shop.subCategories')}}"><i data-feather="list"></i><span> الأقسام</span></a></li>
        @endif
        @if(Auth::guard('shop')->user()->can('show_product'))
            <li @if(request()->segment(2) == 'products') class="active" @endif> <a class="sidebar-header" href="{{route('shop.products')}}"><i data-feather="list"></i><span> المنتجات</span></a></li>
            <li @if(request()->segment(2) == 'not-active-products') class="active" @endif> <a class="sidebar-header" href="{{route('shop.not-active-products')}}"><i data-feather="list"></i><span> المنتجات الغير المفعلة</span></a></li>
        @endif
        @if(Auth::guard('shop')->user()->can('show_offer'))
            <li @if(request()->segment(2) == 'offers') class="active" @endif> <a class="sidebar-header" href="{{route('shop.offers')}}"><i data-feather="list"></i><span> العروض</span></a></li>
        @endif
        @if(Auth::guard('shop')->user()->can('show_order'))
            <li @if(request()->segment(2) == 'orders') class="active" @endif> <a class="sidebar-header" href="{{route('shop.orders')}}"><i data-feather="list"></i><span> الطلبات</span></a></li>
        @endif

        @if( Auth::guard('shop')->user()->can('show_chat'))
                <li><a class="sidebar-header" href="{{route('shop.chat')}}" data-original-title="" title="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-square"><path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg>
                        <span>الشات مع الادمن</span></a>
                </li>
                {{--                <li> <a class="sidebar-header" @if(request()->segment(2) == 'dashboard') style="color: white;" @endif href="{{route('admin.dashboard')}}"><i data-feather="home"></i><span> الرئيسية</span></a></li>--}}
        @endif

        @if( Auth::guard('shop')->user()->can('show_chat'))
            <li>
            <a class="sidebar-header" href="{{route('shop.user_shop_chat')}}" data-original-title="" title=" ">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-square"><path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg>
                <span>شات المناديب</span>
            </a>
            </li>
                <li>
                    <a class="sidebar-header" href="{{route('shop.user_shop_chat_shop')}}" data-original-title="" title=" ">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-square"><path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg>
                        <span>شات المستخدمين</span>
                    </a>
                </li>
        @endif


        @if( Auth::guard('shop')->user()->can('notifications') ||  Auth::guard('shop')->user()->can('send_notification'))
            <li @if(request()->segment(3) == 'notifications') class="open" @endif><a class="sidebar-header" href="#"><i data-feather="message-square"></i><span>الرسائل والإشعارات</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu @if(request()->segment(3) == 'notifications') open @endif">
                    @if( Auth::guard('shop')->user()->can('notifications'))
                        <li> <a @if(request()->segment(3) == 'notifications') style="color: white;" @endif class="sidebar-header" href="{{route('shop.notifications2')}}"><i data-feather="message-circle"></i><span> صندوق الصادر</span></a></li>
                    @endif
                   @if( Auth::guard('shop')->user()->can('send_notification'))
                        <li> <a @if(request()->segment(3) == 'notifications') style="color: white;" @endif class="sidebar-header" href="{{route('shop.notifications.send2')}}"><i data-feather="send"></i><span> إرسال إشعار</span></a></li>
                    @endif
                </ul>
            </li>
        @endif
        @if(Auth::guard('shop')->user()->can('edit_settings'))
            <li @if(request()->segment(1) == 'settings') class="active" @endif> <a class="sidebar-header" href="{{route('shop.settings')}}"><i data-feather="settings"></i><span> الإعدادات</span></a></li>
        @endif
{{--        @if(Auth::guard('shop')->user()->can('show_reports'))--}}
{{--            <li @if(request()->segment(2) == 'reports') class="active" @endif> <a class="sidebar-header" href="{{route('shop.orders.reports')}}"><i data-feather="list"></i><span> التقارير</span></a></li>--}}
{{--        @endif--}}
    </ul>
  </div>
</div>
