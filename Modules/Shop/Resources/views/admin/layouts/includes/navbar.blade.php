<!-- Page Header Start-->
<div class="page-main-header">
  <div class="main-header-right row">
    <div class="main-header-left d-lg-none">
      <div class="logo-wrapper"><a href="{{route('shop.dashboard')}}"><img src="{{url('assets/images/logo.png')}}" alt="غصن"></a></div>
    </div>
    <div class="mobile-sidebar d-block">
      <div class="media-body text-right switch-sm">
        <label class="switch"><a href="#"><i id="sidebar-toggle" data-feather="align-left"></i></a></label>
      </div>
    </div>
      <b class="badge badge-danger">تاريخ انتهاء الاشتراك : {{\Illuminate\Support\Facades\Auth::guard('shop')->user()->expire_at}}</b>
      @if(\Illuminate\Support\Facades\Auth::guard('shop')->user()->type == 0)
          <div class="nav-right col p-0">
              <div class="media">
                  <div class="media-body text-right icon-state switch-outline">
                      <label class="switch">
                          <input type="checkbox" id="shop-status"  @if(\Illuminate\Support\Facades\Auth::guard('shop')->user()->online == 1) checked @endif><span class="switch-state bg-success"></span>
                      </label>
                  </div>
              </div>
          </div>
      @endif
    <div class="nav-right col p-0">
      <ul class="nav-menus">
          <li class="onhover-dropdown"><span class="badge badge-pill badge-primary pull-right" id="order-counter">0</span><i data-feather="bell"></i><span class="dot"> </span>
              <ul class="notification-dropdown onhover-show-div" id="notifications-ul">
                  @if(isset($notifications) && count($notifications)>0)
                      @foreach($notifications as $notification)
                          @if(isset($notification->order_number))
                                    <li>
                                          <a href="{{route('shop.orders.details',['order_id' => $notification->id])}}">
                                              <div class="media">
                                       <div class="media-body">
                                       <h6 class="mt-0">
                                           <span><i class="shopping-color" data-feather="shopping-bag"></i></span>
                                            لديك طلب جديد!
                                      <small class="pull-right">{{$notification->created_at}}</small>
                                       </h6>
                                        <p class="mb-0">اضغط هنا لتصفح الطلب</p>
                                        </div>
                                              </div>
                                          </a>
                                    </li>
                          @else
                              <li>
                                  <a href="{{route('shop.replyUser',$notification->sender_id)}}">
                                      <div class="media">
                                          <div class="media-body">
                                              <h6 class="mt-0"><span><i class="shopping-color" data-feather="shopping-bag"></i></span>
                                                  {{$notification->message}}
                                                  <small class="pull-right">{{$notification->created_at}}</small>
                                              </h6>
                                              <p class="mb-0">أضغط هنا لتصفح الرسالة</p>
                                          </div>
                                      </div>
                                  </a>
                              </li>

                          @endif

                      @endforeach
                      @endif
                      <li>
                          <a href="{{route('shops.notifications')}}">
                          كل الإشعارات
                          </a>
                      </li>

              </ul>
          </li>

          <li class="onhover-dropdown">
          <div class="media align-items-center"><img class="align-self-center pull-right img-50 rounded-circle" src="{{Auth::guard('shop')->user()->logo ? Auth::guard('shop')->user()->logo : url('assets/images/user.png')}}" alt="header-user">
            <div class="dotted-animation"><span class="animate-circle"></span><span class="main-circle"></span></div>
          </div>
          <ul class="profile-dropdown onhover-show-div p-20">
              @if(\Illuminate\Support\Facades\Auth::guard('shop')->user()->type == 0)
                <li><a href="{{route('shop.edit')}}"><i data-feather="user"></i> تعديل المتجر</a></li>
              @endif
            <li><a href="{{route('shop.logout')}}"><i data-feather="log-out"></i> تسجيل خروج</a></li>
          </ul>
        </li>

      </ul>
      <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
    </div>
    <script id="result-template" type="text/x-handlebars-template">
      <div class="ProfileCard u-cf">
        <div class="ProfileCard-avatar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay m-0"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg></div>
        <div class="ProfileCard-details">
          <div class="ProfileCard-realName">{{\Illuminate\Support\Facades\Auth::guard('shop')->user()->name}}</div>
        </div>
      </div>
    </script>
    <script id="empty-template" type="text/x-handlebars-template">
      <div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div>

    </script>
  </div>
</div>
