    @include('shop::admin.layouts.includes.header')
    <!-- Page Header Start-->
    @yield('modals')
    @include('shop::admin.layouts.includes.navbar')
    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        @include('shop::admin.layouts.includes.menu')
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->

        <!-- Right sidebar Ends-->
        <div class="page-body">
            @include('shop::admin.layouts.includes.message')
            @if (\Illuminate\Support\Facades\Auth::guard('shop')->user())
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col">
                                <div class="page-header-left">
                                    <h3>{{$data['title']}}</h3>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#"><i data-feather="home"></i></a></li>
                                        <li class="breadcrumb-item">الرئيسية</li>
                                        <li class="breadcrumb-item">{{$data['title']}}</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @yield('content')
        </div>
    </div>
    @include('shop::admin.layouts.includes.footer')
