<!DOCTYPE html>
<html lang="en">

<head>
{{--    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">--}}
{{--    <meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
{{--    <meta name="description" content="">--}}
{{--    <meta name="keywords" content="">--}}
{{--    <meta name="author" content="pixelstrap">--}}
{{--    <link rel="icon" href="../assets/images/favicon.png" type="image/x-icon">--}}
{{--    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon">--}}
{{--    <title>Order - invoice</title>--}}
{{--    <!-- Google font-->--}}
{{--    <link rel="stylesheet" type="text/css" href="../assets/css/print.css">--}}
{{--    <!-- Plugins css Ends-->--}}
{{--    <!-- Bootstrap css-->--}}
{{--    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">--}}
{{--    <!-- App css-->--}}
{{--    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">--}}
{{--    <!-- Responsive css-->--}}
{{--    <link rel="stylesheet" type="text/css" href="../assets/css/responsive.css">--}}
    <style>
        body {
            font-family: sans-serif;
            font-size: 10pt;
        }

        p {
            margin: 0pt;
        }

        table.items {
            border: 0.1mm solid #e7e7e7;
        }

        td {
            vertical-align: top;
        }

        .items td {
            border-left: 0.1mm solid #e7e7e7;
            border-right: 0.1mm solid #e7e7e7;
        }

        table thead td {
            text-align: center;
            border: 0.1mm solid #e7e7e7;
        }

        .items td.blanktotal {
            background-color: #EEEEEE;
            border: 0.1mm solid #e7e7e7;
            background-color: #FFFFFF;
            border: 0mm none #e7e7e7;
            border-top: 0.1mm solid #e7e7e7;
            border-right: 0.1mm solid #e7e7e7;
        }

        .items td.totals {
            text-align: right;
            border: 0.1mm solid #e7e7e7;
        }

        .items td.cost {
            text-align: center;
        }
        @page {
            header: page-header;

            footer: page-footer;
        }

    </style>

</head>

<body>
<htmlpageheader name="page-header" >

</htmlpageheader>

<div style="text-align: right;">
    <div class="row">
        <strong>صورة المبني : </strong>
        <img width="70px" height="70px"
             src="{{isset($data['order']->address->address) ? $data['order']->address->building_image : url('assets/images/default.png')}}"
             alt=""/>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">

            <strong>رقم الطلب : </strong>
            <span style="color: #0b43c6;">{{isset($data['order']->order_number) ? $data['order']->order_number : ''}}</span>
        </div>
        <div class="col-md-6">
            <strong>نوع التوصيل : </strong>
            @if($data['order']->delivery_type == 0)
                <span class="badge badge-secondary" style="padding-left: 15px;">عادي</span>
            @elseif($data['order']->delivery_type == 1)
                <span class="badge badge-primary" style="padding-left: 15px;">سريع</span>
            @elseif($data['order']->delivery_type == 2)
                <span class="badge badge-primary" style="padding-left: 15px;">من الفرع</span>
            @endif
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <strong>صاحب الطلب : </strong>{{isset($data['order']->user) ? $data['order']->user->name : ''}}
        </div>
        <div class="col-md-6">
        <!--<a href="{{route('admin.shops.products.show',['shop_id' => $data['order']->shop->id])}}">
                                    <strong>المتجر : </strong>{{isset($data['order']->shop) ? $data['order']->shop->name : ''}}
            </a>-->
            <strong>موبايل صاحب الطلب : </strong>{{isset($data['order']->user) ? $data['order']->user->phone : ''}}
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            <strong>تكلفة الطلب : </strong>{{isset($data['order']->total_cost) ? $data['order']->total_cost : ''}}
            ريال
        </div>
        <div class="col-md-6">
            <strong>العنوان : </strong>
            @if (isset($data['order']->address))
                @if (isset($data['order']->address->address))
                    {{$data['order']->address->address}}
                @else
                    {{$data['order']->address->area}}
                @endif
            @endif
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            <strong>رقم المبني : </strong>{{isset($data['order']->address->building_number) ? $data['order']->address->building_number : ''}}

        </div>
        <div class="col-md-6">
            <strong>رقم الشقة : </strong>{{isset($data['order']->address->apartment) ? $data['order']->address->apartment : ''}}
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-md-12">
            <strong>عنوان اضافي : </strong>{{isset($data['order']->address->more_info) ? $data['order']->address->more_info : ''}}

        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-md-6">
            <strong>مصاريف الشحن : </strong>
            @if($data['order']->delivery_type==2)
                0
            @elseif($data['order']->delivery_type==1)
                {{$data['order']['fast_delivery'] }}

            @else
                {{$data['order']->shop->delivery_fees}}
            @endif
            ريال
        </div>
        <div class="col-md-6">
            <strong>وقت التوصيل : </strong>{{isset($data['order']->delivery_time) ? $data['order']->delivery_time : 'غير محدد'}}
        <!--دقيقة-->
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <strong style="color: red">  ملاحظات :
                {{($data['order']->notes)}}
            </strong>
        </div>

    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            <strong>إجمالي التكلفة : </strong>
            @if($data['order']->delivery_type==2)
                {{$data['order']->total_cost}}
            @elseif($data['order']->delivery_type==1)
                {{$data['order']->total_cost + $data['order']['fast_delivery'] }}
            @else
                {{($data['order']->total_cost + $data['order']->shop->delivery_fees)}}
            @endif

            ريال
        </div>
        <div class="col-md-6">
            <strong>نوع الدفع : </strong>
            @if($data['order']->payment == 0)
                <span class="badge badge-dark" style="padding-left: 15px;">كاش</span>
            @elseif($data['order']->payment == 1)
                <span class="badge badge-success" style="padding-left: 15px;">أونلاين</span>
            @elseif($data['order']->payment == 2)
                <span class="badge badge-success" style="padding-left: 15px;">شبكة</span>
            @endif
        </div>
    </div>
    <hr/>
    @if($data['order']->delegate_id != NULL)
        <div class="row">
            <div class="col-md-6">
                <strong> اسم المندوب  : </strong>{{isset($data['order']->delegate) ? $data['order']->delegate->name : ''}}
            </div>
            <div class="col-md-6">
                <strong> موبايل المندوب  : </strong>{{isset($data['order']->delegate) ? $data['order']->delegate->phone : ''}}
            </div>
        </div>
        <hr/>
    @endif
    <div class="row">
        <div class="col-md-6">
            <strong> توقيت الطلب :
                {{($data['order']->created_at_time)}}
            </strong>
        </div>
        {{--                            <div class="col-md-6">--}}
        {{--                                <strong> طريقة استلام الطلب : </strong>--}}
        {{--                                @if($data['order']->delivery_type == 0)--}}
        {{--                                    <span class="badge badge-success">توصيل عادي</span>--}}
        {{--                                @elseif($data['order']->delivery_type == 1)--}}
        {{--                                    <span class="badge badge-success">توصيل سريع</span>--}}
        {{--                                @elseif($data['order']->delivery_type == 2)--}}
        {{--                                    <span class="badge badge-success">استلام من الفرع</span>--}}
        {{--                                @endif--}}
        {{--                            </div>--}}


    </div>
    <hr/>
</div>
{{--<table width="100%" style="font-family: sans-serif;" cellpadding="10">--}}
{{--    <tr>--}}
{{--        <td width="49%" style="border: 0.1mm solid #eee;"><img src="{{$data['order']->delegate->image}}" style="width: 60px !important; height: 60px; border-radius: 50% !important;" alt=""><br><strong>Delivery name:</strong>{{$data['order']->delegate->f_name}} {{$data['order']->delegate->l_name}}<br>Car Number: {{$data['order']->delegate->car_num}}<br>Phone Number: {{$data['order']->delegate->phone}}</td>--}}
{{--        <td width="2%">&nbsp;</td>--}}
{{--        <td width="49%" style="border: 0.1mm solid #eee; text-align: right;"> Customer Name : {{$data['order']->user->name}} <br> Phone Number : {{$data['order']->user->phone}} <br>  </td>--}}
{{--    </tr>--}}
{{--</table>--}}
<br>
<br>
<table class="items" width="100%" style="font-size: 14px; border-collapse: collapse;" cellpadding="8">
    <thead>
    <tr style='padding:0; margin:0'>
        <th>الصورة</th>
        <th>الإسم</th>
        <th>الوصف</th>
        <th>سعر المنتج</th>
        <th>التفاصيل</th>
        <th>الكمية المطلوبة</th>
        <th>السعر</th>
    </tr>
    </thead>
    <tbody  style='padding:0; margin:0'>
    @isset($data['order']->orderItems)
        @foreach($data['order']->orderItems as $order_item)
            <tr  style='padding:0; margin:0'>
                <td><img width="50%" class="img-60 rounded-circle" src="{{isset($order_item->product) ? (!empty($order_item->product->image) ? $order_item->product->image : url('assets/images/default.png')) : url('assets/images/default.png')}}" alt="#" data-original-title="" title=""></td>
                {{--                                    <td>{{isset($order_item->product) ? ($order_item->product->subCategory ? $order_item->product->subCategory->name : "") : ""}}</td>--}}
                <td>{{isset($order_item->product) ? $order_item->product->name : ""}}</td>
            <!--<td>{{isset($order_item->description) ? $order_item->description : ""}}</td>-->
                <td>{{isset($order_item->product) ? $order_item->product->short_description : ""}}</td>
                <td>{{isset($order_item->product) ? ($order_item->product->has_discount == 1 ? $order_item->product->price_after : $order_item->product->price_before) : ""}}</td>
                <td>
                    @isset($order_item->ordItemVars)
                        @foreach($order_item->ordItemVars as $item_variation)
                            <strong>{{$item_variation->variation_name}}</strong> : @if(ctype_xdigit(ltrim($item_variation->option_name, '#'))) <div style="width: 20px; height: 20px; display: inline-block; border-style: ridge; background-color: {{$item_variation->option_name}}"></div> @else {{$item_variation->option_name}} @endif
                            <br><span>({{$item_variation->price/$order_item->quantity}}</span>
                            <span>*</span>
                            <span>{{$order_item->quantity}}</span>
                            <span> =</span>
                            <span>{{$item_variation->price}}</span>
                            <span>ريال)</span>
                            <br/>
                            <br/>
                        @endforeach
                    @endisset
                </td>
                <td>{{isset($order_item->quantity) ? $order_item->quantity : ""}}</td>
                <td>{{isset($order_item->cost) ? $order_item->cost." ريال" : ""}}</td>
            </tr>
        @endforeach
    @endisset
    </tbody>
</table>
<br>
{{--<table width="40%" align="right" style="font-family: sans-serif; font-size: 14px;" >--}}
{{--    <tr>--}}
{{--        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;"><strong>Total without Vat</strong></td>--}}
{{--        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;">{{$data['order']->total_cost - ($tax->val * $data['order']->total_cost/100)}}  SAR</td>--}}
{{--    </tr>--}}
{{--    <tr>--}}
{{--        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;"><strong>Total With Vat</strong></td>--}}
{{--        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;">{{$data['order']->total_cost }} SAR</td>--}}
{{--    </tr>--}}
{{--    <tr>--}}
{{--        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;"><strong>Total Delivery</strong></td>--}}
{{--        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;">{{$data['order']->offer->offer}} </td>--}}
{{--    </tr>--}}
{{--    <tr>--}}
{{--        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;"><strong>Total</strong></td>--}}
{{--        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;">{{$data['order']->total_cost +$data['order']->offer->offer }} SAR</td>--}}
{{--    </tr>--}}
{{--</table>--}}

<br>
<htmlpagefooter name="page-footer">
    <div class="col-md-12 footer-copyright" style="text-align: center">
        <a href="https://amrk.my-staff.net/" target="_blank">
            <b class="mb-0" style="color: #C32F45;"> جميع حقوق النشر محفوظة - لمة 2022
            </b>
        </a>

    </div>
</htmlpagefooter>

</body>
</html>
