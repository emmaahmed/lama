<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/25/2020
 * Time: 2:13 PM
 */

namespace Modules\Shop\Repositories;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Order\Entities\Order;
use Modules\Product\Entities\Product;
use Modules\Shop\Entities\Follower;
use Modules\Shop\Entities\Shop;
use Modules\Shop\Repositories\Interfaces\ShopAdminRepositoryInterface;

class ShopAdminRepository implements ShopAdminRepositoryInterface
{
    public function statistics()
    {
//        dd(Auth::guard('shop')->user()->getPermissionsViaRoles());
        $shop = Auth::guard('shop')->user();
          if($shop->parent_id){
              $shop=Shop::whereId($shop->parent_id)->first();
          }
        $products = Product::where('shop_id',$shop->id)->count();
        $orders = Order::where('shop_id',$shop->id)->count();

        $productstatistics = array('7dayago'=>0,'6dayago'=>0,'5dayago'=>0,'4dayago'=>0,'3dayago'=>0,'2dayago'=>0,'1dayago'=>0,'today'=>0);
        $allproducts = Product::where('shop_id',$shop->id)->where( 'id' ,'>' ,0)->select('created_at')->get();
        foreach ($allproducts  as $oneproduct){
            if ($oneproduct->getOriginal('created_at') < Carbon::now()->subDays(7) && $oneproduct->getOriginal('created_at') > Carbon::now()->subDays(8)){
                $productstatistics['7dayago'] ++ ;
            }if ($oneproduct->getOriginal('created_at') < Carbon::now()->subDays(6) && $oneproduct->getOriginal('created_at') > Carbon::now()->subDays(7)){
                $productstatistics['6dayago'] ++ ;
            }if ($oneproduct->getOriginal('created_at') < Carbon::now()->subDays(5 ) && $oneproduct->getOriginal('created_at') > Carbon::now()->subDays(6) ){
                $productstatistics['5dayago'] ++ ;
            } if ($oneproduct->getOriginal('created_at') < Carbon::now()->subDays(4) && $oneproduct->getOriginal('created_at') > Carbon::now()->subDays(5) ){
                $productstatistics['4dayago'] ++ ;
            } if ($oneproduct->getOriginal('created_at') < Carbon::now()->subDays(3) && $oneproduct->getOriginal('created_at') > Carbon::now()->subDays(4) ){
                $productstatistics['3dayago'] ++ ;
            } if ($oneproduct->getOriginal('created_at') < Carbon::now()->subDays(2) && $oneproduct->getOriginal('created_at') > Carbon::now()->subDays(3) ){
                $productstatistics['2dayago'] ++ ;
            }if ($oneproduct->getOriginal('created_at') < Carbon::now()->subDays(1) && $oneproduct->getOriginal('created_at') > Carbon::now()->subDays(2) ){
                $productstatistics['1dayago'] ++ ;
            } if ($oneproduct->getOriginal('created_at') < Carbon::now() && $oneproduct->getOriginal('created_at') > Carbon::now()->subDays(1) ){
                $productstatistics['today'] ++ ;
            }
        }

        $ordersstatistics=array('7dayago'=>0,'6dayago'=>0,'5dayago'=>0,'4dayago'=>0,'3dayago'=>0,'2dayago'=>0,'1dayago'=>0,'today'=>0);
        $allorders = Order::where('shop_id',$shop->id)->where( 'id' ,'>' ,0)->select('created_at')->get();
        foreach ($allorders  as $order){
            if ($order->getOriginal('created_at') < Carbon::now()->subDays(7) && $order->getOriginal('created_at') > Carbon::now()->subDays(8)){
                $ordersstatistics['7dayago'] ++ ;
            }if ($order->getOriginal('created_at') < Carbon::now()->subDays(6) && $order->getOriginal('created_at') > Carbon::now()->subDays(7)){
                $ordersstatistics['6dayago'] ++ ;
            }if ($order->getOriginal('created_at') < Carbon::now()->subDays(5 ) && $order->getOriginal('created_at') > Carbon::now()->subDays(6) ){
                $ordersstatistics['5dayago'] ++ ;
            } if ($order->getOriginal('created_at') < Carbon::now()->subDays(4) && $order->getOriginal('created_at') > Carbon::now()->subDays(5) ){
                $ordersstatistics['4dayago'] ++ ;
            } if ($order->getOriginal('created_at') < Carbon::now()->subDays(3) && $order->getOriginal('created_at') > Carbon::now()->subDays(4) ){
                $ordersstatistics['3dayago'] ++ ;
            } if ($order->getOriginal('created_at') < Carbon::now()->subDays(2) && $order->getOriginal('created_at') > Carbon::now()->subDays(3) ){
                $ordersstatistics['2dayago'] ++ ;
            }if ($order->getOriginal('created_at') < Carbon::now()->subDays(1) && $order->getOriginal('created_at') > Carbon::now()->subDays(2) ){
                $ordersstatistics['1dayago'] ++ ;
            } if ($order->getOriginal('created_at') < Carbon::now() && $order->getOriginal('created_at') > Carbon::now()->subDays(1) ){
                $ordersstatistics['today'] ++ ;
            }
        }
        $latest_orders = Order::where('shop_id',$shop->id)->orderBy('id', 'desc')->take(10)->get();
        foreach ($latest_orders as $order){
            if (empty($order->getOriginal('image'))){
                $order->default_image = '/assets/images/default.png';
            }
        }
//
        $data['title'] = "";
        $data['products'] = $products;
        $data['orders'] = $orders;
        $data['latest_orders'] = $latest_orders;
        $data['product_statistics'] = $productstatistics;
        $data['order_statistics'] = $ordersstatistics;
        $data['followers_count'] = Follower::where("shop_id",Auth::guard('shop')->user()->id)->count();
        $data['shop'] = Shop::whereId(Auth::guard('shop')->user()->id)->with('followers')->first();
        return $data;
    }


}
