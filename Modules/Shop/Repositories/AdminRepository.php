<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/29/2020
 * Time: 3:19 PM
 */

namespace Modules\Shop\Repositories;


use Illuminate\Support\Facades\Auth;
use Modules\Shop\Entities\Shop;
use Modules\Shop\Repositories\Interfaces\AdminRepositoryInterface;
use Spatie\Permission\Models\Permission;

class AdminRepository implements AdminRepositoryInterface
{
    public function index()
    {
        $auth = Auth::guard('shop')->user();
        $parent_id = null;
        if ($auth->parent_id != null){
            $parent_id = $auth->parent_id;
        }else{
            $parent_id = $auth->id;
        }
        $data['title'] = "المشرفين";
        $data['admins'] = Shop::where('parent_id',$parent_id)->where('type','1')->where('id','!=',$auth->id)->get();
        return $data;
    }

    public function create()
    {
        $data['title'] = "إضافة مشرف";
        $data['permissions'] = Permission::get();
        $data['all_permissions'] = $this->permissions($data['permissions']);
        $data['models'] = array_keys($data['all_permissions']);
        return $data;
    }

    public function store($request)
    {
        $parent_id = null;
        $auth = Auth::guard('shop')->user();
        if ($auth->parent_id != null){
            $parent_id = $auth->parent_id;
        }else{
            $parent_id = $auth->id;
        }
        $admin = Shop::create(array_merge($request->except('permissions'),['parent_id' => $parent_id, 'type' => '1','status'=>1]));
        if ($request->has('permissions')){
            $admin->givePermissionTo($request->permissions);
        }
        $admin->name = $request->name;
        return $admin->save();

    }


    public function edit($request)
    {
        $data['title'] = "تعديل مشرف";
        $data['admin'] = shop::find($request->admin_id);
          $data['permissions'] = Permission::get();
        $data['all_permissions'] = $this->permissions($data['permissions']);
        return $data;
    }

    public function update($request)
    {
        $admin = Shop::where('id',$request->admin_id)->first();
        if($request->password){
            $admin->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
            ]);

        }
        else {
            $admin->update([
                'name' => $request->name,
                'email' => $request->email,
            ]);
        }
        if ($request->has('status') && $request->status != null){
            $admin->status = $request->status;
        }else{
            $admin->status = 0;
        }
        $permissions = Permission::pluck('name')->toArray();
        $admin->revokePermissionTo($permissions);
        if ($request->has('permissions')){
            $admin->givePermissionTo($request->permissions);
        }
        if ($admin->save()){
            if ($request->has('image')){
                if (!empty($admin->getOriginal('image'))){
                    unlinkFile($admin->getOriginal('image'), 'admins');
                }
                $admin->image = $request->image;
            }
            $admin->name = $request->name;
            $admin->save();
            return true;
        }
        return false;
    }

    public function permissions($permissions)
    {

        $all_permissions = [
'المشرفين' => [$permissions[61],$permissions[62],$permissions[63],$permissions[64]],
'الأقسام' => [$permissions[65],$permissions[66],$permissions[67],$permissions[68]],
'المنتجات' => [$permissions[69],$permissions[70],$permissions[71],$permissions[72],$permissions[83]],
'الطلبات' => [$permissions[73],$permissions[74],$permissions[75]],
'الإشعارات' => [$permissions[76],$permissions[77]],
'الإحصائيات' => [$permissions[78]],
//            'التقارير' => [$permissions[75],$permissions[76]],
        ];
        return $all_permissions;
    }
}
