<?php


namespace Modules\Shop\Repositories;


use App\Contact;
use Modules\Admin\Entities\AppExplanation;
use Modules\Admin\Entities\Term;
use Modules\Shop\Repositories\Interfaces\AppRepositoryInterface;

class AppRepository implements AppRepositoryInterface
{
    public function aboutUs(){

       return AppExplanation::whereIn('id',[10,11,12])->select('title','body')->get();
    }
    public function terms(){

       return Term::where('type',1)->select('term')->get();
    }
    public function contactUs($request){
        Contact::create([
            'name'=>$request->name,
            'type'=>1,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'message'=>$request->message,
        ]);
    }
    public function privacy(){

        return AppExplanation::whereId(13)->select('title','body')->get();
    }

}
