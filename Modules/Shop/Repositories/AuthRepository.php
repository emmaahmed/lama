<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/25/2020
 * Time: 1:12 PM
 */

namespace Modules\Shop\Repositories;


use App\Mail\AdminResetPassword;
use App\Mail\ShopResetPassword;
use App\ShopRequest;
use App\Verification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Modules\Shop\Entities\Shop;
use Modules\Shop\Repositories\Interfaces\AuthRepositoryInterface;

class AuthRepository implements AuthRepositoryInterface
{
    public function doLogin($request)
    {
        $remember_me = $request->remember_me == 1 ? true : false;
        //if (shop()->attempt(['email'=>$request->email,'password'=>$request->password,'status' => 1],$remember_me)){
        if (shop()->attempt(['email'=>$request->email,'password'=>$request->password],$remember_me)){
         $now=Carbon::now();
            if(Auth::guard('shop')->user()->type==0 &&$now->gt(Auth::guard('shop')->user()->expire_at)) {
                return false;
            }
            return true;
        }else{
            return false;
        }
    }
    public function doForgetPassword($request)
    {
        $token = Str::random(64);
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
        Mail::to($request->email)->send(new ShopResetPassword(['token' => $token]));
        return true;
    }
    public function resetPassword ($token)
    {
        $check_token = DB::table('password_resets')
            ->where('token',$token)
            ->where('created_at' , '>' , Carbon::now()->subHours(2))->first();
        if (!empty($check_token)){
            return $check_token;
        }else{
            false;
        }
    }
    public function doResetPassword ($request)
    {
        $check_token = DB::table('password_resets')
            ->where('token',$request->token)
            ->where('created_at' , '>' , Carbon::now()->subHours(2))->first();
        if (!empty($check_token)){
            Shop::where('email',$check_token->email)->update([
                'email' => $check_token->email,
                'password' => Hash::make($request->password)
            ]);
            DB::table('password_resets')->where('email',$request->email)->delete();
            shop()->attempt(['email'=>$check_token->email,'password'=>$request->password,'status' => 1],true);
            return true;
        }else{
            return false;
        }
    }
    public function Register($request)
    {
            $data = $request->all();
        $data['jwt']=Str::random('25');
//        dd($data);
        $email=ShopRequest::where('email',$request->email)->first();
        if($email){
            return 'email_exist';
        }
        $phone=ShopRequest::where('phone',$request->phone)->first();
        if($phone){
            return 'phone_exist';
        }
        $user = ShopRequest::create($data);

        return ShopRequest::whereId($user->id)->first();
    }
    public function sendCode($key)
    {
        $data=array();
        $code = verification_code();
        if (is_numeric($key)){     // $key => $phone

            //

            $message = "كود التفعيل الخاص بك هو".$code;
            $message = urlencode($message);
            $data['recipients']=[$key];
            $data['body']=$message;
            $data['sender']='Lama';
            $datastring=json_encode($data);

            //  $url = "https://www.alfa-cell.com/api/msgSend.php?apiKey=53f217ca2e287ba8eceff64d38dcfb9a&numbers=". $key ."&sender=Sedra-AD&msg=" . $message . "&applicationType=68&returnJson=1&lang=3" ;
            $url = "https://api.taqnyat.sa/v1/messages" ;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $datastring);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: Bearer 2e92f3ecd680c605b7ae51d7a59d2538'));

            $data = curl_exec($ch);
            //dd($data);
            curl_close($ch);
            $decodedData = json_decode($data);

            //

            $user = ShopRequest::wherePhone($key)->first();
            Verification::updateOrCreate(
                ['shop_id' => $user->id],
                ['shop_id' => $user->id, 'phone' => $key, 'code' => $code, 'expire_at' => Carbon::now()->addHour()]
            );
        }else{
            $user = ShopRequest::whereEmail($key)->first();// $key => $email
            Verification::updateOrCreate(
                ['shop_id' => $user->id],
                ['shop_id' => $user->id, 'email' => $key, 'code' => $code, 'expire_at' => Carbon::now()->addHour()]
            );
        }
        // Send SMS
//        $this->sms($user->phone,$code, true);
        // Send Email
        //  Mail::to($user->email)->send(new ActivationMail(['code' => $code]));
        $from='lama@emails.mobile-app-company.com';
        $jsonurl = 'http://emails.mobile-app-company.com/index.php?' . "email=" . $user->email  . "&from=" . $from."&verification_code=".$code;
        $json = file_get_contents($jsonurl);

    }
    public function verifyCode($request)
    {
        $verified = Verification::where(['code'=>$request->code,'phone'=>$request->phone])->where('shop_id','!=',null)->first();
        if($verified) {
            if ($verified->used == 1) {
                return callback_data(error(), 'code_used');
            }
            if ($verified->expire_date > Carbon::now()) {
                return callback_data(error(), 'time_exceeded');
            }
            //$user = User::where('phone',$verified->phone)->orWhere('email',$verified->email)->first();
             $user = ShopRequest::whereId($verified->shop_id)->first();
//        dd($user);
            if ($user) {
                // Update User
                $user->platform = $request->platform??'android';
                $user->verified = 1;
                $user->save();
                $shop=Shop::where('phone',$user->phone)->first();
                // Delete Verified
                $verified->delete();
                if($shop) {

                    return callback_data(success(), 'activated', $shop);
                }
                else{
                    return callback_data(success(), 'activated', $user);

                }
            }
            else {
                return callback_data(error(), 'invalid_data');
            }
        }else{
            return callback_data(error(), 'invalid_data');

        }
    }
    public function Login($user,$request)
    {
        $user->jwt = Str::random('25');
        if ($request->has('firebase_token')){
            $user->firebase_token = $request->firebase_token;
        }
        return $user->save();
    }
    public function changePassword($user,$request)
    {
        if($request->old_password){
            if(Hash::check($request->old_password,$user->password)){
                $user->password = $request->password;
                $user->save();

            }
        }
        $user->password = $request->password;
        $user->save();
    }
    public function createShop($user,$request){

//        $request['payment']= explode(",",$request->payment);
//      dd($request['payment']);
//        $request['delivery_types']= implode(",",$request->delivery_types);

        $shopRequest=ShopRequest::where('jwt',$user->jwt)->first();
        $shopRequest->update([
         'shop_name'=>$request->shop_name,
        'shop_owner_name'=>$request->shop_owner_name,
        'latitude'=>$request->latitude,
        'longitude'=>$request->longitude,
        'address'=>$request->address,
        'category_id'=>$request->category_id,
        'shop_info'=>1,
        'city_id'=>$request->city_id,
        'logo'=>$request->logo,
        'min_order'=>$request->min_order,
        'delivery_types'=>$request->delivery_types,
        'from'=>$request->from,
        'to'=>$request->to,
        'payment'=>$request->payment,
        'delivery_time'=>$request->delivery_time,
        'delivery_fees'=>$request->delivery_fees,
            ]);
        if($request->image){
            $shopRequest->image=$request->image;
        }
        if($request->commercial_register){
            $shopRequest->commercial_register=$request->commercial_register;
        }
        if($request->license){
            $shopRequest->license=$request->license;
        }
        if($request->cover){
//            dd($request->cover);
            $shopRequest->cover=$request->cover;
        }
        $shopRequest['status']=(int)$shopRequest->status;

//        $shop->translateOrNew('en')->name = $request->name_en;
        $shopRequest->short_description = $request->short_description;
//        $shop->translateOrNew('en')->short_description = $request->short_description_en;
        $shopRequest->long_description = $request->long_description;

        $shopRequest->save();
        return $shopRequest;
    }
    public function updateShop($user,$request){

        $shop=Shop::where('jwt',$user->jwt)->first();
        $shop->update([
            'shop_name'=>$request->shop_name,
            'shop_owner_name'=>$request->shop_owner_name,
            'latitude'=>$request->latitude,
            'longitude'=>$request->longitude,
            'address'=>$request->address,
            'category_id'=>$request->category_id,
            'shop_info'=>1,
            'city_id'=>$request->city_id,
//            'logo'=>$request->logo,
            'min_order'=>$request->min_order,
            'delivery_types'=>$request->delivery_types,
            'from'=>$request->from,
            'to'=>$request->to,
            'payment'=>$request->payment,
            'delivery_time'=>$request->delivery_time,
            'delivery_fees'=>$request->delivery_fees,

        ]);
        if($request->image){
            $shop->image=$request->image;
        }
        if($request->logo){
            $shop->logo=$request->logo;
        }
        if($request->cover){
            $shop->cover=$request->cover;
        }
        if($request->commercial_register){
            $shop->commercial_register=$request->commercial_register;
        }
        if($request->license){
            $shop->license=$request->license;
        }
        $shop->translateOrNew('ar')->name = $request->shop_name;
        $shop->translateOrNew('en')->name = $request->shop_name;
//        $shop->translateOrNew('en')->name = $request->name_en;
        $shop->translateOrNew('ar')->short_description = $request->short_description;
        $shop->translateOrNew('en')->short_description = $request->short_description;
//        $shop->translateOrNew('en')->short_description = $request->short_description_en;
        $shop->translateOrNew('ar')->long_description = $request->long_description;
        $shop->translateOrNew('en')->long_description = $request->long_description;
//        $shop->translateOrNew('en')->long_description = $request->long_description_en;
        if ($shop->save()) {
            $shop->assignRole('shop1');
        }
        $shop=Shop::select('*')->where('jwt',$user->jwt)->first();
        $shop['status']=(int)$shop->status;
        $shop['verified']=1;
        $shop['shop_info']=1;

        return $shop;

    }
    public function shopInfo($user,$request)
    {

        $shop= Shop::where('jwt', $user->jwt)->first();
        $shop['status']=(int)$shop->status;
        $shop['verified']=1;
        $shop['shop_info']=1;

        return $shop;

    }


    }
