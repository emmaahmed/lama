<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/22/2020
 * Time: 1:08 PM
 */

namespace Modules\Shop\Repositories;


use App\Repositories\Interfaces\HomeRepositoryInterface;
use Modules\Shop\Repositories\Interfaces\SubCategoryRepositoryInterface;

class SubCategoryRepository implements SubCategoryRepositoryInterface
{
    protected $homeRepository;

    public function __construct(HomeRepositoryInterface $homeRepository)
    {
        $this->homeRepository = $homeRepository;
    }

    public function index($request)
    {
        $data['title'] = "أقسام المتجر";
        $data['shop_id'] = $request->shop_id;
        $data['subCategories'] = $this->homeRepository->subCategories()->where('shop_id',$request->shop_id)->orderBy('id','DESC')->get();
        return $data;
    }

    public function store($request)
    {
        $subCategory = $this->homeRepository->subCategories()->updateOrCreate(
            ['id' => $request->subCategory_id],
            ['shop_id' => $request->shop_id]
        );
        $subCategory->translateOrNew('ar')->name = $request->name_ar;
        $subCategory->translateOrNew('en')->name = $request->name_ar;
//        $subCategory->translateOrNew('en')->name = $request->name_en;
        if ($subCategory->save()){
            return true;
        }
        return false;
    }
    public function changeStatus($request)
    {
        $shop = $this->homeRepository->subCategories()->where('id',$request->sub_category_id)->first();
        $shop->active = $request->active;
        if ($shop->save()){
            if ($shop->active == 1){
                return 'activated';
            }
            return 'deactivated';
        }else{
            return false;
        }
    }


}
