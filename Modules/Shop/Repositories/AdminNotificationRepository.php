<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/23/2020
 * Time: 3:30 PM
 */

namespace Modules\Shop\Repositories;


use App\Mail\ShopMail;
use App\Notification;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Modules\Admin\Entities\AdminNotification;
use Modules\Shop\Repositories\Interfaces\AdminNotificationRepositoryInterface;
use Modules\Shop\Entities\Shop;

class AdminNotificationRepository implements AdminNotificationRepositoryInterface
{
    public function index()
    {
        $data['title'] = 'رسائل النظام';
        $shop_id=(Auth::guard('shop')->user()->parent_id == NULL) ? Auth::guard('shop')->user()->id : Auth::guard('shop')->user()->parent_id;

        $data['notifications'] = AdminNotification::where('sender_type','!=',0)->where('shop_id',$shop_id)->latest()->get();
//        dd($data);
        return $data;
    }

    public function sendNotificationView()
    {
        $data['title'] = 'ارسال رسالة';
        $data['users'] = User::where('type',0)->get()->map(function ($item) {
            $item['type'] = 'user';
            return $item;
        });
        $data['shops'] = Shop::get()->map(function ($item) {
            $item['type'] = 'shop';
            return $item;
        });

        return $data;
    }

    public function sendNotification($request)
    {
        $user_type = explode('|', $request->person);
        $shop_id=(Auth::guard('shop')->user()->parent_id == NULL) ? Auth::guard('shop')->user()->id : Auth::guard('shop')->user()->parent_id;
//dd(Shop::whereId($shop_id)->first()->userFollowers);
        if ($user_type['0'] == 'All_users'){
           User::where('type',0)
                ->chunk(1000, function ($users) use ($request,$shop_id) {
                    foreach ($users as $user) {
                        $notification = new Notification();
                        $notification->user_id = $user->id;
                        $notification->sender_type = 1; //shop
                        $notification->shop_id = $shop_id;//shop
                        $notification->save();
                        $notification->translateOrNew('en')->title = 'UserDelegateMessage From Admin(shop)';
                        $notification->translateOrNew('ar')->title = 'رسالة من مدير النظام(متجر)';
                        $notification->translateOrNew('en')->body = $request->message;
                        $notification->translateOrNew('ar')->body = $request->message;
                        $notification->save();
                        $shop=Shop::whereId($shop_id)->select('id','expire_at','logo')->first();
                        unset($shop->images);
                        send_to_user([$user->firebase_token],$request->message ,'10',"","",$shop,$user->platform);
                        AdminNotification::create([
                            'message' => $request->message,
                            'type' => 0,  // All Users
                            'sender_type' => 1,  // shop
                            'shop_id' => $shop_id // shop
                        ]);




                    }});


        }
        elseif ($user_type['0'] == 'All_shops'){
            $shops = Shop::get();
            foreach ($shops as $shop){
                Mail::to($shop->email)->send(new ShopMail(['message' => $request->message]));
            }
            AdminNotification::create([
                'message' => $request->message,
                'type' => 1 ,  // All Users
                'sender_type' => 1,  // shop
                'shop_id' => $shop_id  // shop
            ]);
        }else{
            if ($user_type['1'] == 'user'){
                $user = User::where('id',$user_type['0'])->first();
                $shop=Shop::whereId($shop_id)->first();
                unset($shop->images);
                send_to_user([$user->firebase_token],$request->message ,'10',"",'',$shop,$user->platform);
                $notification = new Notification();
                $notification->user_id = $user->id;
                $notification->sender_type = 1; //shop
                $notification->shop_id =$shop_id; //shop

                $notification->save();
                $notification->translateOrNew('en')->title = 'UserDelegateMessage From Admin(shop)';
                $notification->translateOrNew('ar')->title = 'رسالة من مدير النظام(متجر)';
                $notification->translateOrNew('en')->body = $request->message;
                $notification->translateOrNew('ar')->body = $request->message;
                $notification->save();
                AdminNotification::create([
                    'user_id' => $user->id,
                    'message' => $request->message,
                    'type' => 2,  // User
                    'sender_type' => 1,  // shop
                    'shop_id' => $shop_id  // shop
                ]);
            }
            elseif ($user_type['1'] == 'shop'){
                $shop = Shop::where('id',$user_type['0'])->first();
                Mail::to($shop->email)->send(new ShopMail(['message' => $request->message]));
                AdminNotification::create([
//                    'shop_id' => $shop->id,
                    'message' => $request->message,
                    'type' => 3,  // Shop
                    'sender_type' => 1,  // shop
                    'shop_id' => $shop_id // shop
                ]);
            }
        }
        return true;
    }

}
