<?php


namespace Modules\Shop\Repositories;


use App\Http\Requests\Request;
use App\Notification;
use App\Repositories\Interfaces\HomeRepositoryInterface;
use App\User;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Entities\AdminNotification;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\UserShopMessage;
use Modules\Setting\Entities\Setting;
use Modules\Shop\Entities\Follower;
use Modules\Shop\Entities\Comment;
use Modules\Shop\Entities\Shop;
use Modules\Shop\Entities\ShopImage;
use Modules\Shop\Repositories\Interfaces\ShopRepositoryInterface;
use Carbon\Carbon;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class ShopRepository implements ShopRepositoryInterface
{
    protected $homeRepository;

    public function __construct(HomeRepositoryInterface $homeRepository)
    {
        $this->homeRepository = $homeRepository;
    }

    public function shopsByCategory($request)
    {
        $shops= Shop::where('category_id', $request->category_id)->where('status', 1)
            ->where('expire_at', '>', Carbon::now());
        if ($request->city_id) {
            $shops= $shops->where('city_id', $request->city_id);
        }
        if ($request->sort_by_price && $request->sort_by_price=='high') {
            $shops= $shops->orderBy('min_order','desc');
        }
        if ($request->sort_by_price && $request->sort_by_price=='low') {
            $shops= $shops->orderBy('min_order','asc');
        }
        if ($request->sort_by_date && $request->sort_by =='latest') {
            $shops= $shops->latest();
        }
        if ($request->sort_by_date && $request->sort_by =='oldest') {
            $shops= $shops->oldest();
        }
        return $shops;


    }

    public function shopsByCategoryByLatLng($request)
    {

        $shops = inRange($request->lat, $request->lng, 1000000, 0);
        $shops= $shops->where('category_id', $request->category_id)->where('status', 1)
            ->where('expire_at', '>', Carbon::now());

        if ($request->city_id) {
            $shops= $shops->where('city_id', $request->city_id);
        }
        if ($request->sort_by_price && $request->sort_by_price=='high') {

            $shops= $shops->orderBy('min_order','DESC');
        }
        if ($request->sort_by_price && $request->sort_by_price=='low') {
            $shops= $shops->orderBy('min_order','ASC');
        }
        if ($request->sort_by_date && $request->sort_by =='latest') {
            $shops= $shops->latest();
        }
        if ($request->sort_by_date && $request->sort_by =='oldest') {
            $shops= $shops->oldest();
        }
        return $shops;


    }

//    Admin

    public function getShops()
    {
        $shops = $this->homeRepository->shops()->where('expire_at', '>', Carbon::now())->get();
        return $shops;
    }

    public function getNotActiveShops()
    {
        $shops = $this->homeRepository->shops()->where('expire_at', '<=', Carbon::now()->toDateString())->get();
        return $shops;
    }

    public function create()
    {
        $data['categories'] = $this->homeRepository->categories()->get();
        $data['cities'] = $this->homeRepository->cities()->get();
        $data['title'] = 'إضافة متجر';
        return $data;
    }

    public function store($request)
    {

        $shop = $this->homeRepository->shops()->create($request->except('images'));

        if ($request->has('images')) {
            foreach ($request->images as $image) {
                $shop->images()->create([
                    'image' => $image
                ]);
            }
        }
        $shop->translateOrNew('ar')->name = $request->name_ar;
        $shop->translateOrNew('en')->name = $request->name_ar;
//        $shop->translateOrNew('en')->name = $request->name_en;
        $shop->translateOrNew('ar')->short_description = $request->short_description_ar;
        $shop->translateOrNew('en')->short_description = $request->short_description_ar;
//        $shop->translateOrNew('en')->short_description = $request->short_description_en;
        $shop->translateOrNew('ar')->long_description = $request->long_description_ar;
        $shop->translateOrNew('en')->long_description = $request->long_description_ar;
//        $shop->translateOrNew('en')->long_description = $request->long_description_en;
        if ($shop->save()) {
            $shop->assignRole('shop1');
            return true;
        }
        return false;
    }

    public function edit($request)
    {
        $data['title'] = "تعديل المتجر";
        $data['shop'] = $this->homeRepository->shops()->where('id', $request->shop_id)->first();
        $data['categories'] = $this->homeRepository->categories()->get();
        $data['cities'] = $this->homeRepository->cities()->get();
        return $data;
    }

    public function update($request)
    {
        $shop = $this->homeRepository->shops()->where('id', $request->shop_id)->first();
        if ($request->password) {
            $shop->update($request->except('images', 'from', 'to'));
        } else {
            $shop->update($request->except('images', 'password', 'from', 'to'));

        }

        $shop->assignRole('shop1');
        if ($request->has('images')) {
            foreach ($request->images as $image) {
                $shop->images()->create([
                    'image' => $image
                ]);
            }
        }
        if ($request->has('from')) {
            $shop->from = $request->from;
            $shop->save();
        }
        if ($request->has('to')) {

            $shop->to = $request->to;
            $shop->save();

        }
//        dd($shop);
        $shop->translateOrNew('ar')->name = $request->name_ar ? $request->name_ar : $shop->name_ar;
        $shop->translateOrNew('en')->name = $request->name_ar ? $request->name_ar : $shop->name_ar;
//        $shop->translateOrNew('en')->name = $request->name_en;
        $shop->translateOrNew('ar')->short_description = $request->short_description_ar;
        $shop->translateOrNew('en')->short_description = $request->short_description_ar;
//        $shop->translateOrNew('en')->short_description = $request->short_description_en;
        $shop->translateOrNew('ar')->long_description = $request->long_description_ar;
        $shop->translateOrNew('en')->long_description = $request->long_description_ar;
//        $shop->translateOrNew('en')->long_description = $request->long_description_en;
        if ($shop->save()) {
            return true;
        }
        return false;
    }

    public function changeStatus($request)
    {
        $shop = $this->homeRepository->shops()->where('id', $request->shop_id)->first();
        $shop->status = $request->status;
        if ($shop->save()) {
            if ($shop->status == 1) {
                return 'activated';
            }
            return 'deactivated';
        } else {
            return false;
        }
    }

    public function shopRemoveImages($request)
    {
        $shop_image = ShopImage::where('id', $request->image_id)->where('shop_id', $request->shop_id)->first();
        if ($shop_image) {
            unlinkFile($shop_image->getOriginal('image'), 'shops');
            if ($shop_image->delete()) {
                return true;
            }
            return false;
        }
        return false;

    }

    public function showProducts($request)
    {
        $data['title'] = 'تفاصيل المتجر';
        $data['shop'] = $this->homeRepository->shops()->where('id', $request->shop_id)->first();
        return $data;
    }

    public function search($request)
    {
        $data['title'] = 'نتيجة البحث في كل المنتجات';
        //   dd($request->all());
//        dd($request->shop_id);

        $data['products'] = $this->homeRepository
            ->products()
            ->where('status', $request->status)
            ->where(function ($query)use($request){
                $query->whereTranslationLike('name', "%" . $request->keySearch . "%")
                    ->orWhereTranslationLike('short_description', "%" . $request->keySearch . "%");

            })
            ->where('shop_id', $request->shop_id)

            ->paginate(10);
        return $data;
    }

    public function changeShopStatus($request)
    {

        $shop = $this->homeRepository->shops()->where('id', $request->shop_id)->first();
        $shop->online = $request->status;
        if ($shop->save()) {
            if ($shop->online == 1) {
                return 'shop_online';
            }
            return 'shop_offline';
        } else {
            return false;
        }
    }

    public function followAndUnfollow($id, $input, $lang)
    {
        $followCheck = Follower::where("shop_id", $input->shop_id)
            ->where("user_id", $id)->first();
        if ($followCheck) {
            Follower::where("shop_id", $input->shop_id)
                ->where("user_id", $id)->forcedelete();
            return false;
        } else {
            $add = new Follower();
            $add->shop_id = $input->shop_id;
            $add->user_id = $id;
            $add->save();
            return true;

//            $follower=User::where("id",$id)->first()->name;
//            if($lang == "en"){
//                $title="E3LN";
//                $message =  $follower ." has followed you";
//            }else{
//                $title="اعلن";
//                $message = $follower ."قام بمتابعتك";
//            }
//            $user=User::where("id",$input->shop_id)->first();
//            Notification::send("$user->token", $title , $message , 0 );
        }
    }

    public function followingList($id, $input, $lang)
    {
        global $shops;
        $shops = [];
        $followCheck = Follower::where("user_id", $id)->pluck('shop_id');
        if (sizeof($followCheck) > 0) {
            $shops = Shop::whereIn("id", $followCheck)
                ->where('expire_at', '>', Carbon::now())->where('status', '1')->where('online', 1)
                ->select('shops.id', 'shops.city_id', 'shops.rate', 'shops.logo',
                    'shops.online', 'shops.from', 'shops.to', 'shops.expire_at')
                ->get();
            foreach ($shops as $d) {
                $follow = Follower::where('shop_id', $d->id)->where('user_id', $id)->first();
                if ($follow)
                    $d->is_follow = true;
                else
                    $d->is_follow = false;
            }
            return $shops;
        } else {
            return $shops;
        }
    }

    public function commentsList($id, $input, $lang, $shop_id)
    {
        $comments = User::join('comments', 'comments.user_id', 'users.id')
            ->where('comments.shop_id', $shop_id)
            ->select('comments.id', 'comments.comment', 'comments.created_at',
                'users.id as user_id', 'users.name as user_name',
                'users.image as user_image')
            ->get();
        foreach ($comments as $comment) {
            $comment->rate = Order::where('user_id', $id)->avg('user_rate');
        }

        return $comments;
    }

    public function addComment($id, $input, $lang)
    {
        $add = new Comment();
        $add->shop_id = $input->shop_id;
        $add->user_id = $id;
        $add->comment = $input->comment;
        $add->save();
        return true;

//            $follower=User::where("id",$id)->first()->name;
//            if($lang == "en"){
//                $title="E3LN";
//                $message =  $follower ." has followed you";
//            }else{
//                $title="اعلن";
//                $message = $follower ."قام بمتابعتك";
//            }
//            $user=User::where("id",$input->shop_id)->first();
//            Notification::send("$user->token", $title , $message , 0 );

    }

    public function home($shop)
    {
        $data = [];

        if($shop->notification_flag==1) {
            $orders = Order::where(['shop_id' => $shop->id, 'status' => 0])->orderBy('created_at', 'desc')->take(3)->get();
            foreach ($orders as $order) {
                if ($order->delivery_type == 0) {
                    $fees = $order->shop->delivery_fees;
                } elseif ($order->delivery_type == 1) {
                    $fees = getFastShippingFees();
                } else {
                    $fees = 0;
                }


                $order_details = [];
                $order_details['id'] = $order->id;
                $order_details['order_number'] = $order->order_number;
                $order_details['order_status'] = $order->status;
                $order_details['total_cost'] = strval($order->total_cost + ($fees));
                $order_details['delivery_type'] = $order->delivery_type;
                // $order_details['delivery_time'] = $order->shop ? $order->shop->delivery_time : "";
                $order_details['delivery_time'] = $order->delivery_time ? $order->delivery_time : $order->shop->delivery_time;
                $order_details['shipping_fees'] = $fees;
                $order_details['payment_type'] = $order->payment;
                $order_details['user_id'] = isset($order->user_id) ? $order->user_id : 0;
                $order_details['user_name'] = isset($order->user) ? $order->user->name : "";
                $order_details['user_phone'] = isset($order->user) ? $order->user->phone : "";
                $order_details['user_image'] = isset($order->user) ? $order->user->image : "";

                $order_details['delegate_id'] = isset($order->delegate_id) ? $order->delegate_id : 0;
                $order_details['delegate_phone'] = isset($order->delegate) ? $order->delegate->phone : "";
                $order_details['latitude'] = isset($order->address) ? $order->address->latitude : "";
                $order_details['longitude'] = isset($order->address) ? $order->address->longitude : "";
                $order_details['address'] = isset($order->address) ? $order->address->address : "";
                $order_details['notes'] = isset($order->notes) ? $order->notes : "";
                $order_details['created_at'] = $order->created_at;
                $order_details['is_rated_shop'] = $order->shop_rate ? 1 : 0;
                $order_details['is_rated_user'] = $order->user_rate ? 1 : 0;
                $order_details['is_rated_delegate'] = $order->delegate_rate ? 1 : 0;

                $order_details['items'] = [];
                if (sizeof($order->orderItems) > 0) {
                    foreach ($order->orderItems as $key => $order_item) {
                        $order_details['items'][$key]['item_id'] = $order_item->product_id;
                        $order_details['items'][$key]['item_quantity'] = $order_item->quantity;
                        $order_details['items'][$key]['item_name'] = $order_item->product->name;
                        $order_details['items'][$key]['item_image'] = $order_item->product->image;
                        $order_details['items'][$key]['item_cost'] = $order_item->cost;
                        $order_details['items'][$key]['description'] = $order_item->description;
//                        $shop = Shop::whereId($order_item->product->shop_id)->first();
                        $order_details['shop_lat'] = $shop->latitude;
                        $order_details['shop_lng'] = $shop->longitude;
                        $order_details['items'][$key]['shop_id'] = $shop->id;
                        $order_details['items'][$key]['variations'] = [];
                        if (sizeof($order_item->ordItemVars) > 0) {
                            foreach ($order_item->ordItemVars as $key2 => $variation) {
                                $order_details['items'][$key]['variations'][$variation->variation_name]['id'] = $variation->id;
                                $order_details['items'][$key]['variations'][$variation->variation_name]['name'] = $variation->variation_name;
                                $order_details['items'][$key]['variations'][$variation->variation_name]['options'][]['name'] = $variation->option_name;
                            }
                            $order_details['items'][$key]['variations'][$variation->variation_name]['options'] = array_values($order_details['items'][$key]['variations'][$variation->variation_name]['options']);
                        }
                        $order_details['items'][$key]['variations'] = array_values($order_details['items'][$key]['variations']);
                    }
                }
                $order = $order_details;

            }
            $data['orders'] = $orders;
            $data['notification_flag'] = $shop->notification_flag;
            $data['notification_count'] = Notification::whereIn('sender_type', [0, 2])->where(['type' => -1, 'shop_id' => $shop->id])->where('read_at', null)->latest()->count();
            $data['messages_count'] = UserShopMessage::where('shop_id', $shop->id)->where('read_at', null)->latest()->count();
        }else{
            $orders = Order::where(['shop_id' => $shop->id, 'status' => 0])->orderBy('created_at', 'desc')->where('delivery_time','>',Carbon::now()->toDateTimeString())->take(3)->get();
            foreach ($orders as $order) {
                if ($order->delivery_type == 0) {
                    $fees = $order->shop->delivery_fees;
                } elseif ($order->delivery_type == 1) {
                    $fees = getFastShippingFees();
                } else {
                    $fees = 0;
                }


                $order_details = [];
                $order_details['id'] = $order->id;
                $order_details['order_number'] = $order->order_number;
                $order_details['order_status'] = $order->status;
                $order_details['total_cost'] = strval($order->total_cost + ($fees));
                $order_details['delivery_type'] = $order->delivery_type;
                // $order_details['delivery_time'] = $order->shop ? $order->shop->delivery_time : "";
                $order_details['delivery_time'] = $order->delivery_time ? $order->delivery_time : $order->shop->delivery_time;
                $order_details['shipping_fees'] = $fees;
                $order_details['payment_type'] = $order->payment;
                $order_details['user_id'] = isset($order->user_id) ? $order->user_id : 0;
                $order_details['user_name'] = isset($order->user) ? $order->user->name : "";
                $order_details['user_phone'] = isset($order->user) ? $order->user->phone : "";
                $order_details['user_image'] = isset($order->user) ? $order->user->image : "";

                $order_details['delegate_id'] = isset($order->delegate_id) ? $order->delegate_id : 0;
                $order_details['delegate_phone'] = isset($order->delegate) ? $order->delegate->phone : "";
                $order_details['latitude'] = isset($order->address) ? $order->address->latitude : "";
                $order_details['longitude'] = isset($order->address) ? $order->address->longitude : "";
                $order_details['address'] = isset($order->address) ? $order->address->address : "";
                $order_details['notes'] = isset($order->notes) ? $order->notes : "";
                $order_details['created_at'] = $order->created_at;
                $order_details['is_rated_shop'] = $order->shop_rate ? 1 : 0;
                $order_details['is_rated_user'] = $order->user_rate ? 1 : 0;
                $order_details['is_rated_delegate'] = $order->delegate_rate ? 1 : 0;

                $order_details['items'] = [];
                if (sizeof($order->orderItems) > 0) {
                    foreach ($order->orderItems as $key => $order_item) {
                        $order_details['items'][$key]['item_id'] = $order_item->product_id;
                        $order_details['items'][$key]['item_quantity'] = $order_item->quantity;
                        $order_details['items'][$key]['item_name'] = $order_item->product->name;
                        $order_details['items'][$key]['item_image'] = $order_item->product->image;
                        $order_details['items'][$key]['item_cost'] = $order_item->cost;
                        $order_details['items'][$key]['description'] = $order_item->description;
//                        $shop = Shop::whereId($order_item->product->shop_id)->first();
                        $order_details['shop_lat'] = $shop->latitude;
                        $order_details['shop_lng'] = $shop->longitude;
                        $order_details['items'][$key]['shop_id'] = $shop->id;
                        $order_details['items'][$key]['variations'] = [];
                        if (sizeof($order_item->ordItemVars) > 0) {
                            foreach ($order_item->ordItemVars as $key2 => $variation) {
                                $order_details['items'][$key]['variations'][$variation->variation_name]['id'] = $variation->id;
                                $order_details['items'][$key]['variations'][$variation->variation_name]['name'] = $variation->variation_name;
                                $order_details['items'][$key]['variations'][$variation->variation_name]['options'][]['name'] = $variation->option_name;
                            }
                            $order_details['items'][$key]['variations'][$variation->variation_name]['options'] = array_values($order_details['items'][$key]['variations'][$variation->variation_name]['options']);
                        }
                        $order_details['items'][$key]['variations'] = array_values($order_details['items'][$key]['variations']);
                    }
                }
                $order = $order_details;

            }
            $data['orders'] = $orders;
            $data['notification_flag'] = $shop->notification_flag;
            $data['notification_start_date'] = $shop->notification_start_date;
            $data['notification_end_date'] = Carbon::parse($shop->notification_start_date)->addHours($shop->notification_time)->toDateTimeString();
            $data['notification_count_down'] = Carbon::now()->diffInMinutes($data['notification_end_date'] );
            $data['notification_count'] = Notification::whereIn('sender_type', [0, 2])->where(['type' => -1, 'shop_id' => $shop->id])->where('read_at', null)->latest()->count();
            $data['messages_count'] = UserShopMessage::where('shop_id', $shop->id)->where('read_at', null)->latest()->count();

        }
        return $data;
    }

    public function myOrders($shop, $type)
    {
        if ($type == 0) {
            $orders= Order::where('shop_id' , $shop->id)->whereIn('status' ,[0])->orderBy('created_at', 'desc')->paginate(20);
//          dd($orders);
            foreach ($orders as $order) {
                if ($order->delivery_type == 0) {
                    $fees = $order->shop->delivery_fees;
                } elseif ($order->delivery_type == 1) {
                    $fees = getFastShippingFees();
                } else {
                    $fees = 0;
                }


                $order_details = [];
                $order_details['id'] = $order->id;
                $order_details['order_number'] = $order->order_number;
                $order_details['order_status'] = $order->status;
                $order_details['total_cost'] = strval($order->total_cost + ($fees));
                $order_details['delivery_type'] = $order->delivery_type;
                // $order_details['delivery_time'] = $order->shop ? $order->shop->delivery_time : "";
                $order_details['delivery_time'] = $order->delivery_time ? $order->delivery_time : $order->shop->delivery_time;
                $order_details['shipping_fees'] = $fees;
                $order_details['payment_type'] = $order->payment;
                $order_details['user_id'] = isset($order->user_id) ? $order->user_id : 0;
                $order_details['user_name'] = isset($order->user) ? $order->user->name : "";
                $order_details['user_phone'] = isset($order->user) ? $order->user->phone : "";
                $order_details['user_image'] = isset($order->user) ? $order->user->image : "";

                $order_details['delegate_id'] = isset($order->delegate_id) ? $order->delegate_id : 0;
                $order_details['delegate_phone'] = isset($order->delegate) ? $order->delegate->phone : "";
                $order_details['latitude'] = isset($order->address) ? $order->address->latitude : "";
                $order_details['longitude'] = isset($order->address) ? $order->address->longitude : "";
                $order_details['address'] = isset($order->address) ? $order->address->address : "";
                $order_details['notes'] = isset($order->notes) ? $order->notes : "";
                $order_details['created_at'] = $order->created_at;
                $order_details['is_rated_shop'] = $order->shop_rate ? 1 : 0;
                $order_details['is_rated_user'] = $order->user_rate ? 1 : 0;
                $order_details['is_rated_delegate'] = $order->delegate_rate ? 1 : 0;

                $order_details['items'] = [];
                if (sizeof($order->orderItems) > 0) {
                    foreach ($order->orderItems as $key => $order_item) {
                        $order_details['items'][$key]['item_id'] = $order_item->product_id;
                        $order_details['items'][$key]['item_quantity'] = $order_item->quantity;
                        $order_details['items'][$key]['item_name'] = $order_item->product->name;
                        $order_details['items'][$key]['item_image'] = $order_item->product->image;
                        $order_details['items'][$key]['item_cost'] = $order_item->cost;
                        $order_details['items'][$key]['description'] = $order_item->description;
                        $shop = Shop::whereId($order_item->product->shop_id)->first();
                        $order_details['shop_lat'] = $shop->latitude;
                        $order_details['shop_lng'] = $shop->longitude;
                        $order_details['items'][$key]['shop_id'] = $shop->id;
                        $order_details['items'][$key]['variations'] = [];
                        if (sizeof($order_item->ordItemVars) > 0) {
                            foreach ($order_item->ordItemVars as $key2 => $variation) {
                                $order_details['items'][$key]['variations'][$variation->variation_name]['id'] = $variation->id;
                                $order_details['items'][$key]['variations'][$variation->variation_name]['name'] = $variation->variation_name;
                                $order_details['items'][$key]['variations'][$variation->variation_name]['options'][]['name'] = $variation->option_name;
                            }
                            $order_details['items'][$key]['variations'][$variation->variation_name]['options'] = array_values($order_details['items'][$key]['variations'][$variation->variation_name]['options']);
                        }
                        $order_details['items'][$key]['variations'] = array_values($order_details['items'][$key]['variations']);
                    }
                }
                $order=$order_details;

            }
            return $orders;

        }
        if ($type == 1) {
            $orders= Order::where('shop_id', $shop->id)->whereIn('status', [1, 2, 5])->orderBy('created_at', 'desc')->paginate(20);
            foreach ($orders as $order) {
                if ($order->delivery_type == 0) {
                    $fees = $order->shop->delivery_fees;
                } elseif ($order->delivery_type == 1) {
                    $fees = getFastShippingFees();
                } else {
                    $fees = 0;
                }


                $order_details = [];
                $order_details['id'] = $order->id;
                $order_details['order_number'] = $order->order_number;
                $order_details['order_status'] = $order->status;
                $order_details['total_cost'] = strval($order->total_cost + ($fees));
                $order_details['delivery_type'] = $order->delivery_type;
                // $order_details['delivery_time'] = $order->shop ? $order->shop->delivery_time : "";
                $order_details['delivery_time'] = $order->delivery_time ? $order->delivery_time : $order->shop->delivery_time;
                $order_details['shipping_fees'] = $fees;
                $order_details['payment_type'] = $order->payment;
                $order_details['user_id'] = isset($order->user_id) ? $order->user_id : 0;
                $order_details['user_name'] = isset($order->user) ? $order->user->name : "";
                $order_details['user_phone'] = isset($order->user) ? $order->user->phone : "";
                $order_details['user_image'] = isset($order->user) ? $order->user->image : "";

                $order_details['delegate_id'] = isset($order->delegate_id) ? $order->delegate_id : 0;
                $order_details['delegate_phone'] = isset($order->delegate) ? $order->delegate->phone : "";
                $order_details['latitude'] = isset($order->address) ? $order->address->latitude : "";
                $order_details['longitude'] = isset($order->address) ? $order->address->longitude : "";
                $order_details['address'] = isset($order->address) ? $order->address->address : "";
                $order_details['notes'] = isset($order->notes) ? $order->notes : "";
                $order_details['created_at'] = $order->created_at;
                $order_details['is_rated_shop'] = $order->shop_rate ? 1 : 0;
                $order_details['is_rated_user'] = $order->user_rate ? 1 : 0;
                $order_details['is_rated_delegate'] = $order->delegate_rate ? 1 : 0;

                $order_details['items'] = [];
                if (sizeof($order->orderItems) > 0) {
                    foreach ($order->orderItems as $key => $order_item) {
                        $order_details['items'][$key]['item_id'] = $order_item->product_id;
                        $order_details['items'][$key]['item_quantity'] = $order_item->quantity;
                        $order_details['items'][$key]['item_name'] = $order_item->product->name;
                        $order_details['items'][$key]['item_image'] = $order_item->product->image;
                        $order_details['items'][$key]['item_cost'] = $order_item->cost;
                        $order_details['items'][$key]['description'] = $order_item->description;
                        $shop = Shop::whereId($order_item->product->shop_id)->first();
                        $order_details['shop_lat'] = $shop->latitude;
                        $order_details['shop_lng'] = $shop->longitude;
                        $order_details['items'][$key]['shop_id'] = $shop->id;
                        $order_details['items'][$key]['variations'] = [];
                        if (sizeof($order_item->ordItemVars) > 0) {
                            foreach ($order_item->ordItemVars as $key2 => $variation) {
                                $order_details['items'][$key]['variations'][$variation->variation_name]['id'] = $variation->id;
                                $order_details['items'][$key]['variations'][$variation->variation_name]['name'] = $variation->variation_name;
                                $order_details['items'][$key]['variations'][$variation->variation_name]['options'][]['name'] = $variation->option_name;
                            }
                            $order_details['items'][$key]['variations'][$variation->variation_name]['options'] = array_values($order_details['items'][$key]['variations'][$variation->variation_name]['options']);
                        }
                        $order_details['items'][$key]['variations'] = array_values($order_details['items'][$key]['variations']);
                    }
                }
                $order=$order_details;

            }
            return $orders;

        }
        if ($type == 2) {
            $orders= Order::where(['shop_id' => $shop->id, 'status' => 3])->orderBy('created_at', 'desc')->paginate(20);
            foreach ($orders as $order) {
                if ($order->delivery_type == 0) {
                    $fees = $order->shop->delivery_fees;
                } elseif ($order->delivery_type == 1) {
                    $fees = getFastShippingFees();
                } else {
                    $fees = 0;
                }


                $order_details = [];
                $order_details['id'] = $order->id;
                $order_details['order_number'] = $order->order_number;
                $order_details['order_status'] = $order->status;
                $order_details['total_cost'] = strval($order->total_cost + ($fees));
                $order_details['delivery_type'] = $order->delivery_type;
                // $order_details['delivery_time'] = $order->shop ? $order->shop->delivery_time : "";
                $order_details['delivery_time'] = $order->delivery_time ? $order->delivery_time : $order->shop->delivery_time;
                $order_details['shipping_fees'] = $fees;
                $order_details['payment_type'] = $order->payment;
                $order_details['user_id'] = isset($order->user_id) ? $order->user_id : 0;
                $order_details['user_name'] = isset($order->user) ? $order->user->name : "";
                $order_details['user_phone'] = isset($order->user) ? $order->user->phone : "";
                $order_details['user_image'] = isset($order->user) ? $order->user->image : "";

                $order_details['delegate_id'] = isset($order->delegate_id) ? $order->delegate_id : 0;
                $order_details['delegate_phone'] = isset($order->delegate) ? $order->delegate->phone : "";
                $order_details['latitude'] = isset($order->address) ? $order->address->latitude : "";
                $order_details['longitude'] = isset($order->address) ? $order->address->longitude : "";
                $order_details['address'] = isset($order->address) ? $order->address->address : "";
                $order_details['notes'] = isset($order->notes) ? $order->notes : "";
                $order_details['created_at'] = $order->created_at;
                $order_details['is_rated_shop'] = $order->shop_rate ? 1 : 0;
                $order_details['is_rated_user'] = $order->user_rate ? 1 : 0;
                $order_details['is_rated_delegate'] = $order->delegate_rate ? 1 : 0;

                $order_details['items'] = [];
                if (sizeof($order->orderItems) > 0) {
                    foreach ($order->orderItems as $key => $order_item) {
                        $order_details['items'][$key]['item_id'] = $order_item->product_id;
                        $order_details['items'][$key]['item_quantity'] = $order_item->quantity;
                        $order_details['items'][$key]['item_name'] = $order_item->product->name;
                        $order_details['items'][$key]['item_image'] = $order_item->product->image;
                        $order_details['items'][$key]['item_cost'] = $order_item->cost;
                        $order_details['items'][$key]['description'] = $order_item->description;
                        $shop = Shop::whereId($order_item->product->shop_id)->first();
                        $order_details['shop_lat'] = $shop->latitude;
                        $order_details['shop_lng'] = $shop->longitude;
                        $order_details['items'][$key]['shop_id'] = $shop->id;
                        $order_details['items'][$key]['variations'] = [];
                        if (sizeof($order_item->ordItemVars) > 0) {
                            foreach ($order_item->ordItemVars as $key2 => $variation) {
                                $order_details['items'][$key]['variations'][$variation->variation_name]['id'] = $variation->id;
                                $order_details['items'][$key]['variations'][$variation->variation_name]['name'] = $variation->variation_name;
                                $order_details['items'][$key]['variations'][$variation->variation_name]['options'][]['name'] = $variation->option_name;
                            }
                            $order_details['items'][$key]['variations'][$variation->variation_name]['options'] = array_values($order_details['items'][$key]['variations'][$variation->variation_name]['options']);
                        }
                        $order_details['items'][$key]['variations'] = array_values($order_details['items'][$key]['variations']);
                    }
                }
                $order=$order_details;

            }
            return $orders;

        }
        if ($type == 3) {
            $orders= Order::where('shop_id', $shop->id)->whereIn('status', [-1, 4])->orderBy('created_at', 'desc')->paginate(20);
            foreach ($orders as $order) {
                if ($order->delivery_type == 0) {
                    $fees = $order->shop->delivery_fees;
                } elseif ($order->delivery_type == 1) {
                    $fees = getFastShippingFees();
                } else {
                    $fees = 0;
                }


                $order_details = [];
                $order_details['id'] = $order->id;
                $order_details['order_number'] = $order->order_number;
                $order_details['order_status'] = $order->status;
                $order_details['total_cost'] = strval($order->total_cost + ($fees));
                $order_details['delivery_type'] = $order->delivery_type;
                // $order_details['delivery_time'] = $order->shop ? $order->shop->delivery_time : "";
                $order_details['delivery_time'] = $order->delivery_time ? $order->delivery_time : $order->shop->delivery_time;
                $order_details['shipping_fees'] = $fees;
                $order_details['payment_type'] = $order->payment;
                $order_details['user_id'] = isset($order->user_id) ? $order->user_id : 0;
                $order_details['user_name'] = isset($order->user) ? $order->user->name : "";
                $order_details['user_phone'] = isset($order->user) ? $order->user->phone : "";
                $order_details['user_image'] = isset($order->user) ? $order->user->image : "";

                $order_details['delegate_id'] = isset($order->delegate_id) ? $order->delegate_id : 0;
                $order_details['delegate_phone'] = isset($order->delegate) ? $order->delegate->phone : "";
                $order_details['latitude'] = isset($order->address) ? $order->address->latitude : "";
                $order_details['longitude'] = isset($order->address) ? $order->address->longitude : "";
                $order_details['address'] = isset($order->address) ? $order->address->address : "";
                $order_details['notes'] = isset($order->notes) ? $order->notes : "";
                $order_details['created_at'] = $order->created_at;
                $order_details['is_rated_shop'] = $order->shop_rate ? 1 : 0;
                $order_details['is_rated_user'] = $order->user_rate ? 1 : 0;
                $order_details['is_rated_delegate'] = $order->delegate_rate ? 1 : 0;

                $order_details['items'] = [];
                if (sizeof($order->orderItems) > 0) {
                    foreach ($order->orderItems as $key => $order_item) {
                        $order_details['items'][$key]['item_id'] = $order_item->product_id;
                        $order_details['items'][$key]['item_quantity'] = $order_item->quantity;
                        $order_details['items'][$key]['item_name'] = $order_item->product->name;
                        $order_details['items'][$key]['item_image'] = $order_item->product->image;
                        $order_details['items'][$key]['item_cost'] = $order_item->cost;
                        $order_details['items'][$key]['description'] = $order_item->description;
                        $shop = Shop::whereId($order_item->product->shop_id)->first();
                        $order_details['shop_lat'] = $shop->latitude;
                        $order_details['shop_lng'] = $shop->longitude;
                        $order_details['items'][$key]['shop_id'] = $shop->id;
                        $order_details['items'][$key]['variations'] = [];
                        if (sizeof($order_item->ordItemVars) > 0) {
                            foreach ($order_item->ordItemVars as $key2 => $variation) {
                                $order_details['items'][$key]['variations'][$variation->variation_name]['id'] = $variation->id;
                                $order_details['items'][$key]['variations'][$variation->variation_name]['name'] = $variation->variation_name;
                                $order_details['items'][$key]['variations'][$variation->variation_name]['options'][]['name'] = $variation->option_name;
                            }
                            $order_details['items'][$key]['variations'][$variation->variation_name]['options'] = array_values($order_details['items'][$key]['variations'][$variation->variation_name]['options']);
                        }
                        $order_details['items'][$key]['variations'] = array_values($order_details['items'][$key]['variations']);
                    }
                }
                $order=$order_details;

            }
            return $orders;

        }
    }

    public function orderDetails($order_id)
    {
        $order = Order::where('id', $order_id)->first();
        if ($order->delivery_type == 0) {
            $fees = $order->shop->delivery_fees;
        }
        elseif ($order->delivery_type == 1) {
            $fees = getFastShippingFees();
        }
        else {
            $fees = 0;
        }
        $order_details = [];
        $order_details['id'] = $order->id;
        $order_details['order_number'] = $order->order_number;
        $order_details['order_status'] = $order->status;
        $order_details['total_cost'] = strval($order->total_cost + ($fees));
        $order_details['delivery_type'] = $order->delivery_type;
         $order_details['pdf'] = route('download.order.pdf').'?order_id='.$order->id;
        $order_details['delivery_time'] = $order->delivery_time ? $order->delivery_time : $order->shop->delivery_time;
        $order_details['shipping_fees'] = $fees;
        $order_details['payment_type'] = $order->payment;
        $order_details['user_id'] = isset($order->user_id) ? $order->user_id : 0;
        $order_details['user_name'] = isset($order->user) ? $order->user->name : "";
        $order_details['user_phone'] = isset($order->user) ? $order->user->phone : "";
        $order_details['user_image'] = isset($order->user) ? $order->user->image : "";
        $order_details['delegate_id'] = isset($order->delegate_id) ? $order->delegate_id : 0;
        $order_details['delegate_phone'] = isset($order->delegate) ? $order->delegate->phone : "";
        $order_details['latitude'] = isset($order->address) ? $order->address->latitude : "";
        $order_details['longitude'] = isset($order->address) ? $order->address->longitude : "";
        $order_details['address'] = isset($order->address) ? $order->address->address : "";
        $order_details['notes'] = isset($order->notes) ? $order->notes : "";
        $order_details['created_at'] = $order->created_at;
        $order_details['is_rated_shop'] = $order->shop_rate ? 1 : 0;
        $order_details['shop_rate'] = $order->shop_rate ? $order->shop_rate : 0;
        $order_details['is_rated_user'] = $order->user_rate ? 1 : 0;
        $order_details['is_rated_delegate'] = $order->delegate_rate ? 1 : 0;
        $order_details['delegate'] = $order->delegate;

        $order_details['items'] = [];

        if (sizeof($order->orderItems) > 0) {
            foreach ($order->orderItems as $key => $order_item) {
                $order_details['items'][$key]['item_id'] = $order_item->product_id;
                $order_details['items'][$key]['item_quantity'] = $order_item->quantity;
                $order_details['items'][$key]['item_name'] = $order_item->product->name;
                $order_details['items'][$key]['item_image'] = $order_item->product->image;
                $order_details['items'][$key]['item_cost'] = $order_item->cost;
                $order_details['items'][$key]['description'] = $order_item->description;
                $shop = Shop::whereId($order_item->product->shop_id)->first();
                $order_details['shop_lat'] = $shop->latitude;
                $order_details['shop_lng'] = $shop->longitude;
                $order_details['items'][$key]['shop_id'] = $shop->id;
                $order_details['items'][$key]['variations'] = [];
                if (sizeof($order_item->ordItemVars) > 0) {
                    foreach ($order_item->ordItemVars as $key2 => $variation) {
                        $order_details['items'][$key]['ord_item_vars']=$order_item->ordItemVars;

                        $order_details['items'][$key]['variations'][$variation->variation_name]['id'] = $variation->id;
                        $order_details['items'][$key]['variations'][$variation->variation_name]['name'] = $variation->variation_name;
                        $order_details['items'][$key]['variations'][$variation->variation_name]['options'][]['name'] = $variation->option_name;
                    }
                    $order_details['items'][$key]['variations'][$variation->variation_name]['options'] = array_values($order_details['items'][$key]['variations'][$variation->variation_name]['options']);
                }
                $order_details['items'][$key]['variations'] = array_values($order_details['items'][$key]['variations']);
            }
        }
        return $order_details;

    }

    public function sendToDelegates($request)
    {
        // send notifications to all delegates if order delivery type is fase
        $order = Order::whereId($request->order_id)->first();
        $shop = Shop::whereId($order->shop_id)->select('id','logo','expire_at')->first();
        if ($order->delivery_type < 2) {
            //delivery_type <==> 0=>Normal, 1=>Fast, 2=>From shop
            //status <===> 0=>created ,1=>accepted ,2=>on_way ,3=>delivered ,4=>cancelled
            $order->update(['status' => 5]);
            if ($request->all == 1) {
                $delegates_firebase_tokens = User::where('type', 1)->where('status', 1)->get();
                foreach ($delegates_firebase_tokens as $delegate) {
                    send_to_user([$delegate->firebase_token], 'تم إنشاء طلب جديد', '1', $order->shop_id, $order->id, $shop,$delegate->platform);
                }
            } else {
                $delegates_firebase_tokens = User::where('type', 1)->where('status', 1)->whereIn('id', $request->delegates_id)->get();
                foreach ($delegates_firebase_tokens as $delegate) {

                    send_to_user([$delegate->firebase_token], 'تم إنشاء طلب جديد', '1', $order->shop_id, $order->id, $shop,$delegate->platform);
                }

            }
        }
    }

    public function allDelegates($request)
    {
        if($request->key) {
            if(is_numeric($request->key)) {
                return User::where('phone', 'like', '%'.$request->key.'%')->where('type', 1)->where('status', 1)->select('id', 'name', 'image', 'type', 'status', 'phone')->paginate(20);
            }
            else{
                return User::where('name', 'like', '%'.$request->key.'%')->where('type', 1)->where('status', 1)->select('id', 'name', 'image', 'type', 'status', 'phone')->paginate(20);

            }
            }
        else{
            return User::where('type', 1)->where('status', 1)->select('id', 'name', 'image', 'type', 'status','phone')->paginate(20);

        }
    }

    public function changeOrderStatus($request)
    {
        $order = Order::where('id', $request->order_id)->first();
//        return $order->user->firebase_token;

        $order->status = $request->status;
        $shop = Shop::whereId($order->shop_id)->select('id','expire_at','logo')->first();
        unset($shop->images);
        if ($order->save()) {
            if($order->delivery_type==2) {
                if ($request->status == 1) {

                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز تم قبول طلبك', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => ($shop->parent_id == NULL) ? ($shop->id) : ($shop->parent_id), // shop
                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'تم قبول طلبك';
                    $notification->translateOrNew('en')->body = 'تم قبول طلبك';
                    $notification->save();
                }
                if ($request->status == 2) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز لقد تم تجهيز الطلب', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => ($shop->parent_id == NULL) ? ($shop->id) : ($shop->parent_id), // shop

                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'عميلنا العزيز تم تجهيز طلبك ';
                    $notification->translateOrNew('en')->body = 'عميلنا العزيز تم تجهيز طلبك ';
                    $notification->save();
                }
                if ($request->status == 3) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز لقد تم استلام الطلب', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => ($shop->parent_id == NULL) ? ($shop->id) : ($shop->parent_id), // shop

                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'عميلنا العزيز تم إستلام طلبك ';
                    $notification->translateOrNew('en')->body = 'عميلنا العزيز تم إستلام طلبك ';
                    $notification->save();
                }
                if ($request->status == 4) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز نأسف لإبلاغك بأنه تم إلغاء طلبك', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => ($shop->parent_id == NULL) ? ($shop->id) : ($shop->parent_id), // shop

                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'عميلنا العزيز نأسف لإبلاغك بأنه تم إلغاء طلبك';
                    $notification->translateOrNew('en')->body = 'عميلنا العزيز نأسف لإبلاغك بأنه تم إلغاء طلبك';
                    $notification->save();
                }
            }
            else{
                if ($request->status == 1) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز تم قبول طلبك', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => ($shop->parent_id == NULL) ? ($shop->id) : ($shop->parent_id), // shop
                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'تم قبول طلبك';
                    $notification->translateOrNew('en')->body = 'تم قبول طلبك';
                    $notification->save();
                }
                if ($request->status == 2) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز طلبك في الطريق', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => ($shop->parent_id == NULL) ? ($shop->id) : ($shop->parent_id), // shop

                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'عميلنا العزيز طلبك في الطريق ';
                    $notification->translateOrNew('en')->body = 'عميلنا العزيز طلبك في الطريق ';
                    $notification->save();
                }
                if ($request->status == 3) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز تم إنهاء طلبك بنجاح', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => ($shop->parent_id == NULL) ? ($shop->id) : ($shop->parent_id), // shop

                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'عميلنا العزيز تم إنهاء طلبك بنجاح';
                    $notification->translateOrNew('en')->body = 'عميلنا العزيز تم إنهاء طلبك بنجاح';
                    $notification->save();

                }
                if ($request->status == 4) {
                    send_to_user([$order->user->firebase_token], 'عميلنا العزيز نأسف لإبلاغك بأنه تم إلغاء طلبك', '1', $order->shop_id, $order->id, $shop,$order->user->platform);
                    $notification = Notification::create([
                        'user_id' => $order->user->id,
                        'shop_id' => ($shop->parent_id == NULL) ? ($shop->id) : ($shop->parent_id), // shop

                        'order_id' => $order->id,
                    ]);
                    $notification->translateOrNew('ar')->title = 'رسالة جديدة';
                    $notification->translateOrNew('en')->title = 'رسالة جديدة';
                    $notification->translateOrNew('ar')->body = 'عميلنا العزيز نأسف لإبلاغك بأنه تم إلغاء طلبك';
                    $notification->translateOrNew('en')->body = 'عميلنا العزيز نأسف لإبلاغك بأنه تم إلغاء طلبك';
                    $notification->save();
                }

            }

            return $order;
        }
    }

    public function rates($shop_id)
    {
        return Comment::where('shop_id', $shop_id)->with(['user' => function ($query) {
            $query->select('id', 'name', 'image');
        }])->paginate(20);
    }

    public function followers($shop_id)
    {
        $data = [];
        $data['count'] = Follower::where('shop_id', $shop_id)->count();
        $data['followers'] = Follower::where('shop_id', $shop_id)
            ->when(\request()->key && !is_numeric(\request()->key),function ($query){
                $query->whereHas('user',function ($query){
                    $query->where('name','like','%'.\request()->key.'%');
                });
            })
            ->when(\request()->key && is_numeric(\request()->key),function ($query){
                $query->whereHas('user',function ($query){
                    $query->where('phone','like','%'.\request()->key.'%');
                });
            })
            ->with(['user' => function ($query) {
            $query->select('id', 'name', 'image');
        }])->get();
        return $data;
    }

    public function notifications($request)
    {
        Notification::whereIn('sender_type', [0, 2])->where([ 'type' => -1, 'shop_id' => $request->shop_id])->where('read_at', null)->update(['read_at' => Carbon::now()]);
        return Notification::whereIn('sender_type', [0, 2])->where(['type' => -1, 'shop_id' => $request->shop_id])->when($request->from && $request->to, function ($query) use ($request) {
            $query->whereBetween('created_at', [$request->from, $request->to]);
        })->latest()->paginate(20);
    }

    public function chatHistory($request,$shop_id)
    {
       // dd(Carbon::parse($request->from)->toDateTimeString(), Carbon::parse($request->to)->toDateTimeString());
        $users = UserShopMessage::where('shop_id', $shop_id)
          ->when($request->from && $request->to, function ($query) use ($request) {
              $query->whereBetween('user_shop_messages.created_at', [Carbon::parse($request->from)->toDateTimeString(), Carbon::parse($request->to)->addDay()->toDateTimeString()]);
          })

          ->whereIn('receiver_type', [1,2])->orwhereIn('sender_type', [1,2])

            ->select('*')
            ->latest()
            ->get()
        ;
//        return $users;

        foreach ($users as $key=>$user) {
            if($user->sender_type==1) {
                $d = User::where('id', $user->receiver_id)->first();

            }
            else{
                $d = User::where('id', $user->sender_id)->first();

            }
            if($d){
                $user->id = $d->id;
                $user->name = $d->name;
                $user->image = $d->image;
                $user->email = $d->email;
                $user->phone = $d->phone;

            }
            else{
                $users->forget($key);
            }


        }
        return $users->unique();

    }

    public function messages($shop_id, $user_id)
    {
        UserShopMessage::where('shop_id', $shop_id)->where(function ($query) use ($user_id) {
            $query->where('sender_id', $user_id)->orWhere('receiver_id', $user_id);
        })->select('read_at')->update(['read_at'=>Carbon::now()]);

        $messages = UserShopMessage::where('shop_id', $shop_id)->where(function ($query) use ($user_id) {
            $query->where('sender_id', $user_id)->orWhere('receiver_id', $user_id);
        })->with('user')->latest()->get();
        return $messages;


    }

    public function sendMessage($request)
    {
        $message=UserShopMessage::create([
            'sender_id' => $request->shop_id,
            'sender_type' => 1,
            'receiver_id' => $request->receiver_id,
            'receiver_type' => 2,
            'shop_id' => $request->shop_id,
            'message' => isset($request->message) ? $request->message : '-',

        ]);
        //dd($message);
        //$time = date("F j, Y, g:i a",strtotime($request->message->created_at));
        //Chat::sendChat($request->user_id, $request->message,$time,$request->imag);
        //$message->load('admin');
        $shop = Shop::whereId($request->shop_id)->select('id','expire_at','logo')->first();
        unset($shop->images);
        $receiver = User::where('id', $request->receiver_id)->first();
        send_to_user($receiver->firebase_token, $request->message, 80, $request->shop_id, "", $shop,$receiver->platform);

//        if ($message){
        return $message;

    }
    public function sendNotification($request){
        $shop = Shop::whereId($request->shop_id)->select('id','logo','expire_at')->first();
        unset($shop->images);

        if($request->all==1 && $request->type=='notify'){

            $users = User::where('type', 0)->get();
//            dd(Auth::guard('shop')->user());
            foreach ($users as $user) {
                $notification = new Notification();
                $notification->user_id = $user->id;
                $notification->sender_type = 1; //shop
                $notification->img = $request->img; //shop
                $notification->shop_id = ($shop->parent_id == NULL) ? ($shop->id) : ($shop->parent_id); //shop
                $notification->save();
                $notification->translateOrNew('en')->title = 'UserDelegateMessage From Admin(shop)';
                $notification->translateOrNew('ar')->title = 'رسالة من مدير النظام(متجر)';
                $notification->translateOrNew('en')->body = $request->message;
                $notification->translateOrNew('ar')->body = $request->message;
                $notification->save();
                send_to_user([$user->firebase_token], $request->message, '10', "", '', $shop,$user->platform);

            }

        }
        elseif($request->all==0 && $request->type=='notify'){

            $users = User::whereIn('id',$request->user_ids)->where('type', 0)->get();
//            dd(Auth::guard('shop')->user());
            foreach ($users as $user) {
                $notification = new Notification();
                $notification->user_id = $user->id;
                $notification->sender_type = 1; //shop
                $notification->img = $request->img; //shop

                $notification->shop_id = ($shop->parent_id == NULL) ? ($shop->id) : ($shop->parent_id); //shop
                $notification->save();
                $notification->translateOrNew('en')->title = $request->title;
                $notification->translateOrNew('ar')->title = $request->title;
                $notification->translateOrNew('en')->body = $request->message;
                $notification->translateOrNew('ar')->body = $request->message;
                $notification->save();
                send_to_user([$user->firebase_token], $request->message, '10', "", '', $shop,$user->platform);

            }

        }
        else{
            $userIds=Order::whereIn('status',[1,2,3])->pluck('user_id')->toArray();
            $users = User::whereIn('id',$userIds)->where('type', 0)->get();


            foreach ($users as $user) {
                $notification = new Notification();
                $notification->user_id = $user->id;
                $notification->sender_type = 1; //shop
                $notification->shop_id = ($shop->parent_id == NULL) ? ($shop->id) : ($shop->parent_id); //shop
                $notification->img = $request->img; //shop

                $notification->save();
                $notification->translateOrNew('en')->title = 'UserDelegateMessage From Admin(shop)';
                $notification->translateOrNew('ar')->title = 'رسالة من مدير النظام(متجر)';
                $notification->translateOrNew('en')->body = $request->message;
                $notification->translateOrNew('ar')->body = $request->message;
                $notification->save();
                send_to_user([$user->firebase_token], $request->message, '10', "", '', $shop,$user->platform);

            }
        }

    }
    public function switchNotification($request){
//        dd(Carbon::now()->toDateTimeString());
        $shop = Shop::whereId($request->shop_id)->first();
        $shop->update([
            'online'=>$request->notification_flag==0?0:1,
            'notification_flag'=>$request->notification_flag,
            'notification_time'=>$request->notification_time,
            'notification_start_date'=>Carbon::now()->toDateTimeString(),
        ]);
        return $shop;

    }
    public function searchOrders($request){
        $orders= Order::where('shop_id',$request->shop_id)->where('order_number','like','%'.$request->key.'%')->orwhereHas('user',function ($query)use($request){
            $query->where('phone','like','%'.$request->key.'%')->orwhere('name','like','%'.$request->key.'%');
        })->paginate(20);
        foreach ($orders as $order) {
            if ($order->delivery_type == 0) {
                $fees = $order->shop->delivery_fees;
            } elseif ($order->delivery_type == 1) {
                $fees = getFastShippingFees();
            } else {
                $fees = 0;
            }


            $order_details = [];
            $order_details['id'] = $order->id;
            $order_details['order_number'] = $order->order_number;
            $order_details['order_status'] = $order->status;
            $order_details['total_cost'] = strval($order->total_cost + ($fees));
            $order_details['delivery_type'] = $order->delivery_type;
            // $order_details['delivery_time'] = $order->shop ? $order->shop->delivery_time : "";
            $order_details['delivery_time'] = $order->delivery_time ? $order->delivery_time : $order->shop->delivery_time;
            $order_details['shipping_fees'] = $fees;
            $order_details['payment_type'] = $order->payment;
            $order_details['user_id'] = isset($order->user_id) ? $order->user_id : 0;
            $order_details['user_name'] = isset($order->user) ? $order->user->name : "";
            $order_details['user_phone'] = isset($order->user) ? $order->user->phone : "";
            $order_details['user_image'] = isset($order->user) ? $order->user->image : "";

            $order_details['delegate_id'] = isset($order->delegate_id) ? $order->delegate_id : 0;
            $order_details['delegate_phone'] = isset($order->delegate) ? $order->delegate->phone : "";
            $order_details['latitude'] = isset($order->address) ? $order->address->latitude : "";
            $order_details['longitude'] = isset($order->address) ? $order->address->longitude : "";
            $order_details['address'] = isset($order->address) ? $order->address->address : "";
            $order_details['notes'] = isset($order->notes) ? $order->notes : "";
            $order_details['created_at'] = $order->created_at;
            $order_details['is_rated_shop'] = $order->shop_rate ? 1 : 0;
            $order_details['is_rated_user'] = $order->user_rate ? 1 : 0;
            $order_details['is_rated_delegate'] = $order->delegate_rate ? 1 : 0;

            $order_details['items'] = [];
            if (sizeof($order->orderItems) > 0) {
                foreach ($order->orderItems as $key => $order_item) {
                    $order_details['items'][$key]['item_id'] = $order_item->product_id;
                    $order_details['items'][$key]['item_quantity'] = $order_item->quantity;
                    $order_details['items'][$key]['item_name'] = $order_item->product->name;
                    $order_details['items'][$key]['item_image'] = $order_item->product->image;
                    $order_details['items'][$key]['item_cost'] = $order_item->cost;
                    $order_details['items'][$key]['description'] = $order_item->description;
                    $shop = Shop::whereId($order_item->product->shop_id)->first();
                    $order_details['shop_lat'] = $shop->latitude;
                    $order_details['shop_lng'] = $shop->longitude;
                    $order_details['items'][$key]['shop_id'] = $shop->id;
                    $order_details['items'][$key]['variations'] = [];
                    if (sizeof($order_item->ordItemVars) > 0) {
                        foreach ($order_item->ordItemVars as $key2 => $variation) {
                            $order_details['items'][$key]['variations'][$variation->variation_name]['id'] = $variation->id;
                            $order_details['items'][$key]['variations'][$variation->variation_name]['name'] = $variation->variation_name;
                            $order_details['items'][$key]['variations'][$variation->variation_name]['options'][]['name'] = $variation->option_name;
                        }
                        $order_details['items'][$key]['variations'][$variation->variation_name]['options'] = array_values($order_details['items'][$key]['variations'][$variation->variation_name]['options']);
                    }
                    $order_details['items'][$key]['variations'] = array_values($order_details['items'][$key]['variations']);
                }
            }
            $order=$order_details;

        }
        return $orders;

    }
    public function downloadOrderPdf($request){
        $data['title'] = 'تفاصيل الطلب';
        $order = Order::where('id', $request->order_id)->with(['delegate','shop'])->first();
        $settings=Setting::select('fast_delivery_fees')->first();
        $order['fast_delivery']=$settings->fast_delivery_fees;
        $data['order'] = $order;

        $pdf = Pdf::loadView('shop::pdf', compact('data'));
        return $pdf->stream('Order-' . $order->order_number . '.pdf');

    }

}
