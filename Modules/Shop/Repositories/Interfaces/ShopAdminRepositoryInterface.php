<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/25/2020
 * Time: 2:13 PM
 */

namespace Modules\Shop\Repositories\Interfaces;

interface ShopAdminRepositoryInterface
{
    public function statistics();
}
