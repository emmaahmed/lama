<?php

namespace Modules\Shop\Repositories\Interfaces;

interface ShopRepositoryInterface
{
    public function shopsByCategory($request);

    public function getShops();

    public function create();

    public function store($request);

    public function edit($request);

    public function update($request);

    public function changeStatus($request);

    public function shopRemoveImages($request);

    public function showProducts($request);

    public function changeShopStatus($request);

    public function followAndUnfollow($user_id,$request,$lang);
    public function followingList($user_id,$request,$lang);
    public function commentsList($user_id,$request,$lang,$shop_id);
    public function addComment($user_id,$request,$lang);
    public function getNotActiveShops();



}
