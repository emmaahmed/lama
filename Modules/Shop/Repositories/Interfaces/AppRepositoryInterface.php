<?php


namespace Modules\Shop\Repositories\Interfaces;


interface AppRepositoryInterface
{
    public function aboutUs();
    public function terms();
    public function contactUs($request);
    public function privacy();
}
