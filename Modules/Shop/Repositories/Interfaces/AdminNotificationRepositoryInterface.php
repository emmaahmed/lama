<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/23/2020
 * Time: 3:30 PM
 */

namespace Modules\Shop\Repositories\Interfaces;

interface AdminNotificationRepositoryInterface
{
    public function index();

    public function sendNotificationView();

    public function sendNotification($request);
}
