<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/25/2020
 * Time: 1:14 PM
 */

namespace Modules\Shop\Repositories\Interfaces;

interface AuthRepositoryInterface
{
    public function doLogin($request);

    public function doForgetPassword($request);

    public function resetPassword($token);

    public function doResetPassword($request);
}
