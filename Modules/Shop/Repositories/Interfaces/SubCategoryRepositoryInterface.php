<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/22/2020
 * Time: 1:09 PM
 */

namespace Modules\Shop\Repositories\Interfaces;

interface SubCategoryRepositoryInterface
{
    public function index($request);

    public function store($request);
}
