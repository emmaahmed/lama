<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/29/2020
 * Time: 3:20 PM
 */

namespace Modules\Shop\Repositories\Interfaces;

interface AdminRepositoryInterface
{
    public function index();

    public function create();

    public function store($request);

    public function edit($request);

    public function update($request);
    
    public function permissions($permissions);
}
