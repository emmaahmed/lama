<?php

namespace Modules\Cart\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Variation;
use Modules\Product\Entities\VariationOption;

class CartItemVar extends Model
{

    protected $fillable = ['cart_item_id','order_item_id','variation_id','option_id'];

    protected $hidden = ['created_at','updated_at'];

    public function CartItem()
    {
        return $this->belongsTo(CartItem::class,'cart_item_id');
    }

    public function variation()
    {
        return $this->belongsTo(Variation::class,'variation_id');
    }
    public function option()
    {
        return $this->belongsTo(VariationOption::class,'option_id');
    }
}
