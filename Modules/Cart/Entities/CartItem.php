<?php

namespace Modules\Cart\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Product;

class CartItem extends Model
{
    protected $fillable = ['cart_id','product_id','quantity','cost','description'];

    public function cart()
    {
        return $this->belongsTo(Cart::class,'cart_id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function cartItemVars()
    {
        return $this->hasMany(CartItemVar::class,'cart_item_id');
    }
}
