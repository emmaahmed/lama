<?php

namespace Modules\Cart\Entities;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Shop\Entities\Shop;

class Cart extends Model
{
    protected $fillable = ['user_id','shop_id','total_cost'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function shop()
    {
        return $this->belongsTo(Shop::class,'shop_id');
    }

    public function cartItems()
    {
        return $this->hasMany(CartItem::class);
    }

    public function itemVariations()
    {
        return $this->hasManyThrough(CartItemVar::class, CartItem::class);
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->toDateString();
    }
}
