<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/cart', function (Request $request) {
    return $request->user();
});
Route::middleware('checkDatabase')->group(function () {

Route::namespace('Api')->group(function(){
//    Cart
    Route::post('/add-to-cart', 'CartController@addToCart');
    Route::get('/add-quantity/{cart_id}', 'CartController@addToQuantity');
    Route::get('/cart', 'CartController@cart');
    Route::post('/remove-from-cart', 'CartController@removeFromCart');

});
});


