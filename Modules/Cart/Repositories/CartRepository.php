<?php


namespace Modules\Cart\Repositories;


use App\User;
use Modules\Cart\Entities\Cart;
use Modules\Cart\Entities\CartItem;
use Modules\Cart\Entities\CartItemVar;
use Modules\Cart\Repositories\Interfaces\CartRepositoryInterface;
use Modules\Product\Entities\Product;
use Modules\Setting\Entities\Setting;
use Modules\Shop\Entities\Shop;

class CartRepository implements CartRepositoryInterface
{
    public function addToCart($request, $user_id)
    {
        $cart = Cart::where('user_id',$user_id)->first();
        if(isset($request->forceEmptyAll) && $request->forceEmptyAll == 1){
            $cart->cartItems()->delete();
            $cart->delete();
            return true;
        }

        $variations1 = $request->variations;

         $new_item = Product::where(['id' => $request->product_id])->first();

        if($new_item){
             $shop=Shop::where('id',$new_item->shop_id)->first();

            if($shop) {

                $result= inRange($request->lat, $request->lng, $shop->area*1000,$shop->id);
//             dd($result);
               if(!$result){
                   return 'not-in-range';
               }
            }

        }
        if (isset($cart)) {
            $this->itemToExistingCart($cart, $new_item, $variations1, $request);
        }else{
            $this->firstCartItem($user_id, $new_item, $variations1, $request);
        }
        return true;
    }

    // Add the first item to Cart

    public function firstCartItem($user_id, $new_item, $variations1, $request, $cart = null ,$cart_exists = false)
    {

        if (!$cart_exists) {
            $cart = Cart::create([
                'user_id' => $user_id,
                'shop_id' => $new_item->shop_id!=null?$new_item->shop_id:null,
            ]);
        }

        $cart_item = CartItem::create([
            'cart_id' => $cart->id,
            'product_id' => $new_item->id,
            'quantity' => 1,
            'description' => $request->description,
        ]);
        if (sizeof($variations1) > 0){
            foreach ($variations1 as $variation) {
                foreach ($variation['options'] as $option) {

                    if($variation['variation_id'] >0){
                        CartItemVar::create([
                            'cart_item_id' => $cart_item->id,
                            'variation_id' => $variation['variation_id'],
                            'option_id' => $option['option_id'],
                        ]);
                    }
                    else{
                        CartItemVar::create([
                            'cart_item_id' => $cart_item->id,
                            'variation_id' => $variation['id'],
                            'option_id' => $option['id'],
                        ]);
                    }

                }
            }
        }
    }

    // Add Item To Existing Cart

    public function itemToExistingCart($cart, $new_item, $variations1, $request)
    {
        // Check item from same shop
        if ($cart->shop_id != $new_item->shop_id) {
            if ($request->forceEmpty == 0){
                return 'product_not_in_cart';
            }else{
                $cart->shop_id = $new_item->shop_id!=null?$new_item->shop_id:null;
                $cart->save();
                $cart->cartItems()->delete();
            }
        }

        // Check item exists previously in cart

        $cart_items_ids = $cart->cartItems()->pluck('product_id')->toArray();
        if (in_array($new_item->id, $cart_items_ids)){
            $cart_items = CartItem::where('cart_id', $cart->id)->where('product_id', $new_item->id)->get();

            // Put cart item variations to variations2 array
            $variations2 = [];
            foreach ($cart_items as $key => $cart_item) {
                $data = $cart_item->cartItemVars;
                for ($i = 0; $i < sizeof($data); $i++) {
                    $index = -1;
                    for ($j = 0; $j < sizeof($variations2); $j++) {
                        if ($variations2[$j]['variation_id'] == $data[$i]['variation_id']) {
                            $index = $j;
                            break;
                        }
                    }
                    if ($index == -1) {
                        $index = sizeof($variations2);
                    }
                    $variations2[$index]['variation_id'] = $data[$i]['variation_id'];
                    if (!isset($variations2[$index]['options'])) {
                        $variations2[$index]['options'] = [];
                    }
                    array_push($variations2[$index]['options'] , ['option_id' => $data[$i]['option_id']]);
                    // dd($index);
                }
            }
//            dd($variations1,$variations2);
            if ($this->isSameVariations($variations1, $variations2)){ // update item
                $cart_item->quantity += 1;
                $cart_item->save();
            }else{ // insert new item
                $this->firstCartItem($cart->user_id, $new_item, $variations1, $request, $cart, true);
            }
        }else{
            $this->firstCartItem($cart->user_id, $new_item, $variations1, $request, $cart, true);
        }
    }

    // check variations identification

    public function isSameVariations($variations1, $variations2)
    {
        $count = 0;
        for ($i = 0; $i < sizeof($variations1); $i++) {
            for ($j = 0; $j < sizeof($variations2); $j++) {
                if ($variations1[$i]['variation_id'] == $variations2[$j]['variation_id']
                    && $this->isSameOptions($variations1[$i]['options'],
                        $variations2[$j]['options'])) {
                    $count++;
                }
            }
        }
        return(((sizeof($variations1) == sizeof($variations2)) && sizeof($variations2) == $count));
    }

    // check options identification

    public function isSameOptions($options1, $options2)
    {
        $count = 0;
        for ($i = 0; $i < sizeof($options1); $i++) {
            for ($j = 0; $j < sizeof($options2); $j++) {
                if ($options1[$i]['option_id'] == $options2[$j]['option_id']) {
                    $count++;
                }
            }
        }
        return ((sizeof($options1) == sizeof($options2)) && sizeof($options2) == $count);
    }

    public function cart($user_id)
    {
        $cart = Cart::where('user_id', $user_id)->first();
        $cart_array = $this->getCartItems($cart);
        $settings = Setting::first();
        $cart_array['payment_method'] = $cart && $cart->shop ? $cart->shop->payment : [];
        $cart_array['delivery_types'] = $cart && $cart->shop ? $cart->shop->delivery_types : [];
        $cart_array['delivery_fees']['normal'] = $cart && $cart->shop ? $cart->shop->delivery_fees : "0";
        $cart_array['min_order'] = $cart && $cart->shop && $cart->shop->min_order !=null? $cart->shop->min_order : "0";
        $cart_array['delivery_fees']['fast'] = $settings ? $settings->fast_delivery_fees : 0;
        return $cart_array;
    }

    public function removeFromCart($cart_item_id, $user_id, $type)
    {
        $cart = Cart::where('user_id', $user_id)->first();
       $cart_item = CartItem::where('id', $cart_item_id)->first();
       if (in_array($cart_item->id, $cart->cartItems()->pluck('id')->toArray())){
            if ($cart_item->quantity == 1 || $type == 1){
                $cart_item2 = $cart_item->delete();
                $items = CartItem::where('cart_id', $cart->id)->select('id')->get();
                if(sizeof($items) < 1)
                    $cart->delete();

                return $cart_item2;

            }
            $cart_item->quantity -= 1;
            return $cart_item->save();
        }

        return false;
    }

    public function getCartItems($cart)
    {
        $cart_array = [];
        $cart_array['shop_id'] = 0;
        $cart_array['not_available_products'] = array();
        $cart_array['items'] = [];
        $cart_array['total_cost'] = (int)"";

        if (isset($cart->cartItems) && sizeof($cart->cartItems) > 0){
            $total_cost = 0;
            $cart_array['shop_id'] = $cart->shop_id;
            $shop=Shop::whereId($cart->shop_id)->first();
            if($shop) {
                $cart_array['fast_delivery'] =$shop->fast_delivery ;
            }
            foreach ($cart->cartItems as $key => $cart_item){
//                dd($cart_item->product);
                $item_cost = ($cart_item->product->has_discount ? $cart_item->product->price_after : $cart_item->product->price_before) * $cart_item->quantity;
                $cart_array['items'][$key]['id'] = $cart_item->id;
                $cart_array['items'][$key]['item_id'] = $cart_item->product_id;
                $product=Product::whereId($cart_item->product_id)->first();
                if($product){
                    if($product->quantity<$cart_item->quantity){

                        array_push($cart_array['not_available_products'],$cart_item->product->name);

                    }
                }
                $cart_array['items'][$key]['item_name'] = $cart_item->product->name;
                $cart_array['items'][$key]['description'] = $cart_item->description;
                $cart_array['items'][$key]['item_quantity'] = $cart_item->quantity;
//                $cart_array['items'][$key]['item_old_price'] = $item_cost;
                $cart_array['items'][$key]['item_price'] = "";
                $cart_array['items'][$key]['item_image'] = $cart_item->product->image;

                $cart_array['items'][$key]['variations'] = [];
                $options_cost = 0;
                if (sizeof($cart_item->cartItemVars) > 0) {
                    foreach ($cart_item->cartItemVars as $key2 => $variation){
                        $cart_array['items'][$key]['variations'][$variation->variation_id]['id'] = $variation->variation_id;
                        $cart_array['items'][$key]['variations'][$variation->variation_id]['name'] = $variation->variation->name;
                        //$cart_array['items'][$key]['variations'][$variation->variation_id]['type'] = $variation->variation->type;
                        // type=3 , then this is size of product
                        if($variation->variation->type == 3){
                            $item_cost = 0;
                        }
                        //
                        // $cart_array['items'][$key]['variations'][$variation->variation_id]['options'][$key2]['id'] = $variation->option_id;
                        // $cart_array['items'][$key]['variations'][$variation->variation_id]['options'][$key2]['name'] = $variation->option->name;
                        // $cart_array['items'][$key]['variations'][$variation->variation_id]['options'][$key2]['price'] = $variation->option->price;
                         $cart_array['items'][$key]['variations'][$variation->variation_id]['options'][] = $variation->option;
                        // $cart_array['items'][$key]['variations'][$variation->variation_id]['options'][]['name'] = $variation->option->name;
                        // $cart_array['items'][$key]['variations'][$variation->variation_id]['options'][]['price'] = $variation->option->price;
                        $options_cost += $variation->option->price;
                    }
                    $cart_array['items'][$key]['variations'][$variation->variation_id]['options'] = array_values($cart_array['items'][$key]['variations'][$variation->variation_id]['options']);
                }
                $item_cost += ($options_cost * $cart_item->quantity);

                //$cart_array['items'][$key]['item_price'] = number_format($item_cost, 2, '.', ',');
                $cart_array['items'][$key]['item_price'] = $item_cost;
                $cart_array['items'][$key]['variations'] = array_values($cart_array['items'][$key]['variations']);
                $total_cost += $item_cost;
            }
        //$cart_array['total_cost'] = number_format($total_cost, 2, '.', ',');
            $cart_array['total_cost'] = $total_cost;
        }
        return $cart_array;
    }

    public function addToQuantity($cart_id){
         $cart=CartItem::whereId($cart_id)->first();
         $product=Product::whereId($cart->product_id)->first();
         if($cart->quantity==$product->quantity){
             return 'false';
         }

         if($cart){
             $cart->update([
                 'quantity'=>$cart->quantity+1
             ]);
         }
}
}
