<?php

namespace Modules\Cart\Repositories\Interfaces;

interface CartRepositoryInterface
{
    public function addToCart($request, $user_id);

    public function cart($user_id);

    public function removeFromCart($cart_item_id, $user_id, $type);

    public function getCartItems($cart);


}
