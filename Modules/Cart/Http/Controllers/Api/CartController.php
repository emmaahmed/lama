<?php

namespace Modules\Cart\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Cart\Http\Requests\CartRequest;
use Modules\Cart\Repositories\Interfaces\CartRepositoryInterface;

class CartController extends Controller
{
    protected $cartRepository;

    public function __construct(CartRepositoryInterface $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function addToCart(CartRequest $request)
    {

        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $added = $this->cartRepository->addToCart($request, $user->id);
            if (is_string($added) && $added=='not-in-range' ){
                return callback_data(error(), 'not-in-range');
            }
            if ($added){
                return callback_data(success(), 'item_added_to_cart',['cart_list_size' => count($user->cart?$user->cart->cartItems:[])]);
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }
    public function addToQuantity($cart_id)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
              $added = $this->cartRepository->addToQuantity($cart_id);
              if($added=='false'){
                  return callback_data(error(), 'quantity_not_available');

              }
                return callback_data(success(), 'success');

        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function cart()
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $cart = $this->cartRepository->cart($user->id);
            return callback_data(success(), 'cart', $cart);
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function removeFromCart(CartRequest $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $deleted = $this->cartRepository->removeFromCart($request->cart_item_id, $user->id,$request->type);
            if ($deleted){
                return callback_data(success(), 'item_removed_from_cart');
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }
}
