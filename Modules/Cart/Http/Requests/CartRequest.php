<?php

namespace Modules\Cart\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class CartRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('api/add-to-cart')) {
            return $this->addToCartRules();
        }
        if($this->is('api/remove-from-cart')) {
            return $this->removeFromCartRules();
        }
    }

    public function messages()
    {
        if($this->is('api/add-to-cart')) {
            return $this->addToCartMessages();
        }
        if($this->is('api/remove-from-cart')) {
            return $this->removeFromCartMessages();
        }
    }

    public function addToCartRules()
    {
        return [
            'product_id' => 'sometimes|exists:products,id',
            'lat'=>'required',
            'lng'=>'required',
        ];
    }

    public function addToCartMessages()
    {
        return [
            'product_id.required' => 'product_required',
            'product_id.exists' => 'product_not_found',
            'lat.required' => 'lat_required',
            'lng.required' => 'lng_required',
        ];
    }

    public function removeFromCartRules()
    {
        return [
            'cart_item_id' => 'required|exists:cart_items,id',
        ];
    }

    public function removeFromCartMessages()
    {
        return [
            'cart_item_id.required' => 'cart_item_id_required',
            'cart_item_id.exists' => 'cart_item_id_not_found',
        ];
    }
}
