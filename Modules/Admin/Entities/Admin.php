<?php

namespace Modules\Admin\Entities;

use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable , HasRoles;
    protected $guard_name = 'admin';

    protected $fillable = [
        'name', 'email', 'password','image','firebase_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getRoleNames()
    {
        return $this->roles;
    }

    public function getImageAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/admins').'/'.$image;
        }
        return "";
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function setImageAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'admins');
            $this->attributes['image'] = $imageFields;
        }
    }


    public static function getTableName()
    {
        return (new self())->getTable();
    }
}
