<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Chat extends Model
{

      protected $fillable = [
        'admin_id', 'shop_id', 'message', 'image', 'sender_type', 'receiver_type', 'is_read'
    ];

    public function admin()
    {
        return $this->belongsTo(\App\Http\Middleware\Admin::class, 'admin_id');
    }

    // function getCreatedAtAttribute()
    // {
    //     return Carbon::parse($this->attributes['created_at'])->diffForHumans();
    // }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('images/chats/' . $value);
        }
    }

    public function setImageAttribute($value)
    {
        if ($value) {
            $img_name = time() . rand(1111, 9999) . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images/chats/'), $img_name);
            $this->attributes['image'] = $img_name;
        }
    }

      static function sendChat($user_id, $message,$time, $image = null)
    {
        define('API_ACCESS_KEY', 'AAAAUDod_I8:APA91bFnoaEKFOhsrwSu-ZJmSjQ3dRiZMUofUxWVxjMUEFQkrAHzPFIqAU_sJZ55Ip-COKMGmimXQ3EgIwgryUKFfuoENdLF0OzU5TpONcRI6S0-e4R7XiUcTcztDNCItMYFoPRoHFVY');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $user_token = User::where('id', $user_id)->pluck('firebase_token')->first();

        if (!empty($image)) {
            $message_type = 2;
            $title = 'New Message';
            $body = 'A picture has been sent';
        } else {
            $message_type = 1;
            $title = 'New Message';
            $body = $message;
        }


        $notification = [
            'icon' => 'myIcon',
            'sound' => 'mySound',
            'title' => $title,
            'body' => $body,
            'type_data' => (int)$message_type,
            'notification_type' => 3,
            'time'=>$time,
            'message' => !empty($message) ? $message : "",
            'image' => !empty($image) ? $image : "",
        ];

        $extraNotificationData = ["message" => $notification];
        $fcmNotification = [
            'to' => $user_token, //single token,
            'notification' => $notification,
            'data' => $extraNotificationData
        ];
        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}
