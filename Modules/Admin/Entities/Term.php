<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Term extends Model
{
    use Notifiable;


    protected $table = 'terms';

    protected $fillable = [
        'id','term','type'
    ];




}
