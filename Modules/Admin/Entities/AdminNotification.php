<?php

namespace Modules\Admin\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Shop\Entities\Shop;

class AdminNotification extends Model
{
    protected $fillable = ['user_id','shop_id','delegate_id','message','type','sender_type','image'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function shop()
    {
        return $this->belongsTo(Shop::class,'shop_id');
    }
    public function delegate()
    {
        return $this->belongsTo(User::class,'delegate_id');
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }
    public function setImageAttribute($image)
    {
//        dd($image);
        if (is_file($image)) {
            $imageFields = upload($image, 'notifications');
            $this->attributes['image'] = $imageFields;
        }
    }
    public function getImageAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/notifications').'/'.$image;
        }
        return "";
    }


}
