<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class AboutUs extends Model
{
    use Notifiable;


    protected $table = 'aboutus';

    protected $fillable = [
        'id','body','facebook','twitter','instagram','phone','email'
    ];





}
