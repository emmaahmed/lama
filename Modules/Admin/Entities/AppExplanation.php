<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class AppExplanation extends Model
{
    use Notifiable;


    protected $table = 'app_explanations';

    protected $fillable = [
        'title','body'
    ];





}
