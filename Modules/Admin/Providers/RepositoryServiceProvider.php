<?php

namespace Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Admin\Repositories\AdminNotificationRepository;
use Modules\Admin\Repositories\AdminRepository;
use Modules\Admin\Repositories\AuthRepository;
use Modules\Admin\Repositories\IndexRepository;
use Modules\Admin\Repositories\Interfaces\AdminNotificationRepositoryInterface;
use Modules\Admin\Repositories\Interfaces\AdminRepositoryInterface;
use Modules\Admin\Repositories\Interfaces\AuthRepositoryInterface;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Admin\Repositories\Interfaces\UserRepositoryInterface;
use Modules\Admin\Repositories\UserRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(IndexRepositoryInterface::class, IndexRepository::class);
        $this->app->bind(AdminNotificationRepositoryInterface::class, AdminNotificationRepository::class);
        $this->app->bind(AdminRepositoryInterface::class, AdminRepository::class);
        $this->app->bind(AuthRepositoryInterface::class, AuthRepository::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
