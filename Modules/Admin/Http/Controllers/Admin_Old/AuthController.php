<?php

namespace Modules\Admin\Http\Controllers\Admin_Old;

use App\Admin;
use App\Mail\AdminResetPassword;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login()
    {
        return view('admin::admin.auth.login');
    }
    public function doLogin(Request $request)
    {
        $remember_me = $request->remember_me == 1 ? true : false;
        if (admin()->attempt(['email'=>$request->email,'password'=>$request->password,'status' => 1],$remember_me)){
            return redirect('admin/dashboard');
        }else{
            session()->flash('error','بيانات الدخول خاطئة');
            return redirect('admin/login');
        }
    }

    public function logout()
    {
        auth()->guard('admin')->logout();
        return redirect('admin/logout');
    }
    public function forgetPassword()
    {
        return view('admin::admin.auth.passwords.email');
    }

    public function doForgetPassword(Request $request)
    {
        $admin = Admin::where('email',$request->email)->first();
        if (!empty($admin)){
            $token = Str::random(64);
            DB::table('password_resets')->insert([
                'email' => $admin->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);
            Mail::to($admin->email)->send(new AdminResetPassword(['data' => $admin , 'token' => $token]));
            session()->flash('success','تم الإرسال للبريد الإلكتروني');
            return redirect()->back();
        }
        session()->flash('error','عذرا هذا البريد الإلكتروني غير مسجل لدينا');
        return redirect()->back();
    }

    public function resetPassword ($token)
    {
        $check_token = DB::table('password_resets')
            ->where('token',$token)
            ->where('created_at' , '>' , Carbon::now()->subHours(2))->first();
        if (!empty($check_token)){
            return view('admin::admin.auth.passwords.reset',['data' => $check_token]);
        }else{
            session()->flash('error','للأسف انتهت صلاحية كود التفعيل');
            return redirect(admin_url('forget/password'));
        }
    }
    public function doResetPassword (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required',
        ],[
            'password.required' => 'من فضلك أدخل كلمة المرور',
            'password.confirmed' => 'عفوا كلمة المرور غير متطابقة',
            'password.min' => 'من فضلك أدخل كلمة المرور أكثر من 6 حروف',
            'password_confirmation.required' => 'من فضلك أدخل نأكيد كلمة المرور'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $check_token = DB::table('password_resets')
            ->where('token',$request->token)
            ->where('created_at' , '>' , Carbon::now()->subHours(2))->first();
        if (!empty($check_token)){
            $admin = Admin::where('email',$check_token->email)->update([
                'email' => $check_token->email,
                'password' => bcrypt($request->password)
            ]);
            DB::table('password_resets')->where('email',$request->email)->delete();
            admin()->attempt(['email'=>$check_token->email,'password'=>$request->password,'status' => 1],true);
            session()->flash('success','تم تسجيل الدخول بنجاح');
            return redirect(admin_url('dashboard'));
        }else{
            session()->flash('error','للأسف انتهت صلاحية كود التفعيل');
            return redirect(admin_url('forget/password'));
        }
    }
}
