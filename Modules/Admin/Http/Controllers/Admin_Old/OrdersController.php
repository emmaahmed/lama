<?php

namespace Modules\Admin\Http\Controllers\Admin_Old;
use App\Exports\OrdersExport;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Modules\Order\Entities\Order;
use Modules\Product\Entities\Product;

class OrdersController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_order', ['only' => ['index','show','filterOrders']]);
        $this->middleware('permission:edit_order', ['only' => ['changeStatus']]);
        $this->middleware('permission:delete_order', ['only' => ['delete','destroyMulti','destroy']]);
        $this->middleware('permission:export_order', ['only' => ['exportOrders','PrintOrder']]);
    }

    public function index()
    {
        $orders = Order::orderBy('id','DESC')->get();
        foreach ($orders as $order){
            if (empty($order->getOriginal('image'))){
                $order->default_image = '/assets/images/default.png';
            }
        }

        $data['orders'] = $orders;
        $data['title'] = "الطلبات";
        return view('admin::admin.pages.orders.index',compact('data'));
    }

    public function changeStatus(Request $request)
    {
        $order = Order::where('id',$request->order_id)->first();
        $order->status = $request->status;
        if ($order->save()){
            if ($request->status == 1){
                send_to_user([$order->user->firebase_token],'تم قبول طلبك وجاري التوصيل' ,'1',$order->shop_id,$order->id);
                Notification::create([
                    'user_id' => $order->user->id,
                    'title' => 'رسالة جديدة',
                    'body' => 'تم قبول طلبك وجاري التوصيل',
                ]);
            }
            if ($request->status == 2){
                send_to_user([$order->user->firebase_token],'تم إنهاء طلبك بنجاح' ,'1',$order->shop_id,$order->id);
                Notification::create([
                    'user_id' => $order->user->id,
                    'title' => 'رسالة جديدة',
                    'body' => 'تم إنهاء طلبك بنجاح',
                ]);
            }
            if ($request->status == 3){
                send_to_user([$order->user->firebase_token],'تم إلغاء طلبك' ,'1',$order->shop_id,$order->id);
                Notification::create([
                    'user_id' => $order->user->id,
                    'title' => 'رسالة جديدة',
                    'body' => 'تم إلغاء طلبك',
                ]);
            }

            return response()->json(['success' => 'تم تعديل حالة الطلب بنجاح']);
        }else{
            return response()->json(['error' => 'حدث خطأ ما !']);
        }
    }

    public function showProducts($order_id)
    {
        $order = Order::where('id',$order_id)->first();
        $data['title'] = 'تفاصيل الطلب';
        $data['order'] = $order;

        return view('admin::admin.pages.orders.show',compact('data'));

    }

    public function filterOrders(Request $request , $id = null)
    {
        $builder = Order::whereNotNull('id');
        if(isset($request->from) && isset($request->to)){
            $builder->where('created_at', '>=', $request->from)->where('created_at', '<=', $request->to);
        }
        $orders = $builder->orderBy('id','DESC')->paginate(10);
        $data['orders'] = $orders;
        return View::make('admin::admin.pages.orders.div',compact('data'));
    }

    public function delete(Request $request)
    {
//        if (Auth::guard('admin')->user()->can('delete_user')){
        $order = Order::where('id',$request->order_id)->first();
        if (!empty($order->getOriginal('image'))){
            unlinkFile($order->getOriginal('image'), 'orders');
        }
        if ($order->delete()){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
//        }
//        else{
//            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
//            return redirect()->back();
//        }
    }

    public function destroyMulti(Request $request)
    {
//        if (Auth::guard('admin')->user()->can('delete_user')){
        // Init array of IDs
        $ids = $request->ids;
        foreach ($ids as $id) {
            $delete =$this->destroy($id);
            if (!$delete){
                return response()->json(['error' => 'للأسف حدث خطأ ما']);
            }
        }
        return response()->json(['success' => 'تم الحذف بنجاح']);
//        }
//        else{
//            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
//            return redirect()->back();
//        }
    }

    public function destroy($id)
    {
//        if (Auth::guard('admin')->user()->can('delete_user')){
        $order_id = $id;
        $order = Order::where('id',$order_id)->first();
        if (!empty($order->image)){
            unlinkFile($order->image, 'orders');
        }
        return $order->delete();
//        }
//        else{
//            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
//            return redirect()->back();
//        }
    }

    public function exportOrders($type, $id = null)
    {
        if ($type == 1){  // user orders
            $orders = Order::where('user_id',$id)->orderBy('id','DESC')->get();
        }elseif($type == 2){  // delegate orders
            $orders = Order::where('delegate_id',$id)->orderBy('id','DESC')->get();
        }else{  // all orders
            $orders = Order::orderBy('id','DESC')->get();
        }
        return (new OrdersExport($orders))->download('orders.xlsx');
    }

    public function PrintOrder(Request $request){
        $order = Order::find($request->order_id);
        $data['order'] = $order;
        return view('admin.pages.orders.print',compact('data'));
    }
}
