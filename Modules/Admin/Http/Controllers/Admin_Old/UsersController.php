<?php

namespace Modules\Admin\Http\Controllers\Admin_Old;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
//    function __construct()
//    {
//        $this->middleware('permission:show_user', ['only' => ['index','ordersByUser']]);
//        $this->middleware('permission:edit_user', ['only' => ['changeStatus','edit','update']]);
//        $this->middleware('permission:delete_user', ['only' => ['delete','destroyMulti','destroy']]);
//    }

    public function index()
    {
        $users = User::orderBy('id','DESC')->get();
        $data['users'] = $users;
        $data['title'] = "المستخدمين";
        return view('admin::admin.pages.users.index',compact('data'));
    }

    public function changeStatus(Request $request)
    {
        $user = User::where('id',$request->user_id)->first();
        $user->status = $request->status;
        if ($user->save()){
            if ($user->status == 1){
                return response()->json(['success' => 'تم تفعيل المستخدم بنجاح']);
            }
            return response()->json(['success' => 'تم إلغاء تفعيل المستخدم بنجاح']);
        }else{
            return response()->json(['error' => 'حدث خطأ ما !']);
        }
    }

    public function ordersByUser(Request $request)
    {
        $user_id = $request->user;
//        if (Auth::guard('admin')->user()->can('show_user')){
        $orders = Order::where('user_id',$user_id)->orderBy('id','DESC')->paginate(10);
        foreach ($orders as $order){
            if (empty($order->getOriginal('image'))){
                $order->default_image = '/assets/images/default.png';
            }
        }
        $data['orders'] = $orders;
        $data['user_id'] = $user_id;
        $data['title'] = "الطلبات";
        return view('admin::admin.pages..users.orders',compact('data'));
//        }else{
//            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
//            return redirect()->back();
//        }
    }

    public function edit(Request $request)
    {
        $user = User::where('id',$request->user_id)->first();
        $data['title'] = "تعديل المستخدم";
        $data['user'] = $user;
        return view('admin::admin.pages.users.edit',compact('data'));
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|unique:users,phone,'.$request->user_id,
            'email' => 'required|email|unique:users,email,'.$request->user_id,
        ],[
            'name.required' => 'من فضلك أدخل إسم المستخدم',
            'phone.required' => 'من فضلك أدخل رقم الهاتف',
            'phone.unique' => 'رقم الهاتف مستخدم من قبل',
            'email.required' => 'من فضلك أدخل بريد إلكتروني صالح',
            'email.email' => 'من فضلك أدخل بريد إلكتروني صالح',
            'email.unique' => 'البريد الإلكتروني مستخدم من قبل',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $user = User::where('id',$request->user_id)->first();
        if (isset($user)){
            if ($request->has('image') && is_file($request->image)){
                if (!empty($user->getOriginal('image'))){
                    unlinkFile($user->getOriginal('image'), 'users');
                }
            }
            $user->update($request->all());
            if ($request->has('status')){
                $user->status = 1;
            }else{
                $user->status = 0;
            }
            if ($user->save()){
                session()->flash('success', 'تم التعديل بنجاح');
                return redirect()->route('admin.users');
            }
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $user = User::where('id',$request->user_id)->first();
        if (!empty($user->getOriginal('image'))){
            unlinkFile($user->getOriginal('image'), 'users');
        }
        if ($user->delete()){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(Request $request)
    {
        // Init array of IDs
        $ids = [];
        foreach ($request->ids as $id){
            if ($id != 'on'){
                array_push($ids,$id);
            }
        }
        foreach ($ids as $id) {
            $delete =$this->destroy($id);
            if (!$delete){
                return response()->json(['error' => 'للأسف حدث خطأ ما']);
            }
        }
        return response()->json(['success' => 'تم الحذف بنجاح']);
    }

    public function destroy($id)
    {
            $user_id = $id;
            $user = User::where('id',$user_id)->first();
            if (!empty($user->image)){
                unlinkFile($user->image, 'users');
            }
            return $user->delete();
    }
}
