<?php

namespace Modules\Admin\Http\Controllers\Admin_Old;
use App\Delegate;
use App\DelegateNotification;
use App\Mail\ShopMail;
use App\Notification;
use App\User;
use App\UserNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Modules\Admin\Entities\AdminNotification;
use Modules\Shop\Entities\Shop;

class NotificationsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:notifications', ['only' => ['index','delete','destroyMulti','destroy']]);
        $this->middleware('permission:send_notification', ['only' => ['sendNotificationPage','sendNotification']]);
    }
    public function index()
    {
        $notifications = AdminNotification::get();
        $data['notifications'] = $notifications;
        $data['title'] = 'رسائل النظام';
        return view('admin::admin.pages.notifications.index',compact('data'));
    }

    public function sendNotificationPage()
    {
        $data['title'] = 'ارسال رسالة';
        $users = User::get();
        $shops = Shop::get();
        foreach ($users as $user){
            $user->type = 'user';
        }
        foreach ($shops as $shop){
            $shop->type = 'shop';
        }
        $data['users'] = $users;
        $data['shops'] = $shops;
        return view('admin::admin.pages.notifications.send',compact('data'));
    }

    public function sendNotification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
        ],[
            'message.required' => 'من فضلك أدخل الرد',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $user_type = explode('|', $request->person);
        if ($user_type['0'] == 'All_users'){
            $users_token = User::pluck('firebase_token')->toArray();
            $users = User::get();
            foreach ($users as $user){
                Notification::create([
                    'user_id' => $user->id,
                    'title' => 'رسالة من مدير النظام',
                    'body' => $request->message
                ]);
                send_to_user([$user->firebase_token],$request->message ,'10',"",$user->platform);

            }
            AdminNotification::create([
                'message' => $request->message,
                'type' => 0  // All Users
            ]);

        }elseif ($user_type['0'] == 'All_shops'){
            $shops = Shop::get();
            foreach ($shops as $shop){
                Mail::to($shop->email)->send(new ShopMail(['message' => $request->message]));
            }
            AdminNotification::create([
                'message' => $request->message,
                'type' => 1  // All Shops
            ]);
        }else{
            if ($user_type['1'] == 'user'){
                $user = User::where('id',$user_type['0'])->first();
                send_to_user([$user->firebase_token],$request->message ,'10',"",$user->platform);
                Notification::create([
                    'user_id' => $user->id,
                    'title' => 'رسالة من مدير النظام',
                    'body' => $request->message
                ]);
                AdminNotification::create([
                    'user_id' => $user->id,
                    'message' => $request->message,
                    'type' => 2  // User
                ]);
            }elseif ($user_type['1'] == 'shop'){
                $shop = Shop::where('id',$user_type['0'])->first();
                Mail::to($shop->email)->send(new ShopMail(['message' => $request->message]));
                AdminNotification::create([
                    'shop_id' => $shop->id,
                    'message' => $request->message,
                    'type' => 3  // Shop
                ]);
            }
        }
        session()->flash('success', __("تم إرسال الرسالة بنجاح"));
        return redirect()->back();
//        }
//        else{
//            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
//            return redirect()->back();
//        }
    }

    public function delete(Request $request)
    {
//        if (Auth::guard('admin')->user()->can('delete_notifications')){
        $notification_id = $request->notification_id;
        $notification = AdminNotification::where('id',$notification_id)->first();
        if ($notification){
            $notification->delete();
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }else{
            return response()->json(['error' => 'للأسف حدث خطأ ما']);
        }
//        }
//        else{
//            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
//            return redirect()->back();
//        }
    }

    public function destroyMulti(Request $request)
    {
//        if (Auth::guard('admin')->user()->can('delete_notifications')){
        // Init array of IDs
        $ids = $request->ids;
        foreach ($ids as $id) {
            $delete =$this->destroy($id);
            if (!$delete){
                return response()->json(['error' => 'للأسف حدث خطأ ما']);
            }
        }
        return response()->json(['success' => 'تم الحذف بنجاح']);
//        }
//        else{
//            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
//            return redirect()->back();
//        }
    }

    public function destroy($id)
    {
//        if (Auth::guard('admin')->user()->can('delete_notifications')){
        $notification_id = $id;
        $notification = AdminNotification::where('id',$notification_id)->first();
        return $notification->delete();
//        }
//        else{
//            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
//            return redirect()->back();
//        }
    }

}
