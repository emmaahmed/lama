<?php

namespace Modules\Admin\Http\Controllers\Admin_Old;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Order\Entities\Order;
use Modules\Shop\Entities\Shop;

class HomeController extends Controller
{
//    function __construct()
//    {
//        $this->middleware('permission:show_statistics', ['only' => ['index']]);
//    }
    public function index()
    {
        $users = User::count();
        $shops = Shop::count();
        $orders = Order::count();

        $userstatistics=array('7dayago'=>0,'6dayago'=>0,'5dayago'=>0,'4dayago'=>0,'3dayago'=>0,'2dayago'=>0,'1dayago'=>0,'today'=>0);
        $allusers=User::where( 'id' ,'>' ,0)->select('created_at')->get();
        foreach ($allusers  as $oneuser){
            if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(7) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(8)){
                $userstatistics['7dayago'] ++ ;
            }if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(6) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(7)){
                $userstatistics['6dayago'] ++ ;
            }if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(5 ) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(6) ){
                $userstatistics['5dayago'] ++ ;
            } if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(4) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(5) ){
                $userstatistics['4dayago'] ++ ;
            } if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(3) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(4) ){
                $userstatistics['3dayago'] ++ ;
            } if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(2) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(3) ){
                $userstatistics['2dayago'] ++ ;
            }if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(1) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(2) ){
                $userstatistics['1dayago'] ++ ;
            } if ($oneuser->getOriginal('created_at') < Carbon::now() && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(1) ){
                $userstatistics['today'] ++ ;
            }
        }

        $ordersstatistics=array('7dayago'=>0,'6dayago'=>0,'5dayago'=>0,'4dayago'=>0,'3dayago'=>0,'2dayago'=>0,'1dayago'=>0,'today'=>0);
        $allorders = Order::where( 'id' ,'>' ,0)->select('created_at')->get();
        foreach ($allorders  as $order){
            if ($order->getOriginal('created_at') < Carbon::now()->subDays(7) && $order->getOriginal('created_at') > Carbon::now()->subDays(8)){
                $ordersstatistics['7dayago'] ++ ;
            }if ($order->getOriginal('created_at') < Carbon::now()->subDays(6) && $order->getOriginal('created_at') > Carbon::now()->subDays(7)){
                $ordersstatistics['6dayago'] ++ ;
            }if ($order->getOriginal('created_at') < Carbon::now()->subDays(5 ) && $order->getOriginal('created_at') > Carbon::now()->subDays(6) ){
                $ordersstatistics['5dayago'] ++ ;
            } if ($order->getOriginal('created_at') < Carbon::now()->subDays(4) && $order->getOriginal('created_at') > Carbon::now()->subDays(5) ){
                $ordersstatistics['4dayago'] ++ ;
            } if ($order->getOriginal('created_at') < Carbon::now()->subDays(3) && $order->getOriginal('created_at') > Carbon::now()->subDays(4) ){
                $ordersstatistics['3dayago'] ++ ;
            } if ($order->getOriginal('created_at') < Carbon::now()->subDays(2) && $order->getOriginal('created_at') > Carbon::now()->subDays(3) ){
                $ordersstatistics['2dayago'] ++ ;
            }if ($order->getOriginal('created_at') < Carbon::now()->subDays(1) && $order->getOriginal('created_at') > Carbon::now()->subDays(2) ){
                $ordersstatistics['1dayago'] ++ ;
            } if ($order->getOriginal('created_at') < Carbon::now() && $order->getOriginal('created_at') > Carbon::now()->subDays(1) ){
                $ordersstatistics['today'] ++ ;
            }
        }

        $latest_orders = Order::orderBy('id', 'desc')->take(10)->get();
        foreach ($latest_orders as $order){
            if (empty($order->getOriginal('image'))){
                $order->default_image = '/assets/images/default.png';
            }
        }
//
        $data['title'] = "الرئيسية";
        $data['users'] = $users;
        $data['shops'] = $shops;
        $data['orders'] = $orders;
        $data['latest_orders'] = $latest_orders;
        $data['user_statistics'] = $userstatistics;
        $data['order_statistics'] = $ordersstatistics;

        return view('admin::admin.pages.index',compact('data'));
    }
}
