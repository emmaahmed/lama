<?php

namespace Modules\Admin\Http\Controllers\Admin_Old;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Category\Entities\Category;
use Modules\Product\Entities\Product;
use Modules\Shop\Entities\Shop;
use Modules\Shop\Entities\ShopImage;
use function Psy\sh;

class ShopsController extends Controller
{
//    function __construct()
//    {
//        $this->middleware('permission:show_category', ['only' => ['index']]);
//        $this->middleware('permission:add_category', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_category', ['only' => ['edit','update']]);
//        $this->middleware('permission:delete_category', ['only' => ['delete','destroyMulti','destroy']]);
//    }
    public function index()
    {
        $shops = Shop::orderBy('id','DESC')->get();
        $data['shops'] = $shops;
        $data['title'] = "المتاجر";
        return view('admin::admin.pages.shops.index',compact('data'));
    }

    public function changeStatus(Request $request)
    {
        $shop = Shop::where('id',$request->shop_id)->first();
        $shop->status = $request->status;
        if ($shop->save()){
            if ($shop->status == 1){
                return response()->json(['success' => 'تم تفعيل المتجر بنجاح']);
            }
            return response()->json(['success' => 'تم إلغاء تفعيل المتجر بنجاح']);
        }else{
            return response()->json(['error' => 'حدث خطأ ما !']);
        }
    }

    public function create()
    {
        $categories = Category::orderBy('id','DESC')->get();
        $data['title'] = "إضافة متجر جديد";
        $data['categories'] = $categories;
        return view('admin::admin.pages.shops.create',compact('data'));
    }

    public function store(Request $request)
    {
//        dd($request->all());
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|exists:categories,id',
            'name_ar' => 'required',
            'name_en' => 'required',
            'full_name_ar' => 'required',
            'full_name_en' => 'required',
            'description_ar' => 'required',
            'description_en' => 'required',
            'phone' => 'required|unique:shops,phone',
            'email' => 'required|email|unique:shops,email',
            'password' => 'required',
            'tax_number' => 'required',
            'business_registeration' => 'required',
            'logo' => 'required|mimes:jpeg,jpg,png,gif',
            'images' => 'required'
        ],[
            'category_id.required' => 'من فضلك أختر القسم',
            'category_id.exists' => 'القسم غير موجود',
            'name_ar.required' => 'من فضلك أدخل إسم المتجر باللغة العربية',
            'name_en.required' => 'من فضلك أدخل إسم المتجر باللغة الانجليزية',
            'full_name_ar.required' => 'من فضلك أدخل الإسم بالكامل باللغة العربية',
            'full_name_en.required' => 'من فضلك أدخل الإسم بالكامل باللغة الانجليزية',
            'description_ar.required' => 'من فضلك أدخل وصف المتجر باللغة العربية',
            'description_en.required' => 'من فضلك أدخل وصف المتجر باللغة الانجليزية',
            'phone.required' => 'من فضلك أدخل رقم الهاتف',
            'phone.unique' => 'رقم الهاتف موجود من قبل',
            'email.required' => 'من فضلك أدخل البريد الإلكتروني',
            'email.email' => 'البريد الإلكتروني غير صالح',
            'email.unique' => 'البريد الإلكتروني موجود من قبل',
            'password.required' => 'من فضلك أدخل كلمة المرور',
            'tax_number.required' => 'من فضلك أدخل الرقم الضريبي',
            'business_registeration.required' => 'من فضلك قم برفع ملف السجل التجاري',
            'logo.required' => 'من فضلك أدخل الصورة',
            'logo.mimies' => 'من فضلك أختر صورة',
            'images.required' => 'من فضلك أدخل الصورة'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $shop = Shop::create($request->except('images'));

        if ($request->has('images')){
            foreach ($request->images as $image){
                $shop->images()->create([
                    'image' => $image
                ]);
            }
        }
        $shop->translateOrNew('ar')->name = $request->name_ar;
        $shop->translateOrNew('en')->name = $request->name_en;
        $shop->translateOrNew('ar')->full_name = $request->full_name_ar;
        $shop->translateOrNew('en')->full_name = $request->full_name_en;
        $shop->translateOrNew('ar')->description = $request->description_ar;
        $shop->translateOrNew('en')->description = $request->description_en;
        if ($shop->save()){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('admin.shops');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(Request $request)
    {
        $shop = Shop::where('id',$request->shop_id)->first();
        $categories = Category::orderBy('id','DESC')->get();
        $data['title'] = "تعديل المتجر";
        $data['shop'] = $shop;
        $data['categories'] = $categories;
        return view('admin::admin.pages.shops.edit',compact('data'));
    }

    public function update(Request $request)
    {
//        dd($request->all());
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|exists:categories,id',
            'name_ar' => 'required',
            'name_en' => 'required',
            'full_name_ar' => 'required',
            'full_name_en' => 'required',
            'description_ar' => 'required',
            'delivery_time' => 'required',
            'delivery_fees' => 'required',
            'description_en' => 'required',
            'phone' => 'required|unique:shops,phone,'.$request->shop_id,
            'email' => 'required|email|unique:shops,email,'.$request->shop_id,
            'tax_number' => 'required',
        ],[
            'category_id.required' => 'من فضلك أختر القسم',
            'category_id.exists' => 'القسم غير موجود',
            'name_ar.required' => 'من فضلك أدخل إسم المتجر باللغة العربية',
            'name_en.required' => 'من فضلك أدخل إسم المتجر باللغة الانجليزية',
            'full_name_ar.required' => 'من فضلك أدخل الإسم بالكامل باللغة العربية',
            'full_name_en.required' => 'من فضلك أدخل الإسم بالكامل باللغة الانجليزية',
            'description_ar.required' => 'من فضلك أدخل وصف المتجر باللغة العربية',
            'delivery_time.required' => 'من فضلك أدخل زمن توصيل الطلب',
            'delivery_fees.required' => 'من فضلك أدخل مصاريف التوصيل',
            'description_en.required' => 'من فضلك أدخل وصف المتجر باللغة الانجليزية',
            'phone.required' => 'من فضلك أدخل رقم الهاتف',
            'phone.unique' => 'رقم الهاتف موجود من قبل',
            'email.required' => 'من فضلك أدخل البريد الإلكتروني',
            'email.email' => 'البريد الإلكتروني غير صالح',
            'email.unique' => 'البريد الإلكتروني موجود من قبل',
            'tax_number.required' => 'من فضلك أدخل الرقم الضريبي',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $shop = Shop::where('id',$request->shop_id)->first();
        if (isset($shop)){
            $shop->update($request->except('images'));
            if ($request->has('images')){
                foreach ($request->images as $image){
                    $shop->images()->create([
                        'image' => $image
                    ]);
                }
            }
            $shop->translateOrNew('ar')->name = $request->name_ar;
            $shop->translateOrNew('en')->name = $request->name_en;
            $shop->translateOrNew('ar')->full_name = $request->full_name_ar;
            $shop->translateOrNew('en')->full_name = $request->full_name_en;
            $shop->translateOrNew('ar')->description = $request->description_ar;
            $shop->translateOrNew('en')->description = $request->description_en;
            if ($shop->save()){
                session()->flash('success', 'تم التعديل بنجاح');
                return redirect()->route('admin.shops');
            }
        }

        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function shopRemoveImages($id)
    {
        $shop_image = ShopImage::findOrFail($id);
        unlinkFile($shop_image->getOriginal('image'),'shops/sliders');
        if ($shop_image->delete()){
            return back()->withSuccess('تم حذف الصورة بنجاح');
        }
    }

    public function delete(Request $request)
    {
        $shop = Shop::where('id',$request->shop_id)->first();
        if (!empty($shop->getOriginal('logo'))){
            unlinkFile($shop->getOriginal('logo'), 'shops');
        }
        if (!empty($shop->getOriginal('business_registeration'))){
            unlinkFile($shop->getOriginal('business_registeration'), 'shops/registerations');
        }
        if (!empty($shop->images)){
            foreach ($shop->images as $shop_image){
                unlinkFile($shop_image->getOriginal('image'), 'shops/sliders');
                $shop_image->delete();
            }
        }
        if ($shop->delete()){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(Request $request)
    {
        // Init array of IDs
        $ids = [];
        foreach ($request->ids as $id){
            if ($id != 'on'){
                array_push($ids,$id);
            }
        }
        foreach ($ids as $id) {
            $delete =$this->destroy($id);
            if (!$delete){
                return response()->json(['error' => 'للأسف حدث خطأ ما']);
            }
        }
        return response()->json(['success' => 'تم الحذف بنجاح']);
    }

    public function destroy($id)
    {
//        if (Auth::guard('admin')->user()->can('delete_user')){
        $shop_id = $id;
        $shop = Shop::where('id',$shop_id)->first();
        if (!empty($shop->getOriginal('logo'))){
            unlinkFile($shop->getOriginal('logo'), 'shops');
        }
        if (!empty($shop->getOriginal('business_registeration'))){
            unlinkFile($shop->getOriginal('business_registeration'), 'shops/registerations');
        }
        if (!empty($shop->images)){
            foreach ($shop->images as $shop_image){
                unlinkFile($shop_image->getOriginal('image'), 'shops/sliders');
                $shop_image->delete();
            }
        }
        return $shop->delete();
//        }
//        else{
//            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
//            return redirect()->back();
//        }
    }

    // Shop Products

    public function showProducts(Request $request)
    {
        $shop = Shop::where('id',$request->shop)->first();

        $data['title'] = 'تفاصيل المتجر';
        $data['shop'] = $shop;

        return view('admin::admin.pages.shops.show',compact('data'));

    }

    public function changeProductStatus(Request $request)
    {
        $product = Product::where('id',$request->product_id)->first();
        $product->status = $request->status;
        if ($product->save()){
            if ($product->status == 1){
                return response()->json(['success' => 'تم تفعيل المنتج بنجاح']);
            }
            return response()->json(['success' => 'تم إلغاء تفعيل المنتج بنجاح']);
        }else{
            return response()->json(['error' => 'حدث خطأ ما !']);
        }
    }

    public function deleteProduct(Request $request)
    {
        $product = Product::where('id',$request->product_id)->first();
        if (!empty($product->getOriginal('image'))){
            unlinkFile($product->getOriginal('image'), 'products');
        }
        if ($product->delete()){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMultiProducts(Request $request)
    {
        // Init array of IDs
        $ids = [];
        foreach ($request->ids as $id){
            if ($id != 'on'){
                array_push($ids,$id);
            }
        }
        foreach ($ids as $id) {
            $delete =$this->destroyProduct($id);
            if (!$delete){
                return response()->json(['error' => 'للأسف حدث خطأ ما']);
            }
        }
        return response()->json(['success' => 'تم الحذف بنجاح']);
    }

    public function destroyProduct($id)
    {
        $product_id = $id;
        $product = Product::where('id',$product_id)->first();
        if (!empty($product->getOriginal('image'))){
            unlinkFile($product->getOriginal('image'), 'products');
        }
        return $product->delete();
    }
}
