<?php

namespace Modules\Admin\Http\Controllers\Admin_Old;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Entities\Admin;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;

class AdminsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_admin', ['only' => ['index','show']]);
        $this->middleware('permission:add_admin', ['only' => ['create','store']]);
        $this->middleware('permission:edit_admin', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_admin', ['only' => ['delete']]);
    }

    public function index()
    {
        $auth = Auth::guard('admin')->user()->id;
        $admins = Admin::where('id','!=',1)->where('id','!=',$auth)->get();
            $data['admins'] = $admins;
            $data['title'] = "المشرفين";
            return view('admin::admin.pages.admins.index',compact('data'));
    }

    public function create()
    {
            $permissions = Permission::get();
            $data['title'] = "إضافة مشرف";
            $data['permissions'] = $permissions;
            $data['all_permissions'] = [
                'المشرفين' => [$permissions[0],$permissions[1],$permissions[2],$permissions[3]],
                'المستخدمين' => [$permissions[4],$permissions[5],$permissions[6],$permissions[7]],
                'المتاجر' => [$permissions[8],$permissions[9],$permissions[10],$permissions[11],$permissions[12]],
                'الأقسام' => [$permissions[13],$permissions[14],$permissions[15],$permissions[16]],
                'الأقسام الفرعية' => [$permissions[17],$permissions[18],$permissions[19],$permissions[20]],
                'المنتجات' => [$permissions[21],$permissions[22],$permissions[23],$permissions[24]],
                'الطلبات' => [$permissions[25],$permissions[26],$permissions[27],$permissions[28]],
                'الإشعارات' => [$permissions[29],$permissions[30]],
                'التقارير' => [$permissions[33],$permissions[34]],
                'الإحصائيات' => [$permissions[31]],
                'الإعدادات' => [$permissions[32]],
            ];
            $data['models'] = ['المشرفين','المستخدمين','المتاجر','الأقسام','الفرعية','المنتجات','الطلبات','الإشعارات','التقارير','الإحصائيات','الإعدادات'];

            return view('admin::admin.pages.admins.add',compact('data'));
    }

    public function store(Request $request)
    {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required|min:6'
            ],[
                'name.required' => 'من فضلك أدخل إسم المستخدم',
                'email.required' => 'من فضلك أدخل البريد الإلكتروني',
                'email.email' => 'من فضلك أدخل بريد إلكتروني صالح',
                'password.required' => 'من فضلك أدخل كلمة المرور',
                'password.min' => 'من فضلك أدخل كلمة المرور أكثر من 3 حروف'
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }
            $admin = new Admin();
            if ($request->has('name') && $request->name != null){
                $admin->name = $request->name;
            }if ($request->has('email') && $request->email != null){
                $admin->email = $request->email;
            }if ($request->has('password') && $request->password != null){
                $admin->password = bcrypt($request->password);
            }if ($request->has('permissions')){
                $admin->givePermissionTo($request->permissions);
            }
            if ($admin->save()){
                if ($request->has('image')){
                    $imageField = upload($request->image, 'admins');
                    $admin->image = $imageField;
                    $admin->save();
                }
                session()->flash('success', 'تم الإضافة بنجاح');
                return redirect()->route('admin.admins');
            }
            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
            return redirect()->back();
    }

    public function edit(Request $request)
    {
            $admin = Admin::find($request->admin_id);
            $permissions = Permission::get();
            $data['title'] = "تعديل مشرف";
            $data['admin'] = $admin;
            $data['permissions'] = $permissions;
            $data['all_permissions'] = [
                'المشرفين' => [$permissions[0],$permissions[1],$permissions[2],$permissions[3]],
                'المستخدمين' => [$permissions[4],$permissions[5],$permissions[6],$permissions[7]],
                'المتاجر' => [$permissions[8],$permissions[9],$permissions[10],$permissions[11],$permissions[12]],
                'الأقسام' => [$permissions[13],$permissions[14],$permissions[15],$permissions[16]],
                'الأقسام الفرعية' => [$permissions[17],$permissions[18],$permissions[19],$permissions[20]],
                'المنتجات' => [$permissions[21],$permissions[22],$permissions[23],$permissions[24]],
                'الطلبات' => [$permissions[25],$permissions[26],$permissions[27],$permissions[28]],
                'الإشعارات' => [$permissions[29],$permissions[30]],
                'التقارير' => [$permissions[33],$permissions[34]],
                'الإحصائيات' => [$permissions[31]],
                'الإعدادات' => [$permissions[32]],
            ];

            return view('admin::admin.pages.admins.edit',compact('data'));
    }

    public function update(Request $request)
    {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email'
            ],[
                'name.required' => 'من فضلك أدخل إسم المستخدم',
                'email.required' => 'من فضلك أدخل البريد الإلكتروني',
                'email.email' => 'من فضلك أدخل بريد إلكتروني صالح'
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }
            $admin_id = $request->admin_id;
            $admin = Admin::where('id',$admin_id)->first();
            if ($request->has('name') && $request->name != null){
                $admin->name = $request->name;
            }if ($request->has('email') && $request->email != null){
                $admin->email = $request->email;
            }if ($request->has('password') && $request->password != null){
                $admin->password = bcrypt($request->password);
            }if ($request->has('status') && $request->status != null){
                $admin->status = $request->status;
            }else{
                $admin->status = 0;
            }
            $permissions = Permission::pluck('name')->toArray();
            $admin->revokePermissionTo($permissions);
            if ($request->has('permissions')){
                $admin->givePermissionTo($request->permissions);
            }
            if ($admin->save()){
                if ($request->has('image')){
                    if (!empty($admin->getOriginal('image'))){
                        unlinkFile($admin->getOriginal('image'), 'admins');
                    }
                    $imageField = upload($request->image, 'admins');
                    $admin->image = $imageField;
                }
                $admin->save();
                session()->flash('success', 'تم التعديل بنجاح');
                return redirect()->route('admin.admins');
            }
            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
            return redirect()->back();
    }

    public function show(Request $request)
    {
            $admin_id = $request->admin_id;
            $admin = Admin::where('id', $admin_id)->first();
            $admin = $admin->load('image');
            $title = "تفاصيل المشرف";

            return view('admin::admin.pages.admins.show',compact('admin','title'));
    }

    public function delete(Request $request)
    {
            $admin_id = $request->admin_id;
            $admin = Admin::where('id',$admin_id)->first();
            if (isset($admin)) {
                if ($admin->delete()) {
                    if (!empty($admin->image)) {
                        unlinkFile($admin->image, 'admins');
                    }
                    return response()->json(['success' => 'تم الحذف بنجاح']);
                } else {
                    return response()->json(['error' => 'للأسف حدث خطأ ما']);
                }
            }else{
                return response()->json(['error' => 'للأسف حدث خطأ ما']);
            }
    }
}
