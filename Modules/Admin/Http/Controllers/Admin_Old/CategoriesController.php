<?php

namespace Modules\Admin\Http\Controllers\Admin_Old;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Category\Entities\Category;

class CategoriesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_category', ['only' => ['index']]);
        $this->middleware('permission:add_category', ['only' => ['create','store']]);
        $this->middleware('permission:edit_category', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_category', ['only' => ['delete','destroyMulti','destroy']]);
    }
    public function index()
    {
        $categories = Category::orderBy('id','DESC')->get();
        $data['categories'] = $categories;
        $data['title'] = "الأقسام الرئيسية";
        return view('admin::admin.pages.categories.index',compact('data'));
    }

    public function create()
    {
        $data['title'] = "إضافة قسم جديد";
        return view('admin::admin.pages.categories.create',compact('data'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name_ar' => 'required',
            'name_en' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png,gif'
        ],[
            'name_ar.required' => 'من فضلك أدخل إسم القسم باللغة العربية',
            'name_en.required' => 'من فضلك أدخل إسم القسم باللغة الانجليزية',
            'image.required' => 'من فضلك أدخل الصورة',
            'image.mimies' => 'من فضلك أختر صورة',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $category = new Category();
        if ($request->has('image')){
            $imageField = upload($request->image, 'categories');
            $category->image = $imageField;
        }
        $category->save();
        $category->translateOrNew('ar')->name = $request->name_ar;
        $category->translateOrNew('en')->name = $request->name_en;
        if ($category->save()){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('admin.categories');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(Request $request)
    {
        $category = Category::where('id',$request->category_id)->first();
        $data['title'] = "تعديل القسم";
        $data['category'] = $category;
        return view('admin::admin.pages.categories.edit',compact('data'));
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name_ar' => 'required',
            'name_en' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif'
        ],[
            'name_ar.required' => 'من فضلك أدخل إسم القسم باللغة العربية',
            'name_en.required' => 'من فضلك أدخل إسم القسم باللغة الإنجليزية',
            'image.mimies' => 'من فضلك أختر صورة',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $category = Category::where('id',$request->category_id)->first();
        if (isset($category)){
            $category->translateOrNew('ar')->name = $request->name_ar;
            $category->translateOrNew('en')->name = $request->name_en;
            if ($category->save()){
                if ($request->has('image')){
                    if (!empty($category->getOriginal('image'))){
                        unlinkFile($category->getOriginal('image'), 'categories');
                    }
                    $imageField = upload($request->image, 'categories');
                    $category->image = $imageField;
                    $category->save();
                }
                session()->flash('success', 'تم التعديل بنجاح');
                return redirect()->route('admin.categories');
            }
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $category = Category::where('id',$request->category_id)->first();
        if (!empty($category->getOriginal('image'))){
            unlinkFile($category->getOriginal('image'), 'categories');
        }
        if ($category->delete()){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(Request $request)
    {
        // Init array of IDs
        $ids = $request->ids;
        foreach ($ids as $id) {
            $delete =$this->destroy($id);
            if (!$delete){
                return response()->json(['error' => 'للأسف حدث خطأ ما']);
            }
        }
        return response()->json(['success' => 'تم الحذف بنجاح']);
    }

    public function destroy($id)
    {
        $category_id = $id;
        $category = Category::where('id',$category_id)->first();
        if (!empty($category->image)){
            unlinkFile($category->image, 'categories');
        }
        return $category->delete();
    }
}
