<?php

namespace Modules\Admin\Http\Controllers\Admin_Old;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Category\Entities\Category;
use Modules\Shop\Entities\SubCategory;

class SubCategoriesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_sub_category', ['only' => ['index']]);
        $this->middleware('permission:add_sub_category', ['only' => ['store']]);
        $this->middleware('permission:edit_sub_category', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_sub_category', ['only' => ['delete','destroyMulti','destroy']]);
    }
    public function index(Request $request)
    {
        $sub_categories = SubCategory::where('shop_id',$request->shop)->orderBy('id','DESC')->get();
        $data['subCategories'] = $sub_categories;
        $data['shop_id'] = $request->shop;
        $data['title'] = "أقسام المتجر";
        return view('admin::admin.pages.shops.subCategories.index',compact('data'));
    }

//    public function create()
//    {
//        $data['title'] = "إضافة قسم جديد";
//        return view('admin::admin.pages.categories.create',compact('data'));
//    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name_ar' => 'required',
            'name_en' => 'required'
        ],[
            'name_ar.required' => 'من فضلك أدخل إسم القسم باللغة العربية',
            'name_en.required' => 'من فضلك أدخل إسم القسم باللغة الانجليزية'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $subCategory = SubCategory::updateOrCreate(
            ['id' => $request->subCategory_id],
            ['shop_id' => $request->shop_id]
        );
        $subCategory->translateOrNew('ar')->name = $request->name_ar;
        $subCategory->translateOrNew('en')->name = $request->name_en;
        if ($subCategory->save()){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->back();
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit($id)
    {
        $where = array('id' => $id);
        $subCategory  = SubCategory::where($where)->first();
        $subCategory->name_ar = $subCategory->translate('ar')->name;
        $subCategory->name_en = $subCategory->translate('en')->name;
        return response()->json($subCategory);
    }

    public function delete(Request $request)
    {
        $subCategory = SubCategory::where('id',$request->subCategory_id)->first();
        if ($subCategory->delete()){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(Request $request)
    {
        // Init array of IDs
        $ids = $request->ids;
        foreach ($ids as $id) {
            $delete =$this->destroy($id);
            if (!$delete){
                return response()->json(['error' => 'للأسف حدث خطأ ما']);
            }
        }
        return response()->json(['success' => 'تم الحذف بنجاح']);
    }

    public function destroy($id)
    {
        $subCategory_id = $id;
        $subCategory = SubCategory::where('id',$subCategory_id)->first();
        return $subCategory->delete();
    }
}
