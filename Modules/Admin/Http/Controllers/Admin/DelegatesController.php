<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\UserRequest;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Admin\Repositories\Interfaces\UserRepositoryInterface;

class DelegatesController extends Controller
{
    //    function __construct()
//    {
//        $this->middleware('permission:show_user', ['only' => ['index','ordersByUser']]);
//        $this->middleware('permission:edit_user', ['only' => ['changeStatus','edit','update']]);
//        $this->middleware('permission:delete_user', ['only' => ['delete','destroyMulti','destroy']]);
//    }

    protected $userRepository, $indexRepository;

    public function __construct(UserRepositoryInterface $userRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->userRepository = $userRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_delegate', ['only' => ['index']]);
//        $this->middleware('permission:add_delegate', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_delegate', ['only' => ['changeStatus','edit','update']]);
//        $this->middleware('permission:delete_delegate', ['only' => ['delete','destroyMulti']]);
    }

    public function index(Request $request)
    {
        $data = $this->userRepository->index($request);
        return view('admin::admin.pages.delegates.index',compact('data'));
    }
    public function indexNotActive(Request $request)
    {
        $data = $this->userRepository->indexNotActive($request);
        return view('admin::admin.pages.delegates.index',compact('data'));
    }

    public function changeStatus(UserRequest $request)
    {
        $result = $this->userRepository->changeStatus($request);
        if ($result){
            return $result == 'user_activated' ? response()->json(['success' => 'تم تفعيل المندوب بنجاح']) : response()->json(['success' => 'تم إلغاء تفعيل المندوب بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function create()
    {
        $data['title'] = 'إضافة مندوب';
        return view('admin::admin.pages.delegates.create',compact('data'));
    }

    public function store(UserRequest $request)
    {
//        dd($request->validated());
        if($request->status){
            $request['status']=1;
        }
        else{
            $request['status']=0;
        }
        $created = $this->userRepository->store($request);
        if ($created){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('admin.delegates',['type' => 1]);
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(UserRequest $request)
    {
        $data = $this->userRepository->edit($request);
        return view('admin::admin.pages.delegates.edit',compact('data'));
    }

    public function update(UserRequest $request)
    {
        $updated = $this->userRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->route('admin.delegates',['type' => 1]);
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(UserRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('user', $request->user_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(UserRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('user', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
