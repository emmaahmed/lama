<?php

namespace Modules\Admin\Http\Controllers\Admin;

use App\Repositories\Interfaces\SocialMediaRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\SocialRequest;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;

class SocialController extends Controller
{
    protected $socialMediaRepository, $indexRepository;

    public function __construct(SocialMediaRepositoryInterface $socialMediaRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->socialMediaRepository = $socialMediaRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_social', ['only' => ['index']]);
//        $this->middleware('permission:add_social', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_social', ['only' => ['edit','update']]);
//        $this->middleware('permission:delete_social', ['only' => ['delete','destroyMulti']]);
    }

    public function index()
    {
        $socials = $this->socialMediaRepository->getSocial();
        $data['socials'] = $socials;
        $data['title'] = "وسائل التواصل";
        return view('admin::admin.pages.settings.socials.index',compact('data'));
    }

    public function create()
    {
        $data['title'] = "إضافة وسيلة تواصل";
        return view('admin::admin.pages.settings.socials.create',compact('data'));
    }

    public function store(SocialRequest $request)
    {
        $created = $this->socialMediaRepository->store($request);
        if ($created){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('admin.settings.socials');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(SocialRequest $request)
    {
        $data = $this->socialMediaRepository->edit($request);

        return view('admin::admin.pages.settings.socials.edit',compact('data'));
    }

    public function update(SocialRequest $request)
    {
        $updated = $this->socialMediaRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->route('admin.settings.socials');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(SocialRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('socialMedia', $request->social_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(SocialRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('socialMedia', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
