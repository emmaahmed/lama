<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\UserRequest;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Admin\Repositories\Interfaces\UserRepositoryInterface;

class UsersController extends Controller
{
    //    function __construct()
//    {
//        $this->middleware('permission:show_user', ['only' => ['index','ordersByUser']]);
//        $this->middleware('permission:edit_user', ['only' => ['changeStatus','edit','update']]);
//        $this->middleware('permission:delete_user', ['only' => ['delete','destroyMulti','destroy']]);
//    }

    protected $userRepository, $indexRepository;

    public function __construct(UserRepositoryInterface $userRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->userRepository = $userRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_user', ['only' => ['index']]);
//        $this->middleware('permission:edit_user', ['only' => ['changeStatus','edit','update']]);
//        $this->middleware('permission:delete_user', ['only' => ['delete','destroyMulti']]);
//        $this->middleware('permission:export_user', ['only' => ['delete','destroyMulti','destroy']]);
    }

    public function index(Request $request)
    {
        $data = $this->userRepository->index($request);
        return view('admin::admin.pages.users.index',compact('data'));
    }

    public function changeStatus(UserRequest $request)
    {
        $result = $this->userRepository->changeStatus($request);
        if ($result){
            return $result == 'user_activated' ? response()->json(['success' => 'تم تفعيل المستخدم بنجاح']) : response()->json(['success' => 'تم إلغاء تفعيل المستخدم بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function edit(UserRequest $request)
    {
        $data = $this->userRepository->edit($request);
        return view('admin::admin.pages.users.edit',compact('data'));
    }

    public function update(UserRequest $request)
    {
        $updated = $this->userRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->route('admin.users',['type' => 0]);
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(UserRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('user', $request->user_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(UserRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('user', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
