<?php

namespace Modules\Admin\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Admin\Entities\AppExplanation;
use DemeterChain\B;
use Illuminate\Http\Request;
use DB;
use Route;
use Session;


class AppExplanationController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(){
        $results = AppExplanation::orderBy('id','desc')
            ->get();
        $data=[];
        $data['title'] ='الأسئلة الشائعة';
        return view('admin::admin.pages.app_explantions.index',compact('results','data'));
        //return view('cp.app_explantions.index',['results'=>$results,]);

    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required',
        ]);
        $add            = new AppExplanation();
        $add->title   = $request->title;
        $add->body   = $request->body;
        $add->save();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','تمت العملية بنجاح');
    }


    public function edit_explain(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required',
        ]);

        $c=AppExplanation::where('id', $request->explain_id)->first();
        $c->update($request->all());

        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','تمت العملية بنجاح');
    }

    public function delete_explain(Request $request,$id)
    {
        AppExplanation::destroy($id);
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','تمت العملية بنجاح');
    }

}
