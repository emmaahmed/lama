<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Order\Http\Requests\OrderRequest;
use Modules\Order\Repositories\Interfaces\OrderRepositoryInterface;
include_once(app_path() . '/WebClientPrint/WebClientPrint.php');
use Neodynamic\SDK\Web\WebClientPrint;
use Illuminate\Support\Facades\Session;

class OrdersController extends Controller
{
    protected $orderRepository,$indexRepository;

    public function __construct(OrderRepositoryInterface $orderRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->indexRepository = $indexRepository;

//        $this->middleware('permission:show_order', ['only' => ['index','changeStatus','filterOrders']]);
//        $this->middleware('permission:show_product', ['only' => ['showProducts']]);
//        $this->middleware('permission:delete_order', ['only' => ['delete','destroyMulti']]);
//        $this->middleware('permission:export_order', ['only' => ['']]);
    }

    public function index()
    {
        $data = $this->orderRepository->getOrders();
        $wcpScript = WebClientPrint::createScript(route('print.process'), route('print.commands'), Session::getId());

        return view('admin::admin.pages.orders.index',compact('data','wcpScript'));
    }

    public function finishedOrders()
    {
        $data = $this->orderRepository->finishedOrders();
        return view('admin::admin.pages.orders.index',compact('data'));
    }
    public function unfinishedOrders()
    {
        $data = $this->orderRepository->unfinishedOrders();
        return view('admin::admin.pages.orders.index',compact('data'));
    }

    public function changeStatus(OrderRequest $request)
    {
        $changed = $this->orderRepository->changeStatus($request);
        if ($changed){
            return response()->json(['success' => 'تم تعديل حالة الطلب بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function filterOrders(Request $request)
    {
               if (isset($request->from) && isset($request->to)) {

        $request['from'] = date('Y-m-d',strtotime($request->from));
        $request['to'] = date('Y-m-d',strtotime($request->to));
               }
        $data = $this->orderRepository->filterOrders($request);
        return View::make('admin::admin.pages.orders.div',compact('data'));
    }

    public function showProducts(OrderRequest $request)
    {
        $data = $this->orderRepository->showProducts($request);
        return view('admin::admin.pages.orders.show',compact('data'));
    }

    public function printInvoice(Request $request)
    {
        $data = $this->orderRepository->showProducts($request);
        return view('admin::admin.pages.orders.print',compact('data'));
    }

    public function delete(OrderRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('order', $request->order_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(OrderRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('order', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function editRate(Request $request)
    {
        $edited = $this->orderRepository->editRate($request);
        if ($edited){
            session()->flash('success', 'تم التعديل بنجاح');
            return back()->with('تم التعديل بنجاح');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return back()->with('حدث خطأ ما !');
    }
}
