<?php

namespace Modules\Admin\Http\Controllers\Admin;
use App\DelegateRequest;
use App\ShopRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Repositories\Interfaces\AdminRepositoryInterface;
use Modules\Order\Entities\Order;
use Modules\Shop\Entities\Shop;

class HomeController extends Controller
{
    protected $adminRepository;

    public function __construct(AdminRepositoryInterface $adminRepository)
    {
        $this->adminRepository = $adminRepository;
//        $this->middleware('permission:show_statistics', ['only' => ['index']]);
    }

    public function index()
    {
        $data = $this->adminRepository->statistics();
        return view('admin::admin.pages.index',compact('data'));
    }

    public function shopRequests()
    {
        $data['title'] = "طلبات المتاجر للانضمام";
        $data['shops'] = ShopRequest::orderBy('id','desc')->where('status',0)->where('shop_info',1)->get();
        return view('admin::admin.pages.requests.shopRequests',compact('data'));
    }
    public function deleteShopRequest(Request $request){
        $request=ShopRequest::whereId($request->id)->first();
        $request->delete();
        return response()->json(['success' => 'تم الحذف بنجاح']);


    }
    public function deleteDelegateRequest(Request $request){
        $request=DelegateRequest::whereId($request->id)->first();
        $request->delete();
        return response()->json(['success' => 'تم الحذف بنجاح']);


    }
    public function shopActivated()
    {
        $data['title'] = "طلبات المتاجر للانضمام";
        $data['shops'] = ShopRequest::orderBy('id','desc')->where('shop_info',1)->where('status',0)->get();
        return view('admin::admin.pages.requests.shopRequests',compact('data'));
    }

    public function delegateRequests()
    {
        $data['title'] = "طلبات المناديب للانضمام";
        $data['shops'] = DelegateRequest::orderBy('id','desc')->where('status',0)->get();
        return view('admin::admin.pages.requests.delegateRequests',compact('data'));
    }
    public function delegateActivated()
    {
        $data['title'] = "طلبات المناديب المفعلة";
        $data['shops'] = DelegateRequest::orderBy('id','desc')->where('status',1)->get();
        return view('admin::admin.pages.requests.delegateRequests',compact('data'));
    }


    public function shopChangeStatus(Request $request)
    {
        $shopRequest = ShopRequest::where('id',$request->shop_id)->first();
        $shop=Shop::create(
            [
                'jwt'=>$shopRequest->jwt,
                'category_id'=>$shopRequest->category_id,
                'city_id'=>$shopRequest->city_id,
                'area'=>$shopRequest->area,
                'shop_owner_name'=>$shopRequest->shop_owner_name,
                'email'=>$shopRequest->email,
                'phone'=>$shopRequest->phone,
                'status'=>1,
                'type'=>0,
                'latitude'=>$shopRequest->latitude,
                'longitude'=>$shopRequest->longitude,
                'delivery_types'=>$shopRequest->getOriginal('delivery_types'),
                'payment'=>$shopRequest->getOriginal('payment'),
                'license'=>$shopRequest->license,
                'commercial_register'=>$shopRequest->commercial_register,
                'cover'=>$shopRequest->cover,
                'from'=>$shopRequest->from,
                'to'=>$shopRequest->to,
                'min_order'=>$shopRequest->min_order,
                'delivery_time'=>$shopRequest->delivery_time,
                'delivery_fees'=>$shopRequest->delivery_fees,
                'logo'=>$shopRequest->getOriginal('logo'),
                'expire_at'=>Carbon::parse($shopRequest->created_at)->addMonth(),

            ]
        );
        $shop->preventAttrSet=true;
        $shop->password=$shopRequest->password;


        $shop->translateOrNew('ar')->name = $shopRequest->shop_name;
        $shop->translateOrNew('en')->name = $shopRequest->shop_name;
//        $shop->translateOrNew('en')->name = $request->name_en;
        $shop->translateOrNew('ar')->short_description = $shopRequest->short_description;
        $shop->translateOrNew('en')->short_description = $shopRequest->short_description;
//        $shop->translateOrNew('en')->short_description = $request->short_description_en;
        $shop->translateOrNew('ar')->long_description = $shopRequest->long_description;
        $shop->translateOrNew('en')->long_description = $shopRequest->long_description;
//        $shop->translateOrNew('en')->long_description = $request->long_description_en;

        if ($shop->save()){
            $shop->assignRole('shop1');
        }
        $shopRequest->status = $request->status;
        if ($shopRequest->save()){
            if ($shopRequest->status == 1){
                return 'activated';
            }
            return 'deactivated';
        }else{
            $status = false;
        }
        if ($status){
            if ($status == 'activated'){
                return response()->json(['success' => 'تم تفعيل المتجر بنجاح']);
            }
            return response()->json(['success' => 'تم إلغاء تفعيل المتجر بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function delegateChangeStatus(Request $request)
    {
        $shop = DelegateRequest::where('id',$request->shop_id)->first();
        $shop->status = $request->status;
        if ($shop->save()){
            if ($shop->status == 1){
                return 'activated';
            }
            return 'deactivated';
        }else{
            $status = false;
        }
        if ($status){
            if ($status == 'activated'){
                return response()->json(['success' => 'تم تفعيل المتجر بنجاح']);
            }
            return response()->json(['success' => 'تم إلغاء تفعيل المتجر بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
