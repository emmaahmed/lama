<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Category\Http\Requests\CategoryRequest;
use Modules\Category\Repositories\Interfaces\CategoryRepositoryInterface;

class CategoriesController extends Controller
{

    protected $categoryRepository, $indexRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_category', ['only' => ['index']]);
//        $this->middleware('permission:add_category', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_category', ['only' => ['edit','update']]);
//        $this->middleware('permission:delete_category', ['only' => ['delete','destroyMulti']]);
    }

    public function index()
    {
        $categories = $this->categoryRepository->getCategories();
        $data['categories'] = $categories;
        $data['title'] = "الأقسام الرئيسية";
        return view('admin::admin.pages.categories.index',compact('data'));
    }

    public function create()
    {
        $data['title'] = "إضافة قسم جديد";
        return view('admin::admin.pages.categories.create',compact('data'));
    }

    public function store(CategoryRequest $request)
    {
        $created = $this->categoryRepository->store($request);
        if ($created){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('admin.categories');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(CategoryRequest $request)
    {
        $data = $this->categoryRepository->edit($request);

        return view('admin::admin.pages.categories.edit',compact('data'));
    }

    public function update(CategoryRequest $request)
    {
        $updated = $this->categoryRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->route('admin.categories');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function changeStatus(Request $request)
    {
        $status = $this->categoryRepository->changeStatus($request);
        if ($status){
            if ($status == 'activated'){
                return response()->json(['success' => 'تم تفعيل القسم بنجاح']);
            }
            return response()->json(['success' => 'تم إلغاء تفعيل القسم بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function delete(CategoryRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('category', $request->category_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(CategoryRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('category', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
