<?php

namespace Modules\Admin\Http\Controllers\Admin;

use App\Repositories\Interfaces\TutorialRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\TutorialsRequest;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;

class TutorialsController extends Controller
{
    protected $tutorialRepository, $indexRepository;

    public function __construct(TutorialRepositoryInterface $tutorialRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->tutorialRepository = $tutorialRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_tutorial', ['only' => ['index']]);
//        $this->middleware('permission:add_tutorial', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_tutorial', ['only' => ['edit','update']]);
//        $this->middleware('permission:delete_tutorial', ['only' => ['delete','destroyMulti']]);
    }

    public function index()
    {
        $tutorials = $this->tutorialRepository->tutorials();
        $data['tutorials'] = $tutorials;
        $data['title'] = "الصفحة التعريفية";
        return view('admin::admin.pages.settings.tutorials.index',compact('data'));
    }

    public function create()
    {
        $data['title'] = "إضافة صفحة تعريفية";
        return view('admin::admin.pages.settings.tutorials.create',compact('data'));
    }

    public function store(TutorialsRequest $request)
    {
        $created = $this->tutorialRepository->store($request);
        if ($created){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('admin.settings.tutorials');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(TutorialsRequest $request)
    {
        $data = $this->tutorialRepository->edit($request);

        return view('admin::admin.pages.settings.tutorials.edit',compact('data'));
    }

    public function update(TutorialsRequest $request)
    {
        $updated = $this->tutorialRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->route('admin.settings.tutorials');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(TutorialsRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('tutorial', $request->tutorial_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(TutorialsRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('tutorial', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
