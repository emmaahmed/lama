<?php

namespace Modules\Admin\Http\Controllers\Admin;

use App\Exports\AdminReportsExport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderItem;
use Modules\Shop\Entities\Shop;

class ReportsController extends Controller
{
    function __construct()
    {
//        $this->middleware('permission:show_reports', ['only' => ['index']]);
//        $this->middleware('permission:export_reports', ['only' => ['Reports']]);
    }

    public function index()
    {
        $data['title'] = 'تقارير المبيعات';
        return View::make('admin::admin.pages.reports.index',compact('data'));
    }

    public function Reports(Request $request)
    {
        $builder = Order::whereNotNull('id');
        if ($request->type == 'daily'){
            $builder->whereDate('created_at',date('Y-m-d'));

        }
        if ($request->type == 'weekly'){
            $builder->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
        }
        if ($request->type == 'monthly'){
            $builder->whereMonth('created_at',date('m'));
        }
        if ($request->type == 'annually'){
            $builder->whereYear('created_at',date('Y'));
        }

        $orders = $builder->orderBy('id','DESC')->paginate(10);

        $shops = Shop::get();
        $all_cost = 0;
        foreach ($orders as $order){
            $selling_report = [];
            $total_cost = 0;
            foreach ($order->orderItems as $order_item){
                if (array_key_exists($order_item->product_id,$selling_report)){
                    $selling_report[$order_item->product_id]['quantity'] += $order_item->product->selling;
                    $selling_report[$order_item->product_id]['cost'] += $order_item->cost;
                }else{
                    $selling_report[$order_item->product_id]['product_name'] = $order_item->product->name;
                    $selling_report[$order_item->product_id]['quantity'] = $order_item->product->selling;
                    $selling_report[$order_item->product_id]['cost'] = $order_item->cost;
                }
                $order_items = OrderItem::where('product_id',$order_item->product_id)->get();
                $users_count = [];
                // Start count users who bought this product
                foreach ($order_items as $item){
                    if (array_key_exists($item->order->user_id,$users_count)){
                        $users_count[$item->order->user_id]['users_count'] += 1;
                    }
                    $users_count[$item->order->user_id]['users_count'] = 1;
                }
                // count users who bought this product
                $num_of_users =array();
                foreach($users_count as $value) {
                    foreach($value as $key=>$secondValue) {
                        if(!isset($num_of_users[$key])) {
                            $num_of_users[$key]=0;
                        }
                        $num_of_users[$key]+=$secondValue;
                    }
                }
                // End count users who bought this product
                $selling_report[$order_item->product_id]['num_of_users'] = $num_of_users['users_count'];
                $total_cost += $order_item->cost;
            }
            $order->products = $selling_report;
            $order->total_cost = $total_cost;
            $all_cost += $total_cost;
        }
        $data['orders'] = $orders;
        $data['all_cost'] = $all_cost;
        return View::make('admin::admin.pages.reports.div',compact('data'));
    }

    public function exportReports($type)
    {
        $builder = Order::whereNotNull('id');
        if ($type == 'daily'){
            $builder->whereDate('created_at',date('Y-m-d'));

        }
        if ($type == 'weekly'){
            $builder->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
        }
        if ($type == 'monthly'){
            $builder->whereMonth('created_at',date('m'));
        }
        if ($type == 'annually'){
            $builder->whereYear('created_at',date('Y'));
        }

        $orders = $builder->orderBy('id','DESC')->paginate(10);
        $all_cost = 0;
        foreach ($orders as $order){
            $selling_report = [];
            $total_cost = 0;
            foreach ($order->orderItems as $order_item){
                if (array_key_exists($order_item->product_id,$selling_report)){
                    $selling_report[$order_item->product_id]['quantity'] += $order_item->product->selling;
                    $selling_report[$order_item->product_id]['cost'] += $order_item->cost;
                }else{
                    $selling_report[$order_item->product_id]['product_name'] = $order_item->product->name;
                    $selling_report[$order_item->product_id]['quantity'] = $order_item->product->selling;
                    $selling_report[$order_item->product_id]['cost'] = $order_item->cost;
                }
                $order_items = OrderItem::where('product_id',$order_item->product_id)->get();
                $users_count = [];
                // Start count users who bought this product
                foreach ($order_items as $item){
                    if (array_key_exists($item->order->user_id,$users_count)){
                        $users_count[$item->order->user_id]['users_count'] += 1;
                    }
                    $users_count[$item->order->user_id]['users_count'] = 1;
                }
                // count users who bought this product
                $num_of_users =array();
                foreach($users_count as $value) {
                    foreach($value as $key=>$secondValue) {
                        if(!isset($num_of_users[$key])) {
                            $num_of_users[$key]=0;
                        }
                        $num_of_users[$key]+=$secondValue;
                    }
                }
                // End count users who bought this product
                $selling_report[$order_item->product_id]['num_of_users'] = $num_of_users['users_count'];
                $total_cost += $order_item->cost;
            }
            $order->products = $selling_report;
            $order->total_cost = $total_cost;
            $all_cost += $total_cost;
        }
        $data['orders'] = $orders;
        $data['all_cost'] = $all_cost;

        return (new AdminReportsExport($data))->download('selling_report.xlsx');
    }

//    public function Reports(Request $request)
//    {
//        $builder = Order::whereNotNull('id');
//        if ($request->type == 'daily'){
//            $builder->whereDate('created_at',date('Y-m-d'));
//
//        }
//        if ($request->type == 'weekly'){
//            $builder->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
//        }
//        if ($request->type == 'monthly'){
//            $builder->whereMonth('created_at',date('m'));
//        }
//        if ($request->type == 'annually'){
//            $builder->whereYear('created_at',date('Y'));
//        }
//
//        $orders = $builder->orderBy('id','DESC')->paginate(10);
//        $data['orders'] = $orders;
//        $count_users = Order::distinct('user_id')->count('user_id');
//
//        $shops = Shop::get();
//        $all_cost = 0;
//        foreach ($shops as $shop){
//            $selling_report = [];
//            $total_cost = 0;
//            foreach ($shop->ordered_products as $order_item){
//                if (array_key_exists($order_item->product_id,$selling_report)){
//                    $selling_report[$order_item->product_id]['quantity'] += $order_item->product->selling;
//                    $selling_report[$order_item->product_id]['cost'] += $order_item->cost;
//                }else{
//                    $selling_report[$order_item->product_id]['product_name'] = $order_item->product->name;
//                    $selling_report[$order_item->product_id]['quantity'] = $order_item->product->selling;
//                    $selling_report[$order_item->product_id]['cost'] = $order_item->cost;
//                }
//                $order_items = OrderItem::where('product_id',$order_item->product_id)->get();
//                $users_count = [];
//                // Start count users who bought this product
//                foreach ($order_items as $item){
//                    if (array_key_exists($item->order->user_id,$users_count)){
//                        $users_count[$item->order->user_id]['users_count'] += 1;
//                    }
//                    $users_count[$item->order->user_id]['users_count'] = 1;
//                }
//                // count users who bought this product
//                $num_of_users =array();
//                foreach($users_count as $value) {
//                    foreach($value as $key=>$secondValue) {
//                        if(!isset($num_of_users[$key])) {
//                            $num_of_users[$key]=0;
//                        }
//                        $num_of_users[$key]+=$secondValue;
//                    }
//                }
//                // End count users who bought this product
//                $selling_report[$order_item->product_id]['num_of_users'] = $num_of_users['users_count'];
//                $total_cost += $order_item->cost;
//            }
//            $shop->products = $selling_report;
//            $shop->total_cost = $total_cost;
//            $all_cost += $total_cost;
//        }
//        $data['shops'] = $shops;
//        $data['all_cost'] = $all_cost;
//        return View::make('admin::admin.pages.reports.div',compact('data'));
//    }
//
//    public function exportReports($type)
//    {
//        $builder = Order::whereNotNull('id');
//        if ($type == 'daily'){
//            $builder->whereDate('created_at',date('Y-m-d'));
//
//        }
//        if ($type == 'weekly'){
//            $builder->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
//        }
//        if ($type == 'monthly'){
//            $builder->whereMonth('created_at',date('m'));
//        }
//        if ($type == 'annually'){
//            $builder->whereYear('created_at',date('Y'));
//        }
//
//        $shops_ids = $builder->orderBy('id','DESC')->pluck('shop_id')->toArray();
//
//        $shops = Shop::whereIn('id',$shops_ids)->get();
//        $all_cost = 0;
//        foreach ($shops as $shop){
//            $selling_report = [];
//            $total_cost = 0;
//            foreach ($shop->ordered_products as $order_item){
//                if (array_key_exists($order_item->product_id,$selling_report)){
//                    $selling_report[$order_item->product_id]['quantity'] += $order_item->product->selling;
//                    $selling_report[$order_item->product_id]['cost'] += $order_item->cost;
//                }else{
//                    $selling_report[$order_item->product_id]['product_name'] = $order_item->product->name;
//                    $selling_report[$order_item->product_id]['quantity'] = $order_item->product->selling;
//                    $selling_report[$order_item->product_id]['cost'] = $order_item->cost;
//                }
//                $order_items = OrderItem::where('product_id',$order_item->product_id)->get();
//                $users_count = [];
//                // Start count users who bought this product
//                foreach ($order_items as $item){
//                    if (array_key_exists($item->order->user_id,$users_count)){
//                        $users_count[$item->order->user_id]['users_count'] += 1;
//                    }
//                    $users_count[$item->order->user_id]['users_count'] = 1;
//                }
//                // count users who bought this product
//                $num_of_users =array();
//                foreach($users_count as $value) {
//                    foreach($value as $key=>$secondValue) {
//                        if(!isset($num_of_users[$key])) {
//                            $num_of_users[$key]=0;
//                        }
//                        $num_of_users[$key]+=$secondValue;
//                    }
//                }
//                // End count users who bought this product
//                $selling_report[$order_item->product_id]['num_of_users'] = $num_of_users['users_count'];
//                $total_cost += $order_item->cost;
//            }
//            $shop->products = $selling_report;
//            $shop->total_cost = $total_cost;
//            $all_cost += $total_cost;
//        }
//        $data['shops'] = $shops;
//        $data['all_cost'] = $all_cost;
//
//        return (new AdminReportsExport($data))->download('selling_report.xlsx');
//    }
}
