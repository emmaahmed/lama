<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Modules\Admin\Entities\AboutUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(){
        $abouts = AboutUs::orderBy('id','desc')
            ->get();
        $data=[];
        $data['title'] ='عن التطبيق';
        return view('admin::admin.pages.about.index',compact('abouts','data'));
        //return view('cp.about.index',['abouts'=>$abouts,]);

    }


    public function edit_abouts(Request $request){
        $this->validate($request,[
            'body' => 'required|',
            'facebook' => 'required|',
            'twitter' => 'required|',
            'phone' => 'required|',
            'email' => 'required|',
        ]);
        $c=AboutUs::where('id', $request->term_id)->first();
        $c->update($request->all());
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','تمت العملية بنجاح');
    }



}
