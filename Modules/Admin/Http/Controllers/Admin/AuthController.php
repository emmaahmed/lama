<?php

namespace Modules\Admin\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Admin\Http\Requests\AuthRequest;
use Modules\Admin\Repositories\Interfaces\AuthRepositoryInterface;

class AuthController extends Controller
{
    protected $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function login()
    {
        return view('admin::admin.auth.login');
    }
    public function doLogin(AuthRequest $request)
    {
        $logged_in = $this->authRepository->doLogin($request);
        // dd($logged_in);
        if ($logged_in){
//            session()->flash('success','تم الدخول بنجاح وجاري تحويلك ..');
//            dd(auth('admin')->user()->getAllPermissions());
            return redirect('admin/dashboard');
        }
        session()->flash('error','بيانات الدخول خاطئة');
        return redirect('admin/login');
    }

    public function logout()
    {
        auth()->guard('admin')->logout();
        return redirect('admin/logout');
    }
    public function forgetPassword()
    {
        return view('admin::admin.auth.passwords.email');
    }

    public function doForgetPassword(AuthRequest $request)
    {
        $this->authRepository->doForgetPassword($request);
        session()->flash('success','تم الإرسال للبريد الإلكتروني');
        return redirect()->back();
    }

    public function resetPassword ($token)
    {
        $check_token = $this->authRepository->resetPassword($token);
        if ($check_token){
            return view('admin::admin.auth.passwords.reset',['data' => $check_token]);
        }else{
            session()->flash('error','للأسف انتهت صلاحية كود التفعيل');
            return redirect(admin_url('forget/password'));
        }
    }
    public function doResetPassword (AuthRequest $request)
    {
        $check_token = $this->authRepository->doResetPassword($request);
        if ($check_token){
            session()->flash('success','تم تسجيل الدخول بنجاح');
            return redirect(admin_url('dashboard'));
        }
        session()->flash('error','للأسف انتهت صلاحية كود التفعيل');
        return redirect(admin_url('forget/password'));

    }
}
