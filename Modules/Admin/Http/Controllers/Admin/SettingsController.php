<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Setting\Http\Requests\SettingsRequest;
use Modules\Setting\Repositories\Interfaces\SettingsRepositoryInterface;

class SettingsController extends Controller
{
    protected $settingRepository;

    public function __construct(SettingsRepositoryInterface $settingRepository)
    {
        $this->settingRepository = $settingRepository;
//        $this->middleware('permission:edit_settings', ['only' => ['edit','update']]);
    }

    public function edit()
    {
        $data = $this->settingRepository->edit();

        return view('admin::admin.pages.settings.edit',compact('data'));
    }

    public function update(SettingsRequest $request)
    {
        $updated = $this->settingRepository->update($request);
        if ($updated){
            session()->flash('success','تم التعديل بنجاح');
            return redirect()->back();
        }
        session()->flash('error','حدث خطأ ما ');
        return redirect()->back();
    }
}
