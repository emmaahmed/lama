<?php

namespace Modules\Admin\Http\Controllers\Admin;

use App\Http\Controllers\Interfaces\Admin\CategoryRepositoryInterface;
use App\Terms;
use Modules\Admin\Entities\Term;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormTermsController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(){
        $terms = Terms::orderBy('id','desc')->where('type',1)
            ->get();
        $data=[];
        $data['title'] ='الشروط و الاحكام';
        return view('admin::admin.pages.form-terms.index',compact('terms','data'));
        //return view('cp.terms.index',['terms'=>$terms,]);

    }
    public function indexDelegate(){
        $terms = Terms::orderBy('id','desc')->where('type',0)
            ->get();
        $data=[];
        $data['title'] ='الشروط و الاحكام';
        return view('admin::admin.pages.form-terms.index',compact('terms','data'));
        //return view('cp.terms.index',['terms'=>$terms,]);

    }



    public function update(Request $request){
        /*$this->validate($request,[
            'body_ar' => 'required',
            'body_en' => 'required',
        ]);*/
//        dd($request->all());
        $c=Terms::where('id', $request->term_id)->first();
        $c->update($request->all());
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','تمت العملية بنجاح');
    }

}
