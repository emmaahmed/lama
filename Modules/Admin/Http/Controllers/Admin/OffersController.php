<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\OffersRequest;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Product\Entities\Product;
use Modules\Product\Repositories\Interfaces\OfferRepositoryInterface;
use Modules\Shop\Entities\Shop;

class OffersController extends Controller
{
    protected $offerRepository, $indexRepository;

    public function __construct(OfferRepositoryInterface $offerRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->offerRepository = $offerRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_offer', ['only' => ['index']]);
//        $this->middleware('permission:add_offer', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_offer', ['only' => ['edit','update']]);
//        $this->middleware('permission:delete_offer', ['only' => ['delete','destroyMulti']]);
    }

    public function index()
    {
        $data = $this->offerRepository->index();
        return view('admin::admin.pages.offers.index',compact('data'));
    }

    public function create()
    {

         $data = $this->offerRepository->create();
        $data['shops']=Shop::where('parent_id',NUll)->get();
        return view('admin::admin.pages.offers.create',compact('data'));
    }

    public function store(OffersRequest $request)
    {
        $created = $this->offerRepository->store($request);
        if ($created){
//            dd($request->all());
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('admin.offers');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(OffersRequest $request)
    {
        $data = $this->offerRepository->edit($request);
        $data['shops']=Shop::where('parent_id',NUll)->get();

        return view('admin::admin.pages.offers.edit',compact('data'));
    }

    public function update(OffersRequest $request)
    {
        $updated = $this->offerRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->route('admin.offers');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(OffersRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('offer', $request->offer_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(OffersRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('offer', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function getOffers(Request $request){
        $data= Product::where('shop_id',$request->shop_id)
//            ->select('id','name')
            ->get();
        //dd($data['products']);
        return response()->json($data);
    }
    public function search(Request $request){
//        return $request->all();
        $data = $this->offerRepository->searchOffers($request);

            return view('admin::admin.pages.offers.index',compact('data'));



    }


}
