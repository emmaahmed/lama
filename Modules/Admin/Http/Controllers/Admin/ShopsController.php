<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Product\Entities\Rate;
use Modules\Shop\Entities\Comment;
use Modules\Shop\Entities\Shop;
use Modules\Shop\Http\Requests\ProductRequest;
use Modules\Shop\Http\Requests\ShopRequest;
use Modules\Shop\Repositories\Interfaces\ShopRepositoryInterface;

class ShopsController extends Controller
{
    protected $shopRepository, $indexRepository;

    public function __construct(ShopRepositoryInterface $shopRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->shopRepository = $shopRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_shop', ['only' => ['index']]);
//        $this->middleware('permission:add_shop', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_shop', ['only' => ['edit','update','changeStatus']]);
//        $this->middleware('permission:delete_shop', ['only' => ['delete', 'destroyMulti','shopRemoveImages']]);
//        $this->middleware('permission:export_shop', ['only' => ['delete','destroyMulti']]);
//        $this->middleware('permission:show_product', ['only' => ['showProducts']]);
//        $this->middleware('permission:delete_product', ['only' => ['deleteProduct','destroyMultiProducts']]);
    }

    public function index()
    {
        $shops = $this->shopRepository->getShops();
        $data['shops'] = $shops;
        $data['title'] = "المتاجر";
        return view('admin::admin.pages.shops.index',compact('data'));
    }
    public function indexNotActive()
    {
        $shops = $this->shopRepository->getNotActiveShops();
        $data['shops'] = $shops;
        $data['title'] = "المتاجر الغير مفعلة";
        return view('admin::admin.pages.shops.index',compact('data'));
    }

    public function create()
    {
        $data = $this->shopRepository->create();
        return view('admin::admin.pages.shops.create',compact('data'));
    }

    public function store(ShopRequest $request)
    {
//        return $request->all();
       $request['payment']= implode(",",$request->payment);
        $request['delivery_types']= implode(",",$request->delivery_types);
        $created = $this->shopRepository->store($request);
        if ($created){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('admin.shops');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(ShopRequest $request)
    {

        $data = $this->shopRepository->edit($request);
        return view('admin::admin.pages.shops.edit',compact('data'));
    }

    public function update(ShopRequest $request)
    {
//        dd($request->validated());
        $request['payment']= implode(",",$request->payment);
        $request['delivery_types']= implode(",",$request->delivery_types);

        if(!isset($request->fast_delivery)){
            $request['fast_delivery']=0;
        }
        else{
            $request['fast_delivery']=1;

        }

        $updated = $this->shopRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->route('admin.shops');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function changeStatus(ShopRequest $request)
    {
        $status = $this->shopRepository->changeStatus($request);
        if ($status){
            if ($status == 'activated'){
                return response()->json(['success' => 'تم تفعيل المتجر بنجاح']);
            }
            return response()->json(['success' => 'تم إلغاء تفعيل المتجر بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function shopRemoveImages(ShopRequest $request)
    {
        $image_removed = $this->shopRepository->shopRemoveImages($request);
        if ($image_removed){
            session()->flash('success', 'تم حذف الصورة بنجاح');
            return redirect()->back();
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }
    public function searchChat(Request $request)
    {
        $users = Shop::whereTranslationLike('name', '%' . $request->name . '%')->where('status', 1)->with(['messages' => function ($query) {
            $query->where('sender_type', 1)->orderBy('id', 'desc');
        }])->get();
        $data=[];
        $data['title'] ='الدردشة';
        //dd($users);

        return view('admin::admin.pages.chat.index',compact('users','data'));

    }

    public function showProducts(ShopRequest $request)
    {

        $data = $this->shopRepository->showProducts($request);
        $data['shop']->shop_products=$data['shop']->shop_products()->paginate(5);
        return view('admin::admin.pages.shops.show',compact('data'));
    }

    public function delete(ShopRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('shop', $request->shop_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function deleteComment(Request $request)
    {
        //dd($request->all());
        $deleted = Comment::whereId($request->comment_id)->delete();
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(ShopRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('shop', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function deleteProduct(ShopRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('product', $request->product_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMultiProducts(ShopRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('product', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
    public function changeProductStatus(Request $request)
    {
        $result = $this->indexRepository->changeStatus($request);
        if ($result){
            return $result == 'product_activated' ? response()->json(['success' => 'تم تفعيل المنتج بنجاح']) : response()->json(['success' => 'تم إلغاء تفعيل المنتج بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
    public function deleteProductComment($id){
        $comment=Rate::whereId($id)->first();
        $comment->delete();
        session()->flash('success', 'تم المسح بنجاح');
        //return redirect()->route('shop.products');
        return redirect()->back();
    }


}
