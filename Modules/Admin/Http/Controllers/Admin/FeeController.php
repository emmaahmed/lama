<?php

namespace Modules\Admin\Http\Controllers\Admin;

use App\Fee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeeController extends Controller
{

    public function index(){
        $data=[];
        $data['title'] ='اسعار النطاقات';
        $data['fees'] = Fee::orderBy('id','desc')
            ->get();
        return view('admin::admin.pages.fees.index',compact('data'));
    }

    public function create()
    {
        $data=[];
        $data['title'] ='إضافة نطاق جديد';
        return view('admin::admin.pages.fees.create',compact('data'));
    }

    public function store(Request $request)
    {
        $created = Fee::Create($request->all());
        if ($created){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('admin.fees');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(Request $request)
    {
        $data=[];
        $data['title'] ='تعديل النطاق';
        $data['fee'] = Fee::whereId($request->fee_id)->first();
        return view('admin::admin.pages.fees.edit',compact('data'));
    }

    public function update(Request $request)
    {
        $updated = Fee::whereId($request->fee_id)->update($request->except('_token','fee_id'));
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->route('admin.fees');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $deleted = Fee::whereId($request->fee_id)->delete();
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }



}
