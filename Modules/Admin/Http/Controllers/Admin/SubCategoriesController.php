<?php

namespace Modules\Admin\Http\Controllers\Admin;

use App\Repositories\Interfaces\HomeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Shop\Http\Requests\SubCategoryRequest;
use Modules\Shop\Repositories\Interfaces\SubCategoryRepositoryInterface;

class SubCategoriesController extends Controller
{
    protected $subCategoryRepository, $homeRepository, $indexRepository;

    public function __construct(SubCategoryRepositoryInterface $subCategoryRepository, HomeRepositoryInterface $homeRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->subCategoryRepository = $subCategoryRepository;
        $this->homeRepository = $homeRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_sub_category', ['only' => ['index']]);
//        $this->middleware('permission:add_sub_category', ['only' => ['store']]);
//        $this->middleware('permission:edit_sub_category', ['only' => ['edit','update']]);
//        $this->middleware('permission:delete_sub_category', ['only' => ['delete','destroyMulti']]);
    }

    public function index(SubCategoryRequest $request)
    {
        $data = $this->subCategoryRepository->index($request);
        return view('admin::admin.pages.shops.subCategories.index',compact('data'));
    }

    public function store(SubCategoryRequest $request)
    {
        $created = $this->subCategoryRepository->store($request);
        if ($created){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->back();
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(SubCategoryRequest $request)
    {
        $subCategory  = $this->homeRepository->subCategories()->where('id', $request->subCategory_id)->first();
        $subCategory->name_ar = $subCategory->translate('ar')->name;
        $subCategory->name_en = $subCategory->translate('en')->name;
        return response()->json($subCategory);
    }

    public function update(SubCategoryRequest $request)
    {
        $updated = $this->subCategoryRepository->store($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->back();
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(SubCategoryRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('subCategory', $request->subCategory_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(SubCategoryRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('subCategory', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }


}
