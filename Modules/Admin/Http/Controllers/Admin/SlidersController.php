<?php

namespace Modules\Admin\Http\Controllers\Admin;

use App\Repositories\Interfaces\HomeRepositoryInterface;
use App\Repositories\Interfaces\SlidersRepositoryInterface;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\SlidersRequest;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;

class SlidersController extends Controller
{
    protected $slidersRepository, $indexRepository;

    public function __construct(SlidersRepositoryInterface $slidersRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->slidersRepository = $slidersRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_slider', ['only' => ['index']]);
//        $this->middleware('permission:add_slider', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_slider', ['only' => ['edit','update']]);
//        $this->middleware('permission:delete_slider', ['only' => ['delete','destroyMulti']]);
    }

    public function index()
    {
        $data = $this->slidersRepository->index();
        return view('admin::admin.pages.sliders.index',compact('data'));
    }

    public function create()
    {
        $data = $this->slidersRepository->create();
        return view('admin::admin.pages.sliders.create',compact('data'));
    }

    public function store(SlidersRequest $request)
    {
        $created = $this->slidersRepository->store($request);
        if ($created){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('admin.sliders');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(SlidersRequest $request)
    {
        $data = $this->slidersRepository->edit($request);

        return view('admin::admin.pages.sliders.edit',compact('data'));
    }

    public function update(SlidersRequest $request)
    {
        $updated = $this->slidersRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->route('admin.sliders');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(SlidersRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('slider', $request->slider_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(SlidersRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('slider', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
