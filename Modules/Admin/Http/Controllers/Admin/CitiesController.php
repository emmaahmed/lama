<?php

namespace Modules\Admin\Http\Controllers\Admin;

use App\Repositories\Interfaces\CityRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\CitiesRequest;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;

class CitiesController extends Controller
{
    protected $cityRepository, $indexRepository;

    public function __construct(CityRepositoryInterface $cityRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->cityRepository = $cityRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_city', ['only' => ['index']]);
//        $this->middleware('permission:add_city', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_city', ['only' => ['edit','update']]);
//        $this->middleware('permission:delete_city', ['only' => ['delete','destroyMulti']]);
    }

    public function index()
    {
        $cities = $this->cityRepository->getCities();
        $data['cities'] = $cities;
        $data['title'] = "المدن";
        return view('admin::admin.pages.cities.index',compact('data'));
    }

    public function create()
    {
        $data['title'] = "إضافة مدينة جديدة";
        return view('admin::admin.pages.cities.create',compact('data'));
    }

    public function store(CitiesRequest $request)
    {
        $created = $this->cityRepository->store($request);
        if ($created){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('admin.cities');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function edit(CitiesRequest $request)
    {
        $data = $this->cityRepository->edit($request);

        return view('admin::admin.pages.cities.edit',compact('data'));
    }

    public function update(CitiesRequest $request)
    {
        $updated = $this->cityRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->route('admin.cities');
        }
        session()->flash('error', 'عذراً حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(CitiesRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('city', $request->city_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(CitiesRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('city', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
