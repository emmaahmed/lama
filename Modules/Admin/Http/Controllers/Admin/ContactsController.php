<?php

namespace Modules\Admin\Http\Controllers\Admin;

use App\Repositories\ContactRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\ContactsRequest;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;

class ContactsController extends Controller
{
    protected $contactRepository;
    protected $indexRepository;

    public function __construct(ContactRepository $contactRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->contactRepository = $contactRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_inbox', ['only' => ['index','delete','destroyMulti']]);
//        $this->middleware('permission:reply_inbox', ['only' => ['reply']]);
    }

    public function index()
    {
        $data = $this->contactRepository->index();
        return view('admin::admin.pages.contacts.index',compact('data'));
    }

    public function reply(ContactsRequest $request)
    {
        $replied = $this->contactRepository->reply($request);
        if ($replied){
            session()->flash('success', 'تم الرد علي الرسالة');
            return redirect()->back();
        }
        session()->flash('error', 'حدث خطأ ما !');
        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('contact', $request->contact_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(Request $request)
    {
        $deleted = $this->indexRepository->destroyMulti('contact', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
