<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Entities\Admin;
use Modules\Admin\Http\Requests\AdminRequest;
use Modules\Admin\Repositories\Interfaces\AdminRepositoryInterface;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;

class AdminsController extends Controller
{
    protected $adminRepository, $indexRepository;

    public function __construct(AdminRepositoryInterface $adminRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->adminRepository = $adminRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_admin', ['only' => ['index']]);
//        $this->middleware('permission:add_admin', ['only' => ['create','store']]);
//        $this->middleware('permission:edit_admin', ['only' => ['edit','update']]);
//        $this->middleware('permission:delete_admin', ['only' => ['delete', 'destroyMulti']]);
    }

    public function index()
    {
        $data = $this->adminRepository->index();
        return view('admin::admin.pages.admins.index',compact('data'));
    }

    public function create()
    {
        $data = $this->adminRepository->create();
        return view('admin::admin.pages.admins.add',compact('data'));
    }

    public function store(AdminRequest $request)
    {
        $created = $this->adminRepository->store($request);
        if ($created){
            session()->flash('success', 'تم الإضافة بنجاح');
            return redirect()->route('admin.admins');
        }
        session()->flash('error', 'عذراً حدث خطأ ما');
        return redirect()->back();
    }

    public function edit(AdminRequest $request)
    {
        $data = $this->adminRepository->edit($request);
        return view('admin::admin.pages.admins.edit',compact('data'));
    }

    public function update(AdminRequest $request)
    {
        $updated = $this->adminRepository->update($request);
        if ($updated){
            session()->flash('success', 'تم التعديل بنجاح');
            return redirect()->route('admin.admins');
        }
        session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
        return redirect()->back();
    }

    public function delete(AdminRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('admin', $request->admin_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(AdminRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('admin', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
    public function updateToken(Request $request){

        $id=Auth::guard('admin')->user()->id;

        $admin=Admin::whereId($id)->first();
        $admin->update([
            'firebase_token'=>$request->firebase_token
        ]);
        return response()->json(['success' => 'تم الحذف بنجاح']);

    }
    public function sendTest(){
//        $api_key =  getServerKey();
//
//
//       // dd($api_key);
//        $admin=Admin::whereId(1)->first();
//
//        $firebase_token="cPeA8jTglxf67T1sGLSIbr:APA91bFFhTwubeQX15u5hNjbp_OaRaKNMauJvKF5VEZpbygp8YIgthflpbaj3EMq-J65Hk4Z6msoBHDKSBX8ufeoPzFOQaeAlEHEo32tE1JlFa8CC3Z1o4LA4cIDnbESEdCRTgGepBny";
//        $fields = array
//        (
//            "registration_ids" => [$firebase_token],
//            "priority" => 10,
//
//        'notification' => [
//            'type'    => "1",
//            'body' =>
//            "test",
//            'order_id' => "test",
//            'title' => "رسالة جديدة",
//            'sound' => 'default'
//        ],
//            'vibrate' => 1,
//            'sound' => 1
//        );
//        $headers = array
//        (
//            'accept: application/json',
//            'Content-Type: application/json',
//            'Authorization: key=' .$api_key
//        );
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
//        $result = curl_exec($ch);
//       dd($result);
//        //  var_dump($result);
//        if ($result === FALSE) {
//            die('Curl failed: ' . curl_error($ch));
//        }
//        curl_close($ch);
        dd( sendOrderNotification(1));



    }



}
