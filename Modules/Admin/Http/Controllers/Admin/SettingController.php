<?php

namespace Modules\Admin\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Setting\Entities\Setting;

class SettingController extends Controller
{
    function __construct()
    {
//        $this->middleware('permission:edit_settings', ['only' => ['edit','update']]);
    }

    public function edit()
    {
        $settings = Setting::first();
        $data['settings'] = $settings;
        $data['title'] = 'الاعدادات';
        return view('admin::admin.pages.settings.edit',compact('data'));
    }

    public function update(Request $request)
    {
        $settings = Setting::first();
        if (isset($settings)){
            $settings->translateOrNew('ar')->about_us = $request->about_ar;
            $settings->translateOrNew('en')->about_us = $request->about_en;
            $settings->translateOrNew('ar')->terms = $request->terms_ar;
            $settings->translateOrNew('en')->terms = $request->terms_en;
            $settings->save();
        }else{
            $settings = new Setting();
            $settings->translateOrNew('ar')->about_us = $request->about_ar;
            $settings->translateOrNew('en')->about_us = $request->about_en;
            $settings->translateOrNew('ar')->terms = $request->terms_ar;
            $settings->translateOrNew('en')->terms = $request->terms_en;
            $settings->save();
        }
        session()->flash('success','تم التعديل بنجاح');
        return redirect()->back();
    }
}
