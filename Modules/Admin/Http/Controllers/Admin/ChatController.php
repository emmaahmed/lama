<?php

namespace Modules\Admin\Http\Controllers\Admin;

//use App\Model\Chat;
use App\User;
use Modules\Admin\Entities\Admin;
use Modules\Order\Entities\UserDelegateMessage;
use Modules\Order\Entities\UsershopMessage;
use Modules\Shop\Entities\Shop;
use Modules\Admin\Entities\Chat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use DB;

class ChatController extends Controller
{
    public function index()
    {
        $users = Shop::where('status', 1)->with(['messages' => function ($query) {
            $query->where('sender_type', 1)->orderBy('id', 'desc');
        }])->get();
        $data=[];
        $data['title'] ='الدردشة';
        //dd($users);

        return view('admin::admin.pages.chat.index',compact('users','data'));
        //return view('chat.index', compact('users'));
    }

    public function reply($id)
    {
        $messages = Chat::where('shop_id', $id)
            ->where('admin_id', Auth::guard('admin')->user()->id)->get();
        foreach ($messages as $message) {
            $message->is_read = 1;
            $message->save();
        }
        $user = Shop::where('id', $id)->first();
        $data=[];
        $data['title'] ='الدردشة';

        return view('admin::admin.pages.chat.reply',compact('messages','user','data'));
        //return view('chat.reply', compact('messages', 'user'));
    }

    public function create_message(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'shop_id' => 'required|exists:shops,id',
            'message' => 'required',
        ], [
            'shop_id.required' => 'من فضلك اختر المستخدم',
            'shop_id.exists' => 'عذرًا هذا المستخدم غير موجودة لدينا',
            'message.required' => 'من فضلك ادخل الرسالة',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $message = Chat::create(array_merge($request->all(), [
            'admin_id' => Auth::guard('admin')->user()->id,
            'sender_type' => 2,
            'receiver_type' => 1,
        ]));
        $admin=Admin::whereId(Auth::guard('admin')->user()->id)->first();
        $time = date("F j, Y, g:i a",strtotime($message->created_at));
        //Chat::sendChat($request->user_id, $request->message,$time,$request->imag);
        //$message->load('admin');

        sendNotificationToAdmin(1,$admin->name,$request->message,$admin->image,$request->shop_id,'admin-message');
//        if ($message){
        return response()->json($message);
//        }else{
//            session()->flash('error', __('خطا في البيانات'));
//            return redirect()->back();
//        }
    }

    public function userDelegateChat(Request $request)
    {
        $data=[];

        $users = UserDelegateMessage::where('sender_id', $request->user_id)
            ->orWhere('receiver_id', $request->user_id)
            ->groupBy('delegate_id')
            ->select('delegate_id')
            ->get();

        foreach ($users as $user){
            $delegate = User::where('id',$user->delegate_id)->first();
            $user->id = $delegate->id;
            $user->name = $delegate->name;
            $user->image = $delegate->image;
            $user->email = $delegate->email;
            $user->phone = $delegate->phone;
        }
        $data['title'] ='الشات مع المناديب';
        $data['client'] = User::where('id',$request->user_id)->first();
        //dd($users);

        return view('admin::admin.pages.users.show_delegates',compact('users','data'));
        //return view('chat.index', compact('users'));
    }

    public function userDelegateChatDetails(Request $request)
    {
        $user_id = $request->user_id;
        $delegate_id = $request->delegate_id;
        $messages = DB::select("
                SELECT *
                FROM user_delegate_messages
                Where
                (sender_id = $user_id and delegate_id = $delegate_id)
                OR
                (receiver_id = $user_id and delegate_id = $delegate_id)
                Order By id asc
            ");
        foreach ($messages as $message) {
            $message->sender_image = User::where('id',$user_id)->first()->image;
            $message->receiver_image = User::where('id',$delegate_id)->first()->image;
        }

        $user = User::where('id', $user_id)->first();
        $delegate = User::where('id', $delegate_id)->first();
        $data=[];
        $data['title'] ='الدردشة';

        return view('admin::admin.pages.users.delegate_chat_details',compact('messages','user','delegate','data'));
    }

    public function userShopChat(Request $request)
    {
        $user_id=$request->user_id;
        $data=[];

        $users = DB::select("
                SELECT shop_id
                FROM user_shop_messages
                Where
                (sender_id = $user_id and sender_type = 0)
                OR
                (receiver_id = $user_id and receiver_type = 0 )
                group by shop_id
            ");

        foreach ($users as $user){
            $shop = Shop::where('id',$user->shop_id)->first();
            $user->id = $shop->id;
            $user->name = $shop->name;
            $user->logo = $shop->logo;
            $user->email = $shop->email;
            $user->phone = $shop->phone;
        }
        $data['title'] ='الشات مع المتاجر';
        $data['client'] = User::where('id',$request->user_id)->first();
        //dd($users);

        return view('admin::admin.pages.users.show_shops',compact('users','data'));
        //return view('chat.index', compact('users'));
    }

    public function userShopChatDetails(Request $request)
    {
        $user_id = $request->user_id;
        $shop_id = $request->shop_id;
        $messages = DB::select("
                SELECT *
                FROM user_shop_messages
                Where
                (sender_id = $user_id and shop_id = $shop_id)
                OR
                (receiver_id = $user_id and shop_id = $shop_id)
                Order By id asc
            ");
        foreach ($messages as $message) {
            $message->sender_image = User::where('id',$user_id)->first()->image;
            $message->receiver_image = Shop::where('id',$shop_id)->first()->logo;
        }

        $user = User::where('id', $user_id)->first();
        $delegate = Shop::where('id', $shop_id)->first();
        $data=[];
        $data['title'] ='الدردشة';

        return view('admin::admin.pages.users.shop_chat_details',compact('messages','user','delegate','data'));
    }
}
