<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\AdminNotificationRequest;
use Modules\Admin\Repositories\Interfaces\AdminNotificationRepositoryInterface;
use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;

class NotificationsController extends Controller
{
    protected $adminNotificationRepository;
    protected $indexRepository;

    public function __construct(AdminNotificationRepositoryInterface $adminNotificationRepository, IndexRepositoryInterface $indexRepository)
    {
        $this->adminNotificationRepository = $adminNotificationRepository;
        $this->indexRepository = $indexRepository;
//        $this->middleware('permission:show_sent', ['only' => ['index']]);
//        $this->middleware('permission:send_notification', ['only' => ['sendNotificationView','sendNotification','delete','destroyMulti']]);
    }

    public function index()
    {
        $data = $this->adminNotificationRepository->index();
        return view('admin::admin.pages.notifications.index',compact('data'));
    }

    public function sendNotificationView()
    {
        $data = $this->adminNotificationRepository->sendNotificationView();
        return view('admin::admin.pages.notifications.send',compact('data'));
    }

    public function sendNotification(AdminNotificationRequest $request)
    {
//        return $request->validated();
        $sent = $this->adminNotificationRepository->sendNotification($request);
        session()->flash('success', __("تم إرسال الرسالة بنجاح"));
        return redirect()->back();
    }

    public function delete(AdminNotificationRequest $request)
    {
        $deleted = $deleted = $this->indexRepository->delete('adminNotification', $request->notification_id);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }

    public function destroyMulti(AdminNotificationRequest $request)
    {
        $deleted = $this->indexRepository->destroyMulti('adminNotification', $request);
        if ($deleted){
            return response()->json(['success' => 'تم الحذف بنجاح']);
        }
        return response()->json(['error' => 'حدث خطأ ما !']);
    }
}
