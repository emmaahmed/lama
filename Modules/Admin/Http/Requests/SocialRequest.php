<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SocialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('admin/settings/socials/add')) {
            return $this->storeSocialRules();
        }
        if($this->is('admin/settings/socials/edit')
            || $this->is('admin/settings/socials/delete')) {
            return $this->checkSocialRules();
        }
        if($this->is('admin/settings/socials/update')) {
            return $this->updateSocialRules();
        }
        if($this->is('admin/settings/socials/delete-multi')) {
            return $this->deleteMultiRules();
        }
    }

    public function messages()
    {
        if($this->is('admin/settings/socials/add')) {
            return $this->storeSocialMessages();
        }
        if($this->is('admin/settings/socials/edit')
            || $this->is('admin/settings/socials/delete')) {
            return $this->checkSocialMessages();
        }
        if($this->is('admin/settings/socials/update')) {
            return $this->updateSocialMessages();
        }
        if($this->is('admin/settings/socials/delete-multi')) {
            return $this->deleteMultiMessages();
        }
    }



    public function storeSocialRules()
    {
        return [
            'link' => 'required|url',
            'logo' => 'required|mimes:jpeg,jpg,png,gif'
        ];
    }

    public function storeSocialMessages()
    {
        return [
            'link.required' => 'من فضلك ادخل الرابط',
            'link.url' => 'رابط غير صحيح',
            'logo.required' => 'من فضلك أدخل الصورة',
            'logo.mimies' => 'من فضلك أختر صورة',
        ];
    }

    public function checkSocialRules()
    {
        return [
            'social_id' => 'required|exists:social_media,id',
        ];
    }

    public function checkSocialMessages()
    {
        return [
            'social_id.required' => 'اختر الصفحة',
            'social_id.exists' => 'الصفحة غير موجود',
        ];
    }

    public function updateSocialRules()
    {
        return [
            'link' => 'required|url',
            'logo' => 'mimes:jpeg,jpg,png,gif'
        ];



    }

    public function updateSocialMessages()
    {
        return [
            'link.required' => 'من فضلك ادخل الرابط',
            'link.url' => 'رابط غير صحيح',
            'logo.mimies' => 'من فضلك أختر صورة',
        ];
    }

    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:social_media,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذا القسم غير موجود',
        ];
    }
}
