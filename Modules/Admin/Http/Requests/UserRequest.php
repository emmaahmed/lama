<?php

namespace Modules\Admin\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('admin/delegates/add')) {
            return $this->storeDelegateRules();
        }
        if($this->is('admin/users/change-status')
            || $this->is('admin/users/edit')
            || $this->is('admin/users/delete')
        // Delegate
            ||$this->is('admin/delegates/change-status')
            || $this->is('admin/delegates/edit')
            || $this->is('admin/delegates/delete')) {
                return $this->checkUserRules();
        }
        if($this->is('admin/users/update')
            || $this->is('admin/delegates/update')) {
            return $this->updateUserRules();
        }
        if($this->is('admin/users/delete-multi')
            || $this->is('admin/delegates/delete-multi')) {
            return $this->deleteMultiRules();
        }
    }

    public function messages()
    {
        if($this->is('admin/delegates/add')) {
            return $this->storeDelegateMessages();
        }
        if($this->is('admin/users/change-status')
            || $this->is('admin/users/edit')
            || $this->is('admin/users/delete')
            // Delegate
            ||$this->is('admin/delegates/change-status')
            || $this->is('admin/delegates/edit')
            || $this->is('admin/delegates/delete')) {
                return $this->checkUserMessages();
        }
        if($this->is('admin/users/update')
            || $this->is('admin/delegates/update')) {
            return $this->updateUserMessages();
        }
        if($this->is('admin/users/delete-multi')
            || $this->is('admin/delegates/delete-multi')) {
            return $this->deleteMultiMessages();
        }
    }

    public function checkUserRules()
    {
        return [
            'user_id' => 'required|exists:users,id',
        ];
    }

    public function checkUserMessages()
    {
        return [
            'user_id.required' => 'اختر المستخدم',
            'user_id.exists' => 'المستخدم غير موجود',
        ];
    }

    public function storeDelegateRules()
    {
        return [
            'name' => 'required',
            'phone' => 'required|unique:users,phone',
            'email' => 'required|email|unique:users,email',
            'status' => '',
        ];
    }

    public function storeDelegateMessages()
    {
        return [
            'name.required' => 'من فضلك أدخل إسم المندوب',
            'phone.required' => 'من فضلك أدخل رقم الهاتف',
            'phone.unique' => 'رقم الهاتف مستخدم من قبل',
            'email.required' => 'من فضلك أدخل البريد الإلكتروني',
            'email.email' => 'من فضلك أدخل بريد إلكتروني صالح',
            'email.unique' => 'البريد الإلكتروني مستخدم من قبل',
        ];
    }


    public function updateUserRules()
    {
        return [
            'name' => 'required',
            'phone' => 'required|unique:users,phone,'.$this->user_id,
            'email' => 'required|email|unique:users,email,'.$this->user_id,
        ];
    }

    public function updateUserMessages()
    {
        return [
            'name.required' => 'من فضلك أدخل إسم المستخدم',
            'phone.required' => 'من فضلك أدخل رقم الهاتف',
            'phone.unique' => 'رقم الهاتف مستخدم من قبل',
            'email.required' => 'من فضلك أدخل بريد إلكتروني صالح',
            'email.email' => 'من فضلك أدخل بريد إلكتروني صالح',
            'email.unique' => 'البريد الإلكتروني مستخدم من قبل',
        ];
    }
    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:users,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذا المستخدم غير موجود',
        ];
    }
}
