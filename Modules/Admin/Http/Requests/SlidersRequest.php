<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SlidersRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('admin/sliders/add')) {
            return $this->storeSliderRules();
        }
        if($this->is('admin/sliders/edit')
            || $this->is('admin/sliders/delete')) {
            return $this->checkSliderRules();
        }
        if($this->is('admin/sliders/update')) {
            return $this->updateSliderRules();
        }
        if($this->is('admin/sliders/delete-multi')) {
            return $this->deleteMultiRules();
        }
    }

    public function messages()
    {
        if($this->is('admin/sliders/add')) {
            return $this->storeSliderMessages();
        }
        if($this->is('admin/sliders/edit')
            || $this->is('admin/sliders/delete')) {
            return $this->checkSliderMessages();
        }
        if($this->is('admin/sliders/update')) {
            return $this->updateSliderMessages();
        }
        if($this->is('admin/sliders/delete-multi')) {
            return $this->deleteMultiMessages();
        }
    }

    public function storeSliderRules()
    {
        return [
            'shop_id' => 'required|exists:shops,id',
            'image' => 'required|mimes:jpeg,jpg,png,gif'
        ];
    }

    public function storeSliderMessages()
    {
        return [
            'shop_id.required' => 'اختر المتجر',
            'shop_id.exists' => 'المتجر غير موجود',
            'image.required' => 'من فضلك أختر صورة',
            'image.mimes' => 'من فضلك أختر صورة',
            'image.image' => 'من فضلك أختر صورة',
        ];
    }

    public function checkSliderRules()
    {
        return [
            'slider_id' => 'required|exists:sliders,id',
        ];
    }

    public function checkSliderMessages()
    {
        return [
            'slider_id.required' => 'اختر السلايدر',
            'slider_id.exists' => 'السلايدر غير موجود',
        ];
    }

    public function updateSliderRules()
    {
        return [
            'shop_id' => 'required|exists:shops,id',
            'image' => 'image|mimes:jpeg,jpg,png,gif'
        ];
    }

    public function updateSliderMessages()
    {
        return [
            'shop_id.required' => 'اختر المتجر',
            'shop_id.exists' => 'المتجر غير موجود',
            'image.mimies' => 'من فضلك أختر صورة',
            'image.image' => 'من فضلك أختر صورة',
        ];
    }

    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:sliders,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذا السلايدر غير موجود',
        ];
    }
}
