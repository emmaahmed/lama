<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('admin/login')) {
            return $this->checkLoginRules();
        }
        if($this->is('admin/doforget/password')) {
            return $this->checkEmailRules();
        }
        if($this->is('admin/reset/password')) {
            return $this->resetPasswordRules();
        }
    }

    public function messages()
    {
        if($this->is('admin/login')) {
            return $this->checkLoginMessages();
        }
        if($this->is('admin/doforget/password')) {
            return $this->checkEmailMessages();
        }
        if($this->is('admin/reset/password')) {
            return $this->resetPasswordMessages();
        }
    }


    public function checkLoginRules()
    {
        return [
            'email' => 'required|email|exists:admins,email',
            'password' => 'required'
        ];
    }

    public function checkLoginMessages()
    {
        return [
            'email.required' => 'من فضلك أدخل البريد الإلكتروني',
            'email.email' => 'من فضلك أدخل بريد إلكتروني صالح',
            'email.exists' => 'عذرا البريد الإلكتروني غير مسجل لدينا',
            'password.required' => 'من فضلك أدخل كلمة المرور',
        ];
    }



    public function checkEmailRules()
    {
        return [
            'email' => 'required|email|exists:admins,email'
        ];
    }

    public function checkEmailMessages()
    {
        return [
            'email.required' => 'من فضلك أدخل البريد الإلكتروني',
            'email.email' => 'من فضلك أدخل بريد إلكتروني صالح',
            'email.exists' => 'عذرا البريد الإلكتروني غير مسجل لدينا',
        ];
    }


    public function resetPasswordRules()
    {
        return [
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required',
        ];
    }

    public function resetPasswordMessages()
    {
        return [
            'password.required' => 'من فضلك أدخل كلمة المرور',
            'password.confirmed' => 'عفوا كلمة المرور غير متطابقة',
            'password.min' => 'من فضلك أدخل كلمة المرور أكثر من 6 حروف',
            'password_confirmation.required' => 'من فضلك أدخل نأكيد كلمة المرور'
        ];
    }



}
