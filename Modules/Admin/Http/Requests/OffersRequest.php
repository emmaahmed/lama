<?php

namespace Modules\Admin\Http\Requests;

use App\Http\Requests\Request;

class OffersRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('admin/offers/add')) {
            return $this->storeOfferRules();
        }
        if($this->is('admin/offers/edit')
            || $this->is('admin/offers/delete')) {
            return $this->checkOfferRules();
        }
        if($this->is('admin/offers/update')) {
            return $this->updateOfferRules();
        }
        if($this->is('admin/offers/delete-multi')) {
            return $this->deleteMultiRules();
        }
    }

    public function messages()
    {
        if($this->is('admin/offers/add')) {
            return $this->storeOfferMessages();
        }
        if($this->is('admin/offers/edit')
            || $this->is('admin/offers/delete')) {
            return $this->checkOfferMessages();
        }
        if($this->is('admin/offers/update')) {
            return $this->updateOfferMessages();
        }
        if($this->is('admin/offers/delete-multi')) {
            return $this->deleteMultiMessages();
        }
    }

    public function storeOfferRules()
    {
        return [
            'product_id' => 'required|exists:products,id',
            'image' => 'required',
            'expire_at'=>'required'
        ];
    }

    public function storeOfferMessages()
    {
        return [
            'product_id.required' => 'اختر المنتج',
            'product_id.exists' => 'المنتج غير موجود',
            'image.required' => 'من فضلك أختر صورة',
            'image.mimies' => 'من فضلك أختر صورة',
            'image.image' => 'من فضلك أختر صورة',
        ];
    }

    public function checkOfferRules()
    {
        return [
            'offer_id' => 'required|exists:offers,id',
        ];
    }

    public function checkOfferMessages()
    {
        return [
            'offer_id.required' => 'اختر العرض',
            'offer_id.exists' => 'العرض غير موجود',
        ];
    }

    public function updateOfferRules()
    {
        return [
            'product_id' => 'required|exists:products,id',
            'image' => 'nullable',
            'expire_at'=>'required'

        ];
    }

    public function updateOfferMessages()
    {
        return [
            'product_id.required' => 'اختر المنتج',
            'product_id.exists' => 'المنتج غير موجود',
            'image.mimies' => 'من فضلك أختر صورة',
            'image.image' => 'من فضلك أختر صورة',
        ];
    }

    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:offers,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذا العرض غير موجود',
        ];
    }
}
