<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TutorialsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('admin/settings/tutorials/add')) {
            return $this->storeTutorialRules();
        }
        if($this->is('admin/settings/tutorials/edit')
            || $this->is('admin/settings/tutorials/delete')) {
            return $this->checkTutorialRules();
        }
        if($this->is('admin/settings/tutorials/update')) {
            return $this->updateTutorialRules();
        }
        if($this->is('admin/settings/tutorials/delete-multi')) {
            return $this->deleteMultiRules();
        }
    }

    public function messages()
    {
        if($this->is('admin/settings/tutorials/add')) {
            return $this->storeTutorialMessages();
        }
        if($this->is('admin/settings/tutorials/edit')
            || $this->is('admin/settings/tutorials/delete')) {
            return $this->checkTutorialMessages();
        }
        if($this->is('admin/settings/tutorials/update')) {
            return $this->updateTutorialMessages();
        }
        if($this->is('admin/settings/tutorials/delete-multi')) {
            return $this->deleteMultiMessages();
        }
    }



    public function storeTutorialRules()
    {
        return [
            'type' => 'required|in:0,1',
            'name_ar' => 'required',
//            'name_en' => 'required',
            'description_ar' => 'required',
//            'description_en' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png,gif'
        ];
    }

    public function storeTutorialMessages()
    {
        return [
            'type.required' => 'من فضلك أختر أين يظهر التطبيق',
            'type.in' => 'من فضلك أختر إما تطبيق المستخدم أو المندوب',
            'name_ar.required' => 'من فضلك أدخل الإسم باللغة العربية',
//            'name_en.required' => 'من فضلك أدخل الإسم باللغة الانجليزية',
            'description_ar.required' => 'من فضلك أدخل النص باللغة العربية',
//            'description_en.required' => 'من فضلك أدخل النص باللغة الإنجليزية',
            'image.required' => 'من فضلك أدخل الصورة',
            'image.mimies' => 'من فضلك أختر صورة',
        ];
    }

    public function checkTutorialRules()
    {
        return [
            'tutorial_id' => 'required|exists:tutorials,id',
        ];
    }

    public function checkTutorialMessages()
    {
        return [
            'tutorial_id.required' => 'اختر الصفحة',
            'tutorial_id.exists' => 'الصفحة غير موجود',
        ];
    }

    public function updateTutorialRules()
    {
        return [
            'name_ar' => 'required',
//            'name_en' => 'required',
            'description_ar' => 'required',
//            'description_en' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif'
        ];
    }

    public function updateTutorialMessages()
    {
        return [
            'type.required' => 'من فضلك أختر أين يظهر التطبيق',
            'type.in' => 'من فضلك أختر إما تطبيق المستخدم أو المندوب',
            'name_ar.required' => 'من فضلك أدخل الإسم باللغة العربية',
//            'name_en.required' => 'من فضلك أدخل الإسم باللغة الانجليزية',
            'description_ar.required' => 'من فضلك أدخل النص باللغة العربية',
//            'description_en.required' => 'من فضلك أدخل النص باللغة الإنجليزية',
            'image.mimies' => 'من فضلك أختر صورة',
        ];
    }

    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:tutorials,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذا القسم غير موجود',
        ];
    }
}
