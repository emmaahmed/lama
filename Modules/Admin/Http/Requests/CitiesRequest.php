<?php

namespace Modules\Admin\Http\Requests;

use App\Http\Requests\Request;

class CitiesRequest extends Request
{
    public function rules()
    {
        if($this->is('admin/cities/add')) {
            return $this->storeCityRules();
        }
        if($this->is('admin/cities/edit')
            || $this->is('admin/cities/delete')) {
            return $this->checkCityRules();
        }
        if($this->is('admin/cities/update')) {
            return $this->updateCityRules();
        }
        if($this->is('admin/cities/delete-multi')) {
            return $this->deleteMultiRules();
        }
    }

    public function messages()
    {
        if($this->is('admin/cities/add')) {
            return $this->storeCityMessages();
        }
        if($this->is('admin/cities/edit')
            || $this->is('admin/cities/delete')) {
            return $this->checkCityMessages();
        }
        if($this->is('admin/cities/update')) {
            return $this->updateCityMessages();
        }
        if($this->is('admin/cities/delete-multi')) {
            return $this->deleteMultiMessages();
        }
    }



    public function storeCityRules()
    {
        return [
            'name_ar' => 'required',
//            'name_en' => 'required',
        ];
    }

    public function storeCityMessages()
    {
        return [
            'name_ar.required' => 'من فضلك أدخل إسم المدينة باللغة العربية',
//            'name_en.required' => 'من فضلك أدخل إسم المدينة باللغة الانجليزية',
        ];
    }

    public function checkCityRules()
    {
        return [
            'city_id' => 'required|exists:cities,id',
        ];
    }

    public function checkCityMessages()
    {
        return [
            'city_id.required' => 'اختر المدينة',
            'city_id.exists' => 'المدينة غير موجودة',
        ];
    }

    public function updateCityRules()
    {
        return [
            'name_ar' => 'required',
//            'name_en' => 'required',
        ];
    }

    public function updateCityMessages()
    {
        return [
            'name_ar.required' => 'من فضلك أدخل إسم المدينة باللغة العربية',
//            'name_en.required' => 'من فضلك أدخل إسم المدينة باللغة الإنجليزية',
        ];
    }

    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:cities,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذه المدينة غير موجودة',
        ];
    }
}
