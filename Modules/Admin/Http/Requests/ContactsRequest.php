<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('admin/contacts/reply')) {
            return $this->replyContactsRules();
        }
    }

    public function messages()
    {
        if($this->is('admin/contacts/reply')) {
            return $this->replyContactsMessages();
        }
    }

    public function replyContactsRules()
    {
        return [
            'contact_id' => 'required|exists:contacts,id',
            'reply' => 'required|min:3',
        ];
    }

    public function replyContactsMessages()
    {
        return [
            'contact_id.required' => 'اختر الرسالة المراد الرد عليها',
            'contact_id.exists' => 'الرسالة غير موجودة',
            'reply.required' => 'من فضلك اكتب رد',
            'reply.min' => 'من فضلك الرد 3 أحرف علي الأقل',
        ];
    }
}
