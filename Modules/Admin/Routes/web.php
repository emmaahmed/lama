<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function() {
    Route::get('/', 'AdminController@index');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Config::set('auth.defines', 'admin');
    // Login
    Route::get('login', 'AuthController@login');
    Route::post('login', 'AuthController@doLogin')->name('admin.login');
    //Forget & Reset
    Route::get('forget/password', 'AuthController@forgetPassword');
    Route::post('/doforget/password', 'AuthController@doForgetPassword')->name('forget.password');
    Route::get('reset/password/{token}', 'AuthController@resetPassword');
    Route::post('reset/password', 'AuthController@doResetPassword')->name('reset.password');
    Route::group(['middleware' => 'admin:admin'], function () {
        Route::get('/dashboard', 'HomeController@index')->name('admin.dashboard');
        Route::group(['prefix' => 'admins'], function () {
            Route::get('/send-test', 'AdminsController@sendTest');

            Route::get('/', 'AdminsController@index')->name('admin.admins');
            Route::get('/add', 'AdminsController@create')->name('admin.admins.create');
            Route::post('/add', 'AdminsController@store')->name('admin.admins.store');
            Route::get('/edit', 'AdminsController@edit')->name('admin.admins.edit');
            Route::post('/update', 'AdminsController@update')->name('admin.admins.update');
            Route::get('/show', 'AdminsController@show')->name('admin.admins.show');
            Route::post('/delete', 'AdminsController@delete')->name('admin.admins.delete');
            Route::post('/delete-multi', 'AdminsController@destroyMulti')->name('admin.admins.deleteMulti');
            Route::post('/send-multi', 'AdminsController@sendMulti')->name('admin.admins.sendMulti');
            Route::post('/update-token', 'AdminsController@updateToken')->name('admin.admins.update-token');
        });
        Route::group(['prefix' => 'users'], function () {
            Route::get('/', 'UsersController@index')->name('admin.users');
            Route::get('/add', 'UsersController@create')->name('admin.users.create');
            Route::post('/add', 'UsersController@store')->name('admin.users.store');
            Route::get('/edit', 'UsersController@edit')->name('admin.users.edit');
            Route::post('/update', 'UsersController@update')->name('admin.users.update');
            Route::post('/change-status', 'UsersController@changeStatus')->name('admin.users.status');
//            Route::get('/show/{user_id}', 'UsersController@show')->name('admin.users.show');
            Route::post('/delete', 'UsersController@delete')->name('admin.users.delete');
            Route::post('/delete-multi', 'UsersController@destroyMulti')->name('admin.users.deleteMulti');
//            Route::get('/orders', 'UsersController@ordersByUser')->name('admin.users.orders');
//            Route::get('/export-users','UsersController@exportUsers')->name('admin.users.export');
        });
        Route::group(['prefix' => 'delegates'], function () {
            Route::get('/', 'DelegatesController@index')->name('admin.delegates');
            Route::get('/not-active', 'DelegatesController@indexNotActive')->name('admin.delegates.not-active');
            Route::get('/add', 'DelegatesController@create')->name('admin.delegates.create');
            Route::post('/add', 'DelegatesController@store')->name('admin.delegates.store');
            Route::get('/edit', 'DelegatesController@edit')->name('admin.delegates.edit');
            Route::post('/update', 'DelegatesController@update')->name('admin.delegates.update');
            Route::post('/change-status', 'DelegatesController@changeStatus')->name('admin.delegates.status');
            Route::post('/delete', 'DelegatesController@delete')->name('admin.delegates.delete');
            Route::post('/delete-multi', 'DelegatesController@destroyMulti')->name('admin.delegates.deleteMulti');
        });
        Route::group(['prefix' => 'categories'], function () {
            Route::get('/', 'CategoriesController@index')->name('admin.categories');
            Route::get('/add', 'CategoriesController@create')->name('admin.categories.create');
            Route::post('/add', 'CategoriesController@store')->name('admin.categories.store');
            Route::post('/change-status', 'CategoriesController@changeStatus')->name('admin.categories.status');
            //
            Route::get('/edit', 'CategoriesController@edit')->name('admin.categories.edit');
            Route::post('/update', 'CategoriesController@update')->name('admin.categories.update');
            Route::post('/delete', 'CategoriesController@delete')->name('admin.categories.delete');
            Route::post('/delete-multi', 'CategoriesController@destroyMulti')->name('admin.categories.deleteMulti');
        });
        Route::group(['prefix' => 'cities'], function () {
            Route::get('/', 'CitiesController@index')->name('admin.cities');
            Route::get('/add', 'CitiesController@create')->name('admin.cities.create');
            Route::post('/add', 'CitiesController@store')->name('admin.cities.store');
            Route::get('/edit', 'CitiesController@edit')->name('admin.cities.edit');
            Route::post('/update', 'CitiesController@update')->name('admin.cities.update');
            Route::post('/delete', 'CitiesController@delete')->name('admin.cities.delete');
            Route::post('/delete-multi', 'CitiesController@destroyMulti')->name('admin.cities.deleteMulti');
        });
        //
        Route::group(['prefix' => 'requests'], function () {
            Route::get('/shop-requests', 'HomeController@shopRequests')->name('admin.shopRequests');
            Route::post('/delete-request', 'HomeController@deleteShopRequest')->name('admin.request.delete');
            Route::post('/delete-delegate-request', 'HomeController@deleteDelegateRequest')->name('admin.delegate-request.delete');
            Route::get('/shop-activated', 'HomeController@shopActivated')->name('admin.shopRequests.activated');
            Route::post('shop/change-status', 'HomeController@shopChangeStatus')->name('admin.shopRequests.status');
            Route::get('/delegate-requests', 'HomeController@delegateRequests')->name('admin.delegateRequests');
            Route::get('/delegate-activated', 'HomeController@delegateActivated')->name('admin.delegateRequests.activated');
            Route::post('delegate/change-status', 'HomeController@delegateChangeStatus')->name('admin.delegateRequests.status');
        });
        //
        Route::group(['prefix' => 'shops'], function () {
            Route::get('/', 'ShopsController@index')->name('admin.shops');
            Route::get('/not-active', 'ShopsController@indexNotActive')->name('admin.shops.not-active');
            Route::get('/add', 'ShopsController@create')->name('admin.shops.create');
            Route::post('/add', 'ShopsController@store')->name('admin.shops.store');
            Route::post('/change-status', 'ShopsController@changeStatus')->name('admin.shops.status');
            Route::get('/edit', 'ShopsController@edit')->name('admin.shops.edit');
            Route::post('/update', 'ShopsController@update')->name('admin.shops.update');
            Route::get('remove-images', 'ShopsController@shopRemoveImages')->name('admin.shops.images.remove');
            Route::post('/delete', 'ShopsController@delete')->name('admin.shops.delete');
            Route::post('/deleteComment', 'ShopsController@deleteComment')->name('admin.shops.deleteComment');
            Route::post('/delete-multi', 'ShopsController@destroyMulti')->name('admin.shops.deleteMulti');
            //Shop Sub Categories
            Route::get('/subCategories', 'SubCategoriesController@index')->name('admin.shops.subCategories');
            Route::post('/add/subCategory', 'SubCategoriesController@store')->name('admin.shops.subCategories.store');
            Route::get('/edit/subCategory', 'SubCategoriesController@edit')->name('admin.shops.subCategories.edit');
            Route::post('/update/subCategory', 'SubCategoriesController@update')->name('admin.shops.subCategories.update');
            Route::post('/delete/subCategory', 'SubCategoriesController@delete')->name('admin.shops.subCategories.delete');
            Route::post('/delete-multi/subCategory', 'SubCategoriesController@destroyMulti')->name('admin.shops.subCategories.deleteMulti');
            // Shop Products
            Route::post('/change-product-status', 'ShopsController@changeProductStatus')->name('admin.shops.products.status');
            Route::get('/shop-products', 'ShopsController@showProducts')->name('admin.shops.products.show');
            Route::post('/delete/products', 'ShopsController@deleteProduct')->name('admin.shops.products.delete');
            Route::post('/delete-multi/products', 'ShopsController@destroyMultiProducts')->name('admin.shops.products.deleteMulti');
            Route::any('/search-chat', 'ShopsController@searchChat')->name('admin.chat.search');
            Route::get('/delete-comment/{comment_id}', 'ShopsController@deleteProductComment')->name('admin.products.delete-comment');

        });
        Route::group(['prefix' => 'fees'], function () {
            Route::get('/', 'FeeController@index')->name('admin.fees');
            Route::get('/add', 'FeeController@create')->name('admin.fees.create');
            Route::post('/add', 'FeeController@store')->name('admin.fees.store');
            Route::get('/edit', 'FeeController@edit')->name('admin.fees.edit');
            Route::post('/update', 'FeeController@update')->name('admin.fees.update');
            Route::post('/delete', 'FeeController@delete')->name('admin.fees.delete');
        });
        Route::group(['prefix' => 'sliders'], function () {
            Route::get('/', 'SlidersController@index')->name('admin.sliders');
            Route::get('/add', 'SlidersController@create')->name('admin.sliders.create');
            Route::post('/add', 'SlidersController@store')->name('admin.sliders.store');
            Route::get('/edit', 'SlidersController@edit')->name('admin.sliders.edit');
            Route::post('/update', 'SlidersController@update')->name('admin.sliders.update');
            Route::post('/delete', 'SlidersController@delete')->name('admin.sliders.delete');
            Route::post('/delete-multi', 'SlidersController@destroyMulti')->name('admin.sliders.deleteMulti');
        });
        Route::group(['prefix' => 'offers'], function () {
            Route::post('/search', 'OffersController@search')->name('admin.offers.search');

            Route::get('/', 'OffersController@index')->name('admin.offers');
            Route::get('/add', 'OffersController@create')->name('admin.offers.create');
            Route::post('/add', 'OffersController@store')->name('admin.offers.store');
            Route::get('/edit', 'OffersController@edit')->name('admin.offers.edit');
            Route::post('/update', 'OffersController@update')->name('admin.offers.update');
            Route::post('/delete', 'OffersController@delete')->name('admin.offers.delete');
            Route::post('/delete-multi', 'OffersController@destroyMulti')->name('admin.offers.deleteMulti');
            //ajax
            Route::post('/get/offers', 'OffersController@getOffers')->name('getOffers');

        });
        Route::group(['prefix' => 'orders'], function () {
            Route::get('/', 'OrdersController@index')->name('admin.orders');
            Route::get('/finishedOrders', 'OrdersController@finishedOrders')->name('admin.finishedOrders');
            Route::get('/unfinishedOrders', 'OrdersController@unfinishedOrders')->name('admin.unfinishedOrders');
            Route::post('/change-status', 'OrdersController@changeStatus')->name('admin.orders.status');
            Route::get('/show-details', 'OrdersController@showProducts')->name('admin.orders.details');
            Route::post('/edit-rate', 'OrdersController@editRate')->name('admin.orders.rate');
            //
            Route::get('/print-invoice', 'OrdersController@printInvoice')->name('admin.orders.invoice');
            //
            Route::post('/delete', 'OrdersController@delete')->name('admin.orders.delete');
            Route::post('/delete-multi', 'OrdersController@destroyMulti')->name('admin.orders.deleteMulti');
            Route::get('/filter-orders', 'OrdersController@filterOrders')->name('admin.orders.filter');
            Route::get('/print','OrdersController@PrintOrder')->name('admin.order.print');
            Route::get('/export-orders/{type}/{id?}','OrdersController@exportOrders')->name('admin.order.export');

            Route::get('/reports','ReportsController@index')->name('admin.orders.reports');
            Route::get('/get-reports','ReportsController@Reports')->name('admin.orders.getReports');
            Route::get('/export-reports/{type}','ReportsController@exportReports')->name('admin.orders.reports.export');
            /*
            type :
                0 => all orders ,
                1 => user orders ,
                2 => delegate orders ,
            */
            Route::get('/export-users','OrdersController@exportUsers')->name('admin.user.export');
        });
        Route::group(['prefix' => 'notifications'], function () {
            Route::get('/', 'NotificationsController@index')->name('admin.notifications');
            Route::post('/delete', 'NotificationsController@delete')->name('admin.notifications.delete');
            Route::post('/delete-multi', 'NotificationsController@destroyMulti')->name('admin.notifications.deleteMulti');
            Route::get('send-notification', 'NotificationsController@sendNotificationView')->name('admin.notifications.send');
            Route::post('send-notification', 'NotificationsController@sendNotification')->name('admin.notifications.send-notification');
        });
        Route::group(['prefix' => 'contacts'], function () {
            Route::get('/', 'ContactsController@index')->name('admin.contacts');
            Route::post('reply', 'ContactsController@reply')->name('admin.contacts.reply');
            Route::post('/delete', 'ContactsController@delete')->name('admin.contacts.delete');
            Route::post('/delete-multi', 'ContactsController@destroyMulti')->name('admin.contacts.deleteMulti');
        });
        Route::group(['prefix' => 'settings'], function () {
            Route::group(['prefix' => 'tutorials'], function () {
                Route::get('/', 'TutorialsController@index')->name('admin.settings.tutorials');
                Route::get('/add', 'TutorialsController@create')->name('admin.settings.tutorials.create');
                Route::post('/add', 'TutorialsController@store')->name('admin.settings.tutorials.store');
                Route::get('/edit', 'TutorialsController@edit')->name('admin.settings.tutorials.edit');
                Route::post('/update', 'TutorialsController@update')->name('admin.settings.tutorials.update');
                Route::post('/delete', 'TutorialsController@delete')->name('admin.settings.tutorials.delete');
                Route::post('/delete-multi', 'TutorialsController@destroyMulti')->name('admin.settings.tutorials.deleteMulti');
            });
            Route::group(['prefix' => 'socials'], function () {
                Route::get('/', 'SocialController@index')->name('admin.settings.socials');
                Route::get('/add', 'SocialController@create')->name('admin.settings.socials.create');
                Route::post('/add', 'SocialController@store')->name('admin.settings.socials.store');
                Route::get('/edit', 'SocialController@edit')->name('admin.settings.socials.edit');
                Route::post('/update', 'SocialController@update')->name('admin.settings.socials.update');
                Route::post('/delete', 'SocialController@delete')->name('admin.settings.socials.delete');
                Route::post('/delete-multi', 'SocialController@destroyMulti')->name('admin.settings.socials.deleteMulti');
            });
            Route::get('/edit', 'SettingsController@edit')->name('admin.settings');
            Route::post('/update', 'SettingsController@update')->name('admin.settings.update');
        });



        Route::any('logout', 'AuthController@logout')->name('admin.logout');

        //chat between admin and users
        Route::get('chat', 'ChatController@index')->name('admin.chat');
        Route::get('reply/{id}', 'ChatController@reply')->name('admin.reply');
        Route::post('send', 'ChatController@create_message')->name('admin.send');
        //
        Route::get('user_delegate_chat', 'ChatController@userDelegateChat')->name('admin.user_delegate_chat');
        Route::get('delegateDetails/{delegate_id}/{user_id}','ChatController@userDelegateChatDetails')->name('admin.delegate_chat_details');
        Route::get('user_shop_chat', 'ChatController@userShopChat')->name('admin.user_shop_chat');
        Route::get('shopDetails/{shop_id}/{user_id}','ChatController@userShopChatDetails')->name('admin.shop_chat_details');
        //
        //
        Route::resource('/terms', 'TermController');
        Route::get('form-terms/delegates', 'FormTermsController@indexDelegate')->name('form-terms.delegate');

        Route::resource('form-terms', 'FormTermsController');
        Route::post('/terms/edit', 'TermController@edit_terms')->name('editTerm');

        Route::resource('/abouts', 'AboutController');
        Route::post('/abouts/edit', 'AboutController@edit_abouts')->name('editAbout');

        Route::resource('/explains', 'AppExplanationController');
        Route::post('/explains/edit', 'AppExplanationController@edit_explain')->name('editExplain');
        Route::get('/explains/delet/{id}', 'AppExplanationController@delete_explain')->name('deleteExplain');
        //
    });


});
