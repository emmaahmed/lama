
<table class="display" id="basic-9">
    <thead>
    <tr>
        <th>اسم المتجر</th>
        <th class="row">
            <div class="col-md-3">
                إسم المنتج
            </div>
            |
            <div class="col-md-3">
                الكمية المباعة
            </div>
            |
            <div class="col-md-3">
                السعر
            </div>
            |
            <div class="col-md-3">
                عدد المشترين
            </div>
        </th>
        <th>اجمالي البيع</th>
    </tr>
    </thead>
    <tbody>
    @isset($data['orders'])
        @foreach($data['orders'] as $order)
            @if(isset($order->products) && $order->products != null)
                <tr>
                    <td>{{isset($order->shop) ? $order->shop->name : ""}}</td>
                    <td class="row">
                    @foreach($order->products as $product)
                        <div class="col-md-3">
                            {{isset($product['product_name']) ? $product['product_name'] : ""}}
                        </div>
                        |
                        <div class="col-md-3">
                            {{isset($product['quantity']) ? $product['quantity'] : ""}}
                        </div>
                        |
                        <div class="col-md-3">
                            {{isset($product['cost']) ? $product['cost'] : ""}}
                        </div>
                        |
                        <div class="col-md-3">
                            {{isset($product['num_of_users']) ? $product['num_of_users'] : ""}}
                        </div>
                                <br/>
                    @endforeach
                    </td>
                    <td>{{isset($order) ? $order->total_cost : ""}}</td>
                </tr>
            @endif
        @endforeach
        <tr class="odd">
            <td valign="top" colspan="4" class="dataTables_empty">
                <strong>
                    التكلفة الإجمالية :
                    {{isset($data['all_cost']) ? $data['all_cost'] : ""}}
                </strong>
            </td>
        </tr>
    @endisset
    </tbody>
</table>