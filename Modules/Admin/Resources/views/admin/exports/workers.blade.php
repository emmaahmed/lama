<table>
    <thead>
    <meta charset="UTF-8">
    <tr>
        <th>الإسم</th>
        <th>البريد الإلكتروني</th>
        <th>رقم الهاتف</th>
        <th>المطلوب دفعه</th>
        <th>الحالة</th>
    </tr>
    </thead>
    <tbody>
    @isset($workers)
        @foreach($workers as $worker)
            <tr>
                <td>{{isset($worker->name) ? str_limit($worker->name , 50) : ""}}</td>
                <td>{{isset($worker->email) ? $worker->email : ""}}</td>
                <td>{{isset($worker->phone) ? $worker->phone : ""}}</td>
                <td>{{isset($worker->should_be_paid) ? $worker->should_be_paid : ""}}</td>
                <td>
                    @if($worker->status == 1)
                        <span class="badge badge-info">نشط</span>
                    @else
                        <span class="badge badge-warning">معلق</span>
                    @endif
                </td>
            </tr>
        @endforeach
    @endisset
    </tbody>
</table>