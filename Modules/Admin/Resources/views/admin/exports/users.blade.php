<table>
    <thead>
    <meta charset="UTF-8">
    <tr>
        <th>الإسم</th>
        <th>البريد الإلكتروني</th>
        <th>رقم الهاتف</th>
        <th>الحالة</th>
    </tr>
    </thead>
    <tbody>
    @isset($users)
        @foreach($users as $user)
        <tr>
            <td>{{isset($user->name) ? str_limit($user->name , 50) : ""}}</td>
            <td>{{isset($user->email) ? $user->email : ""}}</td>
            <td>{{isset($user->phone) ? $user->phone : ""}}</td>
            <td>
                @if($user->status == 1)
                    <span class="badge badge-info">نشط</span>
                @else
                    <span class="badge badge-warning">معلق</span>
                @endif
            </td>
        </tr>
    @endforeach
    @endisset
    </tbody>
</table>