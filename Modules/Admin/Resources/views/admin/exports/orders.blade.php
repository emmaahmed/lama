<table>
    <thead>
    <meta charset="UTF-8">
    <tr>
        <th>رقم الطلب</th>
        <th>المتجر</th>
        <th>صاحب الطلب</th>
        <th>مندوب التوصيل</th>
        <th>العنوان</th>
        <th>الوصف</th>
        <th>السعر</th>
        <th>الشحن</th>
        <th>السعر الكلي</th>
        <th>نوع الدفع</th>
        <th>تاريخ الانشاء</th>
        <th>الحالة</th>
    </tr>
    </thead>
    <tbody>
    @isset($orders)
        @foreach($orders as $order)
            <tr>
                <td>{{isset($order->code) ? $order->code : ""}}</td>
                <td>{{isset($order->market) ? $order->market->name : ""}}</td>
                <td>{{isset($order->user) ? $order->user->name : ""}}</td>
                <td>{{isset($order->delegate) ? $order->delegate->name : "-"}}</td>
                <td>{{isset($order->location) ? $order->location : ""}}</td>
                <td>{{isset($order->description) ? str_limit($order->description , 100) : ""}}</td>
                <td>{{isset($order->price) ? $order->price : ""}}</td>
                <td>{{isset($order->shipping) ? $order->shipping : ""}}</td>
                <td>{{isset($order->total_price) ? $order->total_price : ""}}</td>
                <td>
                    @if($order->status == 0)
                        <span class="badge badge-warning">كاش</span>
                    @elseif($order->status == 1)
                        <span class="badge badge-success">اونلاين</span>
                    @endif
                </td>
                <td>{{isset($order->created_at) ? $order->created_at : ""}}</td>
                <td>
                    @if($order->status == 0)
                        <span class="badge badge-warning">معلق</span>
                    @elseif($order->status == 1)
                        <span class="badge badge-success">جاري التوصيل</span>
                    @elseif($order->status == 2)
                        <span class="badge badge-info">جاري الانتهاء</span>
                    @elseif($order->status == 3)
                        <span class="badge badge-dark">منتهي</span>
                    @elseif($order->status == 4)
                        <span class="badge badge-danger">تم الإلغاء</span>
                    @endif
                </td>
            </tr>
        @endforeach
    @endisset
    </tbody>
</table>