@extends('admin::admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/simple-mde.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    @if(Auth::guard('admin')->user()->can('show_inbox'))
                                        <button class="btn btn-square btn-danger" style="float: left; margin-right: 2%;" id="bulk_delete_btn" type="button" title="حذف متعدد">حذف متعدد</button>
                                    @endif
                                    <input type="hidden" name="myids" id="bulk_delete_input" value="">
                                    <br>
                                    <br>
                                    <th>
                                        <label class="d-block" for="chk-ani">
                                            <input class="checkbox_animated select_all" id="chk-ani" type="checkbox">
                                        </label>
                                    </th>
                                    <th>الإسم</th>
                                    <th>البريد الإلكتروني</th>
                                    <th>رقم الهاتف</th>
                                    <th>حالة الرسالة</th>
                                    <th>الرسالة</th>
                                        <th>التاريخ</th>
                                    {{--@if(Auth::guard('admin')->user()->can('show_inbox'))--}}
                                        <th>العمليات</th>
                                    {{--@endif--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['contacts'] as $contact)
                                    <tr>
                                        <td>
                                            <label class="d-block" for="chk-ani">
                                                <input type="checkbox" class="checkbox_animated" name="row_id" id="checkbox_{{ $contact->id }}" value="{{ $contact->id }}">
                                            </label>
                                        </td>
                                        <td>{{isset($contact->name) ? $contact->name : ""}}</td>
                                        <td>{{isset($contact->email) ? $contact->email : ""}}</td>
                                        <td>{{isset($contact->phone) ? $contact->phone : ""}}</td>
                                        <td>
                                            @if ($contact->is_replied == '0')
                                                <span class="badge badge-success">جديدة</span>
                                            @elseif ($contact->is_replied == '1')
                                                <a href="#" data-reply="{{$contact->reply}}" title="رؤية الرد" data-toggle="modal" data-target="#viewReplyModal">
                                                    <span class="badge badge-dark">تم الرد</span>
                                                </a>
                                            @endif
                                        </td>
                                        <td>{{isset($contact->message) ? str_limit($contact->message , 100) : ""}}</td>
                                        <td>{{$contact->created_at}}</td>
                                        <td>
                                            @if(Auth::guard('admin')->user()->can('reply_inbox'))
                                                <div class="row">
                                                    @if(!isset($contact->reply))
                                                    <a href="#" class="should" data-id="{{$contact->id}}" title="إرسال رد" data-toggle="modal" data-target="#replyModal">
                                                        <i width="20" height="20" color="blue" data-feather="send"></i>
                                                    </a> &nbsp;
                                                    @endisset
                                                    @isset($contact->reply)
                                                    <a href="#" data-reply="{{$contact->reply}}" title="رؤية الرد" data-toggle="modal" data-target="#viewReplyModal">
                                                        <i width="20" height="20" color="green" data-feather="inbox"></i>
                                                    </a> &nbsp;
                                                    @endisset
                                                    <a onclick='return deleteContact({{$contact->id}})' title="حذف" data-id="{{$contact->id}}" href="#">
                                                        <i width="20" height="20" color="red" data-feather="trash-2"></i>
                                                    </a>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@section('modals')
    <div class="modal fade rtl" id="replyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">الرد علي الرسالة</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <form class="needs-validation" novalidate="" method="post" action="{{route('admin.contacts.reply')}}" >
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="contact_id" id="contact_id"/>
                        <div class="form-group">
                            <label class="col-form-label float-right" for="reply">الرد</label>
                            <textarea class="form-control" name="reply" id="reply" cols="20" rows="3" required></textarea>
                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary float-right" type="submit">إرسال</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">إغلاق</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade rtl" id="viewReplyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">رد الرسالة : </h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p id="reply-text" style="float: right;"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    {{--<button class="btn btn-primary float-right" type="submit">إرسال</button>--}}
                    {{--<button class="btn btn-secondary" type="button" data-dismiss="modal">إغلاق</button>--}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.should').on('click',function (e) {
            e.preventDefault();
            $('#reply').text('');
        });
        $('#replyModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var modal = $(this);
            modal.find('.modal-body #contact_id').val(id);
        });
        $('#replyModal').on('hidden.bs.modal', function (e) {
            $(this).find("textarea").val('').end();
        });
        $('#viewReplyModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var reply = button.data('reply');
            var modal = $(this);
            modal.find('.modal-body #reply-text').text(reply);
        });
    </script>
    <script>
        function deleteContact(contact_id)
        {
            var id = contact_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.contacts.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {contact_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
        }
        $(document).ready(function(){
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        myids.push(value);
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.contacts.deleteMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

            // End Bulk Delete
        });
    </script>
@endsection
