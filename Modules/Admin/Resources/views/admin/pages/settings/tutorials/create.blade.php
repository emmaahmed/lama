@extends('admin::admin.layouts.master_admin')

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$data['title']}}</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.settings.tutorials.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="type">أين تظهر</label>
                                        <select name="type" class="custom-select form-control" id="type" required>
                                            <option value="" disabled selected>اختر أين تريد أن تظهر هذة الصفحة</option>
                                            <option value="0">تطبيق المستخدم</option>
                                            <option value="1">تطبيق مندوب التوصيل</option>
                                        </select>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">الإسم باللغة العربية</label>
                                        <input class="form-control btn-square" name="name_ar" id="name_ar" type="text" placeholder="الإسم باللغة العربية" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            {{--<div class="form-row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="name_en">الإسم باللغة الإنجليزية</label>--}}
                                        {{--<input class="form-control btn-square" name="name_en" id="name_en" type="text" placeholder="الإسم باللغة الإنجليزية" required>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="description_ar">النص (باللغة العربية)</label>
                                        <textarea class="form-control" name="description_ar" id="description_ar" cols="20" rows="2" required></textarea>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-form-label float-right" for="description_en">النص (باللغة الانجليزية)</label>--}}
                                        {{--<textarea class="form-control" name="description_en" id="description_en" cols="20" rows="2" required></textarea>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <br/>
                            <div class="form-row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="image" id="image" type="file" required="" data-original-title="" title="">
                                        <label class="custom-file-label" for="image">الصورة</label>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
@endsection

