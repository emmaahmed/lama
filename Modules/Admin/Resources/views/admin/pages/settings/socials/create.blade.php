@extends('admin::admin.layouts.master_admin')

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$data['title']}}</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.settings.socials.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="link">الرابط</label>
                                        <input class="form-control btn-square" name="link" id="link" type="url" placeholder="https://www.example.com" required>
                                        <div class="invalid-feedback">رابط غير صحيح</div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="form-row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="logo" id="logo" type="file" required="" data-original-title="" title="">
                                        <label class="custom-file-label" for="logo">الصورة</label>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
@endsection

