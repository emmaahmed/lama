@extends('admin::admin.layouts.master_admin')
@section('styles')
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/simple-mde.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <form class="needs-validation" novalidate="" method="post" action="{{route('admin.settings.update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="text-center">
                                    <img id="image-display" width="200px" height="70px" class="profile-user-img img-fluid img-circle" style="border: 1px solid #e5eaff;"
                                         alt="Settings picture" src="{{$data['settings']->image ? $data['settings']->image : url('assets/images/default.png')}}" >
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label float-right" for="name_ar">الإسم (باللغة العربية)</label>
                                    <textarea class="form-control" name="name_ar" id="name_ar" cols="20" rows="8" required>{{isset($data['settings']) ? $data['settings']->translateOrnew('ar')->name : ""}}</textarea>
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<label class="col-form-label float-right" for="name_en">الإسم (باللغة الانجليزية)</label>--}}
                                    {{--<textarea class="form-control" name="name_en" id="name_en" cols="20" rows="8" required>{{isset($data['settings']) ? $data['settings']->translateOrnew('en')->name : ""}}</textarea>--}}
                                    {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label class="col-form-label float-right" for="about_ar">عن التطبيق (باللغة العربية)</label>
                                    <textarea class="form-control" name="about_ar" id="about_ar" cols="20" rows="8" required>{{isset($data['settings']) ? $data['settings']->translateOrnew('ar')->about: ""}}</textarea>
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<label class="col-form-label float-right" for="about_en">عن التطبيق (باللغة الانجليزية)</label>--}}
                                    {{--<textarea class="form-control" name="about_en" id="about_en" cols="20" rows="8" required>{{isset($data['settings']) ? $data['settings']->translateOrnew('en')->about: ""}}</textarea>--}}
                                    {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label for="fast_delivery_fees">تكاليف التوصيل السريع</label>
                                    <input class="form-control btn-square" name="fast_delivery_fees" id="fast_delivery_fees" type="number" value="{{isset($data['settings']) ? $data['settings']->fast_delivery_fees: ""}}" required>
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col">
                                        <div class="custom-file">
                                            <input class="custom-file-input" name="image" id="image" type="file" data-original-title="" title="">
                                            <label class="custom-file-label" for="image">الصورة</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary float-right" type="submit">حفظ</button>
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">إغلاق</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('modals')

@endsection

@section('scripts')

{{--    <script src="{{ url('/assets/js/editor/simple-mde/simplemde.min.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/editor/simple-mde/simplemde.custom.js') }}"></script>--}}
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image-display').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function() {
            readURL(this);
        });
    </script>
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
@endsection
