@extends('admin::admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/owlcarousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <a href="{{route('admin.orders.invoice',['order_id' => $data['order']->id])}}" target="_blank" title="طباعة الطلب" style='color:green; font-size: x-large;'>
            طباعة الطلب
            <i class="fa fa-print"></i>
        </a> &nbsp;
        <div class="card">
            <div class="row product-page-main">
                <div class="col-xl-4" style='text-align:center'>
                    <img width="100%" src="{{!empty($data['order']->shop) ? $data['order']->shop->logo : url('assets/images/default.png')}}" alt=""/>
                    <a href="{{route('admin.shops.products.show',['shop_id' => $data['order']->shop->id])}}" >
                        <strong>المتجر : </strong>{{isset($data['order']->shop) ? $data['order']->shop->name : ''}}
                    </a>
                </div>
                <div class="col-xl-8">
                    <div class="product-page-details">
                        <div class="row">
                            <div class="col-md-6">
                                <strong>رقم الطلب : </strong><span style="color: #0b43c6;">{{isset($data['order']->order_number) ? $data['order']->order_number : ''}}</span>
                            </div>
                            <div class="col-md-6">
                                <strong>نوع التوصيل : </strong>
                                @if($data['order']->delivery_type == 0)
                                    <span class="badge badge-secondary" style="padding-left: 15px;">عادي</span>
                                @elseif($data['order']->delivery_type == 1)
                                    <span class="badge badge-primary" style="padding-left: 15px;">سريع</span>
                                @elseif($data['order']->delivery_type == 2)
                                    <span class="badge badge-primary" style="padding-left: 15px;">من الفرع</span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>صاحب الطلب : </strong>{{isset($data['order']->user) ? $data['order']->user->name : ''}}
                            </div>
                            <div class="col-md-6">
                            <!--<a href="{{route('admin.shops.products.show',['shop_id' => $data['order']->shop->id])}}">
                                    <strong>المتجر : </strong>{{isset($data['order']->shop) ? $data['order']->shop->name : ''}}
                                </a>-->
                                <strong>موبايل صاحب الطلب : </strong>{{isset($data['order']->user) ? $data['order']->user->phone : ''}}
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>تكلفة الطلب : </strong>{{isset($data['order']->total_cost) ? $data['order']->total_cost : ''}}
                                ريال
                            </div>
                            <div class="col-md-6">
                                <strong>العنوان : </strong>
                                @if (isset($data['order']->address))
                                    @if (isset($data['order']->address->address))
                                        {{$data['order']->address->address}}
                                    @else
                                        {{$data['order']->address->area}}
                                    @endif
                                @endif
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>رقم المبني : </strong>{{isset($data['order']->address->building_number) ? $data['order']->address->building_number : ''}}

                            </div>
                            <div class="col-md-6">
                                <strong>رقم الشقة : </strong>{{isset($data['order']->address->apartment) ? $data['order']->address->apartment : ''}}
                            </div>
                        </div>
                        <hr/>

                        <div class="row">
                            <div class="col-md-12">
                                <strong>عنوان اضافي : </strong>{{isset($data['order']->address->more_info) ? $data['order']->address->more_info : ''}}

                            </div>
                        </div>
                        <hr/>

                        <div class="row">
                            <div class="col-md-6">
                                <strong>مصاريف الشحن : </strong>
                                @if($data['order']->delivery_type==2)
                                    0
                                    @elseif($data['order']->delivery_type==1)
                                    {{$data['order']['fast_delivery'] }}

                                @else
                                {{$data['order']->shop->delivery_fees}}
                                @endif
                                ريال
                            </div>
                            <div class="col-md-6">
                                <strong>وقت التوصيل : </strong>{{isset($data['order']->delivery_time) ? $data['order']->delivery_time : 'غير محدد'}}
                            <!--دقيقة-->
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>إجمالي التكلفة : </strong>
                                @if($data['order']->delivery_type==2)
                                {{$data['order']->total_cost}}
                                @elseif($data['order']->delivery_type==1)
                                    {{$data['order']->total_cost + $data['order']['fast_delivery'] }}
                                @else
                                    {{($data['order']->total_cost + $data['order']->shop->delivery_fees)}}
                                @endif

                                ريال
                            </div>
                            <div class="col-md-6">
                                <strong>نوع الدفع : </strong>
                                @if($data['order']->payment == 0)
                                    <span class="badge badge-dark" style="padding-left: 15px;">نقدي</span>
                                @elseif($data['order']->payment == 1)
                                    <span class="badge badge-success" style="padding-left: 15px;">أونلاين</span>
                                @endif
                            </div>
                        </div>
                        <hr/>
                        @if($data['order']->delegate_id != NULL)
                            <div class="row">
                                <div class="col-md-6">
                                    <strong> اسم المندوب  : </strong>{{isset($data['order']->delegate) ? $data['order']->delegate->name : ''}}
                                </div>
                                <div class="col-md-6">
                                    <strong> موبايل المندوب  : </strong>{{isset($data['order']->delegate) ? $data['order']->delegate->phone : ''}}
                                </div>
                            </div>
                            <hr/>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <strong>  ملاحظات :
                                    {{($data['order']->notes)}}
                                </strong>
                            </div>

                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong> توقيت الطلب : </strong>
                                {{($data['order']->created_at_time)}}
                            </div>
{{--                            <div class="col-md-6">--}}
{{--                                <strong> طريقة استلام الطلب : </strong>--}}
{{--                                @if($data['order']->delivery_type == 0)--}}
{{--                                    <span class="badge badge-success">توصيل عادي</span>--}}
{{--                                @elseif($data['order']->delivery_type == 1)--}}
{{--                                    <span class="badge badge-success">توصيل سريع</span>--}}
{{--                                @elseif($data['order']->delivery_type == 2)--}}
{{--                                    <span class="badge badge-success">استلام من الفرع</span>--}}
{{--                                @endif--}}
{{--                            </div>--}}
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-8">
                                <strong> تقييم المتجر : </strong>
                                <span class="badge badge-dark" style="padding-left: 15px;">{{($data['order']->shop_rate)}}</span> - {{($data['order']->shop_comment)}}

                            </div>

                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-8">
                                <strong> تقييم المندوب : </strong>
                                <span class="badge badge-dark" style="padding-left: 15px;">{{($data['order']->user_rate)}}</span> - {{($data['order']->user_comment)}}

                            </div>

                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-8">
                                <strong> تقييم المستخدم : </strong>
                                <span class="badge badge-dark" style="padding-left: 15px;">{{($data['order']->delegate_rate)}}</span> - {{($data['order']->delegate_comment)}}

                            </div>

                        </div>
                        <hr/>

                    </div>

                    <div class="row">
                        <div class=" col text-center">
                            <button title="تعديل التقييمات" type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit_{{$data['order']->id}}">
                                <i class="fa fa-edit"></i>  تعديل التقييمات
                            </button>
                        </div>
                    </div>


{{--                    ///////////////////////////////--}}
                    <div class="modal fade" id="edit_{{$data['order']->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">تعديل التقييم</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form class="form-horizontal" method="post" action="{{route('admin.orders.rate')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="modal-body">
                                        <input type="hidden" name="order_id" value="{{$data['order']->id}}">


                                        <div class="form-group row ">
                                            <label class="col-lg-12 control-label text-lg-right" for="textinput">تقييم المتجر</label>
                                            <div class="col-lg-12">
                                                <input name="shop_rate" type="number" placeholder="تقييم المتجر" class="form-control btn-square" value="{{$data['order']->shop_rate}}" required>
                                            </div>
                                        </div>

                                        <div class="form-group row ">
                                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> التعليق علي النتجر</label>
                                            <div class="col-lg-12">
                                                <input name="shop_comment" type="text" placeholder=" التعليق علي النتجر" class="form-control btn-square"  value="{{$data['order']->shop_comment}}" required>
                                            </div>
                                        </div>

                                        <div class="form-group row ">
                                            <label class="col-lg-12 control-label text-lg-right" for="textinput">تقييم المندوب</label>
                                            <div class="col-lg-12">
                                                <input name="delegate_rate" type="number" placeholder="تقييم المندوب" class="form-control btn-square" value="{{$data['order']->user_rate}}" required>
                                            </div>
                                        </div>

                                        <div class="form-group row ">
                                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> التعليق علي المندوب</label>
                                            <div class="col-lg-12">
                                                <input name="delegate_comment" type="text" placeholder=" التعليق علي المندوب" class="form-control btn-square"  value="{{$data['order']->user_comment}}" required>
                                            </div>
                                        </div>

                                        <div class="form-group row ">
                                            <label class="col-lg-12 control-label text-lg-right" for="textinput">تقييم المندوب</label>
                                            <div class="col-lg-12">
                                                <input name="user_rate" type="number" placeholder="تقييم المندوب" class="form-control btn-square" value="{{$data['order']->delegate_raet}}" required>
                                            </div>
                                        </div>

                                        <div class="form-group row ">
                                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> التعليق علي المندوب</label>
                                            <div class="col-lg-12">
                                                <input name="user_comment" type="text" placeholder=" التعليق علي المندوب" class="form-control btn-square"  value="{{$data['order']->delegate_comment}}" required>
                                            </div>
                                        </div>

                                    </div>






                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-dark" data-dismiss="modal">اغلاق</button>
                                        <button class="btn btn-primary" type="submit">تعديل</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
{{--                    ///////////////////////////////--}}

                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h5>تفاصيل إضافية</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-9">
                        <thead>
                        <tr>
                            <th>الصورة</th>
                            <th>الإسم</th>
                            <th>الوصف</th>
                            <th>سعر المنتج</th>
                            <th>التفاصيل</th>
                            <th>الكمية المطلوبة</th>
                            <th>السعر</th>
                        </tr>
                        </thead>
                        <tbody>
                        @isset($data['order']->orderItems)
                            @foreach($data['order']->orderItems as $order_item)
                                <tr>
                                    <td><img class="img-60 rounded-circle" src="{{isset($order_item->product) ? (!empty($order_item->product->image) ? $order_item->product->image : url('assets/images/default.png')) : url('assets/images/default.png')}}" alt="#" data-original-title="" title=""></td>
                                    {{--                                    <td>{{isset($order_item->product) ? ($order_item->product->subCategory ? $order_item->product->subCategory->name : "") : ""}}</td>--}}
                                    <td>{{isset($order_item->product) ? $order_item->product->name : ""}}</td>
                                    <!--<td>{{isset($order_item->description) ? $order_item->description : ""}}</td>-->
                                    <td>{{isset($order_item->product) ? $order_item->product->short_description : ""}}</td><td>{{isset($order_item->product) ? ($order_item->product->has_discount == 1 ? $order_item->product->price_after : $order_item->product->price_before) : ""}}</td>
                                    <td>
                                        @isset($order_item->ordItemVars)
                                            @foreach($order_item->ordItemVars as $item_variation)
                                                <strong>{{$item_variation->variation_name}}</strong> : @if(ctype_xdigit(ltrim($item_variation->option_name, '#'))) <div style="width: 20px; height: 20px; display: inline-block; border-style: ridge; background-color: {{$item_variation->option_name}}"></div> @else {{$item_variation->option_name}} @endif
                                                <br><span>({{$item_variation->price/$order_item->quantity}}</span>
                                                <span>*</span>
                                                <span>{{$order_item->quantity}}</span>
                                                <span> =</span>
                                                <span>{{$item_variation->price}}</span>
                                                <span>ريال)</span>
                                                <br/>
                                                <br/>
                                            @endforeach
                                        @endisset
                                    </td>
                                    <td>{{isset($order_item->quantity) ? $order_item->quantity : ""}}</td>
                                    <td>{{isset($order_item->cost) ? $order_item->cost." ريال" : ""}}</td>
                                </tr>
                            @endforeach
                        @endisset
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('modals')

@endsection

@section('scripts')
    <script src="{{ url('/assets/js/owlcarousel/owl.carousel.js') }}"></script>
    <script src="{{ url('/assets/js/rating/jquery.barrating.js') }}"></script>
    <script src="{{ url('/assets/js/ecommerce.js') }}"></script>
    <script src="{{ url('/assets/js/jquery.printPage.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('#btnprn').printPage();
        });
    </script>
@section('scripts')
    <!-- Plugins JS start-->
    {{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
    {{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
    {{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
@endsection

