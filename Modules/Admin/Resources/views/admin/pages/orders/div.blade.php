<thead>
<tr>
    {{--                                    @if(Auth::guard('admin')->user()->can('delete_order'))--}}
    <button class="btn btn-square btn-danger" style="float: left; margin-right: 2%;" id="bulk_delete_btn" type="button" title="حذف متعدد">حذف متعدد</button>
    {{--                                    @endif--}}
    {{--                                     @if(Auth::guard('admin')->user()->can('export_order'))--}}
    {{--                                        <a href="{{route('admin.order.export',['type' => 0])}}" class="btn btn-square btn-success" style="float: left;" title="تحميل الملف">تحميل الملف</a>--}}
    {{--                                     @endif--}}
    <input type="hidden" name="myids" id="bulk_delete_input" value="">
    <br>
    <br>
    <th>
        <label class="d-block" for="chk-ani">
            <input class="checkbox_animated select_all" id="chk-ani" type="checkbox">
        </label>
    </th>
    <th>رقم الطلب</th>
    <th>لوجو المتجر</th>
    <th>اسم المتجر</th>
    <th>صاحب الطلب</th>
    <th>السعر الكلي</th>
    <th>نوع الدفع</th>
    <th>نوع التوصيل</th>
    <th style="width: 25%;">الحالة</th>
    <th style="width: 15%;">تاريخ الإنشاء</th>
    <th>العمليات</th>
</tr>
</thead>
<tbody>
@isset($data['orders'])
    @foreach($data['orders'] as $order)
        <tr>
            <td>
                <label class="d-block" for="chk-ani">
                    <input type="checkbox" class="checkbox_animated" name="row_id" id="checkbox_{{ $order->id }}" value="{{ $order->id }}">
                </label>
            </td>
            <td>{{isset($order->order_number) ? $order->order_number : ""}}</td>
            <td><img class="img-60 rounded-circle" src="{{!empty($order->shop->logo) ? $order->shop->logo : url('assets/images/default.png')}}" alt="#" data-original-title="" title=""></td>
            <td>{{isset($order->shop) ? $order->shop->name : ""}}</td>
            <td>{{isset($order->user) ? $order->user->name : ""}}</td>
            <td>{{isset($order->total_cost) ? $order->total_cost : ""}}</td>
            <td>
                @if($order->payment_type == 0)
                    <span class="badge badge-primary">كاش</span>
                @elseif($order->payment_type == 1)
                    <span class="badge badge-success">اونلاين</span>
                @endif
            </td>
            <td>
                @if($order->delivery_type == 0)
                    <span class="badge badge-primary">توصيل عادي</span>
                @elseif($order->delivery_type == 1)
                    <span class="badge badge-success">توصيل سريع</span>
                @endif
            </td>
            <td>
                <select @if(!Auth::guard('admin')->user()->can('edit_order'))  disabled @endif @if($order->delivery_type == 0) disabled @endif name="order_status" class="custom-select form-control order_status" data-order-id="{{$order->id}}" data-shop-id="{{$order->shop->id}}">
                    <option value="0" @if($order->status == 0) selected @endif>قيد الإنشاء</option>
                    <option value="1" @if($order->status == 1) selected @endif>قبول الطلب</option>
                    <option value="2" @if($order->status == 2) selected @endif>في الطريق</option>
                    <option value="3" @if($order->status == 3) selected @endif>تم التوصيل</option>
                    <option value="4" @if($order->status == 4) selected @endif>ملغي</option>
                </select>
            </td>
            <td>{{isset($order->created_at) ? $order->created_at : ""}}</td>
            <td>
                <div class="row">
                    <a href="{{route('admin.orders.details',['order_id' => $order->id])}}" title="تفاصيل الطلب">
                        <i width="20" height="20" color="blue" data-feather="eye"></i>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye" color="blue"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                    </a> &nbsp;
                    {{--                                                    @if(Auth::guard('admin')->user()->can('delete_order'))--}}
                    <a onclick='return deleteOrder({{$order->id}})' title="حذف" data-id="{{$order->id}}" href="#">
                        <i width="20" height="20" color="red" data-feather="trash-2"></i>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2" color="red"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                    </a>
                    {{--                                                    @endif--}}
                </div>
            </td>
        </tr>
    @endforeach
@endisset
</tbody>
