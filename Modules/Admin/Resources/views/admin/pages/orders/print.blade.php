<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
    body {
        /*background: grey;*/
        /*margin-top: 120px;*/
        /*margin-bottom: 120px;*/

    }
    .p-5 {
     padding: 1px 20px 0 20px !important;
     margin: 5px 20px 0 20px !important;
    }
    hr{
       padding:1px !important;
       margin:1px !important;
    }
    b, strong {
    float: right;
    padding-top : 5px;
    }
    th{ padding-left: 30px;}
    td{ padding-left: 30px;}
    .container-fluid{direction: rtl;}
    .card-header{display: flex;}
    .col-xl-12{direction: rtl;}
</style>
<body onload="window.print();" main-theme-layout="rtl">

    <div class="container">
    <div class="row">

        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row p-5">
                        <div class="col-md-6">

                            <img src="{{url('/assets/images/new-logo.jpeg')}}" alt="logo" style="width: 150px;">

                            <img src="{{url('print.png')}}" alt="printer" width='100px' height='100px' >

                            <img alt="" >
{{--                            <img src="{{$order->image ? $order->image->name : url('assets/images/user/3.jpg')}}" class="img-thumbnail img-fluid" alt="User Profile" width="100" height="100" />--}}
                        </div>

                        <div class="col-md-6 text-right" style="direction:rtl;display:block" style='margin:0px; padding:0px'>
                            <img width="30%" height='110px' style='margin:0px; padding:0px'
                    src="{{!empty($data['order']->shop) ? $data['order']->shop->logo : url('assets/images/default.png')}}" alt=""/>
                    <a href="{{route('admin.shops.products.show',['shop_id' => $data['order']->shop->id])}}" >
                                    <strong>المتجر : </strong>{{isset($data['order']->shop) ? $data['order']->shop->name : ''}}
                                </a>


                        </div>
                    </div>

                    <hr class="my-3">













    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="card" >
            <div class=" product-page-main" style='display: flex; flex:0 0 15%'>
                <div class="col-md-3" >
                    <div class="">
                        <strong>صورة المبني : </strong><br><br>
                        <img width="100%" height="170px"
                        src="{{isset($data['order']->address->address) ? $data['order']->address->building_image : url('assets/images/default.png')}}"
                        alt=""/>
                    </div>
                </div>
                <div class="col-md-9" >
                    <div class="">
                        <div class="row">
                            <div class="col-md-6">

                                <strong>رقم الطلب : </strong>
                                <span style="color: #0b43c6;">{{isset($data['order']->order_number) ? $data['order']->order_number : ''}}</span>
                            </div>
                            <div class="col-md-6">
                                <strong>نوع التوصيل : </strong>
                                @if($data['order']->delivery_type == 0)
                                    <span class="badge badge-secondary" style="padding-left: 15px;">عادي</span>
                                @elseif($data['order']->delivery_type == 1)
                                    <span class="badge badge-primary" style="padding-left: 15px;">سريع</span>
                                @elseif($data['order']->delivery_type == 2)
                                    <span class="badge badge-primary" style="padding-left: 15px;">من الفرع</span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>صاحب الطلب : </strong>{{isset($data['order']->user) ? $data['order']->user->name : ''}}
                            </div>
                            <div class="col-md-6">
                                <!--<a href="{{route('admin.shops.products.show',['shop_id' => $data['order']->shop->id])}}">
                                    <strong>المتجر : </strong>{{isset($data['order']->shop) ? $data['order']->shop->name : ''}}
                                </a>-->
                                <strong>موبايل صاحب الطلب : </strong>{{isset($data['order']->user) ? $data['order']->user->phone : ''}}
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>تكلفة الطلب : </strong>{{isset($data['order']->total_cost) ? $data['order']->total_cost : ''}}
                                ريال
                            </div>
                            <div class="col-md-6">
                                <strong>العنوان : </strong>
                                @if (isset($data['order']->address))
                                    @if (isset($data['order']->address->address))
                                        {{$data['order']->address->address}}
                                    @else
                                        {{$data['order']->address->area}}
                                    @endif
                                @endif
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>رقم المبني : </strong>{{isset($data['order']->address->building_number) ? $data['order']->address->building_number : ''}}

                            </div>
                            <div class="col-md-6">
                                <strong>رقم الشقة : </strong>{{isset($data['order']->address->apartment) ? $data['order']->address->apartment : ''}}
                            </div>
                        </div>
                        <hr/>

                        <div class="row">
                            <div class="col-md-12">
                                <strong>عنوان اضافي : </strong>{{isset($data['order']->address->more_info) ? $data['order']->address->more_info : ''}}

                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>مصاريف الشحن : </strong>
                                @if($data['order']->delivery_type==2)
                                    0
                                @elseif($data['order']->delivery_type==1)
                                    {{$data['order']['fast_delivery'] }}

                                @else
                                    {{$data['order']->shop->delivery_fees}}
                                @endif
                                ريال
                            </div>
                            <div class="col-md-6">
                                <strong>وقت التوصيل : </strong>{{isset($data['order']->delivery_time) ? $data['order']->delivery_time : 'غير محدد'}}
                                <!--دقيقة-->
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-12">
                                <strong style="color: red">  ملاحظات :
                                    {{($data['order']->notes)}}
                                </strong>
                            </div>

                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>إجمالي التكلفة : </strong>
                                @if($data['order']->delivery_type==2)
                                    {{$data['order']->total_cost}}
                                @elseif($data['order']->delivery_type==1)
                                    {{$data['order']->total_cost + $data['order']['fast_delivery'] }}
                                @else
                                    {{($data['order']->total_cost + $data['order']->shop->delivery_fees)}}
                                @endif

                                ريال
                            </div>
                            <div class="col-md-6">
                                <strong>نوع الدفع : </strong>
                                @if($data['order']->payment == 0)
                                    <span class="badge badge-dark" style="padding-left: 15px;">كاش</span>
                                @elseif($data['order']->payment == 1)
                                    <span class="badge badge-success" style="padding-left: 15px;">أونلاين</span>
                                @endif
                            </div>
                        </div>
                        <hr/>
                        @if($data['order']->delegate_id != NULL)
                        <div class="row">
                            <div class="col-md-6">
                                <strong> اسم المندوب  : </strong>{{isset($data['order']->delegate) ? $data['order']->delegate->name : ''}}
                            </div>
                            <div class="col-md-6">
                                <strong> موبايل المندوب  : </strong>{{isset($data['order']->delegate) ? $data['order']->delegate->phone : ''}}
                            </div>
                        </div>
                        <hr/>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <strong> توقيت الطلب :
                                    {{($data['order']->created_at_time)}}
                                </strong>
                            </div>
{{--                            <div class="col-md-6">--}}
{{--                                <strong> طريقة استلام الطلب : </strong>--}}
{{--                                @if($data['order']->delivery_type == 0)--}}
{{--                                    <span class="badge badge-success">توصيل عادي</span>--}}
{{--                                @elseif($data['order']->delivery_type == 1)--}}
{{--                                    <span class="badge badge-success">توصيل سريع</span>--}}
{{--                                @elseif($data['order']->delivery_type == 2)--}}
{{--                                    <span class="badge badge-success">استلام من الفرع</span>--}}
{{--                                @endif--}}
{{--                            </div>--}}

                        </div>
                        <hr/>
                </div>
            </div>
            </div>
        </div>
                        <div class="card">
                            <div class="card-header" style='padding:0 5px; margin:0 5px'>
                                <h5 >تفاصيل إضافية</h5>
                            </div>
                            <div class="card-body"  style='padding: 0 5px; margin:0 5px'>
                                <div class="table-responsive">
                                    <table class="display" id="basic-9">
                                        <thead>
                                        <tr style='padding:0; margin:0'>
                                            <th>الصورة</th>
                                            <th>الإسم</th>
                                            <th>الوصف</th>
                                            <th>سعر المنتج</th>
                                            <th>التفاصيل</th>
                                            <th>الكمية المطلوبة</th>
                                            <th>السعر</th>
                                        </tr>
                                        </thead>
                                        <tbody  style='padding:0; margin:0'>
                                        @isset($data['order']->orderItems)
                                            @foreach($data['order']->orderItems as $order_item)
                                                <tr  style='padding:0; margin:0'>
                                                    <td><img width="50%" class="img-60 rounded-circle" src="{{isset($order_item->product) ? (!empty($order_item->product->image) ? $order_item->product->image : url('assets/images/default.png')) : url('assets/images/default.png')}}" alt="#" data-original-title="" title=""></td>
                                                    {{--                                    <td>{{isset($order_item->product) ? ($order_item->product->subCategory ? $order_item->product->subCategory->name : "") : ""}}</td>--}}
                                                    <td>{{isset($order_item->product) ? $order_item->product->name : ""}}</td>
                                                    <!--<td>{{isset($order_item->description) ? $order_item->description : ""}}</td>-->
                                                    <td>{{isset($order_item->product) ? $order_item->product->short_description : ""}}</td><td>{{isset($order_item->product) ? ($order_item->product->has_discount == 1 ? $order_item->product->price_after : $order_item->product->price_before) : ""}}</td>
                                                    <td>{{isset($order_item->product) ? ($order_item->product->has_discount == 1 ? $order_item->product->price_after : $order_item->product->price_before) : ""}}</td>
                                                    <td>
                                                        @isset($order_item->ordItemVars)
                                                            @foreach($order_item->ordItemVars as $item_variation)
                                                                <strong>{{$item_variation->variation_name}}</strong> : @if(ctype_xdigit(ltrim($item_variation->option_name, '#'))) <div style="width: 20px; height: 20px; display: inline-block; border-style: ridge; background-color: {{$item_variation->option_name}}"></div> @else {{$item_variation->option_name}} @endif
                                                                <br><span>({{$item_variation->price/$order_item->quantity}}</span>
                                                                <span>*</span>
                                                                <span>{{$order_item->quantity}}</span>
                                                                <span> =</span>
                                                                <span>{{$item_variation->price}}</span>
                                                                <span>ريال)</span>
                                                                <br/>
                                                                <br/>
                                                            @endforeach
                                                        @endisset
                                                    </td>
                                                    <td>{{isset($order_item->quantity) ? $order_item->quantity : ""}}</td>
                                                    <td>{{isset($order_item->cost) ? $order_item->cost." ريال" : ""}}</td>
                                                </tr>
                                            @endforeach
                                        @endisset
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Container-fluid Ends-->























                </div>



            </div>
        </div>
    </div>
</div>
</div>




</body>
