@extends('admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>تعديل المستخدم</h5>
                    </div>
                    <form class="form theme-form" method="POST" action="{{route('admin.users.update',['user_id' => $data['user']])}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">

                            <div class="form-group">
                                <input  type="hidden" class="form-control"  name="lat" value="" id="userlat" placeholder="Enter lat ">
                            </div>
                            <div class="form-group">
                                <input  type="hidden" class="form-control"  name="lng" value="" id="userlng" placeholder="Enter long">
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <div id="userlocation" style="width:100%;height:350px"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="autocomplete">البحث عن عنوان</label>
                                        <input type="text" class="form-control" id="autocomplete" placeholder="البحث عن عنوان" >                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name">إسم مستخدم</label>
                                        <input class="form-control btn-square" value="{{isset($data['user']->name) ? $data['user']->name : ""}}" name="name" id="name" type="text" placeholder="اسم المستخدم" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="phone">رقم الهاتف</label>
                                        <input class="form-control btn-square" value="{{isset($data['user']->phone) ? $data['user']->phone : ""}}" name="phone" id="phone" type="text" placeholder="رقم الهاتف" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="email">البريد الإلكتروني</label>
                                        <input class="form-control btn-square" value="{{isset($data['user']->email) ? $data['user']->email : ""}}" name="email" id="email" type="email" placeholder="البريد الإلكتروني" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <div class="col-form-label">المدينة</div>
                                        <select class="js-example-basic-single col-sm-12" name="city" required>
                                            <option disabled selected>من فضلك اختر المدينة</option>
                                            @isset($data['cities'])
                                                @foreach($data['cities'] as $city)
                                                    <option @if($city->id == $data['user']->city_id) selected @endif value="{{$city->id}}">{{$city->name}}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="status">حالة المستخدم</label>
                                <div class="media-body icon-state switch-outline">
                                    <label class="switch">
                                        <input type="checkbox" @if($data['user']->status == 1) checked @endif id="status" name="status"><span class="switch-state bg-success"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="text-center">
                                    <img id="image-display" width="100px" height="100px" class="profile-user-img img-fluid img-circle" style="border: 1px solid #e5eaff;"
                                         alt="Admin profile picture" src="{{$data['user']->image ? $data['user']->image : url('assets/images/user/3.jpg')}}" >
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="image">إختر صورة</label>
                                        <input class="form-control btn-square" name="image" id="image" type="file">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
    <!-- Plugins JS start-->
    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('userlocation'), {
                zoom: 7,
                center: {lat: {{$data['user']->latitude}}, lng: {{$data['user']->longitude}} }
            });
            var MaekerPos = new google.maps.LatLng({{$data['user']->latitude}} , {{$data['user']->longitude}});
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("userlat").value=e.latLng.lat();
                document.getElementById("userlng").value=e.latLng.lng();
            });
        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
    </script>
@endsection
