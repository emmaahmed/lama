@extends('admin::admin.layouts.master_admin')

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$data['title']}}</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.offers.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group">
{{--                                        <label class="col-form-label float-right" for="product_id">المتاجر</label>--}}
                                        <select name="shop_id" class="custom-select form-control js-example-basic-multiple" id="shops" required>
                                            <option value="" disabled selected>اختر المتجر</option>
                                            @foreach($data['shops'] as $shop)
                                                <option shop_id="{{$shop->id}}" value="{{$shop->id}}" {{ (old("shop_id") == $shop->id ? "selected":"") }}>{{$shop->name}} / {{$shop->email}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                <div class="col">
                                    <div class="form-group">


{{--                                        <label class="col-form-label float-right" for="product_id">المنتجات</label>--}}
                                        <select name="product_id" class="custom-select form-control js-example-basic-multiple" id="product_id" required>
                                            <option value="" disabled selected>اختر المنتج</option>
                                            @foreach($data['products'] as $product)
                                                <option value="{{$product->id}}" {{ (old("product_id") == $product->id ? "selected":"") }}>{{$product->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="form-row">
                                <div class="form-group">
                                    <input class="form-control btn-square" value="{{old('expire_at')}}" name="expire_at" id="date" type="date" required="" placeholder='تاريخ انتهاء اشتراك المتجر'>
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    <div class="valid-feedback">بيانات صحيحة</div>
                                </div>

                                <div class="col">
                                    <div class="form-group">

                                    <div class="custom-file">
                                        <label  for="image">صورة العرض</label>

                                        <input class="custom-file-input" value="{{old('image')}}" name="image" id="image" type="file" required="" data-original-title="" title="">
                                        <label class="custom-file-label" for="image">صورة العرض</label>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>

{{--    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>--}}
    <script>
        $(document).on('change', '#shops', function(){

            var shop_id = $(this).val();
            // alert(shop_id);
            //alert('dddd');
            /*alert(service_id);*/
            if(shop_id){
                $.ajax({

                    type: "POST",
                    url: "{{URL::route('getOffers')}}",
                    data: {
                        shop_id: shop_id,
                        "_token": "{{ csrf_token() }}"
                    },
                    // type:"GET",
                    // url:"{{url('/get/offers')}}?doctor_id="+doctor_id,
                    success:function(res){
                        if(res){
                            //console.log(res);
                            $("#product_id").empty();
                            $("#product_id").append('<option value="" disabled selected>اختر المنتج</option>');
                            $.each(res,function(key,value){
                                $("#product_id").append('<option value="'+value.id+'" >'+value.name+'</option>');
                            });
                        }
                        if(res.length === 0){
                            $("#product_id").empty();
                        }
                    }
                });
            }else{
                $("#product_id").empty();
            }
        });
    </script>
@endsection

