@extends('admin::admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>تعديل القسم</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.offers.update',['offer_id' => $data['offer']])}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="text-center">
                                <img id="image-display" width="200px" height="70px" class="profile-user-img img-fluid img-circle" style="border: 1px solid #e5eaff;"
                                     alt="Slider picture" src="{{$data['offer']->image ? $data['offer']->image : url('assets/images/default.png')}}" >
                            </div>

                            <div class="row">
                                <div class="form-group">
                                        <label class="col-form-label float-right" for="product_id">المتاجر</label>
                                        <select name="shop_id" class="custom-select form-control js-example-basic-multiple" id="shops" required>
                                            <option value="" disabled selected>اختر المتجر</option>
                                            @foreach($data['shops'] as $shop)
                                                <option value="{{$shop->id}}" {{ ($data['offer']->shop_id == $shop->id ? "selected":"") }}>{{$shop->name}} / {{$shop->email}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="product_id">المتاجر</label>
                                        <select name="product_id" class="custom-select form-control js-example-basic-multiple" id="product_id" required>
                                            <option value="" disabled selected>اختر المتجر</option>
                                            @foreach($data['products'] as $product)
                                                <option value="{{$product->id}}"  @if($data['offer']->product_id == $product->id) selected @endif>{{$product->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="form-group">
                                    <input class="form-control btn-square" value="{{\Carbon\Carbon::parse($data['offer']->expire_at)->toDateString()}}" name="expire_at" id="date" type="date" required="" >
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                    <div class="valid-feedback">بيانات صحيحة</div>
                                </div>

                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="image" id="image" type="file" data-original-title="" title="">
                                        <label class="custom-file-label" for="image">صورة المنتج</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <!-- Plugins JS start-->

    <script>
        $(document).on('change', '#shops', function(){

            var shop_id = $(this).val();
            // alert(shop_id);
            //alert('dddd');
            /*alert(service_id);*/
            if(shop_id){
                $.ajax({

                    type: "POST",
                    url: "{{URL::route('getOffers')}}",
                    data: {
                        shop_id: shop_id,
                        "_token": "{{ csrf_token() }}"
                    },
                    // type:"GET",
                    // url:"{{url('/get/offers')}}?doctor_id="+doctor_id,
                    success:function(res){
                        if(res){
                            //console.log(res);
                            $("#product_id").empty();
                            $("#product_id").append('<option value="" disabled selected>اختر المنتج</option>');
                            $.each(res,function(key,value){
                                $("#product_id").append('<option value="'+value.id+'" >'+value.name+'</option>');
                            });
                        }
                        if(res.length === 0){
                            $("#product_id").empty();
                        }
                    }
                });
            }else{
                $("#product_id").empty();
            }
        });
    </script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image-display').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function() {
            readURL(this);
        });
    </script>
    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('workerlocation'), {
                zoom: 7,
                center: {lat: 26.719517, lng: 29.2161655 }
            });
            var MaekerPos = new google.maps.LatLng(0 , 0);
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("workerlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("workerlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("workerlat").value=e.latLng.lat();
                document.getElementById("workerlng").value=e.latLng.lng();
            });
        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
    </script>
@endsection
