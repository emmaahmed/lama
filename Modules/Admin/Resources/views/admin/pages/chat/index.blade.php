@extends('admin::admin.layouts.master_admin')
@section('content')
    <!-- Right sidebar Ends-->
    <div class="page-body" style="padding-top: 0;margin-top: 0">


{{--        <div class="container-fluid">--}}
{{--            <div class="page-header">--}}
{{--                <div class="row">--}}
{{--                    <div class="col">--}}
{{--                        <div class="page-header-left">--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}


        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if( count($users) > 0)

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <div class="row">
                                        <form method="post" enctype="multipart/form-data" action="{{route('admin.chat.search')}}">
                                            @csrf
                                            <div class="col-lg-6">
                                                <input name="name" class="form form-control"
                                                       type="text" placeholder="بحث عن اسم المتجر">
                                            </div>
                                            <div class="col-lg-4">
                                                <button class="btn btn-square btn-success " style=""
                                                        type="submit" title="بحث">بحث</button>
                                            </div>
                                        </form>
                                    </div>

                                    <thead>
                                    <tr>
                                        <th>المسلسل</th>
                                        <th>صوره العميل</th>
                                        <th>إسم العميل</th>
                                        <th>أخر رسالة من العميل</th>
                                        {{--                                        <th>حالة الرسالة</th>--}}
                                        <th>العمليات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $index=>$user)
                                        <tr>
                                            <td>{{$index + 1}}</td>
                                            <td><img src=" {{ $user->image }}" style="width: 50px" alt=""></td>
                                            <td>{{$user->name}}</td>
                                            @if(!empty($user['messages']->first()['message']))
                                                <td>{{$user['messages']->first()['message'] ?: "-"}}</td>
                                            @else
                                                <td>
                                                    @if(!empty($user['messages']->first()['image']))
                                                    <img src="{{$user['messages']->first()['image'] ?: "-"}}" style="width: 50px">
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            @endif
                                            <td>
                                                <a href="{{route('admin.reply',['id'=>$user->id])}}"
                                                   title="مشاهده الرسائل" class="btn btn-success btn-sm"><i
                                                        class="fa fa-send"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد رسائل </h4>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection
