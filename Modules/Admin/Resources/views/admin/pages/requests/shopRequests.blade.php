@extends('admin::admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
{{--                    <div class="card-header">--}}
{{--                        <h5>{{$data['title']}}</h5>--}}
{{--                    </div>--}}
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>اللوجو</th>

                                    <th>الإسم</th>
                                    <th>اسم المتجر</th>
                                    <th>البريد الالكتروني</th>
                                    <th>رقم الهاتف</th>
                                    <th> العنوان</th>
                                    <th> الموقع علي الحريطة</th>
                                    <th>الحالة</th>
                                    {{--<th>مصاريف الشحن</th>--}}
                                    {{--<th>مواعيد العمل</th>--}}
                                    <th>السجل التجاري</th>
                                    <th>الرخصة</th>
                                    <th>الهوية</th>
                                    <th>صورة المحل من الداخل</th>
                                    <th>صورة المحل من الخارج</th>
                                    <th>تنزيل الوثيقة</th>
                                    <th>التاريخ</th>

                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['shops'] as $shop)
                                    <tr style="background-color:{{$shop->expire_at != null && $shop->expire_at < date('Y-m-d') ? '#FEACAC' : '' }}">
                                        <td>
                                            {{isset($shop->id) ? $shop->id : ""}}
                                        </td>
                                        <td>
                                            @if($shop->image != null)
                                                <button title="عرض" type="button" class="btn btn-success" style="padding: 2px"
                                                        data-toggle="modal" data-target="#image{{$shop->id}}">
                                                    <img src="{{$shop->image}}" class="image_radius" width="50px" height="50px">
                                                </button>
                                                <div class="modal fade" id="image{{$shop->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <img src="{{$shop->image}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </td>


                                        <td>{{isset($shop->name) ? $shop->name : ""}}</td>
                                        <td>{{isset($shop->shop_name) ? $shop->shop_name : ""}}</td>
                                        <td>{{isset($shop->email) ? $shop->email : ""}}</td>
                                        <td>{{isset($shop->phone) ? $shop->phone : ""}}</td>
                                        <td>{{isset($shop->address) ? $shop->address : ""}}</td>
                                        <td>
                                            <a target="_blank" href="https://www.google.com/maps/search/?api=1&query={{$shop->latitude}},{{$shop->longitude}}"><i class="icon-location-pin fa fa-2x"></i></a>
                                        </td>

                                        <td>
                                            <div class="form-group">
                                                <div class="media-body icon-state switch-outline">
                                                    <label class="switch">
                                                        <input type="checkbox" id="status" class="shop_status" data-shop-id="{{$shop->id}}" name="status" @if($shop->status == 1) checked @endif><span class="switch-state bg-success"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>


                                        <td>
                                            @if($shop->commercial_register != null)
                                                <button title="عرض" type="button" class="btn btn-success" style="padding: 2px"
                                                        data-toggle="modal" data-target="#commercial_register{{$shop->id}}">
                                                    <img src="{{$shop->commercial_register}}" class="image_radius" width="50px" height="50px">
                                                </button>
                                                <div class="modal fade" id="commercial_register{{$shop->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <img src="{{$shop->commercial_register}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </td>
                                        <td>
                                            @if($shop->license != null)
                                                <button title="عرض" type="button" class="btn btn-success" style="padding: 2px"
                                                        data-toggle="modal" data-target="#license{{$shop->id}}">
                                                    <img src="{{$shop->license}}" class="image_radius" width="50px" height="50px">
                                                </button>
                                                <div class="modal fade" id="license{{$shop->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <img src="{{$shop->license}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </td>
                                        <td>
                                            @if($shop->id_image != null)
                                                <button title="عرض" type="button" class="btn btn-success" style="padding: 2px"
                                                        data-toggle="modal" data-target="#id_image{{$shop->id}}">
                                                    <img src="{{$shop->id_image}}" class="image_radius" width="50px" height="50px">
                                                </button>
                                                <div class="modal fade" id="id_image{{$shop->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <img src="{{$shop->id_image}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </td>
                                        <td>
                                            @if($shop->inner_image != null)
                                                <button title="عرض" type="button" class="btn btn-success" style="padding: 2px"
                                                        data-toggle="modal" data-target="#inner_image{{$shop->id}}">
                                                    <img src="{{$shop->inner_image}}" class="image_radius" width="50px" height="50px">
                                                </button>
                                                <div class="modal fade" id="inner_image{{$shop->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <img src="{{$shop->inner_image}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </td>
                                        <td>
                                            @if($shop->outer_image != null)
                                                <button title="عرض" type="button" class="btn btn-success" style="padding: 2px"
                                                        data-toggle="modal" data-target="#outer_image{{$shop->id}}">
                                                    <img src="{{$shop->outer_image}}" class="image_radius" width="50px" height="50px">
                                                </button>
                                                <div class="modal fade" id="outer_image{{$shop->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <img src="{{$shop->outer_image}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </td>
                                        <td><a href="{{$shop->document}}" download="" target="_blank">أضغط هنا</a></td>
                                        <td>{{$shop->created_at}}</td>
                                        @if($shop->status==0)
                                        <td> <a onclick='return deleteRequest({{$shop->id}})' title="حذف" data-id="{{$shop->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a></td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection


@section('scripts')
    <!-- Plugins JS start-->
{{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>
        function deleteRequest(id)
        {
            var id = id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.request.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
            //
        }

        $('.shop_status').on('change.bootstrapSwitch', function(e) {
            var shop_id = $(this).attr('data-shop-id');
            var status = "";
            if (e.target.checked == true){
                var status = 1;
            }else{
                var status = 0;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('admin.shopRequests.status')}}",
                data: {
                    shop_id: shop_id,
                    status: status,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response){
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });





    </script>
@endsection
