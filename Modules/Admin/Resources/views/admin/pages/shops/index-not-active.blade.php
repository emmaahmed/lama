@extends('admin::admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
{{--                    <div class="card-header">--}}
{{--                        <h5>{{$data['title']}}</h5>--}}
{{--                    </div>--}}
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    @if(Auth::guard('admin')->user()->can('add_shop'))
                                    <a class="btn btn-square btn-success" href="{{route('admin.shops.create')}}" title="إضافة"> إضافة متجر جديد</a> &nbsp; &nbsp;
                                    @endif
                                    @if(Auth::guard('admin')->user()->can('delete_shop'))
                                        <button class="btn btn-square btn-danger" style="float: left; margin-right: 2%;" id="bulk_delete_btn" type="button" title="حذف متعدد">حذف متعدد</button>
                                    @endif
                                    <input type="hidden" name="myids" id="bulk_delete_input" value="">
                                    <br>
                                    <br>
                                    <th>
                                        <label class="d-block" for="chk-ani">
                                            <input class="checkbox_animated select_all" id="chk-ani" type="checkbox">
                                        </label>
                                    </th>
                                    <th>اللوجو</th>
                                    <th>المدينة</th>
                                    <th>القسم التابع له</th>
                                    <th>الإسم</th>
                                    <th>البريد الإلكتروني</th>
                                    <th>رقم الهاتف</th>
                                    <th>تاريخ انتهاء الاشتراك</th>
                                    <th>الحالة</th>
                                    {{--<th>مصاريف الشحن</th>--}}
                                    {{--<th>مواعيد العمل</th>--}}
                                    <th>التقييم</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($data['shops'] as $shop)
{{--                                    @php--}}
{{--                                        dd($shop->added_days,date('Y-m-d'));--}}
{{--                                    @endphp--}}
                                    <tr style="background-color:#FEACAC">
                                        <td>
                                            <label class="d-block" for="chk-ani">
                                                <input type="checkbox" class="checkbox_animated" name="row_id" id="checkbox_{{ $shop->id }}" value="{{ $shop->id }}">
                                            </label>
                                        </td>
                                        <td><img class="img-60 rounded-circle" src="{{!empty($shop->logo) ? $shop->logo : url('assets/images/default.png')}}" alt="#" data-original-title="" title=""></td>
                                        <td>{{isset($shop->city) ? $shop->city->name : ""}}</td>
                                        <td>{{isset($shop->category) ? $shop->category->name : ""}}</td>
                                        <td>{{isset($shop->name) ? $shop->name : ""}}</td>
                                        <td>{{isset($shop->email) ? $shop->email : ""}}</td>
                                        <td>{{isset($shop->phone) ? $shop->phone : ""}}</td>
                                        <td>{{isset($shop->expire_at) ? $shop->expire_at : ""}}</td>
                                        <td>
                                            <div class="form-group">
                                                <div class="media-body icon-state switch-outline">
                                                    <label class="switch">
                                                        <input type="checkbox" id="status" class="shop_status" data-shop-id="{{$shop->id}}" name="status" @if($shop->status == 1) checked @endif><span class="switch-state bg-success"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
{{--                                        <td>{{isset($shop->delivery_fees) ? $shop->delivery_fees.' ريال' : ""}}</td>--}}
                                        {{--<td>--}}
                                            {{--مفتوح من الساعة--}}
                                            {{--<span class="badge badge-success">{{$shop->from}}</span>--}}
                                            {{--حتي الساعة--}}
                                            {{--<span class="badge badge-success">{{$shop->to}}</span>--}}
                                        {{--</td>--}}
                                        <td>
                                            <div class="rating-container">
                                                <div class="br-wrapper br-theme-fontawesome-stars">
                                                    <div class="br-widget">
                                                        @for ($i = 0; $i < 5; ++$i)
                                                            <a href="#" data-rating-value="1" data-rating-text="1" class="{{ $shop->rate<=$i?'':'br-selected' }}"></a>
                                                        @endfor
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="row">
                                                @if(Auth::guard('admin')->user()->can('show_sub_category'))
                                                    <a href="{{route('admin.shops.subCategories',['shop_id' => $shop->id])}}" title="الأقسام"> <i width="20" height="20" color="black" data-feather="list"></i></a> &nbsp;
                                                @endif
                                                @if(Auth::guard('admin')->user()->can('show_product'))
                                                    <a href="{{route('admin.shops.products.show',['shop_id' => $shop->id])}}" title="المنتجات"> <i width="20" height="20" color="blue" data-feather="eye"></i></a> &nbsp;
                                                @endif
                                                    @if(Auth::guard('admin')->user()->can('edit_shop'))
                                                    <a title="تعديل" href="{{route('admin.shops.edit',['shop_id' => $shop->id])}}"><i width="20" height="20" color="green" data-feather="edit"></i></a> &nbsp;
                                                @endif
                                                @if(Auth::guard('admin')->user()->can('delete_shop'))
                                                    <a onclick='return deleteShop({{$shop->id}})' title="حذف" data-id="{{$shop->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a>
                                                @endif
                                                @if(Auth::guard('admin')->user()->can('delete_shop'))
                                                        <button title="عرض التعليقات" type="button" class="btn btn-success" style="padding: 2px"
                                                                data-toggle="modal" data-target="#comments{{$shop->id}}">
                                                            <i width="20" height="20" color="black" data-feather="list"></i>
                                                        </button>
                                                        <div class="modal fade" id="comments{{$shop->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog modal-lg" role="document">
                                                                <div class="modal-content">
                                                                    <div class="container-fluid">
                                                                        <div class="row">
                                                                            <div class="col-xl-12 col-md-12">
                                                                                <div class="card">
                                                                                    <div class="card-header">
                                                                                        <h5>كل التعليقات</h5>
                                                                                    </div>
                                                                                    <div class="card-body chart-block">
                                                                                        <div class="table-responsive">
                                                                                            <table class="display" id="basic-9">
                                                                                                <thead>
                                                                                                <tr>
                                                                                                    <th>رقم </th>
                                                                                                    <th>التعليق</th>
                                                                                                    <th style="width: 15%;">تاريخ الإنشاء</th>
                                                                                                    <th style="width: 15%;">العمليات</th>
                                                                                                </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                @isset($shop->comments)
                                                                                                    @foreach($shop->comments as $comment)
                                                                                                        <tr>

                                                                                                            <td>{{isset($comment->id) ? $comment->id : ""}}</td>
                                                                                                            <td>{{isset($comment->comment) ? $comment->comment : ""}}</td>
                                                                                                            <td>{{isset($comment->created_at) ? $comment->created_at : ""}}</td>
                                                                                                            <td><a onclick='return deleteComment({{$comment->id}})' title="حذف التعليق" data-id="{{$comment->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a></td>
                                                                                                        </tr>
                                                                                                    @endforeach
                                                                                                @endisset
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection


@section('scripts')
    <!-- Plugins JS start-->
{{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>
        $('.shop_status').on('change.bootstrapSwitch', function(e) {
            var shop_id = $(this).attr('data-shop-id');
            var status = "";
            if (e.target.checked == true){
                var status = 1;
            }else{
                var status = 0;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('admin.shops.status')}}",
                data: {
                    shop_id: shop_id,
                    status: status,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response){
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });
        function deleteShop(shop_id)
        {
            var id = shop_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.shops.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {shop_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
        //
        }

        function deleteComment(comment_id)
        {
            var id = comment_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.shops.deleteComment')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {comment_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
            //
        }

        $(document).ready(function(){
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        if (value !== 'on'){
                            myids.push(value);
                        }
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.shops.deleteMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

            // End Bulk Delete
        });
    </script>
@endsection
