@extends('admin::admin.layouts.master_admin')

@section('styles')
    <style>
        /*body {*/
            /*padding: 20px;*/
        /*}*/
        .image-area {
            position: relative;
            width: 15%;
            background: #fff;
            margin-bottom: 1%;
        }
        .image-area img{
            width: 100%;
            height: 75%;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            /*background: #E54E4E;*/
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }
    </style>

    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/timepicker.css') }}">
@endsection

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$data['title']}}</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.shops.update',['shop_id' => $data['shop']])}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="text-center">
                                <img id="image-display" width="200px" height="70px" class="profile-user-img img-fluid img-circle" style="border: 1px solid #e5eaff;"
                                     alt="لوجو المتجر" src="{{$data['shop']->logo ? $data['shop']->logo : url('assets/images/default.png')}}" >
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="city_id">المدن</label>
                                        <select name="city_id" class="custom-select form-control" id="city_id" required>
                                            <option value="" disabled selected>اختر المدينة</option>
                                            @foreach($data['cities'] as $city)
                                                <option value="{{$city->id}}"  @if($data['shop']->city_id == $city->id) selected @endif>{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="category_id">الأقسام</label>
                                        <select name="category_id" class="custom-select form-control" id="category_id" required>
                                            <option value="" disabled selected>اختر القسم</option>
                                            @foreach($data['categories'] as $category)
                                                <option value="{{$category->id}}"  @if($data['shop']->category_id == $category->id) selected @endif>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input  type="hidden" class="form-control"  name="longitude" value="{{$data['shop']->longitude}}" id="userlng" placeholder="Enter lat ">
                            </div>
                            <div class="form-group">
                                <input  type="hidden" class="form-control"  name="latitude" value="{{$data['shop']->latitude}}" id="userlat" placeholder="Enter long">
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <div id="userlocation" style="width:100%;height:350px"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="autocomplete">البحث عن عنوان</label>
                                        <input type="text" class="form-control" id="autocomplete" placeholder="البحث عن عنوان" name="soft_address" value="{{old('soft_address')}}"  >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="latlng">البحث عن خطوط الطول و العرض</label>
                                        <input type="text" class="form-control" id="latlng" name="latlng" value="{{$data['shop']->latitude}},{{$data['shop']->longitude}}" placeholder="البحث عن الطول و العرض" >
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="submitLatLng">الظهور علي الخريطة</label>

                                        <input type="button" class="form-control" id="submitLatLng" value="بحث" >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">إسم المتجر (باللغة العربية)</label>
                                        <input class="form-control btn-square" name="name_ar" id="name_ar" type="text" placeholder="إسم المتجر باللغة العربية" value="{{isset($data['shop']->translate('ar')->name) ? $data['shop']->translate('ar')->name : ""}}" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="name_ar">إسم المتجر (باللغة الإنجليزية)</label>--}}
                                        {{--<input class="form-control btn-square" name="name_en" id="name_en" type="text" placeholder="إسم المتجر باللغة الانجليزية" value="{{isset($data['shop']->translate('en')->name) ? $data['shop']->translate('en')->name : ""}}" required>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="short_description_ar">وصف مختصر (باللغة العربية)</label>
                                        <textarea class="form-control" name="short_description_ar" id="short_description_ar" cols="20" rows="1" required>{{isset($data['shop']->translate('ar')->short_description) ? $data['shop']->translate('ar')->short_description : ""}}</textarea>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-form-label float-right" for="short_description_en">وصف مختصر (باللغة الانجليزية)</label>--}}
                                        {{--<textarea class="form-control" name="short_description_en" id="short_description_en" cols="20" rows="1" required>{{isset($data['shop']->translate('en')->short_description) ? $data['shop']->translate('en')->short_description : ""}}</textarea>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="long_description_ar">الوصف بالكامل (باللغة العربية)</label>
                                        <textarea class="form-control" name="long_description_ar" id="long_description_ar" cols="20" rows="3" required>{{isset($data['shop']->translate('ar')->long_description) ? $data['shop']->translate('ar')->long_description : ""}}</textarea>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-form-label float-right" for="long_description_en">الوصف بالكامل (باللغة الانجليزية)</label>--}}
                                        {{--<textarea class="form-control" name="long_description_en" id="long_description_en" cols="20" rows="3" required>{{isset($data['shop']->translate('en')->long_description) ? $data['shop']->translate('en')->long_description : ""}}</textarea>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <label class="col-form-label float-right" for="payment">طرق الدفع المتاحة للمتجر</label>

                                <div class="col">
                                    <div class="form-group">

                                        <div class="col-md-4 mb-4" >
                                            <input class="checkbox_animated checkhour" id="0" value="0"
                                                   type="checkbox" name="payment[]" {{in_array(0,$data['shop']->payment)  ? 'checked' : ''}}>
                                            <label class="mb-0" for="0">نقدي</label>

                                        </div>
                                        <div class="col-md-4 mb-4" >
                                            <input class="checkbox_animated checkhour" id="1" value="1"
                                                   type="checkbox" name="payment[]"{{in_array(1,$data['shop']->payment)  ? 'checked' : ''}}>
                                            <label class="mb-0" for="1">اونلاين</label>

                                        </div>
                                        <div class="col-md-4 mb-4" >
                                            <input class="checkbox_animated checkhour" id="2" value="2"
                                                   type="checkbox" name="payment[]" {{in_array(2,$data['shop']->payment)  ? 'checked' : ''}}>
                                            <label class="mb-0" for="2">شبكة</label>

                                        </div>

                                    </div>
                                </div>
                            </div>

{{--                            <div class="row">--}}
{{--                                <div class="col">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="col-form-label float-right" for="payment">طرق الدفع المتاحة للمتجر</label>--}}
{{--                                        <select name="payment[]" multiple class="custom-select form-control" id="payment" required>--}}
{{--                                            <option value="" disabled >اختر طرق الدفع المتاحة للمتجر</option>--}}

{{--                                            <option value="0" {{in_array(0,$data['shop']->payment)  ? 'selected' : ''}}>نقدي</option>--}}
{{--                                            <option value="1" {{in_array(1,$data['shop']->payment) ? 'selected' : ''}}>اونلاين</option>--}}
{{--                                            <option value="2" {{in_array(2,$data['shop']->payment) ? 'selected' : ''}}>شبكة</option>--}}

{{--                                        </select>--}}
{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label class="col-form-label float-right" for="min_order">اقل قيمة لعمل الطلب</label>
                                        <div class="input-group ">
                                            <input class="form-control" name="min_order" id="min_order" type="number" placeholder="" value="{{isset($data['shop']->min_order) ? $data['shop']->min_order : ""}}" required><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <label class="col-form-label float-right" for="payment">طرق التوصيل المتاحة للمتجر</label>

                                <div class="col">
                                    <div class="form-group">

                                        <div class="col-md-4 mb-4" >
                                            <input class="checkbox_animated checkhour" id="0" value="0"
                                                   type="checkbox" name="delivery_types[]" {{in_array(0,$data['shop']->delivery_types)  ? 'checked' : ''}}>
                                            <label class="mb-0" for="0">توصيل عادي</label>

                                        </div>
                                        <div class="col-md-4 mb-4" >
                                            <input class="checkbox_animated checkhour" id="1" value="1"
                                                   type="checkbox" name="delivery_types[]" {{in_array(1,$data['shop']->delivery_types)  ? 'checked' : ''}}>
                                            <label class="mb-0" for="1">توصيل سريع</label>

                                        </div>
                                        <div class="col-md-4 mb-4" >
                                            <input class="checkbox_animated checkhour" id="2" value="2"
                                                   type="checkbox" name="delivery_types[]" {{in_array(2,$data['shop']->delivery_types)  ? 'checked' : ''}}>
                                            <label class="mb-0" for="2">استلام من الموقع</label>

                                        </div>

                                    </div>
                                </div>
                            </div>

{{--                            <div class="row">--}}
{{--                                <div class="col">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="col-form-label float-right" for="payment">طرق التوصيل المتاحة للمتجر</label>--}}
{{--                                        <select name="delivery_types[]" multiple class="custom-select form-control" id="payment" required>--}}
{{--                                            <option value="" disabled >اختر طرق التوصيل المتاحة للمتجر</option>--}}

{{--                                            <option value="0" {{in_array(0,$data['shop']->delivery_types)  ? 'selected' : ''}}>توصيل عادي</option>--}}
{{--                                            <option value="1" {{in_array(1,$data['shop']->delivery_types) ? 'selected' : ''}}>توصيل سريع</option>--}}
{{--                                            <option value="2" {{in_array(2,$data['shop']->delivery_types) ? 'selected' : ''}}>استلام من الموقع</option>--}}
{{--                                        </select>--}}
{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label class="col-form-label float-right" for="area"> النطاق (بالكيلو)</label>
                                        <div class="input-group ">
                                            <input class="form-control" name="area" id="area" value="{{isset($data['shop']->area) ? $data['shop']->area : ""}}" type="number" placeholder="" required><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label class="col-form-label float-right" for="from">بداية العمل</label>
                                        <div class="input-group clockpicker ">
                                            <input class="form-control" name="from" id="from" type="text" value="{{isset($data['shop']->from) ? $data['shop']->from : ""}}" required><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label class="col-form-label float-right" for="to">نهاية العمل</label>
                                        <div class="input-group clockpicker">
                                            <input class="form-control" name="to" id="to" type="text" value="{{isset($data['shop']->to) ? $data['shop']->to : ""}}" required><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="delivery_time">وقت التوصيل</label>
                                        <input class="form-control btn-square" name="delivery_time" id="delivery_time" type="text" placeholder="وقت التوصيل" value="{{isset($data['shop']->delivery_time) ? $data['shop']->delivery_time : ""}}" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="delivery_fees">مصاريف التوصيل</label>
                                        <input class="form-control btn-square" name="delivery_fees" id="delivery_fees" type="text" placeholder="مصاريف التوصيل" value="{{isset($data['shop']->delivery_fees) ? $data['shop']->delivery_fees : ""}}" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="phone">رقم الهاتف</label>
                                        <input class="form-control btn-square" name="phone" id="phone" type="text" placeholder="رقم الهاتف" value="{{isset($data['shop']->phone) ? $data['shop']->phone : ""}}" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label class="col-form-label float-right" for="min_order">  التوصيل السريع (في حالة إن وجد) </label>

                                        <div class="media-body icon-state switch-outline">
                                            <label class="switch">
                                                <input type="checkbox" name="fast_delivery" value={{$data['shop']->fast_delivery}}  class="shop_status" data-shop-id="15"  @if($data['shop']->fast_delivery==1) checked @endif id="fast_delivery_check" data-original-title="" title=""><span class="switch-state bg-success"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
{{--                            <div class="row">--}}
{{--                                <div class="col">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="phone">اقل قيمة لعمل الطلب</label>--}}
{{--                                        <input class="form-control btn-square" name="min_order" id="min_order" type="text" placeholder="اقل قيمة لعمل الطلب" value="{{isset($data['shop']->min_order) ? $data['shop']->min_order : ""}}" required>--}}
{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="phone">التقييم</label>
                                        <input class="form-control btn-square" name="rate" id="rate" type="text" placeholder="التقييم" value="{{isset($data['shop']->rate) ? $data['shop']->rate : ""}}" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>


                            <br/>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="logo" id="logo" type="file" data-original-title="" title="">
                                        <label class="custom-file-label" for="logo">لوجو المتجر</label>
                                        {{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}
                                        {{--                                        --}}
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="card">
                                <div class="card-header">
                                    <h5>صور المتجر</h5>
                                </div>
                                <div class="card-body shadow-sm p-3 mb-5 bg-white rounded">
                                    <div class="row" id="images">
                                        @foreach($data['shop']->images as $image)
                                            <div class="image-area col-md-3">
                                                <img src="{{$image->image}}"  alt="{{$image->image}}"  class="img-thumbnail img-fluid">
                                                {{--@if(!$loop->first)--}}
                                                    <a class="remove-image" href="{{route('admin.shops.images.remove', ['shop_id' => $data['shop']->id,'image_id' => $image->id])}}" style="display: inline;">&#215;</a>
                                                {{--@endif--}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <br/>
                                    <br/>
                                    <div class="row">
                                        <div class="col">
                                            <div class="custom-file">
                                                <input class="custom-file-input" name="images[]" id="images" multiple type="file" data-original-title="" title="">
                                                <label class="custom-file-label" for="images">صور المتجر</label>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <br/>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label  for="date">تاريخ انتهاء اشتراك المتجر</label>
                                                <input class="form-control btn-square" name="expire_at" id="date" type="date"  placeholder='تاريخ انتهاء اشتراك المتجر' value="{{isset($data['shop']->expire_at) ? $data['shop']->expire_at : ""}}" required>
                                                <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                                <div class="valid-feedback">بيانات صحيحة</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="card">
                                <div class="card-header">
                                    <h5>بيانات لوحة التحكم</h5>
                                </div>
                                <div class="card-body shadow-sm p-3 mb-5 bg-white rounded">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="email">البريد الإلكتروني</label>
                                                <input class="form-control btn-square" name="email" id="email" type="email" placeholder="البريد الإلكتروني" value="{{isset($data['shop']->email) ? $data['shop']->email : ""}}" required>
                                                <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="password">
                                                    كلمة المرور
                                                    <span class="valid-feedback" style="display: inline;">(أتركها فارغة في حالة لا تريد تغييرها)</span>
                                                </label>
                                                <input class="form-control btn-square" name="password" id="password" type="password" placeholder="كلمة المرور">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <script src="{{ url('/assets/js/time-picker/jquery-clockpicker.min.js') }}"></script>
    <script src="{{ url('/assets/js/time-picker/highlight.min.js') }}"></script>
    <script src="{{ url('/assets/js/time-picker/clockpicker.js') }}"></script>
<script>
$('#fast_delivery_check').on('change',function (){
    if($('#fast_delivery_check').val()=='1') {
        $('#fast_delivery_check').val(0);
    }
    else{
        $('#fast_delivery_check').val(1);

    }


});

</script>
    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('userlocation'), {
                zoom: 7,
                center: {lat: {{$data['shop']->latitude??26.719517}}, lng: {{$data['shop']->longitude??29.2161655}} }
            });
            var MaekerPos = new google.maps.LatLng(0 , 0);
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("userlat").value=e.latLng.lat();
                document.getElementById("userlng").value=e.latLng.lng();
            });
            const geocoder = new google.maps.Geocoder();
            const infowindow = new google.maps.InfoWindow();
            geocodeLatLng(geocoder, map, infowindow);

            document.getElementById("submitLatLng").addEventListener("click", () => {
                geocodeLatLng(geocoder, map, infowindow);
            });

            function geocodeLatLng(geocoder, map, infowindow) {
                const input = document.getElementById("latlng").value;
                const latlngStr = input.split(",", 2);
                const latlng = {
                    lat: parseFloat(latlngStr[0]),
                    lng: parseFloat(latlngStr[1]),
                };
                document.getElementById("userlat").value=latlngStr[0];
                document.getElementById("userlng").value=latlngStr[1];

                geocoder.geocode({location: latlng}, (results, status) => {

                    if (status === "OK") {
                        if (results[0]) {
                            map.setZoom(11);
                            const marker = new google.maps.Marker({
                                position: latlng,
                                map: map,
                            });
                            infowindow.setContent(results[0].formatted_address);
                            // console.log(results[0].formatted_address);

                            infowindow.open(map, marker);
                            document.getElementById("autocomplete").value=results[0].formatted_address;

                        } else {
                            window.alert("No results found");
                        }
                    } else {
                        window.alert("Geocoder failed due to: " + status);
                    }
                });
            }

        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhnmMC23noePz6DA8iEvO9_yNDGGlEaeM&libraries=places&callback=initMap">
    </script>

@endsection
@section('extra-js')
    <script>
        // $('#from').datetimepicker({
        //     format : 'g:i A'
        // });
        // $('#to').datetimepicker({
        //     format : 'g:i A'
        // });

    </script>
@endsection
