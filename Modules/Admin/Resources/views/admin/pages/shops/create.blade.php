@extends('admin::admin.layouts.master_admin')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/timepicker.css') }}">
@endsection

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$data['title']}}</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.shops.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <input type="hidden" name="type" value="0">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="city_id">المدن</label>
                                        <select name="city_id" class="custom-select form-control" id="city_id" required>
                                            <option value="" disabled selected>اختر المدينة</option>
                                            @foreach($data['cities'] as $city)
                                                <option value="{{$city->id}}" {{ (old("city_id") == $city->id ? "selected":"") }}>{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="category_id">الأقسام</label>
                                        <select name="category_id" class="custom-select form-control" id="category_id" required>
                                            <option value="" disabled selected>اختر القسم</option>
                                            @foreach($data['categories'] as $category)
                                                <option value="{{$category->id}}" {{ (old("category_id") == $category->id ? "selected":"") }}>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>


{{--                            //////////////////////////////////////////////--}}


                            <div class="form-group">
                                <input  type="hidden" class="form-control"  name="longitude" value="" id="userlat" placeholder="Enter lat ">
                            </div>
                            <div class="form-group">
                                <input  type="hidden" class="form-control"  name="latitude" value="" id="userlng" placeholder="Enter long">
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <div id="userlocation" style="width:100%;height:350px"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="autocomplete">البحث عن عنوان</label>
                                        <input type="text" class="form-control" id="autocomplete" placeholder="البحث عن عنوان" name="soft_address" value="{{old('soft_address')}}"  >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="latlng">البحث عن خطوط الطول و العرض</label>
                                        <input type="text" class="form-control" id="latlng" name="latlng" value="{{old('latlng')}}" placeholder="البحث عن الطول و العرض" >
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="submitLatLng">الظهور علي الخريطة</label>

                                        <input type="button" class="form-control" id="submitLatLng" value="بحث" >
                                    </div>
                                </div>
                            </div>

{{--                            //////////////////////////////////////////////--}}


                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">إسم المتجر باللغة العربية</label>
                                        <input class="form-control btn-square" name="name_ar" value="{{old('name_ar')}}" id="name_ar" type="text" placeholder="إسم المتجر باللغة العربية" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="name_en">إسم المتجر باللغة الإنجليزية</label>--}}
                                        {{--<input class="form-control btn-square" name="name_en" id="name_en" type="text" placeholder="إسم المتجر باللغة الإنجليزية" required>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="short_description_ar">وصف مختصر (باللغة العربية)</label>
                                        <textarea class="form-control" name="short_description_ar" value="{{old('short_description_ar')}}" id="short_description_ar" cols="20" rows="1" required></textarea>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-form-label float-right" for="short_description_en">وصف مختصر (باللغة الانجليزية)</label>--}}
                                        {{--<textarea class="form-control" name="short_description_en" id="short_description_en" cols="20" rows="1" required></textarea>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="long_description_ar">الوصف بالكامل (باللغة العربية)</label>
                                        <textarea class="form-control" name="long_description_ar" value="{{old('long_description_ar')}}" id="long_description_ar" cols="20" rows="3" required></textarea>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-form-label float-right" for="long_description_en">الوصف بالكامل (باللغة الانجليزية)</label>--}}
                                        {{--<textarea class="form-control" name="long_description_en" id="long_description_en" cols="20" rows="3" required></textarea>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <label class="col-form-label float-right" for="payment">طرق الدفع المتاحة للمتجر</label>

                                <div class="col">
                                    <div class="form-group">

                                    <div class="col-md-4 mb-4" >
                                            <input class="checkbox_animated checkhour" id="0" value="0"
                                                   type="checkbox" name="payment[]">
                                            <label class="mb-0" for="0">نقدي</label>

                                        </div>
                                        <div class="col-md-4 mb-4" >
                                            <input class="checkbox_animated checkhour" id="1" value="1"
                                                   type="checkbox" name="payment[]">
                                            <label class="mb-0" for="1">اونلاين</label>

                                        </div>
                                        <div class="col-md-4 mb-4" >
                                            <input class="checkbox_animated checkhour" id="2" value="2"
                                                   type="checkbox" name="payment[]">
                                            <label class="mb-0" for="2">شبكة</label>

                                        </div>

                                </div>
                                </div>
                            </div>
{{--                            <div class="row">--}}
{{--                                <div class="col">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="col-form-label float-right" for="payment">طرق الدفع المتاحة للمتجر</label>--}}
{{--                                        <select name="payment[]" multiple="" class="custom-select form-control" id="payment" required>--}}
{{--                                            <option value=""  disabled selected>اختر طرق الدفع المتاحة للمتجر</option>--}}

{{--                                                <option value="0" >نقدي</option>--}}
{{--                                                <option value="1" >اونلاين</option>--}}
{{--                                                <option value="2" >شبكة</option>--}}

{{--                                        </select>--}}

{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="col-form-label float-right" for="payment">طرق التوصيل المتاحة للمتجر</label>--}}
{{--                                        <select name="delivery_types[]" multiple class="custom-select form-control" id="payment" required>--}}
{{--                                            <option value="" disabled >اختر طرق التوصيل للمتجر</option>--}}

{{--                                            <option value="0" >توصيل عادي</option>--}}
{{--                                            <option value="1" >توصيل سريع</option>--}}
{{--                                            <option value="2" >استلام من الموقع</option>--}}
{{--                                        </select>--}}
{{--                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label class="col-form-label float-right" for="min_order">سعر التوصيل السريع (في حالة إن وجد)</label>
                                        <div class="input-group ">
                                            <input class="form-control" name="fast_delivery" value="{{old('fast_delivery')}}" id="fast_delivery" type="number" placeholder="" ><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <label class="col-form-label float-right" for="payment">طرق التوصيل المتاحة للمتجر</label>

                                <div class="col">
                                    <div class="form-group">

                                        <div class="col-md-4 mb-4" >
                                            <input class="checkbox_animated checkhour" id="0" value="0"
                                                   type="checkbox" name="delivery_types[]">
                                            <label class="mb-0" for="0">توصيل عادي</label>

                                        </div>
                                        <div class="col-md-4 mb-4" >
                                            <input class="checkbox_animated checkhour" id="1" value="1"
                                                   type="checkbox" name="delivery_types[]">
                                            <label class="mb-0" for="1">توصيل سريع</label>

                                        </div>
                                        <div class="col-md-4 mb-4" >
                                            <input class="checkbox_animated checkhour" id="2" value="2"
                                                   type="checkbox" name="delivery_types[]">
                                            <label class="mb-0" for="2">استلام من الموقع</label>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label class="col-form-label float-right" for="min_order">اقل قيمة لعمل الطلب</label>
                                        <div class="input-group ">
                                            <input class="form-control" name="min_order" id="min_order" value="{{old('min_order')}}" type="number" placeholder="" required><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label class="col-form-label float-right" for="area"> النطاق (بالكيلو)</label>
                                        <div class="input-group ">
                                            <input class="form-control" name="area" id="area" value="{{old('area')}}" type="number" placeholder="" required><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label class="col-form-label float-right" for="from">بداية العمل</label>
                                        <div class="input-group clockpicker ">
                                            <input class="form-control" name="from" id="from"  value="{{old('from')}}" type="text" placeholder="08:00" required><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label class="col-form-label float-right" for="to">نهاية العمل</label>
                                        <div class="input-group clockpicker">
                                            <input class="form-control" name="to" value="{{old('to')}}" id="to" type="text" placeholder="22:00" required><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="delivery_time">وقت التوصيل</label>
                                        <input class="form-control btn-square" name="delivery_time" value="{{old('delivery_time')}}" id="delivery_time" type="text" placeholder="وقت التوصيل" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="delivery_fees">مصاريف التوصيل</label>
                                        <input class="form-control btn-square" name="delivery_fees" value="{{old('delivery_fees')}}" id="delivery_fees" type="text" placeholder="مصاريف التوصيل" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="phone">رقم الهاتف</label>
                                        <input class="form-control btn-square" name="phone" id="phone" value="{{old('phone')}}" type="text" placeholder="رقم الهاتف" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="logo" id="logo" type="file" value="{{old('logo')}}" required="" data-original-title="" title="">
                                        <label class="custom-file-label" for="logo">لوجو المتجر</label>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="images[]" value="{{old('images[]')}}" id="images" type="file" required="" multiple data-original-title="" title="">
                                        <label class="custom-file-label" for="images">صور المتجر</label>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label  for="date">تاريخ انتهاء اشتراك المتجر</label>
                                        <input class="form-control btn-square" name="expire_at" id="date" value="{{old('expire_at')}}" type="date" required="" placeholder='تاريخ انتهاء اشتراك المتجر'>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h5>بيانات لوحة التحكم</h5>
                            </div>
                            <div class="card-body shadow-sm p-3 mb-5 bg-white rounded">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="email">البريد الإلكتروني</label>
                                            <input class="form-control btn-square" name="email" id="email" value="{{old('email')}}" type="email" placeholder="البريد الإلكتروني" required>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="password">كلمة المرور</label>
                                            <input class="form-control btn-square" name="password" id="password" type="password" placeholder="كلمة المرور" required>
                                            <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <script src="{{ url('/assets/js/time-picker/jquery-clockpicker.min.js') }}"></script>
    <script src="{{ url('/assets/js/time-picker/highlight.min.js') }}"></script>
    <script src="{{ url('/assets/js/time-picker/clockpicker.js') }}"></script>


{{--    ////////////////////////////////////////////////////////--}}
    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('userlocation'), {
                zoom: 7,
                center: {lat: 26.719517, lng: 29.2161655 }
            });
            var MaekerPos = new google.maps.LatLng(0 , 0);
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("userlat").value=e.latLng.lat();
                document.getElementById("userlng").value=e.latLng.lng();
            });
            const geocoder = new google.maps.Geocoder();
            const infowindow = new google.maps.InfoWindow();

            document.getElementById("submitLatLng").addEventListener("click", () => {
                geocodeLatLng(geocoder, map, infowindow);
            });

            function geocodeLatLng(geocoder, map, infowindow) {
                const input = document.getElementById("latlng").value;
                const latlngStr = input.split(",", 2);
                const latlng = {
                    lat: parseFloat(latlngStr[0]),
                    lng: parseFloat(latlngStr[1]),
                };
                document.getElementById("userlat").value=latlngStr[0];
                document.getElementById("userlng").value=latlngStr[1];

                geocoder.geocode({location: latlng}, (results, status) => {
                    if (status === "OK") {
                        if (results[0]) {
                            map.setZoom(11);
                            const marker = new google.maps.Marker({
                                position: latlng,
                                map: map,
                            });
                            infowindow.setContent(results[0].formatted_address);

                            infowindow.open(map, marker);
                            document.getElementById("autocomplete").value=results[0].formatted_address;

                        } else {
                            window.alert("No results found");
                        }
                    } else {
                        window.alert("Geocoder failed due to: " + status);
                    }
                });
            }

        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhnmMC23noePz6DA8iEvO9_yNDGGlEaeM&libraries=places&callback=initMap">
    </script>
{{--    ////////////////////////////////////////////////////////--}}
@endsection

