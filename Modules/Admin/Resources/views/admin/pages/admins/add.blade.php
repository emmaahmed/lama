@extends('admin::admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>إضافة مشرف</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.admins.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name">إسم المشرف</label>
                                        <input class="form-control btn-square" name="name" id="name" type="text" placeholder="اسم المشرف" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="email">البريد الإلكتروني</label>
                                        <input class="form-control btn-square" name="email" id="email" type="email" placeholder="البريد الإلكتروني" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="password">كلمة المرور</label>
                                        <input class="form-control btn-square" name="password" id="password" type="password" placeholder="كلمة المرور" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="form-row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="image" id="image" type="file" required="" data-original-title="" title="">
                                        <label class="custom-file-label" for="image">صورة المشرف</label>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h3><b>الصلاحيات</b></h3>
                            <hr>
                            @foreach($data['all_permissions'] as $key => $permission)
                                <h5 style="color: #4466f2;">{{$key}}</h5>
                                <div class="row">
                                    @foreach($permission as $single_permission)
                                        <div class="col-md-2">
                                            <label for="check{{$single_permission->id}}">{{$single_permission->display_name}}</label>
{{--                                            <input type="checkbox" id="check{{$single_permission->id}}" name="permissions[]" value="{{ $single_permission->name }}">--}}
                                            <label class="d-block" for="check{{$single_permission->id}}">
                                                <input type="checkbox" class="checkbox_animated" name="permissions[]" id="check{{$single_permission->id}}" value="{{ $single_permission->name }}">
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                                <hr/>
                            @endforeach
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
                {{--
                <div class="card">
                    <div class="card-header">
                        <h5>Custom controls</h5>
                    </div>
                    <form class="form theme-form">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Upload File</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row mb-0">
                                        <label class="col-sm-3 col-form-label">Custom select</label>
                                        <div class="col-sm-9">
                                            <select class="custom-select form-control">
                                                <option selected="">Open this select menu</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">Submit</button>
                            <input class="btn btn-light" type="reset" value="Cancel">
                        </div>
                    </form>
                </div>
                --}}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('modals')

{{--  Send Notification  --}}
    <div class="modal fade rtl" id="sendNotification" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">إرسال إشعار</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <form id="send-sms">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-form-label float-right" for="title">العنوان</label>
                            <input class="form-control" name="title" id="title" type="text" value="">
                        </div>
                        <div class="form-group">
                            {{--                            <textarea class="form-control" name="message" id="message" cols="20" rows="10"></textarea>--}}
                            <label class="col-form-label float-right" for="message">الاشعار</label>
                            <input class="form-control" name="message" id="message" type="text" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary float-right" type="submit">حفظ</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">إغلاق</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <!-- Plugins JS start-->
    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>
{{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>
        $('#editAdmin').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var email = button.data('email');
            var mobile = button.data('mobile');
            var image = button.data('image');
            var modal = $(this);
            modal.find('.modal-body #admin_id').val(id);
            modal.find('.modal-body #name').val(name);
            modal.find('.modal-body #email').val(email);
            modal.find('.modal-body #mobile').val(mobile);
            modal.find('.modal-body #profile-image').attr("src",image);
        });

        $('#editAdminForm').on('submit', function(e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: "{{URL::route('admin.admins.update')}}",
                method: "POST",
                processData: false,
                contentType: false,
                data: formData,
                dataType: 'json',
                success: function(response){
                    $('#editAdmin').modal('hide');
                    // window.location.reload();
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseText.message);
                }

            });
        });
        $(document).ready(function(){
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        myids.push(value);
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.admins.deleteMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

            // End Bulk Delete

            // Bulk Send
            var $bulkSendBtn = $('#bulk_send_btn');
            var $bulksendinput = $('#bulk_send_input');
            // Reposition modal to prevent z-index issues
            // Bulk Send listener
            $('#send-sms').on('submit', function(e) {
                e.preventDefault();
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulksendinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        myids.push(value);
                    });
                    // Set input value
                    $bulksendinput.val(myids);
                    // Show modal
                    var title = $('#title').val();
                    var message = $('#message').val();

                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.admins.sendMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,title:title,message:message,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Send');
                }
            });

            // End Bulk Send
        });
    </script>
@endsection
