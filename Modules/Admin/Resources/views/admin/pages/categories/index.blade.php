@extends('admin::admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
{{--                    <div class="card-header">--}}
{{--                        <h5>{{$data['title']}}</h5>--}}
{{--                    </div>--}}
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    @if(Auth::guard('admin')->user()->can('add_category'))
                                        <a class="btn btn-square btn-success" href="{{route('admin.categories.create')}}" title="إضافة"> إضافة قسم جديد</a> &nbsp; &nbsp;
                                    @endif
                                    @if(Auth::guard('admin')->user()->can('delete_category'))
                                        <button class="btn btn-square btn-danger" style="float: left; margin-right: 2%;" id="bulk_delete_btn" type="button" title="حذف متعدد">حذف متعدد</button>
                                    @endif
                                    <input type="hidden" name="myids" id="bulk_delete_input" value="">
                                    <br>
                                    <br>
                                    <th>
                                        <label class="d-block" for="chk-ani">
                                            <input class="checkbox_animated select_all" id="chk-ani" type="checkbox">
                                        </label>
                                    </th>
                                    <th>الصورة</th>
                                    <th>الإسم</th>
                                    <th>عدد المتاجر</th>
                                    <th>الحالة</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['categories'] as $category)
                                    <tr>
                                        <td>
                                            <label class="d-block" for="chk-ani">
                                                <input type="checkbox" class="checkbox_animated" name="row_id" id="checkbox_{{ $category->id }}" value="{{ $category->id }}">
                                            </label>
                                        </td>
                                        <td><img class="img-60 rounded-circle" src="{{!empty($category->image) ? $category->image : url('assets/images/default.png')}}" alt="#" data-original-title="" title=""></td>
                                        <td>{{isset($category->name) ? str_limit($category->name , 50) : ""}}</td>
                                        <td>{{isset($category->shops) ? (count($category->shops) > 0 ? count($category->shops) : '-') : 0}}</td>
                                        <td>
                                            <div class="form-group">
                                                <div class="media-body icon-state switch-outline">
                                                    <label class="switch">
                                                        <input type="checkbox" id="active" class="category_status" data-category-id="{{$category->id}}" name="active" @if($category->active == 1) checked @endif><span class="switch-state bg-success"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="row">
                                                @if(Auth::guard('admin')->user()->can('show_category'))
                                                    <a title="تعديل" href="{{route('admin.categories.edit',['category_id' => $category->id])}}"><i width="20" height="20" color="green" data-feather="edit"></i></a> &nbsp;
                                                @endif
                                                @if(Auth::guard('admin')->user()->can('delete_category'))
                                                    <a onclick='return deleteCategory({{$category->id}})' title="حذف" data-id="{{$category->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection


@section('scripts')
    <!-- Plugins JS start-->
{{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>
        function deleteCategory(category_id)
        {
            var id = category_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.categories.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {category_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
        //
        }
        $(document).ready(function(){
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        myids.push(value);
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.categories.deleteMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

            // End Bulk Delete
        });

        //////////////////////////////
        //////////////////////////////
        $('.category_status').on('change.bootstrapSwitch', function(e) {
            var category_id = $(this).attr('data-category-id');
            var active = "";
            if (e.target.checked == true){
                var active = 1;
            }else{
                var active = 0;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('admin.categories.status')}}",
                data: {
                    category_id: category_id,
                    active: active,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response){
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });
    </script>
@endsection
