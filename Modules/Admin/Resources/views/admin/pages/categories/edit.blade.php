@extends('admin::admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>تعديل القسم</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.categories.update',['category_id' => $data['category']])}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="text-center">
                                <img id="image-display" width="200px" height="70px" class="profile-user-img img-fluid img-circle" style="border: 1px solid #e5eaff;"
                                     alt="Admin profile picture" src="{{$data['category']->image ? $data['category']->image : url('assets/images/default.png')}}" >
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">إسم القسم باللغة العربية</label>
                                        <input class="form-control btn-square" name="name_ar" id="name_ar" type="text" value="{{isset($data['category']->translate('ar')->name) ? $data['category']->translate('ar')->name : ""}}" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="name_en">إسم القسم باللغة الإنجليزية</label>--}}
                                        {{--<input class="form-control btn-square" name="name_en" id="name_en" type="text" value="{{isset($data['category']->translate('en')->name) ? $data['category']->translate('en')->name : ""}}" placeholder="القسم باللغة الانجليزية" required>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="image" id="image" type="file" data-original-title="" title="">
                                        <label class="custom-file-label" for="image">صورة القسم</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <!-- Plugins JS start-->
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image-display').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function() {
            readURL(this);
        });
    </script>
    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('workerlocation'), {
                zoom: 7,
                center: {lat: 26.719517, lng: 29.2161655 }
            });
            var MaekerPos = new google.maps.LatLng(0 , 0);
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("workerlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("workerlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("workerlat").value=e.latLng.lat();
                document.getElementById("workerlng").value=e.latLng.lng();
            });
        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
    </script>
@endsection
