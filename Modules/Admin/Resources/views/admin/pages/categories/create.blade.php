@extends('admin::admin.layouts.master_admin')

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$data['title']}}</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.categories.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">إسم القسم باللغة العربية</label>
                                        <input class="form-control btn-square" name="name_ar" id="name_ar" value="{{old('name_ar')}}" type="text" placeholder="القسم باللغة العربية" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                            {{--<div class="form-row">--}}
                                {{--<div class="col">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label for="name_en">إسم القسم باللغة الإنجليزية</label>--}}
                                        {{--<input class="form-control btn-square" name="name_en" id="name_en" type="text" placeholder="القسم باللغة الإنجليزية" required>--}}
                                        {{--<div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <br/>
                            <div class="form-row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="image" value="{{old('image')}}" id="image" type="file" required="" data-original-title="" title="">
                                        <label class="custom-file-label" for="image">صورة القسم</label>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
@endsection

