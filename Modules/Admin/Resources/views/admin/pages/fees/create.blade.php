@extends('admin::admin.layouts.master_admin')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/timepicker.css') }}">
@endsection

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$data['title']}}</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.fees.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <input type="hidden" name="type" value="0">


                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">سعر التوصيل العادي</label>
                                        <input class="form-control btn-square" name="normal_delivery" value="{{old('normal_delivery')}}" id="normal_delivery" type="number" placeholder="سعر التوصيل العادي" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">سعر التوصيل السريع</label>
                                        <input class="form-control btn-square" name="fast_delivery" value="{{old('fast_delivery')}}" id="fast_delivery" type="number" placeholder="سعر التوصيل السريع" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">المسافة بالكيلومتر (من)</label>
                                        <input class="form-control btn-square" name="distance_from" value="{{old('distance_from')}}" id="distance_from" type="number" placeholder="المسافة بالكيلومتر (من)" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">المسافة بالكيلومتر (الي)</label>
                                        <input class="form-control btn-square" name="distance_to" value="{{old('distance_to')}}" id="distance_to" type="number" placeholder="المسافة بالكيلومتر (الي)" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <script src="{{ url('/assets/js/time-picker/jquery-clockpicker.min.js') }}"></script>
    <script src="{{ url('/assets/js/time-picker/highlight.min.js') }}"></script>
    <script src="{{ url('/assets/js/time-picker/clockpicker.js') }}"></script>


{{--    ////////////////////////////////////////////////////////--}}
    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('userlocation'), {
                zoom: 7,
                center: {lat: 26.719517, lng: 29.2161655 }
            });
            var MaekerPos = new google.maps.LatLng(0 , 0);
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("userlat").value=e.latLng.lat();
                document.getElementById("userlng").value=e.latLng.lng();
            });
        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
    </script>
{{--    ////////////////////////////////////////////////////////--}}
@endsection

