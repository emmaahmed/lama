@extends('admin::admin.layouts.master_admin')

@section('styles')
    <style>
        /*body {*/
        /*padding: 20px;*/
        /*}*/
        .image-area {
            position: relative;
            width: 15%;
            background: #fff;
            margin-bottom: 1%;
        }
        .image-area img{
            width: 100%;
            height: 75%;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            /*background: #E54E4E;*/
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }
    </style>

    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/timepicker.css') }}">
@endsection

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$data['title']}}</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.fees.update',['fee_id' => $data['fee']])}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">سعر التوصيل العادي</label>
                                        <input class="form-control btn-square" name="normal_delivery" value="{{$data['fee']->normal_delivery}}" id="normal_delivery" type="number" placeholder="سعر التوصيل العادي" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">سعر التوصيل السريع</label>
                                        <input class="form-control btn-square" name="fast_delivery" value="{{$data['fee']->fast_delivery}}" id="fast_delivery" type="number" placeholder="سعر التوصيل السريع" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">المسافة بالكيلومتر (من)</label>
                                        <input class="form-control btn-square" name="distance_from" value="{{$data['fee']->distance_from}}" id="distance_from" type="number" placeholder="المسافة بالكيلومتر (من)" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name_ar">المسافة بالكيلومتر (الي)</label>
                                        <input class="form-control btn-square" name="distance_to" value="{{$data['fee']->distance_to}}" id="distance_to" type="number" placeholder="المسافة بالكيلومتر (الي)" required>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>

                                    </div>
                                </div>
                            </div>
                        </div>

                <div class="card-footer">
                    <button class="btn btn-primary" type="submit">حفط</button>
                    <input class="btn btn-light" type="reset" value="إلغاء">
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Container-fluid Ends-->
@endsection
@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <script src="{{ url('/assets/js/time-picker/jquery-clockpicker.min.js') }}"></script>
    <script src="{{ url('/assets/js/time-picker/highlight.min.js') }}"></script>
    <script src="{{ url('/assets/js/time-picker/clockpicker.js') }}"></script>
@endsection

