@extends('admin::admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
{{--                    <div class="card-header">--}}
{{--                        <h5>{{$data['title']}}</h5>--}}
{{--                    </div>--}}
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    @if(Auth::guard('admin')->user()->can('show_statistics'))
                                    <a class="btn btn-square btn-success" href="{{route('admin.fees.create')}}" title="إضافة"> إضافة نطاق جديد</a> &nbsp; &nbsp;
                                    @endif

                                    <th>
                                        #
                                    </th>
                                    <th>التوصيل العادي</th>
                                    <th>التوصيل السريع</th>
                                    <th>المسافة بالكيلومتر (من)</th>
                                    <th>المسافة بالكيلومتر (الي)</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['fees'] as $fee)
                                    <tr style="background-color:{{$fee->expire_at != null && $fee->expire_at < date('Y-m-d') ? '#FEACAC' : '' }}">
                                        <td>
                                            {{isset($fee->id) ? $fee->id : ""}}
                                        </td>
                                        <td>
                                            {{isset($fee->normal_delivery) ? $fee->normal_delivery : ""}}
                                            <b class="badge badge-primary">$</b>
                                        </td>
                                        <td>
                                            {{isset($fee->fast_delivery) ? $fee->fast_delivery : ""}}
                                            <b class="badge badge-primary">$</b>
                                        </td>
                                        <td>
                                            {{isset($fee->distance_from) ? $fee->distance_from : ""}}
                                            <b class="badge badge-success">كم</b>
                                        </td>
                                        <td>
                                            {{isset($fee->distance_to) ? $fee->distance_to : ""}}
                                            <b class="badge badge-success">كم</b>
                                        </td>

                                        <td>
                                            <div class="row">
                                                @if(Auth::guard('admin')->user()->can('show_statistics'))
                                                    <a title="تعديل" href="{{route('admin.fees.edit',['fee_id' => $fee->id])}}"><i width="20" height="20" color="green" data-feather="edit"></i></a> &nbsp;
                                                @endif

                                                @if(Auth::guard('admin')->user()->can('show_statistics'))
                                                    <a onclick='return deleteFee({{$fee->id}})' title="حذف" data-id="{{$fee->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a>
                                                @endif

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection


@section('scripts')
    <!-- Plugins JS start-->
{{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>

        function deleteFee(fee_id)
        {
            var id = fee_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.fees.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {fee_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
        //
        }




    </script>
@endsection
