@extends('admin::admin.layouts.master_admin')
@section('content')
    <div class="page-body" dir="rtl" style="padding-top: 0;margin-top: 0">
{{--        <div class="container-fluid">--}}
{{--            <div class="page-header">--}}
{{--                <div class="row">--}}
{{--                    <div class="col">--}}
{{--                        <div class="page-header-right">--}}


{{--                            @include('cp.layouts.messages')--}}


{{--                            <h3>--}}
{{--                                <i data-feather="home"></i>--}}
{{--                                عن التطبيق--}}
{{--                            </h3>--}}
{{--                            --}}{{--<ol class="breadcrumb">--}}
{{--                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>--}}
{{--                                <li class="breadcrumb-item active">عن التطبيق</li>--}}
{{--                            </ol>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                {{--<th scope="col">#</th>--}}
                                <th scope="col"><span style="float: right">النص بالعربية</span></th>
                                <th scope="col"><span style="float: right"> روابط مواقع التواصل الاجتماعي</span></th>
                                {{--<th scope="col">النص بالانجليزية</th>--}}
                                <th scope="col">الاختيارات</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($abouts as $c)
                                    <tr id="main_cat_{{$c->id}}" onclick="myFunction({{$c->id}})">
                                        {{--<td>{{$c->id}}</td>--}}
                                        <td>
                                            <textarea class="form-control" name="body" rows="20" placeholder="النص بالعربية">{{$c->body}}</textarea>
                                        </td>
                                        <td>
                                            <lable style="float: right">تويتر</lable>
                                            <textarea class="form-control" name="twitter"  placeholder=" تويتر">{{$c->twitter}}</textarea>
                                            <br>
                                            <label style="float: right"> فيس بوك</label>
                                            <textarea class="form-control" name="facebook"  placeholder=" فيس بوك">{{$c->facebook}}</textarea>
                                            <br>
                                            <lable style="float: right">انستجرام</lable>
                                            <textarea class="form-control" name="instagram"  placeholder=" انستجرام">{{$c->instagram}}</textarea>
                                            <br>
                                            <lable style="float: right">الهاتف</lable>
                                            <textarea class="form-control" name="phone"  placeholder=" الهاتف">{{$c->phone}}</textarea>
                                            <br>
                                            <lable style="float: right">البريد الالكتروني</lable>
                                            <textarea class="form-control" name="email"  placeholder=" البريد الالكتروني">{{$c->email}}</textarea>
                                        </td>
                                        {{--<td>
                                            <textarea class="form-control" name="body_en" rows="15" placeholder="النص بالنجليزية" dir="ltr">{{$c->body_en}}</textarea>
                                        </td>--}}
                                        <td>
                                            {{--@if(admin()->hasPermissionTo('Edit City'))--}}
                                            <button title="تعديل" type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit_{{$c->id}}">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            {{--@endif--}}
                                        </td>

                                        <div class="modal fade" id="edit_{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">تعديل عن التطبيق</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form class="form-horizontal" method="post" action="{{route('editAbout')}}" enctype="multipart/form-data">
                                                        {{csrf_field()}}
                                                        <div class="modal-body">
                                                            <input type="hidden" name="term_id" value="{{$c->id}}">


                                                            <div class="form-group row {{ $errors->has('ar_name') ? ' has-error' : '' }}">
                                                                <label class="col-lg-12 control-label text-lg-right" for="textinput"> النص بالعربية</label>
                                                                <div class="col-lg-12">
                                                                    <textarea class="form-control" name="body" rows="10" placeholder="النص بالعربية" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب ادخاله')">{{$c->body}}</textarea>
                                                                </div>
                                                            </div>
{{--                                                            @include('cp.layouts.error', ['input' => 'ar_name'])--}}

                                                            <div class="form-group row {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                                                <label class="col-lg-12 control-label text-lg-right" for="textinput">النص بالنجليزية</label>
                                                                <div class="col-lg-12">
                                                                    <lable style="float: right">تويتر</lable>
                                                                    <textarea class="form-control" name="twitter"  placeholder=" تويتر" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب ادخاله')">{{$c->twitter}}</textarea>
                                                                    <br>
                                                                    <label style="float: right"> فيس بوك</label>
                                                                    <textarea class="form-control" name="facebook"  placeholder=" فيس بوك" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب ادخاله')">{{$c->facebook}}</textarea>
                                                                    <br>
                                                                    <lable style="float: right">انستجرام</lable>
                                                                    <textarea class="form-control" name="instagram"  placeholder=" انستجرام" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب ادخاله')">{{$c->instagram}}</textarea>
                                                                    <br>
                                                                    <lable style="float: right">الهاتف</lable>
                                                                    <textarea class="form-control" name="phone" required placeholder=" الهاتف">{{$c->phone}}</textarea>
                                                                    <br>
                                                                    <lable style="float: right">البريد الالكتروني</lable>
                                                                    <textarea class="form-control" name="email" required placeholder=" البريد الالكتروني">{{$c->email}}</textarea>
                                                                </div>
                                                            </div>
{{--                                                            @include('cp.layouts.error', ['input' => 'en_name'])--}}






                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="reset" class="btn btn-dark" data-dismiss="modal">اغلاق</button>
                                                            <button class="btn btn-primary" type="submit">تعديل</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->

    </div>


@endsection
