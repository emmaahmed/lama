@extends('admin::admin.layouts.master_admin')
@section('content')
    <!-- Right sidebar Ends-->
    <div class="page-body" style="padding-top: 0;margin-top: 0">


        {{--        <div class="container-fluid">--}}
        {{--            <div class="page-header">--}}
        {{--                <div class="row">--}}
        {{--                    <div class="col">--}}
        {{--                        <div class="page-header-left">--}}
        {{--                        </div>--}}
        {{--                    </div>--}}

        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}


        <div class="row">

            <div class="col-sm-12">
                <div class="row">
                    <table>
                        <tr>

                            <td style="padding: 10px;margin: 10px">{{$data['client']->id}}</td>

                            <td style="padding: 10px;margin: 10px"><img src=" {{ $data['client']->image }}" style="width: 50px" alt=""></td>

                            <td style="padding: 10px;margin: 10px">{{$data['client']->name}}</td>

                            <td style="padding: 10px;margin: 10px">{{$data['client']->email}}</td>

                            <td style="padding: 10px;margin: 10px">{{$data['client']->phone}}</td>

                        </tr>
                    </table>
                </div>
                <div class="card">

                    @if( count($users) > 0)

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display dataTable" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>المسلسل</th>
                                        <th>صوره العميل</th>
                                        <th>إسم العميل</th>
                                        <th>البريد الاكتروني</th>
                                        <th>الموبايل </th>
                                        {{--                                        <th>حالة الرسالة</th>--}}
                                        <th>العمليات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{$user->id}}</td>
                                            <td><img src=" {{ $user->logo }}" style="width: 50px" alt=""></td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->phone}}</td>

                                            <td>
                                                <a href="{{route('admin.shop_chat_details',['delegate_id'=>$user->id,'user_id'=>$data['client']->id])}}"
                                                   title="مشاهده الرسائل" class="btn btn-success btn-sm"><i
                                                        class="fa fa-send"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <h4 class="text-center"> لا توجد رسائل </h4>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection
