@extends('admin::admin.layouts.master_admin')

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$data['title']}}</h5>
                    </div>
                    <form class="needs-validation"  novalidate="" method="POST" action="{{route('admin.sliders.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label float-right" for="shop_id">المتاجر</label>
                                        <select name="shop_id" class="custom-select form-control" id="shop_id" required>
                                            <option value="" disabled selected>اختر المتجر</option>
                                            @foreach($data['shops'] as $shop)
                                                <option value="{{$shop->id}}" {{ (old("shop_id") == $shop->id ? "selected":"") }}>{{$shop->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="form-row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" name="image"  value="{{old('image')}}" id="image" type="file" required="" data-original-title="" title="">
                                        <label class="custom-file-label" for="image">صورة السلايدر</label>
                                        <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                        <div class="valid-feedback">بيانات صحيحة</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">حفط</button>
                            <input class="btn btn-light" type="reset" value="إلغاء">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
@endsection

