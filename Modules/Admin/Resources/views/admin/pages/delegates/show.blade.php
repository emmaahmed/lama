@extends('admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/photoswipe.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="user-profile">
            <div class="row">
                <!-- user profile first-style start-->
                <div class="col-sm-12">
                    <div class="card hovercard text-center">
                        <div style="width: 948px; height: 470px;">
                            {!! Mapper::render() !!}
                        </div>
                        <div class="user-image">
                            <div class="avatar"><img alt="" src="{{$user->image ? $user->image->name : url('assets/images/user/3.jpg')}}"></div>
{{--                            <div class="icon-wrapper"><i class="icofont icofont-pencil-alt-5"></i></div>--}}
                        </div>
                        <div class="info">
                            <div class="row">
                                <div class="col-sm-6 col-lg-4 order-sm-1 order-xl-0">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="ttl-info text-left">
                                                <h6><i class="fa fa-envelope"></i>   الربيد الإلكتروني</h6><span>{{$user->email}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="ttl-info text-left">
                                                <h6><i class="fa fa-calendar"></i>   تاريخ الميلاد</h6><span>{{$user->date_of_birth}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4 order-sm-0 order-xl-1">
                                    <div class="user-designation">
                                        <div class="title"><a target="_blank" href="">{{$user->name}}</a></div>
                                        <div class="desc mt-2">عميل</div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-4 order-sm-2 order-xl-2">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="ttl-info text-left">
                                                <h6><i class="fa fa-phone"></i>   إتصل بنا</h6><span>{{$user->mobile}}</span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="ttl-info text-left">
                                                <h6><i class="fa fa-location-arrow"></i>   الحالة</h6>
                                                @if($user->status == 1)
                                                    <span class="badge badge-info">نشط</span>
                                                @else
                                                    <span class="badge badge-warning">معلق</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="social-media">
                                <ul class="list-inline">
                                    <div class="rating-container">
                                        <div class="br-wrapper br-theme-fontawesome-stars">
                                            <div class="br-widget">
                                                @for ($i = 0; $i < 5; ++$i)
                                                    <a href="#" data-rating-value="1" data-rating-text="1" class="{{ $user->rate<=$i?'':'br-selected' }}"></a>
                                                @endfor
                                            </div>
                                        </div>
                                    </div>
                                </ul>
                            </div>
                            <div class="follow">
                                <div class="row">
                                    <div class="col-4 text-md-left border-right">
                                        <div class="follow-num counter">{{$user->orders_count}}</div>
                                        <span>
                                            <a title="كل الطلبات" href="{{route('admin.users.orders',['user_id' => $user->id , 'type' => 'all'])}}">كل الطلبات</a>
                                        </span>
                                    </div>
                                    <div class="col-4 text-md-left border-right">
                                        <div class="follow-num counter">{{$user->user_orders_count}}</div>
                                        <span>
                                            <a title="طلباتي" href="{{route('admin.users.orders',['user_id' => $user->id , 'type' => 'user'])}}">طلباتي</a>
                                        </span>
                                    </div>
                                    <div class="col-4 text-md-left">
                                        <div class="follow-num counter">{{$user->sales_orders_count}}</div>
                                        <span>
                                            <a title="توصيل" href="{{route('admin.users.orders',['user_id' => $user->id , 'type' => 'sales'])}}">توصيل</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- user profile first-style end-->
                <!-- user profile second-style start-->
                @foreach ($data['rates'] as $rate)
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="profile-img-style">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <a title="بيانات العميل" href="{{route('admin.users.show',['user_id' => $rate->user_id])}}">
                                            <div class="media"><img class="img-thumbnail rounded-circle mr-3" src="{{$user->image ? $user->image->name : url('assets/images/user/7.jpg')}}" alt="Generic placeholder image">
                                                <div class="media-body align-self-center">
                                                    <h5 class="mt-0 user-name">{{$rate->user->name}}</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 align-self-center">
                                        <div class="float-sm-right"><small>{{$rate->created_at}}</small></div>
                                    </div>
                                </div>
                                <hr>
                                <p> {{$rate->comment}}</p>
                                <div class="like-comment">
                                    <ul class="list-inline">
                                        <li class="list-inline-item border-right pr-3">
                                            <a href="/admin/orders/show/{{$rate->order_id}}" title="تفاصيل الطلب"><label class="m-0"><i class="fa fa-shopping-cart"></i>  الطلب : </label><span class="ml-2">{{$rate->order->code}}</span></a>
                                        </li>
                                        <li class="list-inline-item ml-2">
                                            <ul class="list-inline">
                                                <div class="rating-container">
                                                    <div class="br-wrapper br-theme-fontawesome-stars">
                                                        <div class="br-widget">
                                                            @for ($i = 0; $i < 5; ++$i)
                                                                <a href="#" data-rating-value="1" data-rating-text="1" class="{{ $rate->rate<=$i?'':'br-selected' }}"></a>
                                                            @endfor
                                                        </div>
                                                    </div>
                                                </div>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('modals')

@endsection

@section('scripts')
    <script src="{{ url('/assets/js/counter/jquery.waypoints.min.js') }}"></script>
    <script src="{{ url('/assets/js/counter/jquery.counterup.min.js') }}"></script>
    <script src="{{ url('/assets/js/counter/counter-custom.js') }}"></script>
    <script src="{{ url('/assets/js/photoswipe/photoswipe.min.js') }}"></script>
    <script src="{{ url('/assets/js/photoswipe/photoswipe-ui-default.min.js') }}"></script>
    <script src="{{ url('/assets/js/photoswipe/photoswipe.js') }}"></script>

@endsection