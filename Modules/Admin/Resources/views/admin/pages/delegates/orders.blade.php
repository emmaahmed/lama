@extends('admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
    {{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
    {{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
                    {{--                    <div class="card-header">--}}
                    {{--                        <h5>{{$data['title']}}</h5>--}}
                    {{--                    </div>--}}
                    <div class="card-body">
                        <div class="table-responsive">
                            {{--@if(Auth::guard('admin')->user()->can('show_order'))--}}
{{--                            <form class="row" id="form-range">--}}
{{--                                <div class="col">--}}
{{--                                    @isset($data['user_id'])--}}
{{--                                        <input type="hidden" name="user_id" id="user_id" value="{{$data['user_id']}}"/>--}}
{{--                                    @endisset--}}
{{--                                    @isset($data['worker_id'])--}}
{{--                                        <input type="hidden" name="worker_id" id="worker_id" value="{{$data['worker_id']}}"/>--}}
{{--                                    @endisset--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="from">من : </label>--}}
{{--                                        <input type="date" name="from" class="form-control" id="from">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="to">إلي : </label>--}}
{{--                                        <input type="date" name="to" class="form-control" id="to">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col">--}}
{{--                                    <button type="submit" name="filter" id="filter" class="btn btn-info btn-sm" style="margin-top: 9%;">بحث</button>--}}
{{--                                </div>--}}
{{--                            </form>--}}
                            {{--@endif--}}
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    {{--                                    @if(Auth::guard('admin')->user()->can('delete_user'))--}}
                                    <button class="btn btn-square btn-danger" style="float: left; margin-right: 2%;" id="bulk_delete_btn" type="button" title="حذف متعدد">حذف متعدد</button>
                                    <a href="{{route('admin.order.export',['type' => 1,'id' => $data['user_id']])}}" class="btn btn-square btn-success" style="float: left;" title="تحميل الملف">تحميل الملف</a>
                                    {{--                                    @endif--}}
                                    {{--                                    @if(Auth::guard('admin')->user()->can('send_user_notification'))--}}
                                    {{--<button class="btn btn-square btn-primary" style="float: left" type="button" data-toggle="modal" data-target="#sendNotification" title="إرسال متعدد">إرسال متعدد</button>--}}
                                    {{--                                    @endif--}}
                                    <input type="hidden" name="myids" id="bulk_delete_input" value="">
                                    <br>
                                    <br>
                                    <th>
                                        <input type="checkbox" class="select_all">
                                    </th>
                                    <th>رقم الطلب</th>
                                    <th>الصورة</th>
                                    <th>المتجر</th>
                                    <th>صاحب الطلب</th>
                                    <th>مندوب التوصيل</th>
                                    <th>العنوان</th>
                                    <th>الوصف</th>
                                    <th>السعر</th>
                                    <th>الشحن</th>
                                    <th>السعر الكلي</th>
                                    <th>نوع الدفع</th>
                                    <th>تاريخ الانشاء</th>
                                    <th>الحالة</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @isset($data['orders'])
                                    @foreach($data['orders'] as $order)
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="row_id" id="checkbox_{{ $order->id }}" value="{{ $order->id }}">
                                            </td>
                                            <td>{{isset($order->code) ? $order->code : ""}}</td>
                                            <td><img class="img-60 rounded-circle" src="{{!empty($order->getOriginal('image')) ? $order->image : $order->default_image}}" alt="#" data-original-title="" title=""></td>
                                            <td>{{isset($order->market) ? $order->market->name : ""}}</td>
                                            <td>{{isset($order->user) ? $order->user->name : ""}}</td>
                                            <td>{{isset($order->delegate) ? $order->delegate->name : "-"}}</td>
                                            <td>{{isset($order->location) ? $order->location : ""}}</td>
                                            <td>{{isset($order->description) ? str_limit($order->description , 100) : ""}}</td>
                                            <td>{{isset($order->price) ? $order->price : ""}}</td>
                                            <td>{{isset($order->shipping) ? $order->shipping : ""}}</td>
                                            <td>{{isset($order->total_price) ? $order->total_price : ""}}</td>
                                            <td>
                                                @if($order->status == 0)
                                                    <span class="badge badge-warning">كاش</span>
                                                @elseif($order->status == 1)
                                                    <span class="badge badge-success">اونلاين</span>
                                                @endif
                                            </td>
                                            <td>{{isset($order->created_at) ? $order->created_at : ""}}</td>
                                            <td>
                                                @if($order->status == 0)
                                                    <span class="badge badge-warning">معلق</span>
                                                @elseif($order->status == 1)
                                                    <span class="badge badge-success">جاري التوصيل</span>
                                                @elseif($order->status == 2)
                                                    <span class="badge badge-info">جاري الانتهاء</span>
                                                @elseif($order->status == 3)
                                                    <span class="badge badge-dark">منتهي</span>
                                                @elseif($order->status == 4)
                                                    <span class="badge badge-danger">تم الإلغاء</span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <a href="{{route('admin.orders.show',['order_id' => $order->id])}}" title="عرض"> <i width="20" height="20" color="orange" data-feather="eye"></i></a> &nbsp;
                                                    {{--                                                @if(Auth::guard('admin')->user()->can('delete_user'))--}}
                                                    <a onclick='return deleteOrder({{$order->id}})' title="حذف" data-id="{{$order->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a>
                                                    {{--                                                @endif--}}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endisset
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->
        </div>
        @if(isset($data['reviews']) && sizeof($data['reviews']) > 0)
            <div class="row">
                <!-- State saving Starts-->
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>التقييمات</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="table-primary">
                                    <tr>
                                        <th>الصورة</th>
                                        <th>صاحب التقييم</th>
                                        <th>التقييم</th>
                                        <th>تاريخ التقييم</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['reviews'] as $review)
                                        <tr>
                                            <td><img class="img-60 rounded-circle" src="{{!empty($review->image) ? $review->image : url('assets/images/user/3.jpg')}}" alt="#" data-original-title="" title=""></td>
                                            <td>{{!empty($review->name) ? $review->name : ""}}</td>
                                            <td>
                                                <div class="rating-container">
                                                    <div class="br-wrapper br-theme-fontawesome-stars">
                                                        <div class="br-widget">
                                                            @for ($i = 0; $i < 5; ++$i)
                                                                <a href="#" data-rating-value="1" data-rating-text="1" class="{{ $review->rate<=$i?'':'br-selected' }}"></a>
                                                            @endfor
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{!empty($review->created_at) ? $review->created_at : ""}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- State saving Ends-->

            </div>
        @endif
    </div>
    <!-- Container-fluid Ends-->
@endsection

-@section('scripts')
    <!-- Plugins JS start-->
    {{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
    {{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
    {{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>
        function deleteOrder(order_id)
        {
            var id = order_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.orders.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {order_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
            //
        }
        $(document).ready(function(){
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        myids.push(value);
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.orders.deleteMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

            // End Bulk Delete
        });
    </script>

{{--    <script>--}}
{{--        $('#form-range').submit(function (e) {--}}
{{--            e.preventDefault();--}}
{{--            var from = $('#from').val();--}}
{{--            var to = $('#to').val();--}}
{{--            var user_id = $('#user_id').val();--}}
{{--            var worker_id = $('#worker_id').val();--}}
{{--            $.ajax({--}}
{{--                type: 'get',--}}
{{--                dataType: 'html',--}}
{{--                url: '{{route('admin.orders.filter')}}',--}}
{{--                data: {--}}
{{--                    "from": from,--}}
{{--                    "to": to,--}}
{{--                    "user_id": user_id,--}}
{{--                    "worker_id": worker_id,--}}
{{--                },--}}
{{--                success: function (response) {--}}
{{--                    $('#basic-9').html(response);--}}
{{--                }--}}
{{--            });--}}
{{--        })--}}
{{--    </script>--}}
@endsection
