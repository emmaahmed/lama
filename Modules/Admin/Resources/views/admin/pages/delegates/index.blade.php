@extends('admin::admin.layouts.master_admin')
@section('styles')
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
                    {{--<div class="card-header">--}}
                        {{--<h5>{{$data['title']}}</h5>--}}
                    {{--</div>--}}
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    @if(Auth::guard('admin')->user()->can('add_delegate'))
                                        <a class="btn btn-square btn-success" href="{{route('admin.delegates.create')}}" title="إضافة"> إضافة مندوب جديد</a> &nbsp; &nbsp;
                                    @endif
                                    @if(Auth::guard('admin')->user()->can('delete_delegate'))
                                        <button class="btn btn-square btn-danger" style="float: left; margin-right: 2%;" id="bulk_delete_btn" type="button" title="حذف متعدد">حذف متعدد</button>
                                    @endif
                                    {{--<a href="{{route('admin.users.export')}}" class="btn btn-square btn-success" style="float: left;" title="تحميل الملف">تحميل الملف</a>--}}
                                    <input type="hidden" name="myids" id="bulk_delete_input" value="">
                                    <br>
                                    <br>
                                    <th>
                                        <label class="d-block" for="chk-ani">
                                            <input class="checkbox_animated select_all" id="chk-ani" type="checkbox">
                                        </label>
                                    </th>
                                    <th>الصورة</th>
                                    <th>الإسم</th>
                                    <th>البريد الإلكتروني</th>
                                    <th>رقم الهاتف</th>
                                    <th>الحالة</th>
                                    <th>عدد الطلبات</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['delegates'] as $delegate)
                                    <tr style="background-color:{{$delegate->expire_at != null && $delegate->expire_at < date('Y-m-d') ? '#FEACAC' : '' }}">
                                        <td>
                                            <label class="d-block" for="chk-ani">
                                                <input type="checkbox" class="checkbox_animated" name="row_id" id="checkbox_{{ $delegate->id }}" value="{{ $delegate->id }}">
                                            </label>
                                        </td>
                                        <td><img class="img-60 rounded-circle" src="{{!empty($delegate->image) ? $delegate->image : url('assets/images/user.png')}}" alt="#" data-original-title="" title=""></td>
                                        <td>{{isset($delegate->name) ? str_limit($delegate->name , 50) : ""}}</td>
                                        <td>{{isset($delegate->email) ? $delegate->email : ""}}</td>
                                        <td>{{isset($delegate->phone) ? $delegate->phone : ""}}</td>
                                        <td>
                                            <div class="form-group">
                                                <div class="media-body icon-state switch-outline">
                                                    <label class="switch">
                                                        <input type="checkbox" @if(!Auth::guard('admin')->user()->can('edit_delegate'))  disabled @endif id="status" class="delegate_status" data-delegate-id="{{$delegate->id}}" name="status" @if($delegate->status == 1 || $delegate->status == 2) checked @endif><span class="switch-state bg-success"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{isset($delegate->orders) ? count($delegate->orders) : ""}}</td>
                                        <td>
                                            <div class="row">

                                                <i color="blue" data-feather="eye" data-toggle="modal"
                                                   data-target="#show_{{$delegate->id}}"></i>


                                                <div class="modal fade bd-example-modal-lg" id="show_{{$delegate->id}}"
                                                     tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content row">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">
                                                                    الطلبات
                                                                    ({{sizeof($delegate->delegate_orders)}})
                                                                </h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            @if($delegate->delegate_orders)
                                                                <table class="display" id="basic-9">
                                                                    <thead>
                                                                    <tr>

                                                                        {{--                                     @if(Auth::guard('admin')->user()->can('export_order'))--}}
                                                                        {{--                                        <a href="{{route('admin.order.export',['type' => 0])}}" class="btn btn-square btn-success" style="float: left;" title="تحميل الملف">تحميل الملف</a>--}}
                                                                        {{--                                     @endif--}}


                                                                        <th>رقم الطلب</th>
                                                                        <th>لوجو المتجر</th>
                                                                        <th>اسم المتجر</th>
                                                                        <th>صاحب الطلب</th>
                                                                        <th>السعر الكلي</th>
                                                                        <th>نوع الدفع</th>
                                                                        <th>نوع التوصيل</th>
                                                                        <th style="width: 25%;">الحالة</th>
                                                                        <th style="width: 15%;">تاريخ الإنشاء</th>
                                                                        <th>العمليات</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @isset($delegate->delegate_orders)
                                                                        @foreach($delegate->delegate_orders as $order)
                                                                            <tr>

                                                                                <td>{{isset($order->order_number) ? $order->order_number : ""}}</td>
                                                                                <td><img
                                                                                        class="img-60 rounded-circle"
                                                                                        src="{{!empty($order->shop->logo) ? $order->shop->logo : url('assets/images/default.png')}}"
                                                                                        alt="#"
                                                                                        data-original-title=""
                                                                                        title=""></td>
                                                                                <td>{{isset($order->shop) ? $order->shop->name : ""}}</td>
                                                                                <td>{{isset($order->user) ? $order->user->name : ""}}</td>
                                                                                <td>{{isset($order->total_cost) ? $order->total_cost : ""}}</td>
                                                                                <td>
                                                                                    @if($order->payment_type == 0)
                                                                                        <span
                                                                                            class="badge badge-primary">كاش</span>
                                                                                    @elseif($order->payment_type == 1)
                                                                                        <span
                                                                                            class="badge badge-success">اونلاين</span>
                                                                                    @endif
                                                                                </td>
                                                                                <td>
                                                                                    @if($order->delivery_type == 0)
                                                                                        <span
                                                                                            class="badge badge-primary">توصيل عادي</span>
                                                                                    @elseif($order->delivery_type == 1)
                                                                                        <span
                                                                                            class="badge badge-success">توصيل سريع</span>
                                                                                    @endif
                                                                                </td>
                                                                                <td>
                                                                                    {{--                                                <select @if(!Auth::guard('admin')->user()->can('edit_order'))  disabled @endif @if($order->delivery_type == 0) disabled @endif name="order_status" class="custom-select form-control order_status" data-order-id="{{$order->id}}" data-shop-id="{{$order->shop->id}}">--}}
                                                                                    <select disabled
                                                                                            name="order_status"
                                                                                            class="custom-select form-control order_status"
                                                                                            data-order-id="{{$order->id}}"
                                                                                            data-shop-id="{{$order->shop->id}}">
                                                                                        <option value="0"
                                                                                                @if($order->status == 0) selected @endif>
                                                                                            قيد الإنشاء
                                                                                        </option>
                                                                                        <option value="1"
                                                                                                @if($order->status == 1) selected @endif>
                                                                                            قبول الطلب
                                                                                        </option>
                                                                                        <option value="2"
                                                                                                @if($order->status == 2) selected @endif>
                                                                                            في الطريق
                                                                                        </option>
                                                                                        <option value="3"
                                                                                                @if($order->status == 3) selected @endif>
                                                                                            تم التوصيل
                                                                                        </option>
                                                                                        <option value="4"
                                                                                                @if($order->status == 4) selected @endif>
                                                                                            ملغي
                                                                                        </option>
                                                                                    </select>
                                                                                </td>
                                                                                {{--<td>{{isset($order->created_at) ? $order->created_at : ""}}</td>--}}
                                                                                <td>{{isset($order->created_at) ? $order->created_at_time : ""}}</td>
                                                                                <td>
                                                                                    <div class="row">


                                                                                        <a href="{{route('admin.orders.invoice',['order_id' => $order->id])}}"
                                                                                           target="_blank"
                                                                                           title="طباعة الطلب"> <i
                                                                                                class="fa fa-print"></i></a>
                                                                                        &nbsp;


                                                                                        @if(Auth::guard('admin')->user()->can('show_product'))
                                                                                            <a href="{{route('admin.orders.details',['order_id' => $order->id])}}"
                                                                                               title="تفاصيل الطلب">
                                                                                                <i
                                                                                                    width="20"
                                                                                                    height="20"
                                                                                                    color="blue"
                                                                                                    data-feather="eye"></i></a>
                                                                                            &nbsp;


                                                                                        @endif
                                                                                        @if(Auth::guard('admin')->user()->can('delete_order'))
                                                                                            <a onclick='return deleteOrder({{$order->id}})'
                                                                                               title="حذف"
                                                                                               data-id="{{$order->id}}"
                                                                                               href="#"><i
                                                                                                    width="20"
                                                                                                    height="20"
                                                                                                    color="red"
                                                                                                    data-feather="trash-2"></i></a>
                                                                                        @endif
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endisset
                                                                    </tbody>
                                                                </table>
                                                            @endif


                                                        </div>
                                                    </div>
                                                </div>

{{--                                                <a href="{{route('admin.users.orders',['user' => $delegate->id])}}" title="طلباتي"> <i width="20" height="20" color="blue" data-feather="eye"></i></a> &nbsp;--}}
                                                @if(Auth::guard('admin')->user()->can('edit_delegate'))
                                                    <a title="تعديل" href="{{route('admin.delegates.edit',['user_id' => $delegate->id,'type' => 1])}}"><i width="20" height="20" color="green" data-feather="edit"></i></a> &nbsp;
                                                @endif
                                                @if(Auth::guard('admin')->user()->can('delete_delegate'))
                                                    <a onclick='return deleteDelegate({{$delegate->id}})' title="حذف" data-id="{{$delegate->id}}" href="#"><i width="20" height="20" color="red" data-feather="trash-2"></i></a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection


@section('scripts')
    <!-- Plugins JS start-->
{{--    <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/datatable/datatables/datatable.custom.js') }}"></script>--}}
{{--    <script src="{{ url('/assets/js/sweet-alert/sweetalert.min.js') }}"></script>--}}
    <script>
        $('.delegate_status').on('change.bootstrapSwitch', function(e) {
            var user_id = $(this).attr('data-delegate-id');
            var status = "";
            if (e.target.checked == true){
                var status = 1;
            }else{
                var status = 5;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('admin.delegates.status')}}",
                data: {
                    user_id: user_id,
                    status: status,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response){
                    toastr.success(response.success);
                },
                error: function(jqXHR){
                    toastr.error(jqXHR.responseJSON.message);
                }
                });
        });
        function deleteDelegate(delegate_id)
        {
            var id = delegate_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.delegates.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {user_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
        //
        }
        $(document).ready(function(){
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
            // Bulk Delete
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkdeleteinput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            // Bulk delete listener
            $bulkDeleteBtn.click(function (e) {
                var myids = [];
                var $checkedBoxes = $('#basic-9 input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                if (count) {
                    // Reset input value
                    $bulkdeleteinput.val('');
                    $.each($checkedBoxes, function () {
                        var value = $(this).val();
                        if (value !== 'on'){
                            myids.push(value);
                        }
                    });
                    // Set input value
                    $bulkdeleteinput.val(myids);
                    // Show modal
                    e.preventDefault();
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.delegates.deleteMulti')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {ids:myids,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function(jqXHR){
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    });
                } else {
                    // No row selected
                    toastr.warning('Nothing to Delete');
                }
            });

            // End Bulk Delete
        });
        function deleteOrder(order_id)
        {
            var id = order_id;
            swal({
                title: 'هل أنت متأكد!',
                text: 'عفواً لا يمكنك التراجع عن هذا الأمر',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: 'POST',
                        url: "{{URL::route('admin.orders.delete')}}",
                        dataType: 'json',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {order_id:id,"_token": "{{ csrf_token() }}"},

                        success: function(response){
                            window.location.reload();
                            toastr.success(response.success);
                        },
                        error: function() {
                            swal(
                                'يوجد خطأ ما',
                                'من فضلك حاول مرة أخري',
                                'خطأ'
                            )
                        }
                    });
                }
            });
            //
        }
    </script>
@endsection
