<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
    body {
        background: grey;
        margin-top: 120px;
        margin-bottom: 120px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row p-5">
                        <div class="col-md-6">
                            <img src="{{url('/assets/images/logo.png')}}" alt="logo">
                            <img alt="" >
{{--                            <img src="{{$order->image ? $order->image->name : url('assets/images/user/3.jpg')}}" class="img-thumbnail img-fluid" alt="User Profile" width="100" height="100" />--}}
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="mb-1">
                                {{isset($data['order']->code) ? $data['order']->code : ''}}
                                <strong>: رقم الطلب  </strong>
                            </p>
                        </div>
                    </div>

                    <hr class="my-5">

                    <div class="row pb-5 p-5">
                        <div class="col-md-12 text-right">
                            <p>{{isset($data['order']->code) ? $data['order']->code : ''}}<strong> : رقـم الطلب</strong></p><hr>
                            <p><strong>المتجر : </strong>{{isset($data['order']->market) ? $data['order']->market->name : ''}}</p><hr>
                            <p>{{isset($data['order']->user) ? $data['order']->user->name : ''}}<strong> : صاحب الطلب</strong></p><hr>
                            <p>{{isset($data['order']->delegate) ? $data['order']->delegate->name : ''}}<strong> : مندوب التوصيل</strong></p><hr>
                            <p>{{isset($data['order']->description) ? $data['order']->description : ''}}<strong> : الوصف</strong></p><hr>
                            <p>{{isset($data['order']->price) ? $data['order']->price : ''}}<strong> : سعر الطلب</strong></p><hr>
                            <p>{{isset($data['order']->shipping) ? $data['order']->shipping : ''}}<strong> : تكلفة التوصيل</strong></p><hr>
                            <p>{{isset($data['order']->total_price) ? $data['order']->total_price : ''}}<strong> : السعر الإجمالي</strong></p><hr>
                            <p><strong>العنوان : </strong>{{isset($data['order']->location) ? $data['order']->location : ''}}</p><hr>
                            <p> {{$data['order']->created_at}}  <strong> : تاريخ الطلب</strong> </p>
                            <hr>
                            <p>
                                @if($data['order']->status == 0)
                                    <span class="badge badge-warning">معلق</span>
                                @elseif($data['order']->status == 1)
                                    <span class="badge badge-success">جاري التوصيل</span>
                                @elseif($data['order']->status == 2)
                                    <span class="badge badge-info">جاري الانتهاء</span>
                                @elseif($data['order']->status == 3)
                                    <span class="badge badge-dark">منتهي</span>
                                @elseif($data['order']->status == 4)
                                    <span class="badge badge-danger">تم الإلغاء</span>
                                @endif
                                    <strong> : حالة الطلب  </strong>
                            </p>
                            <hr>
                            <p>
                                @if($data['order']->payment == 0)
                                    <span class="badge badge-primary">كاش</span>
                                @elseif($order->payment_type == 1)
                                    <span class="badge badge-success">اونلاين</span>
                                @endif
                                    <strong> :  نوع الدفع </strong>
                            </p>
                        </div>

                    </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">الإجمالي</div>
                            <div class="h2 font-weight-light">{{$data['order']->total_price}} </div>
                        </div>

{{--                        <div class="py-3 px-5 text-right">--}}
{{--                            <div class="mb-2">Discount</div>--}}
{{--                            <div class="h2 font-weight-light">10%</div>--}}
{{--                        </div>--}}

{{--                        <div class="py-3 px-5 text-right">--}}
{{--                            <div class="mb-2">Sub - Total amount</div>--}}
{{--                            <div class="h2 font-weight-light">$32,432</div>--}}
{{--                        </div>--}}
                    </div>
                    <hr class="my-5">
                </div>
            </div>
        </div>
    </div>
</div>


