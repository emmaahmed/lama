@extends('admin::admin.layouts.master_admin')
@section('styles')
    <!-- Plugins css start-->
    {{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/rating.css') }}">
    {{--    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/sweetalert2.css') }}">--}}

    <!-- Plugins css Ends-->
    <style>
        #basic-9_length{
            display: none;
        }
        #basic-9_filter{
            display: none;
        }
        #basic-9_info{
            display: none;
        }
        #basic-9_paginate{
            display: none;
        }
    </style>
@endsection
@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- State saving Starts-->
            <div class="col-sm-12">
                <div class="card">
                        <div class="card-header">
                            <h5>{{$data['title']}}</h5>
                        </div>
                    <div class="card-body animate-chk">
                        <div class="table-responsive">
                            <div class="col">
                                <div class="form-group">
                                    <label for="type">نوع التقرير </label>
                                    <select name="type" id="type" class="custom-select form-control select_status">
                                        <option value="" selected disabled="">اختر مدة التقرير</option>
                                        <option value="daily">يوميًا</option>
                                        <option value="weekly">اسبوعيًا</option>
                                        <option value="monthly">شهريًا</option>
                                        <option value="annually">سنويًا</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="card" id="reports-div">
                    <div class="card-header">
                        <h5>تفاصيل التقرير</h5>
                        <a id="export" hidden href="{{route('admin.orders.reports.export',['type' => 0])}}" class="btn btn-square btn-success" style="float: left;" title="تحميل الملف">تحميل الملف</a>
                    </div>
                    <div class="card-body animate-chk">
                        <div class="table-responsive">
                            <table class="display" id="basic-9">
                                <thead>
                                <tr>
                                    <th>صورة المتجر</th>
                                    <th>اسم المتجر</th>
                                    <th class="row">
                                        <div class="col-md-3">
                                            إسم المنتج
                                        </div>
                                        <div class="col-md-3">
                                            الكمية المباعة
                                        </div>
                                        <div class="col-md-3">
                                            السعر
                                        </div>
                                        <div class="col-md-3">
                                            عدد المشترين
                                        </div>
                                    </th>
                                    <th>اجمالي البيع</th>
                                </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- State saving Ends-->
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@section('scripts')
    <script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
    <script>
        $('#type').change(function () {
            var type = $(this).val();
            $('#export').prop('hidden', false);
            $.ajax({
                type: 'get',
                dataType: 'html',
                url: '{{route('admin.orders.getReports')}}',
                data: {
                    "type": type
                },
                success: function (response) {
                    $('#basic-9').html(response);
                    var url = '{{ route("admin.orders.reports.export", ":type") }}';
                    url = url.replace(':type', type);
                    $("#export").attr("href", url);
                }
            });
        });
    </script>
@endsection
