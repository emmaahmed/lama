@include('admin::admin.layouts.includes.header')
<div class="container-fluid">
    <div class="authentication-main">
        <div class="row">
            <div class="col-md-12 p-0">
                <div class="auth-innerright">
{{--                    <div class="reset-password-box" style="margin-bottom: 20%;">--}}
                    <div class="reset-password-box" style="margin-bottom: 20%;">
                        <div class="text-center"><img src="{{url('/assets/images/logo.png')}}" alt=""></div>
                        <div class="card mt-4 mb-0">
                            <h4>إعادة تعيين كلمة المرور</h4>
                            <form class="needs-validation"  novalidate="" class="theme-form" method="POST" action="{{ route('forget.password') }}">
                                @csrf
                                <div class="form-group">
                                    <label class="col-form-label" for="email"><strong>البريد الإلكتروني</strong></label>
                                    <div class="form-row">
                                        <div class="col-md-10">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                        @endif
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-primary m-0" type="submit">إرسال</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin::admin.layouts.includes.footer')
