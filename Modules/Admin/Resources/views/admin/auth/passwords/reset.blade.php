@include('admin::admin.layouts.includes.header')
<div class="container-fluid">
    <!-- Reset Password page start-->
    <div class="authentication-main">
        <div class="row">
            <div class="col-md-12 p-0">
                <div class="auth-innerright">
                    <div class="authentication-box">
                        <div class="text-center"><img src="{{url('/assets/images/logo.png')}}" alt=""></div>
                        <div class="card mt-4 p-4">
                            <form class="needs-validation"  novalidate="" class="theme-form" method="POST" action="{{route('reset.password')}}">
                                @csrf
                                <input type="hidden" name="token" value="{{$data->token}}"/>
                                <h5 class="f-16 mb-3 f-w-600">تغيير كلمة المرور</h5>
                                @include('admin::admin.layouts.includes.message')
                                <div class="form-group">
                                    <label class="col-form-label">كلمة المرور</label>
                                    <input class="form-control" name="password" type="password" placeholder="*****" required>
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">تأكيد كلمة المرور</label>
                                    <input class="form-control" name="password_confirmation" type="password" placeholder="*****" required>
                                    <div class="invalid-feedback">لا يمكنك ترك هذا الحقل فارغ</div>
                                </div>
                                <div class="form-group form-row mb-0">
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="submit">تغيير</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Reset Password page end-->
</div>
<script src="{{ url('/assets/js/form-validation-custom.js') }}"></script>
@include('admin::admin.layouts.includes.footer')
