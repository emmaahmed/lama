     @if(Auth::guard('admin')->check())
        <!-- footer start-->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-4 footer-copyright">
                        <p class="mb-1" style="margin-right: 10%;">جميع حقوق النشر محفوظه</p>
                    </div>
                    <div class="col-md-2">
                        <a class="pull-right mb-0" href="http://2grand.net" target="_blank"><img src="{{ url('assets/images/logo/grand.png')}}" style="width:100px;">
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    @endif
        </div>
        </div>

        <!-- latest jquery-->
        <script src="{{ url('/assets/js/jquery-3.2.1.min.js') }}"></script>
        @yield('scripts')
        <!-- Bootstrap js-->
        <script src="{{ url('/assets/js/bootstrap/popper.min.js') }}"></script>
        <script src="{{ url('/assets/js/bootstrap/bootstrap.js') }}"></script>
        <!-- feather icon js-->
        <script src="{{ url('/assets/js/icons/feather-icon/feather.min.js') }}"></script>
        <script src="{{ url('/assets/js/icons/feather-icon/feather-icon.js') }}"></script>
        <!-- Sidebar jquery-->
        <script src="{{ url('/assets/js/sidebar-menu.js') }}"></script>
        <script src="{{ url('/assets/js/config.js') }}"></script>

        <script src="{{ url('/assets/js/typeahead/handlebars.js') }}"></script>
        <script src="{{ url('/assets/js/typeahead/typeahead.bundle.js') }}"></script>
        <script src="{{ url('/assets/js/typeahead/typeahead.custom.js') }}"></script>


        <script src="{{ url('/assets/js/notify/bootstrap-notify.min.js') }}"></script>

        <script src="{{ url('/assets/js/chat-menu.js') }}"></script>
        <script src="{{ url('/assets/js/height-equal.js') }}"></script>
        <script src="{{ url('/assets/js/tooltip-init.js') }}"></script>

        <script src="{{ url('/assets/js/select2/select2.full.min.js') }}"></script>
        <script src="{{ url('/assets/js/select2/select2-custom.js') }}"></script>

        <script src="{{ url('/assets/js/typeahead-search/handlebars.js') }}"></script>
        <script src="{{ url('/assets/js/typeahead-search/typeahead-custom.js') }}"></script>
        <!-- Plugins JS Ends-->
        <!-- Theme js-->
        <script src="{{ url('/assets/js/script.js') }}"></script>
{{--        <script src="{{ url('/assets/js/theme-customizer/customizer.js') }}"></script>--}}
        <script src="{{ url('/assets/js/toastr.min.js') }}"></script>
        <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
{{--        --}}
        <script src="{{ url('/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ url('/assets/js/datatable/datatable-extension/dataTables.buttons.min.js') }}"></script>
        <script src="{{ url('/assets/js/datatable/datatable-extension/buttons.html5.min.js') }}"></script>
        <script src="{{ url('/assets/js/datatable/datatable-extension/buttons.colVis.min.js') }}"></script>

{{--        --}}
        <script src="{{ url('/assets/js/sweetalert.min.js') }}"></script>
        <!-- Plugin used-->
        <script type="text/javascript">
            toastr.options = {
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                toastClass: 'alert',
            };
        </script>
        <script type="text/javascript">
            @if(session()->has('success'))
                toastr.success("{{session()->get('success')}}");
            @elseif(session()->has('error'))
                toastr.error("{{session()->get('error')}}");
            @endif
        </script>
        <script>
            $('#basic-9').DataTable({
                "language": {
                    "sProcessing": "جارٍ التحميل...",
                    "sLengthMenu": "أظهر _MENU_ مدخلات",
                    "sZeroRecords": "لم يعثر على أية سجلات",
                    "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                    "sInfoPostFix": "",
                    "sSearch": "ابحث:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "الأول",
                        "sPrevious": "السابق",
                        "sNext": "التالي",
                        "sLast": "الأخير"
                    }
                }
            });
        </script>
        <!-- The core Firebase JS SDK is always required and must be listed first -->
        <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-app.js" ></script>
        <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-messaging.js" ></script>





        <script>
            // Retrieve Firebase Messaging object.

            // IDs of divs that display registration token UI or request permission UI.
            // messaging.onBackgroundMessage(function(payload) {
            //   console.log('[firebase-messaging-sw.js] Received background message ', payload);
            //   // Customize notification here
            //   const notificationTitle = 'Background Message Title';
            //   const notificationOptions = {
            //     body: 'Background Message body.',
            //     icon: '/firebase-logo.png'
            //   };

            //   self.registration.showNotification(notificationTitle,
            //     notificationOptions);
            // });

            const tokenDivId = 'token_div';
            const permissionDivId = 'permission_div';
            firebase.initializeApp({
                apiKey: "AIzaSyBm6BnPO2GEGoYyqosPrUlvuRnNmWZ9mVY",
                authDomain: "lama-84345.firebaseapp.com",
                projectId: "lama-84345",
                storageBucket: "lama-84345.appspot.com",
                messagingSenderId: "654711803490",
                appId: "1:654711803490:web:fd03d963fc0aede0d48b3b",
                measurementId: "G-06MSCXBJ9T"
            });
            const messaging = firebase.messaging();

            // Handle incoming messages. Called when:
            // - a message is received while the app has focus
            // - the user clicks on an app notification created by a service worker
            //   `messaging.onBackgroundMessage` handler.


            function resetUI() {

                messaging.onMessage((payload) => {
                 //   console.log(payload.data['gcm.notification.type']);
                    if(payload.data['gcm.notification.type']=='shop-message'){
                        $('<audio id="chatAudio"><source src="https://lamh.online/sound.mp3" type="audio/ogg"><source src="https://lamh.online/sound.mp3" type="audio/mpeg"><source src="https://lamh.online/sound.mp3" type="audio/wav"></audio>').appendTo('body');
                        $('#chatAudio')[0].play();
                        $('#order-counter').html(count+1);

                        $('#notifications-ul').prepend(
                            '                  <li>' +
                            '                     <a href="/shop/reply/' + payload.data['gcm.notification.shop_id'] + '"> <div class="media">' +
                            '                          <div class="media-body">' +
                            '                              <h6 class="mt-0"><span><i class="shopping-color" data-feather="shopping-bag"></i></span>لديك رسالة جديدة' +
                            '<small class="pull-right">' + new Date(Date.now()).toString() + '</small></h6>\n' +
                            '                              <p class="mb-0">اضغط هنا لتصفح الرسالة</p>' +
                            '                          </div>' +
                            '                      </div></a>' +
                            '                  </li>\n'
                        );
                        var notify = $.notify('<i class="fa fa-cloud-upload"></i><strong>رسالة جديدة</strong> لديك رسالة جديدة', {
                            type: 'theme',
                            allow_dismiss: true,
                            delay: 2000,
                            timer: 300
                        });

                        setTimeout(function() {
                            notify.update('message', '<i class="fa fa-cloud-upload"></i><strong>رسالة جديدة</strong> لديك رسالة جديدة.');
                        }, 1000);
                        $('#message-chat'+payload.data['gcm.notification.shop_id']).append(
'                                                        <li class="clearfix">\n' +
'                                                            <div class="message other-message pull-right"><img\n' +
'                                                                    class="rounded-circle float-right chat-user-img img-30"\n' +
'                                                                    src="'+payload.data['gcm.notification.user_image']+'" alt="">\n' +
'                                                                <div class="message-data"><span\n' +
'                                                                        class="message-data-time"> {{\Carbon\Carbon::now()->format('Y-M-D')}}</span>\n' +
'                                                                </div>\n'
                            +payload.data['gcm.notification.user_message']+
                            '                                                            </div>\n' +
                            '                                                        </li>\n'

                        );
                    }
                    // Update the UI to include the received message.
                    appendMessage(payload);
                });




                // clearMessages();
                // showToken('loading...');
                // Get registration token. Initially this makes a network call, once retrieved
                // subsequent calls to getToken will return from cache.
                messaging.getToken({vapidKey: 'BFLNZBCrORXv7TFXxbYQgCMBRVWQD309XCmY0JM3JF6eEUO1bPcvXLVL3eCmdaN7PWgZ3a8NZF6jylayAXpma-0'}).then((currentToken) => {
                    if (currentToken) {
                        sendTokenToServer(currentToken);
                        // updateUIForPushEnabled(currentToken);
                    } else {
                        // Show permission request.
                        console.log('No registration token available. Request permission to generate one.');
                        // Show permission UI.
                        // updateUIForPushPermissionRequired();
                        // setTokenSentToServer(false);
                    }
                }).catch((err) => {
                    console.log('An error occurred while retrieving token. ', err);
                    // showToken('Error retrieving registration token. ', err);
                    setTokenSentToServer(false);
                });
            }


            // function showToken(currentToken) {
            //     // Show token in console and UI.
            //     const tokenElement = document.querySelector('#token');
            //     tokenElement.textContent = currentToken;
            // }

            // Send the registration token your application server, so that it can:
            // - send messages back to this app
            // - subscribe/unsubscribe the token from topics
            function sendTokenToServer(currentToken) {
                // if (!isTokenSentToServer()) {
                //     console.log('Sending token to server...');
                //     // TODO(developer): Send the current token to your server.
                    setTokenSentToServer(true,currentToken);
                // } else {
                //     console.log('Token already sent to server so won\'t send it again ' +
                //         'unless it changes');
                // }
            }
            //
            function isTokenSentToServer() {
                return window.localStorage.getItem('sentToServer') === '1';
            }
            //
            function setTokenSentToServer(sent,currentToken) {
                // window.localStorage.setItem('sentToServer', sent ? '1' : '0');
                $.ajax({
                    method: 'POST',
                    url: "{{URL::route('admin.admins.update-token')}}",
                    dataType: 'json',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: {firebase_token:currentToken,"_token": "{{ csrf_token() }}"},

                    success: function(response){
                        // window.location.reload();
                        // toastr.success(response.success);
                    },
                    error: function() {
                    }
                });

            }
            function appendMessage(payload) {
                console.log(payload);
            }

            resetUI();
        </script>

        <script>
          //   // Your web app's Firebase configuration
          //   var firebaseConfig = {
          //       apiKey: "AIzaSyADVFxbyZ5RxYuahKD2oVNHeTWGT70KLQE",
          //       // authDomain: "lamah-9a470.firebaseapp.com",
          //       // databaseURL: "https://lamah-9a470.firebaseio.com",
          //       projectId: "lamah-9a470",
          //       // storageBucket: "lamah-9a470.appspot.com",
          //       messagingSenderId: "151230146434",
          //       appId: "1:151230146434:web:08452f3deb1f224be5919b"
          //   };
          //   // Initialize Firebase
          // firebase.initializeApp(firebaseConfig);
          //   const messaging = firebase.messaging();
            // messaging.onMessage(function(payload) {
            //     console.log(
            //         "[firebase-messaging-sw.js] Received background message ",
            //         payload,
            //     );
            //     // Customize notification here
            //     const notificationTitle = "Background Message Title";
            //     const notificationOptions = {
            //         body: "Background Message body.",
            //         icon: "/itwonders-web-logo.png",
            //     };
            //
            //     return self.registration.showNotification(
            //         notificationTitle,
            //         notificationOptions,
            //     );
            // });
            // messaging
            //     .requestPermission()
            //     .then(function () {
            //         // MsgElem.innerHTML = "Notification permission granted."
            //         console.log("Notification permission granted.");
            //
            //         // get the token in the form of promise
            //         return messaging.getToken()
            //     })
            //     .then(function(token) {
            //         // print the token on the HTML page
            //         // TokenElem.innerHTML = "token is : " + token
            //     })
            //     .catch(function (err) {
            //         // ErrElem.innerHTML = ErrElem.innerHTML + "; " + err
            //         console.log("Unable to get permission to notify.", err);
            //     });



        </script>

        </body>
</html>
