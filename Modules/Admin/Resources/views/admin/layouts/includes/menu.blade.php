<div class="page-sidebar">
    <div class="main-header-left d-none d-lg-block">
        {{--    <div class="logo-wrapper"><a href="#"><img style="margin-top: 595%;" src="{{url('/assets/images/grand.png')}}" alt="غصن"></a></div>--}}
        <div class="logo-wrapper"><a href="#"><img src="{{url('/assets/images/logo-horizontal.png')}}" alt="غصن"></a>
        </div>
    </div>
    <div class="sidebar custom-scrollbar">
        {{--<div class="sidebar-user text-center">--}}
        {{--<div><img class="img-60 rounded-circle" src="{{Auth::guard('admin')->user()->image ? Auth::guard('admin')->user()->image : url('assets/images/default.png')}}" alt="profile">--}}
        {{--<div class="profile-edit"><a href="{{route('admin.admins.edit',['admin_id' => \Illuminate\Support\Facades\Auth::guard('admin')->user()->id])}}" target="_blank"><i data-feather="edit"></i></a></div>--}}
        {{--</div>--}}
        {{--<h6 class="mt-3 f-14">{{Auth::guard('admin')->user()->name}}</h6>--}}
        {{--<p>مشرف</p>--}}
        {{--</div>--}}
        <ul class="sidebar-menu">
            @if(Auth::guard('admin')->user()->can('show_statistics'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'dashboard') style="color: white;"
                       @endif href="{{route('admin.dashboard')}}"><i data-feather="home"></i><span> الرئيسية</span></a>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_admin'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'admins') style="color: white;"
                       @endif href="{{route('admin.admins')}}"><i
                            data-feather="user-check"></i><span> المشرفين</span></a></li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_user'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'users') style="color: white;"
                       @endif href="{{route('admin.users',['type' => 0])}}"><i data-feather="users"></i><span> المستخدمين</span></a>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_delegate'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'delegates') style="color: white;"
                       @endif href="{{route('admin.delegates',['type' => 1])}}"><i data-feather="users"></i><span> مندوبي التوصيل</span></a>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_delegate'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'delegates') style="color: white;"
                       @endif href="{{route('admin.delegates.not-active',['type' => 1])}}"><i data-feather="users"></i><span> المندوبين المنتهيين</span></a>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_category'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'categories') style="color: white;"
                       @endif href="{{route('admin.categories')}}"><i data-feather="list"></i><span> الأقسام</span></a>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_city'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'cities') style="color: white;"
                       @endif href="{{route('admin.cities')}}"><i data-feather="list"></i><span> المدن</span></a></li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_shop'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'shops') style="color: white;"
                       @endif href="{{route('admin.shops')}}"><i data-feather="list"></i><span> المتاجر</span></a></li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_shop'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'shops') style="color: white;"
                       @endif href="{{route('admin.shops.not-active')}}"><i data-feather="list"></i><span> المتاجر المنتهية</span></a></li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_slider'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'sliders') style="color: white;"
                       @endif href="{{route('admin.sliders')}}"><i data-feather="list"></i><span> السلايدر</span></a>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_offer'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'offers') style="color: white;"
                       @endif href="{{route('admin.offers')}}"><i data-feather="list"></i><span> العروض</span></a></li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_order'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'orders') style="color: white;"
                       @endif href="{{route('admin.orders')}}"><i data-feather="list"></i><span> الطلبات</span></a></li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_sent') || Auth::guard('admin')->user()->can('show_inbox') || Auth::guard('admin')->user()->can('send_notification'))
                <li @if(request()->segment(3) == 'notifications') class="open" @endif><a class="sidebar-header"
                                                                                         @if(request()->segment(2) == 'notifications') style="color: white;"
                                                                                         @endif @if(request()->segment(2) == 'contacts') style="color: white;"
                                                                                         @endif href="#"><i
                            data-feather="message-square"></i><span>الرسائل والإشعارات</span><i
                            class="fa fa-angle-right pull-right"></i></a>
                    <ul class="sidebar-submenu @if(request()->segment(3) == 'notifications') open @endif">
                        @if(Auth::guard('admin')->user()->can('show_sent'))
                            <li><a @if(request()->segment(3) == 'notifications') style="color: white;"
                                   @endif class="sidebar-header"
                                   @if(request()->segment(2) == 'notifications') style="color: white;"
                                   @endif href="{{route('admin.notifications')}}"><i
                                        data-feather="message-circle"></i><span> صندوق الصادر</span></a></li>
                        @endif
                        @if(Auth::guard('admin')->user()->can('show_inbox'))
                            <li><a @if(request()->segment(3) == 'contacts') style="color: white;"
                                   @endif class="sidebar-header"
                                   @if(request()->segment(2) == 'contacts') style="color: white;"
                                   @endif href="{{route('admin.contacts')}}"><i data-feather="inbox"></i><span> صندوق الوارد</span></a>
                            </li>
                        @endif
                        @if(Auth::guard('admin')->user()->can('send_notification'))
                            <li><a @if(request()->segment(3) == 'send-notification') style="color: white;"
                                   @endif class="sidebar-header" href="{{route('admin.notifications.send')}}"><i
                                        data-feather="send"></i><span> إرسال إشعار</span></a></li>
                        @endif
                    </ul>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('edit_settings'))
                <li @if(request()->segment(2) == 'settings') class="open" @endif>
                    <a class="sidebar-header" href="#"><i
                            data-feather="settings"></i><span>الإعدادات</span><i
                            class="fa fa-angle-right pull-right"></i></a>
                    <ul class="sidebar-submenu @if(request()->segment(2) == 'settings') open @endif">
                        @if(Auth::guard('admin')->user()->can('show_tutorial'))
                            <li><a @if(request()->segment(3) == 'tutorials') style="color: white;"
                                   @endif class="sidebar-header" href="{{route('admin.settings.tutorials')}}"><i
                                        data-feather="book-open"></i><span> الصفحات التعريفية</span></a></li>
                        @endif
                        @if(Auth::guard('admin')->user()->can('show_social'))
                            <li><a @if(request()->segment(3) == 'socials') style="color: white;"
                                   @endif class="sidebar-header" href="{{route('admin.settings.socials')}}"><i
                                        data-feather="chrome"></i><span> وسائل التواصل الإجتماعي</span></a></li>
                        @endif
                        @if(Auth::guard('admin')->user()->can('edit_settings'))
                            <li><a @if(request()->segment(3) == 'edit') style="color: white;"
                                   @endif class="sidebar-header" href="{{route('admin.settings')}}"><i
                                        data-feather="settings"></i><span> إعدادات عامة</span></a></li>
                        @endif
                    </ul>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_statistics'))
                <li><a class="sidebar-header" href="{{route('admin.chat')}}" data-original-title="" title="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-message-square">
                            <path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path>
                        </svg>
                        <span>الشات مع المتاجر</span></a>
                </li>
                {{--                <li> <a class="sidebar-header" @if(request()->segment(2) == 'dashboard') style="color: white;" @endif href="{{route('admin.dashboard')}}"><i data-feather="home"></i><span> الرئيسية</span></a></li>--}}
            @endif

            @if(Auth::guard('admin')->user()->can('show_shop'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'requests') style="color: white;"
                       @endif href="{{route('admin.shopRequests')}}"><i
                            data-feather="list"></i><span>  طلبات المتاجر الغير مفعلة</span></a>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_shop'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'requests') style="color: white;"
                       @endif href="{{route('admin.shopRequests.activated')}}"><i
                            data-feather="list"></i><span>طلبات المتاجر </span></a>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_shop'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'requests') style="color: white;"
                       @endif href="{{route('admin.delegateRequests')}}"><i
                            data-feather="list"></i><span>طلبات المناديب الغيرالمفعلة</span></a>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_shop'))
                <li><a class="sidebar-header" @if(request()->segment(2) == 'requests') style="color: white;"
                       @endif href="{{route('admin.delegateRequests.activated')}}"><i
                            data-feather="list"></i><span>طلبات المناديب  مفعلة</span></a>
                </li>
            @endif

{{--            @if(Auth::guard('admin')->user()->can('show_statistics'))--}}
{{--                <li><a class="sidebar-header" @if(request()->segment(2) == 'fees') style="color: white;"--}}
{{--                       @endif href="{{route('admin.fees')}}"><i--}}
{{--                            data-feather="list"></i><span>اسعار النطاقات</span></a>--}}
{{--                </li>--}}
{{--            @endif--}}


            @if(Auth::guard('admin')->user()->can('show_statistics'))
                <li><a class="sidebar-header" href="{{asset('admin/terms')}}"><i data-feather="home"></i>
                        <span>الشروط و الاحكام</span>
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_statistics'))
                <li><a class="sidebar-header" href="{{asset('admin/abouts')}}"><i data-feather="home"></i>
                        <span>عن التطبيق</span>
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                </li>
            @endif
            @if(Auth::guard('admin')->user()->can('show_statistics'))
                <li><a class="sidebar-header" href="{{asset('admin/explains')}}"><i data-feather="home"></i>
                        <span>الأسئلة الشائعة</span>
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                </li>
            @endif
                @if(Auth::guard('admin')->user()->can('show_statistics'))

                <li><a class="sidebar-header" href="{{asset('admin/form-terms')}}"><i data-feather="home"></i>
                        <span>شروط كن متجر</span>
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                </li>

                <li><a class="sidebar-header" href="{{asset('admin/form-terms/delegates')}}"><i data-feather="home"></i>
                        <span>شروط كن مندوب</span>
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                </li>

                @endif

            {{--@if(Auth::guard('admin')->user()->can('show_reports'))--}}
            {{--<li @if(request()->segment(2) == 'reports') class="active" @endif> <a class="sidebar-header" href="{{route('admin.orders.reports')}}"><i data-feather="list"></i><span> التقارير</span></a></li>--}}
            {{--@endif--}}
        </ul>
    </div>
</div>
