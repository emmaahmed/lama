<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="endless admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
  <meta name="keywords" content="admin template, endless admin template, dashboard template, flat admin template, responsive admin template, web app">
  <meta name="author" content="pixelstrap">
  <link rel="icon" href="{{ url('/assets/images/logo.jpeg') }}" type="image/x-icon">
  <link rel="shortcut icon" href="{{ url('/assets/images/logo.jpeg') }}"  type="image/x-icon">
  <title>لمه</title>
  <!-- Google font-->
  <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Font Awesome-->
  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/fontawesome.css') }}">
  <!-- ico-font-->
  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/icofont.css') }}">
  <!-- Themify icon-->
  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/themify.css') }}">
  <!-- Flag icon-->
  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/flag-icon.css') }}">
  <!-- Feather icon-->
  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/feather-icon.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatables.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/datatable-extension.css') }}">
  <link rel="stylesheet" href="{{ url('/assets/css/sweetalert.css') }}">
  <link rel="stylesheet" href="{{ url('/assets/css/select2.css') }}">
  <!-- Plugins css start-->
{{--  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/chartist.css') }}">--}}
{{--  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/prism.css') }}">--}}
  <!-- Plugins css Ends-->
@yield('styles')
  <style>
    .page-main-header .main-header-right .nav-right>ul>li:first-child {
      margin-right: 87% !important;
      border-left: none;
    }
  </style>
<!-- Plugins css start-->
  <!-- Bootstrap css-->
  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/bootstrap.css') }}">
<!-- App css-->
  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/style.css') }}">
  <link id="color" rel="stylesheet" media="screen" href="{{ url('/assets/css/light-1.css') }}">
<!-- Responsive css-->
  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/responsive.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/toastr.min.css') }}">

  <style>
    table.dataTable input {
      height: 10px;
    }
    .toast-message {
        /*font-weight: bold;*/
        font-size: 18px;
    }
    </style>
{{--  <style>--}}
{{--    @font-face {--}}
{{--      font-family:GESSTwoBold ;--}}
{{--      src: url({{asset('GE-SS-Two-Bold.otf')}});--}}
{{--    }--}}
{{--    * {--}}
{{--      margin: 0px;--}}
{{--      padding: 0px;--}}
{{--      box-sizing: border-box;--}}
{{--      font-family: GESSTwoBold, Arial, sans-serif;--}}
{{--      /*color: #602248;*/--}}
{{--    }--}}

{{--  </style>--}}
{{--  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />--}}
{{--  <link href="https://fonts.googleapis.com/earlyaccess/droidarabickufi.css" rel="stylesheet" type="text/css" />--}}
{{--  <style>--}}
{{--    body{--}}
{{--      font-family: 'Droid Arabic Kufi' !important;--}}
{{--      font-size: 14px;--}}
{{--    }--}}
{{--    h1,h2,h3,h4,h5,h6,span,p{--}}
{{--      font-family: 'Droid Arabic Kufi' !important;--}}
{{--      font-size: 14px;--}}
{{--    }--}}
{{--    .dataTables_wrapper {--}}
{{--      font-family: 'Droid Arabic Kufi' !important;--}}
{{--      font-size: 14px;--}}
{{--    }--}}
{{--    .bootstrap-tagsinput{--}}
{{--      display: block !important;--}}
{{--    }--}}
{{--    .bootstrap-tagsinput input {--}}
{{--      width: 15em !important;--}}
{{--    }--}}
{{--  </style>--}}
</head>
<body main-theme-layout="rtl">
<!-- Loader starts-->
<div class="loader-wrapper">
  <div class="loader bg-white">
    <div class="whirly-loader"> </div>
  </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper">
