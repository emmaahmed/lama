<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/23/2020
 * Time: 3:30 PM
 */

namespace Modules\Admin\Repositories;


use App\Mail\ShopMail;
use App\Notification;
use App\User;
use Illuminate\Support\Facades\Mail;
use Modules\Admin\Entities\AdminNotification;
use Modules\Admin\Repositories\Interfaces\AdminNotificationRepositoryInterface;
use Modules\Shop\Entities\Shop;

class AdminNotificationRepository implements AdminNotificationRepositoryInterface
{
    public function index()
    {
        $data['title'] = 'رسائل النظام';
        $data['notifications'] = AdminNotification::latest()->get();
        return $data;
    }

    public function sendNotificationView()
    {
        $data['title'] = 'ارسال رسالة';
        $data['users'] = User::get()->map(function ($item) {
            $item['type'] = 'user';
            return $item;
        });
        $data['shops'] = Shop::get()->map(function ($item) {
            $item['type'] = 'shop';
            return $item;
        });
        return $data;
    }

    public function sendNotification($request)
    {
        $user_type = explode('|', $request->person);
        if ($user_type['0'] == 'All_users'){
//            $users_token = User::where()->pluck('firebase_token')->toArray();
//            dd(count($users_token));
            User::where('type',0)
                ->chunk(1000, function ($users) use ($request) {
                    foreach ($users as $user) {
                        $notification = new Notification();
                        $notification->user_id = $user->id;
                        $notification->img = $request->image;

                        $notification->save();
                        $notification->translateOrNew('en')->title = 'Message From Admin';
                        $notification->translateOrNew('ar')->title = 'رسالة من مدير النظام';
                        $notification->translateOrNew('en')->body = $request->message;
                        $notification->translateOrNew('ar')->body = $request->message;
                        $notification->save();

                        send_to_user([$user->firebase_token],$request->message ,'10',"",$user->platform);
                        AdminNotification::create([
                            'image' => $request->image,
                            'message' => $request->message,
                            'type' => 0  // All Users
                        ]);




                    }});

//            $users = User::where('type',0)->get();
//            foreach ($users as $user){
//            }
        }
        elseif ($user_type['0'] == 'All_shops'){
            $shops = Shop::get();
            foreach ($shops as $shop){
//                $from='lama@emails.mobile-app-company.com';
//                $jsonurl = 'http://emails.mobile-app-company.com/index-lamma.php?' . "email=" . $shop->email  . "&from=" . $from."&message=".$request->message;
//                $json = file_get_contents($jsonurl);

              //  Mail::to($shop->email)->send(new ShopMail(['message' => $request->message]));
                $notification = new Notification();
                $notification->type = -1;
                $notification->sender_type = 0;
                $notification->shop_id = $shop->id;
                $notification->save();
                $notification->translateOrNew('en')->title = 'Message From Admin';
                $notification->translateOrNew('ar')->title = 'رسالة من مدير النظام';
                $notification->translateOrNew('en')->body = $request->message;
                $notification->translateOrNew('ar')->body = $request->message;
                $notification->save();

            }
            AdminNotification::create([
                'image' => $request->image,

                'message' => $request->message,
                'type' => 1  // All Shops,
            ]);
            send_to_shop([$shop->firebase_token],$request->message ,1,$shop->id,null,$shop,$shop->platform);

            //'shop_id','order_id','status','type','sender_type'
        }
        elseif ($user_type['0'] == 'All_delegates'){
            $users_token = User::where('type',1)->pluck('firebase_token')->toArray();

            $users = User::where('type',1)->get();

            send_to_user($users_token,$request->message ,'10',"");
            foreach ($users as $user){
                $notification = new Notification();
                $notification->user_id = $user->id;
                $notification->img = $request->image;

                $notification->save();
                $notification->translateOrNew('en')->title = 'Message From Admin';
                $notification->translateOrNew('ar')->title = 'رسالة من مدير النظام';
                $notification->translateOrNew('en')->body = $request->message;
                $notification->translateOrNew('ar')->body = $request->message;
                $notification->save();
            }
            AdminNotification::create([
                'image' => $request->image,

                'message' => $request->message,
                'type' => 0  // All Users
            ]);
        }
        elseif ($user_type['0'] == 'All_shops'){
            $shops = Shop::get();
            foreach ($shops as $shop){
                $from='lama@emails.mobile-app-company.com';
                $jsonurl = 'http://emails.mobile-app-company.com/index-lamma.php?' . "email=" . $shop->email  . "&from=" . $from."&message=".$request->message;
                $json = file_get_contents($jsonurl);
                send_to_shop([$shop->firebase_token],$request->message ,1,$shop->id,null,$shop,$shop->platform);

              //  Mail::to($shop->email)->send(new ShopMail(['message' => $request->message]));
            }
            AdminNotification::create([
                'image' => $request->image,

                'message' => $request->message,
                'type' => 1  // All Shops
            ]);

        }
        else{
            if ($user_type['1'] == 'user'){
                $user = User::where('id',$user_type['0'])->first();
                send_to_user([$user->firebase_token],$request->message ,'10',"","","",$user->platform);
                $notification = new Notification();
                $notification->user_id = $user->id;
                $notification->img = $request->image;
                $notification->save();
                $notification->translateOrNew('en')->title = 'Message From Admin';
                $notification->translateOrNew('ar')->title = 'رسالة من مدير النظام';
                $notification->translateOrNew('en')->body = $request->message;
                $notification->translateOrNew('ar')->body = $request->message;
                $notification->save();
                AdminNotification::create([
                    'image' => $request->image,

                    'user_id' => $user->id,
                    'message' => $request->message,
                    'type' => 2  // User
                ]);
            }
            elseif ($user_type['1'] == 'shop'){
                $shop = Shop::where('id',$user_type['0'])->first();

//                $from='lama@emails.mobile-app-company.com';
//                $jsonurl = 'http://emails.mobile-app-company.com/index-lamma.php?' . "email=" . $shop->email  . "&from=" . $from."&message=".$request->message;
//                $json = file_get_contents($jsonurl);

                AdminNotification::create([
                    'image' => $request->image,

                    'shop_id' => $shop->id,
                    'message' => $request->message,
                    'type' => 3  // Shop
                ]);
                $notification = new Notification();
                $notification->type = -1;
                $notification->sender_type = 0;
                $notification->shop_id = $shop->id;
                $notification->save();
                $notification->translateOrNew('en')->title = 'Message From Admin';
                $notification->translateOrNew('ar')->title = 'رسالة من مدير النظام';
                $notification->translateOrNew('en')->body = $request->message;
                $notification->translateOrNew('ar')->body = $request->message;
                $notification->save();
//                dd($shop->firebase_token);
                send_to_shop([$shop->firebase_token],$request->message ,1,$shop->id,null,$shop,$shop->platform);

            }
        }
        return true;
    }

}
