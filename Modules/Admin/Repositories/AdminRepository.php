<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/24/2020
 * Time: 3:43 PM
 */

namespace Modules\Admin\Repositories;


use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Entities\Admin;
use Modules\Admin\Repositories\Interfaces\AdminRepositoryInterface;
use Modules\Order\Entities\Order;
use Modules\Shop\Entities\Shop;
use Spatie\Permission\Models\Permission;

class AdminRepository implements AdminRepositoryInterface
{
    public function index()
    {
        $data['title'] = "المشرفين";
        $auth = Auth::guard('admin')->user()->id;
        $data['admins'] = Admin::where('id','!=',1)->where('id','!=',$auth)->get();
        return $data;
    }

    public function create()
    {
        $data['title'] = "إضافة مشرف";
        $data['permissions'] = Permission::get();
        $data['all_permissions'] = $this->permissions($data['permissions']);
        $data['models'] = array_keys($data['all_permissions']);
        return $data;
    }

    public function store($request)
    {
        $admin = Admin::create($request->all());
        if ($request->has('permissions')){
            $admin->givePermissionTo($request->permissions);
        }
        return $admin->save();

    }

    public function edit($request)
    {
        $data['title'] = "تعديل مشرف";
        $data['admin'] = Admin::find($request->admin_id);
        $data['permissions'] = Permission::get();
        $data['all_permissions'] = $this->permissions($data['permissions']);
        return $data;
    }

    public function update($request)
    {
        $admin = Admin::where('id',$request->admin_id)->first();
        $admin->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);
        if ($request->has('password') && $request->password != null){
            $admin->password = $request->password;
        }
        if ($request->has('status') && $request->status != null){
            $admin->status = $request->status;
        }else{
            $admin->status = 0;
        }
        $permissions = Permission::pluck('name')->toArray();
        $admin->revokePermissionTo($permissions);
        if ($request->has('permissions')){
            $admin->givePermissionTo($request->permissions);
        }
        if ($admin->save()){
            if ($request->has('image')){
                if (!empty($admin->getOriginal('image'))){
                    unlinkFile($admin->getOriginal('image'), 'admins');
                }
                $admin->image = $request->image;
            }
            $admin->save();
            return true;
        }
        return false;
    }

    public function show($request)
    {
        $admin_id = $request->admin_id;
        $admin = Admin::where('id', $admin_id)->first();
        $admin = $admin->load('image');
        $title = "تفاصيل المشرف";

        return view('admin::admin.pages.admins.show',compact('admin','title'));
    }

    public function permissions($permissions)
    {

        $all_permissions = [
            'المشرفين' => [$permissions[0],$permissions[1], $permissions[2],$permissions[3]],
            'المستخدمين' => [$permissions[4],$permissions[5], $permissions[6],$permissions[7]],
            'مندوبي التوصيل' => [$permissions[8],$permissions[9], $permissions[10],$permissions[11]],
            'الأقسام' => [$permissions[12],$permissions[13], $permissions[14],$permissions[15]],
            'المدن' => [$permissions[16],$permissions[17], $permissions[18],$permissions[19]],
            'السلايدر' => [$permissions[20],$permissions[21], $permissions[22],$permissions[23]],
            'العروض' => [$permissions[24],$permissions[25], $permissions[26],$permissions[27]],
            'الصفحات التعريفية' => [$permissions[28],$permissions[29], $permissions[30],$permissions[31]],
            'وسائل التواصل الإجتماعي' => [$permissions[32],$permissions[33], $permissions[34],$permissions[35]],
            'المتاجر' => [$permissions[36],$permissions[37],$permissions[38], $permissions[39],$permissions[40]],
            'الأقسام الفرعية' => [$permissions[41],$permissions[42], $permissions[43],$permissions[44]],
            'المنتجات' => [$permissions[45], $permissions[48]],
            'الطلبات' => [$permissions[50],$permissions[51],$permissions[52]],
            'الإشعارات' => [$permissions[53],$permissions[54]
                ,$permissions[55],$permissions[56]],
            'الإحصائيات' => [$permissions[57]],
            'الإعدادات' => [$permissions[58]],
//            'التقارير' => [$permissions[55],$permissions[56]]
        ];


        return $all_permissions;
    }


    public function statistics()
    {
        $users = User::count();
        $shops = Shop::count();
        $orders = Order::count();

        $userstatistics=array('7dayago'=>0,'6dayago'=>0,'5dayago'=>0,'4dayago'=>0,'3dayago'=>0,'2dayago'=>0,'1dayago'=>0,'today'=>0);
        $allusers=User::where( 'id' ,'>' ,0)->where('type',0)->select('created_at')->get();
        foreach ($allusers  as $oneuser){
            if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(7) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(8)){
                $userstatistics['7dayago'] ++ ;
            }if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(6) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(7)){
                $userstatistics['6dayago'] ++ ;
            }if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(5 ) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(6) ){
                $userstatistics['5dayago'] ++ ;
            } if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(4) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(5) ){
                $userstatistics['4dayago'] ++ ;
            } if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(3) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(4) ){
                $userstatistics['3dayago'] ++ ;
            } if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(2) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(3) ){
                $userstatistics['2dayago'] ++ ;
            }if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(1) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(2) ){
                $userstatistics['1dayago'] ++ ;
            } if ($oneuser->getOriginal('created_at') < Carbon::now() && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(1) ){
                $userstatistics['today'] ++ ;
            }
        }

        $delegatestatistics=array('7dayago'=>0,'6dayago'=>0,'5dayago'=>0,'4dayago'=>0,'3dayago'=>0,'2dayago'=>0,'1dayago'=>0,'today'=>0);
        $alldelegates=User::where( 'id' ,'>' ,0)->where('type',1)->select('created_at')->get();
        foreach ($alldelegates  as $oneuser){
            if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(7) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(8)){
                $delegatestatistics['7dayago'] ++ ;
            }if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(6) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(7)){
                $delegatestatistics['6dayago'] ++ ;
            }if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(5 ) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(6) ){
                $delegatestatistics['5dayago'] ++ ;
            } if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(4) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(5) ){
                $delegatestatistics['4dayago'] ++ ;
            } if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(3) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(4) ){
                $delegatestatistics['3dayago'] ++ ;
            } if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(2) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(3) ){
                $delegatestatistics['2dayago'] ++ ;
            }if ($oneuser->getOriginal('created_at') < Carbon::now()->subDays(1) && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(2) ){
                $delegatestatistics['1dayago'] ++ ;
            } if ($oneuser->getOriginal('created_at') < Carbon::now() && $oneuser->getOriginal('created_at') > Carbon::now()->subDays(1) ){
                $delegatestatistics['today'] ++ ;
            }
        }

        $ordersstatistics=array('7dayago'=>0,'6dayago'=>0,'5dayago'=>0,'4dayago'=>0,'3dayago'=>0,'2dayago'=>0,'1dayago'=>0,'today'=>0);
        $allorders = Order::where( 'id' ,'>' ,0)->select('created_at')->get();
        foreach ($allorders  as $order){
            if ($order->getOriginal('created_at') < Carbon::now()->subDays(7) && $order->getOriginal('created_at') > Carbon::now()->subDays(8)){
                $ordersstatistics['7dayago'] ++ ;
            }if ($order->getOriginal('created_at') < Carbon::now()->subDays(6) && $order->getOriginal('created_at') > Carbon::now()->subDays(7)){
                $ordersstatistics['6dayago'] ++ ;
            }if ($order->getOriginal('created_at') < Carbon::now()->subDays(5 ) && $order->getOriginal('created_at') > Carbon::now()->subDays(6) ){
                $ordersstatistics['5dayago'] ++ ;
            } if ($order->getOriginal('created_at') < Carbon::now()->subDays(4) && $order->getOriginal('created_at') > Carbon::now()->subDays(5) ){
                $ordersstatistics['4dayago'] ++ ;
            } if ($order->getOriginal('created_at') < Carbon::now()->subDays(3) && $order->getOriginal('created_at') > Carbon::now()->subDays(4) ){
                $ordersstatistics['3dayago'] ++ ;
            } if ($order->getOriginal('created_at') < Carbon::now()->subDays(2) && $order->getOriginal('created_at') > Carbon::now()->subDays(3) ){
                $ordersstatistics['2dayago'] ++ ;
            }if ($order->getOriginal('created_at') < Carbon::now()->subDays(1) && $order->getOriginal('created_at') > Carbon::now()->subDays(2) ){
                $ordersstatistics['1dayago'] ++ ;
            } if ($order->getOriginal('created_at') < Carbon::now() && $order->getOriginal('created_at') > Carbon::now()->subDays(1) ){
                $ordersstatistics['today'] ++ ;
            }
        }

        $latest_orders = Order::orderBy('id', 'desc')->take(10)->get();
        foreach ($latest_orders as $order){
            if (empty($order->getOriginal('image'))){
                $order->default_image = '/assets/images/default.png';
            }
        }
//
        $data['title'] = "الرئيسية";
        $data['users'] = $users;
        $data['shops'] = $shops;
        $data['orders'] = $orders;
        $data['latest_orders'] = $latest_orders;
        $data['user_statistics'] = $userstatistics;
        $data['delegate_statistics'] = $delegatestatistics;
        $data['order_statistics'] = $ordersstatistics;
        return $data;
    }
}
