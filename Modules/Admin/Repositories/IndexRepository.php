<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/19/2020
 * Time: 3:08 PM
 */

namespace Modules\Admin\Repositories;


use Modules\Admin\Repositories\Interfaces\IndexRepositoryInterface;
use Modules\Product\Entities\Product;

class IndexRepository implements IndexRepositoryInterface
{
    public function delete($model, $id): ?bool
    {
        $class = "Modules\\".ucfirst($model)."\\Entities\\" . ucfirst($model);
        if ($model == "user" || $model == "city" || $model == "slider" || $model == "contact"
            || $model == "contact" || $model == "tutorial" || $model == "socialMedia") {
            $class = "\\App\\" . ucfirst($model);
        }
        if ($model == 'subCategory'){
            $class = "Modules\\Shop\\Entities\\" . ucfirst($model);
        }
        if ($model == 'adminNotification'){
            $class = "Modules\\Admin\\Entities\\" . ucfirst($model);
        }
        if ($model == 'offer'){
            $class = "Modules\\Product\\Entities\\" . ucfirst($model);
        }
        $object = $class::find($id);
        $path = $class::getTableName();
//        dd($model);

        if ($model == "product" || $model == "shop") {
            if ($model == "shop"){
                unlinkFile($object->logo, $path);
            }
            if (!empty($object->images)){
                foreach ($object->images as $image){
                    unlinkFile($image->getOriginal('image'), $path);
                    $image->delete();
                }
            }
        }
        else{
            if (!empty($object->image)) {
                unlinkFile($object->image, $path);
            }
        }
        $deleted = $object->delete();
        return $deleted;
    }
    public function destroyMulti($model, $request)
    {
        foreach ($request->ids as $id) {
            $delete =$this->delete($model, $id);
            if (!$delete){
                return false;
            }
        }
        return true;
    }
    public function changeStatus($request)
    {
        $product = Product::where('id',$request->product_id)->first();
        $product->status = $request->status;
        if ($product->save()){
            if ($product->status == 1){
                return 'product_activated';
            }
            return 'product_deactivated';
        }else{
            return 'error';
        }
    }

}
