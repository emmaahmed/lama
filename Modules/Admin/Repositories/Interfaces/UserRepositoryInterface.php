<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/19/2020
 * Time: 1:10 PM
 */

namespace Modules\Admin\Repositories\Interfaces;

interface UserRepositoryInterface
{
    public function index($request);
    public function indexNotActive($request);

    public function changeStatus($request);

    public function store($request);

    public function edit($request);

    public function update($request);

}
