<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/19/2020
 * Time: 3:11 PM
 */

namespace Modules\Admin\Repositories\Interfaces;

interface IndexRepositoryInterface
{
    public function delete($model, $id);

    public function destroyMulti($model, $ids);
}
