<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/24/2020
 * Time: 3:44 PM
 */

namespace Modules\Admin\Repositories\Interfaces;

interface AdminRepositoryInterface
{
    public function index();

    public function create();

    public function store($request);

    public function edit($request);

    public function update($request);

    public function show($request);

    public function statistics();
}
