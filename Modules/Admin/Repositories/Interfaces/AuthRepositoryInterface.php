<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/25/2020
 * Time: 8:07 AM
 */

namespace Modules\Admin\Repositories\Interfaces;

interface AuthRepositoryInterface
{
    public function doLogin($request);

    public function doForgetPassword($request);

    public function resetPassword($token);

    public function doResetPassword($request);
}
