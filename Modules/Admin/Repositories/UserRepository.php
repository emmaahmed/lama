<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/19/2020
 * Time: 1:08 PM
 */

namespace Modules\Admin\Repositories;


use App\Repositories\Interfaces\AuthRepositoryInterface;
use App\Repositories\Interfaces\HomeRepositoryInterface;
use Carbon\Carbon;
use Modules\Admin\Repositories\Interfaces\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    protected $homeRepository, $authRepository, $indexRepository;

    public function __construct(HomeRepositoryInterface $homeRepository, AuthRepositoryInterface $authRepository, IndexRepository $indexRepository)
    {
        $this->homeRepository = $homeRepository;
        $this->authRepository = $authRepository;
        $this->indexRepository = $indexRepository;
    }

    // for user and delegate

    public function index($request)
    {
        $data['title'] = $request->type == 0 ? "المستخدمين" : "مندوبي التوصيل";
        $persons = $this->homeRepository->users()->where('type',$request->type)->with('user_orders')->get();
        if($request->type == 0){
            $data['users'] = $persons;
        }else{
            $data['delegates'] = $persons;
        }
        //dd($data['users'][0]->order);
        return $data;
    }
    public function indexNotActive($request)
    {
        $data['title'] = $request->type == 0 ? "المستخدمين" : "مندوبي التوصيل";
        $persons = $this->homeRepository->users()->where('type',$request->type)->where('expire_at','<',Carbon::now())->with('user_orders')->get();
        if($request->type == 0){
            $data['users'] = $persons;
        }else{
            $data['delegates'] = $persons;
        }
        //dd($data['users'][0]->order);
        return $data;
    }

    // for user and delegate

    public function changeStatus($request)
    {
        $user = $this->homeRepository->users()->where('id',$request->user_id)->first();
        $user->status = $request->status;
        if($user->password_wrong_times==3){
            $user->password_wrong_times = 0;

        }
        $user->status = $request->status;
        if ($user->save()){
            if ($user->status == 1){
                return 'user_activated';
            }
            else{
               sendBlocked($user->firebase_token,'تم عدم تفعيل المستخدم من الأدمن',40);

            }
            return 'user_deactivated';
        }else{
            return 'error';
        }
    }

    // for delegate only

    public function store($request)
    {
            $created = $this->authRepository->Register($request);
        if ($created){
            return true;
        }
        return false;
    }

    // for user and delegate

    public function edit($request)
    {
        $data['title'] = "تعديل المستخدم";
        $data['title'] = $request->type == 0 ? "تعديل المستخدم" : "تعديل المندوب";
        $person = $this->homeRepository->users()->where('id',$request->user_id)->first();
        $request->type == 0 ? $data['user'] = $person : $data['delegate'] = $person;
        return $data;
    }

    // for user and delegate

    public function update($request)
    {
        $user = $this->homeRepository->users()->where('id',$request->user_id)->first();
        if (isset($user)){
            $updated = $this->authRepository->updateProfile($user,$request);
            if ($request->has('status')){
                $user->status = 1;
            }else{
                $user->status = 0;
            }
            if ($user->save()){
                return true;
            }
        }
        return false;
    }

}
