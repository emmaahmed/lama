<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/25/2020
 * Time: 8:00 AM
 */

namespace Modules\Admin\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Mail\AdminResetPassword;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Modules\Admin\Entities\Admin;
use Modules\Admin\Http\Requests\AuthRequest;
use Modules\Admin\Repositories\Interfaces\AuthRepositoryInterface;

class AuthRepository implements AuthRepositoryInterface
{
    public function doLogin($request)
    {
        $remember_me = $request->remember_me == 1 ? true : false;
        if (admin()->attempt(['email'=>$request->email,'password'=>$request->password,'status' => 1],$remember_me)){
            return true;
        }else{
            return false;
        }
    }

    public function doForgetPassword($request)
    {
        $token = Str::random(64);
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
        Mail::to($request->email)->send(new AdminResetPassword(['token' => $token]));
        return true;
    }

    public function resetPassword ($token)
    {
        $check_token = DB::table('password_resets')
            ->where('token',$token)
            ->where('created_at' , '>' , Carbon::now()->subHours(2))->first();
        if (!empty($check_token)){
            return $check_token;
        }else{
            false;
        }
    }
    public function doResetPassword ($request)
    {
        $check_token = DB::table('password_resets')
            ->where('token',$request->token)
            ->where('created_at' , '>' , Carbon::now()->subHours(2))->first();
        if (!empty($check_token)){
            Admin::where('email',$check_token->email)->update([
                'email' => $check_token->email,
                'password' => Hash::make($request->password)
            ]);
            DB::table('password_resets')->where('email',$request->email)->delete();
            admin()->attempt(['email'=>$check_token->email,'password'=>$request->password,'status' => 1],true);
            return true;
        }else{
            return false;
        }
    }
}
