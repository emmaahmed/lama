<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class VariationOptionTranslation extends Model
{
    protected $fillable = ['variation_option_id','name'];

    protected $hidden = ['translations','created_at','updated_at'];

    public function VariationOption()
    {
        $this->belongsTo(VariationOption::class,'variation_option_id');
    }
}
