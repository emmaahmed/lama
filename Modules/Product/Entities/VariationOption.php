<?php

namespace Modules\Product\Entities;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class VariationOption extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = ['product_variation_id','price','color','status'];

    protected $hidden = [
        'created_at','updated_at','product_variation_id','translations','variation_id'
    ];

    public function ProductVariation()
    {
        return $this->belongsTo(ProductVariation::class,'product_variation_id');
    }

    public function getStatusAttribute($status)
    {
        return $status ? (int)$status : 0;
    }
}
