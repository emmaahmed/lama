<?php

namespace Modules\Product\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class FavouriteProduct extends Model
{
    protected $fillable = ['user_id','product_id'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
}
