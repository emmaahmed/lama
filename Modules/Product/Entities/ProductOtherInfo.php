<?php

namespace Modules\Product\Entities;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ProductOtherInfo extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['text1','text2','text3'];

    protected $hidden = ['translations','created_at','updated_at'];

    protected $fillable = ['id','product_id'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
