<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Shop\Entities\Shop;

class Offer extends Model
{

    protected $fillable = ['shop_id','product_id','image','expire_at'];

    protected $hidden = ['created_at','updated_at'];

    public function getImageAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/offers').'/'.$image;
        }
        return "";
    }

    public function setImageAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'offers');
            $this->attributes['image'] = $imageFields;
        }
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }


    public static function getTableName()
    {
        return (new self())->getTable();
    }
    public function shop(){
        return $this->belongsTo(Shop::class,'shop_id');

    }
    public static function getShop($shop_id)
    {
        return Shop::whereId($shop_id)->first();
    }
}
