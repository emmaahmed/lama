<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class VariationTranslation extends Model
{
    protected $fillable = ['variation_id','name'];

    protected $hidden = ['translations','created_at','updated_at'];

    public function variation()
    {
        return $this->belongsTo(Variation::class,'variation_id');
    }
}
