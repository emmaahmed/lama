<?php

namespace Modules\Product\Entities;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = ['user_id','user_name','product_id','rate','comment'];

    protected $hidden = ['updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getUserNameAttribute($user_name)
    {
        return $user_name ?: "";
    }
    public function getCommentAttribute($comment)
    {
        return $comment ?: "";
    }  public function getRateAttribute($rate)
    {
        return (double)$rate;
    }
    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->toDateString();
    }
}
