<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model
{
    protected $fillable = ['product_id','variation_id'];

    public function variationOptions()
    {
        $this->hasMany(VariationOption::class,'product_variation_id');
    }
}
