<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductInfoTranslation extends Model
{
    protected $fillable = ['product_info_id','text1','text2','text3'];

    protected $hidden = ['translations','created_at','updated_at'];

    public function product_info()
    {
        return $this->belongsTo(ProductInfo::class,'product_info_id');
    }
}
