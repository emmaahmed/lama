<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = ['product_id','image'];

    protected $hidden = ['created_at','updated_at'];

    public function getImageAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/products').'/'.$image;
        }
        return "";
    }

    public function setImageAttribute($image)
    {
        if (is_file($image)) {
            $imageFields = upload($image, 'products');
            $this->attributes['image'] = $imageFields;
        }
        else{
            $this->attributes['image'] = $image;

        }
    }
}
