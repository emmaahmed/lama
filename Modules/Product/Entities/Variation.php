<?php

namespace Modules\Product\Entities;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Variation extends Model implements TranslatableContract
{
    use Translatable;

    protected $fillable = ['type','required','is_size'];

    public $translatedAttributes = ['name'];

    protected $hidden = ['translations','created_at','updated_at','pivot'];



    public function options()
    {
        return $this->hasManyThrough(VariationOption::class, ProductVariation::class);
    }

    public function getTypeAttribute($type)
    {
        return $type ? (int)$type : 0;
    }
    public function getRequiredAttribute($required)
    {
        return $required ? (int)$required : 0;
    }

}
