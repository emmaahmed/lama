<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    protected $fillable = ['product_id','short_description','long_description'];

    protected $hidden = ['translations','created_at','updated_at'];

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
}
