<?php

namespace Modules\Product\Entities;

use App\User;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderItem;
use Modules\Shop\Entities\Shop;
use Modules\Shop\Entities\SubCategory;
use Modules\Product\Entities\Offer;

class Product extends Model implements TranslatableContract
{

    use Translatable;

    protected $appends = ['image','is_favourite'];

    public $translatedAttributes = ['name','short_description','long_description'];

    protected $hidden = ['translations','updated_at'];

    protected $fillable = ['shop_id','sub_category_id','price_before','price_after'
        ,'percent','quantity','selling','status','has_discount','rate'];

    public function shop()
    {
        return $this->belongsTo(Shop::class,'shop_id')
            ->select('id','logo','email','phone','online','rate','from','to','min_order','payment','fast_delivery','expire_at');
    }
    public function shopExpire()
    {
        return $this->belongsTo(Shop::class,'shop_id')
            ->select('id','logo','email','phone','online','rate','from','to','min_order','fast_delivery','expire_at')->where('expire_at','>',Carbon::now());
    }
    public function shopStatus()
    {
        return $this->belongsTo(Shop::class,'shop_id')->where('status','1')->where('online','1');
    }

    public function subCategory()
    {
        return $this->belongsTo(SubCategory::class,'sub_category_id');
    }

    public function variations()
    {
        return $this->belongsToMany(Variation::class, 'product_variations',
            'product_id', 'variation_id');
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }
    public function other_info()
    {
        return $this->hasMany(ProductInfo::class);
    }
    public function reviews()
    {
        return $this->hasMany(Rate::class);
    }

    public function getImageAttribute()
    {
        return $this->images()->first()?$this->images()->first()['image']:'';
    }

    public function getIsFavouriteAttribute()
    {
        $user = checkJWT();
        if ($user){
            $favourite = FavouriteProduct::where('product_id', $this->id)->where('user_id', $user->id)->first();
            if ($favourite){
                return 1;
            }
            else {
                return 0;
            }
        }
        return 0;
    }
    public function offer()
    {
        return $this->hasOne(Offer::class,'product_id');
    }

    public function getRateAttribute()
    {
        $sum = 0;
        $rates = Rate::where('product_id', $this->id)->get();
        if (sizeof($rates) > 0) {
            foreach ($rates as $rate) {
                $sum += $rate->rate;
            }
            return $sum/sizeof($rates);
        }else{
            return 0;
        }
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->toDateString();
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->using(FavouriteProduct::class);
    }

    public function favourite_products()
    {
        return $this->hasMany(Product::class)->using(FavouriteProduct::class);
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }


}
