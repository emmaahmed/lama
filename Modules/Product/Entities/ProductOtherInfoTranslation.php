<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductOtherInfoTranslation extends Model
{
    protected $fillable = ['prod_info_id','text1','text2','text3'];

    protected $hidden = ['translations','created_at','updated_at'];

    public function product_info()
    {
        return $this->belongsTo(ProductOtherInfo::class,'prod_info_id');
    }
}
