<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariationOptionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variation_option_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('variation_option_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('locale')->index();
            $table->unique(['variation_option_id','locale']);
            $table->timestamps();
            $table->foreign('variation_option_id')->references('id')->on('variation_options')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variation_option_translations');
    }
}
