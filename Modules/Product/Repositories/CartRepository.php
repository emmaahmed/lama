<?php


namespace Modules\Product\Repositories;


use Modules\Cart\Entities\Cart;
use Modules\Cart\Entities\CartItem;
use Modules\Cart\Entities\CartItemVar;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Repositories\Interfaces\CartRepositoryInterface;

class CartRepository implements CartRepositoryInterface
{
    public function addToCart($request, $user_id)
    {
        $variations = $request->variations;
        $cart = Cart::where('user_id',$user_id)->first();
        $new_item = Product::where(['id' => $request->product_id])->first();
        if (isset($cart)) {
            if ($cart->shop_id != $new_item->shop_id) {
                return 'product_not_in_cart';
            }
            $cart_items_ids = $cart->cartItems()->pluck('product_id')->toArray();
            if (in_array($new_item->id, $cart_items_ids)){
                $cart_items = CartItem::where('cart_id', $cart->id)->where('product_id', $new_item->id)->get();
                // Variations
                $flag = 0;
                foreach ($cart_items as $cart_item){
                    $cart_items_vars = CartItemVar::where('cart_item_id',$cart_item->id)->get();
                    if (sizeof($variations) != sizeof($cart_items_vars->groupBy('variation_id'))){
                        $flag = 1;
                    }else{
                        $flag = 0;
                    }
                    foreach ($variations as $variation){
                        $item_vars = CartItemVar::where('cart_item_id',$cart_item->id)
                            ->where('variation_id', $variation['id'])->get();
                        if (sizeof($item_vars) > 0){
                            if (sizeof($variation['options']) != sizeof($item_vars)){
                                $flag = 1;
                            }
                            foreach ($variation['options'] as $option){
                                $item_var_option = $item_vars->where('option_id', $option['id'])->first();
                                if (!$item_var_option){
                                    $flag = 1;
                                }
                            }
                        }else{
                            $flag = 1;
                        }
                    }
                }
//                dd($flag);
                if ($flag == 0){ // update item
                    $cart_item->quantity += 1;
                    $cart_item->save();
                }else{ // insert new item
                    $cart_item = CartItem::create([
                        'cart_id' => $cart->id,
                        'product_id' => $new_item->id,
                        'quantity' => 1,
                        'description' => $request->description,
                    ]);
                    foreach ($variations as $variation){
                        foreach ($variation['options'] as $option){
                            CartItemVar::create([
                                'cart_item_id' => $cart_item->id,
                                'variation_id' => $variation['id'],
                                'option_id' => $option['id'],
                            ]);
                        }
                    }
                }
            }else{
                $cart_item = CartItem::create([
                    'cart_id' => $cart->id,
                    'product_id' => $new_item->id,
                    'quantity' => 1,
                    'description' => $request->description,
                ]);
                foreach ($variations as $variation){
                    foreach ($variation['options'] as $option){
                        CartItemVar::create([
                            'cart_item_id' => $cart_item->id,
                            'variation_id' => $variation['id'],
                            'option_id' => $option['id'],
                        ]);
                    }
                }
            }
        }else{
            $cart = Cart::create([
                'user_id' => $user_id,
                'shop_id' => $new_item->shop_id,
            ]);
            $cart_item = CartItem::create([
                'cart_id' => $cart->id,
                'product_id' => $new_item->id,
                'quantity' => 1,
                'description' => $request->description,
            ]);
            foreach ($variations as $variation){
                foreach ($variation['options'] as $option){
                    CartItemVar::create([
                        'cart_item_id' => $cart_item->id,
                        'variation_id' => $variation['id'],
                        'option_id' => $option['id'],
                    ]);
                }
            }
        }
        return $cart;
    }

    public function cart($user_id)
    {
        $cart = Cart::where('user_id', $user_id)->first();
        if (isset($cart->cartItems)){
            $cart_array = [];
            $total_cost = 0;
            foreach ($cart->cartItems as $key => $cart_item){
                $item_cost = ($cart_item->has_discount ? $cart_item->product->price_after : $cart_item->product->price_before) * $cart_item->quantity;
                $cart_array['items'][$key]['item_id'] = $cart_item->product_id;
                $cart_array['items'][$key]['item_name'] = $cart_item->product->name;
                $cart_array['items'][$key]['item_old_price'] = $cart_item->has_discount ? $cart_item->product->price_after : $cart_item->product->price_before;
                $cart_array['items'][$key]['item_quantity'] = $cart_item->quantity;
                $cart_array['items'][$key]['item_image'] = $cart_item->product->image;
                $cart_array['items'][$key]['variations'] = [];
                if (isset($cart_item->cartItemVars)) {
                    $options_cost = 0;
                    foreach ($cart_item->cartItemVars as $key2 => $variation){
                        $cart_array['items'][$key]['variations'][$variation->variation_id]['id'] = $variation->variation_id;
                        $cart_array['items'][$key]['variations'][$variation->variation_id]['name'] = $variation->variation->name;
                        $cart_array['items'][$key]['variations'][$variation->variation_id]['options'][$key2]['id'] = $variation->option_id;
                        $cart_array['items'][$key]['variations'][$variation->variation_id]['options'][$key2]['name'] = $variation->option->name;
                        $cart_array['items'][$key]['variations'][$variation->variation_id]['options'][$key2]['price'] = $variation->option->price;
                        $options_cost += $variation->option->price;
                    }
                    $cart_array['items'][$key]['variations'][$variation->variation_id]['options'] = array_values($cart_array['items'][$key]['variations'][$variation->variation_id]['options']);
                }
                $item_cost += ($options_cost * $cart_item->quantity);
                $cart_array['items'][$key]['item_price'] = $item_cost;
                $cart_array['items'][$key]['variations'] = array_values($cart_array['items'][$key]['variations']);
                $total_cost += $item_cost;
            }
            $cart_array['total_cost'] = $total_cost;
        }
        
        return ($cart_array);
    }

    public function removeFromCart($product_id, $user_id)
    {

    }

}