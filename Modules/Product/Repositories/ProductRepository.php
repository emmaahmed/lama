<?php


namespace Modules\Product\Repositories;


use App\Repositories\Interfaces\HomeRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Modules\Cart\Entities\Cart;
use Modules\Cart\Entities\CartItem;
use Modules\Product\Entities\FavouriteProduct;
use Modules\Product\Entities\Offer;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductImage;
use Modules\Product\Entities\ProductInfo;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\Rate;
use Modules\Product\Entities\VariationOption;
use Modules\Product\Http\Requests\ProductsRequest;
use Modules\Product\Repositories\Interfaces\ProductRepositoryInterface;
use Modules\Shop\Entities\Shop;
use Carbon\Carbon;

class ProductRepository implements ProductRepositoryInterface
{
    protected $homeRepository;

    public function __construct(HomeRepositoryInterface $homeRepository)
    {
        $this->homeRepository = $homeRepository;
    }

    public function getProducts($shop_id = null)
    {
        $data = $this->homeRepository->products()->with(['shop','reviews'])->select('id','shop_id','price_before','price_after','percent','has_discount','rate','created_at')->where('status',1)->orderBy('id','desc')->paginate(10);
        if ($shop_id){
            $data['products'] = $this->homeRepository->products()
                ->with('shop')->where('shop_id', $shop_id)->where('status',1)->orderBy('id','desc')->paginate(5);
           $products=$this->homeRepository->products()
               ->with('shop')->where('shop_id', $shop_id)->where('status',1)->orderBy('id','desc')->count();
            $data['title'] = 'منتجاتي'.' '.$products ;

        }
        return $data;
    }
    public function getNotActiveProducts($shop_id = null)
    {
        $data = $this->homeRepository->products()->with(['shop','reviews'])->select('id','shop_id','price_before','price_after','percent','has_discount','rate','created_at')->where('status',1)->orderBy('id','desc')->paginate(10);
        if ($shop_id){
            $data['title'] = 'منتجاتي';
            $data['products'] = $this->homeRepository->products()
                ->with('shop')->where('shop_id', $shop_id)->where('status',0)->orderBy('id','desc')->paginate(5);
        }
        return $data;
    }
    public function getProductsApi($shop_id = null)
    {
        $data = $this->homeRepository->products()->with('shop')->whereHas('shop',function($query){
                $query->where('expire_at','>',Carbon::now())->where('status','1')->where('online',1);
        }
            )->select('id','shop_id','price_before','price_after','percent','has_discount','rate','created_at')->where('status',1)->orderBy('id','desc')->paginate(10);
        if ($shop_id){
            $data['title'] = 'منتجاتي';
            $data['products'] = $this->homeRepository->products()
                ->with('shop')->whereHas('shop',function($query){
                $query->where('expire_at','>',Carbon::now())->where('status','1')->where('online',1);
        }
            )->where('shop_id', $shop_id)->orderBy('id','desc')->paginate(5);
        }
        return $data;
    }

    public function getOffers()
    {
        $offers = Offer::with('shop')->where('expire_at','>',Carbon::now())->whereHas('shop',function($query){
                $query->where('expire_at','>',Carbon::now())->where('status','1')->where('online',1);
        }
            )->orderBy('id','desc')->paginate(10);
        return $offers;
    }

    public function searchProducts($request)
    {
        $products = $this->homeRepository->products();

//        if ($request->has('price')){
//            $products->orderBy('price_before',$request->price);
////            Product::all()->sortByDESC('price_before');
//        }
//        if ($request->has('date')){
//            $products->orderBy('id',$request->date);
//        }
        $products->where('status',1)->whereTranslationLike('name', '%' . $request->key . '%');
//        ->orWhereTranslationLike('short_description', '%' . $request->key . '%')
//        ->orWhereTranslationLike('long_description', '%' . $request->key . '%');
        if($request->shop_id){
            $products->where('shop_id',$request->shop_id);
        }
        return $products
            ->select('id','shop_id','price_before','price_after','percent','has_discount','rate')
            ->with('shop')
            ->whereHas('shop',function($query){
                $query->where('expire_at','>',Carbon::now())->where('status','1')->where('online',1);
        })
//            ->whereHas('subCategory',function ($query){
//            $query->where('active',1);
//            })
            ->where('status',1)
            ->paginate(10);
    }

    public function getProductsByShop($shop_id,$user_id)
    {
        $data = $this->homeRepository->shops()->where('id',$shop_id)->with('images')->select('id','logo','from','to','rate','min_order','expire_at')->withCount(['followers as is_follow'=>function($query)use($user_id){
           $query->where('user_id',$user_id);

       }])->withCount('followers as followers')->first();
        $data['sub_categories'] = $this->homeRepository->subCategories()->where('shop_id',$shop_id)->select('id')->orderBy('id','desc')->Active()->get();
         $data['products'] = $this->homeRepository->products()->with('shop')->where('shop_id',$shop_id)->where('status',1)->select('id','shop_id','sub_category_id','price_before','price_after','percent','has_discount','rate','created_at')->orderBy('id','desc')->paginate(10);
        return $data;
    }

    public function getProductsBySubCategory($sub_category_id)
    {
        $data = $this->homeRepository->products()->where('status',1)->with('shop')->where('sub_category_id',$sub_category_id)->select('id','shop_id','sub_category_id','price_before','price_after','percent','has_discount','rate','created_at')->where('status',1)->orderBy('id','desc')->paginate(10);
        return $data;
    }

    public function productDetails($product_id,$user_id = null)
    {
        $data = $this->homeRepository->products()->with('shop')->where('id',$product_id)->select('id','shop_id','price_before','price_after','percent','has_discount','rate','created_at','quantity')->with('images')->with(['other_info' => function ($q){
            $q->take(2);
        }])->withCount('reviews')->first();
        $data->in_cart = false;
        if ($user_id != null){
            $cart = Cart::where('user_id',$user_id)->first();
            if($cart){
                $cart_items_ids = CartItem::where('cart_id', $cart->id)->pluck('product_id')->toArray();
                if (in_array($product_id,$cart_items_ids)){
                    $data->in_cart = true;
                }
            }
        }

//        Product Variations
        $product_variations = $data->variations;

         if (isset($product_variations)){
             foreach ($product_variations as $variation){
                 $variation->options = $variation->options()->where('product_id',$data->id)->where('status',1)->get();
             }
             $data['variations'] = $product_variations;
         }else{
             $data['variations'] = [];
         }

        return $data;
    }

    public function productOtherInfo($product_id)
    {
        $product = $this->homeRepository->products()->with('shop')->where('id',$product_id)->first();
        $data = $product->other_info;
        return $data;
    }

    public function getProductReviews($product_id)
    {
        $reviews = Rate::where('product_id',$product_id)->get();
        foreach ($reviews as $review){
            $review->user_name = $review->user_name ?: $review->user->name;
            $review->user_image = $review->user ? $review->user->image : "";
            unset($review->user);
        }
        return $reviews;
    }
    public function addReview($request, $user_id)
    {
        $review = Rate::create(array_merge($request->all(), ['user_id' => $user_id]));
        return $review;
    }

//    Favourite

    public function FavouriteAction($product_id, $user_id)
    {
        $favourite_product = $this->checkFavourite($product_id, $user_id);
        if ($favourite_product === null){
            FavouriteProduct::create([
                'user_id' => $user_id,
                'product_id' => $product_id,
            ]);
            return 'added';
        }
        FavouriteProduct::where('user_id', $user_id)->where('product_id', $product_id)->delete();
        return 'removed';
    }

    public function checkFavourite($product_id, $user_id)
    {
        $is_favourite = FavouriteProduct::where('user_id', $user_id)->where('product_id', $product_id)->first();
        return $is_favourite;
    }

    public function getFavouriteProducts($user_id)
    {
        $favourites = FavouriteProduct::where('user_id', $user_id)->pluck('product_id')->toArray();
        $favourite_products = Product::whereIn('id',$favourites)->where('status',1)
            ->whereHas('shopStatus')
            ->whereHas('shopExpire')
//            ->with(['shopStatus','shopExpire'])

            ->select('id','price_before','price_after','has_discount','rate','shop_id','created_at')->get();
//       dd($favourite_products);
        return $favourite_products;
    }


    // Admin

    public function create()
    {
        $shop = Auth::guard('shop')->user();

        $data['title'] = "إضافة منتج جديد";
        $data['subCategories'] = $this->homeRepository->subCategories()->where('shop_id',$shop->id)->orWhere('shop_id',$shop->parent_id)->orderBy('id','DESC')->get();
        return $data;
    }

    public function store($request)
    {
        //dd($request->variations[0]['options']);
        if(Auth::guard('shop')->user()->parent_id!=null){
            $id=Auth::guard('shop')->user()->parent_id;
        }
        else{
            $id=Auth::guard('shop')->user()->id;

        }
        $shop = Shop::where('id',$id)->first();
        if ($request->percent > 0 && $request->price_after){
            $request->request->add(['has_discount' => '1']);
        }
        else{
            $request['price_after']=$request->price_before;
        }
        $product = $shop->products()->create($request->except('name_ar','name_en','short_description_ar','short_description_en','long_description_ar','long_description_en'));
        if ($request->has('images')){
            foreach ($request->images as $image){
                $product->images()->create([
                    'image' => $image
                ]);
            }
        }
        $product->translateOrNew('ar')->name = $request->name_ar;
        $product->translateOrNew('en')->name = $request->name_ar;
//        $product->translateOrNew('en')->name = $request->name_en;
        $product->translateOrNew('ar')->short_description = $request->short_description_ar;
        $product->translateOrNew('en')->short_description = $request->short_description_ar;
//        $product->translateOrNew('en')->short_description = $request->short_description_en;
        $product->translateOrNew('ar')->long_description = $request->long_description_ar;
        $product->translateOrNew('en')->long_description = $request->long_description_ar;
//        $product->translateOrNew('en')->long_description = $request->long_description_en;
        if ($product->save()){

            ////////////////////

            if ($request->has('variations')){
                foreach ($request->variations as $var){
                    $variation = $product->variations()->create([
                        'type' => $var['type'],
                        'required' => isset($var['required']) ? 1 : 0,
                        'is_size' => $var['type']== 3?1:0,

                    ]);
                    $variation->translateOrNew('ar')->name = $var['ar'];
                    $variation->translateOrNew('en')->name = $var['ar'];
//                    $variation->translateOrNew('en')->name = $var['en'];
                    $variation->save();
                    if (isset($var['options'])){
                        // type=3 , then this is size of product
                        if($var['type'] == 3){
                            global $option_prices;
                            $option_prices = [];
                        }
                        //
                        foreach ($var['options'] as $option){
                            //
                            if($var['type'] == 3){
                                array_push($option_prices,$option['price']);
                            }
                            //
                            $var_product = ProductVariation::where(['product_id' => $product->id,'variation_id' => $variation->id])->first();
                            $var_option = VariationOption::create([
                                'product_variation_id' => $var_product->id,
                                'price' =>$option['price'],
                                'color' =>$var['type'] == 2 ? $option['color'] : '',
                                'status' => isset($option['status']) ? 1 : 0,
                            ]);
                            $var_option->translateOrNew('ar')->name = $option['ar'];
                            $var_option->translateOrNew('en')->name = $option['ar'];
//                            $var_option->translateOrNew('en')->name = $option['en'];
                            $var_option->save();
                        }
                        // type=3 , then this is size of product
                        if($var['type'] == 3){
                            //dd(min ( $option_prices ));
                            Product::whereId($product->id)->update([
                                'price_before' => $request->price_before ,
                                'price_after'  => $request->price_after            ,
                                'percent'      => 0
                                ]);
                        }
                        //
                    }
                }
            }
            if ($request->has('info')) {
                foreach ($request->info as $info){
                    $product_info = $product->other_info()->create();
                    $product_info->translateOrNew('ar')->text1 = $info['text1'];
                    $product_info->translateOrNew('en')->text1 = $info['text1'];
                    $product_info->translateOrNew('ar')->text2 = $info['text2'];
                    $product_info->translateOrNew('en')->text2 = $info['text2'];
                    $product_info->translateOrNew('ar')->text3 = $info['text3'];
                    $product_info->translateOrNew('en')->text3 = $info['text3'];
                    $product_info->save();
                }
            }
//            $users_ids = $shop->follows()->pluck('user_id');
//            $users = User::whereIn('id',$users_ids)->get();
//            foreach ($users as $user){
//                send_to_user([$user->firebase_token],'تم إضافة منتج جديد' ,'0',$shop->id);
//                Notification::create([
//                    'user_id' => $user->id,
//                    'title' => 'رسالة جديدة',
//                    'body' => 'تم إضافة منتج جديد',
//                ]);
//            }
            /////////////////////
            return true;
        }
        return false;
    }

    public function changeStatus($request)
    {
        $product = $this->homeRepository->products()->where('id',$request->product_id)->first();
        $product->status = $request->status;
        if ($product->save()){
            if ($product->status == 1){
                return 'product_activated';
            }
            return 'product_deactivated';
        }else{
             return 'error';
        }
    }

    public function edit($request)
    {
        $shop = Auth::guard('shop')->user();
        $data['title'] = "تعديل المنتج";
        $data['product'] = $this->homeRepository->products()->where('id',$request->product_id)->first();
        $data['subCategories'] = $this->homeRepository->subCategories()->where('shop_id',$shop->id)->orWhere('shop_id',$shop->parent_id)->orderBy('id','DESC')->get();
        return $data;
//        dd($data);
    }

    public function update($request)
    {
        $product = Product::where('id',$request->product_id)->first();
        if (isset($product)){
            $product->update($request->except('images'));
            if ($request->has('images')){
                foreach ($request->images as $image){
                    $product->images()->create([
                        'image' => $image
                    ]);
                }
            }
            if ($request->percent > 0 && $request->price_after){

                $product->update(['has_discount' => '1']);
            }

            $product->translateOrNew('ar')->name = $request->name_ar;
            $product->translateOrNew('en')->name = $request->name_ar;
//        $product->translateOrNew('en')->name = $request->name_en;
            $product->translateOrNew('ar')->short_description = $request->short_description_ar;
            $product->translateOrNew('en')->short_description = $request->short_description_ar;
//        $product->translateOrNew('en')->short_description = $request->short_description_en;
            $product->translateOrNew('ar')->long_description = $request->long_description_ar;
            $product->translateOrNew('en')->long_description = $request->long_description_ar;
//        $product->translateOrNew('en')->long_description = $request->long_description_en;
            if ($product->save()){

                ProductVariation::where('product_id', $product->id)->delete();
                if ($request->has('variations')){
                    foreach ($request->variations as $var){
                        $variation = $product->variations()->create([
                            'type' => $var['type'],
                            'required' => isset($var['required']) ? 1 : 0,
                            'is_size' => $var['type']== 3?1:0,

                        ]);
                        $variation->translateOrNew('ar')->name = $var['ar'];
                        $variation->translateOrNew('en')->name = $var['ar'];
//                    $variation->translateOrNew('en')->name = $var['en'];
                        $variation->save();
                        if (isset($var['options'])){
                        // type=3 , then this is size of product
                        if($var['type'] == 3){
                            global $option_prices;
                            $option_prices = [];
                        }
                        //
                            foreach ($var['options'] as $option){
                            //
                            if($var['type'] == 3){
                                array_push($option_prices,$option['price']);
                            }
                            //
                                $var_product = ProductVariation::where(['product_id' => $product->id,'variation_id' => $variation->id])->first();
                                $var_option = VariationOption::create([
                                    'product_variation_id' => $var_product->id,
                                    'price' =>$option['price'],
                                    'status' => isset($option['status']) ? 1 : 0,
                                    'color' => isset($option['color']) ? $option['color'] : "",
                                ]);
                                $var_option->translateOrNew('ar')->name = $option['ar'];
                                $var_option->translateOrNew('en')->name = $option['ar'];
//                            $var_option->translateOrNew('en')->name = $option['en'];
                                $var_option->save();
                            }
                        }
                    }
                    // type=3 , then this is size of product
//                    dd($var['type']);

                    if($var['type'] == 3){
                            //dd(min ( $option_prices ));
//                            Product::whereId($product->id)->update([
//                                'price_before' => min ( $option_prices ) ,
//                                'price_after'  => min ( $option_prices ) ,
//                                'percent'      => 0
//                                ]);
                        }
                        //
                }
                ProductInfo::where('product_id', $product->id)->delete();
                if ($request->has('info')) {
                    foreach ($request->info as $info){
                        $product_info = $product->other_info()->create();
                        $product_info->translateOrNew('ar')->text1 = $info['text1'];
                        $product_info->translateOrNew('en')->text1 = $info['text1'];
                        $product_info->translateOrNew('ar')->text2 = $info['text2'];
                        $product_info->translateOrNew('en')->text2 = $info['text2'];
                        $product_info->translateOrNew('ar')->text3 = $info['text3'];
                        $product_info->translateOrNew('en')->text3 = $info['text3'];
                        $product_info->save();
                    }
                }
                return true;
            }
        }
        return false;
    }

    public function productRemoveImages($request)
    {
        $product_image = ProductImage::where('id', $request->image_id)->where('product_id', $request->product_id)->first();
        if ($product_image){
            unlinkFile($product_image->getOriginal('image'),'products');
            if ($product_image->delete()){
                return true;
            }
            return false;
        }
        return false;
    }


}
