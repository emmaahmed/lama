<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/22/2020
 * Time: 4:11 PM
 */

namespace Modules\Product\Repositories\Interfaces;

interface OfferRepositoryInterface
{
    public function index();
    public function indexOffersShop($id);
    public function create();

    public function store($request);

    public function edit($request);

    public function update($request);
}
