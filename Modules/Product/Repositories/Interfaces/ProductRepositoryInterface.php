<?php

namespace Modules\Product\Repositories\Interfaces;

interface ProductRepositoryInterface
{
    public function getProducts($shop_id = null);
    public function getNotActiveProducts($shop_id = null);

    public function getOffers();

    public function searchProducts($request);

    public function getProductsByShop($shop_id,$user_id);

    public function getProductsBySubCategory($sub_category_id);

    public function productDetails($product_id,$user_id = null);

    public function productOtherInfo($product_id);

    public function addReview($request, $user_id);

    public function getProductReviews($product_id);

    public function checkFavourite($product_id, $user_id);

    public function FavouriteAction($product_id, $user_id);

    public function getFavouriteProducts($user_id);

//    Admin

    public function create();

    public function store($request);

    public function changeStatus($request);

    public function edit($request);

    public function update($request);

    public function productRemoveImages($request);
}
