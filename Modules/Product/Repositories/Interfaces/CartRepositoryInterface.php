<?php

namespace Modules\Product\Repositories\Interfaces;

interface CartRepositoryInterface
{
    public function addToCart($request, $user_id);

    public function cart($user_id);

    public function removeFromCart($item_id, $user_id);
}