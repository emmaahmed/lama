<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 3/22/2020
 * Time: 3:58 PM
 */

namespace Modules\Product\Repositories;


use App\Notification;
use App\Repositories\Interfaces\HomeRepositoryInterface;
use App\User;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Entities\AdminNotification;
use Modules\Product\Repositories\Interfaces\OfferRepositoryInterface;
use Modules\Shop\Entities\Follower;
use Modules\Shop\Entities\Shop;

class OfferRepository implements OfferRepositoryInterface
{
    protected $homeRepository;

    public function __construct(HomeRepositoryInterface $homeRepository)
    {
        $this->homeRepository = $homeRepository;
    }

    public function index()
    {
        $data['title'] = "العروض";
        $data['offers'] = $this->homeRepository->offers()->with('product')->paginate(5);
        return $data;
    }
    public function indexOffersShop($id)
    {
        $data['title'] = "العروض";
        $data['offers'] = $this->homeRepository->offers()->where('shop_id',$id)->with('product')->paginate(5);
        return $data;
    }

    public function create()
    {
        $data['title'] = "إضافة عرض";
        $data['products'] = $this->homeRepository->products()->get();
        return $data;
    }

    public function store($request)
    {
        $created = $this->homeRepository->offers()->create($request->all());
        $shop=Shop::whereId($request->shop_id)->first();
        unset($shop->images);
        $followers= Follower::where('shop_id',$request->shop_id)->pluck('user_id');
//        function send_to_user($tokens, $msg , $type, $shop_id, $order_id=""){
//            send($tokens, $msg , $type, $shop_id , $order_id);
//        }
        $users = User::whereIn('id',$followers)->select('id','firebase_token','platform')->get();
        foreach ($users as $user) {
            $msg = $shop->name . ' قام باضافة عرض جديد ';
            send_to_user($user->firebase_token, $msg, '', $request->shop_id, "","$shop",$user->platform);
            Notification::create([
                    'user_id' => $user->id,
                    'title' => $shop->name . ' قام باضافة عرض جديد ',
                    'body' => $shop->name . ' قام باضافة عرض جديد ',
                    'shop_id' => $request->shop_id
                ]);
            AdminNotification::create([
                'shop_id' => $shop->id,
                'message' => $request->message,
                'type' => 3,  // Shop
                'sender_type' => 1,  // shop
//                'shop_id' => $request->shop_id  // shop
            ]);
        }
        return $created;

    }

    public function edit($request)
    {
        $data['title'] = "تعديل العرض";
        $offer = $this->homeRepository->offers()->where('id',$request->offer_id)->first();
        $data['offer'] = $offer;
        $data['products'] = $this->homeRepository->products()->where('shop_id',$offer->shop_id)->get();
        return $data;
    }

    public function update($request)
    {
        $offer = $this->homeRepository->offers()->where('id',$request->offer_id)->first();
        if (isset($offer)){
            $offer->update($request->only(['product_id', 'image','expire_at']));
            return true;
        }
        return false;
    }
    public function searchOffers($request){
        $data['title'] = "العروض";

        $data['offers']=  $this->homeRepository->offers()->whereHas('product',function ($query)use ($request){
            $query->whereTranslationLike('name',"%". $request->keySearch ."%");
        })->with('product')->paginate(5);
        return $data;
    }

}
