<?php

namespace Modules\Product\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Repositories\Interfaces\ProductRepositoryInterface;

class OffersController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function offers()
    {
        $offers = $this->productRepository->getOffers();
        return callback_data(success(), 'offers', $offers);
    }
}
