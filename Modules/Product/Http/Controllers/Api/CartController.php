<?php

namespace Modules\Product\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Http\Requests\CartRequest;
use Modules\Product\Repositories\Interfaces\CartRepositoryInterface;
use Modules\Product\Repositories\Interfaces\ProductRepositoryInterface;

class CartController extends Controller
{
    protected $cartRepository;

    public function __construct(CartRepositoryInterface $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function addToCart(CartRequest $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $cart = $this->cartRepository->addToCart($request, $user->id);
            if ($cart){
                return callback_data(success(), 'item_added_to_cart');
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function cart()
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $cart = $this->cartRepository->cart($user->id);
            if ($cart){
                return callback_data(success(), 'cart', $cart);
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function removeFromCart(CartRequest $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $cart = $this->cartRepository->removeFromCart($request->product_id, $user->id);
            if ($cart){
                return callback_data(success(), 'item_removed_from_cart');
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }
}
