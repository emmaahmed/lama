<?php

namespace Modules\Product\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Http\Requests\ProductsRequest;
use Modules\Product\Repositories\Interfaces\ProductRepositoryInterface;

class ProductsController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function products()
    {
        $products = $this->productRepository->getProductsApi();
        return callback_data(success(), 'new_products', $products);
    }

    public function offers()
    {
        $offers = $this->productRepository->getOffers();
        return callback_data(success(), 'offers', $offers);
    }
    public function searchProducts(ProductsRequest $request)
    {
        $result = $this->productRepository->searchProducts($request);
        return callback_data(success(), 'search_results', $result);
    }

    public function productsByShop(ProductsRequest $request)
    {
        $user = checkJWT();
        $user_id = $user != null ? $user->id : null;

        $products = $this->productRepository->getProductsByShop($request->shop_id,$user_id);
        return callback_data(success(), 'products_by_shop', $products);
    }

    public function productsBySubCategory(ProductsRequest $request)
    {
        $products = $this->productRepository->getProductsBySubCategory($request->sub_category_id);
        return callback_data(success(), 'products_by_sub_category', $products);
    }

    public function productDetails(ProductsRequest $request)
    {
        $user = checkJWT();
        $user_id = $user != null ? $user->id : null;
        $product_details = $this->productRepository->productDetails($request->product_id,($user ? $user_id : null));
        return callback_data(success(), 'product_details', $product_details);
    }
    public function productOtherInfo(ProductsRequest $request)
    {
        $product_other_info = $this->productRepository->productOtherInfo($request->product_id);
        return callback_data(success(), 'product_other_info', $product_other_info);
    }

    public function getReviews(ProductsRequest $request)
    {
        $reviews = $this->productRepository->getProductReviews($request->product_id);
        return callback_data(success(),'product_reviews',$reviews);
    }

    public function addReview(ProductsRequest $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $review = $this->productRepository->addReview($request, $user->id);
            if ($review){
                return callback_data(success(), 'review_added');
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function FavouriteAction(ProductsRequest $request)
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $favourite = $this->productRepository->FavouriteAction($request->product_id, $user->id);
            if ($favourite){
                return callback_data(success(), $favourite == 'added' ? 'product_added_to_favourite' : 'product_removed_from_favourite', $favourite == 'added' ? ['added' => 1] :  ['added' => 0] );
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }

    public function getFavouriteProducts()
    {
        if (!request()->headers->has('jwt')){
            return callback_data(error(),'check_jwt');
        }
        $user = checkJWT();
        if ($user){
            $favourite_products = $this->productRepository->getFavouriteProducts($user->id);
            if ($favourite_products){
                return callback_data(success(), 'favourite_products', $favourite_products);
            }
            return callback_data(error(), 'errors');
        }else{
            return callback_data(error(),'user_not_found');
        }
    }
}
