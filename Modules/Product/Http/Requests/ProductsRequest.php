<?php

namespace Modules\Product\Http\Requests;

use App\Http\Requests\Request;

class ProductsRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->is('api/search-products')) {
            return $this->searchProductsRules();
        }
        if($this->is('api/products-by-shop')) {
            return $this->productsByShopRules();
        }
        if($this->is('api/products-by-sub_category')) {
            return $this->productsBySubCategoryRules();
        }
        if($this->is('api/product-details') || $this->is('api/product-other-info') || $this->is('api/get-reviews') || $this->is('api/favourite-action')) {
            return $this->productDetailsRules();
        }
        if($this->is('api/add-review')) {
            return $this->addReviewRules();
        }
    }

    public function messages()
    {
        if($this->is('api/search-products')) {
            return $this->searchProductsMessages();
        }
        if($this->is('api/products-by-shop')) {
            return $this->productsByShopMessages();
        }
        if($this->is('api/products-by-sub_category')) {
            return $this->productsBySubCategoryMessages();
        }
        if($this->is('api/product-details') || $this->is('api/product-other-info') || $this->is('api/get-reviews') || $this->is('api/favourite-action')) {
            return $this->productDetailsMessages();
        }
        if($this->is('api/add-review')) {
            return $this->addReviewMessages();
        }
    }

    public function searchProductsRules()
    {
        return [
//            'price' => 'required_without:date|in:desc,asc',
//            'date' => 'required_without:price|in:desc,asc',
//            'price' => 'required_without:date|in:desc,asc',
            'key' => 'required',
        ];
    }

    public function searchProductsMessages()
    {
        return [
//            'price.required_without' => 'price_required',
//            'price.in' => 'price_in',
//            'date.required_without' => 'date_required',
//            'date.in' => 'date_in',
            'key.required' => 'write_name_or_description',
        ];
    }

    public function productsByShopRules()
    {
        return [
            'shop_id' => 'required|exists:shops,id',
        ];
    }

    public function productsByShopMessages()
    {
        return [
            'shop_id.required' => 'shop_required',
            'shop_id.exists' => 'shop_not_found',
        ];
    }

    public function productsBySubCategoryRules()
    {
        return [
            'sub_category_id' => 'required|exists:sub_categories,id',
        ];
    }

    public function productsBySubCategoryMessages()
    {
        return [
            'sub_category_id.required' => 'sub_category_required',
            'sub_category_id.exists' => 'sub_category_not_found',
        ];
    }

    public function productDetailsRules()
    {
        return [
            'product_id' => 'required|exists:products,id',
        ];
    }

    public function productDetailsMessages()
    {
        return [
            'product_id.required' => 'product_required',
            'product_id.exists' => 'product_not_found',
        ];
    }

    public function addReviewRules()
    {
        return [
            'product_id' => 'required|exists:products,id',
            'rate' => 'required',
            'comment' => 'required|min:3',
            'user_name' => 'min:3',
        ];
    }

    public function addReviewMessages()
    {
        return [
            'product_id.required' => 'product_required',
            'product_id.exists' => 'product_not_found',
            'rate.required' => 'rate_required',
            'comment.min' => 'comment_min',
            'user_name.min' => 'user_name_min',
        ];
    }
}
