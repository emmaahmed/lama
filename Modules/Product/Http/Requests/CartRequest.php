<?php

namespace Modules\Product\Http\Requests;

use App\Http\Requests\Request;

class CartRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->is('api/add-to-cart') || $this->is('api/remove-from-cart')) {
            return $this->addToCartRules();
        }
    }

    public function messages()
    {
        if($this->is('api/add-to-cart') || $this->is('api/remove-from-cart')) {
            return $this->addToCartMessages();
        }
    }

    public function addToCartRules()
    {
        return [
            'product_id' => 'required|exists:products,id',
        ];
    }

    public function addToCartMessages()
    {
        return [
            'product_id.required' => 'product_required',
            'product_id.exists' => 'product_not_found',
        ];
    }
}
