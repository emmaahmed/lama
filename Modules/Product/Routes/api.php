<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/product', function (Request $request) {
    return $request->user();
});
Route::middleware('checkDatabase')->group(function () {

Route::namespace('Api')->group(function(){
    Route::get('/products', 'ProductsController@products');
    Route::get('/offers', 'ProductsController@offers');

    Route::get('/search-products', 'ProductsController@searchProducts');
    Route::get('/products-by-shop', 'ProductsController@productsByShop');
    Route::get('/products-by-sub_category', 'ProductsController@productsBySubCategory');
    Route::get('/product-details', 'ProductsController@productDetails');
    Route::get('/product-other-info', 'ProductsController@productOtherInfo');

//    Product Reviews
    Route::get('/get-reviews', 'ProductsController@getReviews');
    Route::post('/add-review', 'ProductsController@addReview');

//    Favourite
    Route::post('/favourite-action', 'ProductsController@FavouriteAction');
    Route::get('/get-favourites', 'ProductsController@getFavouriteProducts');

});
});


