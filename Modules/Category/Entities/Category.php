<?php

namespace Modules\Category\Entities;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Shop\Entities\Shop;

class Category extends Model  implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = ['image','active'];

    protected $hidden = ['translations','created_at','updated_at'];

    public function getImageAttribute($image)
    {
        if (!empty($image)){
            return asset('uploads/categories').'/'.$image;
        }
        return "";
    }
    public function shops(){
        return $this->hasMany( Shop::class);
    }

    public static function getTableName()
    {
        return (new self())->getTable();
    }
}
