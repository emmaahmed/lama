<?php

namespace Modules\Category\Repositories\Interfaces;

interface CategoryRepositoryInterface
{
    public function getCategories();

    public function store($request);
    
    public function changeStatus($request);
}
