<?php


namespace Modules\Category\Repositories;


use App\Repositories\Interfaces\HomeRepositoryInterface;
use Modules\Category\Entities\Category;
use Modules\Category\Repositories\Interfaces\CategoryRepositoryInterface;

class CategoryRepository implements CategoryRepositoryInterface
{
    protected $homeRepository;

    public function __construct(HomeRepositoryInterface $homeRepository)
    {
        $this->homeRepository = $homeRepository;
    }

    public function getCategories()
    {
//        $categories = $this->homeRepository->categories()->paginate(10);
        $categories = $this->homeRepository->categories()->get();
        return $categories;
    }

    // Admin

    public function store($request)
    {
        $category = new Category();
        if ($request->has('image')){
            $imageField = upload($request->image, 'categories');
            $category->image = $imageField;
        }
        $category->save();
        $category->translateOrNew('ar')->name = $request->name_ar;
        $category->translateOrNew('en')->name = $request->name_ar;
//        $category->translateOrNew('en')->name = $request->name_en;
        return $category->save();
    }

    public function edit($request)
    {
        $data['title'] = "تعديل القسم";
        $category = $this->homeRepository->categories()->where('id',$request->category_id)->first();
        $data['category'] = $category;
        return $data;
    }

    public function update($request)
    {
        $category = $this->homeRepository->categories()->where('id',$request->category_id)->first();
        if (isset($category)){
            $category->translateOrNew('ar')->name = $request->name_ar;
            $category->translateOrNew('en')->name = $request->name_ar;
//            $category->translateOrNew('en')->name = $request->name_en;
            if ($category->save()){
                if ($request->has('image')){
                    if (!empty($category->getOriginal('image'))){
                        unlinkFile($category->getOriginal('image'), 'categories');
                    }
                    $imageField = upload($request->image, 'categories');
                    $category->image = $imageField;
                    $category->save();
                }
                return true;
            }
        }
        return false;
    }

    public function changeStatus($request)
    {
        $shop = $this->homeRepository->categories()->where('id',$request->category_id)->first();
        $shop->active = $request->active;
        if ($shop->save()){
            if ($shop->active == 1){
                return 'activated';
            }
            return 'deactivated';
        }else{
            return false;
        }
    }

}
