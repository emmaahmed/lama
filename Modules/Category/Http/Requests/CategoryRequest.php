<?php

namespace Modules\Category\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->is('admin/categories/add')) {
            return $this->storeCategoryRules();
        }
        if($this->is('admin/categories/edit')
            || $this->is('admin/categories/delete')) {
                return $this->checkCategoryRules();
        }
        if($this->is('admin/categories/update')) {
            return $this->updateCategoryRules();
        }
        if($this->is('admin/categories/delete-multi')) {
            return $this->deleteMultiRules();
        }
    }

    public function messages()
    {
        if($this->is('admin/categories/add')) {
            return $this->storeCategoryMessages();
        }
        if($this->is('admin/categories/edit')
            || $this->is('admin/categories/delete')) {
                return $this->checkCategoryMessages();
        }
        if($this->is('admin/categories/update')) {
            return $this->updateCategoryMessages();
        }
        if($this->is('admin/categories/delete-multi')) {
            return $this->deleteMultiMessages();
        }
    }



    public function storeCategoryRules()
    {
        return [
            'name_ar' => 'required',
//            'name_en' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png,gif'
        ];
    }

    public function storeCategoryMessages()
    {
        return [
            'name_ar.required' => 'من فضلك أدخل إسم القسم باللغة العربية',
//            'name_en.required' => 'من فضلك أدخل إسم القسم باللغة الانجليزية',
            'image.required' => 'من فضلك أدخل الصورة',
            'image.mimies' => 'من فضلك أختر صورة',
        ];
    }

    public function checkCategoryRules()
    {
        return [
            'category_id' => 'required|exists:categories,id',
        ];
    }

    public function checkCategoryMessages()
    {
        return [
            'category_id.required' => 'اختر القسم',
            'category_id.exists' => 'القسم غير موجود',
        ];
    }

    public function updateCategoryRules()
    {
        return [
            'name_ar' => 'required',
//            'name_en' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif'
        ];
    }

    public function updateCategoryMessages()
    {
        return [
            'name_ar.required' => 'من فضلك أدخل إسم القسم باللغة العربية',
//            'name_en.required' => 'من فضلك أدخل إسم القسم باللغة الإنجليزية',
            'image.mimies' => 'من فضلك أختر صورة',
        ];
    }

    public function deleteMultiRules()
    {
        return [
            'ids' => 'required|array',
            'ids.*.id' => 'exists:categories,id',
        ];
    }

    public function deleteMultiMessages()
    {
        return [
            'ids.required' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.array' => 'من فضلك أختر عنصر واحد علي الأقل',
            'ids.*.id' => 'هذا القسم غير موجود',
        ];
    }
}
