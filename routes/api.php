<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/code', function (){
    $verify=\App\Verification::where('phone',\request()->phone)->where('shop_id','!=',null)->first();
     if($verify){
         return $verify->code;
     }
     else{
         return 'incorrect';
     }
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware('checkDatabase')->group(function () {

Route::namespace('Api\User')->prefix('user')->group(function(){
//    Auth Cycle
    Route::post('/register', 'AuthController@Register');
    Route::post('/check-social', 'AuthController@checkSocial');
    Route::post('/verify-code', 'AuthController@Verify');
    Route::post('/resend-code', 'AuthController@resendCode');
    Route::post('/login', 'AuthController@Login');
    Route::post('/send-code', 'AuthController@sendCode');
    Route::post('/change-password', 'AuthController@changePassword');
    Route::post('/update-profile', 'AuthController@updateProfile');
    Route::get('/delete-account', 'AuthController@deleteAccount');
    Route::get('/delete-shop-account', 'AuthController@deleteShopAccount');

//  General
    Route::get('/home', 'HomeController@index');
    Route::get('/all-offers', 'HomeController@allOffers');

//    User Addresses
    Route::get('/addresses', 'UserAddressController@getAddresses');
    Route::post('/create-address', 'UserAddressController@store')->name('create-address');
    Route::post('/remove-address', 'UserAddressController@delete');

//    Notifications
    Route::get('/notifications', 'NotificationController@getNotifications');
});

Route::namespace('Api\Delegate')->prefix('delegate')->group(function(){
//    Auth Cycle
    Route::post('/verify-code', 'AuthController@Verify');
    Route::post('/resend-code', 'AuthController@resendCode');
    Route::post('/login', 'AuthController@Login');
    Route::post('/send-code', 'AuthController@sendCode');
    Route::post('/change-password', 'AuthController@changePassword');
    Route::post('/update-profile', 'AuthController@updateProfile');
    Route::get('/change-status', 'AuthController@changeStatus');

});

Route::namespace('Api')->group(function(){
    Route::get('/tutorials', 'TutorialsController@tutorials');
    Route::get('/cities', 'CitiesController@cities');

//  Contact
    Route::post('/send-contact', 'ContactController@sendContact');
});
    Route::get('/policy', 'HomeController@policy');

});

