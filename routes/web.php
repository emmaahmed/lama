<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Artisan;
//delegate form
Route::get('delegate/signup-form', function () {
    $user_id = 0;
    $phone = 0;
    $car_types = [];
    $terms=\App\Terms::where('type',0)->get();

    return view('delegate-signup-form',compact('user_id','phone','car_types','terms'));
});
Route::get('shop/signup-form', function () {
    $user_id = 0;
    $phone = 0;
    $car_types = [];
    $terms=\App\Terms::where('type',1)->get();
    return view('shop-signup-form',compact('user_id','phone','car_types','terms'));
});
    Route::get('code',function (){
        if(\App\Verification::where('shop_id','!=',null)->where('phone',request()->phone)->first()) {
            return \App\Verification::where('shop_id', '!=', null)->where('phone', request()->phone)->first()->code;
        }
        else{
            return 'Check phone again';
        }
        });
Route::post('/signup/form','HomeController@formRequest')->name('form-submit');
//
Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/cache-clear', function()
{
    Artisan::call('config:clear');
    Artisan::call('optimize:clear');
        Artisan::call('cache:clear');

    echo 'cache:clear complete';
});
Route::get('PrintESCPOSController', 'PrintESCPOSController@printCommands')->name('print.commands');
Route::any('WebClientPrintController', 'WebClientPrintController@processRequest')->name('print.process');
