<?php

return
    [
        'errors' =>
            [
                'ar' => 'حدث خطأ ما',
                'en' => 'Something Went wrong ',
            ],
        'success' =>
            [
                'ar' => 'تمت العملية بنجاح',
                'en' => 'Done successfully ',
            ],
        'check_lang' => 'تأكد من اللغة',
        'check_jwt' =>
            [
                'ar' => 'غير قادر علي التحقق من المستخدم',
                'en' => 'Can\'t validate user',
            ],
        'delegate_not_found' =>
            [
                'ar' => 'مندوب غير موجود',
                'en' => 'Delegate not found',
            ],
        'name_required' =>
            [
                'ar' => 'الإسم مطلوب',
                'en' => 'Name required',
            ],
        'email_required' =>
            [
                'ar' => 'الايميل مطلوب',
                'en' => 'Email required',
            ],
        'phone_required' =>
            [
                'ar' => 'رقم الهاتف مطلوب',
                'en' => 'Phone required',
            ],
        'password_required' =>
            [
                'ar' => 'كلمة المرور مطلوب',
                'en' => 'Password required',
            ],
        'phone_exist' =>
            [
                'ar' => 'الهاتف موجود من قبل',
                'en' => 'Phone already exists',
            ],
        'email_exist' =>
            [
                'ar' => 'البريد الإلكتروني موجود من قبل',
                'en' => 'Email already exists',
            ],
        'registered' =>
            [
                'ar' => 'تم التسجيل بنجاح,الرجاء التفعيل',
                'en' => 'Registered Successfully ,please activate',
            ],
        'phone_not_found' =>
            [
                'ar' => 'رقم التليفون غير موجود',
                'en' => 'phone not exists',
            ],
        'email_not_found' =>
            [
                'ar' => 'البريد الإلكتروني غير موجود',
                'en' => 'email not exists',
            ],
        'code_sent' =>
            [
                'ar' => 'تم إرسال الكود',
                'en' => 'Code Sent Successfully',
            ],
        'invalid_data' =>
            [
                'ar' => 'بيانات خاطئة',
                'en' => 'Invalid Data',
            ],
        'user_not_found' =>
            [
                'ar' => 'مستخدم غير موجود',
                'en' => 'User not exist',
            ],
        'code_required' =>
            [
                'ar' => 'الكود مطلوب',
                'en' => 'Code required',
            ],
        'code_not_found' =>
            [
                'ar' => 'كود التفعيل غير موجود',
                'en' => 'code not found',
            ],
        'code_used' =>
            [
                'ar' => 'تم استخدام هذا الكود من قبل',
                'en' => 'Code used before',
            ],
        'time_exceeded' =>
            [
                'ar' => 'الكود لم يعد صالح',
                'en' => 'Code time exceeded',
            ],
        'activated' =>
            [
                'ar' => 'تم التفعيل بنجاح',
                'en' => 'Activated Successfully',
            ],
        'user_not_activated' =>
            [
                'ar' => 'من فضلك قم بتفعيل حسابك أولا',
                'en' => 'account not activated',
            ],
        'logged_in' =>
            [
                'ar' => 'تم تسجيل الدخول بنجاح',
                'en' => 'LoggedIn Successfully',
            ],
        'password_confirmation_required' =>
            [
                'ar' => 'كلمة المرور مطلوب',
                'en' => 'Password required',
            ],
        'password_not_matches' =>
            [
                'ar' => 'كلمة المرور غير متطابقة',
                'en' => 'password not matches',
            ],
        'password_changes' =>
            [
                'ar' => 'تم تغيير كلمة المرور بنجاح',
                'en' => 'password changes successfully',
            ],
        'phone_or_email_required' =>
            [
                'ar' => 'من فضلك أدخل رقم الهاتف / البريد الإلكتروني',
                'en' => 'please enter email / phone',
            ],
        'password_not_correct' =>
            [
                'ar' => 'كلمة المرور غير صحيحة',
                'en' => 'password not correct',
            ],
        'logout' =>
            [
                'ar' => 'تم تسجيل الخروج',
                'en' => 'logout successfully',
            ],
        'home' =>
            [
                'ar' => 'الرئيسية',
                'en' => 'Home',
            ],
        'searched_before' =>
            [
                'ar' => 'الأكثر بحثاً',
                'en' => 'most searched',
            ],
        'search_result' =>
            [
                'ar' => 'نتائج البحث',
                'en' => 'search results',
            ],
        'category_required' =>
            [
                'ar' => 'اختر القسم',
                'en' => 'Category required',
            ],
        'category_not_found' =>
            [
                'ar' => 'القسم غير موجود',
                'en' => 'Category not exists',
            ],
        'sub_category_required' =>
            [
                'ar' => 'اختر القسم الفرعي',
                'en' => 'Sub Category required',
            ],
        'sub_category_not_found' =>
            [
                'ar' => 'القسم الفرعي غير موجود',
                'en' => 'Sub Category not exists',
            ],
        'shops_by_category' =>
            [
                'ar' => 'المتاجر حسب القسم',
                'en' => 'Shops By Category',
            ],
        'offers' =>
            [
                'ar' => 'العروض',
                'en' => 'offers',
            ],
        'category_tags' =>
            [
                'ar' => 'الوسوم حسب القسم',
                'en' => 'Tags By Category',
            ],
        'filteration_results' =>
            [
                'ar' => 'نتائج التصفية',
                'en' => 'Filter results',
            ],
        'no_shops' =>
            [
                'ar' => 'لا يوجد متاجر',
                'en' => 'No Shops',
            ],
        'shop_details' =>
            [
                'ar' => 'تفاصيل المتجر',
                'en' => 'Shop Details',
            ],
        'shop_id_required' =>
            [
                'ar' => 'اختر المتجر',
                'en' => 'Shop required',
            ],
        'shop_not_found' =>
            [
                'ar' => 'المتجر غير موجود',
                'en' => 'Shop not found',
            ],
        'new_products' =>
            [
                'ar' => 'المنتجات الجديدةً',
                'en' => 'New Products',
            ],
        'no_products' =>
            [
                'ar' => 'لا يوجد منتجات',
                'en' => 'No products',
            ],
        'sub_category_id_required' =>
            [
                'ar' => 'اختر القسم الفرعي',
                'en' => 'Select Sub Category',
            ],
        'sub_category_not_found' =>
            [
                'ar' => 'القسم الفرعي غير موجود',
                'en' => 'Sub Category Not Found',
            ],
        'shop_followed_successfully' =>
            [
                'ar' => 'لقد قمت بمتابعة المتجر',
                'en' => 'You Followed Shop Successfully',
            ],
        'shop_followed_before' =>
            [
                'ar' => 'لقد قمت بمتابعة المتجر من قبل',
                'en' => 'You Followed Shop Before',
            ],
        'product_id_required' =>
            [
                'ar' => 'اختر المنتج',
                'en' => 'Select product',
            ],
        'product_not_found' =>
            [
                'ar' => 'المنتج غير موجود',
                'en' => 'Product not found',
            ],
        'product_details' =>
            [
                'ar' => 'تفاصيل المنتج',
                'en' => 'Product Details',
            ],
        'items_ids_required' =>
            [
                'ar' => 'من فضلك أدخل المنتجات',
                'en' => 'items required',
            ],
        'shops_ids_required' =>
            [
                'ar' => 'من فضلك أدخل المتجر',
                'en' => 'shop required',
            ],
        'address_created' =>
            [
                'ar' => 'تم إضافة العنوان بنجاح',
                'en' => 'Address Added successfully',
            ],
        'address_updated' =>
            [
                'ar' => 'تم تعديل العنوان بنجاح',
                'en' => 'Address Updated successfully',
            ],
        'order_created' =>
            [
                'ar' => 'تم إنشاء الطلب بنجاح',
                'en' => 'Order created successfully',
            ],
        'followed_shops' =>
            [
                'ar' => 'المتاجر التي سبق وقمت بعمل متابعة لها',
                'en' => 'Markets you followed before now',
            ],
        'orders' =>
            [
                'ar' => 'طلباتي',
                'en' => 'My Orders',
            ],
        'order_id_required' =>
            [
                'ar' => 'اختر الطلب',
                'en' => 'Order required',
            ],
        'order_not_found' =>
            [
                'ar' => 'الطلب غير موجود',
                'en' => 'Order not found',
            ],
        'order_details' =>
            [
                'ar' => 'تفاصيل الطلب',
                'en' => 'Order details',
            ],
        'shop_rated_before' =>
            [
                'ar' => 'لقد قمت بتقييم المتجر من قبل',
                'en' => 'Shop Rated Before',
            ],
        'shop_rated' =>
            [
                'ar' => 'تم تقييم المتجر',
                'en' => 'Shop Rated Successfully',
            ],
        'rate_required' =>
            [
                'ar' => 'اختر التقييم من 1 إلي 5 نجوم',
                'en' => 'Choose Stars from 1 to 5',
            ],
        'shop_rates' =>
            [
                'ar' => 'تقييمات المتجر',
                'en' => 'shop rates',
            ],
        'title_required' =>
            [
                'ar' => 'ادخل عنوان الرسالة',
                'en' => 'title required',
            ],
        'description_required' =>
            [
                'ar' => 'ادخل نص الرسالة',
                'en' => 'message required',
            ],
        'contact_sent_successfully' =>
            [
                'ar' => 'شكرا لتواصلك معنا ، سيتم النظر في الأمر',
                'en' => 'Thank you for contacting us , The matter will be considered',
            ],
        'settings' =>
            [
                'ar' => 'الإعدادات',
                'en' => 'Settings',
            ],
        'social_id_required' =>
            [
                'ar' => 'ارسل رقم التواصل',
                'en' => 'Social id required',
            ],
        'social_found' =>
            [
                'ar' => 'رقم التواصل موجود',
                'en' => 'Social id found Successfully',
            ],
        'social_not_found' =>
            [
                'ar' => 'رقم التواصل غير موجود',
                'en' => 'Social id not found',
            ],
        'profile_updated' =>
            [
                'ar' => 'تم تعديل بياناتك بنجاح',
                'en' => 'Profile Updated Successfully',
            ],
        'saved_addresses' =>
            [
                'ar' => 'العناوين المحفوظة',
                'en' => 'Saved Addresses',
            ],
        'address_not_found' =>
            [
                'ar' => 'العنوان غير موجود',
                'en' => 'Address Not Found',
            ],
        'address_added_to_favourite' =>
            [
                'ar' => 'تم إضافة العنوان لقائمة العناوين المحفوظة',
                'en' => 'Address Added To Saved Locations Successfully',
            ],
        'address_removed_from_favourite' =>
            [
                'ar' => 'تم حذف العنوان من قائمة العناوين المحفوظة',
                'en' => 'Address removed from favourite Locations Successfully',
            ],
        'shop_registered' =>
            [
                'ar' => 'تم إرسال طلب التقديم كمتجر وسيتم التواصل معك لتكملة بياناتك',
                'en' => 'Rquest Sent Successfully , please wait for our contact',
            ],
        'name_exist' =>
            [
                'ar' => 'الاسم موجود من قبل',
                'en' => 'Name Exists before',
            ],
        'full_name_exist' =>
            [
                'ar' => 'الاسم موجود من قبل',
                'en' => 'Name Exists before',
            ],
        'full_name_required' =>
            [
                'ar' => 'ادخل الاسم باللغة الإنجليزية',
                'en' => 'English Name Required',
            ],
        'business_registeration_required' =>
            [
                'ar' => 'السجل التجاري',
                'en' => 'Business registeration required',
            ],
        'tax_number_required' =>
            [
                'ar' => 'الرقم الضريبي مطلوب',
                'en' => 'Tax Number Required',
            ],
        'order_cancelled' =>
            [
                'ar' => 'تم إلغاء الطلب بنجاح',
                'en' => 'Order Cancelled successfully',
            ],
        'notifications' =>
            [
                'ar' => 'الإشعارات',
                'en' => 'Notifications',
            ],
        'email_or_password_not_correct' =>
            [
                'ar' => 'البيانات غير صحيحة',
                'en' => 'info not correct',
            ],
        'check_data' =>
            [
                'ar' => 'بريد الإلكتروني و كلمة السر غير صحيحة',
                'en' => 'email or password not correct',
            ],
        'check_phone_data' =>
            [
                'ar' => 'الهاتف أو كلمة السر غير صحيحة',
                'en' => 'phone or password not correct',
            ],
        'account_not_activated' =>
            [
                'ar' => 'الحساب غير مفعل يرجي تفعيله أولا',
                'en' => 'Account not verified please activate first',
            ],

        //////////////////////////////////////////////////////////////////////////////////

        'tutorials' =>
            [
                'ar' => 'مرحبا',
                'en' => 'Welcome',
            ],
        'cities' =>
            [
                'ar' => 'المدن',
                'en' => 'Cities',
            ],
        'social' =>
            [
                'ar' => 'السوشيال ميديا',
                'en' => 'Social Media',
            ],
        'categories' =>
            [
                'ar' => 'الأقسام',
                'en' => 'categories',
            ],
        'message_required' =>
            [
                'ar' => 'من فضلك ادخل الرسالة',
                'en' => 'messages required',
            ],
        'message_min' =>
            [
                'ar' => 'من فضلك ادخل 6 أحرف علي الاقل',
                'en' => 'message 6 chars at least',
            ],
        'contact_sent' =>
            [
                'ar' => 'تم إرسال الرسالة وسيتم الرد قريبًا',
                'en' => 'UserDelegateMessage sent, please wait for reply',
            ],
        'user_addresses' =>
            [
                'ar' => 'العناوين الخاصة بي',
                'en' => 'My Addresses',
            ],
        'city_id_required' =>
            [
                'ar' => 'اختر المدينة',
                'en' => 'City Required',
            ],
        'city_id_exists' =>
            [
                'ar' => 'المدينة غير موجودة',
                'en' => 'City not found',
            ],
        'latitude_required' =>
            [
                'ar' => 'اختر العنوان من الخريطة',
                'en' => 'Please Choose Address',
            ],
        'longitude_required' =>
            [
                'ar' => 'اختر العنوان من الخريطة',
                'en' => 'Please Choose Address',
            ],
        'address_required' =>
            [
                'ar' => 'ادخل العنوان ',
                'en' => 'Please write Address',
            ],
        'full_name_min' =>
            [
                'ar' => 'ادخل الإسم بالكامل',
                'en' => 'please write full name',
            ],
        'building_name_required' =>
            [
                'ar' => 'من فضلك ادخل اسم المبني',
                'en' => 'Please Choose building name',
            ],
        'building_image_required' =>
            [
                'ar' => 'من فضلك ادخل صورة المبني',
                'en' => 'Please Choose building image',
            ],
        'building_number_required' =>
            [
                'ar' => 'من فضلك ادخل رقم المبني',
                'en' => 'Please Choose building number',
            ],
        'apartment_required' =>
            [
                'ar' => 'من فضلك ادخل رقم الشقة',
                'en' => 'Please Choose apartment number',
            ],
        'address_id_exists' =>
            [
                'ar' => 'عذراً العنوان غير موجود',
                'en' => 'Soory Address not Found',
            ],
        'address_deleted' =>
            [
                'ar' => 'تم حذف العنوان بنجاح',
                'en' => 'User Address Deleted',
            ],
        'search_results' =>
            [
                'ar' => 'نتائج البحث',
                'en' => 'Search Results',
            ],
        'price_required' =>
            [
                'ar' => 'من فضلك اختر السعر او التاريخ',
                'en' => 'Please choose price or date',
            ],
        'price_in' =>
            [
                'ar' => 'من فضلك اختر السعر إما desc او asc',
                'en' => 'price should be one of desc or asc',
            ],
        'date_required' =>
            [
                'ar' => 'من فضلك اختر السعر او التاريخ',
                'en' => 'Please choose price or date',
            ],
        'date_in' =>
            [
                'ar' => 'من فضلك اختر التاريخ إما desc او asc',
                'en' => 'date should be one of desc or asc',
            ],
        'shop_required' =>
            [
                'ar' => 'اختر المتجر',
                'en' => 'Shop Required',
            ],
        'products_by_shop' =>
            [
                'ar' => 'المنتجات حسب المتجر',
                'en' => 'Shop Products',
            ],
        'products_by_sub_category' =>
            [
                'ar' => 'المنتجات حسب القسم الفرعي',
                'en' => 'Sub Category Products',
            ],
        'product_required' =>
            [
                'ar' => 'اختر المنتج',
                'en' => 'Product Required',
            ],
        'product_reviews' =>
            [
                'ar' => 'تقييمات المنتج',
                'en' => 'Product Reviews',
            ],
        'review_added' =>
            [
                'ar' => 'تم إضافة تقييم',
                'en' => 'Reveiw Added Successfully',
            ],
        'comment_min' =>
            [
                'ar' => 'من فضلك ادخل 3 حروف علي الأقل',
                'en' => 'Comment at least 3 letters',
            ],
        'user_name_min' =>
            [
                'ar' => 'من فضلك ادخل 3 حروف علي الأقل',
                'en' => 'Username at least 3 letters',
            ],
        'item_added_to_cart' =>
            [
                'ar' => 'تم إضافة المنتج للسلة',
                'en' => 'Items added to cart successfully',
            ],
        'cart' =>
            [
                'ar' => 'سلة التسوق',
                'en' => 'Cart',
            ],
        'item_removed_from_cart' =>
            [
                'ar' => 'تم حذف المنتج من السلة',
                'en' => 'Items removed from cart successfully',
            ],
        'cart_item_id_required' =>
            [
                'ar' => 'اختر العنصر داخل السلة',
                'en' => 'Please Choose Cart item',
            ],
        'cart_item_id_not_found' =>
            [
                'ar' => 'العنصر غير موجود بالسلة',
                'en' => 'Item Not found in cart',
            ],
        'delivery_fees' =>
            [
                'ar' => 'مصاريف الشحن',
                'en' => 'Delivery Fees',
            ],
        'delivery_required' =>
            [
                'ar' => 'اختر نوع التوصيل',
                'en' => 'Choose Delivery type',
            ],
        'delivery_type_not_found' =>
            [
                'ar' => 'نوع التوصيل غير موجود',
                'en' => 'Delivery Type Not Found',
            ],
        'payment_required' =>
            [
                'ar' => 'اختر طريقة الدفع',
                'en' => 'Choose payment Method',
            ],
        'payment_not_found' =>
            [
                'ar' => 'طريقة الدفع غير موجودة',
                'en' => 'Payment Method not found',
            ],
        'cart_not_found' =>
            [
                'ar' => 'السلة فارغة',
                'en' => 'Cart not found',
            ],
        'cart_not_array' =>
            [
                'ar' => 'مشكلة في السلة',
                'en' => 'Cart Error',
            ],
        'order_can_not_be_cancelled' =>
            [
                'ar' => 'لا يمكن إلغاء الطلب',
                'en' => 'Order Can not be cancelled now',
            ],
        'product_added_to_favourite' =>
            [
                'ar' => 'تم إضافة المنتج للمفضلة',
                'en' => 'Product Added To Favourite',
            ],
        'product_removed_from_favourite' =>
            [
                'ar' => 'تم حذف المنتج من المفضلة',
                'en' => 'Product Removed From Favourite',
            ],
        'favourite_products' =>
            [
                'ar' => 'السلع المفضلة',
                'en' => 'Favourite Products',
            ],
        'product_other_info' =>
            [
                'ar' => 'التفاصيل التقنية',
                'en' => 'Technical Details',
            ],
        'name_min' =>
            [
                'ar' => 'من فضلك ادخل 3 حروف علي الأقل',
                'en' => 'Username at least 3 letters',
            ],
        'write_name_or_description' =>
            [
                'ar' => 'من فضلك ادخل حرف واحد علي الأقل',
                'en' => 'Please Write a letter at least',
            ],
        'can_not_cancel_order' =>
            [
                'ar' => 'لا يمكن إلغاء الطلب الآن',
                'en' => 'Can not reject order now',
            ],
        'order_canceled_before' =>
            [
                'ar' => 'عذرا تم إلغاء الطلب من قبل',
                'en' => 'Sorry Order Canceled before',
            ],
        'order_accepted' =>
            [
                'ar' => 'تم قبول الطلب',
                'en' => 'Order Accepted Successfully',
            ],
        'on_way' =>
            [
                'ar' => 'في الطريق',
                'en' => 'On Way',
            ],
        'arrived' =>
            [
                'ar' => 'تم التوصيل بنجاح',
                'en' => 'Delivered Successfully',
            ],
        'no_action' =>
            [
                'ar' => 'لا يمكن القيام بأي إجراء',
                'en' => 'No Action could be done',
            ],
        'status_required' =>
            [
                'ar' => 'اختر الحالة',
                'en' => 'Status required',
            ],
        'status_not_valid' =>
            [
                'ar' => 'حالة الطلب غير موجودة',
                'en' => 'Status Not Valid',
            ],
        'canceled' =>
            [
                'ar' => 'تم رفض الطلب',
                'en' => 'Order Rejected',
            ],
        'my_orders' =>
            [
                'ar' => 'طلباتي',
                'en' => 'My Orders',
            ],
        'shop_online' =>
            [
                'ar' => 'المتجر متاح',
                'en' => 'Shop Online',
            ],
        'shop_offline' =>
            [
                'ar' => 'المتجر مغلق',
                'en' => 'Shop Closed',
            ],
        // Admin
        'user_required' =>
            [
                'ar' => 'اختر المستخدم',
                'en' => 'User Required',
            ],
        'followed' =>
            [
                'ar' => 'تمت المتابعة',
                'en' => 'Followed successfully',
            ],
        'unfollowed' =>
            [
                'ar' => 'تم الغاء المتابعة',
                'en' => 'Unfollowed successfully',
            ],
        'order_rated' =>
            [
                'ar' => 'تم التقييم لنجاح',
                'en' => 'rated successfully',
            ],
        'order_can_not_be_rated' =>
            [
                'ar' => 'لا يمكنك التقييم',
                'en' => 'Order Can not be rated',
            ],
        'quantity_not_available' =>
            [
                'ar' => 'الكمية غير موجودة',
                'en' => 'quantity not available',
            ],
        'email_or_phone' =>
            [
                'ar' => 'من فضلك ادخل ايميل او رقم الهاتف',
                'en' => 'Please Enter Email Or Phone',
            ],
        'product_quantity_unavailable' =>
            [
                'ar' => 'هذه الكمية غير متاحة من المنتج ',
                'en' => 'Quantity is not available for ',
            ],
        'status_updated' =>
            [
                'ar' => 'تم تغيير الحالة بنجاح ',
                'en' => 'Status Changed Successfully ',
            ],
        'not-in-range' =>
            [
                'ar' => 'المتجر خارج نطاق الخدمة',
                'en' => 'shop is out of range ',
            ],
        'account_blocked' =>
            [
                'ar' => 'تم غلق الحساب من قبل الإدمن',
                'en' => 'your account is blocked by admin ',
            ],
        'lat_required' =>
            [
                'ar' => 'من فضلك ادخل خطوط الطول',
                'en' => 'please enter lat ',
            ],
        'lng_required' =>
            [
                'ar' => 'من فضلك ادخل خطوط العرض',
                'en' => 'please enter lng ',
            ],
        'account_waiting_for_approval' =>
            [
                'ar' => 'يتم مراجعة المتجر من الأدمن',
                'en' => 'Waiting for admin approval',
            ],
        'about_us' =>
            [
                'ar' => 'عن التطبيق',
                'en' => 'About App',
            ],
        'terms' =>
            [
                'ar' => 'الشروط و الأحكام',
                'en' => 'Terms & Conditions',
            ],
        'privacy' =>
            [
                'ar' => 'سياسة الخصوصية',
                'en' => 'privacy',
            ],
        'account_will_be_suspended' =>
            [
                'ar' => 'سوف يتم ايقاف الحساب بعد محاولة إدخال اخري خاطئة',
                'en' => 'account will be suspended after your third trail',
            ],
        'in_high-and-low' =>
            [
                'ar' => 'يجب ان تكون اما high او low',
                'en' => 'it should be high or low',
            ],
        'in_latest-and-oldest' =>
            [
                'ar' => 'يجب ان تكون اما latest او oldest',
                'en' => 'it should be latest or oldest',
            ],
    ];
