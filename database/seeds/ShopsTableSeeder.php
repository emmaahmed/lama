<?php

use Illuminate\Database\Seeder;
use Modules\Shop\Entities\Shop;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shop_1 = new Shop();
        $shop_1->category_id = 1;
        $shop_1->city_id = 1;
        $shop_1->type = 0;
        $shop_1->email = 'shop1@gmail.com';
        $shop_1->phone = '123456';
        $shop_1->logo = '1.png';
        $shop_1->password = '123456';
        $shop_1->status = 1;
        $shop_1->delivery_time = 20;
        $shop_1->delivery_fees = 20;
        $shop_1->from = '08:00:00';
        $shop_1->to = '20:00:00';
        $shop_1->online = 1;
        $shop_1->save();
        $shop_1->translateOrNew('en')->name = 'Burger King';
        $shop_1->translateOrNew('ar')->name = 'برجر كنج';
        $shop_1->translateOrNew('en')->short_description = 'Burger King Short Desc';
        $shop_1->translateOrNew('ar')->short_description = 'وصف برجر كنج';
        $shop_1->translateOrNew('en')->long_description = 'Burger King Long Description';
        $shop_1->translateOrNew('ar')->long_description = 'الوصف الكامل برجر كنج';
        $shop_1->save();

        $shop_1->images()->createMany([
            [
                'image' => 'shop1_img1.png',
            ],
            [
                'image' => 'shop1_img2.png',
            ]
        ]);

        $shop_2 = new Shop();
        $shop_2->category_id = 1;
        $shop_2->city_id = 2;
        $shop_2->type = 0;
        $shop_2->email = 'shop2@gmail.com';
        $shop_2->phone = '123123';
        $shop_2->logo = '2.png';
        $shop_2->password = '123456';
        $shop_2->status = 1;
        $shop_2->delivery_time = 30;
        $shop_2->delivery_fees = 30;
        $shop_2->from = '09:00:00';
        $shop_2->to = '21:00:00';
        $shop_2->online = 1;
        $shop_2->save();
        $shop_2->translateOrNew('en')->name = 'KFC';
        $shop_2->translateOrNew('ar')->name = 'كنتاكي';
        $shop_2->translateOrNew('en')->short_description = 'KFC Short Desc';
        $shop_2->translateOrNew('ar')->short_description = 'وصف كنتاكي';
        $shop_2->translateOrNew('en')->long_description = 'KFC Long Description';
        $shop_2->translateOrNew('ar')->long_description = 'الوصف الكامل كنتاكي';
        $shop_2->save();

        $shop_2->images()->createMany([
            [
                'image' => 'shop2_img1.png',
            ]
        ]);

        $shop_3 = new Shop();
        $shop_3->category_id = 1;
        $shop_3->city_id = 1;
        $shop_3->type = 0;
        $shop_3->email = 'shop3@gmail.com';
        $shop_3->phone = '123456789';
        $shop_3->logo = '3.png';
        $shop_3->password = '123456';
        $shop_3->status = 1;
        $shop_3->delivery_time = 40;
        $shop_3->delivery_fees = 40;
        $shop_3->from = '10:00:00';
        $shop_3->to = '22:00:00';
        $shop_3->online = 1;
        $shop_3->save();
        $shop_3->translateOrNew('en')->name = 'Mac';
        $shop_3->translateOrNew('ar')->name = 'ماكدونالدز';
        $shop_3->translateOrNew('en')->short_description = 'Mac Short Desc';
        $shop_3->translateOrNew('ar')->short_description = 'وصف ماكدونالدز';
        $shop_3->translateOrNew('en')->long_description = 'Mac Long Description';
        $shop_3->translateOrNew('ar')->long_description = 'الوصف الكامل ماكدونالدز';
        $shop_3->save();

        $shop_3->images()->createMany([
            [
                'image' => 'shop3_img1.png',
            ]
        ]);

        $shop_4 = new Shop();
        $shop_4->category_id = 2;
        $shop_4->city_id = 3;
        $shop_4->type = 0;
        $shop_4->email = 'shop4@gmail.com';
        $shop_4->phone = '123456123';
        $shop_4->logo = '4.png';
        $shop_4->password = '123456';
        $shop_4->status = 1;
        $shop_4->delivery_time = 10;
        $shop_4->delivery_fees = 10;
        $shop_4->from = '07:00:00';
        $shop_4->to = '24:00:00';
        $shop_4->online = 1;
        $shop_4->save();
        $shop_4->translateOrNew('en')->name = 'Awlad Ragab';
        $shop_4->translateOrNew('ar')->name = 'أولاد رجب';
        $shop_4->translateOrNew('en')->short_description = 'Awlad Ragab Short Desc';
        $shop_4->translateOrNew('ar')->short_description = 'وصف أولاد رجب';
        $shop_4->translateOrNew('en')->long_description = 'Awlad Ragab Long Description';
        $shop_4->translateOrNew('ar')->long_description = 'الوصف الكامل أولاد رجب';
        $shop_4->save();

        $shop_4->images()->createMany([
            [
                'image' => 'shop4_img1.png',
            ]
        ]);

        $shop_1->assignRole('shop1');
    }


}
