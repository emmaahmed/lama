<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category1 = new \App\City();
        $category1->save();
        $category1->translateOrNew('en')->name = 'Cairo';
        $category1->translateOrNew('ar')->name = 'القاهرة';
        $category1->save();

        $category2 = new \App\City();
        $category2->save();
        $category2->translateOrNew('en')->name = 'Giza';
        $category2->translateOrNew('ar')->name = 'الجيزة';
        $category2->save();

        $category3 = new \App\City();
        $category3->save();
        $category3->translateOrNew('en')->name = 'Alex';
        $category3->translateOrNew('ar')->name = 'الأسكندرية';
        $category3->save();
    }
}
