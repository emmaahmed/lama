<?php

use App\SocialMedia;
use Illuminate\Database\Seeder;

class SocialMediasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $offer1 = new SocialMedia();
        $offer1->link = 'https://www.facebook.com/';
        $offer1->logo = '1.png';
        $offer1->save();

        $offer2 = new SocialMedia();
        $offer2->link = 'https://www.instagram.com/';
        $offer2->logo = '2.png';
        $offer2->save();

        $offer3 = new SocialMedia();
        $offer3->link = 'http://lama.my-staff.net/';
        $offer3->logo = '3.png';
        $offer3->save();
    }
}
