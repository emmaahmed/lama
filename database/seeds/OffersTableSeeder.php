<?php

use Illuminate\Database\Seeder;
use Modules\Product\Entities\Offer;

class OffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $offer1 = new Offer();
        $offer1->product_id = 1;
        $offer1->image = '1.png';
        $offer1->save();

        $offer2 = new Offer();
        $offer2->product_id = 2;
        $offer2->image = '2.png';
        $offer2->save();

        $offer3 = new Offer();
        $offer3->product_id = 3;
        $offer3->image = '3.png';
        $offer3->save();
    }
}
