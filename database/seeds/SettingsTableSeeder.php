<?php

use Illuminate\Database\Seeder;
use Modules\Setting\Entities\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = new Setting();
        $settings->image = '1.png';
        $settings->fast_delivery_fees = '50';
        $settings->save();
        $settings->translateOrNew('en')->name = 'Social Media';
        $settings->translateOrNew('ar')->name = 'السوشيال ميديا';
        $settings->translateOrNew('en')->about = 'You can follow us and view all exclusive offers and products through Social Media and interact with us always .';
        $settings->translateOrNew('ar')->about = 'يمكنك متابعتنا ومشاهدة كل العروض الحصرية والمنتجات من خلال السوشيال ميديا والتفاعل معنا دائما .';
        $settings->save();
    }
}
