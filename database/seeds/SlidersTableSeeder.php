<?php

use App\Slider;
use Illuminate\Database\Seeder;

class SlidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slider1 = new Slider();
        $slider1->shop_id = 1;
        $slider1->image = '1.png';
        $slider1->save();

        $slider2 = new Slider();
        $slider2->shop_id = 2;
        $slider2->image = '2.png';
        $slider2->save();

        $slider3 = new Slider();
        $slider3->shop_id = 3;
        $slider3->image = '3.png';
        $slider3->save();
    }
}
