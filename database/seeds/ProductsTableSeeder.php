<?php

use Illuminate\Database\Seeder;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductInfo;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product_1 = new Product();
        $product_1->shop_id = 1;
        $product_1->price_before = 200;
        $product_1->price_after = 150;
        $product_1->percent = 25;
        $product_1->quantity = 150;
        $product_1->selling = 10;
        $product_1->has_discount = 1;
        $product_1->save();
        $product_1->translateOrNew('en')->name = 'Big Mac';
        $product_1->translateOrNew('ar')->name = 'بيج ماك';
        $product_1->translateOrNew('en')->short_description = 'Big Mac Short Desc';
        $product_1->translateOrNew('ar')->short_description = 'وصف بيج ماك';
        $product_1->translateOrNew('en')->long_description = 'Big Mac Long Description';
        $product_1->translateOrNew('ar')->long_description = 'الوصف الكامل لبيج ماك';
        $product_1->save();

        $product_1->images()->createMany([
            [
                'image' => 'product_1_img1.png',
            ],
            [
                'image' => 'product_1_img2.png',
            ]
        ]);

        $product_2 = new Product();
        $product_2->shop_id = 1;
        $product_2->price_before = 300;
        $product_2->price_after = 270;
        $product_2->percent = 10;
        $product_2->quantity = 250;
        $product_2->selling = 12;
        $product_2->has_discount = 1;
        $product_2->save();
        $product_2->translateOrNew('en')->name = 'Big Tasty';
        $product_2->translateOrNew('ar')->name = 'بيج تستي';
        $product_2->translateOrNew('en')->short_description = 'Big Tasty Description';
        $product_2->translateOrNew('ar')->short_description = 'وصف بيج تستي';
        $product_2->translateOrNew('en')->long_description = 'Big Tasty Long Description';
        $product_2->translateOrNew('ar')->long_description = 'الوصف الكامل لبيج تستي';
        $product_2->save();

        $product_2->images()->createMany([
            [
                'image' => 'product_2_img1.png',
            ]
        ]);

        $product_3 = new Product();
        $product_3->shop_id = 1;
        $product_3->price_before = 350;
        $product_3->price_after = 304.5;
        $product_3->percent = 13;
        $product_3->quantity = 300;
        $product_3->selling = 8;
        $product_3->has_discount = 1;
        $product_3->save();
        $product_3->translateOrNew('en')->name = 'Grand Share Box';
        $product_3->translateOrNew('ar')->name = 'جراند شير بوكس';
        $product_3->translateOrNew('en')->short_description = 'Grand Share Box Description';
        $product_3->translateOrNew('ar')->short_description = 'وصف جراند شير بوكس';
        $product_3->translateOrNew('en')->long_description = 'Grand Share Box Long Description';
        $product_3->translateOrNew('ar')->long_description = 'الوصف الكامل لجراند شير بوكس';
        $product_3->save();

        $product_3->images()->createMany([
            [
                'image' => 'product_3_img1.png',
            ]
        ]);

        $product_4 = new Product();
        $product_4->shop_id = 1;
        $product_4->price_before = 260;
        $product_4->quantity = 170;
        $product_4->selling = 5;
        $product_4->save();
        $product_4->translateOrNew('en')->name = 'Chicken Mac';
        $product_4->translateOrNew('ar')->name = 'تشيكن ماك';
        $product_4->translateOrNew('en')->short_description = 'Chicken Mac Description';
        $product_4->translateOrNew('ar')->short_description = 'وصف تشيكن ماك';
        $product_4->translateOrNew('en')->long_description = 'Chicken Mac Long Description';
        $product_4->translateOrNew('ar')->long_description = 'الوصف الكامل لوصف تشيكن ماك';
        $product_4->save();

        $product_4->images()->createMany([
            [
                'image' => 'product_4_img1.png',
            ]
        ]);

        $product_5 = new Product();
        $product_5->shop_id = 4;
        $product_5->sub_category_id = 2;
        $product_5->price_before = 2500;
        $product_5->price_after = 2000;
        $product_5->percent = 20;
        $product_5->quantity = 100;
        $product_5->selling = 5;
        $product_5->has_discount = 1;
        $product_5->save();
        $product_5->translateOrNew('en')->name = 'Zanousy';
        $product_5->translateOrNew('ar')->name = 'غسالة زانوسي';
        $product_5->translateOrNew('en')->short_description = 'Zanousy Description';
        $product_5->translateOrNew('ar')->short_description = 'وصف غسالة زانوسي';
        $product_5->translateOrNew('en')->long_description = 'Zanousy Long Description';
        $product_5->translateOrNew('ar')->long_description = 'الوصف الكامل لوصف غسالة زانوسي';
        $product_5->save();

        $product_5->images()->createMany([
            [
                'image' => 'product_5_img1.png',
            ]
        ]);

        $product_other_info1 = new ProductInfo();
        $product_other_info1->product_id = 5;
        $product_other_info1->save();
        $product_other_info1->translateOrNew('en')->text1 = 'Battery';
        $product_other_info1->translateOrNew('ar')->text1 = 'البطارية';
        $product_other_info1->translateOrNew('en')->text2 = 'Battery Capacity';
        $product_other_info1->translateOrNew('ar')->text2 = 'سعة البطارية';
        $product_other_info1->translateOrNew('en')->text3 = '4000';
        $product_other_info1->translateOrNew('ar')->text3 = '4000';
        $product_other_info1->save();

        $product_other_info2 = new ProductInfo();
        $product_other_info2->product_id = 5;
        $product_other_info2->save();
        $product_other_info2->translateOrNew('en')->text1 = 'Storage';
        $product_other_info2->translateOrNew('ar')->text1 = 'التخزين';
        $product_other_info2->translateOrNew('en')->text2 = 'Internal Storage Capacity';
        $product_other_info2->translateOrNew('ar')->text2 = 'سعة التخزين';
        $product_other_info2->translateOrNew('en')->text3 = '128';
        $product_other_info2->translateOrNew('ar')->text3 = '128';
        $product_other_info2->save();

        $product_other_info3 = new ProductInfo();
        $product_other_info3->product_id = 5;
        $product_other_info3->save();
        $product_other_info3->translateOrNew('en')->text2 = 'Internal Ram In GB';
        $product_other_info3->translateOrNew('ar')->text2 = 'سعة التخزين';
        $product_other_info3->translateOrNew('en')->text3 = '6';
        $product_other_info3->translateOrNew('ar')->text3 = '6';
        $product_other_info3->save();

        $product_other_info4 = new ProductInfo();
        $product_other_info4->product_id = 5;
        $product_other_info4->save();
        $product_other_info4->translateOrNew('en')->text1 = 'Camera';
        $product_other_info4->translateOrNew('ar')->text1 = 'الكاميرا';
        $product_other_info4->translateOrNew('en')->text2 = 'Secondry Camera';
        $product_other_info4->translateOrNew('ar')->text2 = 'الكاميرا الفرعية';
        $product_other_info4->translateOrNew('en')->text3 = 'Yes';
        $product_other_info4->translateOrNew('ar')->text3 = 'نعم';
        $product_other_info4->save();
    }
}
