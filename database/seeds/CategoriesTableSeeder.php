<?php

use Illuminate\Database\Seeder;
use Modules\Category\Entities\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category1 = new Category();
        $category1->image = '1.png';
        $category1->save();
        $category1->translateOrNew('en')->name = 'Restaurants';
        $category1->translateOrNew('ar')->name = 'مطاعم';
        $category1->save();

        $category2 = new Category();
        $category2->image = '2.png';
        $category2->save();
        $category2->translateOrNew('en')->name = 'Super Market';
        $category2->translateOrNew('ar')->name = 'سوبر ماركت';
        $category2->save();

        $category3 = new Category();
        $category3->image = '3.png';
        $category3->save();
        $category3->translateOrNew('en')->name = 'Productive families';
        $category3->translateOrNew('ar')->name = 'أسر منتجة';
        $category3->save();
    }
}
