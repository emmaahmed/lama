<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(ShopsTableSeeder::class);
        $this->call(SubCategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(VariationTableSeeder::class);
        $this->call(OffersTableSeeder::class);
        $this->call(TutorialsTableSeeder::class);
        $this->call(SlidersTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RatesTableSeeder::class);
        $this->call(SocialMediasTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(NotificationsTableSeeder::class);
        $this->call(AdminTableSeeder::class);
    }
}
