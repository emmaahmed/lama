<?php

use Illuminate\Database\Seeder;

class NotificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $notification1 = new \App\Notification();
        $notification1->user_id = 1;
        $notification1->shop_id = 1;
        $notification1->status = 0;
        $notification1->save();
        $notification1->translateOrNew('en')->title = 'Order Accepted';
        $notification1->translateOrNew('ar')->title = 'تم قبول طلبك';
        $notification1->translateOrNew('en')->body = 'Your order from .. has been accepted';
        $notification1->translateOrNew('ar')->body = 'مبروك لقد تم قبول طلبك من .. ';
        $notification1->save();

        $notification2 = new \App\Notification();
        $notification2->user_id = 1;
        $notification2->shop_id = 1;
        $notification2->status = 0;
        $notification2->save();
        $notification2->translateOrNew('en')->title = 'Order Accepted';
        $notification2->translateOrNew('ar')->title = 'تم قبول طلبك';
        $notification2->translateOrNew('en')->body = 'Your order from .. has been accepted';
        $notification2->translateOrNew('ar')->body = 'مبروك لقد تم قبول طلبك من .. ';
        $notification2->save();

        $notification3 = new \App\Notification();
        $notification3->user_id = 1;
        $notification3->shop_id = 2;
        $notification3->status = 0;
        $notification3->save();
        $notification3->translateOrNew('en')->title = 'Order Accepted';
        $notification3->translateOrNew('ar')->title = 'تم قبول طلبك';
        $notification3->translateOrNew('en')->body = 'Your order from .. has been accepted';
        $notification3->translateOrNew('ar')->body = 'مبروك لقد تم قبول طلبك من .. ';
        $notification3->save();

        $notification4 = new \App\Notification();
        $notification4->user_id = 1;
        $notification4->shop_id = 3;
        $notification4->status = 0;
        $notification4->save();
        $notification4->translateOrNew('en')->title = 'Order Accepted';
        $notification4->translateOrNew('ar')->title = 'تم قبول طلبك';
        $notification4->translateOrNew('en')->body = 'Your order from .. has been accepted';
        $notification4->translateOrNew('ar')->body = 'مبروك لقد تم قبول طلبك من .. ';
        $notification4->save();

        $notification5 = new \App\Notification();
        $notification5->user_id = 1;
        $notification5->shop_id = 4;
        $notification5->status = 0;
        $notification5->save();
        $notification5->translateOrNew('en')->title = 'Order Accepted';
        $notification5->translateOrNew('ar')->title = 'تم قبول طلبك';
        $notification5->translateOrNew('en')->body = 'Your order from .. has been accepted';
        $notification5->translateOrNew('ar')->body = 'مبروك لقد تم قبول طلبك من .. ';
        $notification5->save();
    }
}
