<?php

use Illuminate\Database\Seeder;
use Modules\Shop\Entities\SubCategory;

class SubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sub_category1 = new SubCategory();
        $sub_category1->shop_id = 4;
        $sub_category1->save();
        $sub_category1->translateOrNew('en')->name = 'Food Products';
        $sub_category1->translateOrNew('ar')->name = 'منتجات غذائية';
        $sub_category1->save();

        $sub_category2 = new SubCategory();
        $sub_category2->shop_id = 4;
        $sub_category2->save();
        $sub_category2->translateOrNew('en')->name = 'Devices';
        $sub_category2->translateOrNew('ar')->name = 'أجهزة';
        $sub_category2->save();

        $sub_category3 = new SubCategory();
        $sub_category3->shop_id = 4;
        $sub_category3->save();
        $sub_category3->translateOrNew('en')->name = 'Cleaning supplies';
        $sub_category3->translateOrNew('ar')->name = 'مستلزمات تنظيف';
        $sub_category3->save();

        $sub_category4 = new SubCategory();
        $sub_category4->shop_id = 4;
        $sub_category4->save();
        $sub_category4->translateOrNew('en')->name = 'Child';
        $sub_category4->translateOrNew('ar')->name = 'الطفل';
        $sub_category4->save();
    }
}
