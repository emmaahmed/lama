<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admins Permissions
        Permission::create([
            'name' => 'show_admin',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'add_admin',
            'display_name' => 'إضافة',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'edit_admin',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_admin',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        // Users Permissions
        Permission::create([
            'name' => 'show_user',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'edit_user',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_user',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'export_user',
            'display_name' => 'تصدير',
            'guard_name' => 'admin'
        ]);
        // Delegates Permissions
        Permission::create([
            'name' => 'show_delegate',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'add_delegate',
            'display_name' => 'إضافة',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'edit_delegate',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_delegate',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        // Categories Permissions
        Permission::create([
            'name' => 'show_category',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'add_category',
            'display_name' => 'إضافة',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'edit_category',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_category',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        // Cities Permissions
        Permission::create([
            'name' => 'show_city',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'add_city',
            'display_name' => 'إضافة',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'edit_city',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_city',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        // Sliders Permissions
        Permission::create([
            'name' => 'show_slider',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'add_slider',
            'display_name' => 'إضافة',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'edit_slider',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_slider',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        // Offers Permissions
        Permission::create([
            'name' => 'show_offer',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'add_offer',
            'display_name' => 'إضافة',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'edit_offer',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_offer',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        // Tutorials Permissions
        Permission::create([
            'name' => 'show_tutorial',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'add_tutorial',
            'display_name' => 'إضافة',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'edit_tutorial',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_tutorial',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        // Social Permissions
        Permission::create([
            'name' => 'show_social',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'add_social',
            'display_name' => 'إضافة',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'edit_social',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_social',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        // Shops Permissions
        Permission::create([
            'name' => 'show_shop',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'add_shop',
            'display_name' => 'إضافة',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'edit_shop',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_shop',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'export_shop',
            'display_name' => 'تصدير',
            'guard_name' => 'admin'
        ]);
        // Sub Categories Permissions
        Permission::create([
            'name' => 'show_sub_category',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'add_sub_category',
            'display_name' => 'إضافة',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'edit_sub_category',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_sub_category',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        // Products Permissions
        Permission::create([
            'name' => 'show_product',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'add_product',
            'display_name' => 'إضافة',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'edit_product',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_product',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'export_product',
            'display_name' => 'تصدير',
            'guard_name' => 'admin'
        ]);
        // Orders Permissions
        Permission::create([
            'name' => 'show_order',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'delete_order',
            'display_name' => 'حذف',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'export_order',
            'display_name' => 'تصدير',
            'guard_name' => 'admin'
        ]);
//        Notifications
        Permission::create([
            'name' => 'show_sent',
            'display_name' => 'عرض الصادر',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'show_inbox',
            'display_name' => 'عرض الوارد',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'reply_inbox',
            'display_name' => 'الرد علي الوارد',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'send_notification',
            'display_name' => 'إرسال إشعار',
            'guard_name' => 'admin'
        ]);
        // Home Permissions
        Permission::create([
            'name' => 'show_statistics',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        // Settings Permissions
        Permission::create([
            'name' => 'edit_settings',
            'display_name' => 'تعديل',
            'guard_name' => 'admin'
        ]);
        // Reports Permissions
        Permission::create([
            'name' => 'show_reports',
            'display_name' => 'عرض',
            'guard_name' => 'admin'
        ]);
        Permission::create([
            'name' => 'export_reports',
            'display_name' => 'تصدير',
            'guard_name' => 'admin'
        ]);

        // Roles

        $super_admin = Role::create([
            'name' => 'super_admin',
            'display_name' => 'مدير النظام',
            'guard_name' => 'admin'
        ]);

        $permissions = \Spatie\Permission\Models\Permission::where('guard_name','admin')->pluck('id')->toArray();

        $super_admin->permissions()->sync($permissions);

////////////////////////////////////////////////////////////////////////////////////////////////


        // Shop
        // Admins Permissions
        Permission::create([
            'name' => 'show_admin',
            'display_name' => 'عرض',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'add_admin',
            'display_name' => 'إضافة',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'edit_admin',
            'display_name' => 'تعديل',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'delete_admin',
            'display_name' => 'حذف',
            'guard_name' => 'shop'
        ]);
        // Sub Categories Permissions
        Permission::create([
            'name' => 'show_sub_category',
            'display_name' => 'عرض',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'add_sub_category',
            'display_name' => 'إضافة',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'edit_sub_category',
            'display_name' => 'تعديل',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'delete_sub_category',
            'display_name' => 'حذف',
            'guard_name' => 'shop'
        ]);
        // Products Permissions
        Permission::create([
            'name' => 'show_product',
            'display_name' => 'عرض',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'add_product',
            'display_name' => 'إضافة',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'edit_product',
            'display_name' => 'تعديل',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'delete_product',
            'display_name' => 'حذف',
            'guard_name' => 'shop'
        ]);
        // Orders Permissions
        Permission::create([
            'name' => 'show_order',
            'display_name' => 'عرض',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'delete_order',
            'display_name' => 'حذف',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'export_order',
            'display_name' => 'تصدير',
            'guard_name' => 'shop'
        ]);
//        Notifications
        Permission::create([
            'name' => 'notifications',
            'display_name' => 'عرض',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'send_notification',
            'display_name' => 'إرسال إشعار',
            'guard_name' => 'shop'
        ]);
        // Home Permissions
        Permission::create([
            'name' => 'show_statistics',
            'display_name' => 'عرض',
            'guard_name' => 'shop'
        ]);
        // Reports Permissions
        Permission::create([
            'name' => 'show_reports',
            'display_name' => 'عرض',
            'guard_name' => 'shop'
        ]);
        Permission::create([
            'name' => 'export_reports',
            'display_name' => 'تصدير',
            'guard_name' => 'shop'
        ]);

        // Roles

        $shop1 = Role::create([
            'name' => 'shop1',
            'display_name' => 'مدير المتجر',
            'guard_name' => 'shop'
        ]);

        $permissions_shop = \Spatie\Permission\Models\Permission::where('guard_name','shop')->pluck('id')->toArray();

        $shop1->permissions()->sync($permissions_shop);
    }
}
