<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'name' => 'Ibrahim',
            'phone' => '01222502043',
            'email' => 'hema.3nany@gmail.com',
            'image' => '1.png',
            'password' => '123456',
            'jwt' => Str::random('25'),
            'status' => 1,
            'latitude' => '31.1',
            'longitude' => '30.2',
            'location' => 'test address'
        ]);

        $user2 = User::create([
            'name' => 'mostafa elnagar',
            'phone' => '01009313883',
            'email' => 'melnagar271@gmail.com',
            'image' => '2.png',
            'password' => '123456',
            'jwt' => Str::random('25'),
            'status' => 1,
            'latitude' => '31.1',
            'longitude' => '30.2',
            'location' => 'test address'
        ]);

        $user3 = User::create([
            'name' => 'test3',
            'phone' => '01090624441',
            'email' => 'test3@gmail.com',
            'image' => '3.png',
            'password' => '123456',
            'jwt' => Str::random('25'),
            'status' => 1,
            'latitude' => '31.1',
            'longitude' => '30.2',
            'location' => 'test address'
        ]);

        $user4 = User::create([
            'name' => 'test4',
            'phone' => '01017257056',
            'email' => 'test4@gmail.com',
            'image' => '4.png',
            'password' => '123456',
            'jwt' => Str::random('25'),
            'status' => 0,
            'latitude' => '31.1',
            'longitude' => '30.2',
            'location' => 'test address'
        ]);
    }
}
