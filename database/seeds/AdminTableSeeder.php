<?php

use Illuminate\Database\Seeder;
use Modules\Admin\Entities\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_admin = Admin::create([
            'name' => 'Manager',
            'email' => 'manager@gmail.com',
            'image' => '1.png',
            'password' => '123456',
            'status' => 1
        ]);

        $super_admin->assignRole('super_admin');
    }
}
