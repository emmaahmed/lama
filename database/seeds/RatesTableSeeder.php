<?php

use Illuminate\Database\Seeder;
use Modules\Product\Entities\Rate;

class RatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rate::create([
            'product_id' => 1,
            'user_id' => 1,
            'rate' => '3',
            'comment' => 'good product',
        ]);
        Rate::create([
            'product_id' => 1,
            'user_id' => 2,
            'user_name' => 'Ibrahim Al3nany',
            'rate' => '4',
            'comment' => 'very good product',
        ]);
        Rate::create([
            'product_id' => 2,
            'user_id' => 1,
            'user_name' => 'ali salah',
            'rate' => '2',
            'comment' => 'accepted product',
        ]);
    }
}
