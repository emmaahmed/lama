<?php

use Illuminate\Database\Seeder;
use Modules\Product\Entities\ProductVariation;
use Modules\Product\Entities\Variation;
use Modules\Product\Entities\VariationOption;

class VariationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //        Create Variations
        $variation1 = new Variation();
        $variation1->type = 3;
        $variation1->required = 1;
        $variation1->save();
        $variation1->translateOrNew('en')->name = 'Size';
        $variation1->translateOrNew('ar')->name = 'الحجم';
        $variation1->save();
        $variation2 = new Variation();
        $variation2->type = 2;
        $variation2->required = 1;
        $variation2->save();
        $variation2->translateOrNew('en')->name = 'Color';
        $variation2->translateOrNew('ar')->name = 'اللون';
        $variation2->save();
        $variation3 = new Variation();
        $variation3->type = 1;
        $variation3->required = 0;
        $variation3->save();
        $variation3->translateOrNew('en')->name = 'Adds';
        $variation3->translateOrNew('ar')->name = 'الإضافات';
        $variation3->save();

//        Create Product Variations
        ProductVariation::create([
            'variation_id' => 1,
            'product_id' => 1,
        ]);
        ProductVariation::create([
            'variation_id' => 2,
            'product_id' => 1,
        ]);
        ProductVariation::create([
            'variation_id' => 3,
            'product_id' => 1,
        ]);

        //        Create Options
        $option1 = new VariationOption();
        $option1->product_variation_id = 1;
        $option1->price = 40;
        $option1->status = 1;
        $option1->save();
        $option1->translateOrNew('en')->name = 'Regular';
        $option1->translateOrNew('ar')->name = 'متوسط';
        $option1->save();
        $option2 = new VariationOption();
        $option2->product_variation_id = 1;
        $option2->price = 50;
        $option2->status = 0;
        $option2->save();
        $option2->translateOrNew('en')->name = 'Large';
        $option2->translateOrNew('ar')->name = 'كبير';
        $option2->save();

        //        Create Options
        $option3 = new VariationOption();
        $option3->product_variation_id = 2;
        $option3->price = 40;
        $option3->status = 1;
        $option3->save();
        $option3->translateOrNew('en')->name = '#FFFFFF';
        $option3->translateOrNew('ar')->name = '#FFFFFF';
        $option3->save();
        $option4 = new VariationOption();
        $option4->product_variation_id = 2;
        $option4->price = 50;
        $option4->status = 1;
        $option4->save();
        $option4->translateOrNew('en')->name = '#d687e8';
        $option4->translateOrNew('ar')->name = '#d687e8';
        $option4->save();


        $option5 = new VariationOption();
        $option5->product_variation_id = 3;
        $option5->price = 3;
        $option5->status = 1;
        $option5->save();
        $option5->translateOrNew('en')->name = 'Tomato';
        $option5->translateOrNew('ar')->name = 'طماطم';
        $option5->save();
        $option6 = new VariationOption();
        $option6->product_variation_id = 3;
        $option6->price = 3;
        $option6->status = 1;
        $option6->save();
        $option6->translateOrNew('en')->name = 'Onions';
        $option6->translateOrNew('ar')->name = 'بصل';
        $option6->save();
        $option7 = new VariationOption();
        $option7->product_variation_id = 3;
        $option7->price = 3;
        $option7->status = 1;
        $option7->save();
        $option7->translateOrNew('en')->name = 'Sauce';
        $option7->translateOrNew('ar')->name = 'صوص';
        $option7->save();
        $option8 = new VariationOption();
        $option8->product_variation_id = 3;
        $option8->price = 3;
        $option8->status = 1;
        $option8->save();
        $option8->translateOrNew('en')->name = 'Cheese';
        $option8->translateOrNew('ar')->name = 'جبنة';
        $option8->save();
        $option9 = new VariationOption();
        $option9->product_variation_id = 3;
        $option9->price = 3;
        $option9->status = 1;
        $option9->save();
        $option9->translateOrNew('en')->name = 'Ketchup';
        $option9->translateOrNew('ar')->name = 'كاتشب';
        $option9->save();
    }
}
