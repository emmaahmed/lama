<?php

use App\Tutorial;
use Illuminate\Database\Seeder;

class TutorialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tutorial1 = new Tutorial();
        $tutorial1->image = '1.png';
        $tutorial1->type = 0;
        $tutorial1->save();
        $tutorial1->translateOrNew('en')->name = 'Order';
        $tutorial1->translateOrNew('ar')->name = 'اطلب';
        $tutorial1->translateOrNew('en')->description = 'You can order where to be';
        $tutorial1->translateOrNew('ar')->description = 'يمكنك الطلب أين تكون ....';
        $tutorial1->save();

        $tutorial2 = new Tutorial();
        $tutorial2->image = '2.png';
        $tutorial2->type = 0;
        $tutorial2->save();
        $tutorial2->translateOrNew('en')->name = 'Payment';
        $tutorial2->translateOrNew('ar')->name = 'الدفع';
        $tutorial2->translateOrNew('en')->description = 'You can order where to be';
        $tutorial2->translateOrNew('ar')->description = 'يمكنك الطلب أين تكون ....';
        $tutorial2->save();

        $tutorial3 = new Tutorial();
        $tutorial3->image = '3.png';
        $tutorial3->type = 0;
        $tutorial3->save();
        $tutorial3->translateOrNew('en')->name = 'Delivery';
        $tutorial3->translateOrNew('ar')->name = 'التوصيل';
        $tutorial3->translateOrNew('en')->description = 'You can order where to be';
        $tutorial3->translateOrNew('ar')->description = 'يمكنك الطلب أين تكون ....';
        $tutorial3->save();
    }
}
