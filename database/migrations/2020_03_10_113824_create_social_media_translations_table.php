<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialMediaTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_media_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('social_media_id')->unsigned();
            $table->string('name')->nullable();
            $table->text('about')->nullable();
            $table->string('locale')->index();
            $table->unique(['social_media_id','locale']);
            $table->timestamps();

            $table->foreign('social_media_id')->references('id')->on('social_media')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_media_translations');
    }
}
