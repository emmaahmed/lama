
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="endless admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, endless admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{ asset('site/images/favicon.ico') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('site/images/favicon.ico') }}" type="image/x-icon">
    <title>تطبيق الصياد العربي</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <style type="text/css">
        body{
            width: 650px;
            font-family: work-Sans, sans-serif;
            background-color: #f6f7fb;
            display: block;
        }
        a{
            text-decoration: none;
        }
        span {
            font-size: 14px;
        }
        p {
            font-size: 13px;
            line-height: 1.7;
            letter-spacing: 0.7px;
            margin-top: 0;
        }
        .text-center{
            text-align: center
        }
        h6 {
            font-size: 16px;
            margin: 0 0 18px 0;
        }
    </style>
</head>
<body style="margin: 30px auto;">
<table style="width: 100%">
    <tbody>
    <tr>
        <td>
            <table style="background-color: #f6f7fb; width: 100%">
                <tbody>
                <tr>
                    <td>
                        <table style="width: 650px; margin: 0 auto; margin-bottom: 30px">
                            <tbody>
                            <tr>
                                <td><img src="{{url('/assets/images/logo.png')}}" alt="الصياد العربي"></td>
                                <td style="text-align: right; color:#999"><span>تطبيق الصياد العربي</span></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <h6 style="font-weight: 600;  float: right;">تم اكتمال الطلب </h6>
            <table style="width: 650px; margin: 0 auto; background-color: #fff; text-align: right; border-radius: 8px">
                <thead>
                <tr>
                    <th>@lang('messages.order.total')</th>
                    <th>@lang('messages.order.quantity')</th>
                    <th>@lang('messages.price')</th>
                    <th>@lang('messages.name')</th>
                </tr>
                </thead>
                @isset($data['order']->order_products)
                    @foreach($data['order']->order_products as $product)
                        <tbody>
                        <tr>
                            <td>{{$product['total']}}</td>
                            <td>{{$product['quantity']}}</td>
                            <td>{{$product['price_after']}}</td>
                            <td>{{$product['name']}}</td>
                        </tr>
                        </tbody>
                    @endforeach
                @endisset
            </table>
            <p style="margin-bottom: 0;">
                شكرا,<br>تطبيق الصياد العربي</p>
{{--            <table style="width: 650px; margin: 0 auto; margin-top: 30px; text-align: center">--}}
{{--                <tbody>--}}
{{--                <tr>--}}
{{--                    <td><a href="email-header.html#" style="font-size:13px">Want to change how you receive these emails?</a></td>--}}
{{--                </tr>--}}
{{--                <tr>--}}
{{--                    <td>--}}
{{--                        <p style="font-size:13px; margin:0;">2018 - 19 Copy Right by Themeforest powerd by Pixel Strap</p>--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--                <tr>--}}
{{--                    <td><a href="email-header.html#" style="font-size:13px; margin:0;text-decoration: underline;">Unsubscribe</a></td>--}}
{{--                </tr>--}}
{{--                </tbody>--}}
{{--            </table>--}}
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
