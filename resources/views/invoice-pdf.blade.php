<!DOCTYPE html>
<html lang="ar" style="margin: 0 !important; padding:0!important; text-align: left; " dir="rtl">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <style>
        @page {
            margin: 0px;
        }

        body {
            font-family: 'examplefont', sans-serif;
            font-size: 12px;
        }

        hr {
            height: 2px;
            border-width: 1px;
            color: #000;
            background-color: #000;        }
    </style>
</head>
<body style="margin: 0 !important; padding:0!important; text-align: left;    ">
<div id="invoice-POS" style="text-align: left;margin: 0 !important; padding:0!important;">
    <div id="mid" >
        <div class="info" style="margin-left:100px; ">
            <p>فاتورة ضريبية مبسطة</p>

            <img src="{{$order->shop->logo}}" width="100">
        </div>
        <div class="info" style="margin-left:100px; ">

            <h3 style="margin: 0;">{{$order->shop->name}}</h3>
            <h3 style="margin: 0;">{{$order->order_number}}</h3>


        </div>
    </div>
    <!--End Info-->
    <!--End InvoiceTop-->
    <table style="direction: rtl; margin-left:100px; ">
        <tr class="tabletitle">
            <td class="tableitem">
                <p class="itemtext">الأسم : </p>
            </td>

            <td class="Hours" >
                <h4>{{$order->user->name}}</h4>
            </td>
            <td class="Rate">
                <h4></h4>
            </td>

        </tr>
        <tr class="tabletitle">
            <td class="tableitem">
                <p class="itemtext">الهاتف : </p>
            </td>

            <td class="Hours" >
                <h4>{{$order->user->phone}}</h4>
            </td>
            <td class="Rate">
                <h4></h4>
            </td>

        </tr>
        <tr class="tabletitle">
            <td class="tableitem">
                <p class="itemtext">التاريخ : </p>
            </td>

            <td class="Hours" >
                <h4>{{\Carbon\Carbon::now()->format('d/m/Y')}}</h4>
            </td>
            <td class="Rate">
                <h4></h4>
            </td>

        </tr>
        <tr class="tabletitle">
            <td class="tableitem">
                <p class="itemtext">الوقت : </p>
            </td>

            <td class="Hours" >
                <h4>{{\Carbon\Carbon::now()->format('h:i:s A')}}</h4>
            </td>
            <td class="Rate">
                <h4></h4>
            </td>

        </tr>


    </table>
{{--    <div id="mid">--}}
{{--        <div class="info" style="direction: rtl">--}}
{{--            <p style="float: right;">الأسم :  </p>--}}
{{--            <p style="float: left;"> {{$order->user->name}}  </p><br>--}}
{{--            <p style="float: right;">الهاتف :  </p>--}}
{{--            <p style="float: left;"> {{$order->user->phone}}  </p><br>--}}
{{--            <p style="float: right;">التاريخ :  </p>--}}
{{--            <p style="float: left;"> {{\Carbon\Carbon::now()->format('d/m/Y')}}  </p><br>--}}
{{--            <p style="float: right;">الوقت :  </p>--}}
{{--            <p style="float: left;"> {{\Carbon\Carbon::now()->format('h:i:s A')}}  </p><br>--}}
{{--            <p style="float: right;">العنوان :  </p>--}}
{{--            <p style="float: left;"> {{str_replace('‬','',$order->address->address)}}  </p>--}}



{{--        </div>--}}
{{--    </div>--}}
    <hr>
{{--    <div id="mid">--}}
{{--        <div class="info">--}}
{{--            <p>--}}
{{--                Delegate info:<br>--}}
{{--                Name : {{$order->delegate->name}} <br>--}}
{{--                Phone : {{$order->delegate->phone}}<br>--}}
{{--            </p>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <hr>--}}
<!--End Invoice Mid-->

    <div id="bot">

        <div id="table" style="direction: rtl">
            <table>
                <tr class="tabletitle">
                    <td class="item">
                        <h4>المنتج</h4>
                    </td>
                    <td class="Hours" style="width:100px ;">
                        <h4>العدد</h4>
                    </td>
                    <td class="Rate">
                        <h4>السعر</h4>
                    </td>
                </tr>
                @if(count($order->orderItems)>0)
                    @foreach ($order->orderItems  as $key => $order_item)



                        <tr class="service">
                            <td class="tableitem">
                                <p class="itemtext">{{$order_item->product->name}}</p>
                            </td>
                            <td class="tableitem">
                                <p class="itemtext"> * {{$order_item->quantity}}</p>
                            </td>
                            <td class="tableitem">
                                <p class="itemtext">{{$order_item->cost}}</p>
                            </td>
                        </tr>
                    @endforeach
                @endif

                <tr class="tabletitle">
                    <td class="Rate">
                        <h3>المجموع</h3>
                    </td>
                    <td class="payment">
                        <h4>{{$order->total_cost }}</h4>
                    </td>
                    <td></td>

                </tr>

                <tr class="tabletitle">
                    <td class="Rate">
                        <h3>التوصيل </h3>
                    </td>
                    <td class="payment">
                        <h4>{{$fees}}</h4>
                    </td>

                </tr>
                {{--                <tr class="tabletitle">--}}
                {{--                    <td class="Rate">--}}
                {{--                        <h3>delivery</h3>--}}
                {{--                    </td>--}}
                {{--                    <td class="payment">--}}
                {{--                        <h4>{{$lastOffer->offer}}</h4>--}}
                {{--                    </td>--}}
                {{--                    <td></td>--}}

                {{--                </tr>--}}
                <tr class="tabletitle">
                    <td class="Rate">
                        <h3>المجموع الكلي</h3>
                    </td>
                    <td class="payment">
                        <h4>{{ strval($order->total_cost + ($fees))}}</h4>
                    </td>
                    <td></td>

                </tr>

            </table>
        </div>
        <!--End Table-->
        {{--        @if($order->notes!=null && $order->notes!='')--}}
        {{--            <hr>--}}

        {{--            <div id="legalcopy">--}}
        {{--                <p class="legal">Order Notes:<strong>{{$order->notes}}</strong>--}}
        {{--                </p>--}}
        {{--            </div>--}}
        {{--        @endif--}}
        <hr>
        <div id="legalcopy" style="margin-left: 65px;">
            <p class="legal">{{\Carbon\Carbon::now()->format('l jS \of F Y h:i:s A')}}</p>
        </div>
        <div class="col-md-12" style="margin-left: 65px;">
            <img src="{{$output}}"><br>


        </div>

        <div id="legalcopy" style="margin-left: 65px;">
            <h4 style="margin: 0;    margin-right: 22px; text-align: center;width: 180px;float: left;">{{str_replace('‬','',$order->address->address)}}</h4>

        </div>


    </div>
    <!--End InvoiceBot-->
</div>
<!--End Invoice-->
</body>
</html>
